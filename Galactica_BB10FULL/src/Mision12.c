/*
 * Mision12.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "Mision12.h"
#include "QuickModeGame.h"
#include "Lista.h"
#include "Boton.h"
#include "Nave.h"
#include "Enemigo.h"
#include "BarraVida.h"
#include "Audio.h"
#include "MisionModeGOver.h"
#include "MisionMode.h"
#include "Almacenamiento.h"
#include "Animation.h"
#include "Dibujar.h"
#include "Textura.h"
#include "Control.h"
#include "Collition.h"
#include "NMini.h"
#include "Mision1.h"
#include "Cadena.h"

static Mision12 Mision12S;

void Mision12_reiniciar()
{
	QuickModeGame_eliminarTodos();
	QuickModeGame_eliminaAnimaciones();
	Mision12S.positionMiniS.X = 6;
	Mision12S.positionMiniS.Y = 900;
	Mision12S.positionTime.X = 420;
	Mision12S.positionTime.Y = 900;
	Lista_Destruir(&((*QuickModeGame_getSObject()).igb.botones));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BPause, 5, 950));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BAudio, 530, 950));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BPower, 480, 20));

	(*QuickModeGame_getSObject()).igb.velocidadEnem = 2;
	(*QuickModeGame_getSObject()).igb.velocidadEnemY = 2;
	(*QuickModeGame_getSObject()).igb.velocidadHiEnem = 3;
	(*QuickModeGame_getSObject()).igb.velocidadHiEnemY = 3;
	(*QuickModeGame_getSObject()).igb.maxEnemigos = 4;
	Mision12S.cuentaTiempo = 90;
	(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo = 0;
	Mision12S.ultimoTMinuto = 0;
	(*QuickModeGame_getSObject()).igb.ultimoAumento = 0;
	NMini_reiniciar(Mision12S.mini);
	(*QuickModeGame_getSObject()).igb.EscalaMensaje =
			(*QuickModeGame_getSObject()).igb.escalaInicialMensaje = 1.5f;
	(*QuickModeGame_getSObject()).igb.actualFrameMensaje = 0;
	(*QuickModeGame_getSObject()).igb.tiempoTitileoMensaje = 0;
	(*QuickModeGame_getSObject()).igb.mensajeInicialActivo = 1;
	(*Nave_getSObject()).disparosDiagActivos = 1;
}

void Mision12_load()
{
	Mision12S.tituloMision = "Mission 12";

	Lista_Inicia(&Mision12S.mensajeMision);
	Lista_AddInFin(&Mision12S.mensajeMision,
			String_creaString("Protects the small"));
	Lista_AddInFin(&Mision12S.mensajeMision,
			String_creaString("spaceship of enemies,"));
	Lista_AddInFin(&Mision12S.mensajeMision,
			String_creaString("and avoid that them "));
	Lista_AddInFin(&Mision12S.mensajeMision,
			String_creaString("destroy it, until the "));
	Lista_AddInFin(&Mision12S.mensajeMision, String_creaString("time finish."));
	Lista_AddInFin(&Mision12S.mensajeMision,
			String_creaString("Tip:Do not let the"));
	Lista_AddInFin(&Mision12S.mensajeMision,
			String_creaString("enemy get too close."));

	Lista_Inicia(&Mision12S.recompensa);
	Lista_AddInFin(&Mision12S.recompensa,
			String_creaString("Maximum energy increased"));
	Lista_AddInFin(&Mision12S.recompensa,
			String_creaString("by 200, in Quick Game."));

	Lista_Inicia(&Mision12S.noRecompensa);
	Lista_AddInFin(&Mision12S.noRecompensa, String_creaString("No Reward.."));

	Mision12S.cuentaTiempo = 90;
	Mision12S.mini = NMini_Crear();
	Mision12S.barraVidaMini = BarraVida_Crear(GameFramework_Vector2(6, 890),
			BVmini);
}

void Mision12_update()
{
	Nave_update();
	QuickModeGame_updateEstrellas();
	Mision12_updateGame();
	QuickModeGame_updateEnemigos();
	QuickModeGame_updateAnimaciones();
	NMini_update(Mision12S.mini);
	for (Mision12S.i = 0;
			Mision12S.i < (*QuickModeGame_getSObject()).igb.botones.size;
			Mision12S.i++)
	{
		Boton_update(
				(Boton*) Lista_GetIn(&(*QuickModeGame_getSObject()).igb.botones,
						Mision12S.i));
	}

	for (Mision12S.i = 0;
			Mision12S.i < (*QuickModeGame_getSObject()).igb.enemigos.size;
			Mision12S.i++)
	{
		for (Mision12S.j = 0;
				Mision12S.j
						< (*(Enemigo*) Lista_GetIn(
								&(*QuickModeGame_getSObject()).igb.enemigos,
								Mision12S.i)).disparosSid.size; Mision12S.j++)
		{
			if (Collition_colisionDispAMini(
					&(*QuickModeGame_getSObject()).igb.colision, Mision12S.mini,
					(DisparoEnemigo*) Lista_GetIn(
							&(*(Enemigo*) Lista_GetIn(
									&(*QuickModeGame_getSObject()).igb.enemigos,
									Mision12S.i)).disparosSid, Mision12S.j))
					== 1
					&& (*(Enemigo*) Lista_GetIn(
							&(*QuickModeGame_getSObject()).igb.enemigos,
							Mision12S.i)).tipoEnemigo == ESid)
			{
				(*(DisparoEnemigo*) Lista_GetIn(
						&(*(Enemigo*) Lista_GetIn(
								&(*QuickModeGame_getSObject()).igb.enemigos,
								Mision12S.i)).disparosSid, Mision12S.j)).estaEnColision =
						0;

				(*Mision12S.mini).vida = (*Mision12S.mini).vida - 3;

				Lista_AddInFin(&(*Enemigo_getSObject()).disparosSidTotal,
						Lista_GetIn(
								&(*(Enemigo*) Lista_GetIn(
										&(*QuickModeGame_getSObject()).igb.enemigos,
										Mision12S.i)).disparosSid,
								Mision12S.j));

				Lista_RemoveAt(
						&(*(Enemigo*) Lista_GetIn(
								&(*QuickModeGame_getSObject()).igb.enemigos,
								Mision12S.i)).disparosSid, Mision12S.j);
			}
		}

	}

	for (Mision12S.i = 0;
			Mision12S.i < (*QuickModeGame_getSObject()).igb.enemigos.size;
			Mision12S.i++)
	{
		if (Collition_colisionMiniAEnemigo(
				&(*QuickModeGame_getSObject()).igb.colision, Mision12S.mini,
				(Enemigo*) Lista_GetIn(
						&(*QuickModeGame_getSObject()).igb.enemigos,
						Mision12S.i)) == 1)
		{
			(*Mision12S.mini).vida = (*Mision12S.mini).vida - 20;
			(*(Enemigo*) Lista_GetIn(
					&(*QuickModeGame_getSObject()).igb.enemigos, Mision12S.i)).estaEnColisionNave =
					1;
			(*(Enemigo*) Lista_GetIn(
					&(*QuickModeGame_getSObject()).igb.enemigos, Mision12S.i)).vida =
					0;
		}
	}

	//Cuenta de tiempo
	if (GameFramework_getTotalTime() > Mision12S.ultimoTMinuto + 1000000)
	{
		Mision12S.cuentaTiempo = Mision12S.cuentaTiempo - 1;
		Mision12S.ultimoTMinuto = GameFramework_getTotalTime();
	}

	QuickModeGame_updateMensajeInicial();
	BarraVida_update((*QuickModeGame_getSObject()).igb.barraVidaNave,
			(*Nave_getSObject()).Vida, (*Nave_getSObject()).MaxVida);
	BarraVida_update(Mision12S.barraVidaMini, (*Mision12S.mini).vida,
			(*Mision12S.mini).maxVida);
	if ((*Nave_getSObject()).Vida <= 0 || (*Mision12S.mini).vida <= 0)
	{
		Animation_update((*Animation_getSObject()).animacionesEstallidoNave);
	}
	Mision12_gameOver();

	if ((*Control_getSObject()).controlActivo == 1)
	{
		Control_update((*QuickModeGame_getSObject()).igb.control);
	}

}

void Mision12_draw()
{
	//CLEAR NEGRO
	Dibujar_drawSprite(Textura_getTexturaFondo(0), GameFramework_Vector2(0, 0),
			GameFramework_Color(0, 0, 0, 1), 0, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(40, 40));

	Nave_draw();
	QuickModeGame_drawEstrellas();
	QuickModeGame_drawEnemigos();
	Mision12_drawEstadNave();
	QuickModeGame_drawAnimaciones();
	Mision12_drawMensajeInicial();
	NMini_draw(Mision12S.mini);
	if ((*Nave_getSObject()).Vida <= 0 || (*Mision12S.mini).vida <= 0)
	{
		Animation_draw((*Animation_getSObject()).animacionesEstallidoNave);
	}

	for (Mision12S.i = 0;
			Mision12S.i < (*QuickModeGame_getSObject()).igb.botones.size;
			Mision12S.i++)
	{
		Boton_draw(
				(Boton*) Lista_GetIn(&(*QuickModeGame_getSObject()).igb.botones,
						Mision12S.i));
	}
	if ((*Control_getSObject()).controlActivo == 1)
	{
		Control_draw((*QuickModeGame_getSObject()).igb.control);
	}
	//drawEstadisticas(sb);
}

//******************Estadisticas de la nave*************
void Mision12_drawEstadNave()
{
	//** Vida de la nave
	Dibujar_drawSprite(Textura_getTexturaFondo(1),
			GameFramework_Vector2(0, 945), GameFramework_Color(0, 0, 0, 1), 0,
			GameFramework_Vector2(0, 0), GameFramework_Vector2(50, 2.6));

	BarraVida_draw((*QuickModeGame_getSObject()).igb.barraVidaNave);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_EnteroACadena((*Nave_getSObject()).Vida),
			GameFramework_Vector2(100, 975), GameFramework_Color(1, 1, 1, 1),
			0.8);

	Dibujar_drawText((*Textura_getSObject()).Fuente, "/",
			GameFramework_Vector2(285, 965), GameFramework_Color(1, 1, 1, 1),
			1.4);

	Nave_drawAnimacionesPoder();

	//Tiempo restante
	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "Time:",
					GameFramework_EnteroACadena(Mision12S.cuentaTiempo)),
			GameFramework_Vector2(420, 900),
			GameFramework_Color(GameFramework_getByteColorToFloatColor(244),
					GameFramework_getByteColorToFloatColor(232),
					GameFramework_getByteColorToFloatColor(16), 1), 0.9);

	BarraVida_draw(Mision12S.barraVidaMini);

	Dibujar_drawText((*Textura_getSObject()).Fuente, "Mini S",
			Mision12S.positionMiniS, GameFramework_Color(1, 1, 1, 1), 0.75);
}

void Mision12_updateGame()
{
	if (Mision12S.cuentaTiempo == 20
			&& (*QuickModeGame_getSObject()).igb.maxEnemigos <= 4)
	{
		(*QuickModeGame_getSObject()).igb.maxEnemigos =
				(*QuickModeGame_getSObject()).igb.maxEnemigos + 3;
	}

	if ((*QuickModeGame_getSObject()).igb.enemigos.size
			< (*QuickModeGame_getSObject()).igb.maxEnemigos)
	{
		Mision12_insertaEnemigo();
	}
}

void Mision12_gameOver()
{
	if (Mision12S.cuentaTiempo <= 0)
	{
		if ((*Audio_getSObject()).musicaActiva == 1)
		{
			Audio_stopAudio("Galactic");
			Audio_playAudio("GameOver");
		}
		(*MisionModeGOver_getSObject()).gana = 1;

		Almacenamiento_cargarMisiones();
		if (Almacenamiento_getPremiosDados() == 11)
		{
			Almacenamiento_salvarBonus(600, ALsumarAsalud);
			(*MisionModeGOver_getSObject()).cosasGanadas = &Mision12S.recompensa;
			Almacenamiento_salvarMisiones();
		}
		else
		{
			(*MisionModeGOver_getSObject()).cosasGanadas =
					&(*Mision1_getSObject()).yaRecompensa;
		}

		(*MisionMode_getSObject()).estadoActual = MMGameOver;
	}

	if ((*Nave_getSObject()).Vida <= 0)
	{
		(*Nave_getSObject()).Vida = 0;
		(*Nave_getSObject()).EscalaN = GameFramework_Vector2(0, 0);
		Animation_setPosition(
				(*Animation_getSObject()).animacionesEstallidoNave,
				(*Nave_getSObject()).PositionNav);
		(*MisionModeGOver_getSObject()).gana = 0;
		(*MisionModeGOver_getSObject()).cosasGanadas = &Mision12S.noRecompensa;
		if ((*(*Animation_getSObject()).animacionesEstallidoNave).animacionFinalizada
				== 1)
		{
			if ((*Audio_getSObject()).musicaActiva == 1)
			{
				Audio_stopAudio("Galactic");
				Audio_playAudio("GameOver");
			}
			(*MisionMode_getSObject()).estadoActual = MMGameOver;
		}
	}
	else if ((*Mision12S.mini).vida <= 0)
	{
		(*Mision12S.mini).vida = 0;
		(*Mision12S.mini).Escala = GameFramework_Vector2(0, 0);
		Animation_setPosition(
				(*Animation_getSObject()).animacionesEstallidoNave,
				(*Mision12S.mini).Position);
		(*MisionModeGOver_getSObject()).gana = 0;
		(*MisionModeGOver_getSObject()).cosasGanadas = &Mision12S.noRecompensa;
		if ((*(*Animation_getSObject()).animacionesEstallidoNave).animacionFinalizada
				== 1)
		{
			if ((*Audio_getSObject()).musicaActiva == 1)
			{
				Audio_stopAudio("Galactic");
				Audio_playAudio("GameOver");
			}
			(*MisionMode_getSObject()).estadoActual = MMGameOver;
		}
	}
}

//********Dibujo Mensaje Inicial
void Mision12_drawMensajeInicial()
{
	if ((*QuickModeGame_getSObject()).igb.mensajeInicialActivo == 1)
	{
		Dibujar_drawText((*Textura_getSObject()).Fuente,
				GameFramework_Concatenar(2, Mision12S.tituloMision, " Start!!"),
				GameFramework_Vector2(50, 620),
				GameFramework_Color(GameFramework_getByteColorToFloatColor(7),
						GameFramework_getByteColorToFloatColor(237),
						GameFramework_getByteColorToFloatColor(134), 1),
				(*QuickModeGame_getSObject()).igb.EscalaMensaje);
	}
}

void Mision12_insertaEnemigo()
{
	(*QuickModeGame_getSObject()).igb.num = 0;
	(*QuickModeGame_getSObject()).igb.num2 = 0;
	(*QuickModeGame_getSObject()).igb.num = GameFramework_Rand(1, 4);

	if ((*QuickModeGame_getSObject()).igb.num == 1)
	{
		(*QuickModeGame_getSObject()).igb.num2 = GameFramework_Rand(1, 3);
		if ((*QuickModeGame_getSObject()).igb.num2 == 1)
		{
			if ((*Enemigo_getSObject()).KamiTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).KamiTotal),
						(*Enemigo_getSObject()).KamiTotal.size - 1);
			}
		}
		if ((*QuickModeGame_getSObject()).igb.num2 == 2)
		{
			if ((*Enemigo_getSObject()).KamiTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).KamiTotal),
						(*Enemigo_getSObject()).KamiTotal.size - 1);
			}
		}
	}
	if ((*QuickModeGame_getSObject()).igb.num == 2)
	{
		(*QuickModeGame_getSObject()).igb.num2 = GameFramework_Rand(1, 3);
		if ((*QuickModeGame_getSObject()).igb.num2 == 1)
		{
			if ((*Enemigo_getSObject()).SidTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem,
						(*QuickModeGame_getSObject()).igb.velocidadEnemY,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).SidTotal),
						(*Enemigo_getSObject()).SidTotal.size - 1);
			}
		}
		if ((*QuickModeGame_getSObject()).igb.num2 == 2)
		{
			if ((*Enemigo_getSObject()).SidTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem,
						(*QuickModeGame_getSObject()).igb.velocidadEnemY,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).SidTotal),
						(*Enemigo_getSObject()).SidTotal.size - 1);
			}
		}
	}
	if ((*QuickModeGame_getSObject()).igb.num == 3)
	{
		(*QuickModeGame_getSObject()).igb.num2 = GameFramework_Rand(1, 3);
		if ((*QuickModeGame_getSObject()).igb.num2 == 1)
		{
			if ((*Enemigo_getSObject()).HiKamiTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).HiKamiTotal),
								(*Enemigo_getSObject()).HiKamiTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).HiKamiTotal),
								(*Enemigo_getSObject()).HiKamiTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).HiKamiTotal),
						(*Enemigo_getSObject()).HiKamiTotal.size - 1);
			}
		}
		if ((*QuickModeGame_getSObject()).igb.num2 == 2)
		{
			if ((*Enemigo_getSObject()).HiKamiTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).HiKamiTotal),
								(*Enemigo_getSObject()).HiKamiTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).HiKamiTotal),
								(*Enemigo_getSObject()).HiKamiTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).HiKamiTotal),
						(*Enemigo_getSObject()).HiKamiTotal.size - 1);
			}
		}
	}
	if ((*QuickModeGame_getSObject()).igb.num == 4)
	{
		(*QuickModeGame_getSObject()).igb.num2 = GameFramework_Rand(1, 3);
		if ((*QuickModeGame_getSObject()).igb.num2 == 1)
		{
			if ((*Enemigo_getSObject()).HiSidTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).HiSidTotal),
								(*Enemigo_getSObject()).HiSidTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem,
						(*QuickModeGame_getSObject()).igb.velocidadEnemY,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).HiSidTotal),
								(*Enemigo_getSObject()).HiSidTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).HiSidTotal),
						(*Enemigo_getSObject()).HiSidTotal.size - 1);
			}
		}
		if ((*QuickModeGame_getSObject()).igb.num2 == 2)
		{
			if ((*Enemigo_getSObject()).HiSidTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).HiSidTotal),
								(*Enemigo_getSObject()).HiSidTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem,
						(*QuickModeGame_getSObject()).igb.velocidadEnemY,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).HiSidTotal),
								(*Enemigo_getSObject()).HiSidTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).HiSidTotal),
						(*Enemigo_getSObject()).HiSidTotal.size - 1);
			}
		}
	}

}

Mision12* Mision12_getSObject()
{
	return &Mision12S;
}
