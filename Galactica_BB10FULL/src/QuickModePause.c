/*
 * QuickModePause.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "QuickModePause.h"
#include "Lista.h"
#include "Boton.h"
#include "Nave.h"
#include "Dibujar.h"
#include "Textura.h"
#include "GameComun.h"

static QuickModePause QuickModePauseS;

void QuickModePause_load()
{
	QuickModePause_setTexturas();
	QuickModePause_loadTitulo();
	Lista_Inicia(&QuickModePauseS.botones);
	Lista_AddInFin(&QuickModePauseS.botones, Boton_Crear(BResumeGame, 120, 620));
	Lista_AddInFin(&QuickModePauseS.botones, Boton_Crear(BExitGame, 120, 440));
}

void QuickModePause_update()
{
	GameComun_updateEstrellas();
	for (QuickModePauseS.i = 0;
			QuickModePauseS.i < QuickModePauseS.botones.size;
			QuickModePauseS.i++)
	{
		Boton_update(
				(Boton*) Lista_GetIn(&QuickModePauseS.botones,
						QuickModePauseS.i));
	}
	(*Boton_getSObject()).ultimaActualizacion = GameFramework_getTotalTime();
	(*Nave_getSObject()).ultimaActualizacion = GameFramework_getTotalTime();
}

void QuickModePause_draw()
{
	//CLEAR NEGRO
	Dibujar_drawSprite(Textura_getTexturaFondo(2), GameFramework_Vector2(0, 0),
			GameFramework_Color(1, 1, 1, 1), 0, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(2.5, 2));

	GameComun_drawEstrellas();
	QuickModePause_drawTitulo();
	for (QuickModePauseS.i = 0;
			QuickModePauseS.i < QuickModePauseS.botones.size;
			QuickModePauseS.i++)
	{
		Boton_draw(
				(Boton*) Lista_GetIn(&QuickModePauseS.botones,
						QuickModePauseS.i));
	}
}

//*****************************************

//******TITULO***********
void QuickModePause_loadTitulo()
{
	QuickModePauseS.positionTitulo = GameFramework_Vector2(300, 880);
	QuickModePauseS.origenTitulo = GameFramework_Vector2(
			(*(TexturaObjeto*) Lista_GetIn(&QuickModePauseS.textura, 0)).ancho
					/ 2,
			(*(TexturaObjeto*) Lista_GetIn(&QuickModePauseS.textura, 0)).alto
					/ 2);
}

void QuickModePause_drawTitulo()
{
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_GetIn(&QuickModePauseS.textura, 0),
			QuickModePauseS.positionTitulo, GameFramework_Color(1, 1, 1, 1), 0,
			QuickModePauseS.origenTitulo, GameFramework_Vector2(1.0, 1.0));
}

//*********Texturas**************
void QuickModePause_setTexturas()
{
	Lista_Inicia(&QuickModePauseS.textura);
	Lista_AddInFin(&QuickModePauseS.textura, Textura_getTextura("TituloPause")); //0
}

QuickModePause* QuickModePause_getSObject()
{
	return &QuickModePauseS;
}

