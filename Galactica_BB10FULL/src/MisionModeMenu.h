/*
 * MisionModeMenu.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISIONMODEMENU_H_
#define MISIONMODEMENU_H_

#include "GalacticaTipos.h"

void MisionModeMenu_load();
void MisionModeMenu_cargarMisiones();
void MisionModeMenu_update();
void MisionModeMenu_draw();
void MisionModeMenu_loadTitulo();
void MisionModeMenu_drawTitulo();
void MisionModeMenu_verificaBotonMision();
void MisionModeMenu_setTexturas();
MisionModeMenu* MisionModeMenu_getSObject();

#endif /* MISIONMODEMENU_H_ */
