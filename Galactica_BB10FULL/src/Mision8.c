/*
 * Mision8.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "Mision8.h"
#include "QuickModeGame.h"
#include "Animation.h"
#include "Jefe.h"
#include "BarraVida.h"
#include "Boton.h"
#include "Nave.h"
#include "Lista.h"
#include "Control.h"
#include "Textura.h"
#include "Dibujar.h"
#include "Audio.h"
#include "Mision1.h"
#include "Enemigo.h"
#include "MisionMode.h"
#include "CollitionJefe.h"
#include "Almacenamiento.h"
#include "MisionModeGOver.h"
#include "Plus.h"
#include "Cadena.h"

static Mision8 Mision8S;

void Mision8_reiniciar()
{
	QuickModeGame_eliminaAnimaciones();
	Mision8S.positionTime.X = 340;
	Mision8S.positionTime.Y = 50;
	Mision8S.positionEnemigoS.X = 6;
	Mision8S.positionEnemigoS.Y = 50;
	Mision8S.positionToBeat.X = 6;
	Mision8S.positionToBeat.Y = 50;
	Mision8S.positionAyuda1.X = 90;
	Mision8S.positionAyuda1.Y = 150;
	Mision8S.positionAyuda2.X = 50;
	Mision8S.positionAyuda2.Y = 150;
	Lista_Destruir(&(Mision8S.elEnemigo));
	for (Mision8S.i = 0; Mision8S.i < 3; Mision8S.i++)
	{
		Lista_InsertaEnFinal(&(Mision8S.elEnemigo), Mision8S.elEnemigo.fin,
				Enemigo_Crear(ENor, 300, 400, 0, 0));
	}
	Lista_Destruir(&((*QuickModeGame_getSObject()).igb.botones));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BPause, 5, 950));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BAudio, 530, 950));
	(*QuickModeGame_getSObject()).igb.velocidadEnem = 4;
	(*QuickModeGame_getSObject()).igb.velocidadEnemY = 4;
	(*QuickModeGame_getSObject()).igb.velocidadHiEnem = 6;
	(*QuickModeGame_getSObject()).igb.velocidadHiEnemY = 6;
	(*QuickModeGame_getSObject()).igb.maxEnemigos = 5;
	(*Mision8S.estrella).muestra = 1;
	Mision8S.periodoMuestra = 1;
	Mision8S.periodoEscoger = 0;
	Mision8S.enemSeleccionado = 20;
	Mision8S.aciertos = 0;
	for (Mision8S.i = 0; Mision8S.i < Mision8S.elEnemigo.size; Mision8S.i++)
	{
		(*(Enemigo*) Lista_RetornaElemento(&(Mision8S.elEnemigo), Mision8S.i)).movimientoLoco =
				1;
		Enemigo_setPosition(
				(Enemigo*) Lista_RetornaElemento(&(Mision8S.elEnemigo),
						Mision8S.i),
				GameFramework_Vector2(GameFramework_Rand(40, 400),
						GameFramework_Rand(300, 400)));
		(*(Enemigo*) Lista_RetornaElemento(&(Mision8S.elEnemigo), Mision8S.i)).maxVida =
				200;
		(*(Enemigo*) Lista_RetornaElemento(&(Mision8S.elEnemigo), Mision8S.i)).vida =
				200;
		(*(Enemigo*) Lista_RetornaElemento(&(Mision8S.elEnemigo), Mision8S.i)).velMinLoco =
				4;
		(*(Enemigo*) Lista_RetornaElemento(&(Mision8S.elEnemigo), Mision8S.i)).velMaxLoco =
				8;
		(*(Enemigo*) Lista_RetornaElemento(&(Mision8S.elEnemigo), Mision8S.i)).locoAbajo =
				450;
		(*(Enemigo*) Lista_RetornaElemento(&(Mision8S.elEnemigo), Mision8S.i)).locoArriba =
				620;
		(*(Enemigo*) Lista_RetornaElemento(&(Mision8S.elEnemigo), Mision8S.i)).locoDerecha =
				580;
		(*(Enemigo*) Lista_RetornaElemento(&(Mision8S.elEnemigo), Mision8S.i)).locoIzquierda =
				20;
		(*(Enemigo*) Lista_RetornaElemento(&(Mision8S.elEnemigo), Mision8S.i)).movAposition =
				1;
	}
	(*(Enemigo*) Lista_RetornaElemento(&(Mision8S.elEnemigo), 0)).positionAMoverse =
			GameFramework_Vector2(130, 450);
	(*(Enemigo*) Lista_RetornaElemento(&(Mision8S.elEnemigo), 1)).positionAMoverse =
			GameFramework_Vector2(290, 450);
	(*(Enemigo*) Lista_RetornaElemento(&(Mision8S.elEnemigo), 2)).positionAMoverse =
			GameFramework_Vector2(450, 450);
	(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo = 0;
	(*QuickModeGame_getSObject()).igb.ultimoAumento = 0;
	(*QuickModeGame_getSObject()).igb.EscalaMensaje =
			(*QuickModeGame_getSObject()).igb.escalaInicialMensaje = 1.5f;
	(*QuickModeGame_getSObject()).igb.actualFrameMensaje = 0;
	(*QuickModeGame_getSObject()).igb.tiempoTitileoMensaje = 0;
	(*QuickModeGame_getSObject()).igb.mensajeInicialActivo = 1;
	Nave_puntoInicio(GameFramework_Vector2(300, 200));
}

void Mision8_load()
{
	Mision8S.tituloMision = "Mission 8";

	Lista_Inicia(&Mision8S.mensajeMision);
	Lista_AddInFin(&Mision8S.mensajeMision,
			String_creaString("Look attentively which"));
	Lista_AddInFin(&Mision8S.mensajeMision,
			String_creaString("of the enemies hide"));
	Lista_AddInFin(&Mision8S.mensajeMision,
			String_creaString("the star (plus) and"));
	Lista_AddInFin(&Mision8S.mensajeMision,
			String_creaString("destroy him, dont fail"));
	Lista_AddInFin(&Mision8S.mensajeMision,
			String_creaString("more than three times"));
	Lista_AddInFin(&Mision8S.mensajeMision,
			String_creaString("or you will lose."));
	Lista_AddInFin(&Mision8S.mensajeMision,
			String_creaString("Tip:Keep your eye on"));
	Lista_AddInFin(&Mision8S.mensajeMision, String_creaString("the target."));

	Lista_Inicia(&Mision8S.recompensa);
	Lista_AddInFin(&Mision8S.recompensa,
			String_creaString("Increases the shooting"));
	Lista_AddInFin(&Mision8S.recompensa,
			String_creaString("power,in Quick Game."));

	Lista_Inicia(&Mision8S.noRecompensa);
	Lista_AddInFin(&Mision8S.noRecompensa, String_creaString("No Reward.."));

	Lista_Inicia(&Mision8S.elEnemigo);
	Mision8S.estrella = Plus_Crear(GameFramework_Vector2(0, 0));
	Mision8S.escogido = 0, Mision8S.enemSeleccionado = 20, Mision8S.aciertos = 0;
	Mision8S.tiempoEsperaMuestra = 0, Mision8S.tiempoMovLoco = 0;
}

void Mision8_update()
{

	Nave_update();
	QuickModeGame_updateEstrellas();
	Mision8_updateGame();
	QuickModeGame_updateAnimaciones();
	(*Nave_getSObject()).movEnY = 0;
	for (Mision8S.i = 0; Mision8S.i < Mision8S.elEnemigo.size; Mision8S.i++)
	{
		Enemigo_update((Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, Mision8S.i));
	}

	for (Mision8S.i = 0; Mision8S.i < Mision8S.elEnemigo.size; Mision8S.i++)
	{
		if ((*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, Mision8S.i)).estaEnColisionDisp
				&& Mision8S.periodoEscoger == 0)
		{
			(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, Mision8S.i)).estaEnColisionDisp =
					0;
			if ((*Animation_getSObject()).animacionesDispAEnemigoTotal.size > 0)
			{
				Animation_setPosition(
						(Animation*) Lista_GetIn(
								&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
								(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
										- 1),
						(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo,
								Mision8S.i)).Position);

				Lista_AddInFin(&(*QuickModeGame_getSObject()).igb.animacionesQ,
						Lista_GetIn(
								&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
								(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
										- 1));

				Lista_RemoveAt(
						&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
						(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
								- 1);
			}
		}
	}

	for (Mision8S.i = 0;
			Mision8S.i < (*QuickModeGame_getSObject()).igb.botones.size;
			Mision8S.i++)
	{
		Boton_update(
				(Boton*) Lista_GetIn(&(*QuickModeGame_getSObject()).igb.botones,
						Mision8S.i));
	}
	QuickModeGame_updateMensajeInicial();
	BarraVida_update((*QuickModeGame_getSObject()).igb.barraVidaNave,
			(*Nave_getSObject()).Vida, (*Nave_getSObject()).MaxVida);
	if ((*Nave_getSObject()).Vida <= 0)
	{
		Animation_update((*Animation_getSObject()).animacionesEstallidoNave);
	}
	Mision8_gameOver();
	if ((*Control_getSObject()).controlActivo == 1)
	{
		Control_update((*QuickModeGame_getSObject()).igb.control);
	}

}

void Mision8_draw()
{
	//CLEAR NEGRO
	Dibujar_drawSprite(Textura_getTexturaFondo(0), GameFramework_Vector2(0, 0),
			GameFramework_Color(0, 0, 0, 1), 0, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(40, 40));

	Nave_draw();
	QuickModeGame_drawEstrellas();
	QuickModeGame_drawEnemigos();
	Mision8_drawEstadNave();
	QuickModeGame_drawAnimaciones();
	Mision8_drawMensajeInicial();

	for (Mision8S.i = 0; Mision8S.i < Mision8S.elEnemigo.size; Mision8S.i++)
	{
		Enemigo_draw((Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, Mision8S.i));
	}

	if ((*Nave_getSObject()).Vida <= 0)
	{
		Animation_draw((*Animation_getSObject()).animacionesEstallidoNave);
	}

	for (Mision8S.i = 0;
			Mision8S.i < (*QuickModeGame_getSObject()).igb.botones.size;
			Mision8S.i++)
	{
		Boton_draw(
				(Boton*) Lista_GetIn(&(*QuickModeGame_getSObject()).igb.botones,
						Mision8S.i));
	}

	if (GameFramework_Vector2IgualA(
			(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 0)).positionAMoverse,
			(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 0)).Position) == 1
			&& GameFramework_Vector2IgualA(
					(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 1)).positionAMoverse,
					(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 1)).Position)
					== 1
			&& GameFramework_Vector2IgualA(
					(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 2)).positionAMoverse,
					(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 2)).Position)
					== 1 && Mision8S.periodoMuestra == 1)
	{
		Plus_draw(Mision8S.estrella);
	}
	if ((*Control_getSObject()).controlActivo == 1)
	{
		Control_draw((*QuickModeGame_getSObject()).igb.control);
	}
	// drawEstadisticas(sb);
}

//******************Estadisticas de la nave************
void Mision8_drawEstadNave()
{
	//** Vida de la nave
	Dibujar_drawSprite(Textura_getTexturaFondo(1),
			GameFramework_Vector2(0, 945), GameFramework_Color(0, 0, 0, 1), 0,
			GameFramework_Vector2(0, 0), GameFramework_Vector2(50, 2.6));

	BarraVida_draw((*QuickModeGame_getSObject()).igb.barraVidaNave);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_EnteroACadena((*Nave_getSObject()).Vida),
			GameFramework_Vector2(100, 975), GameFramework_Color(1, 1, 1, 1),
			0.8);

	Dibujar_drawText((*Textura_getSObject()).Fuente, "/",
			GameFramework_Vector2(285, 965), GameFramework_Color(1, 1, 1, 1),
			1.4);

	Nave_drawAnimacionesPoder();

	//****Puntos a vencer
	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(3, "Successes:",
					GameFramework_EnteroACadena(Mision8S.aciertos), "/3"),
			GameFramework_Vector2(6, 900), GameFramework_Color(1, 1, 1, 1),
			0.9);

	if (Mision8S.periodoEscoger == 0)
	{
		Dibujar_drawText((*Textura_getSObject()).Fuente, "Pay attention!",
				GameFramework_Vector2(60, 780),
				GameFramework_Color(GameFramework_getByteColorToFloatColor(244),
						GameFramework_getByteColorToFloatColor(232),
						GameFramework_getByteColorToFloatColor(16), 1), 1.2);
	}
	if (Mision8S.periodoEscoger == 1
			&& (GameFramework_Vector2IgualA(
					(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 0)).positionAMoverse,
					(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 0)).Position)
					== 1
					&& GameFramework_Vector2IgualA(
							(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 1)).positionAMoverse,
							(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 1)).Position)
							== 1
					&& GameFramework_Vector2IgualA(
							(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 2)).positionAMoverse,
							(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 2)).Position)
							== 1))
	{
		Dibujar_drawText((*Textura_getSObject()).Fuente, "Shoot! to enemy",
				GameFramework_Vector2(60, 800),
				GameFramework_Color(GameFramework_getByteColorToFloatColor(244),
						GameFramework_getByteColorToFloatColor(232),
						GameFramework_getByteColorToFloatColor(16), 1), 1.2);

		Dibujar_drawText((*Textura_getSObject()).Fuente, "with the star",
				GameFramework_Vector2(60, 770),
				GameFramework_Color(GameFramework_getByteColorToFloatColor(244),
						GameFramework_getByteColorToFloatColor(232),
						GameFramework_getByteColorToFloatColor(16), 1), 1.2);
	}

}

//Update Game
void Mision8_updateGame()
{

	if (GameFramework_getTotalTime() > Mision8S.tiempoMovLoco + 10000000
			&& Mision8S.periodoMuestra == 0 && Mision8S.periodoEscoger == 0)
	{
		Mision8S.periodoEscoger = 1;
		for (Mision8S.i = 0; Mision8S.i < Mision8S.elEnemigo.size; Mision8S.i++)
		{
			(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, Mision8S.i)).movAposition =
					1;
		}
	}

	if (Mision8S.periodoEscoger == 0 && Mision8S.periodoMuestra == 0)
	{
		(*Nave_getSObject()).disparosActivos = 0;
	}

	if (Mision8S.periodoMuestra)
	{
		(*Nave_getSObject()).disparosActivos = 0;
		if ((*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 0)).movAposition == 1
				&& (*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 0)).movAposition
						== 1
				&& (*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 1)).movAposition
						== 1
				&& (*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 2)).movAposition
						== 1)
		{
			if (GameFramework_Vector2IgualA(
					(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 0)).positionAMoverse,
					(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 0)).Position)
					== 1
					&& GameFramework_Vector2IgualA(
							(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 1)).positionAMoverse,
							(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 1)).Position)
							== 1
					&& GameFramework_Vector2IgualA(
							(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 2)).positionAMoverse,
							(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 2)).Position)
							== 1)
			{

				if (GameFramework_getTotalTime()
						> Mision8S.tiempoEsperaMuestra + 3000000)
				{
					Mision8S.periodoMuestra = 0;
					(*QuickModeGame_getSObject()).igb.num2 = GameFramework_Rand(
							1, 4);
					switch ((*QuickModeGame_getSObject()).igb.num2)
					{
					case 1:
						(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 0)).positionAMoverse =
								GameFramework_Vector2(130, 450);
						(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 1)).positionAMoverse =
								GameFramework_Vector2(290, 450);
						(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 2)).positionAMoverse =
								GameFramework_Vector2(450, 450);
						break;
					case 2:
						(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 0)).positionAMoverse =
								GameFramework_Vector2(290, 450);
						(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 1)).positionAMoverse =
								GameFramework_Vector2(130, 450);
						(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 2)).positionAMoverse =
								GameFramework_Vector2(450, 450);
						break;
					case 3:
						(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 0)).positionAMoverse =
								GameFramework_Vector2(130, 450);
						(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 1)).positionAMoverse =
								GameFramework_Vector2(450, 450);
						(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 2)).positionAMoverse =
								GameFramework_Vector2(290, 450);
						break;
					case 4:
						(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 0)).positionAMoverse =
								GameFramework_Vector2(450, 450);
						(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 1)).positionAMoverse =
								GameFramework_Vector2(290, 450);
						(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 2)).positionAMoverse =
								GameFramework_Vector2(130, 450);
						break;
					}
					for (Mision8S.i = 0; Mision8S.i < Mision8S.elEnemigo.size;
							Mision8S.i++)
					{
						(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo,
								Mision8S.i)).movAposition = 0;
					}
				}
				Mision8S.tiempoMovLoco = GameFramework_getTotalTime();
				switch ((*QuickModeGame_getSObject()).igb.num)
				{
				case 1:
					(*Mision8S.estrella).Position.X = (*(Enemigo*) Lista_GetIn(
							&Mision8S.elEnemigo, 0)).positionAMoverse.X;
					(*Mision8S.estrella).Position.Y = (*(Enemigo*) Lista_GetIn(
							&Mision8S.elEnemigo, 0)).positionAMoverse.Y;
					Mision8S.escogido = 0;
					break;
				case 2:
					(*Mision8S.estrella).Position.X = (*(Enemigo*) Lista_GetIn(
							&Mision8S.elEnemigo, 1)).positionAMoverse.X;
					(*Mision8S.estrella).Position.Y = (*(Enemigo*) Lista_GetIn(
							&Mision8S.elEnemigo, 1)).positionAMoverse.Y;
					Mision8S.escogido = 1;
					break;
				case 3:
					(*Mision8S.estrella).Position.X = (*(Enemigo*) Lista_GetIn(
							&Mision8S.elEnemigo, 2)).positionAMoverse.X;
					(*Mision8S.estrella).Position.Y = (*(Enemigo*) Lista_GetIn(
							&Mision8S.elEnemigo, 2)).positionAMoverse.Y;
					Mision8S.escogido = 2;
					break;
				}
			}
			else
			{
				Mision8S.tiempoEsperaMuestra = GameFramework_getTotalTime();
				(*QuickModeGame_getSObject()).igb.num = GameFramework_Rand(1,
						3);
			}
		}
	}

	if (Mision8S.periodoEscoger)
	{
		if ((*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 0)).movAposition
				&& (*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 1)).movAposition
				&& (*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 2)).movAposition)
		{
			if (GameFramework_Vector2IgualA(
					(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 0)).positionAMoverse,
					(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 0)).Position)
					== 1
					&& GameFramework_Vector2IgualA(
							(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 1)).positionAMoverse,
							(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 1)).Position)
							== 1
					&& GameFramework_Vector2IgualA(
							(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 2)).positionAMoverse,
							(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo, 2)).Position)
							== 1)
			{
				(*Nave_getSObject()).disparosActivos = 1;
				for (Mision8S.i = 0; Mision8S.i < Mision8S.elEnemigo.size;
						Mision8S.i++)
				{
					if ((*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo,
							Mision8S.i)).estaEnColisionDisp == 1)
					{
						(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo,
								Mision8S.i)).estaEnColisionDisp = 0;
						Mision8S.enemSeleccionado = Mision8S.i;
						Mision8S.periodoMuestra = 1;
						Mision8S.periodoEscoger = 0;
						Mision8S.tiempoEsperaMuestra =
								GameFramework_getTotalTime();
						if (Mision8S.enemSeleccionado == Mision8S.escogido)
						{
							Mision8S.aciertos = Mision8S.aciertos + 1;
							Audio_playAudio("Estrella");
						}
						else
						{
							Audio_playAudio("Error");
							(*Nave_getSObject()).Vida =
									(*Nave_getSObject()).Vida - 35;
						}
						for (Mision8S.j = 0;
								Mision8S.j < Mision8S.elEnemigo.size;
								Mision8S.j++)
						{
							if ((*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo,
									Mision8S.j)).estaEnColisionDisp == 1)
							{
								(*(Enemigo*) Lista_GetIn(&Mision8S.elEnemigo,
										Mision8S.j)).estaEnColisionDisp = 0;
							}
						}
						break;
					}
				}

			}
			else
			{
				Mision8S.tiempoEsperaMuestra = GameFramework_getTotalTime();
				(*QuickModeGame_getSObject()).igb.num = GameFramework_Rand(1,
						3);
				(*Nave_getSObject()).disparosActivos = 0;
			}
		}
	}

}

void Mision8_gameOver()
{

	if (Mision8S.aciertos == 3)
	{
		if ((*Audio_getSObject()).musicaActiva == 1)
		{
			Audio_stopAudio("Galactic");
			Audio_playAudio("GameOver");
		}
		(*MisionModeGOver_getSObject()).gana = 1;

		Almacenamiento_cargarMisiones();
		if (Almacenamiento_getPremiosDados() == 7)
		{
			Almacenamiento_salvarBonus(10, ALpoderDisp);
			(*MisionModeGOver_getSObject()).cosasGanadas = &Mision8S.recompensa;
			Almacenamiento_salvarMisiones();
		}
		else
		{
			(*MisionModeGOver_getSObject()).cosasGanadas =
					&(*Mision1_getSObject()).yaRecompensa;
		}

		(*MisionMode_getSObject()).estadoActual = MMGameOver;
	}

	if ((*Nave_getSObject()).Vida <= 0)
	{
		(*Nave_getSObject()).Vida = 0;
		(*Nave_getSObject()).EscalaN = GameFramework_Vector2(0, 0);
		Animation_setPosition(
				(*Animation_getSObject()).animacionesEstallidoNave,
				(*Nave_getSObject()).PositionNav);
		(*MisionModeGOver_getSObject()).gana = 0;
		(*MisionModeGOver_getSObject()).cosasGanadas = &Mision8S.noRecompensa;
		if ((*(*Animation_getSObject()).animacionesEstallidoNave).animacionFinalizada
				== 1)
		{
			if ((*Audio_getSObject()).musicaActiva == 1)
			{
				Audio_stopAudio("Galactic");
				Audio_playAudio("GameOver");
			}
			(*MisionMode_getSObject()).estadoActual = MMGameOver;
		}
	}

}

//********Dibujo Mensaje Inicial
void Mision8_drawMensajeInicial()
{
	if ((*QuickModeGame_getSObject()).igb.mensajeInicialActivo == 1)
	{
		Dibujar_drawText((*Textura_getSObject()).Fuente,
				GameFramework_Concatenar(2, Mision8S.tituloMision, " Start!!"),
				GameFramework_Vector2(50, 620),
				GameFramework_Color(GameFramework_getByteColorToFloatColor(7),
						GameFramework_getByteColorToFloatColor(237),
						GameFramework_getByteColorToFloatColor(134), 1),
				(*QuickModeGame_getSObject()).igb.EscalaMensaje);
	}
}

Mision8* Mision8_getSObject()
{
	return &Mision8S;
}
