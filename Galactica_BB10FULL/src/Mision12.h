/*
 * Mision12.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION12_H_
#define MISION12_H_

#include "GalacticaTipos.h"

void Mision12_reiniciar();
void Mision12_load();
void Mision12_update();
void Mision12_draw();
void Mision12_drawEstadNave();
void Mision12_updateGame();
void Mision12_gameOver();
void Mision12_drawMensajeInicial();
void Mision12_insertaEnemigo();
Mision12* Mision12_getSObject();

#endif /* MISION12_H_ */
