/*
 * Dibujar.h
 *
 *  Created on: 6/03/2013
 *      Author: Jarts
 */

#ifndef DIBUJAR_H_
#define DIBUJAR_H_

#include "GalacticaTipos.h"

void Dibujar_drawSpriteTr(TexturaObjeto *tex, Vector2 pos, Rectangle trozoTex,
		Color color, float rota, Vector2 ori, Vector2 esc);
void Dibujar_drawSprite(TexturaObjeto *tex, Vector2 pos, Color color,
		float rota, Vector2 ori, Vector2 esc);
void Dibujar_drawText(font_t* font, const char* msg, Vector2 position,
		Color color, float Escala);
float Dibujar_dePixelAFloat(float MaxPixeles, float pixeles);
void Dibujar_drawErrorText(char* errort, Vector2 pos);
double Dibujar_transformaPosY(double posY);
double Dibujar_transformaPosX(double posX);
double Dibujar_transformaEscalaY(double escalaY);
double Dibujar_transformaEscalaX(double escalaX);

#endif /* DIBUJAR_H_ */
