/*
 * GalacticaMain.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "GalacticaMain.h"
#include "GameFramework.h"
#include "Textura.h"
#include "String.h"
#include "bbutil.h"
#include "Dibujar.h"
#include "AcercaDe.h"
#include "Cadena.h"
#include "Poderes.h"
#include "Animation.h"
#include "BarraVida.h"
#include "Boton.h"
#include "Onda.h"
#include "Disparo.h"
#include "DisparoEnemigo.h"
#include "DisparoJefe.h"
#include "Enemigo.h"
#include "Plus.h"
#include "Estrella.h"
#include "GameComun.h"
#include "MainMenu.h"
#include "Nave.h"
#include "Jefe.h"
#include "QuickMode.h"
#include "Acelerometro.h"
#include "Almacenamiento.h"
#include "Audio.h"
#include "Ayuda.h"
#include "Buy.h"
#include "Control.h"
#include "GalacticaTipos.h"
#include "InGameBase.h"
#include "Lista.h"
#include "Mision1.h"
#include "Mision10.h"
#include "Mision11.h"
#include "Mision12.h"
#include "Mision13.h"
#include "Mision14.h"
#include "Mision15.h"
#include "Mision16.h"
#include "Mision17.h"
#include "Mision18.h"
#include "Mision19.h"
#include "Mision2.h"
#include "Mision20.h"
#include "Mision3.h"
#include "Mision4.h"
#include "Mision5.h"
#include "Mision6.h"
#include "Mision7.h"
#include "Mision8.h"
#include "Mision9.h"
#include "MisionMode.h"
#include "MisionModeGame.h"
#include "MisionModeGOver.h"
#include "MisionModeInfo.h"
#include "MisionModeMenu.h"
#include "MisionModePause.h"
#include "NMini.h"
#include "Opciones.h"
#include "QuickModeGOver.h"
#include "QuickModeGame.h"
#include "QuickModeMenu.h"
#include "QuickModePause.h"
#include "SavedGame.h"
#include "TouchControl.h"

#include <GLES/gl.h>

static GalacticaMain GalacticaMainS;

void GalacticaMain_Load()
{
	GalacticaMainS.BESTSCORE = 0;
	GalacticaMainS.checkTrialModeDelay = 1000000;
	GalacticaMainS.introTime = 4000000;
	GalacticaMainS.dibujaIntro = 1, GalacticaMainS.empezarCarga = 1;
	GalacticaMainS.enCarga = 0, GalacticaMainS.enJuego = 0;
	GalacticaMainS.isTrialMode = 1; //static
	glClearColor(0.0f, 0.0f, 0.2f, 1.0f);
	Textura_cargaIntro();
}

void GalacticaMain_cargarTodo()
{
	GalacticaMainS.estadoActual = GMMainMenu;
	GalacticaMain_cargarTexturas();
	Dibujar_drawErrorText("Texturas Cargadas", GameFramework_Vector2(10, 200));
	GalacticaMain_cargarAudio();
	Dibujar_drawErrorText("Audios Cargadas", GameFramework_Vector2(10, 200));

	//Lo que debe cargar
	Acelerometro_load();
	Dibujar_drawErrorText("Acelerometro Cargado",
			GameFramework_Vector2(10, 200));
	Poderes_load();
	Dibujar_drawErrorText("Poderes Cargado", GameFramework_Vector2(10, 200));
	Animation_load();
	Dibujar_drawErrorText("Animacion Cargado", GameFramework_Vector2(10, 200));
	BarraVida_load();
	Dibujar_drawErrorText("Barra vida Cargado", GameFramework_Vector2(10, 200));
	Boton_load();
	Dibujar_drawErrorText("Boton Cargado", GameFramework_Vector2(10, 200));
	Onda_load();
	Dibujar_drawErrorText("OndaCargado", GameFramework_Vector2(10, 200));
	Disparo_load();
	Dibujar_drawErrorText("Disparo Cargado", GameFramework_Vector2(10, 200));
	DisparoEnemigo_load();
	Dibujar_drawErrorText("DisparoEnemigo Cargado",
			GameFramework_Vector2(10, 200));
	Enemigo_load();
	Dibujar_drawErrorText("Enemigo Cargado", GameFramework_Vector2(10, 200));
	Plus_load();
	Dibujar_drawErrorText("Plus Cargado", GameFramework_Vector2(10, 200));
	Estrella_load();
	Dibujar_drawErrorText("Estrella Cargado", GameFramework_Vector2(10, 200));
	GameComun_loadEstrellas();
	Dibujar_drawErrorText("GameComun Cargado", GameFramework_Vector2(10, 200));
	MainMenu_load();
	Dibujar_drawErrorText("MainMenu Cargado", GameFramework_Vector2(10, 200));
	QuickMode_load();
	Dibujar_drawErrorText("QuickMode Cargado", GameFramework_Vector2(10, 200));
	Nave_load();
	Dibujar_drawErrorText("Nave Cargado", GameFramework_Vector2(10, 200));
	DisparoJefe_load();
	Dibujar_drawErrorText("DisparoJefe Cargado",
			GameFramework_Vector2(10, 200));
	Jefe_load();
	Dibujar_drawErrorText("Jefe Cargado", GameFramework_Vector2(10, 200));
	QuickModeGame_load();
	Dibujar_drawErrorText("QuickModeGame Cargado",
			GameFramework_Vector2(10, 200));
	QuickModePause_load();
	Dibujar_drawErrorText("QuickModePause Cargado",
			GameFramework_Vector2(10, 200));
	NMini_load();
	Dibujar_drawErrorText("NMini Cargado", GameFramework_Vector2(10, 200));
	QuickModeMenu_load();
	Dibujar_drawErrorText("QuickModeMenu Cargado",
			GameFramework_Vector2(10, 200));
	QuickModeGOver_load();
	Dibujar_drawErrorText("QuickModeGOver Cargado",
			GameFramework_Vector2(10, 200));
	MisionModeGame_load();
	Dibujar_drawErrorText("QuickModeGame Cargado",
			GameFramework_Vector2(10, 200));
	MisionModeMenu_load();
	Dibujar_drawErrorText("MisionModeMenu Cargado",
			GameFramework_Vector2(10, 200));
	MisionModeInfo_load();
	Dibujar_drawErrorText("MisionModeInfo Cargado",
			GameFramework_Vector2(10, 200));
	MisionModePause_load();
	Dibujar_drawErrorText("MisionModePause Cargado",
			GameFramework_Vector2(10, 200));
	MisionModeGOver_load();
	Dibujar_drawErrorText("MisionModeGOver Cargado",
			GameFramework_Vector2(10, 200));
	Control_load();
	Dibujar_drawErrorText("Control Cargado", GameFramework_Vector2(10, 200));
	Opciones_load();
	Dibujar_drawErrorText("Opciones Cargado", GameFramework_Vector2(10, 200));
	Ayuda_load();
	Dibujar_drawErrorText("Ayuda Cargado", GameFramework_Vector2(10, 200));
	AcercaDe_load();
	Dibujar_drawErrorText("Acercade Cargado", GameFramework_Vector2(10, 200));
	SavedGame_load();
	Dibujar_drawErrorText("Saved game Cargado", GameFramework_Vector2(10, 200));
	Buy_load();
	Dibujar_drawErrorText("Buy Cargado", GameFramework_Vector2(10, 200));
	MisionMode_load();
	Dibujar_drawErrorText("MisionMode Cargado", GameFramework_Vector2(10, 200));

	//Almacenamiento.borrar();
	Almacenamiento_crearDatos();
	Dibujar_drawErrorText("Crear datos Cargado",
			GameFramework_Vector2(10, 200));
	Almacenamiento_cargarOpciones();
	Dibujar_drawErrorText("Cargar opciones Cargado",
			GameFramework_Vector2(10, 200));
	Almacenamiento_cargarScore();
	Dibujar_drawErrorText("Cargar score Cargado",
			GameFramework_Vector2(10, 200));
}

// *********** UPDATE ************
void GalacticaMain_Update()
{
	if (GalacticaMainS.introTime >= 0)
	{
		GalacticaMainS.introTime = GalacticaMainS.introTime
				- GameFramework_getSwapTime();
		GalacticaMainS.dibujaIntro = 1;
		if (GalacticaMainS.introTime <= 0)
		{
			GalacticaMainS.dibujaIntro = 0;
		}
	}
	if (GalacticaMainS.enCarga == 1)
	{
		GalacticaMain_cargarTodo();
		GalacticaMainS.enJuego = 1;
		GalacticaMainS.enCarga = 0;
	}
	if (GalacticaMainS.enJuego == 1)
	{

		GalacticaMainS.contUpdates++;
		Acelerometro_update();

		switch (GalacticaMainS.estadoActual)
		{

		case GMMainMenu:
			MainMenu_update();
			break;
		case GMQuickGame:
			QuickMode_update();
			break;
		case GMMisionMode:
			MisionMode_update();
			break;
		case GMOptions:
			Opciones_update();
			break;
		case GMHelp:
			Ayuda_update();
			break;
		case GMAbout:
			AcercaDe_update();
			break;
		case GMSavedGame:
			SavedGame_update();
			break;
		case GMBuy:
			Buy_update();
			break;

		}

		//Trial mode comprobacion
		if (GalacticaMainS.isTrialMode
				&& GalacticaMainS.checkTrialModeDelay >= 0)
		{
			GalacticaMainS.checkTrialModeDelay -= GameFramework_getSwapTime();
			if (GalacticaMainS.checkTrialModeDelay <= 0)
			{
				GalacticaMainS.isTrialMode = 1;
			}
		}

		//Fotogramas por segundo o FPS
		if (GameFramework_getTotalTime()
				> GalacticaMainS.ultima_actua + 1000000)
		{
			GalacticaMainS.frames = GalacticaMainS.cont_draw;
			GalacticaMainS.updates = GalacticaMainS.contUpdates;
			GalacticaMainS.cont_draw = 0;
			GalacticaMainS.contUpdates = 0;

			GalacticaMainS.ultima_actua = GameFramework_getTotalTime();
		}
	}

}

// ************** DRAW ****************
void GalacticaMain_Draw()
{
	GalacticaMainS.cont_draw++;
	glClear(GL_COLOR_BUFFER_BIT);

	//glMatrixMode(GL_MODELVIEW);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);

	if (GalacticaMainS.dibujaIntro == 1)
	{
		Dibujar_drawSprite((*Textura_getSObject()).intro,
				GameFramework_Vector2(0, 0), GameFramework_Color(1, 1, 1, 1), 0,
				GameFramework_Vector2(0, 0), GameFramework_Vector2(1.17, 1));

		if (GalacticaMainS.empezarCarga == 1)
		{
			GalacticaMainS.enCarga = 1;
			GalacticaMainS.empezarCarga = 0;
		}
	}
	else
	{
		switch (GalacticaMainS.estadoActual)
		{

		case GMMainMenu:
			MainMenu_draw();
			break;
		case GMQuickGame:
			QuickMode_draw();
			break;
		case GMMisionMode:
			MisionMode_draw();
			break;
		case GMOptions:
			Opciones_draw();
			break;
		case GMHelp:
			Ayuda_draw();
			break;
		case GMAbout:
			AcercaDe_draw();
			break;
		case GMSavedGame:
			SavedGame_draw();
			break;
		case GMBuy:
			Buy_draw();
			break;

		}
	}

	/*Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "FPS: ",
					GameFramework_EnteroACadena(GalacticaMainS.frames)),
			GameFramework_Vector2(10, 50), GameFramework_Color(1, 1, 1, 1),
			0.8);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "UPS: ",
					GameFramework_EnteroACadena(GalacticaMainS.updates)),
			GameFramework_Vector2(10, 70), GameFramework_Color(1, 1, 1, 1),
			0.8);*/

	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

}

/*void OnActivated(object sender, EventArgs args)
 {
 isTrialMode = true;
 checkTrialModeDelay = 1000.0f;
 GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
 QuickMode.estadoActual = QuickMode.Estado.Menu;
 }

 void OnExiting(object sender, EventArgs args)
 {
 if (QuickMode.estadoActual == QuickMode.Estado.Game)
 {
 if (GalacticaMain.estadoActual == GalacticaMain.Estado.Buy
 || GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
 {
 Almacenamiento.salvarInGame();
 }
 }
 }

 void OnDeactivated(object sender, EventArgs args)
 {
 if (QuickMode.estadoActual == QuickMode.Estado.Game)
 {
 if (GalacticaMain.estadoActual == GalacticaMain.Estado.Buy
 || GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
 {
 Almacenamiento.salvarInGame();
 }
 }
 }*/

// *** Cargar Audio
void GalacticaMain_cargarAudio()
{
	Audio_Inicia();

	Audio_addAudio("/app/native/assets/disparo.mp3",
			String_creaString("Disparo"), 0);
	Audio_addAudio("/app/native/assets/disparo2.mp3",
			String_creaString("Disparo2"), 0);
	Audio_addAudio("/app/native/assets/disparo3.mp3",
			String_creaString("Disparo3"), 0);
	Audio_addAudio("/app/native/assets/EstallidoEnemigo.mp3",
			String_creaString("EstallidoEnemigo"), 0);
	Audio_addAudio("/app/native/assets/EstallidoJefe.mp3",
			String_creaString("EstallidoJefe"), 0);
	Audio_addAudio("/app/native/assets/inicioPoder.mp3",
			String_creaString("InicioPoder"), 0);
	Audio_addAudio("/app/native/assets/presionarBoton.mp3",
			String_creaString("PresionarBoton"), 0);
	Audio_addAudio("/app/native/assets/Estallido.mp3",
			String_creaString("Estallido"), 0);
	Audio_addAudio("/app/native/assets/Error.mp3", String_creaString("Error"),
			0);
	Audio_addAudio("/app/native/assets/Estrella.mp3",
			String_creaString("Estrella"), 0);
	Audio_addAudio("/app/native/assets/silencio.mp3",
			String_creaString("Silencio"), 0);
	Audio_addAudio("/app/native/assets/Galactic.mp3",
			String_creaString("Galactic"), 1);
	Audio_addAudio("/app/native/assets/GameOver.mp3",
			String_creaString("GameOver"), 0);
	Audio_addAudio("/app/native/assets/mainMenu.mp3",
			String_creaString("MainMenu"), 1);
}

//**********Carga Texturas**************************
void GalacticaMain_cargarTexturas()
{
	Textura_Inicia();
	Textura_addTextura("app/native/assets/NGalactica.png",
			String_creaString("NGalactica"));
	Textura_addTextura("app/native/assets/NCarafe.png",
			String_creaString("NCarafe"));
	Textura_addTextura("app/native/assets/NHelix.png",
			String_creaString("NHelix"));
	Textura_addTextura("app/native/assets/NArp.png", String_creaString("NArp"));
	Textura_addTextura("app/native/assets/NPerseus.png",
			String_creaString("NPerseus"));
	Textura_addTextura("app/native/assets/NPisces.png",
			String_creaString("NPisces"));
	Textura_addTextura("app/native/assets/PoderGalactica.png",
			String_creaString("PoderGalactica"));
	Textura_addTextura("app/native/assets/PoderCarafe.png",
			String_creaString("PoderCarafe"));
	Textura_addTextura("app/native/assets/PoderHelix.png",
			String_creaString("PoderHelix"));
	Textura_addTextura("app/native/assets/PoderArp.png",
			String_creaString("PoderArp"));
	Textura_addTextura("app/native/assets/PoderPerseus.png",
			String_creaString("PoderPerseus"));
	Textura_addTextura("app/native/assets/PoderPisces.png",
			String_creaString("PoderPisces"));
	Textura_addTextura("app/native/assets/ENor.png", String_creaString("ENor"));
	Textura_addTextura("app/native/assets/EKami.png",
			String_creaString("EKami"));
	Textura_addTextura("app/native/assets/ESid.png", String_creaString("ESid"));
	Textura_addTextura("app/native/assets/Disparo.png",
			String_creaString("Disparo"));
	Textura_addTextura("app/native/assets/SuperDisparo.png",
			String_creaString("SuperDisparo"));
	Textura_addTextura("app/native/assets/Titulo.png",
			String_creaString("Titulo"));
	Textura_addTextura("app/native/assets/DisparoSid.png",
			String_creaString("DisparoSid"));
	Textura_addTextura("app/native/assets/DisparoNor.png",
			String_creaString("DisparoNor"));
	Textura_addTextura("app/native/assets/Estrella.png",
			String_creaString("Estrella"));
	Textura_addTextura("app/native/assets/animationDispANave.png",
			String_creaString("animationDispANave"));
	Textura_addTextura("app/native/assets/animationDispAEnemigo.png",
			String_creaString("animationDispAEnemigo"));
	Textura_addTextura("app/native/assets/animationPoder.png",
			String_creaString("animationPoder"));
	Textura_addTextura("app/native/assets/EstallidoEnemigo.png",
			String_creaString("EstallidoEnemigo"));
	Textura_addTextura("app/native/assets/EstallidoJefe.png",
			String_creaString("EstallidoJefe"));
	Textura_addTextura("app/native/assets/EstallidoJefe.png",
			String_creaString("EstallidoJefe"));
	Textura_addTextura("app/native/assets/EstallidoNave.png",
			String_creaString("EstallidoNave"));
	Textura_addTextura("app/native/assets/PauseBoton.png",
			String_creaString("PauseBoton"));
	Textura_addTextura("app/native/assets/TituloPause.png",
			String_creaString("TituloPause"));
	Textura_addTextura("app/native/assets/TituloPauseMision.png",
			String_creaString("TituloPauseMision"));
	Textura_addTextura("app/native/assets/TituloQuickGame.png",
			String_creaString("TituloQuickGame"));
	Textura_addTextura("app/native/assets/TituloQuickGame.png",
			String_creaString("TituloQuickGame"));
	Textura_addTextura("app/native/assets/TituloGameOver.png",
			String_creaString("TituloGameOver"));
	Textura_addTextura("app/native/assets/TituloMisionMode.png",
			String_creaString("TituloMisionMode"));
	Textura_addTextura("app/native/assets/Plus.png", String_creaString("Plus"));
	Textura_addTextura("app/native/assets/BarraVida.png",
			String_creaString("BarraVida"));
	Textura_addTextura("app/native/assets/BarraVidaMarco.png",
			String_creaString("BarraVidaMarco"));
	Textura_addTextura("app/native/assets/BQuickMode.png",
			String_creaString("BQuickMode"));
	Textura_addTextura("app/native/assets/BOpciones.png",
			String_creaString("BOpciones"));
	Textura_addTextura("app/native/assets/BResumeGame.png",
			String_creaString("BResumeGame"));
	Textura_addTextura("app/native/assets/BExitGame.png",
			String_creaString("BExitGame"));
	Textura_addTextura("app/native/assets/BFlecha.png",
			String_creaString("BFlecha"));
	Textura_addTextura("app/native/assets/BPlay.png",
			String_creaString("BPlay"));
	Textura_addTextura("app/native/assets/BAudio.png",
			String_creaString("BAudio"));
	Textura_addTextura("app/native/assets/BMision.png",
			String_creaString("BMision"));
	Textura_addTextura("app/native/assets/BMisionMode.png",
			String_creaString("BMisionMode"));
	Textura_addTextura("app/native/assets/BPower.png",
			String_creaString("BPower"));
	Textura_addTextura("app/native/assets/BContinue.png",
			String_creaString("BContinue"));
	Textura_addTextura("app/native/assets/BBack.png",
			String_creaString("BBack"));
	Textura_addTextura("app/native/assets/BNextMision.png",
			String_creaString("BNextMision"));
	Textura_addTextura("app/native/assets/BRestart.png",
			String_creaString("BRestart"));
	Textura_addTextura("app/native/assets/BSalir.png",
			String_creaString("BSalir"));
	Textura_addTextura("app/native/assets/BHelp.png",
			String_creaString("BHelp"));
	Textura_addTextura("app/native/assets/BAbout.png",
			String_creaString("BAbout"));
	Textura_addTextura("app/native/assets/BTilting.png",
			String_creaString("BTilting"));
	Textura_addTextura("app/native/assets/BButtons.png",
			String_creaString("BButtons"));
	Textura_addTextura("app/native/assets/BMedium.png",
			String_creaString("BMedium"));
	Textura_addTextura("app/native/assets/BLarge.png",
			String_creaString("BLarge"));
	Textura_addTextura("app/native/assets/BContinueGame.png",
			String_creaString("BContinueGame"));
	Textura_addTextura("app/native/assets/BNewGame.png",
			String_creaString("BNewGame"));
	Textura_addTextura("app/native/assets/BFacebook.png",
			String_creaString("BFacebook"));
	Textura_addTextura("app/native/assets/BTwitter.png",
			String_creaString("BTwitter"));
	Textura_addTextura("app/native/assets/BComent.png",
			String_creaString("BComent"));
	Textura_addTextura("app/native/assets/BGoBuy.png",
			String_creaString("BGoBuy"));
	Textura_addTextura("app/native/assets/BNotNow.png",
			String_creaString("BNotNow"));
	Textura_addTextura("app/native/assets/TEscogerNave.png",
			String_creaString("TEscogerNave"));
	Textura_addTextura("app/native/assets/TResultados.png",
			String_creaString("TResultados"));
	Textura_addTextura("app/native/assets/TInfo.png",
			String_creaString("TInfo"));
	Textura_addTextura("app/native/assets/TRecompensa.png",
			String_creaString("TRecompensa"));
	Textura_addTextura("app/native/assets/TituloWin.png",
			String_creaString("TituloWin"));
	Textura_addTextura("app/native/assets/TituloLose.png",
			String_creaString("TituloLose"));
	Textura_addTextura("app/native/assets/TituloOpciones.png",
			String_creaString("TituloOpciones"));
	Textura_addTextura("app/native/assets/TituloSonidosMusica.png",
			String_creaString("TituloSonidosMusica"));
	Textura_addTextura("app/native/assets/TituloControlMode.png",
			String_creaString("TituloControlMode"));
	Textura_addTextura("app/native/assets/TituloButtonSize.png",
			String_creaString("TituloButtonSize"));
	Textura_addTextura("app/native/assets/TituloSavedGame.png",
			String_creaString("TituloSavedGame"));
	Textura_addTextura("app/native/assets/TituloBuyGame.png",
			String_creaString("TituloBuyGame"));
	Textura_addTextura("app/native/assets/Separador.png",
			String_creaString("Separador"));
	Textura_addTextura("app/native/assets/JLaxo.png",
			String_creaString("JLaxo"));
	Textura_addTextura("app/native/assets/JEsp.png", String_creaString("JEsp"));
	Textura_addTextura("app/native/assets/JEspBrazos1.png",
			String_creaString("JEspBrazos1"));
	Textura_addTextura("app/native/assets/JEspBrazos2.png",
			String_creaString("JEspBrazos2"));
	Textura_addTextura("app/native/assets/JQuad.png",
			String_creaString("JQuad"));
	Textura_addTextura("app/native/assets/JQuadMartillo.png",
			String_creaString("JQuadMartillo"));
	Textura_addTextura("app/native/assets/JRued.png",
			String_creaString("JRued"));
	Textura_addTextura("app/native/assets/JRuedFlecha.png",
			String_creaString("JRuedFlecha"));
	Textura_addTextura("app/native/assets/JefeDisparoMax.png",
			String_creaString("JefeDisparoMax"));
	Textura_addTextura("app/native/assets/JefeDisparoNormal.png",
			String_creaString("JefeDisparoNormal"));
	Textura_addTextura("app/native/assets/Capitana.png",
			String_creaString("Capitana"));
	Textura_addTextura("app/native/assets/NMini.png",
			String_creaString("NMini"));
	Textura_addTextura("app/native/assets/Onda.png", String_creaString("Onda"));
	Textura_addTextura("app/native/assets/Control.png",
			String_creaString("Control"));
	Textura_addTextura("app/native/assets/Help.png", String_creaString("Help"));
	Textura_addTextura("app/native/assets/About.png",
			String_creaString("About"));
	//Fondos
	Textura_addTexturaFondo("app/native/assets/FondoBlack.png",
			String_creaString("FondoBlack")); //0
	Textura_addTexturaFondo("app/native/assets/FondoEstadisticas.png",
			String_creaString("FondoEstadisticas")); //1
	Textura_addTexturaFondo("app/native/assets/FondoPause.png",
			String_creaString("FondoPause")); //2
}

GalacticaMain* GalacticaMain_getObject()
{
	return &GalacticaMainS;
}

