/*
 * Collition.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef COLLITION_H_
#define COLLITION_H_

#include "GalacticaTipos.h"

int Collition_colisionPlusANave(Collition* collition, Plus* plus);
int Collition_colisionDispAEnemigo(Collition* collition, Enemigo* enemigo,
		TipoEnemigo tipoEnemigo);
int Collition_colisionPoderGalacticaAEnemigo(Collition* collition,
		Enemigo* enemigo, TipoEnemigo tipoEnemigo);
int Collition_colisionPoderCarafeAEnemigo(Collition* collition,
		Enemigo* enemigo, TipoEnemigo tipoEnemigo);
int Collition_colisionPoderHelixAEnemigo(Collition* collition, Enemigo* enemigo,
		TipoEnemigo tipoEnemigo);
int Collition_colisionPoderArpAEnemigo(Collition* collition, Enemigo* enemigo,
		TipoEnemigo tipoEnemigo);
int Collition_colisionPoderPerseusAEnemigo(Collition* collition, Enemigo* enemigo,
		TipoEnemigo tipoEnemigo);
int Collition_colisionPoderPiscesAEnemigo(Collition* collition,
		Enemigo* enemigo, TipoEnemigo tipoEnemigo);
int Collition_colisionDispEnemigoANave(Collition* collition,
		DisparoEnemigo* disparoEnemigo);
int Collition_colisionDispJefeANave(Collition* collition,
		DisparoJefe* disparoJefe);
int Collition_colisionNaveAEnemigo(Collition* collition, Enemigo* enemigo);
int Collition_colisionDispAMini(Collition* collition, NMini* mini,
		DisparoEnemigo* disparo);
int Collition_colisionMiniAEnemigo(Collition* collition, NMini* mini,
		Enemigo* enemigo);
void Collition_confirmaEliminaPoderCarafe(int indice);
void Collition_confirmaEliminaDisparoNave(int indice);
Collition* Collition_getSObject();

#endif /* COLLITION_H_ */
