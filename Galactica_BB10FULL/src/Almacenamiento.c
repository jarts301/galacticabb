/*
 * Almacenamiento.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "Almacenamiento.h"
#include "Acelerometro.h"
#include "Audio.h"
#include "Nave.h"
#include "Boton.h"
#include "Plus.h"
#include "Control.h"
#include "Lista.h"
#include "GalacticaMain.h"
#include "QuickModeMenu.h"
#include "QuickModeGame.h"
#include "Jefe.h"
#include "Enemigo.h"
#include "DisparoJefe.h"
#include "DisparoEnemigo.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

static Almacenamiento almacenamientoS;
static DatScore datScore;
static DatMisiones datMisiones;
static DatBonus datBonus;
static DatInGame datInGame;
static DatOpciones datOpciones;

void Almacenamiento_Inicia()
{
	almacenamientoS.directoryName = "GEstadoJuego.dat";
	almacenamientoS.misionFile = "GMisions.dat";
	almacenamientoS.bonusFile = "GBonus.dat";
	almacenamientoS.gameFile = "GInGame.dat";
	almacenamientoS.scoreFile = "GScore.dat";
	almacenamientoS.optionsFile = "GOpciones.dat";
}

//****************************************************************
void Almacenamiento_borrar()
{
	char* rutaMisionFile = "data/GMisions.dat";
	char* rutaBonusFile = "data/GBonus.dat";
	char* rutaOptionsFile = "data/GOpciones.dat";
	char* rutaInGameFile = "data/GInGame.dat";
	char* rutaScoreFile = "data/GScore.dat";

	FILE* MisionFile = fopen(rutaMisionFile, "r");
	FILE* BonusFile = fopen(rutaBonusFile, "r");
	FILE* OptionsFile = fopen(rutaOptionsFile, "r");
	FILE* InGameFile = fopen(rutaInGameFile, "r");
	FILE* ScoreFile = fopen(rutaScoreFile, "r");

	if (MisionFile != NULL)
	{
		fclose(MisionFile);
		remove(rutaMisionFile);
	}
	if (BonusFile != NULL)
	{
		fclose(BonusFile);
		remove(rutaBonusFile);
	}
	if (OptionsFile != NULL)
	{
		fclose(OptionsFile);
		remove(rutaOptionsFile);
	}
	if (InGameFile != NULL)
	{
		fclose(InGameFile);
		remove(rutaInGameFile);
	}
	if (ScoreFile != NULL)
	{
		fclose(ScoreFile);
		remove(rutaScoreFile);
	}
}

//Salvar datos
void Almacenamiento_crearDatos()
{
	char* rutaMisionFile = "data/GMisions.dat";
	char* rutaBonusFile = "data/GBonus.dat";
	char* rutaOptionsFile = "data/GOpciones.dat";
	char* rutaScoreFile = "data/GScore.dat";
	char bufer[10];

	FILE* MisionFile = fopen(rutaMisionFile, "r");

	if (MisionFile == NULL)
	{
		MisionFile = fopen(rutaMisionFile, "w");

		datMisiones.activas = 1; //1
		datMisiones.premio = 0; //0

		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datMisiones.activas);
		fputs(bufer, MisionFile);
		fputs("\n", MisionFile);
		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datMisiones.premio);
		fputs(bufer, MisionFile);
		fputs("\n", MisionFile);

		fclose(MisionFile);

	}
	else
	{
		fclose(MisionFile);
	}

	FILE* BonusFile = fopen(rutaBonusFile, "r");

	if (BonusFile == NULL)
	{

		BonusFile = fopen(rutaBonusFile, "w");

		datBonus.sumarASaludNave = 100;
		datBonus.disparoSuperActivo = 0;
		datBonus.disparoDiagActivo = 0;
		datBonus.naves = 1; //1
		datBonus.tiempoEsperaPoder = 8000000; //8000000
		datBonus.sumaPoderDisparos = 0;
		datBonus.recuperacion = 50;

		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datBonus.sumarASaludNave);
		fputs(bufer, BonusFile);
		fputs("\n", BonusFile);
		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datBonus.disparoSuperActivo);
		fputs(bufer, BonusFile);
		fputs("\n", BonusFile);
		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datBonus.disparoDiagActivo);
		fputs(bufer, BonusFile);
		fputs("\n", BonusFile);
		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datBonus.naves);
		fputs(bufer, BonusFile);
		fputs("\n", BonusFile);
		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datBonus.tiempoEsperaPoder);
		fputs(bufer, BonusFile);
		fputs("\n", BonusFile);
		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datBonus.sumaPoderDisparos);
		fputs(bufer, BonusFile);
		fputs("\n", BonusFile);
		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datBonus.recuperacion);
		fputs(bufer, BonusFile);
		fputs("\n", BonusFile);

		fclose(BonusFile);

	}
	else
	{
		fclose(BonusFile);
	}

	FILE* OptionsFile = fopen(rutaOptionsFile, "r");

	if (OptionsFile == NULL)
	{
		OptionsFile = fopen(rutaOptionsFile, "w");
		//Datos iniciales
		//Opciones
		datOpciones.audioActivo = 1;
		datOpciones.buttonSize = 1;
		datOpciones.controlMode = 1;

		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datOpciones.audioActivo);
		fputs(bufer, OptionsFile);
		fputs("\n", OptionsFile);
		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datOpciones.buttonSize);
		fputs(bufer, OptionsFile);
		fputs("\n", OptionsFile);
		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datOpciones.controlMode);
		fputs(bufer, OptionsFile);
		fputs("\n", OptionsFile);

		fclose(OptionsFile);

	}
	else
	{
		fclose(OptionsFile);
	}

	FILE* ScoreFile = fopen(rutaScoreFile, "r");

	if (ScoreFile == NULL)
	{
		ScoreFile = fopen(rutaScoreFile, "w");
		//Datos iniciales
		//Opciones
		datScore.score = 0;

		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datScore.score);
		fputs(bufer, ScoreFile);
		fputs("\n", ScoreFile);

		fclose(ScoreFile);

	}
	else
	{
		fclose(ScoreFile);
	}

}

void Almacenamiento_cargarScore()
{
	char* rutaScoreFile = "data/GScore.dat";
	FILE* ScoreFile = fopen(rutaScoreFile, "r");
	char bufer[10];

	if (ScoreFile != NULL)
	{
		memset(bufer, '\0', 10);
		fgets(bufer, 10, ScoreFile);
		datScore.score = atoi(bufer);

		(*GalacticaMain_getObject()).BESTSCORE = datScore.score;

		fclose(ScoreFile);
	}
}

void Almacenamiento_salvarScore()
{
	char* rutaScoreFile = "data/GScore.dat";
	FILE* ScoreFile = fopen(rutaScoreFile, "w");
	char bufer[10];

	if (ScoreFile != NULL)
	{
		datScore.score = (*GalacticaMain_getObject()).BESTSCORE;

		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datScore.score);
		fputs(bufer, ScoreFile);
		fputs("\n", ScoreFile);

		fclose(ScoreFile);
	}
}

void Almacenamiento_cargarMisiones()
{
	char* rutaMisionFile = "data/GMisions.dat";
	FILE* MisionFile = fopen(rutaMisionFile, "r");
	char bufer[10];

	if (MisionFile != NULL)
	{
		memset(bufer, '\0', 10);
		fgets(bufer, 10, MisionFile);
		datMisiones.activas = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, MisionFile);
		datMisiones.premio = atoi(bufer);

		fclose(MisionFile);
	}

}

void Almacenamiento_salvarMisiones()
{
	char* rutaMisionFile = "data/GMisions.dat";
	FILE* MisionFile = fopen(rutaMisionFile, "r");
	char bufer[10];

	if (MisionFile != NULL)
	{
		memset(bufer, '\0', 10);
		fgets(bufer, 10, MisionFile);
		datMisiones.activas = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, MisionFile);
		datMisiones.premio = atoi(bufer);

		fclose(MisionFile);
	}

	MisionFile = fopen(rutaMisionFile, "w");

	if (MisionFile != NULL)
	{
		datMisiones.activas = datMisiones.activas + 1;
		datMisiones.premio = datMisiones.premio + 1;

		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datMisiones.activas);
		fputs(bufer, MisionFile);
		fputs("\n", MisionFile);
		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datMisiones.premio);
		fputs(bufer, MisionFile);
		fputs("\n", MisionFile);

		fclose(MisionFile);
	}
}

//Cargar Bonus
void Almacenamiento_cargarBonus()
{
	char* rutaBonusFile = "data/GBonus.dat";
	FILE* BonusFile = fopen(rutaBonusFile, "r");
	char bufer[10];

	if (BonusFile != NULL)
	{

		memset(bufer, '\0', 10);
		fgets(bufer, 10, BonusFile);
		datBonus.sumarASaludNave = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, BonusFile);
		datBonus.disparoSuperActivo = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, BonusFile);
		datBonus.disparoDiagActivo = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, BonusFile);
		datBonus.naves = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, BonusFile);
		datBonus.tiempoEsperaPoder = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, BonusFile);
		datBonus.sumaPoderDisparos = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, BonusFile);
		datBonus.recuperacion = atoi(bufer);

		(*Nave_getSObject()).MaxVida = datBonus.sumarASaludNave;
		(*Boton_getSObject()).navesActivas = datBonus.naves;
		(*Boton_getSObject()).tiempoEsperaPoder = datBonus.tiempoEsperaPoder;
		(*Nave_getSObject()).masPoderDisp = datBonus.sumaPoderDisparos;
		(*Plus_getSObject()).recuperacion = datBonus.recuperacion;

		if (datBonus.disparoSuperActivo == 1)
		{
			(*Nave_getSObject()).sePuedeDispSuper = 1;
		}
		else
		{
			(*Nave_getSObject()).sePuedeDispSuper = 0;
		}
		if (datBonus.disparoDiagActivo == 1)
		{
			(*Nave_getSObject()).sePuedeDispDiag = 1;
		}
		else
		{
			(*Nave_getSObject()).sePuedeDispDiag = 0;
		}

		(*QuickModeMenu_getSObject()).rescatadas = datBonus.naves;

		fclose(BonusFile);
	}

}

//Salvar Bonus
void Almacenamiento_salvarBonus(int datoASalvar, BonusTipo tipoB)
{
	char* rutaBonusFile = "data/GBonus.dat";
	FILE* BonusFile = fopen(rutaBonusFile, "r");
	char bufer[10];

	if (BonusFile != NULL)
	{
		memset(bufer, '\0', 10);
		fgets(bufer, 10, BonusFile);
		datBonus.sumarASaludNave = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, BonusFile);
		datBonus.disparoSuperActivo = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, BonusFile);
		datBonus.disparoDiagActivo = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, BonusFile);
		datBonus.naves = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, BonusFile);
		datBonus.tiempoEsperaPoder = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, BonusFile);
		datBonus.sumaPoderDisparos = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, BonusFile);
		datBonus.recuperacion = atoi(bufer);

		fclose(BonusFile);
	}

	BonusFile = fopen(rutaBonusFile, "w");
	if (BonusFile != NULL)
	{
		switch (tipoB)
		{
		case ALsumarAsalud:
			datBonus.sumarASaludNave = datoASalvar;
			break;
		case ALdisparoSuper:
			datBonus.disparoSuperActivo = datoASalvar;
			break;
		case ALdisparoDiag:
			datBonus.disparoDiagActivo = datoASalvar;
			break;
		case ALnaves:
			datBonus.naves = datoASalvar;
			break;
		case ALtEsperaPoder:
			datBonus.tiempoEsperaPoder = datoASalvar;
			break;
		case ALpoderDisp:
			datBonus.sumaPoderDisparos = datoASalvar;
			break;
		case ALrecuperacion:
			datBonus.recuperacion = datoASalvar;
			break;
		}

		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datBonus.sumarASaludNave);
		fputs(bufer, BonusFile);
		fputs("\n", BonusFile);
		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datBonus.disparoSuperActivo);
		fputs(bufer, BonusFile);
		fputs("\n", BonusFile);
		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datBonus.disparoDiagActivo);
		fputs(bufer, BonusFile);
		fputs("\n", BonusFile);
		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datBonus.naves);
		fputs(bufer, BonusFile);
		fputs("\n", BonusFile);
		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datBonus.tiempoEsperaPoder);
		fputs(bufer, BonusFile);
		fputs("\n", BonusFile);
		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datBonus.sumaPoderDisparos);
		fputs(bufer, BonusFile);
		fputs("\n", BonusFile);
		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datBonus.recuperacion);
		fputs(bufer, BonusFile);
		fputs("\n", BonusFile);

		fclose(BonusFile);

	}

}

//Salvar opciones
void Almacenamiento_salvarOpciones()
{
	char* rutaOptionFile = "data/GOpciones.dat";
	FILE* OptionsFile = fopen(rutaOptionFile, "w");
	char bufer[10];

	if (OptionsFile != NULL)
	{
		if ((*Audio_getSObject()).musicaActiva==1
				&& (*Audio_getSObject()).efectosActivos==1)
		{
			datOpciones.audioActivo = 1;
		}
		else
		{
			datOpciones.audioActivo = 0;
		}
		if ((*Control_getSObject()).tamanoGrande == 0)
		{
			datOpciones.buttonSize = 0;
		}
		else
		{
			datOpciones.buttonSize = 1;
		}
		if ((*Control_getSObject()).controlActivo == 0)
		{
			datOpciones.controlMode = 0;
		}
		else
		{
			datOpciones.controlMode = 1;
		}

		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datOpciones.audioActivo);
		fputs(bufer, OptionsFile);
		fputs("\n", OptionsFile);
		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datOpciones.buttonSize);
		fputs(bufer, OptionsFile);
		fputs("\n", OptionsFile);
		memset(bufer, '\0', 10);
		sprintf(bufer, "%d", datOpciones.controlMode);
		fputs(bufer, OptionsFile);
		fputs("\n", OptionsFile);

		fclose(OptionsFile);
	}

}

//Cargar opciones
void Almacenamiento_cargarOpciones()
{
	char* rutaOptionFile = "data/GOpciones.dat";
	FILE* OptionsFile = fopen(rutaOptionFile, "r");
	char bufer[10];

	if (OptionsFile != NULL)
	{
		memset(bufer, '\0', 10);
		fgets(bufer, 10, OptionsFile);
		datOpciones.audioActivo = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, OptionsFile);
		datOpciones.buttonSize = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, OptionsFile);
		datOpciones.controlMode = atoi(bufer);

		if (datOpciones.audioActivo == 1)
		{
			(*Audio_getSObject()).musicaActiva = 1;
			(*Audio_getSObject()).efectosActivos = 1;
		}
		else
		{
			(*Audio_getSObject()).musicaActiva = 0;
			(*Audio_getSObject()).efectosActivos = 0;
		}
		if (datOpciones.buttonSize == 1)
		{
			(*Control_getSObject()).tamanoGrande = true;
		}
		else
		{
			(*Control_getSObject()).tamanoGrande = false;
		}

		if (datOpciones.controlMode == 1)
		{
			(*Control_getSObject()).controlActivo = 1;
			(*Acelerometro_getSObject()).acelActivo = 0;
		}
		else
		{
			(*Control_getSObject()).controlActivo = 0;
			(*Acelerometro_getSObject()).acelActivo = 1;
		}

		fclose(OptionsFile);
	}
}

//Salvar In Game
void Almacenamiento_salvarInGame()
{
	char* rutaInGameFile = "data/GInGame.dat";
	FILE* InGameFile = fopen(rutaInGameFile, "w");

	if (InGameFile != NULL)
	{
		Almacenamiento_datosASalvarInGame(InGameFile);
	}

}

//Borrar In Game
void Almacenamiento_borrarInGame()
{
	char* rutaInGameFile = "data/GInGame.dat";
	FILE* InGameFile = fopen(rutaInGameFile, "r");

	if (InGameFile != NULL)
	{
		fclose(InGameFile);
		remove(rutaInGameFile);
	}
}

//Cargar In Game
void Almacenamiento_cargarInGame()
{
	char* rutaInGameFile = "data/GInGame.dat";
	FILE* InGameFile = fopen(rutaInGameFile, "r");
	char bufer[10];

	if (InGameFile != NULL)
	{
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.puntos = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.vidaNave = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.texturaNaveActual = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.maxEnemigos = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.enemigosDestruidos = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.disparoDiagActivo = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.disparoSuperActivo = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.tiempoEsperaPlus = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.aumentoVidaJefe = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.aumentoVidaEnemigo = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.adicionDanoJefe = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.adicionDanoDispJefe = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.adicionDanoEnemigo = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.adicionDanoDispEnemigo = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.ultimaAdJefe = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.ultimoAumento = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.hayJefe = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.tipoJefe = atoi(bufer);
		memset(bufer, '\0', 10);
		fgets(bufer, 10, InGameFile);
		datInGame.vidaJefe = atoi(bufer);

		(*Nave_getSObject()).Puntos = datInGame.puntos;
		(*Nave_getSObject()).Vida = datInGame.vidaNave;
		(*Nave_getSObject()).TexturaNave = datInGame.texturaNaveActual;
		(*QuickModeGame_getSObject()).igb.maxEnemigos = datInGame.maxEnemigos;
		(*Nave_getSObject()).Destruidos = datInGame.enemigosDestruidos;
		if (datInGame.disparoDiagActivo == 1)
		{
			(*Nave_getSObject()).disparosDiagActivos = 1;
		}
		else
		{
			(*Nave_getSObject()).disparosDiagActivos = 0;
		}
		if (datInGame.disparoSuperActivo == 1)
		{
			(*Nave_getSObject()).disparoSuperActivo = 1;
		}
		else
		{
			(*Nave_getSObject()).disparoSuperActivo = 0;
		}
		(*QuickModeGame_getSObject()).igb.tiempoEsperaPlus =
				datInGame.tiempoEsperaPlus;
		(*QuickModeGame_getSObject()).igb.aumentoEnVidaJefe =
				datInGame.aumentoVidaJefe;
		(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo =
				datInGame.aumentoVidaEnemigo;
		(*Jefe_getSObject()).adicionDanoNave = datInGame.adicionDanoJefe;
		(*DisparoJefe_getSObject()).adicionDanoDisNave =
				datInGame.adicionDanoDispJefe;
		(*Enemigo_getSObject()).adicionDanoNave = datInGame.adicionDanoEnemigo;
		(*DisparoEnemigo_getSObject()).adicionDanoDisNave =
				datInGame.adicionDanoDispEnemigo;
		(*QuickModeGame_getSObject()).igb.ultimoAumento =
				datInGame.ultimoAumento;

		if ((*Nave_getSObject()).Puntos >= 1500)
		{
			(*QuickModeGame_getSObject()).igb.llegaA1500 = 1;
		}
		else
		{
			(*QuickModeGame_getSObject()).igb.llegaA1500 = 0;
		}

		(*QuickModeGame_getSObject()).igb.ultimaAdicionJefe =
				datInGame.ultimaAdJefe;
		if (datInGame.hayJefe == 1)
		{

			switch (datInGame.tipoJefe)
			{
			case 1:
				QuickModeGame_agregarJefe(JLaxo);
				break;
			case 2:
				QuickModeGame_agregarJefe(JEsp);
				break;
			case 3:
				QuickModeGame_agregarJefe(JQuad);
				break;
			case 4:
				QuickModeGame_agregarJefe(JRued);
				break;
			}

			(*(Jefe*) Lista_RetornaElemento(
					&((*QuickModeGame_getSObject()).igb.jefes), 0)).vida =
					datInGame.vidaJefe;
		}
		fclose(InGameFile);
	}

}

//****
int Almacenamiento_getMisionesActivas()
{
	return datMisiones.activas;
}

int Almacenamiento_getPremiosDados()
{
	return datMisiones.premio;
}

int Almacenamiento_getBestScore()
{
	return datScore.score;
}

//****************************************************************************

void Almacenamiento_datosASalvarInGame(FILE* InGameFile)
{
	char bufer[10];

	datInGame.puntos = (*Nave_getSObject()).Puntos;
	datInGame.vidaNave = (*Nave_getSObject()).Vida;
	datInGame.texturaNaveActual = (*Nave_getSObject()).TexturaNave;
	datInGame.maxEnemigos = (*QuickModeGame_getSObject()).igb.maxEnemigos;
	datInGame.enemigosDestruidos = (*Nave_getSObject()).Destruidos;
	if ((*Nave_getSObject()).disparosDiagActivos == 1)
	{
		datInGame.disparoDiagActivo = 1;
	}
	else
	{
		datInGame.disparoDiagActivo = 0;
	}
	if ((*Nave_getSObject()).disparoSuperActivo)
	{
		datInGame.disparoSuperActivo = 1;
	}
	else
	{
		datInGame.disparoSuperActivo = 0;
	}
	datInGame.tiempoEsperaPlus =
			(*QuickModeGame_getSObject()).igb.tiempoEsperaPlus;
	datInGame.aumentoVidaJefe =
			(*QuickModeGame_getSObject()).igb.aumentoEnVidaJefe;
	datInGame.aumentoVidaEnemigo =
			(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo;
	datInGame.adicionDanoJefe = (*Jefe_getSObject()).adicionDanoNave;
	datInGame.adicionDanoDispJefe =
			(*DisparoJefe_getSObject()).adicionDanoDisNave;
	datInGame.adicionDanoEnemigo = (*Enemigo_getSObject()).adicionDanoNave;
	datInGame.adicionDanoDispEnemigo =
			(*DisparoEnemigo_getSObject()).adicionDanoDisNave;
	datInGame.ultimoAumento = (*QuickModeGame_getSObject()).igb.ultimoAumento;

	datInGame.ultimaAdJefe =
			(*QuickModeGame_getSObject()).igb.ultimaAdicionJefe;
	if ((*QuickModeGame_getSObject()).igb.jefes.size > 0)
	{
		datInGame.hayJefe = 1;
		datInGame.vidaJefe = (*(Jefe*) Lista_RetornaElemento(
				&((*QuickModeGame_getSObject()).igb.jefes), 0)).vida;

		switch ((*(Jefe*) Lista_RetornaElemento(
				&((*QuickModeGame_getSObject()).igb.jefes), 0)).tipoJefe)
		{
		case JLaxo:
			datInGame.tipoJefe = 1;
			break;
		case JEsp:
			datInGame.tipoJefe = 2;
			break;
		case JQuad:
			datInGame.tipoJefe = 3;
			break;
		case JRued:
			datInGame.tipoJefe = 4;
			break;
		}

	}
	else
	{
		datInGame.hayJefe = 0;
		datInGame.vidaJefe = 0;
		datInGame.tipoJefe = 0;
	}

	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.puntos);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);
	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.vidaNave);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);
	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.texturaNaveActual);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);
	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.maxEnemigos);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);
	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.enemigosDestruidos);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);
	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.disparoDiagActivo);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);
	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.disparoSuperActivo);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);
	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.tiempoEsperaPlus);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);
	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.aumentoVidaJefe);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);
	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.aumentoVidaEnemigo);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);
	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.adicionDanoJefe);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);
	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.adicionDanoDispJefe);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);
	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.adicionDanoEnemigo);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);
	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.adicionDanoDispEnemigo);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);
	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.ultimaAdJefe);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);
	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.ultimoAumento);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);
	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.hayJefe);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);
	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.tipoJefe);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);
	memset(bufer, '\0', 10);
	sprintf(bufer, "%d", datInGame.vidaJefe);
	fputs(bufer, InGameFile);
	fputs("\n", InGameFile);

	fclose(InGameFile);
}

int Almacenamiento_hayJuegoSalvado()
{
	char* rutaInGameFile = "data/GInGame.dat";
	FILE* InGameFile = fopen(rutaInGameFile, "r");

	if (InGameFile != NULL)
	{
		fclose(InGameFile);
		return 1;
	}
	else
	{
		return 0;
	}
}

