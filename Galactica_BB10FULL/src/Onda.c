/*
 * Onda.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "Onda.h"
#include "Dibujar.h"
#include "Lista.h"
#include "Textura.h"
#include <stdlib.h>

static Onda OndaS;

Onda* Onda_Crear()
{
	Onda* nuevo;
	if ((nuevo = (Onda*) malloc(sizeof(Onda))) == NULL)
		return NULL;

	(*nuevo).Rotacion = 0;
	(*nuevo).Escala = GameFramework_Vector2(1.5, 1.5);
	(*nuevo).Position.X = 300;
	(*nuevo).Position.Y = -40;
	(*nuevo).actualframe = 0;
	Onda_setTrozoAnimacion(nuevo);
	Onda_setOrigen(nuevo);
	(*nuevo).texturaActual = 0;
	(*nuevo).dato=0;

	return nuevo;
}

void Onda_reiniciar(Onda* onda)
{
	(*onda).Rotacion = 0;
	(*onda).Escala = GameFramework_Vector2(1.5, 1.5);
	(*onda).Position.X = 300;
	(*onda).Position.Y = -40;
	(*onda).actualframe = 0;
	Onda_setTrozoAnimacion(onda);
	Onda_setOrigen(onda);
	(*onda).texturaActual = 0;
}

void Onda_load()
{
	Onda_setTextura();
}

void Onda_update(Onda* onda)
{
	Onda_velocidadAnimacion(onda, 3);
	Onda_setTrozoAnimacion(onda);
	Onda_setOrigen(onda);
}

void Onda_draw(Onda* onda)
{
	Dibujar_drawSpriteTr(
			(TexturaObjeto*) Lista_GetIn(&OndaS.texDisparo,
					(*onda).texturaActual), (*onda).Position, (*onda).TrozoAnim,
			GameFramework_Color(1, 1, 1, 1), (*onda).Rotacion, (*onda).Origen,
			(*onda).Escala);
}

//**********************************

void Onda_setTextura()
{
	Lista_Inicia(&OndaS.texDisparo);
	Lista_AddInFin(&OndaS.texDisparo, Textura_getTextura("Onda"));
}

void Onda_setPosition(Onda* onda, Vector2 posNave)
{
	(*onda).Position.X = posNave.X;
	(*onda).Position.Y = posNave.Y;
}

Vector2 Onda_getPosition(Onda* onda)
{
	return (*onda).Position;
}

void Onda_setOrigen(Onda* onda)
{
	(*onda).Origen.X = (*onda).TrozoAnim.Width / 2;
	(*onda).Origen.Y = (*onda).TrozoAnim.Height / 2;
}

//define la velocidad de la animacion de la nave
void Onda_velocidadAnimacion(Onda* onda, int velocidad)
{
	(*onda).dato += GameFramework_getSwapTime();
	if ((*onda).dato >= ((GameFramework_getSwapTime()) * velocidad))
	{
		(*onda).actualframe = (*onda).actualframe + 1;
		(*onda).dato = 0;
	}
	if ((*onda).actualframe >= 7)
	{
		(*onda).actualframe = 0;
	}
}

//el cuadro que debe mostrar en el siguiente draw
void Onda_setTrozoAnimacion(Onda* onda)
{
	(*onda).TrozoAnim.X = 0;
	(*onda).TrozoAnim.Y = (*onda).actualframe * 146.285;
	(*onda).TrozoAnim.Width = 512;
	(*onda).TrozoAnim.Height = 146.285;
}

//Movimiento nave mini
Onda* Onda_getSObject()
{
	return &OndaS;
}

