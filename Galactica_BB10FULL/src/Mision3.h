/*
 * Mision3.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION3_H_
#define MISION3_H_

#include "GalacticaTipos.h"

void Mision3_reiniciar();
void Mision3_load();
void Mision3_update();
void Mision3_draw();
void Mision3_drawEstadNave();
void Mision3_updateGame();
void Mision3_gameOver();
void Mision3_drawMensajeInicial();
void Mision3_insertaEnemigo();
Mision3* Mision3_getSObject();

#endif /* MISION3_H_ */
