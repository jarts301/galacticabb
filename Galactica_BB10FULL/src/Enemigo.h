/*
 * Enemigo.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef ENEMIGO_H_
#define ENEMIGO_H_

#include "GameFramework.h"
#include "Lista.h"

#include "GalacticaTipos.h"

Enemigo* Enemigo_Crear(TipoEnemigo t, int x, int y, int vx, int vy);
void Enemigo_reinicia(Enemigo* enemigo, int x, int y, int vx, int vy,
		int aumentoVida);
void Enemigo_load();
void Enemigo_update(Enemigo* enemigo);
void Enemigo_draw(Enemigo* enemigo);
void Enemigo_setAumentoVida(Enemigo* enemigo, int v);
void Enemigo_setPosition(Enemigo* enemigo, Vector2 pos);
void Enemigo_loadListasEnemigos();
void Enemigo_velocidadAnimacion(Enemigo* enemigo);
void Enemigo_defineDatosAnimacion(Enemigo* enemigo);
void Enemigo_defineDatosIniciales(Enemigo* enemigo);
void Enemigo_defineTrozoAnim(Enemigo* enemigo, int x, int y, int width,
		int height);
void Enemigo_defineOrigen(Enemigo* enemigo);
void Enemigo_movimientoNor(Enemigo* enemigo);
void Enemigo_updateDisparosNor(Enemigo* enemigo);
void Enemigo_drawDisparosNor(Enemigo* enemigo);
void Enemigo_movimientoKami(Enemigo* enemigo);
void Enemigo_movimientoSid(Enemigo* enemigo);
void Enemigo_updateDisparosSid(Enemigo* enemigo);
void Enemigo_drawDisparosSid(Enemigo* enemigo);
void Enemigo_defineCollitionBoxSid();
void Enemigo_defineCollitionBoxHiSid();
void Enemigo_updateAnimaciones(Enemigo* enemigo);
void Enemigo_drawAnimaciones(Enemigo* enemigo);
void Enemigo_setTextura();
void Enemigo_defineListasDeDisparos();
void Enemigo_confirmaColisiones(Collition* colision, Enemigo* enemigo,
		int danoNave);
Enemigo* Enemigo_getSObject();

#endif /* ENEMIGO_H_ */
