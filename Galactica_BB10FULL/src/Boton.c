/*
 * Boton.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "Boton.h"
#include "Audio.h"
#include "Nave.h"
#include "Textura.h"
#include "GameFramework.h"
#include "Almacenamiento.h"
#include "Acelerometro.h"
#include "MisionModeGOver.h"
#include "MisionModeInfo.h"
#include "MisionModePause.h"
#include "MisionModeGame.h"
#include "MisionModeMenu.h"
#include "MisionMode.h"
#include "MainMenu.h"
#include "GalacticaMain.h"
#include "Control.h"
#include "QuickMode.h"
#include "QuickModeGOver.h"
#include "QuickModeGame.h"
#include "QuickModeMenu.h"
#include "QuickModePause.h"
#include "TouchControl.h"
#include "Dibujar.h"
#include <stdlib.h>
#include <stdio.h>
#include "Mision1.h"
#include "Mision2.h"
#include "Mision3.h"
#include "Mision4.h"
#include "Mision5.h"
#include "Mision6.h"
#include "Mision7.h"
#include "Mision8.h"
#include "Mision9.h"
#include "Mision10.h"
#include "Mision11.h"
#include "Mision12.h"
#include "Mision13.h"
#include "Mision14.h"
#include "Mision15.h"
#include "Mision16.h"
#include "Mision17.h"
#include "Mision18.h"
#include "Mision19.h"
#include "Mision20.h"

//Para estaticas
static Boton botonS;

Boton* Boton_Crear(Boton_tipo tb, int posX, int posY)
{
	Boton* nuevo;
	if ((nuevo = (Boton*) malloc(sizeof(Boton))) == NULL)
		return NULL;

	(*nuevo).tipoActual = tb;
	switch (tb)
	{
	case BFlecha:
		//Nada ya esta en otro update
		break;
	case BQuickGame:
		Boton_defineBQuickMode(nuevo, posX, posY);
		break;
	case BPause:
		Boton_defineBPause(nuevo, posX, posY);
		break;
	case BResumeGame:
		Boton_defineBResumeGame(nuevo, posX, posY);
		break;
	case BExitGame:
		Boton_defineBExitGame(nuevo, posX, posY);
		break;
	case BPlay:
		Boton_defineBPlay(nuevo, posX, posY);
		break;
	case BPower:
		Boton_defineBPower(nuevo, posX, posY);
		break;
	case BContinue:
		Boton_defineBContinue(nuevo, posX, posY);
		break;
	case BMisionMode:
		Boton_defineBMisionMode(nuevo, posX, posY);
		break;
	case BMision:
		Boton_defineBMision(nuevo, posX, posY);
		break;
	case BBack:
		Boton_defineBBack(nuevo, posX, posY);
		break;
	case BNextMision:
		Boton_defineBNextMision(nuevo, posX, posY);
		break;
	case BRestart:
		Boton_defineBRestart(nuevo, posX, posY);
		break;
	case BAudio:
		Boton_defineBAudio(nuevo, posX, posY);
		break;
	case BOpciones:
		Boton_defineBOpciones(nuevo, posX, posY);
		break;
	case BSalir:
		Boton_defineBSalir(nuevo, posX, posY);
		break;
	case BTilting:
		Boton_defineBTilting(nuevo, posX, posY);
		break;
	case BButtons:
		Boton_defineBButtons(nuevo, posX, posY);
		break;
	case BMedium:
		Boton_defineBMedium(nuevo, posX, posY);
		break;
	case BLarge:
		Boton_defineBLarge(nuevo, posX, posY);
		break;
	case BHelp:
		Boton_defineBHelp(nuevo, posX, posY);
		break;
	case BAbout:
		Boton_defineBAbout(nuevo, posX, posY);
		break;
	case BContinueGame:
		Boton_defineBContinueGame(nuevo, posX, posY);
		break;
	case BNewGame:
		Boton_defineBNewGame(nuevo, posX, posY);
		break;
	case BFacebook:
		Boton_defineBFacebook(nuevo, posX, posY);
		break;
	case BTwitter:
		Boton_defineBTwitter(nuevo, posX, posY);
		break;
	case BComent:
		Boton_defineBComent(nuevo, posX, posY);
		break;
	case BGoBuy:
		Boton_defineBGoBuy(nuevo, posX, posY);
		break;
	case BNotNow:
		Boton_defineBNotNow(nuevo, posX, posY);
		break;
	}
	(*nuevo).color = GameFramework_Color(1, 1, 1, 1);

	return nuevo;
}

Boton* Boton_CrearBotonF(Boton_tipo tb, int posX, int posY,
		int dirDerechaFlecha)
{
	Boton* nuevo;
	if ((nuevo = (Boton*) malloc(sizeof(Boton))) == NULL)
		return NULL;

	(*nuevo).tipoActual = tb;

	if (tb == BFlecha)
	{
		(*nuevo).flechaDerecha = dirDerechaFlecha;
		Boton_defineBFlecha(nuevo, posX, posY, dirDerechaFlecha);
	}

	(*nuevo).color = GameFramework_Color(1, 1, 1, 1);

	return nuevo;
}

void Boton_load()
{
	Boton_setTexturas();
	botonS.indiceMision = -1;
	botonS.ultimaActualizacion = 0;
}

void Boton_update(Boton* boton)
{
	//touches = TouchControl.touches;
	switch ((*boton).tipoActual)
	{
	case BQuickGame:
		Boton_verificaTouchBQuickMode(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BPause:
		Boton_verificaTouchBPause(boton, 64, 64);
		break;
	case BResumeGame:
		Boton_verificaTouchBResumeGame(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BExitGame:
		Boton_verificaTouchBExitGame(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BPlay:
		Boton_verificaTouchBPlay(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BFlecha:
		Boton_verificaTouchBFlecha(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BPower:
		Boton_verificaTouchBPower(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BContinue:
		Boton_verificaTouchBContinue(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BMisionMode:
		Boton_verificaTouchBMisionMode(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BMision:
		Boton_verificaTouchBMision(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BBack:
		Boton_verificaTouchBBack(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BNextMision:
		Boton_verificaTouchBNextMision(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BRestart:
		Boton_verificaTouchBRestart(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BAudio:
		Boton_verificaTouchBAudio(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BOpciones:
		Boton_verificaTouchBOpciones(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BSalir:
		Boton_verificaTouchBSalir(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BTilting:
		Boton_verificaTouchBTilting(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BButtons:
		Boton_verificaTouchBButtons(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BMedium:
		Boton_verificaTouchBMedium(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BLarge:
		Boton_verificaTouchBLarge(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BHelp:
		Boton_verificaTouchBHelp(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BAbout:
		Boton_verificaTouchBAbout(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BContinueGame:
		Boton_verificaTouchBContinueGame(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BNewGame:
		Boton_verificaTouchBNewGame(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BFacebook:
		Boton_verificaTouchBFacebook(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BTwitter:
		Boton_verificaTouchBTwitter(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BComent:
		Boton_verificaTouchBComent(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BGoBuy:
		Boton_verificaTouchBGoBuy(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	case BNotNow:
		Boton_verificaTouchBNotNow(boton, (*boton).TrozoAnim.Width,
				(*boton).TrozoAnim.Height);
		break;
	}

}

void Boton_draw(Boton* boton)
{
	Dibujar_drawSpriteTr(
			(TexturaObjeto*) Lista_RetornaElemento(&(botonS.textura),
					(*boton).texActual), (*boton).Position, (*boton).TrozoAnim,
			(*boton).color, (*boton).Rotacion, (*boton).Origen,
			(*boton).Escala);
}

//***************************************

//*************Boton de Quick mode ************
void Boton_defineBQuickMode(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 0;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.17f;
	(*boton).Escala.Y = 0.81f;
	//Capa = 0.1f;
}

//*************Boton de Pause ************
void Boton_defineBPause(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	Boton_defineTrozoAnim(boton, 0, 0, 64, 64);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 1;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.1f;
	(*boton).Escala.Y = 1.1f;
	//Capa = 0.4f;
}

//*************Boton de Resume Game ************
void Boton_defineBResumeGame(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 2;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.47f;
	(*boton).Escala.Y = 1.11f;
	//Capa = 0.1f;
}

//*************Boton de Exit Game ************
void Boton_defineBExitGame(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 3;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.47f;
	(*boton).Escala.Y = 1.11f;
	//Capa = 0.1f;
}

//*************Boton de Flecha ************
void Boton_defineBFlecha(Boton* boton, int posX, int posY, int dirDerechaFlecha)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 128, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 4;
	if (dirDerechaFlecha == 1)
	{
		(*boton).Rotacion = 180;
	}
	if (dirDerechaFlecha == 0)
	{
		(*boton).Rotacion = 0;
	}
	(*boton).Escala.X = 0.8f;
	(*boton).Escala.Y = 0.8f;
}

//*********Boton Play***********
void Boton_defineBPlay(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 5;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.22f;
	(*boton).Escala.Y = 0.86f;
	//Capa = 0.1f;
}

//*************Boton de Power ************
void Boton_defineBPower(Boton* boton, int posX, int posY)
{
	(*boton).bPoderActivo = 0;
	(*boton).lineaAnim = 0;
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, 0, 170.666, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 6;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 0.5f;
	(*boton).Escala.Y = 0.7f;
	//Capa = 0.4f;
}

//*************Boton de Continue ************
void Boton_defineBContinue(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 7;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.0f;
	(*boton).Escala.Y = 0.81f;
	//Capa = 0.1f;
}

//*************Boton de Mision Mode ************
void Boton_defineBMisionMode(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 9;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.17f;
	(*boton).Escala.Y = 0.81f;
	//Capa = 0.1f;
}

//*************Boton de Mision ************
void Boton_defineBMision(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 1;
	Boton_defineTrozoAnim(boton, (*boton).actualframeB * 128, 0, 128, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 8;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.0f;
	(*boton).Escala.Y = 1.0f;
	//Capa = 0.1f;
}

//*************Boton de Back ************
void Boton_defineBBack(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 10;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 0.72f;
	(*boton).Escala.Y = 0.53f;
	//Capa = 0.1f;
}

//*************Boton de Next mision ************
void Boton_defineBNextMision(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 11;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.47f;
	(*boton).Escala.Y = 1.11f;
	//Capa = 0.1f;
}

//*************Boton de Restart ************
void Boton_defineBRestart(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 12;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.0f;
	(*boton).Escala.Y = 0.81f;
	//Capa = 0.1f;
}

//*************Boton de Audio ************
void Boton_defineBAudio(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	if ((*Audio_getSObject()).musicaActiva == 1
			&& (*Audio_getSObject()).efectosActivos == 1)
	{
		(*boton).actualframeB = 0;
	}
	else
	{
		(*boton).actualframeB = 1;
	}
	Boton_defineTrozoAnim(boton, (*boton).actualframeB * 64, 0, 64, 64);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 13;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.1f;
	(*boton).Escala.Y = 1.1f;
	//Capa = 0.4f;
}

//*************Boton de Opciones ************
void Boton_defineBOpciones(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 14;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.17f;
	(*boton).Escala.Y = 0.81f;
	//Capa = 0.1f;
}

//*************Boton de Salir ************
void Boton_defineBSalir(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 15;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.17f;
	(*boton).Escala.Y = 0.81f;
	//Capa = 0.1f;
}

//*************Boton Tilting ************
void Boton_defineBTilting(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 16;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 0.9f;
	(*boton).Escala.Y = 0.7f;
	//Capa = 0.1f;
}

//*************Boton Buttons ************
void Boton_defineBButtons(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 17;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 0.9f;
	(*boton).Escala.Y = 0.7f;
	//Capa = 0.1f;
}

//*************Boton Medium ************
void Boton_defineBMedium(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 18;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 0.9f;
	(*boton).Escala.Y = 0.7f;
	//Capa = 0.1f;
}

//*************Boton Large ************
void Boton_defineBLarge(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 19;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 0.9f;
	(*boton).Escala.Y = 0.7f;
	//  Capa = 0.1f;
}

//*************Boton de Help ************
void Boton_defineBHelp(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 20;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.17f;
	(*boton).Escala.Y = 0.81f;
	//Capa = 0.1f;
}

//*************Boton de Acerca de************
void Boton_defineBAbout(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 21;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.17f;
	(*boton).Escala.Y = 0.81f;
	//Capa = 0.1f;
}

//*************Boton de ContinueGame************
void Boton_defineBContinueGame(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 22;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.47f;
	(*boton).Escala.Y = 1.11f;
	//Capa = 0.1f;
}

//*************Boton de NewGame************
void Boton_defineBNewGame(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 23;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.47f;
	(*boton).Escala.Y = 1.11f;
	//Capa = 0.1f;
}

//*************Boton de Facebook************
void Boton_defineBFacebook(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 64, 64, 64);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 24;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.0f;
	(*boton).Escala.Y = 1.0f;
	//Capa = 0.1f;
}

//*************Boton de Twitter************
void Boton_defineBTwitter(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 64, 64, 64);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 28;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.0f;
	(*boton).Escala.Y = 1.0f;
	//Capa = 0.1f;
}

//*************Boton de Comentario************
void Boton_defineBComent(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 25;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.0f;
	(*boton).Escala.Y = 1.0f;
	//Capa = 0.1f;
}

//*************Boton de Go Buy************
void Boton_defineBGoBuy(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 26;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.3f;
	(*boton).Escala.Y = 1.3f;
	//Capa = 0.1f;
}

//*************Boton de NotNow************
void Boton_defineBNotNow(Boton* boton, int posX, int posY)
{
	Boton_setPosition(boton, posX, posY);
	(*boton).actualframeB = 0;
	Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	(*boton).Origen.X = 0;
	(*boton).Origen.Y = 0;
	(*boton).texActual = 27;
	(*boton).Rotacion = 0.0f;
	(*boton).Escala.X = 1.3f;
	(*boton).Escala.Y = 1.3f;
	//Capa = 0.1f;
}

//*********Metodos Verificacion***********
//Boton pause
void Boton_verificaTouchBPause(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{

				if ((*Audio_getSObject()).musicaActiva == 1)
				{
					Audio_playAudio("PresionarBoton");
					Audio_stopAudio("Galactic");
				}

				if ((*QuickMode_getSObject()).estadoActual == QMGame
						&& (*GalacticaMain_getObject()).estadoActual
								== GMQuickGame)
				{
					(*QuickMode_getSObject()).estadoActual = QMPause;
				}
				if ((*MisionMode_getSObject()).estadoActual == MMGame
						&& (*GalacticaMain_getObject()).estadoActual
								== GMMisionMode)
				{
					(*MisionMode_getSObject()).estadoActual = MMPause;
				}
			}
		}
	}
}

//Boton Audio
void Boton_verificaTouchBAudio(Boton* boton, int ancho, int alto)
{
	if ((*Audio_getSObject()).efectosActivos == 1
			&& (*Audio_getSObject()).musicaActiva == 1)
	{

		if (((*QuickMode_getSObject()).estadoActual == QMGame
				|| (*MisionMode_getSObject()).estadoActual == MMGame)
				&& (*Audio_getAudio("Galactic")).estado != playing)
		{

			Audio_playAudio("Galactic");
		}

		if (((*QuickMode_getSObject()).estadoActual != QMGame
				&& (*QuickMode_getSObject()).estadoActual != QMPause
				&& (*MisionMode_getSObject()).estadoActual != MMGame
				&& (*MisionMode_getSObject()).estadoActual != MMPause)
				&& (*Audio_getAudio("Galactic")).estado != playing)
		{
			Audio_playAudio("MainMenu");
		}

		(*boton).actualframeB = 0;
		Boton_defineTrozoAnim(boton, (*boton).actualframeB * 64, 0, 64, 64);
	}
	else
	{

		if (((*QuickMode_getSObject()).estadoActual == QMGame
				|| (*MisionMode_getSObject()).estadoActual == MMGame)
				&& (*Audio_getAudio("Galactic")).estado == playing)
		{

			Audio_stopAudio("Galactic");
		}

		if (((*QuickMode_getSObject()).estadoActual != QMGame
				&& (*MisionMode_getSObject()).estadoActual != MMGame)
				&& (*Audio_getAudio("Galactic")).estado == playing)
		{

			Audio_stopAudio("MainMenu");

		}

		(*boton).actualframeB = 1;
		Boton_defineTrozoAnim(boton, (*boton).actualframeB * 64, 0, 64, 64);
	}

	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				if ((*Audio_getSObject()).efectosActivos == 1
						&& (*Audio_getSObject()).musicaActiva == 1)
				{
					if ((*QuickMode_getSObject()).estadoActual == QMGame
							|| (*MisionMode_getSObject()).estadoActual
									== MMGame)
					{
						Audio_stopAudio("Galactic");
					}
					if ((*QuickMode_getSObject()).estadoActual != QMGame
							&& (*MisionMode_getSObject()).estadoActual
									!= MMGame)
					{
						Audio_stopAudio("MainMenu");
					}
					(*boton).actualframeB = 1;
					(*Audio_getSObject()).efectosActivos = 0;
					(*Audio_getSObject()).musicaActiva = 0;
				}
				else
				{
					if ((*QuickMode_getSObject()).estadoActual != QMGame
							&& (*MisionMode_getSObject()).estadoActual
									!= MMGame)
					{
						Audio_playAudio("MainMenu");
					}
					if ((*QuickMode_getSObject()).estadoActual == QMGame
							|| (*MisionMode_getSObject()).estadoActual
									== MMGame)
					{
						if ((*Audio_getAudio("Galactic")).estado == stopped)
						{
							Audio_playAudio("Galactic");
						}
						else
						{
							Audio_playAudio("Galactic");
						}
					}
					(*boton).actualframeB = 0;
					(*Audio_getSObject()).efectosActivos = 1;
					(*Audio_getSObject()).musicaActiva = 1;
				}
				Almacenamiento_salvarOpciones();
			}
		}
	}
}

//Boton Exit Game
void Boton_verificaTouchBExitGame(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				if ((*Audio_getSObject()).musicaActiva == 1)
				{
					Audio_stopAudio("Galactic");
				}
				(*MainMenu_getSObject()).playMusic = 1;

				if ((*QuickMode_getSObject()).estadoActual == QMPause
						&& (*GalacticaMain_getObject()).estadoActual
								== GMQuickGame)
				{
					Almacenamiento_salvarInGame();
					(*QuickMode_getSObject()).estadoActual = QMMenu;
					(*GalacticaMain_getObject()).estadoActual = GMMainMenu;
					Nave_reiniciar();
					QuickModeGame_reiniciar();
				}
				if ((*MisionMode_getSObject()).estadoActual == MMPause
						&& (*GalacticaMain_getObject()).estadoActual
								== GMMisionMode)
				{
					(*MisionMode_getSObject()).estadoActual = MMMenu;
					(*GalacticaMain_getObject()).estadoActual = GMMainMenu;
					Nave_reiniciar();
					Boton_reiniciarMision();
				}
			}
		}
	}
}

//Boton Resume game
void Boton_verificaTouchBResumeGame(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				if ((*Audio_getSObject()).musicaActiva == 1)
				{
					Audio_playAudio("Galactic");
				}
				if ((*QuickMode_getSObject()).estadoActual == QMPause
						&& (*GalacticaMain_getObject()).estadoActual
								== GMQuickGame)
				{
					(*QuickModeGame_getSObject()).igb.ultimoTPlus =
							GameFramework_getTotalTime();
					(*QuickMode_getSObject()).estadoActual = QMGame;
				}
				if ((*MisionMode_getSObject()).estadoActual == MMPause
						&& (*GalacticaMain_getObject()).estadoActual
								== GMMisionMode)
				{
					(*MisionMode_getSObject()).estadoActual = MMGame;
				}
			}
		}
	}
}

//Boton Tilting
void Boton_verificaTouchBTilting(Boton* boton, int ancho, int alto)
{
	if ((*Acelerometro_getSObject()).acelActivo == 1)
	{
		(*boton).actualframeB = 0;
		Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	}
	else
	{
		(*boton).actualframeB = 1;
		Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	}
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				(*Acelerometro_getSObject()).acelActivo = 1;
				(*Control_getSObject()).controlActivo = 0;
				//Acelerometro.acelerometro.Start();
			}
		}
	}
}

//Boton Buttons
void Boton_verificaTouchBButtons(Boton* boton, int ancho, int alto)
{
	if ((*Control_getSObject()).controlActivo == 1)
	{
		(*boton).actualframeB = 0;
		Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	}
	else
	{
		(*boton).actualframeB = 1;
		Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	}
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				(*Acelerometro_getSObject()).acelActivo = 0;
				(*Control_getSObject()).controlActivo = 1;
				//Acelerometro.acelerometro.Stop();
			}
		}
	}
}

//Boton Medium
void Boton_verificaTouchBMedium(Boton* boton, int ancho, int alto)
{
	if ((*Control_getSObject()).tamanoGrande == 1)
	{
		(*boton).actualframeB = 1;
		Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	}
	else
	{
		(*boton).actualframeB = 0;
		Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	}
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				(*Control_getSObject()).tamanoGrande = 0;
			}
		}
	}
}

//Boton Large
void Boton_verificaTouchBLarge(Boton* boton, int ancho, int alto)
{
	if ((*Control_getSObject()).tamanoGrande == 1)
	{
		(*boton).actualframeB = 0;
		Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	}
	else
	{
		(*boton).actualframeB = 1;
		Boton_defineTrozoAnim(boton, 0, (*boton).actualframeB * 128, 256, 128);
	}
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				(*Control_getSObject()).tamanoGrande = 1;
			}
		}
	}
}

//Boton Quick Mode
void Boton_verificaTouchBQuickMode(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{

				Audio_playAudio("PresionarBoton");
				if (Almacenamiento_hayJuegoSalvado() == 1)
				{
					QuickModeMenu_reiniciar();
					(*GalacticaMain_getObject()).estadoActual = GMSavedGame;
				}
				else
				{
					QuickModeMenu_reiniciar();
					Almacenamiento_cargarBonus();
					(*GalacticaMain_getObject()).estadoActual = GMQuickGame;
				}
			}
		}
	}
}

//Boton About
void Boton_verificaTouchBAbout(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{

				Audio_playAudio("PresionarBoton");
				(*GalacticaMain_getObject()).estadoActual = GMAbout;
			}
		}
	}
}

//Boton Salir
void Boton_verificaTouchBSalir(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				Audio_stopAudio("MainMenu");
				(*GalacticaMain_getObject()).shutdown = 1;
			}
		}
	}
}

//Boton Opciones
void Boton_verificaTouchBOpciones(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				(*GalacticaMain_getObject()).estadoActual = GMOptions;
			}
		}
	}
}

//Boton Mision Mode
void Boton_verificaTouchBMisionMode(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				MisionModeMenu_cargarMisiones();
				(*GalacticaMain_getObject()).estadoActual = GMMisionMode;
			}
		}
	}
}

//Boton Help
void Boton_verificaTouchBHelp(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				(*GalacticaMain_getObject()).estadoActual = GMHelp;
			}
		}
	}
}

//Boton Mision
void Boton_verificaTouchBMision(Boton* boton, int ancho, int alto)
{
	Boton_defineTrozoAnim(boton, (*boton).actualframeB * 128, 0, 128, 128);
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				for (botonS.j = 0;
						botonS.j < (*MisionModeMenu_getSObject()).idMision.size;
						botonS.j++)
				{
					if ((*botonS.touched).position.X
							>= (*(Vector2*) Lista_RetornaElemento(
									&((*MisionModeMenu_getSObject()).idMision),
									botonS.j)).X
							&& (*botonS.touched).position.X
									<= (*(Vector2*) Lista_RetornaElemento(
											&((*MisionModeMenu_getSObject()).idMision),
											botonS.j)).X
											+ ((*boton).TrozoAnim.Width
													* (*boton).Escala.X)
							&& (*botonS.touched).position.Y
									>= (*(Vector2*) Lista_RetornaElemento(
											&((*MisionModeMenu_getSObject()).idMision),
											botonS.j)).Y
							&& (*botonS.touched).position.Y
									<= (*(Vector2*) Lista_RetornaElemento(
											&((*MisionModeMenu_getSObject()).idMision),
											botonS.j)).Y
											+ ((*boton).TrozoAnim.Height
													* (*boton).Escala.Y))
					{
						if ((*(Integer*) Lista_RetornaElemento(
								&((*MisionModeMenu_getSObject()).estaActivo),
								botonS.j)).num == 1)
						{
							botonS.indiceMision = botonS.j;
							break;
						}
						else
						{
							botonS.indiceMision = -1;
						}
					}
					else
					{
						botonS.indiceMision = -1;
					}
				}
				Boton_comprobarMensMision();
			}
		}
	}
}

//Boton Play
void Boton_verificaTouchBPlay(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				Audio_stopAudio("MainMenu");
				if ((*Audio_getSObject()).musicaActiva == 1)
				{
					Audio_playAudio("Galactic");
				}

				if ((*GalacticaMain_getObject()).estadoActual == GMQuickGame)
				{
					(*QuickMode_getSObject()).estadoActual = QMGame;
					QuickModeGame_reiniciar();
					Nave_reiniciar();
					(*QuickModeGOver_getSObject()).tEsperaActivaBoton = 1200000;
					(*QuickModeGame_getSObject()).igb.ultimoTPlus =
							GameFramework_getTotalTime();
					(*QuickModeGame_getSObject()).igb.tiempoTitileoMensaje =
							GameFramework_getTotalTime();
				}
				if ((*GalacticaMain_getObject()).estadoActual == GMMisionMode)
				{
					QuickModeGame_reiniciar();
					Boton_reiniciarMision();
					(*MisionMode_getSObject()).estadoActual = MMGame;
					(*MisionModeGOver_getSObject()).tEsperaActivaBoton =
							1200000;
					(*QuickModeGame_getSObject()).igb.tiempoTitileoMensaje =
							GameFramework_getTotalTime();
				}
			}
		}

	}
}

//Boton Flecha
void Boton_verificaTouchBFlecha(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*boton).flechaDerecha == 1)
				if ((*botonS.touched).estado == Released
						&& (*botonS.touched).position.X <= (*boton).Position.X
						&& (*botonS.touched).position.X
								>= (*boton).Position.X
										- ((*boton).TrozoAnim.Width
												* (*boton).Escala.X)
						&& (*botonS.touched).position.Y <= (*boton).Position.Y
						&& (*botonS.touched).position.Y
								>= (*boton).Position.Y
										- ((*boton).TrozoAnim.Height
												* (*boton).Escala.Y))
				{
					Audio_playAudio("PresionarBoton");
					if ((*QuickModeMenu_getSObject()).texturaNaveActual
							< botonS.navesActivas - 1)
					{
						(*Nave_getSObject()).TexturaNave =
								(*QuickModeMenu_getSObject()).texturaNaveActual
										+ 1;
						(*QuickModeMenu_getSObject()).texturaNaveActual =
								(*QuickModeMenu_getSObject()).texturaNaveActual
										+ 1;
					}
				}

			if ((*boton).flechaDerecha == 0)
				if ((*botonS.touched).estado == Released
						&& (*botonS.touched).position.X >= (*boton).Position.X
						&& (*botonS.touched).position.X
								<= (*boton).Position.X
										+ ((*boton).TrozoAnim.Width
												* (*boton).Escala.X)
						&& (*botonS.touched).position.Y >= (*boton).Position.Y
						&& (*botonS.touched).position.Y
								<= (*boton).Position.Y
										+ ((*boton).TrozoAnim.Height
												* (*boton).Escala.Y))
				{
					Audio_playAudio("PresionarBoton");
					if ((*QuickModeMenu_getSObject()).texturaNaveActual > 0)
					{
						(*Nave_getSObject()).TexturaNave =
								(*QuickModeMenu_getSObject()).texturaNaveActual
										- 1;
						(*QuickModeMenu_getSObject()).texturaNaveActual =
								(*QuickModeMenu_getSObject()).texturaNaveActual
										- 1;
					}
				}
		}
	}
}

//Boton Power
void Boton_verificaTouchBPower(Boton* boton, int ancho, int alto)
{
	if ((GameFramework_getTotalTime()
			> botonS.ultimaActualizacion + botonS.tiempoEsperaPoder)
			&& (*boton).bPoderActivo == 0
			&& (*QuickMode_getSObject()).estadoActual != QMPause
			&& (*Nave_getSObject()).poderActivo == 0)
	{
		(*boton).actualframeB = (*boton).actualframeB + 1;
		botonS.ultimaActualizacion = GameFramework_getTotalTime();
	}

	botonS.dato = botonS.dato + GameFramework_getSwapTime();
	if ((botonS.dato >= ((GameFramework_getSwapTime()) * 3))
			&& (*boton).bPoderActivo == 1)
	{
		(*boton).actualframeB = (*boton).actualframeB + 1;
		botonS.dato = 0;
	}

	if ((*boton).actualframeB >= 6 && (*boton).bPoderActivo == 0)
	{
		(*boton).actualframeB = 0;
		(*boton).lineaAnim = 128;
		(*boton).bPoderActivo = 1;
	}

	if ((*boton).actualframeB >= 6 && (*boton).bPoderActivo == 1)
	{
		(*boton).actualframeB = 0;
	}

	Boton_defineTrozoAnim(boton, 170.666 * (*boton).actualframeB,
			(*boton).lineaAnim, 170.666, 128);

	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y)
					&& (*boton).bPoderActivo == 1)
			{
				Audio_playAudio("InicioPoder");
				(*Nave_getSObject()).poderActivo = 1;
				Boton_reiniciaBPower(boton);
				(*Nave_getSObject()).tEsperaPGalactica = 50000;
			}
		}
	}
}

//Boton Continue
void Boton_verificaTouchBContinue(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				(*MainMenu_getSObject()).playMusic = 1;

				if ((*QuickMode_getSObject()).estadoActual == QMGameOver
						&& (*GalacticaMain_getObject()).estadoActual
								== GMQuickGame)
				{
					(*QuickMode_getSObject()).estadoActual = QMMenu;
					(*GalacticaMain_getObject()).estadoActual = GMMainMenu;
					Nave_reiniciar();
					QuickModeGame_reiniciar();
				}
				if ((*MisionMode_getSObject()).estadoActual == MMGameOver
						&& (*GalacticaMain_getObject()).estadoActual
								== GMMisionMode)
				{
					(*MisionMode_getSObject()).estadoActual = MMMenu;
					(*GalacticaMain_getObject()).estadoActual = GMMainMenu;
					Boton_reiniciarMision();
				}
			}
		}
	}
}

//Boton Back
void Boton_verificaTouchBBack(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");

				if ((*QuickMode_getSObject()).estadoActual == QMMenu
						&& (*GalacticaMain_getObject()).estadoActual
								== GMQuickGame)
				{
					(*GalacticaMain_getObject()).estadoActual = GMMainMenu;
				}
				else if ((*GalacticaMain_getObject()).estadoActual == GMOptions)
				{
					Almacenamiento_salvarOpciones();
					(*GalacticaMain_getObject()).estadoActual = GMMainMenu;
				}
				else if ((*GalacticaMain_getObject()).estadoActual == GMHelp)
				{
					(*GalacticaMain_getObject()).estadoActual = GMMainMenu;
				}
				else if ((*GalacticaMain_getObject()).estadoActual == GMAbout)
				{
					(*GalacticaMain_getObject()).estadoActual = GMMainMenu;
				}
				else if ((*GalacticaMain_getObject()).estadoActual
						== GMSavedGame)
				{
					(*GalacticaMain_getObject()).estadoActual = GMMainMenu;
				}
				else if ((*MisionMode_getSObject()).estadoActual == MMMenu
						&& (*GalacticaMain_getObject()).estadoActual
								== GMMisionMode)
				{
					(*GalacticaMain_getObject()).estadoActual = GMMainMenu;
				}
				else if ((*MisionMode_getSObject()).estadoActual == MMInfo
						&& (*GalacticaMain_getObject()).estadoActual
								== GMMisionMode)
				{
					(*MisionMode_getSObject()).estadoActual = MMMenu;
					MisionModeMenu_cargarMisiones();
				}
			}
		}
	}
}

//Boton Next Mision
void Boton_verificaTouchBNextMision(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				Audio_playAudio("MainMenu");
				(*MainMenu_getSObject()).playMusic = 1;
				Boton_reiniciarMision();
				botonS.indiceMision = botonS.indiceMision + 1;
				Boton_comprobarMensMision();
			}
		}
	}
}

//Boton Restart
void Boton_verificaTouchBRestart(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				if ((*Audio_getSObject()).musicaActiva == 1)
				{
					Audio_playAudio("Galactic");
				}

				if ((*QuickMode_getSObject()).estadoActual == QMGameOver
						&& (*GalacticaMain_getObject()).estadoActual
								== GMQuickGame)
				{
					Nave_reiniciar();
					QuickModeGame_reiniciar();
					(*QuickModeGame_getSObject()).igb.ultimoTPlus =
							GameFramework_getTotalTime();
					(*QuickModeGOver_getSObject()).tEsperaActivaBoton = 1200000;
					(*QuickModeGame_getSObject()).igb.tiempoTitileoMensaje =
							GameFramework_getTotalTime();
					(*QuickMode_getSObject()).estadoActual = QMGame;
				}
				if ((*MisionMode_getSObject()).estadoActual == MMGameOver
						&& (*GalacticaMain_getObject()).estadoActual
								== GMMisionMode)
				{
					Boton_reiniciarMision();
					(*MisionModeGOver_getSObject()).tEsperaActivaBoton =
							1200000;
					(*MisionMode_getSObject()).estadoActual = MMGame;
					(*QuickModeGame_getSObject()).igb.tiempoTitileoMensaje =
							GameFramework_getTotalTime();
				}

			}
		}
	}
}

//Boton ContinueGame
void Boton_verificaTouchBContinueGame(Boton* boton, int ancho, int alto)
{

	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				QuickModeGame_reiniciar();
				Nave_reiniciar();
				Almacenamiento_cargarBonus();
				Almacenamiento_cargarInGame();
				(*QuickModeGame_getSObject()).igb.tiempoTitileoMensaje =
						GameFramework_getTotalTime();
				(*QuickModeGame_getSObject()).igb.ultimoTPlus =
						GameFramework_getTotalTime();
				(*QuickMode_getSObject()).estadoActual = QMGame;
				(*GalacticaMain_getObject()).estadoActual = GMQuickGame;
				if ((*Audio_getSObject()).musicaActiva == 1)
				{
					Audio_stopAudio("MainMenu");
					Audio_playAudio("Galactic");
				}
			}
		}
	}
}

//Boton NewGame
void Boton_verificaTouchBNewGame(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				Almacenamiento_borrarInGame();
				QuickModeGame_reiniciar();
				Nave_reiniciar();
				Almacenamiento_cargarBonus();
				(*GalacticaMain_getObject()).estadoActual = GMQuickGame;
				(*QuickMode_getSObject()).estadoActual = QMMenu;
			}
		}
	}
}

//Boton Facebook
void Boton_verificaTouchBFacebook(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				navigator_invoke("https://www.facebook.com/jarts301", NULL);
			}
		}
	}
}

//Boton Twitter
void Boton_verificaTouchBTwitter(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				navigator_invoke("https://twitter.com/jarts301", NULL);
			}
		}
	}
}

//Boton Comentario
void Boton_verificaTouchBComent(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				navigator_invoke("appworld://content/37179894", NULL);
			}
		}
	}
}

//Boton GoBuy
void Boton_verificaTouchBGoBuy(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				//Guide.ShowMarketplace(PlayerIndex.One);
			}
		}
	}
}

//Boton GoBuy
void Boton_verificaTouchBNotNow(Boton* boton, int ancho, int alto)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (botonS.i = 0; botonS.i < (*TouchControl_getTouches()).size;
				botonS.i++)
		{
			botonS.touched = TouchControl_getATouch(botonS.i);
			if ((*botonS.touched).estado == Released
					&& (*botonS.touched).position.X >= (*boton).Position.X
					&& (*botonS.touched).position.X
							<= (*boton).Position.X
									+ ((*boton).TrozoAnim.Width
											* (*boton).Escala.X)
					&& (*botonS.touched).position.Y >= (*boton).Position.Y
					&& (*botonS.touched).position.Y
							<= (*boton).Position.Y
									+ ((*boton).TrozoAnim.Height
											* (*boton).Escala.Y))
			{
				Audio_playAudio("PresionarBoton");
				if ((*QuickMode_getSObject()).estadoActual == QMGame)
				{
					(*GalacticaMain_getObject()).estadoActual = GMMainMenu;
					(*QuickMode_getSObject()).estadoActual = QMMenu;
					Nave_reiniciar();
					QuickModeGame_reiniciar();
				}
				else
				{
					(*GalacticaMain_getObject()).estadoActual = GMMisionMode;
					(*MisionMode_getSObject()).estadoActual = MMMenu;
					MisionModeMenu_cargarMisiones();
				}
			}
		}
	}
}

//**************Demas metodos***************

void Boton_reiniciaBPower(Boton* boton)
{
	(*boton).actualframeB = 0;
	(*boton).lineaAnim = 0;
	(*boton).bPoderActivo = 0;
	(*boton).ultimaActualizacion = 0;
}

void Boton_defineTrozoAnim(Boton* boton, double x, double y, double width,
		double height)
{
	(*boton).TrozoAnim.X = x;
	(*boton).TrozoAnim.Y = y;
	(*boton).TrozoAnim.Width = width;
	(*boton).TrozoAnim.Height = height;
}

void Boton_setPosition(Boton* boton, int x, int y)
{
	(*boton).Position.X = x;
	(*boton).Position.Y = y;
}
/*
 public void setOrigen(int x, int y)
 {
 Position.X = x;
 Position.Y = y;
 }
 */
void Boton_setTexturas()
{
	Lista_Inicia(&(botonS.textura));
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BQuickMode")); //0
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("PauseBoton")); //1
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BResumeGame")); //2
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BExitGame")); //3
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BFlecha")); //4
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BPlay")); //5
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BPower")); //6
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BContinue")); //7
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BMision")); //8
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BMisionMode")); //9
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BBack")); //10
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BNextMision")); //11
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BRestart")); //12
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BAudio")); //13
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BOpciones")); //14
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BSalir")); //15
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BTilting")); //16
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BButtons")); //17
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BMedium")); //18
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BLarge")); //19
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BHelp")); //20
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BAbout")); //21
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BContinueGame")); //22
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BNewGame")); //23
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BFacebook")); //24
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BComent")); //25
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BGoBuy")); //26
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BNotNow")); //27
	Lista_InsertaEnFinal(&(botonS.textura), botonS.textura.fin,
			Textura_getTextura("BTwitter")); //28
}

//****Reiniciar Mision
void Boton_reiniciarMision()
{
	if (botonS.indiceMision != -1)
	{
		Nave_reiniciar();
		switch ((*MisionModeGame_getSObjec()).numMision)
		{
		case 1:
			Mision1_reiniciar();
			break;
		case 2:
			Mision2_reiniciar();
			break;
		case 3:
			Mision3_reiniciar();
			break;
		case 4:
			Mision4_reiniciar();
			break;
		case 5:
			Mision5_reiniciar();
			break;
		case 6:
			Mision6_reiniciar();
			break;
		case 7:
			Mision7_reiniciar();
			break;
		case 8:
			Mision8_reiniciar();
			break;
		case 9:
			Mision9_reiniciar();
			break;
		case 10:
			Mision10_reiniciar();
			break;
		case 11:
			Mision11_reiniciar();
			break;
		case 12:
			Mision12_reiniciar();
			break;
		case 13:
			Mision13_reiniciar();
			break;
		case 14:
			Mision14_reiniciar();
			break;
		case 15:
			Mision15_reiniciar();
			break;
		case 16:
			Mision16_reiniciar();
			break;
		case 17:
			Mision17_reiniciar();
			break;
		case 18:
			Mision18_reiniciar();
			break;
		case 19:
			Mision19_reiniciar();
			break;
		case 20:
			Mision20_reiniciar();
			break;
		}
	}
}

void Boton_comprobarMensMision()
{

	(*MisionModeGame_getSObjec()).numMision = botonS.indiceMision + 1;

	if (botonS.indiceMision != -1)
	{
		switch ((*MisionModeGame_getSObjec()).numMision)
		{
		case 1:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision1_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision1_getSObject()).tituloMision;
			break;
		case 2:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision2_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision2_getSObject()).tituloMision;
			break;
		case 3:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision3_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision3_getSObject()).tituloMision;
			break;
		case 4:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision4_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision4_getSObject()).tituloMision;
			break;
		case 5:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision5_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision5_getSObject()).tituloMision;
			break;
		case 6:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision6_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision6_getSObject()).tituloMision;
			break;
		case 7:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision7_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision7_getSObject()).tituloMision;
			break;
		case 8:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision8_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision8_getSObject()).tituloMision;
			break;
		case 9:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision9_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision9_getSObject()).tituloMision;
			break;
		case 10:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision10_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision10_getSObject()).tituloMision;
			break;
		case 11:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision11_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision11_getSObject()).tituloMision;
			break;
		case 12:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision12_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision12_getSObject()).tituloMision;
			break;
		case 13:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision13_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision13_getSObject()).tituloMision;
			break;
		case 14:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision14_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision14_getSObject()).tituloMision;
			break;
		case 15:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision15_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision15_getSObject()).tituloMision;
			break;
		case 16:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision16_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision16_getSObject()).tituloMision;
			break;
		case 17:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision17_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision17_getSObject()).tituloMision;
			break;
		case 18:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision18_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision18_getSObject()).tituloMision;
			break;
		case 19:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision19_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision19_getSObject()).tituloMision;
			break;
		case 20:
			(*MisionMode_getSObject()).estadoActual = MMInfo;
			(*MisionModeInfo_getSObject()).InfoMision =
					&(*Mision20_getSObject()).mensajeMision;
			(*MisionModeInfo_getSObject()).TituloMision =
					(*Mision20_getSObject()).tituloMision;
			break;
		}
	}
}

Boton* Boton_getSObject()
{
	return &botonS;
}

