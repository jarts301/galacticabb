/*
 * Control.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef CONTROL_H_
#define CONTROL_H_

#include "GalacticaTipos.h"

Control* Control_creaObjecto(Vector2 pos);
void Control_load();
void Control_update(Control* control);
void Control_draw(Control* control);
void Control_defineTrozoAnim(Control* control, int x, int y, int width,
		int height);
void Control_setPosition(Control* control, Vector2 pos);
int Control_estaSobre(Control* control);
void Control_procesaMovimiento(Control* control);
Control* Control_getSObject();
void Control_setOrigen(Control* control);

#endif /* CONTROL_H_ */
