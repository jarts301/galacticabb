/*
 * AcercaDe.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef ACERCADE_H_
#define ACERCADE_H_

#include "GalacticaTipos.h"

void AcercaDe_setTexturas();
void AcercaDe_draw();
void AcercaDe_update();
void AcercaDe_load();
void AcercaDe_drawTitulo();
void AcercaDe_loadTitulo();

#endif /* ACERCADE_H_ */
