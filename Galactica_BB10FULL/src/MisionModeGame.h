/*
 * MisionModeGame.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISIONMODEGAME_H_
#define MISIONMODEGAME_H_

#include "GalacticaTipos.h"

void MisionModeGame_load();
void MisionModeGame_update();
void MisionModeGame_draw();
MisionModeGame* MisionModeGame_getSObjec();

#endif /* MISIONMODEGAME_H_ */
