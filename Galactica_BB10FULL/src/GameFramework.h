/*
 * GameFramework.h
 *
 *  Created on: 31/01/2013
 *      Author: Jarts
 */

#ifndef GAMEFRAMEWORK_H_
#define GAMEFRAMEWORK_H_

#include <time.h>
#include <stdarg.h>

#define PI 3.141592654

typedef struct{
      double swapTime;
      double startTime;
      double totalTime;
}GameTime;

typedef struct{
      double X;
      double Y;
}Vector2;

typedef struct{
      float X;
      float Y;
      float Z;
}Vector3;

typedef struct{
	double X;
	double Y;
	double Width;
	double Height;
}Rectangle;

typedef struct{
	float R;
	float G;
	float B;
	float A;
}Color;

typedef struct{
	int num;
}Integer;

void GameFramework_setStartTime();
void GameFramework_updateTotalTime();
void GameFramework_setSwapTime();
double GameFramework_getTotalTime();
double GameFramework_getSwapTime();
Color GameFramework_Color(float r, float g, float b, float a);
void GameFramework_ColorTo(Color* color,float r, float g, float b, float a);
float GameFramework_getByteColorToFloatColor(int bytes);
int GameFramework_Rand(int min,int max);
double GameFramework_Rand0A1();
Vector2 GameFramework_Vector2(float x, float y);
Vector2* GameFramework_Vector2Crear(float x, float y);
double GameFramework_ToRadians(double dat);
double GameFramework_ToDegrees(double dat);
int* GameFramework_creaInteger(int numero);
char* GameFramework_EnteroACadena(int i);
char* GameFramework_Concatenar(int numCads, ...);
int GameFramework_Vector2IgualA(Vector2 vectorA, Vector2 vectorB);

#endif /* GAMEFRAMEWORK_H_ */
