/*
 * QuickMode.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef QUICKMODE_H_
#define QUICKMODE_H_

#include "GalacticaTipos.h"

void QuickMode_load();
void QuickMode_update();
void QuickMode_draw();
QuickMode* QuickMode_getSObject();

#endif /* QUICKMODE_H_ */
