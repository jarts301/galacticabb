/*
 * MisionModeInfo.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "MisionModeInfo.h"
#include "Lista.h"
#include "Boton.h"
#include "GameComun.h"
#include "Textura.h"
#include "Dibujar.h"

static MisionModeInfo MisionModeInfoS;

void MisionModeInfo_load()
{
	MisionModeInfo_setTexturas();
	MisionModeInfo_loadTitulo();
	Lista_Inicia(&MisionModeInfoS.botones);
	Lista_AddInFin(&MisionModeInfoS.botones, Boton_Crear(BPlay, 290, 10));
	Lista_AddInFin(&MisionModeInfoS.botones, Boton_Crear(BBack, 10, 10));
}

void MisionModeInfo_update()
{
	GameComun_updateEstrellas();
	for (MisionModeInfoS.i = 0;
			MisionModeInfoS.i < MisionModeInfoS.botones.size;
			MisionModeInfoS.i++)
	{
		Boton_update(
				(Boton*) Lista_GetIn(&MisionModeInfoS.botones,
						MisionModeInfoS.i));
	}
}

void MisionModeInfo_draw()
{
	Dibujar_drawSprite(Textura_getTexturaFondo(0), GameFramework_Vector2(0, 0),
			GameFramework_Color(0, 0, 0, 1), 0, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(40, 40));

	GameComun_drawEstrellas();
	MisionModeInfo_drawTitulo();
	for (MisionModeInfoS.i = 0;
			MisionModeInfoS.i < MisionModeInfoS.botones.size;
			MisionModeInfoS.i++)
	{
		Boton_draw(
				(Boton*) Lista_GetIn(&MisionModeInfoS.botones,
						MisionModeInfoS.i));
	}

	for (MisionModeInfoS.z = 0;
			MisionModeInfoS.z < (*MisionModeInfoS.InfoMision).size;
			MisionModeInfoS.z++)
	{
		MisionModeInfoS.positionInfo = GameFramework_Vector2(8, 530);
		MisionModeInfoS.positionInfo.Y = MisionModeInfoS.positionInfo.Y
				- (30*(MisionModeInfoS.z + 1));

		Dibujar_drawText((*Textura_getSObject()).Fuente,
				(char*) Lista_GetIn(MisionModeInfoS.InfoMision,
						MisionModeInfoS.z), MisionModeInfoS.positionInfo,
				GameFramework_Color(1, 1, 1, 1), 1.0);
	}

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			MisionModeInfoS.TituloMision, MisionModeInfoS.positionTMision,
			GameFramework_Color(GameFramework_getByteColorToFloatColor(7),
					GameFramework_getByteColorToFloatColor(237),
					GameFramework_getByteColorToFloatColor(134), 1), 1.8);

}

//******************************************

//******TITULO***********
void MisionModeInfo_loadTitulo()
{
	MisionModeInfoS.positionTitulo = GameFramework_Vector2(300, 930);
	MisionModeInfoS.origenTitulo = GameFramework_Vector2(
			(*(TexturaObjeto*) Lista_GetIn(&MisionModeInfoS.textura, 0)).ancho
					/ 2,
			(*(TexturaObjeto*) Lista_GetIn(&MisionModeInfoS.textura, 0)).alto
					/ 2);
	MisionModeInfoS.positionSubT = GameFramework_Vector2(300, 400);
	MisionModeInfoS.origenSubT = GameFramework_Vector2(
			(*(TexturaObjeto*) Lista_GetIn(&MisionModeInfoS.textura, 1)).ancho
					/ 2,
			(*(TexturaObjeto*) Lista_GetIn(&MisionModeInfoS.textura, 1)).ancho
					/ 2);
	MisionModeInfoS.positionInfo = GameFramework_Vector2(8, 530);
	MisionModeInfoS.positionImagen = GameFramework_Vector2(15, 640);
	MisionModeInfoS.positionTMision = GameFramework_Vector2(230, 760);
}

void MisionModeInfo_drawTitulo()
{
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_RetornaElemento(&(MisionModeInfoS.textura),
					0), MisionModeInfoS.positionTitulo,
			GameFramework_Color(1, 1, 1, 1), 0.0f, MisionModeInfoS.origenTitulo,
			GameFramework_Vector2(1.0, 1.0));
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_RetornaElemento(&(MisionModeInfoS.textura),
					1), MisionModeInfoS.positionSubT,
			GameFramework_Color(1, 1, 1, 1), 0.0f, MisionModeInfoS.origenSubT,
			GameFramework_Vector2(1.35, 1.0));
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_RetornaElemento(&(MisionModeInfoS.textura),
					2), MisionModeInfoS.positionImagen,
			GameFramework_Color(1, 1, 1, 1), 0.0f, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(0.62, 0.45));
}

//*********Texturas***************
void MisionModeInfo_setTexturas()
{
	Lista_Inicia(&MisionModeInfoS.textura);
	Lista_AddInFin(&MisionModeInfoS.textura,
			Textura_getTextura("TituloMisionMode")); //0
	Lista_AddInFin(&MisionModeInfoS.textura, Textura_getTextura("TInfo")); //1
	Lista_AddInFin(&MisionModeInfoS.textura, Textura_getTextura("Capitana")); //2
}

MisionModeInfo* MisionModeInfo_getSObject()
{
	return &MisionModeInfoS;
}
