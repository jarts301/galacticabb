#include <ctype.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/keycodes.h>
#include <screen/screen.h>
#include <assert.h>
#include <bps/sensor.h>
#include <bps/navigator.h>
#include <bps/screen.h>
#include <bps/bps.h>
#include <bps/event.h>
#include <bps/orientation.h>
#include <math.h>
#include <time.h>
#include <screen/screen.h>
#include <EGL/egl.h>
#include <GLES/gl.h>

#include "GalacticaMain.h"
#include "Textura.h"
#include "bbutil.h"
#include "GameFramework.h"
#include "Acelerometro.h"
#include "Lista.h"
#include "String.h"
#include "Dibujar.h"
#include "TouchControl.h"
#include "Audio.h"
#include "Almacenamiento.h"
#include "AcercaDe.h"
#include <sys/strm.h>
#include <sys/stat.h>
#include <mm/renderer.h>
#include <bps/mmrenderer.h>

//static bool shutdown;
static int orientation_angle;
static screen_context_t screen_cxt;
//static float width, height;
int frames = 0, posx = 0, posy = 0, pos2x, pos2y;
int ultimaActua = 0, contadorFrame = 0;
clock_t start_time, end_time;
//char* bufer3;
struct timespec tp1, *tp2;
int exit_application = 0;
int paused = 0, k;
//Lista miLista;
font_t* fuente;
int ancho = 64, alto = 64;
float tex = 0, texy = 0;
int setOrigen = 1;
char* mensaje = " ";
char bufer3[100], bufer[100];
Vector2 position, position2, position3;
float escala;
Color color;
bps_event_t *event = NULL;
const char* ctxPosition;

char* eventNombre;
char* musicaNombre;
mmrenderer_state_t eventEstado;
int z;

int load()
{
	EGLint surface_width, surface_height;
	int dpi = bbutil_calculate_dpi(screen_cxt);

	//int point_size = (int)(15.0f / ((float)dpi / 170.0f ));

	(*Textura_getSObject()).Fuente = bbutil_load_font(
			"app/native/assets/ZeroHour.ttf", 6, dpi);
	//Query width and height of the window surface created by utility code
	eglQuerySurface(egl_disp, egl_surf, EGL_WIDTH, &surface_width);
	eglQuerySurface(egl_disp, egl_surf, EGL_HEIGHT, &surface_height);

	EGLint err = eglGetError();
	if (err != 0x3000)
	{
		fprintf(stderr, "Unable to query EGL surface dimensions\n");
		return EXIT_FAILURE;
	}

	(*GalacticaMain_getObject()).Wwidth = (float) surface_width;
	(*GalacticaMain_getObject()).Wheight = (float) surface_height;

	//Initialize GL for 2D rendering
	glViewport(0, 0, (int) (*GalacticaMain_getObject()).Wwidth,
			(int) (*GalacticaMain_getObject()).Wheight);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrthof(0.0f, (*GalacticaMain_getObject()).Wwidth, 0.0f,
			(*GalacticaMain_getObject()).Wheight, 1.0f, -1.0f);
	//glOrthof(0.0f, width / height, 0.0f, 1.0f, -1.0f, 1.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//Set world coordinates to coincide with screen pixels
	glScalef(1.0f / (float) (*GalacticaMain_getObject()).Wheight,
			1.0f / (float) (*GalacticaMain_getObject()).Wheight, 1.0f);

	TouchControl_Inicia();
	GalacticaMain_Load();

	return EXIT_SUCCESS;
}

static void update()
{
	GameFramework_updateTotalTime();
	//sprintf(bufer,"%ld",(long)getSwapTime());
	GalacticaMain_Update();
}

Vector2 po, o, es;
Rectangle tA;

void draw()
{
	//contadorFrame++;
	//Typical rendering pass
	GalacticaMain_Draw();

	GameFramework_setSwapTime();
	bbutil_swap(); //Use utility code to update the screen
}

static void handleScreenEvent(bps_event_t *event)
{

	int screen_val, toucheFind, t;
	int positionVal[2];
	int idenVal;
	//int buttons;
	//static bool mouse_pressed = false;
	Vector2 posToPass;
	Touche* toqueAEv;
	touchEvento evento = Nada;
	toucheFind = 0;

	screen_event_t screen_event = screen_event_get_event(event);

	screen_get_event_property_iv(screen_event, SCREEN_PROPERTY_TYPE,
			&screen_val);
	screen_get_event_property_iv(screen_event, SCREEN_PROPERTY_SOURCE_POSITION,
			positionVal);
	screen_get_event_property_iv(screen_event, SCREEN_PROPERTY_TOUCH_ID,
			&idenVal);

	posToPass.X = positionVal[0];
	posToPass.Y = positionVal[1];

	if (screen_val == SCREEN_EVENT_MTOUCH_TOUCH)
	{
		evento = Pressed;
	}
	else if (screen_val == SCREEN_EVENT_MTOUCH_MOVE)
	{
		evento = Moved;
	}
	else if (screen_val == SCREEN_EVENT_MTOUCH_RELEASE)
	{
		evento = Released;
	}
	else
	{
		evento = Nada;
	}

	for (t = 0; t < (*TouchControl_getTouches()).size; t++)
	{
		toqueAEv = TouchControl_getATouch(t);
		if ((*toqueAEv).identifi == idenVal)
		{
			if (evento == Pressed)
			{
				(*toqueAEv).estado = Moved;
			}
			else
			{
				(*toqueAEv).estado = evento;
			}
			(*toqueAEv).position.X = Touch_transformaPosX(positionVal[0]);
			(*toqueAEv).position.Y = Touch_transformaPosY(
					(*GalacticaMain_getObject()).Wheight - positionVal[1]);
			toucheFind = 1;
			break;
		}
		toucheFind = 0;
	}

	if (toucheFind == 0 && evento != Nada && idenVal <= 3)
	{
		TouchControl_actualizaTouch(evento, posToPass, idenVal);
	}
}

static void handleNavigatorEvent(bps_event_t *event)
{
	switch (bps_event_get_code(event))
	{
	case NAVIGATOR_SWIPE_DOWN:

		break;
	case NAVIGATOR_EXIT:
		(*GalacticaMain_getObject()).shutdown = 1;
		break;
	}
}

static void handleSensorEvent(bps_event_t *event)
{
	if (event)
	{
		if (bps_event_get_domain(event) == navigator_get_domain())
		{
			/*
			 * If it is a NAVIGATOR_EXIT event then set the
			 * exit_application flag so the application will stop
			 * processing events, clean up and exit.
			 *
			 * If it is a NAVIGATOR_WINDOW_STATE event, and the window
			 * state is NAVIGATOR_WINDOW_INVISIBLE, set paused to true so
			 * that we stop collecting accelerometer data. Otherwise, if
			 * we're currently paused, then set paused back to false to
			 * resume collecting accelerometer data.
			 */
			unsigned int event_code = bps_event_get_code(event);
			if (NAVIGATOR_EXIT == event_code)
			{
				exit_application = 1;
			}
			else if (NAVIGATOR_WINDOW_STATE == event_code)
			{
				if (navigator_event_get_window_state(event)
						== NAVIGATOR_WINDOW_FULLSCREEN)
				{
					/*
					 * The application is now full screen.
					 * Resume reading the accelerometer.
					 */
					paused = 0;
					sensor_request_events(SENSOR_TYPE_ACCELEROMETER);
					//snprintf(msg, MSG_SIZE, "Resuming accelerometer reads.\n");
				}
				else if (!paused)
				{
					/*
					 * The application has become thumbnailed or invisible.
					 * If it is not already paused, then pause it.
					 * Otherwise, ignore the window state change.
					 */
					paused = 1;
					sensor_stop_events(SENSOR_TYPE_ACCELEROMETER);
					//snprintf(msg, MSG_SIZE, "Pausing accelerometer reads.\n");
				}
			}
		}
		else if (bps_event_get_domain(event) == sensor_get_domain())
		{
			if (SENSOR_ACCELEROMETER_READING == bps_event_get_code(event))
			{
				/*
				 * Extract the accelerometer data from the event and
				 * display it.
				 */
				sensor_event_get_xyz(event,
						&((*Acelerometro_getSObject()).AcelerometroAcelerDat.X),
						&((*Acelerometro_getSObject()).AcelerometroAcelerDat.Y),
						&((*Acelerometro_getSObject()).AcelerometroAcelerDat.Z));
			}
		}
	}

	/*if (SENSOR_ACCELEROMETER_READING == bps_event_get_code(event))
	 {
	 sensor_request_events(SENSOR_TYPE_ACCELEROMETER);
	 //sensor_stop_events(SENSOR_TYPE_ACCELEROMETER);
	 sensor_event_get_xyz(event,
	 &((*Acelerometro_getSObject()).AcelerometroAcelerDat.X),
	 &((*Acelerometro_getSObject()).AcelerometroAcelerDat.Y),
	 &((*Acelerometro_getSObject()).AcelerometroAcelerDat.Z));
	 }*/

	if (SENSOR_AZIMUTH_PITCH_ROLL_READING == bps_event_get_code(event))
	{
		float azimuth, pitch, roll;
		float result_x = 0.0f, result_y = -1.0f;

		sensor_event_get_apr(event, &azimuth, &pitch, &roll);

		float radians = abs(roll) * M_PI / 180;
		float horizontal = sin(radians) * 0.5f;
		float vertical = cos(radians) * 0.5f;

		if (pitch < 0)
		{
			vertical = -vertical;
		}
		if (roll >= 0)
		{
			horizontal = -horizontal;
		}

		//Account for axis change due to different starting orientations
		if (orientation_angle == 0)
		{
			result_x = horizontal;
			result_y = vertical;
		}
		else if (orientation_angle == 90)
		{
			result_x = -vertical;
			result_y = horizontal;
		}
		else if (orientation_angle == 180)
		{
			result_x = -horizontal;
			result_y = -vertical;
		}
		else if (orientation_angle == 270)
		{
			result_x = vertical;
			result_y = -horizontal;
		}
	}
}

static void handleMmrendererEvent(bps_event_t *event)
{
	eventEstado = mmrenderer_event_get_state(event);
	eventNombre = (char*) ((void*) mmrenderer_event_get_userdata(event));

	for (z = 0; z < (*Audio_getListaNombreMusica()).size; z++)
	{
		musicaNombre = (char*) Lista_RetornaElemento(
				Audio_getListaNombreMusica(), z);
		if (strcmp(musicaNombre, eventNombre) == 0)
		{
			if (eventEstado == MMR_DESTROYED)
			{
				(*(Musica*) Lista_RetornaElemento(Audio_getListaMusica(), z)).estado =
						destroyed;
			}
			else if (eventEstado == MMR_IDLE)
			{
				(*(Musica*) Lista_RetornaElemento(Audio_getListaMusica(), z)).estado =
						idle;
			}
			else if (eventEstado == MMR_STOPPED)
			{
				(*(Musica*) Lista_RetornaElemento(Audio_getListaMusica(), z)).estado =
						stopped;
			}
			else if (eventEstado == MMR_PLAYING)
			{
				(*(Musica*) Lista_RetornaElemento(Audio_getListaMusica(), z)).estado =
						playing;
			}
			break;
		}
	}

}

static void handle_events()
{
	int screen_domain = screen_get_domain();
	int navigator_domain = navigator_get_domain();
	int sensor_domain = sensor_get_domain();
	int mmrenderer_domain = mmrenderer_get_domain();

	TouchControl_Clear();

	int rc;

	//Request and process available BPS events
	for (;;)
	{
		bps_event_t *event = NULL;
		rc = bps_get_event(&event, 0);
		assert(rc == BPS_SUCCESS);

		if (event)
		{
			int domain = bps_event_get_domain(event);

			if (domain == mmrenderer_domain)
			{
				handleMmrendererEvent(event);
			}
			else if (domain == screen_domain)
			{
				//TouchControl_addEvent(event);
				handleScreenEvent(event);
			}
			else if (domain == navigator_domain)
			{
				handleNavigatorEvent(event);
			}
			else if (domain == sensor_domain)
			{
				handleSensorEvent(event);
			}
		}
		else
		{
			//No more events in the queue
			break;
		}
	}
}

int main(int argc, char **argv)
{

	start_time = clock();
	(*GalacticaMain_getObject()).shutdown = 0;
	GameFramework_setStartTime();

	//Create a screen context that will be used to create an EGL surface to to receive libscreen events
	screen_create_context(&screen_cxt, 0);

	//Initialize BPS library
	bps_initialize();

	//Determine initial orientation angle
	orientation_direction_t direction;
	orientation_get(&direction, &orientation_angle);

	//Use utility code to initialize EGL for rendering with GL ES 1.1
	if (EXIT_SUCCESS != bbutil_init_egl(screen_cxt))
	{
		fprintf(stderr, "bbutil_init_egl failed\n");
		bbutil_terminate();
		screen_destroy_context(screen_cxt);
		return 0;
	}

	//Initialize application logic
	if (EXIT_SUCCESS != load())
	{
		fprintf(stderr, "initialize failed\n");
		bbutil_terminate();
		screen_destroy_context(screen_cxt);
		return 0;
	}

	//Signal BPS library that navigator and screen events will be requested
	if (BPS_SUCCESS != screen_request_events(screen_cxt))
	{
		fprintf(stderr, "screen_request_events failed\n");
		bbutil_terminate();
		screen_destroy_context(screen_cxt);
		return 0;
	}

	if (BPS_SUCCESS != navigator_request_events(0))
	{
		fprintf(stderr, "navigator_request_events failed\n");
		bbutil_terminate();
		screen_destroy_context(screen_cxt);
		return 0;
	}

	//Signal BPS library that navigator orientation is not to be locked
	if (BPS_SUCCESS != navigator_rotation_lock(true))
	{
		fprintf(stderr, "navigator_rotation_lock failed\n");
		bbutil_terminate();
		screen_destroy_context(screen_cxt);
		return 0;
	}

	//Setup Sensors
	if (sensor_is_supported(SENSOR_TYPE_AZIMUTH_PITCH_ROLL))
	{
		//Microseconds between sensor reads. This is the rate at which the
		//sensor data will be updated from hardware. The hardware update
		//rate is set below using sensor_set_rate.
		static const int SENSOR_RATE = 25000;

		//Initialize the sensor by setting the rates at which the
		//sensor values will be updated from hardware
		sensor_set_rate(SENSOR_TYPE_AZIMUTH_PITCH_ROLL, SENSOR_RATE);

		sensor_set_skip_duplicates(SENSOR_TYPE_AZIMUTH_PITCH_ROLL, true);
		sensor_request_events(SENSOR_TYPE_AZIMUTH_PITCH_ROLL);
	}

	//Setup Acelerometer
	if (sensor_is_supported(SENSOR_TYPE_ACCELEROMETER))
	{
		//Microseconds between sensor reads. This is the rate at which the
		//sensor data will be updated from hardware. The hardware update
		//rate is set below using sensor_set_rate.
		static const int SENSOR_RATE = 25000;
		//Initialize the sensor by setting the rates at which the
		//sensor values will be updated from hardware
		sensor_set_rate(SENSOR_TYPE_ACCELEROMETER, SENSOR_RATE);
		sensor_set_skip_duplicates(SENSOR_TYPE_ACCELEROMETER, true);
		sensor_request_events(SENSOR_TYPE_ACCELEROMETER);
	}

	//bbutil_rotate_screen_surface(270);
	while ((*GalacticaMain_getObject()).shutdown == 0)
	{
		// Handle user input and sensors
		handle_events();
		//update fuction
		update();
		draw();
	}
	//Stop requesting events from libscreen
	screen_stop_events(screen_cxt);

	//Shut down BPS library for this process
	bps_shutdown();

	//Use utility code to terminate EGL setup
	bbutil_terminate();

	//Destroy libscreen context
	screen_destroy_context(screen_cxt);
	return 0;
}

/*int screen_val, toucheFind, t;
 int positionVal[2];
 int idenVal = 0;
 int buttons;
 static bool mouse_pressed = false;
 Vector2 posToPass;
 Touche* toqueAEv;
 touchEvento evento = Nada;
 toucheFind = 0;

 screen_event_t screen_event = screen_event_get_event(event);

 screen_get_event_property_iv(screen_event, SCREEN_PROPERTY_TYPE,
 &screen_val);
 screen_get_event_property_iv(screen_event, SCREEN_PROPERTY_SOURCE_POSITION,
 positionVal);
 //screen_get_event_property_iv(screen_event, SCREEN_PROPERTY_TOUCH_ID,
 //&idenVal);

 posToPass.X = positionVal[0];
 posToPass.Y = positionVal[1];

 if (screen_val == SCREEN_EVENT_POINTER)
 {
 //This is a mouse move event, it is applicable to a device with a usb mouse or simulator
 screen_get_event_property_iv(screen_event, SCREEN_PROPERTY_BUTTONS,
 &buttons);

 if ((buttons & SCREEN_LEFT_MOUSE_BUTTON) != 0)
 {
 //Left mouse button is pressed
 //mouse_pressed = true;
 evento = Released;
 }
 else if (mouse_pressed)
 {
 //Left mouse button was released, add a cube
 //mouse_pressed = false;
 evento = Pressed;
 }
 }

 for (t = 0; t < (*TouchControl_getTouches()).size; t++)
 {
 toqueAEv = TouchControl_getATouch(t);
 if ((*toqueAEv).identifi == idenVal)
 {
 if (evento == Pressed)
 {
 (*toqueAEv).estado = Moved;
 }
 else
 {
 (*toqueAEv).estado = evento;
 }
 (*toqueAEv).position.X = Touch_transformaPosX(positionVal[0]);
 (*toqueAEv).position.Y = Touch_transformaPosY(
 (*GalacticaMain_getObject()).Wheight - positionVal[1]);
 toucheFind = 1;
 break;
 }
 toucheFind = 0;
 }

 if (toucheFind == 0 && evento != Nada && idenVal <= 3)
 {
 TouchControl_actualizaTouch(evento, posToPass, idenVal);
 }*/

