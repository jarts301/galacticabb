/*
 * SavedGame.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "SavedGame.h"
#include "Lista.h"
#include "Boton.h"
#include "Nave.h"
#include "Dibujar.h"
#include "Textura.h"
#include "GameComun.h"

static SavedGame SavedGameS;

void SavedGame_load()
{
	SavedGame_setTexturas();
	SavedGame_loadTitulo();
	Lista_Inicia(&SavedGameS.botones);
	Lista_AddInFin(&SavedGameS.botones, Boton_Crear(BContinueGame, 120, 680));
	Lista_AddInFin(&SavedGameS.botones, Boton_Crear(BNewGame, 120, 500));
	Lista_AddInFin(&SavedGameS.botones, Boton_Crear(BBack, 10, 10));
}

void SavedGame_update()
{
	GameComun_updateEstrellas();
	for (SavedGameS.i = 0; SavedGameS.i < SavedGameS.botones.size;
			SavedGameS.i++)
	{
		Boton_update((Boton*) Lista_GetIn(&SavedGameS.botones, SavedGameS.i));
	}
	(*Boton_getSObject()).ultimaActualizacion = GameFramework_getTotalTime();
	(*Nave_getSObject()).ultimaActualizacion = GameFramework_getTotalTime();
}

void SavedGame_draw()
{
	//CLEAR NEGRO
	Dibujar_drawSprite(Textura_getTexturaFondo(0), GameFramework_Vector2(0, 0),
			GameFramework_Color(0, 0, 0, 1), 0, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(100, 100));

	GameComun_drawEstrellas();
	SavedGame_drawTitulo();
	for (SavedGameS.i = 0; SavedGameS.i < SavedGameS.botones.size;
			SavedGameS.i++)
	{
		Boton_draw((Boton*) Lista_GetIn(&SavedGameS.botones, SavedGameS.i));
	}

}

//******************************************

//******TITULO***********
void SavedGame_loadTitulo()
{
	SavedGameS.positionTitulo = GameFramework_Vector2(300, 880);
	SavedGameS.origenTitulo = GameFramework_Vector2(
			(*(TexturaObjeto*) Lista_GetIn(&SavedGameS.textura, 0)).ancho / 2,
			(*(TexturaObjeto*) Lista_GetIn(&SavedGameS.textura, 0)).alto / 2);
}

void SavedGame_drawTitulo()
{
	Dibujar_drawSprite((TexturaObjeto*) Lista_GetIn(&SavedGameS.textura, 0),
			SavedGameS.positionTitulo, GameFramework_Color(1, 1, 1, 1), 0,
			SavedGameS.origenTitulo, GameFramework_Vector2(1.0, 1.0));
}

//*********Texturas***************
void SavedGame_setTexturas()
{
	Lista_Inicia(&SavedGameS.textura);
	Lista_AddInFin(&SavedGameS.textura, Textura_getTextura("TituloSavedGame")); //0
}

