/*
 * Mision7.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "Mision7.h"
#include "QuickModeGame.h"
#include "Enemigo.h"
#include "Animation.h"
#include "Jefe.h"
#include "BarraVida.h"
#include "Boton.h"
#include "Nave.h"
#include "Lista.h"
#include "Control.h"
#include "Textura.h"
#include "Dibujar.h"
#include "Audio.h"
#include "Mision1.h"
#include "MisionMode.h"
#include "CollitionJefe.h"
#include "Almacenamiento.h"
#include "MisionModeGOver.h"
#include "Cadena.h"

static Mision7 Mision7S;

void Mision7_reiniciar()
{
	QuickModeGame_eliminarTodos();
	QuickModeGame_eliminaAnimaciones();
	Mision7S.positionToBeat.X = 6;
	Mision7S.positionToBeat.Y = 50;
	Mision7S.positionTime.X = 340;
	Mision7S.positionTime.Y = 50;
	Mision7S.positionEnemigoS.X = 6;
	Mision7S.positionEnemigoS.Y = 900;
	Lista_Destruir(&((*QuickModeGame_getSObject()).igb.botones));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BPause, 5, 950));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BAudio, 530, 950));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BPower, 480, 20));
	(*QuickModeGame_getSObject()).igb.velocidadEnem = 2;
	(*QuickModeGame_getSObject()).igb.velocidadEnemY = 2;
	(*QuickModeGame_getSObject()).igb.velocidadHiEnem = 3;
	(*QuickModeGame_getSObject()).igb.velocidadHiEnemY = 3;
	(*QuickModeGame_getSObject()).igb.maxEnemigos = 5;
	Mision7S.cuentaTiempo = 60;
	(*Mision7S.elEnemigo).movimientoLoco = 1;
	Vector2 auxp;
	auxp.X = 300;
	auxp.Y = 750;
	Enemigo_setPosition(Mision7S.elEnemigo, auxp);
	(*Mision7S.elEnemigo).maxVida = 200;
	(*Mision7S.elEnemigo).vida = 30;
	(*Mision7S.elEnemigo).velMinLoco = 4;
	(*Mision7S.elEnemigo).velMaxLoco = 8;
	(*Mision7S.elEnemigo).locoAbajo = 450;
	(*Mision7S.elEnemigo).locoArriba = 900;
	(*Mision7S.elEnemigo).locoDerecha = 600;
	(*Mision7S.elEnemigo).locoIzquierda = 0;
	(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo = 0;
	Mision7S.ultimoTMinuto = 0;
	(*QuickModeGame_getSObject()).igb.ultimoAumento = 0;
	(*QuickModeGame_getSObject()).igb.EscalaMensaje =
			(*QuickModeGame_getSObject()).igb.escalaInicialMensaje = 1.5f;
	(*QuickModeGame_getSObject()).igb.actualFrameMensaje = 0;
	(*QuickModeGame_getSObject()).igb.tiempoTitileoMensaje = 0;
	(*QuickModeGame_getSObject()).igb.mensajeInicialActivo = 1;
	Vector2 aux2p;
	aux2p.X = 300;
	aux2p.Y = 250;
	Nave_puntoInicio(aux2p);
}

void Mision7_load()
{
	Mision7S.tituloMision = "Mission 7";

	Lista_Inicia(&Mision7S.mensajeMision);
	Lista_AddInFin(&Mision7S.mensajeMision,
			String_creaString("Shoot to the enemy all"));
	Lista_AddInFin(&Mision7S.mensajeMision,
			String_creaString("the time, dont let to"));
	Lista_AddInFin(&Mision7S.mensajeMision,
			String_creaString("do it or he will reload"));
	Lista_AddInFin(&Mision7S.mensajeMision,
			String_creaString("all his energy and"));
	Lista_AddInFin(&Mision7S.mensajeMision,
			String_creaString("you will lose."));
	Lista_AddInFin(&Mision7S.mensajeMision,
			String_creaString("Tip:Shoot as fast as"));
	Lista_AddInFin(&Mision7S.mensajeMision, String_creaString("you can."));

	Lista_Inicia(&Mision7S.recompensa);
	Lista_AddInFin(&Mision7S.recompensa,
			String_creaString("Maximum energy increased"));
	Lista_AddInFin(&Mision7S.recompensa,
			String_creaString("by 50, in Quick Game."));

	Lista_Inicia(&Mision7S.noRecompensa);
	Lista_AddInFin(&Mision7S.noRecompensa, String_creaString("No Reward.."));

	Mision7S.cuentaTiempo = 60;
	Mision7S.elEnemigo = Enemigo_Crear(ENor, 300, 750, 0, 0);
	Mision7S.barraVidaEnemigo = BarraVida_Crear(GameFramework_Vector2(6, 890),
			BVmini);
}

void Mision7_update()
{
	Nave_update();
	QuickModeGame_updateEstrellas();
	(*Nave_getSObject()).movEnY = 0;
	Enemigo_update(Mision7S.elEnemigo);
	if ((*Mision7S.elEnemigo).estaEnColisionDisp)
	{
		(*Mision7S.elEnemigo).estaEnColisionDisp = 0;
		if ((*Animation_getSObject()).animacionesDispAEnemigoTotal.size > 0)
		{
			Animation_setPosition(
					(Animation*) Lista_GetIn(
							&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
							(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
									- 1), (*Mision7S.elEnemigo).Position);

			Lista_AddInFin(&(*QuickModeGame_getSObject()).igb.animacionesQ,
					Lista_GetIn(
							&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
							(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
									- 1));

			Lista_RemoveAt(
					&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
					(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
							- 1);
		}
	}

	QuickModeGame_updateAnimaciones();
	for (Mision7S.i = 0;
			Mision7S.i < (*QuickModeGame_getSObject()).igb.botones.size;
			Mision7S.i++)
	{
		Boton_update(
				(Boton*) Lista_GetIn(&(*QuickModeGame_getSObject()).igb.botones,
						Mision7S.i));
	}

	if (GameFramework_getTotalTime() > Mision7S.ultimoTMinuto + 1000000)
	{
		Mision7S.cuentaTiempo = Mision7S.cuentaTiempo - 1;
		Mision7S.ultimoTMinuto = GameFramework_getTotalTime();
		(*Mision7S.elEnemigo).vida = (*Mision7S.elEnemigo).vida + 10;
	}
	if ((*Mision7S.elEnemigo).vida <= 0)
	{
		(*Mision7S.elEnemigo).vida = 20;
	}

	QuickModeGame_updateMensajeInicial();
	BarraVida_update((*QuickModeGame_getSObject()).igb.barraVidaNave,
			(*Nave_getSObject()).Vida, (*Nave_getSObject()).MaxVida);

	BarraVida_update(Mision7S.barraVidaEnemigo, (*Mision7S.elEnemigo).vida,
			(*Mision7S.elEnemigo).maxVida);

	if ((*Nave_getSObject()).Vida <= 0)
	{
		Animation_update((*Animation_getSObject()).animacionesEstallidoNave);
	}
	Mision7_gameOver();
	if ((*Control_getSObject()).controlActivo == 1)
	{
		Control_update((*QuickModeGame_getSObject()).igb.control);
	}
}

void Mision7_draw()
{
	//CLEAR NEGRO
	Dibujar_drawSprite(Textura_getTexturaFondo(0), GameFramework_Vector2(0, 0),
			GameFramework_Color(0, 0, 0, 1), 0, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(40, 40));

	Nave_draw();
	QuickModeGame_drawEstrellas();
	Mision7_drawEstadNave();
	Enemigo_draw(Mision7S.elEnemigo);
	QuickModeGame_drawAnimaciones();
	Mision7_drawMensajeInicial();
	if ((*Nave_getSObject()).Vida <= 0)
	{
		Animation_draw((*Animation_getSObject()).animacionesEstallidoNave);
	}

	for (Mision7S.i = 0;
			Mision7S.i < (*QuickModeGame_getSObject()).igb.botones.size;
			Mision7S.i++)
	{
		Boton_draw(
				(Boton*) Lista_GetIn(&(*QuickModeGame_getSObject()).igb.botones,
						Mision7S.i));
	}
	if ((*Control_getSObject()).controlActivo == 1)
	{
		Control_draw((*QuickModeGame_getSObject()).igb.control);
	}
	// drawEstadisticas(sb);
}

//******************Estadisticas de la nave*************
void Mision7_drawEstadNave()
{
	//** Vida de la nave
	Dibujar_drawSprite(Textura_getTexturaFondo(1),
			GameFramework_Vector2(0, 945), GameFramework_Color(0, 0, 0, 1), 0,
			GameFramework_Vector2(0, 0), GameFramework_Vector2(50, 2.6));

	BarraVida_draw((*QuickModeGame_getSObject()).igb.barraVidaNave);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_EnteroACadena((*Nave_getSObject()).Vida),
			GameFramework_Vector2(100, 975), GameFramework_Color(1, 1, 1, 1),
			0.8);

	Dibujar_drawText((*Textura_getSObject()).Fuente, "/",
			GameFramework_Vector2(285, 965), GameFramework_Color(1, 1, 1, 1),
			1.4);

	Nave_drawAnimacionesPoder();

	//Tiempo restante
	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "Time:",
					GameFramework_EnteroACadena(Mision7S.cuentaTiempo)),
			GameFramework_Vector2(420, 900),
			GameFramework_Color(GameFramework_getByteColorToFloatColor(244),
					GameFramework_getByteColorToFloatColor(232),
					GameFramework_getByteColorToFloatColor(16), 1), 0.9);

	BarraVida_draw(Mision7S.barraVidaEnemigo);

	Dibujar_drawText((*Textura_getSObject()).Fuente, "Enemy",
			Mision7S.positionEnemigoS, GameFramework_Color(1, 1, 1, 1), 0.75);
}

void Mision7_gameOver()
{
	if (Mision7S.cuentaTiempo <= 0)
	{
		if ((*Audio_getSObject()).musicaActiva == 1)
		{
			Audio_stopAudio("Galactic");
			Audio_playAudio("GameOver");
		}
		(*MisionModeGOver_getSObject()).gana = 1;

		Almacenamiento_cargarMisiones();
		if (Almacenamiento_getPremiosDados() == 6)
		{
			Almacenamiento_salvarBonus(300, ALsumarAsalud);
			(*MisionModeGOver_getSObject()).cosasGanadas = &Mision7S.recompensa;
			Almacenamiento_salvarMisiones();
		}
		else
		{
			(*MisionModeGOver_getSObject()).cosasGanadas =
					&(*Mision1_getSObject()).yaRecompensa;
		}

		(*MisionMode_getSObject()).estadoActual = MMGameOver;
	}

	if ((*Nave_getSObject()).Vida <= 0
			|| (*Mision7S.elEnemigo).vida >= (*Mision7S.elEnemigo).maxVida)
	{
		(*Nave_getSObject()).Vida = 0;
		(*Mision7S.elEnemigo).vida = (*Mision7S.elEnemigo).maxVida;
		(*Nave_getSObject()).EscalaN = GameFramework_Vector2(0, 0);
		Animation_setPosition(
				(*Animation_getSObject()).animacionesEstallidoNave,
				(*Nave_getSObject()).PositionNav);
		(*MisionModeGOver_getSObject()).gana = 0;
		(*MisionModeGOver_getSObject()).cosasGanadas = &Mision7S.noRecompensa;
		if ((*(*Animation_getSObject()).animacionesEstallidoNave).animacionFinalizada
				== 1)
		{
			if ((*Audio_getSObject()).musicaActiva == 1)
			{
				Audio_stopAudio("Galactic");
				Audio_playAudio("GameOver");
			}
			(*MisionMode_getSObject()).estadoActual = MMGameOver;
		}
	}
}

//********Dibujo Mensaje Inicial
void Mision7_drawMensajeInicial()
{
	if ((*QuickModeGame_getSObject()).igb.mensajeInicialActivo == 1)
	{
		Dibujar_drawText((*Textura_getSObject()).Fuente,
				GameFramework_Concatenar(2, Mision7S.tituloMision, " Start!!"),
				GameFramework_Vector2(50, 620),
				GameFramework_Color(GameFramework_getByteColorToFloatColor(7),
						GameFramework_getByteColorToFloatColor(237),
						GameFramework_getByteColorToFloatColor(134), 1),
				(*QuickModeGame_getSObject()).igb.EscalaMensaje);
	}
}

Mision7* Mision7_getSObject()
{
	return &Mision7S;
}
