/*
 * Mision9.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION9_H_
#define MISION9_H_

#include "GalacticaTipos.h"

void Mision9_reiniciar();
void Mision9_load();
void Mision9_update();
void Mision9_draw();
void Mision9_drawEstadoNave();
void Mision9_updateGame();
void Mision9_gameOver();
void Mision9_drawMensajeInicial();
void Mision9_insertaEnemigo();
Mision9* Mision9_getSObject();

#endif /* MISION9_H_ */
