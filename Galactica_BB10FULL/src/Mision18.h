/*
 * Mision18.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION18_H_
#define MISION18_H_

#include "GalacticaTipos.h"

void Mision18_reiniciar();
void Mision18_load();
void Mision18_update();
void Mision18_draw();
void Mision18_drawEstadNave();
void Mision18_updateGame();
void Mision18_gameOver();
void Mision18_drawMensajeInicial();
void Mision18_updateEnemigos();
void Mision18_insertaEnemigo();
Mision18* Mision18_getSObject();

#endif /* MISION18_H_ */
