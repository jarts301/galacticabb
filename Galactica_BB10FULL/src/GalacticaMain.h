/*
 * GalacticaMain.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef GALACTICAMAIN_H_
#define GALACTICAMAIN_H_

#include "GalacticaTipos.h"

void GalacticaMain_Load();
void GalacticaMain_Update();
void GalacticaMain_Draw();
void GalacticaMain_cargarAudio();
void GalacticaMain_cargarTexturas();
void GalacticaMain_cargarTodo();
GalacticaMain* GalacticaMain_getObject();

#endif /* GALACTICAMAIN_H_ */
