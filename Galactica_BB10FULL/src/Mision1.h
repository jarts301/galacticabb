/*
 * Mision1.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION1_H_
#define MISION1_H_

#include "GalacticaTipos.h"

void Mision1_reiniciar();
void Mision1_Load();
void Mision1_update();
void Mision1_draw();
void Mision1_drawEstadoNave();
void Mision1_updateGame();
void Mision1_gameOver();
void Mision1_drawMensajeInicial();
void Mision1_insertaEnemigo();
Mision1* Mision1_getSObject();

#endif /* MISION1_H_ */
