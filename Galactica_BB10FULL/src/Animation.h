/*
 * Animation.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef ANIMATION_H_
#define ANIMATION_H_

#include "GalacticaTipos.h"

Animation* Animation_Crear(Animation_tipo t);
void Animation_load();
void Animation_update(Animation* animation);
void Animation_draw(Animation* animation);
void Animation_setPosition(Animation* animation, Vector2 p);
Vector2 Animation_getPosition(Animation* animation);
void Animation_cargaListaAnimaciones();
void Animation_updateDisparoANave(Animation* animation);
void Animation_updateDisparoAEnemigo(Animation* animation);
void Animation_updateEstallidoEnemigo(Animation* animation);
void Animation_updateEstallidoJefe(Animation* animation);
void Animation_updateEstallidoNave(Animation* animation);
void Animation_updateAnimacionPoder(Animation* animation);
void Animation_TerminaAnimacion(Animation* animation, int siOno);
int Animation_isFinAnimation(Animation* animation);
void Animation_setTextura();
void Animation_defineTrozoAnim(Animation* animation, int x, int y, int width,
		int height);
void Animation_setOrigenV(Animation* animation, Vector2 o);
void Animation_setOrigen(Animation* animation);
Animation* Animation_getSObject();

#endif /* ANIMATION_H_ */
