/*
 * Estrella.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef ESTRELLA_H_
#define ESTRELLA_H_

#include "GalacticaTipos.h"

Estrella* Estrella_crear(Vector2 posIni);
void Estrella_update(Estrella* estrella);
void Estrella_movimientoEstrella(Estrella* estrella);
void Estrella_setPosition(Estrella* estrella,Vector2 pos);
void Estrella_setOrigen(Estrella* estrella);
void Estrella_setTextura();
void Estrella_load();
void Estrella_draw(Estrella* estrella);


#endif /* ESTRELLA_H_ */
