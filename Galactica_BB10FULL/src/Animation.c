/*
 * Animation.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "Animation.h"
#include "Textura.h"
#include <stdlib.h>
#include "GameFramework.h"
#include "Dibujar.h"

static Animation AnimationS;

/*

 public Animation()
 {

 }
 */
Animation* Animation_Crear(Animation_tipo t)
{
	Animation* nuevo;
	if ((nuevo = (Animation*) malloc(sizeof(Animation))) == NULL)
		return NULL;

	(*nuevo).tipoActual = t;
	switch (t)
	{
	case ADisparoANave:
		(*nuevo).textura = 0;
		(*nuevo).actualframe = 0;
		(*nuevo).maxFrames = 2;
		(*nuevo).Escala.X = 1.0f;
		(*nuevo).Escala.Y = 1.0f;
		(*nuevo).velocidad = 3;
		GameFramework_ColorTo(&((*nuevo).Tinte), 1, 1, 1, 1);
		Animation_defineTrozoAnim(nuevo, (*nuevo).actualframe * 32, 0, 32, 32);
		Animation_setOrigen(nuevo);
		break;
	case ADisparoAEnemigo:
		(*nuevo).textura = 1;
		(*nuevo).actualframe = 0;
		(*nuevo).maxFrames = 2;
		(*nuevo).Escala.X = 1.0f;
		(*nuevo).Escala.Y = 1.0f;
		(*nuevo).velocidad = 3;
		GameFramework_ColorTo(&((*nuevo).Tinte), 1, 1, 1, 1);
		Animation_defineTrozoAnim(nuevo, (*nuevo).actualframe * 32, 0, 32, 32);
		Animation_setOrigen(nuevo);
		break;
	case AEstallidoEnemigo:
		(*nuevo).textura = 2;
		(*nuevo).actualframe = 0;
		(*nuevo).maxFrames = 4;
		(*nuevo).Escala.X = 1.0f;
		(*nuevo).Escala.Y = 1.0f;
		(*nuevo).velocidad = 3;
		GameFramework_ColorTo(&((*nuevo).Tinte), 1, 1, 1, 1);
		Animation_defineTrozoAnim(nuevo, (*nuevo).actualframe * 128, 0, 128,
				128);
		Animation_setOrigen(nuevo);
		break;
	case AEstallidoJefe:
		(*nuevo).textura = 4;
		(*nuevo).actualframe = 0;
		(*nuevo).maxFrames = 4;
		(*nuevo).Escala.X = 1.0f;
		(*nuevo).Escala.Y = 1.0f;
		(*nuevo).velocidad = 3;
		GameFramework_ColorTo(&((*nuevo).Tinte), 1, 1, 1, 1);
		Animation_defineTrozoAnim(nuevo, (*nuevo).actualframe * 512, 0, 512,
				512);
		Animation_setOrigen(nuevo);
		break;
	case AEstallidoNave:
		(*nuevo).textura = 5;
		(*nuevo).actualframe = 0;
		(*nuevo).maxFrames = 4;
		(*nuevo).Escala.X = 1.0f;
		(*nuevo).Escala.Y = 1.0f;
		(*nuevo).velocidad = 3;
		GameFramework_ColorTo(&((*nuevo).Tinte), 1, 1, 1, 1);
		Animation_defineTrozoAnim(nuevo, (*nuevo).actualframe * 256, 0, 256,
				256);
		Animation_setOrigen(nuevo);
		break;
	case APoderGalactica:
		(*nuevo).textura = 3;
		(*nuevo).actualframe = 0;
		(*nuevo).maxFrames = 2;
		(*nuevo).Escala.X = 1.17f;
		(*nuevo).Escala.Y = 1.0f;
		(*nuevo).velocidad = 6;
		GameFramework_ColorTo(&((*nuevo).Tinte), 1, 1, 1, 1);
		Animation_defineTrozoAnim(nuevo, (*nuevo).actualframe * 512, 0, 512,
				1024);
		Animation_setOrigenV(nuevo, GameFramework_Vector2(0, 0));
		break;
	case APoderCarafe:
		(*nuevo).textura = 3;
		(*nuevo).actualframe = 0;
		(*nuevo).maxFrames = 2;
		(*nuevo).Escala.X = 1.17f;
		(*nuevo).Escala.Y = 1.0f;
		(*nuevo).velocidad = 6;
		GameFramework_ColorTo(&((*nuevo).Tinte),
				GameFramework_getByteColorToFloatColor(224),
				GameFramework_getByteColorToFloatColor(140),
				GameFramework_getByteColorToFloatColor(28), 1);
		Animation_defineTrozoAnim(nuevo, (*nuevo).actualframe * 512, 0, 512,
				1024);
		Animation_setOrigenV(nuevo, GameFramework_Vector2(0, 0));
		break;
	case APoderHelix:
		(*nuevo).textura = 3;
		(*nuevo).actualframe = 0;
		(*nuevo).maxFrames = 2;
		(*nuevo).Escala.X = 1.17f;
		(*nuevo).Escala.Y = 1.0f;
		(*nuevo).velocidad = 6;
		GameFramework_ColorTo(&((*nuevo).Tinte),
				GameFramework_getByteColorToFloatColor(150),
				GameFramework_getByteColorToFloatColor(230),
				GameFramework_getByteColorToFloatColor(135), 1);
		Animation_defineTrozoAnim(nuevo, (*nuevo).actualframe * 512, 0, 512,
				1024);
		Animation_setOrigenV(nuevo, GameFramework_Vector2(0, 0));
		break;
	case APoderArp:
		(*nuevo).textura = 3;
		(*nuevo).actualframe = 0;
		(*nuevo).maxFrames = 2;
		(*nuevo).Escala.X = 1.17f;
		(*nuevo).Escala.Y = 1.0f;
		(*nuevo).velocidad = 6;
		GameFramework_ColorTo(&((*nuevo).Tinte),
				GameFramework_getByteColorToFloatColor(255),
				GameFramework_getByteColorToFloatColor(242),
				GameFramework_getByteColorToFloatColor(128), 1);
		Animation_defineTrozoAnim(nuevo, (*nuevo).actualframe * 512, 0, 512,
				1024);
		Animation_setOrigenV(nuevo, GameFramework_Vector2(0, 0));
		break;
	case APoderPerseus:
		(*nuevo).textura = 3;
		(*nuevo).actualframe = 0;
		(*nuevo).maxFrames = 2;
		(*nuevo).Escala.X = 1.17f;
		(*nuevo).Escala.Y = 1.0f;
		(*nuevo).velocidad = 6;
		GameFramework_ColorTo(&((*nuevo).Tinte),
				GameFramework_getByteColorToFloatColor(255),
				GameFramework_getByteColorToFloatColor(48),
				GameFramework_getByteColorToFloatColor(0), 1);
		Animation_defineTrozoAnim(nuevo, (*nuevo).actualframe * 512, 0, 512,
				1024);
		Animation_setOrigenV(nuevo, GameFramework_Vector2(0, 0));
		break;
	case APoderPisces:
		(*nuevo).textura = 3;
		(*nuevo).actualframe = 0;
		(*nuevo).maxFrames = 2;
		(*nuevo).Escala.X = 1.17f;
		(*nuevo).Escala.Y = 1.0f;
		(*nuevo).velocidad = 6;
		GameFramework_ColorTo(&((*nuevo).Tinte),
				GameFramework_getByteColorToFloatColor(255),
				GameFramework_getByteColorToFloatColor(105),
				GameFramework_getByteColorToFloatColor(235), 1);
		Animation_defineTrozoAnim(nuevo, (*nuevo).actualframe * 512, 0, 512,
				1024);
		Animation_setOrigenV(nuevo, GameFramework_Vector2(0, 0));
		break;
	}
	(*nuevo).animacionFinalizada = 1;
	(*nuevo).dato = 0;

	return nuevo;
}

void Animation_load()
{
	Animation_setTextura();
	Animation_cargaListaAnimaciones();
}

void Animation_update(Animation* animation)
{
	switch ((*animation).tipoActual)
	{
	case ADisparoANave:
		Animation_updateDisparoANave(animation);
		break;

	case ADisparoAEnemigo:
		Animation_updateDisparoAEnemigo(animation);
		break;

	case AEstallidoEnemigo:
		Animation_updateEstallidoEnemigo(animation);
		break;

	case AEstallidoJefe:
		Animation_updateEstallidoJefe(animation);
		break;
	case AEstallidoNave:
		Animation_updateEstallidoNave(animation);
		break;
	case APoderGalactica:
		Animation_updateAnimacionPoder(animation);
		break;
	case APoderCarafe:
		Animation_updateAnimacionPoder(animation);
		break;
	case APoderHelix:
		Animation_updateAnimacionPoder(animation);
		break;
	case APoderArp:
		Animation_updateAnimacionPoder(animation);
		break;
	case APoderPerseus:
		Animation_updateAnimacionPoder(animation);
		break;
	case APoderPisces:
		Animation_updateAnimacionPoder(animation);
		break;
	}
}

void Animation_draw(Animation* animation)
{
	if ((*animation).animacionFinalizada == 0)
	{
		Dibujar_drawSpriteTr(
				(TexturaObjeto*) Lista_RetornaElemento(&(AnimationS.texturas),
						(*animation).textura), (*animation).Position,
				(*animation).TrozoAnim, (*animation).Tinte, 0.0f,
				(*animation).Origen, (*animation).Escala);
		/*spriteBatch.Draw(texturas[textura], Position, TrozoAnim, Tinte, 0.0f,
		 Origen, Escala, SpriteEffects.None, 0.3f);*/
	}
}

//***********************************

void Animation_setPosition(Animation* animation, Vector2 p)
{
	(*animation).Position.X = p.X;
	(*animation).Position.Y = p.Y;
}
Vector2 Animation_getPosition(Animation* animation)
{
	return (*animation).Position;
}

//Carga lista animaciones disparo a nave
void Animation_cargaListaAnimaciones()
{
	Lista_Inicia(&(AnimationS.animacionesDispANaveTotal));
	Lista_Inicia(&(AnimationS.animacionesDispAEnemigoTotal));
	Lista_Inicia(&(AnimationS.animacionesEstallidoEnemigoTotal));
	Lista_Inicia(&(AnimationS.animacionesEstallidoJefeTotal));
	AnimationS.animacionesEstallidoNave = Animation_Crear(AEstallidoNave);
	AnimationS.animacionEstallidoJefe = Animation_Crear(AEstallidoJefe);
	for (AnimationS.w = 0; AnimationS.w < 200; AnimationS.w++)
	{
		Lista_InsertaEnFinal(&(AnimationS.animacionesDispANaveTotal),
				AnimationS.animacionesDispANaveTotal.fin,
				Animation_Crear(ADisparoANave));
	}
	for (AnimationS.w = 0; AnimationS.w < 150; AnimationS.w++)
	{
		Lista_InsertaEnFinal(&(AnimationS.animacionesDispAEnemigoTotal),
				AnimationS.animacionesDispAEnemigoTotal.fin,
				Animation_Crear(ADisparoAEnemigo));
		Lista_InsertaEnFinal(&(AnimationS.animacionesEstallidoEnemigoTotal),
				AnimationS.animacionesEstallidoEnemigoTotal.fin,
				Animation_Crear(AEstallidoEnemigo));
	}
	for (AnimationS.w = 0; AnimationS.w < 30; AnimationS.w++)
	{
		Lista_InsertaEnFinal(&(AnimationS.animacionesEstallidoJefeTotal),
				AnimationS.animacionesEstallidoJefeTotal.fin,
				Animation_Crear(AEstallidoJefe));
	}
}

//************************
//********** Disparo a Nave*************
void Animation_updateDisparoANave(Animation* animation)
{
	(*animation).dato = (*animation).dato + GameFramework_getSwapTime();
	if ((*animation).dato
			>= ((GameFramework_getSwapTime()) * (*animation).velocidad))
	{
		(*animation).actualframe = (*animation).actualframe + 1;
		(*animation).dato = 0;
	}

	if ((*animation).actualframe >= 2)
	{
		(*animation).actualframe = 0;
		(*animation).animacionFinalizada = 1;
	}

	Animation_defineTrozoAnim(animation, (*animation).actualframe * 32, 0, 32,
			32);

	Animation_setOrigen(animation);
}

//*************Disparo a Enemigo***********
void Animation_updateDisparoAEnemigo(Animation* animation)
{
	(*animation).dato = (*animation).dato + GameFramework_getSwapTime();
	if ((*animation).dato
			>= ((GameFramework_getSwapTime()) * (*animation).velocidad))
	{
		(*animation).actualframe = (*animation).actualframe + 1;
		(*animation).dato = 0;
	}

	if ((*animation).actualframe >= 2)
	{
		(*animation).actualframe = 0;
		(*animation).animacionFinalizada = 1;
	}

	Animation_defineTrozoAnim(animation, (*animation).actualframe * 32, 0, 32,
			32);

	Animation_setOrigen(animation);
}

//****************Estallido Enemigo*************
void Animation_updateEstallidoEnemigo(Animation* animation)
{
	(*animation).dato = (*animation).dato + GameFramework_getSwapTime();
	if ((*animation).dato
			>= ((GameFramework_getSwapTime()) * (*animation).velocidad))
	{
		(*animation).actualframe = (*animation).actualframe + 1;
		(*animation).dato = 0;
	}

	if ((*animation).actualframe >= 4)
	{
		(*animation).actualframe = 0;
		(*animation).animacionFinalizada = 1;
	}

	Animation_defineTrozoAnim(animation, (*animation).actualframe * 128, 0, 128,
			128);

	Animation_setOrigen(animation);
}

//****************Estallido Jefe*************
void Animation_updateEstallidoJefe(Animation* animation)
{
	(*animation).dato = (*animation).dato + GameFramework_getSwapTime();
	if ((*animation).dato
			>= ((GameFramework_getSwapTime()) * (*animation).velocidad))
	{
		(*animation).actualframe = (*animation).actualframe + 1;
		(*animation).dato = 0;
	}

	if ((*animation).actualframe >= 4)
	{
		(*animation).actualframe = 0;
		(*animation).animacionFinalizada = 1;
	}

	Animation_defineTrozoAnim(animation, (*animation).actualframe * 512, 0, 512,
			512);

	Animation_setOrigen(animation);
}

//****************Estallido Nave*************
void Animation_updateEstallidoNave(Animation* animation)
{
	(*animation).dato = (*animation).dato + GameFramework_getSwapTime();
	if ((*animation).dato
			>= ((GameFramework_getSwapTime()) * (*animation).velocidad))
	{
		(*animation).actualframe = (*animation).actualframe + 1;
		(*animation).dato = 0;
	}

	if ((*animation).actualframe >= 4)
	{
		(*animation).actualframe = 0;
		(*animation).animacionFinalizada = 1;
	}

	Animation_defineTrozoAnim(animation, (*animation).actualframe * 256, 0, 256,
			256);

	Animation_setOrigen(animation);
}

// *******  actualiza animacion Poder
void Animation_updateAnimacionPoder(Animation* animation)
{
	(*animation).dato = (*animation).dato + GameFramework_getSwapTime();
	if ((*animation).dato
			>= ((GameFramework_getSwapTime()) * (*animation).velocidad))
	{
		(*animation).actualframe = (*animation).actualframe + 1;
		(*animation).dato = 0;
	}

	if ((*animation).actualframe >= 2)
	{
		(*animation).actualframe = 0;
	}

	Animation_defineTrozoAnim(animation, (*animation).actualframe * 512, 0, 512,
			1024);
	Animation_setOrigenV(animation, GameFramework_Vector2(0, 0));
}

//* Define si animacion termina o no
void Animation_TerminaAnimacion(Animation* anim, int siOno)
{
	(*anim).animacionFinalizada = siOno;
}

//**************************************
int Animation_isFinAnimation(Animation* animation)
{
	if ((*animation).actualframe >= (*animation).maxFrames)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void Animation_setTextura()
{
	Lista_Inicia(&(AnimationS.texturas));
	Lista_InsertaEnFinal(&(AnimationS.texturas), AnimationS.texturas.fin,
			Textura_getTextura("animationDispANave")); //0
	Lista_InsertaEnFinal(&(AnimationS.texturas), AnimationS.texturas.fin,
			Textura_getTextura("animationDispAEnemigo")); //1
	Lista_InsertaEnFinal(&(AnimationS.texturas), AnimationS.texturas.fin,
			Textura_getTextura("EstallidoEnemigo")); //2
	Lista_InsertaEnFinal(&(AnimationS.texturas), AnimationS.texturas.fin,
			Textura_getTextura("animationPoder")); //3
	Lista_InsertaEnFinal(&(AnimationS.texturas), AnimationS.texturas.fin,
			Textura_getTextura("EstallidoJefe")); //4
	Lista_InsertaEnFinal(&(AnimationS.texturas), AnimationS.texturas.fin,
			Textura_getTextura("EstallidoNave")); //5
}

void Animation_defineTrozoAnim(Animation* animation, int x, int y, int width,
		int height)
{
	(*animation).TrozoAnim.X = x;
	(*animation).TrozoAnim.Y = y;
	(*animation).TrozoAnim.Width = width;
	(*animation).TrozoAnim.Height = height;
}

void Animation_setOrigenV(Animation* animation, Vector2 o)
{
	(*animation).Origen.X = o.X;
	(*animation).Origen.Y = o.Y;
}

void Animation_setOrigen(Animation* animation)
{
	(*animation).Origen.X = (*animation).TrozoAnim.Width / 2;
	(*animation).Origen.Y = (*animation).TrozoAnim.Height / 2;
}

Animation* Animation_getSObject()
{
	return &AnimationS;
}

