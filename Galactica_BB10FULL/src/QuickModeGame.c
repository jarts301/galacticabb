/*
 * QuickModeGame.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "QuickModeGame.h"
#include "Boton.h"
#include "DisparoJefe.h"
#include "DisparoEnemigo.h"
#include "Enemigo.h"
#include "Jefe.h"
#include "BarraVida.h"
#include "Plus.h"
#include "Lista.h"
#include "Nave.h"
#include "Control.h"
#include "Animation.h"
#include "Dibujar.h"
#include "Textura.h"
#include "GalacticaMain.h"
#include "Audio.h"
#include "Almacenamiento.h"
#include "QuickMode.h"
#include "CollitionJefe.h"
#include "TouchControl.h"
#include "Collition.h"
#include "Estrella.h"

static QuickModeGame QuickModeGameS;

void QuickModeGame_reiniciar() {
	(*Jefe_getSObject()).adicionDanoNave = 0;
	(*DisparoJefe_getSObject()).adicionDanoDisNave = 0;
	(*Enemigo_getSObject()).adicionDanoNave = 0;
	(*DisparoEnemigo_getSObject()).adicionDanoDisNave = 0;
	QuickModeGameS.igb.llegaA1500 = 0;
	QuickModeGameS.igb.tiempoEsperaPlus = 30000000;
	QuickModeGameS.igb.ultimoTPlus = 0;
	QuickModeGameS.igb.velocidadEnem = 2;
	QuickModeGameS.igb.velocidadEnemY = 2;
	QuickModeGameS.igb.velocidadHiEnem = 3;
	QuickModeGameS.igb.velocidadHiEnemY = 3;
	QuickModeGameS.igb.maxEnemigos = 4;
	QuickModeGameS.igb.aumentoEnVidaEnemigo = 0;
	QuickModeGameS.igb.ultimoAumento = QuickModeGameS.igb.ultimaAdicionJefe = 0;
	QuickModeGame_eliminarTodos();
	QuickModeGame_eliminarJefes();
	QuickModeGame_eliminaAnimaciones();
	(*QuickModeGameS.igb.plus).plusActivo = 0;
	(*QuickModeGameS.igb.plus).muestra = 0;
	//Mensaje Inicial
	QuickModeGameS.igb.EscalaMensaje = QuickModeGameS.igb.escalaInicialMensaje =
			1.5f;
	QuickModeGameS.igb.actualFrameMensaje = 0;
	QuickModeGameS.igb.tiempoTitileoMensaje = 0;
	QuickModeGameS.igb.mensajeInicialActivo = 1;

	Lista_Destruir(&(QuickModeGameS.igb.botones));
	Lista_InsertaEnFinal(&(QuickModeGameS.igb.botones),
			QuickModeGameS.igb.botones.fin, Boton_Crear(BPause, 5, 950));
	Lista_InsertaEnFinal(&(QuickModeGameS.igb.botones),
			QuickModeGameS.igb.botones.fin, Boton_Crear(BAudio, 530, 950));
	Lista_InsertaEnFinal(&(QuickModeGameS.igb.botones),
			QuickModeGameS.igb.botones.fin, Boton_Crear(BPower, 480, 20));
}

void QuickModeGame_load() {
	Lista_Inicia(&QuickModeGameS.igb.animaciones);
	Lista_Inicia(&QuickModeGameS.igb.disparosSid);
	Lista_Inicia(&QuickModeGameS.igb.disparosNor);
	Lista_Inicia(&QuickModeGameS.igb.texturas);
	Lista_Inicia(&QuickModeGameS.igb.estrellas);
	Lista_Inicia(&QuickModeGameS.igb.enemigos);
	Lista_Inicia(&QuickModeGameS.igb.jefes);
	Lista_Inicia(&QuickModeGameS.igb.animacionesQ);

	QuickModeGameS.igb.PositionSalud = GameFramework_Vector2(0, 0);
	//QuickModeGameS.igb.PositionVistaSalud = GameFramework_Vector2(70, 900);
	//QuickModeGameS.igb.PositionBarraSalud = GameFramework_Vector2(55, 900);
	QuickModeGameS.igb.barraVidaJefe = BarraVida_Crear(
			GameFramework_Vector2(5, 920), BVjefe);
	QuickModeGameS.igb.barraVidaNave = BarraVida_Crear(
			GameFramework_Vector2(90, 965), BVnave);
	QuickModeGameS.igb.plus = Plus_Crear(GameFramework_Vector2(0, 0));

	/*Fondo*/
	QuickModeGameS.igb.position1 = GameFramework_Vector2(0, 0);
	QuickModeGameS.igb.position2 = GameFramework_Vector2(0, 0);

	//Para pintar puntaje
	QuickModeGameS.igb.PositionPuntos = GameFramework_Vector2(0, 0);
	QuickModeGameS.igb.PositionLabelPuntos = GameFramework_Vector2(0, 0);
	Lista_Inicia(&QuickModeGameS.igb.botones);

	//Control
	QuickModeGameS.igb.control = Control_creaObjecto(
			GameFramework_Vector2(0, 0));

	QuickModeGame_LoadEnemigos();
	QuickModeGame_loadEstrellas();
}

void QuickModeGame_update() {
	Nave_update();
	QuickModeGame_updateEstrellas();
	QuickModeGame_updateEnemigos();
	QuickModeGame_updateGame();
	QuickModeGame_updateAnimaciones();
	for (QuickModeGameS.igb.i = 0;
			QuickModeGameS.igb.i < QuickModeGameS.igb.botones.size;
			QuickModeGameS.igb.i++) {
		Boton_update(
				(Boton*) Lista_GetIn(&QuickModeGameS.igb.botones,
						QuickModeGameS.igb.i));
	}
	QuickModeGame_updateMensajeInicial();
	QuickModeGame_updateJefes();
	BarraVida_update(QuickModeGameS.igb.barraVidaNave,
			(*Nave_getSObject()).Vida, (*Nave_getSObject()).MaxVida);

	if ((*Nave_getSObject()).Vida <= 0) {
		Animation_update((*Animation_getSObject()).animacionesEstallidoNave);
	}
	QuickModeGame_updatePlus();
	QuickModeGame_gameOver();

	if ((*Control_getSObject()).controlActivo == 1) {
		Control_update(QuickModeGameS.igb.control);
	}
}

void QuickModeGame_draw() {
	//CLEAR NEGRO
	Dibujar_drawSprite(Textura_getTexturaFondo(0), GameFramework_Vector2(0, 0),
			GameFramework_Color(0, 0, 0, 1), 0, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(40, 40));

	QuickModeGame_drawEstrellas();
	QuickModeGame_drawEnemigos();
	QuickModeGame_drawJefes();
	QuickModeGame_drawAnimaciones();
	Nave_draw();
	QuickModeGame_drawEstadNave();
	if ((*Nave_getSObject()).Vida <= 0) {
		Animation_draw((*Animation_getSObject()).animacionesEstallidoNave);
	}
	if ((*QuickModeGameS.igb.plus).plusActivo == 1) {
		Plus_draw(QuickModeGameS.igb.plus);
	}
	for (QuickModeGameS.igb.i = 0;
			QuickModeGameS.igb.i < QuickModeGameS.igb.botones.size;
			QuickModeGameS.igb.i++) {
		Boton_draw(
				(Boton*) Lista_GetIn(&QuickModeGameS.igb.botones,
						QuickModeGameS.igb.i));
	}

	if ((*Control_getSObject()).controlActivo == 1) {
		Control_draw(QuickModeGameS.igb.control);
	}

	QuickModeGame_drawMensajeInicial();

	//QuickModeGame_drawEstadisticas();

}
//******************Estadisticas de la nave*************
void QuickModeGame_drawEstadNave() {
	//** Vida de la nave
	Dibujar_drawSprite(Textura_getTexturaFondo(1),
			GameFramework_Vector2(0, 945), GameFramework_Color(0, 0, 0, 1), 0,
			GameFramework_Vector2(0, 0), GameFramework_Vector2(50, 2.6));

	BarraVida_draw(QuickModeGameS.igb.barraVidaNave);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_EnteroACadena((*Nave_getSObject()).Vida),
			GameFramework_Vector2(100, 975), GameFramework_Color(1, 1, 1, 1),
			0.8);
	//**Puntos
	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_EnteroACadena((*Nave_getSObject()).Puntos),
			GameFramework_Vector2(310, 965),
			GameFramework_Color(GameFramework_getByteColorToFloatColor(244),
					GameFramework_getByteColorToFloatColor(232),
					GameFramework_getByteColorToFloatColor(16), 1), 1.4);

	Dibujar_drawText((*Textura_getSObject()).Fuente, "/",
			GameFramework_Vector2(285, 965), GameFramework_Color(1, 1, 1, 1),
			1.4);

	Nave_drawAnimacionesPoder();

	/*for (QuickModeGameS.igb.i = 0; QuickModeGameS.igb.i < 4;
	 QuickModeGameS.igb.i++)
	 {
	 Dibujar_drawText((*Textura_getSObject()).Fuente,
	 GameFramework_EnteroACadena(
	 (*TouchControl_getATouch(QuickModeGameS.igb.i)).estado),
	 GameFramework_Vector2(0, 100 * QuickModeGameS.igb.i),
	 GameFramework_Color(1, 1, 1, 1), 0.8);

	 Dibujar_drawText((*Textura_getSObject()).Fuente,
	 GameFramework_EnteroACadena(
	 (*TouchControl_getATouch(QuickModeGameS.igb.i)).identifi),
	 GameFramework_Vector2(100, 100 * QuickModeGameS.igb.i),
	 GameFramework_Color(1, 1, 1, 1), 0.8);
	 }*/

}

void QuickModeGame_updatePlus() {
	if (GameFramework_getTotalTime()
			> QuickModeGameS.igb.ultimoTPlus
					+ QuickModeGameS.igb.tiempoEsperaPlus) {
		(*QuickModeGameS.igb.plus).plusActivo = 1;
		Plus_reiniciar(QuickModeGameS.igb.plus,
				GameFramework_Vector2(GameFramework_Rand(20, 460), 1100));
		QuickModeGameS.igb.ultimoTPlus = GameFramework_getTotalTime();
	}

	if ((*QuickModeGameS.igb.plus).plusActivo == 1) {
		Plus_update(QuickModeGameS.igb.plus);
	}

	if ((*Nave_getSObject()).Vida <= (((*Nave_getSObject()).MaxVida * 2) / 8)) {
		(*Nave_getSObject()).disparoSuperActivo = 0;
	}
	if ((*Nave_getSObject()).Vida <= (((*Nave_getSObject()).MaxVida * 1) / 8)) {
		(*Nave_getSObject()).disparosDiagActivos = 0;
	}
}

//***Actualiza el juego
void QuickModeGame_updateGame() {
	if ((*Nave_getSObject()).Puntos >= 1500
			&& QuickModeGameS.igb.llegaA1500 == 0) {
		QuickModeGameS.igb.maxEnemigos = QuickModeGameS.igb.maxEnemigos + 1;
		QuickModeGameS.igb.llegaA1500 = 1;
	}

	if ((*Nave_getSObject()).Puntos > (QuickModeGameS.igb.ultimoAumento + 3000)
			&& QuickModeGameS.igb.llegaA1500 == 1) {
		QuickModeGameS.igb.aumentoEnVidaEnemigo =
				QuickModeGameS.igb.aumentoEnVidaEnemigo + 3;
		QuickModeGameS.igb.tiempoEsperaPlus =
				QuickModeGameS.igb.tiempoEsperaPlus + 5000000;
		QuickModeGameS.igb.aumentoEnVidaJefe =
				QuickModeGameS.igb.aumentoEnVidaJefe + 10000;
		(*Jefe_getSObject()).adicionDanoNave =
				(*Jefe_getSObject()).adicionDanoNave + 3;
		(*DisparoJefe_getSObject()).adicionDanoDisNave =
				(*DisparoJefe_getSObject()).adicionDanoDisNave + 3;
		(*Enemigo_getSObject()).adicionDanoNave =
				(*Enemigo_getSObject()).adicionDanoNave + 3;
		(*DisparoEnemigo_getSObject()).adicionDanoDisNave =
				(*DisparoEnemigo_getSObject()).adicionDanoDisNave + 3;
		QuickModeGameS.igb.maxEnemigos = QuickModeGameS.igb.maxEnemigos + 1;
		QuickModeGameS.igb.ultimoAumento = (*Nave_getSObject()).Puntos;
	}

	/***Saber si es trial o full version
	 if ((*GalacticaMain_getObject()).isTrialMode == 1)
	 {
	 if ((*Nave_getSObject()).Puntos >= 5000)
	 {
	 Almacenamiento_salvarInGame();
	 Audio_playAudio("MainMenu");
	 (*GalacticaMain_getObject()).estadoActual = GMBuy;
	 }
	 }*/
	//**** fin saber si es trial o full //
	if (QuickModeGameS.igb.enemigos.size < QuickModeGameS.igb.maxEnemigos
			&& QuickModeGameS.igb.jefes.size == 0) {
		QuickModeGame_insertaEnemigo();
	}

}

//Actualiza enemigos
void QuickModeGame_updateEnemigos() {
	if (QuickModeGameS.igb.enemigos.size > 0) {
		for (QuickModeGameS.igb.i = 0;
				QuickModeGameS.igb.i < QuickModeGameS.igb.enemigos.size;
				QuickModeGameS.igb.i++) {
			Enemigo_update(
					(Enemigo*) Lista_GetIn(&QuickModeGameS.igb.enemigos,
							QuickModeGameS.igb.i));

			if ((*(Enemigo*) Lista_GetIn(&QuickModeGameS.igb.enemigos,
					QuickModeGameS.igb.i)).estaEnColisionDisp == 1
					|| (*(Enemigo*) Lista_GetIn(&QuickModeGameS.igb.enemigos,
							QuickModeGameS.igb.i)).estaEnColisionNave == 1
					|| (*(Enemigo*) Lista_GetIn(&QuickModeGameS.igb.enemigos,
							QuickModeGameS.igb.i)).estaEnColisionPoder == 1) {
				(*(Enemigo*) Lista_GetIn(&QuickModeGameS.igb.enemigos,
						QuickModeGameS.igb.i)).estaEnColisionDisp = 0;
				(*(Enemigo*) Lista_GetIn(&QuickModeGameS.igb.enemigos,
						QuickModeGameS.igb.i)).estaEnColisionNave = 0;
				(*(Enemigo*) Lista_GetIn(&QuickModeGameS.igb.enemigos,
						QuickModeGameS.igb.i)).estaEnColisionPoder = 0;

				if ((*Animation_getSObject()).animacionesDispAEnemigoTotal.size
						> 0) {
					Animation_setPosition(
							(Animation*) Lista_GetIn(
									&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
									(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
											- 1),
							(*(Enemigo*) Lista_GetIn(
									&QuickModeGameS.igb.enemigos,
									QuickModeGameS.igb.i)).Position);

					Lista_AddInFin(&QuickModeGameS.igb.animacionesQ,
							Lista_GetIn(
									&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
									(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
											- 1));

					Lista_RemoveAt(
							&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
							(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
									- 1);
				}

				if ((*(Enemigo*) Lista_GetIn(&QuickModeGameS.igb.enemigos,
						QuickModeGameS.igb.i)).vida <= 0
						|| (*(Enemigo*) Lista_GetIn(
								&QuickModeGameS.igb.enemigos,
								QuickModeGameS.igb.i)).estaEnColisionNave
								== 1) {
					Audio_playAudio("EstallidoEnemigo");

					(*Nave_getSObject()).Destruidos =
							(*Nave_getSObject()).Destruidos + 1;
					(*(Enemigo*) Lista_GetIn(&QuickModeGameS.igb.enemigos,
							QuickModeGameS.igb.i)).estaEnColisionNave = 0;

					if ((*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
							> 0) {
						Animation_setPosition(
								(Animation*) Lista_GetIn(
										&(*Animation_getSObject()).animacionesEstallidoEnemigoTotal,
										(*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
												- 1),
								(*(Enemigo*) Lista_GetIn(
										&QuickModeGameS.igb.enemigos,
										QuickModeGameS.igb.i)).Position);

						Lista_AddInFin(&QuickModeGameS.igb.animacionesQ,
								Lista_GetIn(
										&(*Animation_getSObject()).animacionesEstallidoEnemigoTotal,
										(*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
												- 1));

						Lista_RemoveAt(
								&(*Animation_getSObject()).animacionesEstallidoEnemigoTotal,
								(*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
										- 1);
					}

					switch ((*(Enemigo*) Lista_GetIn(
							&QuickModeGameS.igb.enemigos, QuickModeGameS.igb.i)).tipoEnemigo) {
					case ENor:
						(*Nave_getSObject()).Puntos =
								(*Nave_getSObject()).Puntos + 10;
						QuickModeGame_confirmaEliminarNor(QuickModeGameS.igb.i);
						break;
					case EKami:
						(*Nave_getSObject()).Puntos =
								(*Nave_getSObject()).Puntos + 20;
						QuickModeGame_EliminarYRetornoEnemigoTotal(
								QuickModeGameS.igb.i);
						break;
					case ESid:
						(*Nave_getSObject()).Puntos =
								(*Nave_getSObject()).Puntos + 30;
						QuickModeGame_confirmaEliminarSid(QuickModeGameS.igb.i);
						break;
					case EHiNor:
						(*Nave_getSObject()).Puntos =
								(*Nave_getSObject()).Puntos + 50;
						QuickModeGame_confirmaEliminarNor(QuickModeGameS.igb.i);
						break;
					case EHiKami:
						(*Nave_getSObject()).Puntos =
								(*Nave_getSObject()).Puntos + 60;
						QuickModeGame_EliminarYRetornoEnemigoTotal(
								QuickModeGameS.igb.i);
						break;
					case EHiSid:
						(*Nave_getSObject()).Puntos =
								(*Nave_getSObject()).Puntos + 80;
						QuickModeGame_confirmaEliminarSid(QuickModeGameS.igb.i);
						break;
					}

				} //si murio el enemigo

			} //si esta wne colision

		} //for mirando cada enemigo

	} //si enemigos count >0
}

void QuickModeGame_gameOver() {
	if ((*Nave_getSObject()).Vida <= 0) {
		(*Nave_getSObject()).Vida = 0;
		(*Nave_getSObject()).EscalaN = GameFramework_Vector2(0, 0);
		Animation_setPosition(
				(*Animation_getSObject()).animacionesEstallidoNave,
				(*Nave_getSObject()).PositionNav);

		if ((*(*Animation_getSObject()).animacionesEstallidoNave).animacionFinalizada
				== 1) {
			if ((*Audio_getSObject()).musicaActiva == 1) {
				Audio_stopAudio("Galactic");
				Audio_playAudio("GameOver");
			}
			Almacenamiento_borrarInGame();

			if ((*Nave_getSObject()).Puntos
					> (*GalacticaMain_getObject()).BESTSCORE) {
				(*GalacticaMain_getObject()).BESTSCORE =
						(*Nave_getSObject()).Puntos;
				Almacenamiento_salvarScore();
			}

			(*QuickMode_getSObject()).estadoActual = QMGameOver;
		}
	}
}

///*******Dibuja enemigos*****
void QuickModeGame_drawEnemigos() {
	if (QuickModeGameS.igb.enemigos.size > 0) {
		for (QuickModeGameS.igb.i = 0;
				QuickModeGameS.igb.i < QuickModeGameS.igb.enemigos.size;
				QuickModeGameS.igb.i++) {
			Enemigo_draw(
					(Enemigo*) Lista_GetIn(&QuickModeGameS.igb.enemigos,
							QuickModeGameS.igb.i));
		}
	}
}

//********************Animaciones**************
void QuickModeGame_updateAnimaciones() {
	if (QuickModeGameS.igb.animacionesQ.size > 0) {
		for (QuickModeGameS.igb.i = 0;
				QuickModeGameS.igb.i < QuickModeGameS.igb.animacionesQ.size;
				QuickModeGameS.igb.i++) {
			Animation_update(
					(Animation*) Lista_GetIn(&QuickModeGameS.igb.animacionesQ,
							QuickModeGameS.igb.i));
		}
	}

	if (QuickModeGameS.igb.animacionesQ.size > 0) {
		for (QuickModeGameS.igb.i = 0;
				QuickModeGameS.igb.i < QuickModeGameS.igb.animacionesQ.size;
				QuickModeGameS.igb.i++) {
			if ((*(Animation*) Lista_GetIn(&QuickModeGameS.igb.animacionesQ,
					QuickModeGameS.igb.i)).animacionFinalizada == 1) {
				(*(Animation*) Lista_GetIn(&QuickModeGameS.igb.animacionesQ,
						QuickModeGameS.igb.i)).animacionFinalizada = 0;

				switch ((*(Animation*) Lista_GetIn(
						&QuickModeGameS.igb.animacionesQ, QuickModeGameS.igb.i)).tipoActual) {
				case ADisparoAEnemigo:
					Lista_AddInFin(
							&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
							Lista_GetIn(&QuickModeGameS.igb.animacionesQ,
									QuickModeGameS.igb.i));
					break;
				case AEstallidoEnemigo:
					Lista_AddInFin(
							&(*Animation_getSObject()).animacionesEstallidoEnemigoTotal,
							Lista_GetIn(&QuickModeGameS.igb.animacionesQ,
									QuickModeGameS.igb.i));
					break;
				case AEstallidoJefe:
					Lista_AddInFin(
							&(*Animation_getSObject()).animacionesEstallidoJefeTotal,
							Lista_GetIn(&QuickModeGameS.igb.animacionesQ,
									QuickModeGameS.igb.i));
					break;
				case ADisparoANave:
					break;
				case AEstallidoNave:
					break;
				case APoderArp:
					break;
				case APoderCarafe:
					break;
				case APoderGalactica:
					break;
				case APoderHelix:
					break;
				case APoderPerseus:
					break;
				case APoderPisces:
					break;
				}
				Lista_RemoveAt(&QuickModeGameS.igb.animacionesQ,
						QuickModeGameS.igb.i);
			}
		}
	}

}

//*******Dibuja animaciones***********
void QuickModeGame_drawAnimaciones() {
	if (QuickModeGameS.igb.animacionesQ.size > 0) {
		for (QuickModeGameS.igb.i = 0;
				QuickModeGameS.igb.i < QuickModeGameS.igb.animacionesQ.size;
				QuickModeGameS.igb.i++) {
			Animation_draw(
					(Animation*) Lista_GetIn(&QuickModeGameS.igb.animacionesQ,
							QuickModeGameS.igb.i));
		}
	}
}

void QuickModeGame_eliminaAnimaciones() {
	QuickModeGameS.igb.count = QuickModeGameS.igb.animacionesQ.size;
	for (QuickModeGameS.igb.i = QuickModeGameS.igb.count - 1;
			QuickModeGameS.igb.i >= 0; QuickModeGameS.igb.i--) {
		(*(Animation*) Lista_RetornaElemento(&(QuickModeGameS.igb.animacionesQ),
				QuickModeGameS.igb.i)).animacionFinalizada = 0;
		switch ((*(Animation*) Lista_GetIn(&QuickModeGameS.igb.animacionesQ,
				QuickModeGameS.igb.i)).tipoActual) {
		case ADisparoAEnemigo:
			Lista_AddInFin(
					&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
					Lista_GetIn(&QuickModeGameS.igb.animacionesQ,
							QuickModeGameS.igb.i));
			break;
		case AEstallidoEnemigo:
			Lista_AddInFin(
					&(*Animation_getSObject()).animacionesEstallidoEnemigoTotal,
					Lista_GetIn(&QuickModeGameS.igb.animacionesQ,
							QuickModeGameS.igb.i));
			break;
		case AEstallidoJefe:
			Lista_AddInFin(
					&(*Animation_getSObject()).animacionesEstallidoJefeTotal,
					Lista_GetIn(&QuickModeGameS.igb.animacionesQ,
							QuickModeGameS.igb.i));
			break;
		case ADisparoANave:
			break;
		case AEstallidoNave:
			break;
		case APoderArp:
			break;
		case APoderCarafe:
			break;
		case APoderGalactica:
			break;
		case APoderHelix:
			break;
		case APoderPerseus:
			break;
		case APoderPisces:
			break;

		}
		Lista_RemoveAt(&QuickModeGameS.igb.animacionesQ, QuickModeGameS.igb.i);
	}
}

//********************ENEMIGOS***********************
//confirma solo estos porque los Kami no dispara
void QuickModeGame_confirmaEliminarNor(int c) {
	if ((*(Enemigo*) Lista_RetornaElemento(&(QuickModeGameS.igb.enemigos), c)).disparosNor.size
			<= 0
			&& (*(Enemigo*) Lista_RetornaElemento(
					&(QuickModeGameS.igb.enemigos), c)).animaciones.size <= 0) {
		QuickModeGame_EliminarYRetornoEnemigoTotal(c);
	} else {
		if ((*(Enemigo*) Lista_RetornaElemento(&(QuickModeGameS.igb.enemigos),
				c)).disparosNor.size > 0) {
			for (QuickModeGameS.igb.i = 0;
					QuickModeGameS.igb.i
							< (*(Enemigo*) Lista_RetornaElemento(
									&(QuickModeGameS.igb.enemigos), c)).disparosNor.size;
					QuickModeGameS.igb.i++) {
				Lista_AddInFin(&(*Enemigo_getSObject()).disparosNorTotal,
						Lista_GetIn(
								&(*(Enemigo*) Lista_GetIn(
										&QuickModeGameS.igb.enemigos, c)).disparosNor,
								QuickModeGameS.igb.i));
				Lista_RemoveAt(
						&((*(Enemigo*) Lista_RetornaElemento(
								&(QuickModeGameS.igb.enemigos), c)).disparosNor),
						QuickModeGameS.igb.i);
			}
			Lista_Destruir(
					&((*(Enemigo*) Lista_RetornaElemento(
							&(QuickModeGameS.igb.enemigos), c)).disparosNor));
		}

		if ((*(Enemigo*) Lista_RetornaElemento(&(QuickModeGameS.igb.enemigos),
				c)).animaciones.size > 0) {
			for (QuickModeGameS.igb.i = 0;
					QuickModeGameS.igb.i
							< (*(Enemigo*) Lista_RetornaElemento(
									&(QuickModeGameS.igb.enemigos), c)).animaciones.size;
					QuickModeGameS.igb.i++) {
				if ((*(Animation*) Lista_RetornaElemento(
						&((*(Enemigo*) Lista_RetornaElemento(
								&(QuickModeGameS.igb.enemigos), c)).animaciones),
						QuickModeGameS.igb.i)).tipoActual == ADisparoANave) {
					Lista_AddInFin(
							&(*Animation_getSObject()).animacionesDispANaveTotal,
							Lista_GetIn(
									&(*(Enemigo*) Lista_GetIn(
											&QuickModeGameS.igb.enemigos, c)).animaciones,
									QuickModeGameS.igb.i));
				}
			}
			Lista_Destruir(
					&((*(Enemigo*) Lista_RetornaElemento(
							&(QuickModeGameS.igb.enemigos), c)).animaciones));
		}
		QuickModeGame_EliminarYRetornoEnemigoTotal(c);
	}
}

void QuickModeGame_confirmaEliminarSid(int c) {
	if ((*(Enemigo*) Lista_RetornaElemento(&(QuickModeGameS.igb.enemigos), c)).disparosSid.size
			<= 0
			&& (*(Enemigo*) Lista_RetornaElemento(
					&(QuickModeGameS.igb.enemigos), c)).animaciones.size <= 0) {
		QuickModeGame_EliminarYRetornoEnemigoTotal(c);
	} else {
		if ((*(Enemigo*) Lista_RetornaElemento(&(QuickModeGameS.igb.enemigos),
				c)).disparosSid.size > 0) {
			for (QuickModeGameS.igb.i = 0;
					QuickModeGameS.igb.i
							< (*(Enemigo*) Lista_GetIn(
									&QuickModeGameS.igb.enemigos, c)).disparosSid.size;
					QuickModeGameS.igb.i++) {
				Lista_AddInFin(&(*Enemigo_getSObject()).disparosSidTotal,
						Lista_GetIn(
								&(*(Enemigo*) Lista_GetIn(
										&QuickModeGameS.igb.enemigos, c)).disparosSid,
								QuickModeGameS.igb.i));
			}
			Lista_Destruir(
					&((*(Enemigo*) Lista_RetornaElemento(
							&(QuickModeGameS.igb.enemigos), c)).disparosSid));
		}

		if ((*(Enemigo*) Lista_RetornaElemento(&(QuickModeGameS.igb.enemigos),
				c)).animaciones.size > 0) {
			for (QuickModeGameS.igb.i = 0;
					QuickModeGameS.igb.i
							< (*(Enemigo*) Lista_GetIn(
									&QuickModeGameS.igb.enemigos, c)).animaciones.size;
					QuickModeGameS.igb.i++) {
				if ((*(Animation*) Lista_GetIn(
						&(*(Enemigo*) Lista_GetIn(&QuickModeGameS.igb.enemigos,
								c)).animaciones, QuickModeGameS.igb.i)).tipoActual
						== ADisparoANave) {
					Lista_AddInFin(
							&(*Animation_getSObject()).animacionesDispANaveTotal,
							Lista_GetIn(
									&(*(Enemigo*) Lista_GetIn(
											&QuickModeGameS.igb.enemigos, c)).animaciones,
									QuickModeGameS.igb.i));
				}
			}
			Lista_Destruir(
					&((*(Enemigo*) Lista_RetornaElemento(
							&(QuickModeGameS.igb.enemigos), c)).animaciones));
		}

		QuickModeGame_EliminarYRetornoEnemigoTotal(c);
	}
}

//Agregar jefe
void QuickModeGame_agregarJefe() {
	QuickModeGameS.igb.num = GameFramework_Rand(1, 4);
	switch (QuickModeGameS.igb.num) {
	case 1:
		Lista_AddInFin(&QuickModeGameS.igb.jefes,
				Lista_GetIn(&(*Jefe_getSObject()).jefesTotales, 0));

		Lista_RemoveAt(&(*Jefe_getSObject()).jefesTotales, 0);
		break;
	case 2:
		Lista_AddInFin(&QuickModeGameS.igb.jefes,
				Lista_GetIn(&(*Jefe_getSObject()).jefesTotales, 1));

		Lista_RemoveAt(&(*Jefe_getSObject()).jefesTotales, 1);
		break;
	case 3:
		Lista_AddInFin(&QuickModeGameS.igb.jefes,
				Lista_GetIn(&(*Jefe_getSObject()).jefesTotales, 2));

		Lista_RemoveAt(&(*Jefe_getSObject()).jefesTotales, 2);
		break;
	case 4:
		Lista_AddInFin(&QuickModeGameS.igb.jefes,
				Lista_GetIn(&(*Jefe_getSObject()).jefesTotales, 3));

		Lista_RemoveAt(&(*Jefe_getSObject()).jefesTotales, 3);
		break;
	}
}

//Agregar jefe
void QuickModeGame_agregarJefe2(Jefe_tipo tipoJe) {
	for (QuickModeGameS.igb.i = 0;
			QuickModeGameS.igb.i < (*Jefe_getSObject()).jefesTotales.size;
			QuickModeGameS.igb.i++) {
		if ((*(Jefe*) Lista_RetornaElemento(
				&((*Jefe_getSObject()).jefesTotales), QuickModeGameS.igb.i)).tipoJefe
				== tipoJe) {
			Lista_InsertaEnFinal(&(QuickModeGameS.igb.jefes),
					(QuickModeGameS.igb.jefes).fin,
					Lista_RetornaElemento(&((*Jefe_getSObject()).jefesTotales),
							QuickModeGameS.igb.i));
			Lista_RemoveAt(&((*Jefe_getSObject()).jefesTotales),
					QuickModeGameS.igb.i);
		}
	}
}

//***Update Jefes
void QuickModeGame_updateJefes() {
	for (QuickModeGameS.igb.i = 0;
			QuickModeGameS.igb.i < QuickModeGameS.igb.jefes.size;
			QuickModeGameS.igb.i++) {
		Jefe_update(
				(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
						QuickModeGameS.igb.i));

		if ((*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
				QuickModeGameS.igb.i)).estaEnColisionDisp == 1) {
			(*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
					QuickModeGameS.igb.i)).estaEnColisionDisp = 0;
			if ((*Animation_getSObject()).animacionesDispAEnemigoTotal.size
					> 0) {
				Animation_setPosition(
						(Animation*) Lista_GetIn(
								&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
								(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
										- 1),
						(*CollitionJefe_getSObject()).positionUltimoDisparo);

				Lista_AddInFin(&QuickModeGameS.igb.animacionesQ,
						Lista_GetIn(
								&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
								(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
										- 1));

				Lista_RemoveAt(
						&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
						(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
								- 1);
			}
		}
		if ((*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
				QuickModeGameS.igb.i)).estaEnColisionPoder == 1) {
			(*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
					QuickModeGameS.igb.i)).estaEnColisionPoder = 0;
			if ((*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
					> 0) {
				Animation_setPosition(
						(Animation*) Lista_GetIn(
								&(*Animation_getSObject()).animacionesEstallidoEnemigoTotal,
								(*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
										- 1),
						(*CollitionJefe_getSObject()).positionUltimoDisparo);

				Lista_AddInFin(&QuickModeGameS.igb.animacionesQ,
						Lista_GetIn(
								&(*Animation_getSObject()).animacionesEstallidoEnemigoTotal,
								(*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
										- 1));

				Lista_RemoveAt(
						&(*Animation_getSObject()).animacionesEstallidoEnemigoTotal,
						(*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
								- 1);
			}
		}
		if ((*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
				QuickModeGameS.igb.i)).vida <= 0) {
			Audio_playAudio("EstallidoJefe");
			if ((*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
					QuickModeGameS.igb.i)).animaciones.size > 0) {
				for (QuickModeGameS.igb.j = 0;
						QuickModeGameS.igb.j
								< (*(Jefe*) Lista_GetIn(
										&QuickModeGameS.igb.jefes,
										QuickModeGameS.igb.i)).animaciones.size;
						QuickModeGameS.igb.j++) {
					if ((*(Animation*) Lista_GetIn(
							&(*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
									QuickModeGameS.igb.i)).animaciones,
							QuickModeGameS.igb.j)).tipoActual
							== ADisparoANave) {
						(*(Animation*) Lista_GetIn(
								&(*(Jefe*) Lista_GetIn(
										&QuickModeGameS.igb.jefes,
										QuickModeGameS.igb.i)).animaciones,
								QuickModeGameS.igb.j)).animacionFinalizada = 0;

						Lista_AddInFin(
								&(*Animation_getSObject()).animacionesDispANaveTotal,
								Lista_GetIn(
										&(*(Jefe*) Lista_GetIn(
												&QuickModeGameS.igb.jefes,
												QuickModeGameS.igb.i)).animaciones,
										QuickModeGameS.igb.j));

						Lista_RemoveAt(
								&(*(Jefe*) Lista_GetIn(
										&QuickModeGameS.igb.jefes,
										QuickModeGameS.igb.i)).animaciones,
								QuickModeGameS.igb.j);
					}
				}
			}

			if ((*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
					QuickModeGameS.igb.i)).disparos.size > 0) {
				for (QuickModeGameS.igb.j = 0;
						QuickModeGameS.igb.j
								< (*(Jefe*) Lista_GetIn(
										&QuickModeGameS.igb.jefes,
										QuickModeGameS.igb.i)).disparos.size;
						QuickModeGameS.igb.j++) {
					if ((*(DisparoJefe*) Lista_GetIn(
							&(*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
									QuickModeGameS.igb.i)).disparos,
							QuickModeGameS.igb.j)).tipoActual == DJnormal) {
						Lista_AddInFin(
								&(*Jefe_getSObject()).disparosNormalTotal,
								Lista_GetIn(
										&(*(Jefe*) Lista_GetIn(
												&QuickModeGameS.igb.jefes,
												QuickModeGameS.igb.i)).disparos,
										QuickModeGameS.igb.j));
					}
				}
				for (QuickModeGameS.igb.j = 0;
						QuickModeGameS.igb.j
								< (*(Jefe*) Lista_GetIn(
										&QuickModeGameS.igb.jefes,
										QuickModeGameS.igb.i)).disparos.size;
						QuickModeGameS.igb.j++) {
					if ((*(DisparoJefe*) Lista_GetIn(
							&(*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
									QuickModeGameS.igb.i)).disparos,
							QuickModeGameS.igb.j)).tipoActual == DJmaximo) {
						Lista_AddInFin(&(*Jefe_getSObject()).disparosMaxTotal,
								Lista_GetIn(
										&(*(Jefe*) Lista_GetIn(
												&QuickModeGameS.igb.jefes,
												QuickModeGameS.igb.i)).disparos,
										QuickModeGameS.igb.j));
					}
				}
			}
			Lista_Destruir(
					&(*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
							QuickModeGameS.igb.i)).disparos);

			if ((*Animation_getSObject()).animacionesEstallidoJefeTotal.size
					> 0) {
				Animation_setPosition(
						(Animation*) Lista_GetIn(
								&(*Animation_getSObject()).animacionesEstallidoJefeTotal,
								(*Animation_getSObject()).animacionesEstallidoJefeTotal.size
										- 1),
						(*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
								QuickModeGameS.igb.i)).Position);

				Lista_AddInFin(&QuickModeGameS.igb.animacionesQ,
						Lista_GetIn(
								&(*Animation_getSObject()).animacionesEstallidoJefeTotal,
								(*Animation_getSObject()).animacionesEstallidoJefeTotal.size
										- 1));

				Lista_RemoveAt(
						&(*Animation_getSObject()).animacionesEstallidoJefeTotal,
						(*Animation_getSObject()).animacionesEstallidoJefeTotal.size
								- 1);
			}

			(*Nave_getSObject()).Puntos = (*Nave_getSObject()).Puntos + 10000;
			BarraVida_reiniciaColor(QuickModeGameS.igb.barraVidaJefe);

			Jefe_reinicia(
					&(*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
							QuickModeGameS.igb.i)), 300, 2048,
					QuickModeGameS.igb.aumentoEnVidaJefe);

			Lista_AddInFin(&(*Jefe_getSObject()).jefesTotales,
					Lista_GetIn(&QuickModeGameS.igb.jefes,
							QuickModeGameS.igb.i));

			Lista_RemoveAt(&QuickModeGameS.igb.jefes, QuickModeGameS.igb.i);
		}
	}

	if (QuickModeGameS.igb.jefes.size > 0) {
		BarraVida_update(QuickModeGameS.igb.barraVidaJefe,
				(*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes, 0)).vida,
				(*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes, 0)).maxVida);

		QuickModeGame_eliminarTodosLosEnemigos();
	}

	if ((*Nave_getSObject()).Puntos
			> (QuickModeGameS.igb.ultimaAdicionJefe + 8000)) {
		QuickModeGame_agregarJefe();
		QuickModeGameS.igb.ultimaAdicionJefe = (*Nave_getSObject()).Puntos
				+ (10000);
	}

}

//******Dibujar jefes
void QuickModeGame_drawJefes() {
	for (QuickModeGameS.igb.i = 0;
			QuickModeGameS.igb.i < QuickModeGameS.igb.jefes.size;
			QuickModeGameS.igb.i++) {
		Jefe_draw(
				(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
						QuickModeGameS.igb.i));
	}
	if (QuickModeGameS.igb.jefes.size > 0) {
		BarraVida_draw(QuickModeGameS.igb.barraVidaJefe);
	}
}

//***********************ESTRELLAS*******************
//****Carga estrellas*
void QuickModeGame_loadEstrellas() {
	for (QuickModeGameS.igb.i = 0; QuickModeGameS.igb.i < 50;
			QuickModeGameS.igb.i++) {
		Lista_AddInFin(&QuickModeGameS.igb.estrellas,
				Estrella_crear(
						GameFramework_Vector2(GameFramework_Rand(0, 600),
								GameFramework_Rand(0, 1024))));

		(*(Estrella*) Lista_GetIn(&QuickModeGameS.igb.estrellas,
				QuickModeGameS.igb.i)).color = GameFramework_Color(
				GameFramework_getByteColorToFloatColor(
						GameFramework_Rand(200, 255)),
				GameFramework_getByteColorToFloatColor(
						GameFramework_Rand(200, 255)),
				GameFramework_getByteColorToFloatColor(
						GameFramework_Rand(200, 255)), 1);

		double r = GameFramework_Rand0A1();
		(*(Estrella*) Lista_GetIn(&QuickModeGameS.igb.estrellas,
				QuickModeGameS.igb.i)).Escala = GameFramework_Vector2(r, r);

		(*(Estrella*) Lista_GetIn(&QuickModeGameS.igb.estrellas,
				QuickModeGameS.igb.i)).vel = GameFramework_Rand(4, 25);
	}
}

//****Actualiza estrellas**
void QuickModeGame_updateEstrellas() {
	for (QuickModeGameS.igb.i = 0; QuickModeGameS.igb.i < 50;
			QuickModeGameS.igb.i++) {
		Estrella_update(
				(Estrella*) Lista_GetIn(&QuickModeGameS.igb.estrellas,
						QuickModeGameS.igb.i));
	}
}

//****Dibuja estrellas*
void QuickModeGame_drawEstrellas() {
	for (QuickModeGameS.igb.i = 0; QuickModeGameS.igb.i < 50;
			QuickModeGameS.igb.i++) {
		Estrella_draw(
				(Estrella*) Lista_GetIn(&QuickModeGameS.igb.estrellas,
						QuickModeGameS.igb.i));
	}
}

//******Actualiza Mensaje Inicial
void QuickModeGame_updateMensajeInicial() {
	if (QuickModeGameS.igb.mensajeInicialActivo == 1) {
		QuickModeGameS.igb.EscalaMensaje =
				QuickModeGameS.igb.actualFrameMensaje;

		QuickModeGameS.igb.tiempoAyuda += GameFramework_getSwapTime();
		if (QuickModeGameS.igb.tiempoAyuda
				>= ((GameFramework_getSwapTime()) * 10)) {
			QuickModeGameS.igb.actualFrameMensaje =
					QuickModeGameS.igb.actualFrameMensaje
							+ QuickModeGameS.igb.escalaInicialMensaje;

			QuickModeGameS.igb.tiempoAyuda = 0;
		}

		if (QuickModeGameS.igb.actualFrameMensaje
				> QuickModeGameS.igb.escalaInicialMensaje) {
			QuickModeGameS.igb.actualFrameMensaje = 0;
		}

		if (GameFramework_getTotalTime()
				> QuickModeGameS.igb.tiempoTitileoMensaje + 2000000) {
			QuickModeGameS.igb.EscalaMensaje =
					QuickModeGameS.igb.escalaInicialMensaje;

			QuickModeGameS.igb.mensajeInicialActivo = 0;
		}
	}
}

//********Dibujo Mensaje Inicial
void QuickModeGame_drawMensajeInicial() {
	if (QuickModeGameS.igb.mensajeInicialActivo == 1) {
		Dibujar_drawText((*Textura_getSObject()).Fuente, "Quick Mode",
				GameFramework_Vector2(100, 650),
				GameFramework_Color(GameFramework_getByteColorToFloatColor(7),
						GameFramework_getByteColorToFloatColor(237),
						GameFramework_getByteColorToFloatColor(134), 1),
				QuickModeGameS.igb.EscalaMensaje);

		Dibujar_drawText((*Textura_getSObject()).Fuente, "Start!!",
				GameFramework_Vector2(150, 600),
				GameFramework_Color(GameFramework_getByteColorToFloatColor(7),
						GameFramework_getByteColorToFloatColor(237),
						GameFramework_getByteColorToFloatColor(134), 1),
				QuickModeGameS.igb.EscalaMensaje);
	}
}

//********Eliminar todos los enemigos
void QuickModeGame_eliminarTodosLosEnemigos() {
	if (QuickModeGameS.igb.enemigos.size > 0) {
		for (QuickModeGameS.igb.z = QuickModeGameS.igb.enemigos.size - 1;
				QuickModeGameS.igb.z >= 0; QuickModeGameS.igb.z--) {
			if ((*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
					> 0) {
				Animation_setPosition(
						(Animation*) Lista_GetIn(
								&(*Animation_getSObject()).animacionesEstallidoEnemigoTotal,
								(*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
										- 1),
						(*(Enemigo*) Lista_GetIn(&QuickModeGameS.igb.enemigos,
								QuickModeGameS.igb.z)).Position);

				Lista_AddInFin(&QuickModeGameS.igb.animacionesQ,
						Lista_GetIn(
								&(*Animation_getSObject()).animacionesEstallidoEnemigoTotal,
								(*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
										- 1));

				Lista_RemoveAt(
						&(*Animation_getSObject()).animacionesEstallidoEnemigoTotal,
						(*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
								- 1);
			}

			switch ((*(Enemigo*) Lista_GetIn(&QuickModeGameS.igb.enemigos,
					QuickModeGameS.igb.z)).tipoEnemigo) {
			case ENor:
				(*Nave_getSObject()).Puntos = (*Nave_getSObject()).Puntos + 10;
				QuickModeGame_confirmaEliminarNor(QuickModeGameS.igb.z);
				break;
			case EKami:
				(*Nave_getSObject()).Puntos = (*Nave_getSObject()).Puntos + 20;
				QuickModeGame_EliminarYRetornoEnemigoTotal(
						QuickModeGameS.igb.z);
				break;
			case ESid:
				(*Nave_getSObject()).Puntos = (*Nave_getSObject()).Puntos + 30;
				QuickModeGame_confirmaEliminarSid(QuickModeGameS.igb.z);
				break;
			case EHiNor:
				(*Nave_getSObject()).Puntos = (*Nave_getSObject()).Puntos + 50;
				QuickModeGame_confirmaEliminarNor(QuickModeGameS.igb.z);
				break;
			case EHiKami:
				(*Nave_getSObject()).Puntos = (*Nave_getSObject()).Puntos + 60;
				QuickModeGame_EliminarYRetornoEnemigoTotal(
						QuickModeGameS.igb.z);
				break;
			case EHiSid:
				(*Nave_getSObject()).Puntos = (*Nave_getSObject()).Puntos + 80;
				QuickModeGame_confirmaEliminarSid(QuickModeGameS.igb.z);
				break;
			}
			(*Nave_getSObject()).Destruidos = (*Nave_getSObject()).Destruidos
					+ 1;
		}
	}
}

//********Eliminar todos los enemigos
void QuickModeGame_eliminarTodos() {
	for (QuickModeGameS.igb.k = QuickModeGameS.igb.enemigos.size - 1;
			QuickModeGameS.igb.k >= 0; QuickModeGameS.igb.k--) {
		(*(Enemigo*) Lista_GetIn(&(QuickModeGameS.igb.enemigos),
				QuickModeGameS.igb.k)).estaEnColisionDisp = 0;
		(*(Enemigo*) Lista_GetIn(&(QuickModeGameS.igb.enemigos),
				QuickModeGameS.igb.k)).estaEnColisionNave = 0;
		(*(Enemigo*) Lista_GetIn(&(QuickModeGameS.igb.enemigos),
				QuickModeGameS.igb.k)).estaEnColisionPoder = 0;
		switch ((*(Enemigo*) Lista_GetIn(&(QuickModeGameS.igb.enemigos),
				QuickModeGameS.igb.k)).tipoEnemigo) {
		case ENor:
			QuickModeGame_confirmaEliminarNor(QuickModeGameS.igb.k);
			break;
		case EKami:
			QuickModeGame_EliminarYRetornoEnemigoTotal(QuickModeGameS.igb.k);
			break;
		case ESid:
			QuickModeGame_confirmaEliminarSid(QuickModeGameS.igb.k);
			break;
		case EHiNor:
			QuickModeGame_confirmaEliminarNor(QuickModeGameS.igb.k);
			break;
		case EHiKami:
			QuickModeGame_EliminarYRetornoEnemigoTotal(QuickModeGameS.igb.k);
			break;
		case EHiSid:
			QuickModeGame_confirmaEliminarSid(QuickModeGameS.igb.k);
			break;
		}
	}
}

void QuickModeGame_EliminarYRetornoEnemigoTotal(int indice) {
	switch ((*(Enemigo*) Lista_GetIn(&QuickModeGameS.igb.enemigos, indice)).tipoEnemigo) {
	case ENor:
		Lista_AddInFin(&(*Enemigo_getSObject()).NorTotal,
				Lista_GetIn(&QuickModeGameS.igb.enemigos, indice));
		break;
	case EKami:
		Lista_AddInFin(&(*Enemigo_getSObject()).KamiTotal,
				Lista_GetIn(&QuickModeGameS.igb.enemigos, indice));
		break;
	case ESid:
		Lista_AddInFin(&(*Enemigo_getSObject()).SidTotal,
				Lista_GetIn(&QuickModeGameS.igb.enemigos, indice));
		break;
	case EHiNor:
		Lista_AddInFin(&(*Enemigo_getSObject()).HiNorTotal,
				Lista_GetIn(&QuickModeGameS.igb.enemigos, indice));
		break;
	case EHiKami:
		Lista_AddInFin(&(*Enemigo_getSObject()).HiKamiTotal,
				Lista_GetIn(&QuickModeGameS.igb.enemigos, indice));
		break;
	case EHiSid:
		Lista_AddInFin(&(*Enemigo_getSObject()).HiSidTotal,
				Lista_GetIn(&QuickModeGameS.igb.enemigos, indice));
		break;
	}
	Lista_RemoveAt(&QuickModeGameS.igb.enemigos, indice);
}

//Eliminar Jefes
void QuickModeGame_eliminarJefes() {

	if (QuickModeGameS.igb.jefes.size > 0) {
		for (QuickModeGameS.igb.i = 0;
				QuickModeGameS.igb.i < QuickModeGameS.igb.jefes.size;
				QuickModeGameS.igb.i++) {
			if ((*(Jefe*) Lista_RetornaElemento(&(QuickModeGameS.igb.jefes),
					QuickModeGameS.igb.i)).animaciones.size > 0) {
				for (QuickModeGameS.igb.j = 0;
						QuickModeGameS.igb.j
								< (*(Jefe*) Lista_RetornaElemento(
										&(QuickModeGameS.igb.jefes),
										QuickModeGameS.igb.i)).animaciones.size;
						QuickModeGameS.igb.j++) {
					if ((*(Animation*) Lista_RetornaElemento(
							&((*(Jefe*) Lista_RetornaElemento(
									&(QuickModeGameS.igb.jefes),
									QuickModeGameS.igb.i)).animaciones),
							QuickModeGameS.igb.j)).tipoActual
							== ADisparoANave) {
						(*(Animation*) Lista_RetornaElemento(
								&((*(Jefe*) Lista_RetornaElemento(
										&(QuickModeGameS.igb.jefes),
										QuickModeGameS.igb.i)).animaciones),
								QuickModeGameS.igb.j)).animacionFinalizada = 0;

						Lista_AddInFin(
								&(*Animation_getSObject()).animacionesDispANaveTotal,
								Lista_GetIn(
										&(*(Jefe*) Lista_GetIn(
												&QuickModeGameS.igb.jefes,
												QuickModeGameS.igb.i)).animaciones,
										QuickModeGameS.igb.j));

						Lista_RemoveAt(
								&((*(Jefe*) Lista_RetornaElemento(
										&(QuickModeGameS.igb.jefes),
										QuickModeGameS.igb.i)).animaciones),
								QuickModeGameS.igb.j);
					}
				}
			}

			if ((*(Jefe*) Lista_RetornaElemento(&(QuickModeGameS.igb.jefes),
					QuickModeGameS.igb.i)).disparos.size > 0) {
				for (QuickModeGameS.igb.j = 0;
						QuickModeGameS.igb.j
								< (*(Jefe*) Lista_RetornaElemento(
										&(QuickModeGameS.igb.jefes),
										QuickModeGameS.igb.i)).disparos.size;
						QuickModeGameS.igb.j++) {
					if ((*(DisparoJefe*) Lista_GetIn(
							&(*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
									QuickModeGameS.igb.i)).disparos,
							QuickModeGameS.igb.j)).tipoActual == DJnormal) {
						Lista_AddInFin(
								&(*Jefe_getSObject()).disparosNormalTotal,
								Lista_GetIn(
										&(*(Jefe*) Lista_GetIn(
												&QuickModeGameS.igb.jefes,
												QuickModeGameS.igb.i)).disparos,
										QuickModeGameS.igb.j));
					}
				}
				for (QuickModeGameS.igb.j = 0;
						QuickModeGameS.igb.j
								< (*(Jefe*) Lista_GetIn(
										&QuickModeGameS.igb.jefes,
										QuickModeGameS.igb.i)).disparos.size;
						QuickModeGameS.igb.j++) {
					if ((*(DisparoJefe*) Lista_GetIn(
							&(*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
									QuickModeGameS.igb.i)).disparos,
							QuickModeGameS.igb.j)).tipoActual == DJmaximo) {
						Lista_AddInFin(&(*Jefe_getSObject()).disparosMaxTotal,
								Lista_GetIn(
										&(*(Jefe*) Lista_GetIn(
												&QuickModeGameS.igb.jefes,
												QuickModeGameS.igb.i)).disparos,
										QuickModeGameS.igb.j));
					}
				}
			}

			Lista_Destruir(
					&((*(Jefe*) Lista_RetornaElemento(
							&(QuickModeGameS.igb.jefes), QuickModeGameS.igb.i)).disparos));
			BarraVida_reiniciaColor(QuickModeGameS.igb.barraVidaJefe);
			Jefe_reinicia(
					(Jefe*) Lista_RetornaElemento(&(QuickModeGameS.igb.jefes),
							QuickModeGameS.igb.i), 300, 2048,
					QuickModeGameS.igb.aumentoEnVidaJefe);
			Lista_RemoveAt(&(QuickModeGameS.igb.jefes), QuickModeGameS.igb.i);
		}
	}
}

//******Dibuja Estadisticas
void QuickModeGame_drawEstadisticas() {
	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "DispSid: ",
					GameFramework_EnteroACadena(
							(*Enemigo_getSObject()).disparosSidTotal.size)),
			GameFramework_Vector2(0, 25), GameFramework_Color(1, 1, 1, 1), 1);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "DispNor: ",
					GameFramework_EnteroACadena(
							(*Enemigo_getSObject()).disparosNorTotal.size)),
			GameFramework_Vector2(0, 50), GameFramework_Color(1, 1, 1, 1), 1);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "AnimDisANav: ",
					GameFramework_EnteroACadena(
							(*Animation_getSObject()).animacionesDispANaveTotal.size)),
			GameFramework_Vector2(0, 75), GameFramework_Color(1, 1, 1, 1), 1);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "AnimDisAEne: ",
					GameFramework_EnteroACadena(
							(*Animation_getSObject()).animacionesDispAEnemigoTotal.size)),
			GameFramework_Vector2(0, 100), GameFramework_Color(1, 1, 1, 1), 1);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(6, "DispNave/Iz/De: ",
					GameFramework_EnteroACadena(
							(*Nave_getSObject()).disparosTotales.size), "/",
					GameFramework_EnteroACadena(
							(*Nave_getSObject()).disparosDiagIzquierdaTotales.size),
					"/",
					GameFramework_EnteroACadena(
							(*Nave_getSObject()).disparosDiagDerechaTotales.size)),
			GameFramework_Vector2(0, 125), GameFramework_Color(1, 1, 1, 1), 1);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "AnimEstall: ",
					GameFramework_EnteroACadena(
							(*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size)),
			GameFramework_Vector2(0, 150), GameFramework_Color(1, 1, 1, 1), 1);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "PoderGalact: ",
					GameFramework_EnteroACadena(
							(*Nave_getSObject()).poderGalacticaTotal.size)),
			GameFramework_Vector2(0, 175), GameFramework_Color(1, 1, 1, 1), 1);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(4, "DisparosJefe: ",
					GameFramework_EnteroACadena(
							(*Jefe_getSObject()).disparosMaxTotal.size), ";",
					GameFramework_EnteroACadena(
							(*Jefe_getSObject()).disparosNormalTotal.size)),
			GameFramework_Vector2(0, 200), GameFramework_Color(1, 1, 1, 1), 1);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "DisparosSuperNav: ",
					GameFramework_EnteroACadena(
							(*Nave_getSObject()).disparosSuperTotales.size)),
			GameFramework_Vector2(0, 225), GameFramework_Color(1, 1, 1, 1), 1);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "PoderHelix: ",
					GameFramework_EnteroACadena(
							(*Nave_getSObject()).poderHelixTotal.size)),
			GameFramework_Vector2(0, 250), GameFramework_Color(1, 1, 1, 1), 1);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "PoderArp: ",
					GameFramework_EnteroACadena(
							(*Nave_getSObject()).poderArpTotal.size)),
			GameFramework_Vector2(0, 275), GameFramework_Color(1, 1, 1, 1), 1);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "PoderPerseus: ",
					GameFramework_EnteroACadena(
							(*Nave_getSObject()).poderPerseusTotal.size)),
			GameFramework_Vector2(0, 300), GameFramework_Color(1, 1, 1, 1), 1);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "PoderPisces: ",
					GameFramework_EnteroACadena(
							(*Nave_getSObject()).poderPiscesTotal.size)),
			GameFramework_Vector2(0, 325), GameFramework_Color(1, 1, 1, 1), 1);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "AnimEstallJefe: ",
					GameFramework_EnteroACadena(
							(*Animation_getSObject()).animacionesEstallidoJefeTotal.size)),
			GameFramework_Vector2(0, 350), GameFramework_Color(1, 1, 1, 1), 1);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "AnimEstallJefe: ",
					GameFramework_EnteroACadena(
							(*Animation_getSObject()).animacionesEstallidoJefeTotal.size)),
			GameFramework_Vector2(0, 350), GameFramework_Color(1, 1, 1, 1), 1);

	if (QuickModeGameS.igb.jefes.size > 0) {
		Dibujar_drawText((*Textura_getSObject()).Fuente,
				GameFramework_Concatenar(4, "VidaJefe: ",
						GameFramework_EnteroACadena(
								(*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
										0)).vida), "MVJ:",
						GameFramework_EnteroACadena(
								(*(Jefe*) Lista_GetIn(&QuickModeGameS.igb.jefes,
										0)).maxVida)),
				GameFramework_Vector2(0, 375), GameFramework_Color(1, 1, 1, 1),
				1);
	}

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "TotalJefes: ",
					GameFramework_EnteroACadena(
							(*Jefe_getSObject()).jefesTotales.size)),
			GameFramework_Vector2(0, 400), GameFramework_Color(1, 1, 1, 1), 1);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(6, "PoderCarafe D/I/M: ",
					GameFramework_EnteroACadena(
							(*Nave_getSObject()).poderCarafeDerechaTotal.size),
					"/",
					GameFramework_EnteroACadena(
							(*Nave_getSObject()).poderCarafeIzquierdaTotal.size),
					"/",
					GameFramework_EnteroACadena(
							(*Nave_getSObject()).poderCarafeMedioTotal.size)),
			GameFramework_Vector2(0, 425), GameFramework_Color(1, 1, 1, 1), 1);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(6, "TotEnemig: ",
					GameFramework_EnteroACadena(
							(*Enemigo_getSObject()).NorTotal.size), "\n",
					GameFramework_EnteroACadena(
							(*Enemigo_getSObject()).KamiTotal.size), "\n",
					GameFramework_EnteroACadena(
							(*Enemigo_getSObject()).SidTotal.size)),
			GameFramework_Vector2(0, 500), GameFramework_Color(1, 1, 1, 1), 1);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(6, "\n",
					GameFramework_EnteroACadena(
							(*Enemigo_getSObject()).HiNorTotal.size), "\n",
					GameFramework_EnteroACadena(
							(*Enemigo_getSObject()).HiKamiTotal.size), "\n",
					GameFramework_EnteroACadena(
							(*Enemigo_getSObject()).HiSidTotal.size)),
			GameFramework_Vector2(0, 580), GameFramework_Color(1, 1, 1, 1), 1);
}

void QuickModeGame_insertaEnemigo() {
	QuickModeGameS.igb.num = 0;
	QuickModeGameS.igb.num2 = 0;

	if ((*Nave_getSObject()).Puntos > 1500) {
		QuickModeGameS.igb.num = GameFramework_Rand(1, 20);
	} else {
		QuickModeGameS.igb.num = GameFramework_Rand(1, 15);
	}

	if (QuickModeGameS.igb.num == 1 || QuickModeGameS.igb.num == 2
			|| QuickModeGameS.igb.num == 3 || QuickModeGameS.igb.num == 4
			|| QuickModeGameS.igb.num == 5 || QuickModeGameS.igb.num == 6) {
		QuickModeGameS.igb.num2 = GameFramework_Rand(1, 2);
		if (QuickModeGameS.igb.num2 == 1) {
			if ((*Enemigo_getSObject()).NorTotal.size > 0) {
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).NorTotal),
								(*Enemigo_getSObject()).NorTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						QuickModeGameS.igb.velocidadEnem, 0,
						QuickModeGameS.igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&(QuickModeGameS.igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).NorTotal),
								(*Enemigo_getSObject()).NorTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).NorTotal),
						(*Enemigo_getSObject()).NorTotal.size - 1);
			}
		}
		if (QuickModeGameS.igb.num2 == 2) {
			if ((*Enemigo_getSObject()).NorTotal.size > 0) {
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).NorTotal),
								(*Enemigo_getSObject()).NorTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						QuickModeGameS.igb.velocidadEnem, 0,
						QuickModeGameS.igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&(QuickModeGameS.igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).NorTotal),
								(*Enemigo_getSObject()).NorTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).NorTotal),
						(*Enemigo_getSObject()).NorTotal.size - 1);
			}
		}
	}
	if (QuickModeGameS.igb.num == 7 || QuickModeGameS.igb.num == 8
			|| QuickModeGameS.igb.num == 9 || QuickModeGameS.igb.num == 10
			|| QuickModeGameS.igb.num == 11) {
		QuickModeGameS.igb.num2 = GameFramework_Rand(1, 3);
		if (QuickModeGameS.igb.num2 == 1) {
			if ((*Enemigo_getSObject()).KamiTotal.size > 0) {
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						QuickModeGameS.igb.velocidadEnem, 0,
						QuickModeGameS.igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&(QuickModeGameS.igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).KamiTotal),
						(*Enemigo_getSObject()).KamiTotal.size - 1);
			}
		}
		if (QuickModeGameS.igb.num2 == 2) {
			if ((*Enemigo_getSObject()).KamiTotal.size > 0) {
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						QuickModeGameS.igb.velocidadEnem, 0,
						QuickModeGameS.igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&(QuickModeGameS.igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).KamiTotal),
						(*Enemigo_getSObject()).KamiTotal.size - 1);
			}
		}
	}
	if (QuickModeGameS.igb.num == 12 || QuickModeGameS.igb.num == 13
			|| QuickModeGameS.igb.num == 14 || QuickModeGameS.igb.num == 15) {
		QuickModeGameS.igb.num2 = GameFramework_Rand(1, 3);
		if (QuickModeGameS.igb.num2 == 1) {
			if ((*Enemigo_getSObject()).SidTotal.size > 0) {
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						QuickModeGameS.igb.velocidadEnem,
						QuickModeGameS.igb.velocidadEnemY,
						QuickModeGameS.igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&(QuickModeGameS.igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).SidTotal),
						(*Enemigo_getSObject()).SidTotal.size - 1);
			}
		}
		if (QuickModeGameS.igb.num2 == 2) {
			if ((*Enemigo_getSObject()).SidTotal.size > 0) {
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						QuickModeGameS.igb.velocidadEnem,
						QuickModeGameS.igb.velocidadEnemY,
						QuickModeGameS.igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&(QuickModeGameS.igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).SidTotal),
						(*Enemigo_getSObject()).SidTotal.size - 1);
			}
		}
	}
	if (QuickModeGameS.igb.num == 16 || QuickModeGameS.igb.num == 17) {
		QuickModeGameS.igb.num2 = GameFramework_Rand(1, 2);
		if (QuickModeGameS.igb.num2 == 1) {
			if ((*Enemigo_getSObject()).HiNorTotal.size > 0) {
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).HiNorTotal),
								(*Enemigo_getSObject()).HiNorTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						QuickModeGameS.igb.velocidadEnem, 0,
						QuickModeGameS.igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&(QuickModeGameS.igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).HiNorTotal),
								(*Enemigo_getSObject()).HiNorTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).HiNorTotal),
						(*Enemigo_getSObject()).HiNorTotal.size - 1);
			}
		}
		if (QuickModeGameS.igb.num2 == 2) {
			if ((*Enemigo_getSObject()).HiNorTotal.size > 0) {
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).HiNorTotal),
								(*Enemigo_getSObject()).HiNorTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						QuickModeGameS.igb.velocidadEnem, 0,
						QuickModeGameS.igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&(QuickModeGameS.igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).HiNorTotal),
								(*Enemigo_getSObject()).HiNorTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).HiNorTotal),
						(*Enemigo_getSObject()).HiNorTotal.size - 1);
			}
		}
	}
	if (QuickModeGameS.igb.num == 18 || QuickModeGameS.igb.num == 19) {
		QuickModeGameS.igb.num2 = GameFramework_Rand(1, 3);
		if (QuickModeGameS.igb.num2 == 1) {
			if ((*Enemigo_getSObject()).HiKamiTotal.size > 0) {
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).HiKamiTotal),
								(*Enemigo_getSObject()).HiKamiTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						QuickModeGameS.igb.velocidadEnem, 0,
						QuickModeGameS.igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&(QuickModeGameS.igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).HiKamiTotal),
								(*Enemigo_getSObject()).HiKamiTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).HiKamiTotal),
						(*Enemigo_getSObject()).HiKamiTotal.size - 1);
			}
		}
		if (QuickModeGameS.igb.num2 == 2) {
			if ((*Enemigo_getSObject()).HiKamiTotal.size > 0) {
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).HiKamiTotal),
								(*Enemigo_getSObject()).HiKamiTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						QuickModeGameS.igb.velocidadEnem, 0,
						QuickModeGameS.igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&(QuickModeGameS.igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).HiKamiTotal),
								(*Enemigo_getSObject()).HiKamiTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).HiKamiTotal),
						(*Enemigo_getSObject()).HiKamiTotal.size - 1);
			}
		}
	}
	if (QuickModeGameS.igb.num == 20) {
		QuickModeGameS.igb.num2 = GameFramework_Rand(1, 3);
		if (QuickModeGameS.igb.num2 == 1) {
			if ((*Enemigo_getSObject()).HiSidTotal.size > 0) {
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).HiSidTotal),
								(*Enemigo_getSObject()).HiSidTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						QuickModeGameS.igb.velocidadEnem,
						QuickModeGameS.igb.velocidadEnemY,
						QuickModeGameS.igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&(QuickModeGameS.igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).HiSidTotal),
								(*Enemigo_getSObject()).HiSidTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).HiSidTotal),
						(*Enemigo_getSObject()).HiSidTotal.size - 1);
			}
		}
		if (QuickModeGameS.igb.num2 == 2) {
			if ((*Enemigo_getSObject()).HiSidTotal.size > 0) {
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).HiSidTotal),
								(*Enemigo_getSObject()).HiSidTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						QuickModeGameS.igb.velocidadEnem,
						QuickModeGameS.igb.velocidadEnemY,
						QuickModeGameS.igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&(QuickModeGameS.igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).HiSidTotal),
								(*Enemigo_getSObject()).HiSidTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).HiSidTotal),
						(*Enemigo_getSObject()).HiSidTotal.size - 1);
			}
		}
	}

}

void QuickModeGame_LoadEnemigos() {
	QuickModeGameS.igb.num = 0;
	QuickModeGameS.igb.num2 = 0;

	for (QuickModeGameS.igb.i = 0; QuickModeGameS.igb.i < 2;
			QuickModeGameS.igb.i++) {
		QuickModeGameS.igb.num = GameFramework_Rand(1, 3);
		if (QuickModeGameS.igb.num == 1) {
			QuickModeGameS.igb.num2 = GameFramework_Rand(1, 2);
			if (QuickModeGameS.igb.num2 == 1) {
				if ((*Enemigo_getSObject()).NorTotal.size > 0) {
					Enemigo_reinicia(
							(Enemigo*) Lista_GetIn(
									&((*Enemigo_getSObject()).NorTotal),
									(*Enemigo_getSObject()).NorTotal.size - 1),
							(-1) * GameFramework_Rand(100, 500),
							GameFramework_Rand(600, 1000),
							QuickModeGameS.igb.velocidadEnem, 0,
							QuickModeGameS.igb.aumentoEnVidaEnemigo);

					Lista_AddInFin(&(QuickModeGameS.igb.enemigos),
							Lista_GetIn(&((*Enemigo_getSObject()).NorTotal),
									(*Enemigo_getSObject()).NorTotal.size - 1));

					Lista_RemoveAt(&((*Enemigo_getSObject()).NorTotal),
							(*Enemigo_getSObject()).NorTotal.size - 1);
				}
			}
			if (QuickModeGameS.igb.num2 == 2) {
				if ((*Enemigo_getSObject()).NorTotal.size > 0) {
					Enemigo_reinicia(
							(Enemigo*) Lista_GetIn(
									&((*Enemigo_getSObject()).NorTotal),
									(*Enemigo_getSObject()).NorTotal.size - 1),
							GameFramework_Rand(1000, 1500),
							GameFramework_Rand(600, 1000),
							QuickModeGameS.igb.velocidadEnem, 0,
							QuickModeGameS.igb.aumentoEnVidaEnemigo);

					Lista_AddInFin(&(QuickModeGameS.igb.enemigos),
							Lista_GetIn(&((*Enemigo_getSObject()).NorTotal),
									(*Enemigo_getSObject()).NorTotal.size - 1));

					Lista_RemoveAt(&((*Enemigo_getSObject()).NorTotal),
							(*Enemigo_getSObject()).NorTotal.size - 1);
				}
			}
		}
		if (QuickModeGameS.igb.num == 2) {
			QuickModeGameS.igb.num2 = GameFramework_Rand(1, 3);
			if (QuickModeGameS.igb.num2 == 1) {
				if ((*Enemigo_getSObject()).KamiTotal.size > 0) {
					Enemigo_reinicia(
							(Enemigo*) Lista_GetIn(
									&((*Enemigo_getSObject()).KamiTotal),
									(*Enemigo_getSObject()).KamiTotal.size - 1),
							(-1) * GameFramework_Rand(100, 500),
							GameFramework_Rand(600, 1000),
							QuickModeGameS.igb.velocidadEnem, 0,
							QuickModeGameS.igb.aumentoEnVidaEnemigo);

					Lista_AddInFin(&(QuickModeGameS.igb.enemigos),
							Lista_GetIn(&((*Enemigo_getSObject()).KamiTotal),
									(*Enemigo_getSObject()).KamiTotal.size
											- 1));

					Lista_RemoveAt(&((*Enemigo_getSObject()).KamiTotal),
							(*Enemigo_getSObject()).KamiTotal.size - 1);
				}
			}
			if (QuickModeGameS.igb.num2 == 2) {
				if ((*Enemigo_getSObject()).KamiTotal.size > 0) {
					Enemigo_reinicia(
							(Enemigo*) Lista_GetIn(
									&((*Enemigo_getSObject()).KamiTotal),
									(*Enemigo_getSObject()).KamiTotal.size - 1),
							GameFramework_Rand(1000, 1500),
							GameFramework_Rand(600, 1000),
							QuickModeGameS.igb.velocidadEnem, 0,
							QuickModeGameS.igb.aumentoEnVidaEnemigo);

					Lista_AddInFin(&(QuickModeGameS.igb.enemigos),
							Lista_GetIn(&((*Enemigo_getSObject()).KamiTotal),
									(*Enemigo_getSObject()).KamiTotal.size
											- 1));

					Lista_RemoveAt(&((*Enemigo_getSObject()).KamiTotal),
							(*Enemigo_getSObject()).KamiTotal.size - 1);
				}
			}
		}
		if (QuickModeGameS.igb.num == 3) {
			QuickModeGameS.igb.num2 = GameFramework_Rand(1, 3);
			if (QuickModeGameS.igb.num2 == 1) {
				if ((*Enemigo_getSObject()).SidTotal.size > 0) {
					Enemigo_reinicia(
							(Enemigo*) Lista_GetIn(
									&((*Enemigo_getSObject()).SidTotal),
									(*Enemigo_getSObject()).SidTotal.size - 1),
							(-1) * GameFramework_Rand(100, 500),
							GameFramework_Rand(600, 1000),
							QuickModeGameS.igb.velocidadEnem,
							QuickModeGameS.igb.velocidadEnemY,
							QuickModeGameS.igb.aumentoEnVidaEnemigo);

					Lista_AddInFin(&(QuickModeGameS.igb.enemigos),
							Lista_GetIn(&((*Enemigo_getSObject()).SidTotal),
									(*Enemigo_getSObject()).SidTotal.size - 1));

					Lista_RemoveAt(&((*Enemigo_getSObject()).SidTotal),
							(*Enemigo_getSObject()).SidTotal.size - 1);
				}
			}
			if (QuickModeGameS.igb.num2 == 2) {
				if ((*Enemigo_getSObject()).SidTotal.size > 0) {
					Enemigo_reinicia(
							(Enemigo*) Lista_GetIn(
									&((*Enemigo_getSObject()).SidTotal),
									(*Enemigo_getSObject()).SidTotal.size - 1),
							GameFramework_Rand(1000, 1500),
							GameFramework_Rand(600, 1000),
							QuickModeGameS.igb.velocidadEnem,
							QuickModeGameS.igb.velocidadEnemY,
							QuickModeGameS.igb.aumentoEnVidaEnemigo);

					Lista_AddInFin(&(QuickModeGameS.igb.enemigos),
							Lista_GetIn(&((*Enemigo_getSObject()).SidTotal),
									(*Enemigo_getSObject()).SidTotal.size - 1));

					Lista_RemoveAt(&((*Enemigo_getSObject()).SidTotal),
							(*Enemigo_getSObject()).SidTotal.size - 1);
				}
			}
		}

	}
}

QuickModeGame* QuickModeGame_getSObject() {
	return &QuickModeGameS;
}

