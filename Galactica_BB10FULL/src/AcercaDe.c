/*
 * AcercaDe.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "AcercaDe.h"
#include "Textura.h"
#include "Boton.h"
#include "Lista.h"
#include "Dibujar.h"
#include "GameComun.h"
#include <stdlib.h>
#include <stdio.h>

//Acerca de
static AcercaDe AcercaDeS;

void AcercaDe_load()
{
	AcercaDe_setTexturas();
	AcercaDe_loadTitulo();
	Lista_Inicia(&(AcercaDeS.botones));
	Lista_InsertaEnFinal(&(AcercaDeS.botones), (AcercaDeS.botones).fin,
			Boton_Crear(BBack, 10, 10));
}

void AcercaDe_update()
{
	GameComun_updateEstrellas();
	for (AcercaDeS.i = 0; AcercaDeS.i < AcercaDeS.botones.size; AcercaDeS.i++)
	{
		Boton_update(
				(Boton*) Lista_RetornaElemento(&(AcercaDeS.botones),
						AcercaDeS.i));
	}
}

void AcercaDe_draw()
{
	//CLEAR NEGRO
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_RetornaElemento(&(AcercaDeS.textura), 1),
			GameFramework_Vector2(0, 0), GameFramework_Color(0, 0, 0, 1.0),
			0.0f, GameFramework_Vector2(0, 0), GameFramework_Vector2(100, 100));
	GameComun_drawEstrellas();
	AcercaDe_drawTitulo();
	for (AcercaDeS.i = 0; AcercaDeS.i < AcercaDeS.botones.size; AcercaDeS.i++)
	{
		Boton_draw(
				(Boton*) Lista_RetornaElemento(&(AcercaDeS.botones),
						AcercaDeS.i));
	}
}

//*****************************************

//******TITULO***********
void AcercaDe_loadTitulo()
{
	AcercaDeS.positionTitulo.X = 0;
	AcercaDeS.positionTitulo.Y = 0;
	AcercaDeS.origenTitulo.X = 0;
	AcercaDeS.origenTitulo.Y = 0;
	AcercaDeS.escalaTitulo.X = 1.17;
	AcercaDeS.escalaTitulo.Y = 1;
}

void AcercaDe_drawTitulo()
{
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_RetornaElemento(&(AcercaDeS.textura), 0),
			AcercaDeS.positionTitulo, GameFramework_Color(1, 1, 1, 1), 0.0f,
			AcercaDeS.origenTitulo, AcercaDeS.escalaTitulo);
	/*sb.Draw(textura[0],Vector2.Zero, null, Color.White, 0.0f, origenTitulo,
	 1.0f, SpriteEffects.None, 0.5f);*/
}

//*********Texturas***************
void AcercaDe_setTexturas()
{
	Lista_Inicia(&(AcercaDeS.textura));
	Lista_InsertaEnFinal(&(AcercaDeS.textura), AcercaDeS.textura.fin,
			Textura_getTextura("About")); //0
	Lista_InsertaEnFinal(&(AcercaDeS.textura), AcercaDeS.textura.fin,
			Textura_getTexturaFondo(0)); //1
}

