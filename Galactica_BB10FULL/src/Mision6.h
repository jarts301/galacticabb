/*
 * Mision6.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION6_H_
#define MISION6_H_

#include "GalacticaTipos.h"

void Mision6_reiniciar();
void Mision6_load();
void Mision6_update();
void Mision6_draw();
void Mision6_drawEstadNave();
void Mision6_updateGame();
void Mision6_gameOver();
void Mision6_drawMensajeInicial();
void Mision6_insertaEnemigo();
Mision6* Mision6_getSObject();

#endif /* MISION6_H_ */
