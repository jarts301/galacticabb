/*
 * Acelerometro.h
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#ifndef ACELEROMETRO_H_
#define ACELEROMETRO_H_

#include "GalacticaTipos.h"

Acelerometro* Acelerometro_getSObject();
void Acelerometro_load();
void Acelerometro_update();

#endif /* ACELEROMETRO_H_ */
