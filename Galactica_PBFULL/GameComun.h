/*
 * GameComun.h
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#ifndef GAMECOMUN_H_
#define GAMECOMUN_H_

#include "GameFramework.h"
#include "Lista.h"
#include "Collition.h"
#include "CollitionJefe.h"
#include "GalacticaTipos.h"

void GameComun_loadEstrellas();
void GameComun_updateEstrellas();
void GameComun_drawEstrellas();

#endif /* GAMECOMUN_H_ */
