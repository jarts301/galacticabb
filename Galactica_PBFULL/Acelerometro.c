/*
 * Acelerometro.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "Acelerometro.h"
#include "QuickMode.h"
#include "MisionMode.h"
#include "Nave.h"

//Para objetos estaticos
static Acelerometro AcelerometroS;

void Acelerometro_load()
{
	AcelerometroS.acelActivo = 1;
}

void Acelerometro_update()
{
	if (AcelerometroS.acelActivo == 1
			&& ((*QuickMode_getSObject()).estadoActual == QMGame
					|| (*MisionMode_getSObject()).estadoActual == MMGame))
	{
		(*Nave_getSObject()).limiteShot = 0;
		if (AcelerometroS.AcelerometroAcelerDat.X < -0.8
				&& (*Nave_getSObject()).movEnY == 1)
		{
			(*Nave_getSObject()).auxPosition.Y =
					(*Nave_getSObject()).auxPosition.Y
							- (*Nave_getSObject()).velocidadNave;
		}
		if (AcelerometroS.AcelerometroAcelerDat.X > 0.8
				&& (*Nave_getSObject()).movEnY == 1)
		{
			(*Nave_getSObject()).auxPosition.Y =
					(*Nave_getSObject()).auxPosition.Y
							+ (*Nave_getSObject()).velocidadNave;
		}
		if (AcelerometroS.AcelerometroAcelerDat.Y < -0.8)
		{
			(*Nave_getSObject()).auxPosition.X =
					(*Nave_getSObject()).auxPosition.X
							+ (*Nave_getSObject()).velocidadNave;
		}
		if (AcelerometroS.AcelerometroAcelerDat.Y > 0.8)
		{
			(*Nave_getSObject()).auxPosition.X =
					(*Nave_getSObject()).auxPosition.X
							- (*Nave_getSObject()).velocidadNave;
		}

		Nave_setPosition((*Nave_getSObject()).auxPosition);
	}
}

Acelerometro* Acelerometro_getSObject()
{
	return &AcelerometroS;
}

