/*
 * TouchControl.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef TOUCHCONTROL_H_
#define TOUCHCONTROL_H_

#include "GalacticaTipos.h"

void TouchControl_Inicia();
Lista* TouchControl_getTouches();
Touche* TouchControl_getATouch(int i);
void TouchControl_Clear();
void TouchControl_addTouch(Touche *touche);
Touche* TouchControl_crearTouch(touchEvento est,Vector2 pos,int id);
Lista* TouchControl_getTouches();
Touche* TouchControl_getToucheLibre();
void TouchControl_actualizaTouch(touchEvento te, Vector2 position, int idVal);

#endif /* TOUCHCONTROL_H_ */
