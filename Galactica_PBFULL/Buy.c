/*
 * Buy.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "Buy.h"
#include "Lista.h"
#include "Textura.h"
#include "Boton.h"
#include "Nave.h"
#include "bbutil.h"
#include "GameComun.h"
#include "GameFramework.h"
#include "Dibujar.h"
#include <stdlib.h>

static Buy BuyS;


void Buy_load()
{
	Buy_setTexturas();
	Buy_loadTitulo();
	BuyS.mensaje =
			"Enjoy all the missions,\nand the full Quick Game.\nRescue all spaceships,\nwith its incredible powers,\nand Do not lose the benefit\nof overcoming each mission,\nso you can go a lot further\nin Quick Game,and get many\nmore points.";
	Lista_Inicia(&(BuyS.botones));
	Lista_InsertaEnFinal(&(BuyS.botones), BuyS.botones.fin,
			Boton_Crear(BGoBuy, 80, 450));
	Lista_InsertaEnFinal(&(BuyS.botones), BuyS.botones.fin,
			Boton_Crear(BNotNow, 80, 570));
}

void Buy_update()
{
	GameComun_updateEstrellas();
	for (BuyS.i = 0; BuyS.i < BuyS.botones.size; BuyS.i++)
	{
		Boton_update((Boton*) Lista_RetornaElemento(&(BuyS.botones), BuyS.i));
	}
	(*Boton_getSObject()).ultimaActualizacion = GameFramework_getTotalTime();
	(*Nave_getSObject()).ultimaActualizacion = GameFramework_getTotalTime();
}

void Buy_draw()
{
	//CLEAR NEGRO
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_RetornaElemento(&(BuyS.textura), 1),
			GameFramework_Vector2(0, 1024), GameFramework_Color(0, 0, 0, 1.0),
			0.0f, GameFramework_Vector2(0, 0), GameFramework_Vector2(100, 100));
	GameComun_drawEstrellas();
	Buy_drawTitulo();
	for (BuyS.i = 0; BuyS.i < BuyS.botones.size; BuyS.i++)
	{
		Boton_draw((Boton*) Lista_RetornaElemento(&(BuyS.botones), BuyS.i));
	}

	Dibujar_drawText((*Textura_getSObject()).Fuente, BuyS.mensaje,
			GameFramework_Vector2(20, 140), GameFramework_Color(1, 1, 1, 1),
			1.0f);
//(Letra, mensaje, new Vector2(20,140),
//		Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.01f);

}

//*****************************************

//******TITULO**********
void Buy_loadTitulo()
{
	BuyS.positionTitulo.X = 480 / 2;
	BuyS.positionTitulo.Y = 100;
	BuyS.origenTitulo.X = (*(TexturaObjeto*) Lista_RetornaElemento(
			&(BuyS.textura), 0)).ancho / 2;
	BuyS.origenTitulo.Y = (*(TexturaObjeto*) Lista_RetornaElemento(
			&(BuyS.textura), 0)).alto / 2;
}

void Buy_drawTitulo()
{
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_RetornaElemento(&(BuyS.textura), 0),
			BuyS.positionTitulo, GameFramework_Color(1, 1, 1, 1.0), 0.0f,
			GameFramework_Vector2(0, 0), GameFramework_Vector2(1.2, 1.2));
}

//*********Texturas***************
void Buy_setTexturas()
{
	Lista_Inicia(&(BuyS.textura));
	Lista_InsertaEnFinal(&(BuyS.textura), BuyS.textura.fin,
			Textura_getTextura("TituloBuyGame")); //0
	Lista_InsertaEnFinal(&(BuyS.textura), BuyS.textura.fin,
			Textura_getTexturaFondo(0)); //1
}

