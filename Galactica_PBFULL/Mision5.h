/*
 * Mision5.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION5_H_
#define MISION5_H_

#include "GalacticaTipos.h"

void Mision5_reiniciar();
void Mision5_load();
void Mision5_update();
void Mision5_draw();
void Mision5_drawEstadNave();
void Mision5_gameOver();
void Mision5_updateJefes();
void Mision5_drawJefes();
void Mision5_drawMensajeInicial();
void Mision5_reiniciaJefe();
Mision5* Mision5_getSObject();

#endif /* MISION5_H_ */
