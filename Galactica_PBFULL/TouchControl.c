/*
 * TouchControlS.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "TouchControl.h"
#include <bps/event.h>
#include "Lista.h"
#include "GameFramework.h"
#include <screen/screen.h>
#include <stdlib.h>

static TouchControl TouchControlS;

Touche* auxTouche;

void TouchControl_Inicia()
{
	Lista_Inicia(&(TouchControlS.touches));

	for (TouchControlS.i = 0; TouchControlS.i < 4; TouchControlS.i++)
	{
		TouchControl_addTouch(
				TouchControl_crearTouch(libre, GameFramework_Vector2(0, 0),
						-1));
	}
}

Lista* TouchControl_getTouches()
{
	return &(TouchControlS.touches);
}

Touche* TouchControl_getToucheLibre()
{
	for (TouchControlS.i = 0; TouchControlS.i < 4; TouchControlS.i++)
	{
		if ((*(Touche*) Lista_GetIn(&(TouchControlS.touches), TouchControlS.i)).estado
				== libre)
		{
			return (Touche*) Lista_GetIn(&(TouchControlS.touches),
					TouchControlS.i);
		}
	}
	return NULL;
}

void TouchControl_actualizaTouch(touchEvento te, Vector2 position, int idVal)
{
	/*for (TouchControlS.i = 0; TouchControlS.i < 8; TouchControlS.i++)
	 {
	 if ((*(Touche*) Lista_GetIn(&(TouchControlS.touches), TouchControlS.i)).identifi
	 == idVal)
	 {
	 (*(Touche*) Lista_GetIn(&(TouchControlS.touches), TouchControlS.i)).estado =
	 te;
	 (*(Touche*) Lista_GetIn(&(TouchControlS.touches), TouchControlS.i)).position =
	 position;

	 return;
	 }
	 }*/

	auxTouche = TouchControl_getToucheLibre();

	if (auxTouche != NULL)
	{
		(*auxTouche).estado = te;
		(*auxTouche).position.X = position.X;
		(*auxTouche).position.Y = 1024 - position.Y;
		(*auxTouche).identifi = idVal;
		return;
	}

}

Touche* TouchControl_getATouch(int i)
{
	return (Touche*) Lista_RetornaElemento(&(TouchControlS.touches), i);
}

void TouchControl_Clear()
{
	for (TouchControlS.i = 0; TouchControlS.i < 4; TouchControlS.i++)
	{
		if ((*TouchControl_getATouch(TouchControlS.i)).estado == Pressed)
		{
			(*TouchControl_getATouch(TouchControlS.i)).estado = Moved;
		}

		if ((*TouchControl_getATouch(TouchControlS.i)).estado == Released)
		{
			(*(Touche*) Lista_GetIn(&(TouchControlS.touches), TouchControlS.i)).estado =
					libre;
			(*(Touche*) Lista_GetIn(&(TouchControlS.touches), TouchControlS.i)).identifi =
					-1;
			(*(Touche*) Lista_GetIn(&(TouchControlS.touches), TouchControlS.i)).position =
					GameFramework_Vector2(0, 0);
		}
	}
	//Lista_Destruir(&(TouchControlS.touches));
	//Lista_Inicia(&(TouchControlS.touches));
}

void TouchControl_addTouch(Touche *touche)
{
	Lista_InsertaEnFinal(&(TouchControlS.touches), TouchControlS.touches.fin,
			touche);
}

Touche* TouchControl_crearTouch(touchEvento est, Vector2 pos, int id)
{

	Touche* nuevo;
	if ((nuevo = (Touche*) malloc(sizeof(Touche))) == NULL)
		return NULL;

	(*nuevo).estado = est;
	(*nuevo).identifi = id;
	(*nuevo).position.X = pos.X;
	(*nuevo).position.Y = 1024 - pos.Y;

	return nuevo;
}

