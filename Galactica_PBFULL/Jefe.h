/*
 * Jefe.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef JEFE_H_
#define JEFE_H_

#include "GalacticaTipos.h"

Jefe* Jefe_crearObject(Jefe_tipo t, int x, int y);
void Jefe_reinicia(Jefe* jefe, int x, int y, int aumentoVida);
void Jefe_load();
void Jefe_update(Jefe* jefe);
void Jefe_draw(Jefe* jefe);
void Jefe_setAumentoVida(Jefe* jefe, int v);
void Jefe_velocidadAnimacion(Jefe* jefe);
void Jefe_cargaListaDisparos();
void Jefe_updateDisparos(Jefe* jefe);
void Jefe_drawDisparosLaxo(Jefe* jefe);
void Jefe_defineDatosAnimacion(Jefe* jefe);
void Jefe_movimientoLaxo(Jefe* jefe);
void Jefe_movimientoEsp(Jefe* jefe);
void Jefe_movimientoQuad(Jefe* jefe);
void Jefe_movimientoRued(Jefe* jefe);
void Jefe_updateAnimaciones(Jefe* jefe);
void Jefe_drawAnimaciones(Jefe* jefe);
void Jefe_defineTrozoAnim(Jefe* jefe, int x, int y, int width, int height);
void Jefe_updateTiemposEspera(Jefe* jefe);
void Jefe_inicializaBrazosEsp(Jefe* jefe);
void Jefe_inicializaMartillosQuad(Jefe* jefe);
void Jefe_inicializaFlechasRued(Jefe* jefe);
void Jefe_updateBrazosEsp(Jefe* jefe);
void Jefe_updateMartillosQuad(Jefe* jefe);
void Jefe_updateFlechasRued(Jefe* jefe);
void Jefe_drawBrazosEsp(Jefe* jefe);
void Jefe_drawMartillosQuad(Jefe* jefe);
void Jefe_drawFlechasRued(Jefe* jefe);
void Jefe_velocidadAnimacionMartillo(Jefe* jefe);
void Jefe_velocidadAnimacionFlecha(Jefe* jefe);
void Jefe_defineTrozoAnimMartillo(Jefe* jefe, int x, int y, int width,
		int height);
void Jefe_setOrigenMartillo(Jefe* jefe);
void Jefe_setOrigenFlecha(Jefe *jefe);
void Jefe_setOrigen(Jefe* jefe);
void Jefe_setTextura();
void Jefe_defineCollitionBox();
void Jefe_confirmaColisionesLaxo(Jefe* jefe);
void Jefe_confirmaColisionesEsp(Jefe* jefe);
void Jefe_confirmaColisionesQuad(Jefe* jefe);
void Jefe_confirmaColisionesRued(Jefe* jefe);
Jefe* Jefe_getSObject();

#endif /* JEFE_H_ */
