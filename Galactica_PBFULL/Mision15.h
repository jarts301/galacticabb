/*
 * Mision15.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION15_H_
#define MISION15_H_

#include "GalacticaTipos.h"

void Mision15_reiniciar();
void Mision15_load();
void Mision15_update();
void Mision15_draw();
void Mision15_drawEstadNave();
void Mision15_gameOver();
void Mision15_updateJefes();
void Mision15_drawJefes();
void Mision15_drawMensajeInicial();
void Mision15_reiniciaJefe();
Mision15* Mision15_getSObject();

#endif /* MISION15_H_ */
