/*
 * Mision11.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION11_H_
#define MISION11_H_

#include "GalacticaTipos.h"

void Mision11_reiniciar();
void Mision11_load();
void Mision11_update();
void Mision11_draw();
void Mision11_drawEstadNave();
void Mision11_updateGame();
void Mision11_gameOver();
void Mision11_drawMensajeInicial();
void Mision11_updateEnemigos();
void Mision11_insertaEnemigo();
Mision11* Mision11_getSObject();

#endif /* MISION11_H_ */
