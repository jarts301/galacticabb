/*
 * Dibujar.c
 *
 *  Created on: 6/03/2013
 *      Author: Jarts
 */
#include "Dibujar.h"
#include "GameFramework.h"
#include "Textura.h"
#include "bbutil.h"
#include <GLES/gl.h>

void Dibujar_drawSpriteTr(TexturaObjeto *tex, Vector2 pos, Rectangle trozoTex,
		Color color, float rotacion, Vector2 origen, Vector2 escala)
{
	(*tex).texCoord[0] = Dibujar_dePixelAFloat((*tex).ancho, trozoTex.X);
	(*tex).texCoord[1] = Dibujar_dePixelAFloat((*tex).alto, trozoTex.Height)
			+ Dibujar_dePixelAFloat((*tex).alto, trozoTex.Y);
	(*tex).texCoord[2] = Dibujar_dePixelAFloat((*tex).ancho, trozoTex.Width)
			+ Dibujar_dePixelAFloat((*tex).ancho, trozoTex.X);
	(*tex).texCoord[3] = Dibujar_dePixelAFloat((*tex).alto, trozoTex.Height)
			+ Dibujar_dePixelAFloat((*tex).alto, trozoTex.Y);
	(*tex).texCoord[4] = Dibujar_dePixelAFloat((*tex).ancho, trozoTex.X);
	(*tex).texCoord[5] = Dibujar_dePixelAFloat((*tex).alto, trozoTex.Y);
	(*tex).texCoord[6] = Dibujar_dePixelAFloat((*tex).ancho, trozoTex.Width)
			+ Dibujar_dePixelAFloat((*tex).ancho, trozoTex.X);
	(*tex).texCoord[7] = Dibujar_dePixelAFloat((*tex).alto, trozoTex.Y);

	if ((*tex).setOrigen == 1)
	{
		(*tex).cuadCoord[0] = trozoTex.X - origen.X;
		(*tex).cuadCoord[1] = trozoTex.Height - origen.Y;
		(*tex).cuadCoord[2] = trozoTex.Width - origen.X;
		(*tex).cuadCoord[3] = trozoTex.Height - origen.Y;
		(*tex).cuadCoord[4] = trozoTex.X - origen.X;
		(*tex).cuadCoord[5] = trozoTex.Y - origen.Y;
		(*tex).cuadCoord[6] = trozoTex.Width - origen.X;
		(*tex).cuadCoord[7] = trozoTex.Y - origen.Y;

		(*tex).setOrigen = 0;
	}

	glLoadIdentity();
	glBindTexture(GL_TEXTURE_2D, (*tex).textura);
	glColor4f(color.R, color.G, color.B, 1.0f);
	glTranslatef(pos.X, pos.Y, 0);
	glRotatef(rotacion, 0, 0, 1);
	glScalef(escala.X, escala.Y, 0.0f);
	glTexCoordPointer(2, GL_FLOAT, 0, (*tex).texCoord);
	glVertexPointer(2, GL_FLOAT, 0, (*tex).cuadCoord);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glLoadIdentity();

}

void Dibujar_drawSprite(TexturaObjeto *tex, Vector2 pos, Color color,
		float rotacion, Vector2 origen, Vector2 escala)
{
	(*tex).texCoord[0] = 0.0f;
	(*tex).texCoord[1] = 1.0f;
	(*tex).texCoord[2] = 1.0f;
	(*tex).texCoord[3] = 1.0f;
	(*tex).texCoord[4] = 0.0f;
	(*tex).texCoord[5] = 0.0f;
	(*tex).texCoord[6] = 1.0f;
	(*tex).texCoord[7] = 0.0f;

	if ((*tex).setOrigen == 1)
	{

		(*tex).cuadCoord[0] = origen.X * (-1);
		(*tex).cuadCoord[1] = ((*tex).alto) - origen.Y;
		(*tex).cuadCoord[2] = (*tex).ancho - origen.X;
		(*tex).cuadCoord[3] = ((*tex).alto) - origen.Y;
		(*tex).cuadCoord[4] = origen.X * (-1);
		(*tex).cuadCoord[5] = origen.Y * (-1);
		(*tex).cuadCoord[6] = (*tex).ancho - origen.X;
		(*tex).cuadCoord[7] = origen.Y * (-1);

		(*tex).setOrigen = 0;
	}

	glLoadIdentity();
	glBindTexture(GL_TEXTURE_2D, (*tex).textura);
	glColor4f(color.R, color.G, color.B, 1.0f);
	glTranslatef(pos.X, pos.Y, 0);
	glRotatef(rotacion, 0, 0, 1);
	glScalef(escala.X, escala.Y, 0.0f);
	glTexCoordPointer(2, GL_FLOAT, 0, &((*tex).texCoord));
	glVertexPointer(2, GL_FLOAT, 0, &((*tex).cuadCoord));
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glLoadIdentity();

}

void Dibujar_drawText(font_t* font, const char* msg, Vector2 position,
		Color color, float escala)
{
	bbutil_render_text(font, msg, position, color, escala);
}

void Dibujar_drawErrorText(char* errort, Vector2 pos)
{
	/*glClear(GL_COLOR_BUFFER_BIT);

	//glMatrixMode(GL_MODELVIEW);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);

	Dibujar_drawText((*Textura_getSObject()).Fuente, errort, pos,
			GameFramework_Color(1, 1, 1, 1), 1);

	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	bbutil_swap();

	double tespera = 10000000;
	while (tespera >= 0)
	{
		tespera = tespera - 10;
	}*/

}

float Dibujar_dePixelAFloat(float MaxPixeles, float pixeles)
{
	return ((float) pixeles / (float) MaxPixeles);
}

