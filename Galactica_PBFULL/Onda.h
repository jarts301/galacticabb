/*
 * Onda.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef ONDA_H_
#define ONDA_H_

#include "GalacticaTipos.h"

Onda* Onda_Crear();
void Onda_reiniciar(Onda* onda);
void Onda_load();
void Onda_update(Onda* onda);
void Onda_draw(Onda* onda);
void Onda_setTextura();
void Onda_setPosition(Onda* onda,Vector2 posNave);
Vector2 Onda_getPosition(Onda* onda);
void Onda_setOrigen(Onda* onda);
void Onda_velocidadAnimacion(Onda* onda, int velocidad);
void Onda_setTrozoAnimacion(Onda* onda);
Onda* Onda_getSObject();

#endif /* ONDA_H_ */
