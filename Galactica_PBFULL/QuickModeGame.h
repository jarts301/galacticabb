/*
 * QuickModeGame.h
 *
 *  Created on: 17/03/2013
 *      Author: Jarts
 */

#ifndef QUICKMODEGAME_H_
#define QUICKMODEGAME_H_

#include "GalacticaTipos.h"

void QuickModeGame_reiniciar();
void QuickModeGame_load();
void QuickModeGame_update();
void QuickModeGame_draw();
void QuickModeGame_drawEstadNave();
void QuickModeGame_updatePlus();
void QuickModeGame_updateGame();
void QuickModeGame_updateEnemigos();
void QuickModeGame_gameOver();
void QuickModeGame_drawEnemigos();
void QuickModeGame_updateAnimaciones();
void QuickModeGame_drawAnimaciones();
void QuickModeGame_eliminaAnimaciones();
void QuickModeGame_confirmaEliminarNor(int c);
void QuickModeGame_confirmaEliminarSid(int c);
void QuickModeGame_agregarJefe();
void QuickModeGame_agregarJefe2(Jefe_tipo tipoJe);
void QuickModeGame_updateJefes();
void QuickModeGame_drawJefes();
void QuickModeGame_loadEstrellas();
void QuickModeGame_updateEstrellas();
void QuickModeGame_drawEstrellas();
void QuickModeGame_updateMensajeInicial();
void QuickModeGame_drawMensajeInicial();
void QuickModeGame_eliminarTodosLosEnemigos();
void QuickModeGame_eliminarTodos();
void QuickModeGame_EliminarYRetornoEnemigoTotal(int indice);
void QuickModeGame_eliminarJefes();
void QuickModeGame_drawEstadisticas();
void QuickModeGame_insertaEnemigo();
void QuickModeGame_LoadEnemigos();
QuickModeGame* QuickModeGame_getSObject();


#endif /* QUICKMODEGAME_H_ */
