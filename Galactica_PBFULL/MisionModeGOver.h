/*
 * MisionModeGOver.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISIONMODEGOVER_H_
#define MISIONMODEGOVER_H_

#include "GalacticaTipos.h"

void MisionModeGOver_load();
void MisionModeGOver_update();
void MisionModeGOver_draw();
void MisionModeGOver_loadTitulo();
void MisionModeGOver_updateTitulo();
void MisionModeGOver_drawTitulo();
void MisionModeGOver_setOrigen(int Tex);
void MisionModeGOver_setTexturas();
MisionModeGOver* MisionModeGOver_getSObject();

#endif /* MISIONMODEGOVER_H_ */
