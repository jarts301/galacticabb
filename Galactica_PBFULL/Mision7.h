/*
 * Mision7.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION7_H_
#define MISION7_H_

#include "GalacticaTipos.h"

void Mision7_reiniciar();
void Mision7_load();
void Mision7_update();
void Mision7_draw();
void Mision7_drawEstadNave();
void Mision7_gameOver();
void Mision7_drawMensajeInicial();
Mision7* Mision7_getSObject();

#endif /* MISION7_H_ */
