/*
 * Buy.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef BUY_H_
#define BUY_H_

#include "GalacticaTipos.h"

void Buy_load();
void Buy_update();
void Buy_draw();
void Buy_loadTitulo();
void Buy_drawTitulo();
void Buy_setTexturas();

#endif /* BUY_H_ */
