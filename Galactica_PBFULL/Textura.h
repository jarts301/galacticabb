/*
 * Textura.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef TEXTURA_H_
#define TEXTURA_H_

#include "GalacticaTipos.h"

void Textura_Inicia();
int Textura_addTextura(char* direccion, char *nombre);
int Textura_addTexturaFondo(char* direccion, char *nombre);
TexturaObjeto* Textura_getTexturaFondo(int indice);
TexturaObjeto* Textura_getTextura(char* nombre);
int Textura_cargaIntro();
TexturaObjeto* Textura_getTexturaIn(int indice);
Textura* Textura_getSObject();

#endif /* TEXTURA_H_ */
