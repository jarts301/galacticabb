/*
 * Boton.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef BOTON_H_
#define BOTON_H_

#include "GalacticaTipos.h"

Boton* Boton_Crear(Boton_tipo tb, int posX, int posY);
Boton* Boton_CrearBotonF(Boton_tipo tb, int posX, int posY,
		int dirDerechaFlecha);
void Boton_load();
void Boton_update(Boton* boton);
void Boton_draw(Boton* boton);
void Boton_defineBQuickMode(Boton* boton, int posX, int posY);
void Boton_defineBPause(Boton* boton, int posX, int posY);
void Boton_defineBResumeGame(Boton* boton, int posX, int posY);
void Boton_defineBExitGame(Boton* boton, int posX, int posY);
void Boton_defineBFlecha(Boton* boton, int posX, int posY, int dirDerechaFlecha);
void Boton_defineBPlay(Boton* boton, int posX, int posY);
void Boton_defineBPower(Boton* boton, int posX, int posY);
void Boton_defineBContinue(Boton* boton, int posX, int posY);
void Boton_defineBMisionMode(Boton* boton, int posX, int posY);
void Boton_defineBMision(Boton* boton, int posX, int posY);
void Boton_defineBBack(Boton* boton, int posX, int posY);
void Boton_defineBNextMision(Boton* boton, int posX, int posY);
void Boton_defineBRestart(Boton* boton, int posX, int posY);
void Boton_defineBAudio(Boton* boton, int posX, int posY);
void Boton_defineBOpciones(Boton* boton, int posX, int posY);
void Boton_defineBSalir(Boton* boton, int posX, int posY);
void Boton_defineBTilting(Boton* boton, int posX, int posY);
void Boton_defineBButtons(Boton* boton, int posX, int posY);
void Boton_defineBMedium(Boton* boton, int posX, int posY);
void Boton_defineBLarge(Boton* boton, int posX, int posY);
void Boton_defineBHelp(Boton* boton, int posX, int posY);
void Boton_defineBAbout(Boton* boton, int posX, int posY);
void Boton_defineBContinueGame(Boton* boton, int posX, int posY);
void Boton_defineBNewGame(Boton* boton, int posX, int posY);
void Boton_defineBFacebook(Boton* boton, int posX, int posY);
void Boton_defineBTwitter(Boton* boton, int posX, int posY);
void Boton_defineBComent(Boton* boton, int posX, int posY);
void Boton_defineBGoBuy(Boton* boton, int posX, int posY);
void Boton_defineBNotNow(Boton* boton, int posX, int posY);
void Boton_reiniciarMision();
void Boton_setTexturas();
void Boton_reiniciaBPower(Boton* boton);
void Boton_setPosition(Boton* boton, int x, int y);
void Boton_comprobarMensMision();
void Boton_defineTrozoAnim(Boton* boton, double x, double y, double width,
		double height);
void Boton_verificaTouchBPause(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBAudio(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBExitGame(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBResumeGame(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBTilting(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBButtons(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBMedium(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBLarge(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBQuickMode(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBAbout(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBSalir(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBOpciones(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBMisionMode(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBHelp(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBMision(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBPlay(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBFlecha(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBPower(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBContinue(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBBack(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBNextMision(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBRestart(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBContinueGame(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBNewGame(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBFacebook(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBTwitter(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBComent(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBGoBuy(Boton* boton, int ancho, int alto);
void Boton_verificaTouchBNotNow(Boton* boton, int ancho, int alto);
Boton* Boton_getSObject();

#endif /* BOTON_H_ */
