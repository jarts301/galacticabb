/*
 * MisionModeGame_c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "MisionModeGame.h"
#include "Mision1.h"
#include "Mision10.h"
#include "Mision11.h"
#include "Mision12.h"
#include "Mision13.h"
#include "Mision14.h"
#include "Mision15.h"
#include "Mision16.h"
#include "Mision17.h"
#include "Mision18.h"
#include "Mision19.h"
#include "Mision2.h"
#include "Mision20.h"
#include "Mision3.h"
#include "Mision4.h"
#include "Mision5.h"
#include "Mision6.h"
#include "Mision7.h"
#include "Mision8.h"
#include "Mision9.h"

static MisionModeGame MisionModeGameS;

void MisionModeGame_load()
{
	MisionModeGameS.numMision = 0;
}

void MisionModeGame_update()
{
	switch (MisionModeGameS.numMision)
	{
	case 0:
		break;
	case 1:
		Mision1_update();
		break;
	case 2:
		Mision2_update();
		break;
	case 3:
		Mision3_update();
		break;
	case 4:
		Mision4_update();
		break;
	case 5:
		Mision5_update();
		break;
	case 6:
		Mision6_update();
		break;
	case 7:
		Mision7_update();
		break;
	case 8:
		Mision8_update();
		break;
	case 9:
		Mision9_update();
		break;
	case 10:
		Mision10_update();
		break;
	case 11:
		Mision11_update();
		break;
	case 12:
		Mision12_update();
		break;
	case 13:
		Mision13_update();
		break;
	case 14:
		Mision14_update();
		break;
	case 15:
		Mision15_update();
		break;
	case 16:
		Mision16_update();
		break;
	case 17:
		Mision17_update();
		break;
	case 18:
		Mision18_update();
		break;
	case 19:
		Mision19_update();
		break;
	case 20:
		Mision20_update();
		break;
	}

}

void MisionModeGame_draw()
{
	switch (MisionModeGameS.numMision)
	{
	case 0:
		break;
	case 1:
		Mision1_draw();
		break;
	case 2:
		Mision2_draw();
		break;
	case 3:
		Mision3_draw();
		break;
	case 4:
		Mision4_draw();
		break;
	case 5:
		Mision5_draw();
		break;
	case 6:
		Mision6_draw();
		break;
	case 7:
		Mision7_draw();
		break;
	case 8:
		Mision8_draw();
		break;
	case 9:
		Mision9_draw();
		break;
	case 10:
		Mision10_draw();
		break;
	case 11:
		Mision11_draw();
		break;
	case 12:
		Mision12_draw();
		break;
	case 13:
		Mision13_draw();
		break;
	case 14:
		Mision14_draw();
		break;
	case 15:
		Mision15_draw();
		break;
	case 16:
		Mision16_draw();
		break;
	case 17:
		Mision17_draw();
		break;
	case 18:
		Mision18_draw();
		break;
	case 19:
		Mision19_draw();
		break;
	case 20:
		Mision20_draw();
		break;
	}

}

MisionModeGame* MisionModeGame_getSObjec()
{
	return &MisionModeGameS;
}

