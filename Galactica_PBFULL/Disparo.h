/*
 * Disparo.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef DISPARO_H_
#define DISPARO_H_

#include "GalacticaTipos.h"

Disparo* Disparo_CrearObject(Vector2 posN, Disparo_Tipo t);
void Disparo_load();
void Disparo_update(Disparo* disparo);
void Disparo_draw(Disparo* disparo);
void Disparo_setTextura();
void Disparo_setPositionPequed(Disparo* disparo, Vector2 posNave);
void Disparo_setPositionPequei(Disparo* disparo, Vector2 posNave);
void Disparo_setPositionSuper(Disparo* disparo, Vector2 posNave);
Vector2 Disparo_getPosition(Disparo* disparo);
void Disparo_movimientoDisparo(Disparo* disparo);
void Disparo_movimientoDisparoDiagDerecha(Disparo* disparo);
void Disapro_movimientoDisparoDiagIzquierda(Disparo* disparo);
void Disparo_movimientoDisparoSuper(Disparo* disparo);
void Disparo_setOrigen(Disparo* disparo, int textura);

#endif /* DISPARO_H_ */
