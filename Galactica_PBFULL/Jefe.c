/*
 * Jefe.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "Jefe.h"
#include "Textura.h"
#include "GameFramework.h"
#include "DisparoJefe.h"
#include "Dibujar.h"
#include "CollitionJefe.h"
#include "Nave.h"
#include "Animation.h"
#include <stdlib.h>

static Jefe JefeS;

Jefe* Jefe_crearObject(Jefe_tipo t, int x, int y)
{
	Jefe* jefe;
	if ((jefe = (Jefe*) malloc(sizeof(Jefe))) == NULL)
		return NULL;

	(*jefe).tipoJefe = t;
	(*jefe).Position.X = x;
	(*jefe).Position.Y = y;
	(*jefe).estaEnColisionDisp = 0;
	(*jefe).estaEnColisionPoder = 0;
	(*jefe).tiempoEsperaDisparo = 9000000;
	(*jefe).tiempoEsperaAtaque = 8000000;
	(*jefe).dato2 = 0;
	(*jefe).dato = 0;

	switch (t)
	{
	case JLaxo:
		(*jefe).velocidad = 4;
		(*jefe).vida = (*jefe).maxVida = 20000;
		(*jefe).Escala = GameFramework_Vector2(1.25f, 1.0f);
		(*jefe).texturaAnim = 0;
		(*jefe).actualframe = 0;
		(*jefe).lineaAnimacion = 0;
		(*jefe).ultimoTDisparoNormal = 0;
		(*jefe).ultimoTDisparoNormalAux = 0;
		(*jefe).ultimoTDisparoMax = 0;
		(*jefe).modoIndefenso = 0;
		JefeS.radioCollitionLaxo = (int) (160 * (*jefe).Escala.X);
		JefeS.radioCollitionDanoLaxo = (int) (37 * (*jefe).Escala.X);
		(*jefe).Rotacion = 0;
		Jefe_defineTrozoAnim(jefe, (*jefe).actualframe * 409.6,
				(*jefe).lineaAnimacion * 512, 409.6, 512);
		Jefe_setOrigen(jefe);
		break;

	case JEsp:
		(*jefe).velocidad = 4;
		(*jefe).vida = (*jefe).maxVida = 20000;
		(*jefe).Escala = GameFramework_Vector2(1.35f, 1.1f);
		(*jefe).texturaAnim = 1;
		(*jefe).actualframe = 0;
		(*jefe).lineaAnimacion = 0;
		(*jefe).ultimoTDisparoNormal = 0;
		(*jefe).ultimoTDisparoNormalAux = 0;
		(*jefe).Rotacion = 0;
		JefeS.radioCollitionEsp = (int) (42 * (*jefe).Escala.X);
		JefeS.radioCollitionBrazoEsp = (int) (25 * 1.2f);
		(*jefe).numeroAleatorio = 0;
		Jefe_defineTrozoAnim(jefe, (*jefe).actualframe * 409.6,
				(*jefe).lineaAnimacion * 512, 409.6, 512);
		Jefe_setOrigen(jefe);
		Jefe_inicializaBrazosEsp(jefe);
		break;

	case JQuad:
		(*jefe).velocidad = 4;
		(*jefe).vida = (*jefe).maxVida = 30000;
		(*jefe).Escala = GameFramework_Vector2(1.3f, 1.45f);
		(*jefe).texturaAnim = 2;
		(*jefe).actualframe = 0;
		(*jefe).lineaAnimacion = 0;
		(*jefe).ultimoTDisparoNormal = 0;
		(*jefe).ultimoTDisparoNormalAux = 0;
		(*jefe).ultimoTDisparoNormalAux2 = 0;
		(*jefe).Rotacion = 0;
		JefeS.radioCollitionQuad = (int) (50 * (*jefe).Escala.X);
		JefeS.radioCollitionMartilloQuad = (int) (50 * 1.2f);
		Jefe_defineTrozoAnim(jefe, (*jefe).actualframe * 409.6,
				(*jefe).lineaAnimacion * 256, 409.6, 256);
		Jefe_setOrigen(jefe);
		Jefe_inicializaMartillosQuad(jefe);
		break;

	case JRued:
		(*jefe).velocidad = 4;
		(*jefe).limiteLlegada = 950;
		(*jefe).vida = (*jefe).maxVida = 50000;
		(*jefe).Escala = GameFramework_Vector2(1.35f, 1.1f);
		(*jefe).Avance1 = 1;
		(*jefe).Avance2 = (*jefe).Avance3 = (*jefe).Avance4 = 0;
		(*jefe).texturaAnim = 3;
		(*jefe).actualframe = 0;
		(*jefe).lineaAnimacion = 0;
		(*jefe).ultimoTDisparoNormal = 0;
		(*jefe).Rotacion = 0;
		JefeS.radioCollitionRued = (int) (200 * (*jefe).Escala.X);
		JefeS.radioCollitionFlechaRued = (int) (94 * 0.5f);
		Jefe_defineTrozoAnim(jefe, (*jefe).actualframe * 409.6,
				(*jefe).lineaAnimacion * 512, 409.6, 512);
		Jefe_setOrigen(jefe);
		Jefe_inicializaFlechasRued(jefe);
		break;
	}

	return jefe;

}

void Jefe_reinicia(Jefe* jefe, int x, int y, int aumentoVida)
{
	(*jefe).Position.X = x;
	(*jefe).Position.Y = y;
	(*jefe).estaEnColisionDisp = 0;
	(*jefe).estaEnColisionPoder = 0;
	(*jefe).tiempoEsperaDisparo = 9000000;
	(*jefe).tiempoEsperaAtaque = 8000000;
	(*jefe).dato2 = 0;
	(*jefe).dato = 0;

	switch ((*jefe).tipoJefe)
	{
	case JLaxo:
		(*jefe).velocidad = 4;
		(*jefe).vida = (*jefe).maxVida = 20000;
		(*jefe).Escala = GameFramework_Vector2(1.25f, 1.0f);
		(*jefe).texturaAnim = 0;
		(*jefe).actualframe = 0;
		(*jefe).lineaAnimacion = 0;
		(*jefe).ultimoTDisparoNormal = 0;
		(*jefe).ultimoTDisparoNormalAux = 0;
		(*jefe).ultimoTDisparoMax = 0;
		(*jefe).modoIndefenso = 0;
		JefeS.radioCollitionLaxo = (int) (160 * (*jefe).Escala.X);
		JefeS.radioCollitionDanoLaxo = (int) (37 * (*jefe).Escala.X);
		(*jefe).Rotacion = 0;
		Jefe_defineTrozoAnim(jefe, (*jefe).actualframe * 409.6,
				(*jefe).lineaAnimacion * 512, 409.6, 512);
		Jefe_setOrigen(jefe);
		break;

	case JEsp:
		(*jefe).velocidad = 4;
		(*jefe).vida = (*jefe).maxVida = 20000;
		(*jefe).Escala = GameFramework_Vector2(1.35f, 1.1f);
		(*jefe).texturaAnim = 1;
		(*jefe).actualframe = 0;
		(*jefe).lineaAnimacion = 0;
		(*jefe).ultimoTDisparoNormal = 0;
		(*jefe).ultimoTDisparoNormalAux = 0;
		(*jefe).Rotacion = 0;
		JefeS.radioCollitionEsp = (int) (42 * (*jefe).Escala.X);
		JefeS.radioCollitionBrazoEsp = (int) (25 * 1.2f);
		(*jefe).numeroAleatorio = 0;
		Jefe_defineTrozoAnim(jefe, (*jefe).actualframe * 409.6,
				(*jefe).lineaAnimacion * 512, 409.6, 512);
		Jefe_setOrigen(jefe);
		Jefe_inicializaBrazosEsp(jefe);
		break;

	case JQuad:
		(*jefe).velocidad = 4;
		(*jefe).vida = (*jefe).maxVida = 30000;
		(*jefe).Escala = GameFramework_Vector2(1.3f, 1.45f);
		(*jefe).texturaAnim = 2;
		(*jefe).actualframe = 0;
		(*jefe).lineaAnimacion = 0;
		(*jefe).ultimoTDisparoNormal = 0;
		(*jefe).ultimoTDisparoNormalAux = 0;
		(*jefe).ultimoTDisparoNormalAux2 = 0;
		(*jefe).Rotacion = 0;
		JefeS.radioCollitionQuad = (int) (50 * (*jefe).Escala.X);
		JefeS.radioCollitionMartilloQuad = (int) (50 * 1.2f);
		Jefe_defineTrozoAnim(jefe, (*jefe).actualframe * 409.6,
				(*jefe).lineaAnimacion * 256, 409.6, 256);
		Jefe_setOrigen(jefe);
		Jefe_inicializaMartillosQuad(jefe);
		break;

	case JRued:
		(*jefe).velocidad = 4;
		(*jefe).limiteLlegada = 950;
		(*jefe).vida = (*jefe).maxVida = 50000;
		(*jefe).Escala = GameFramework_Vector2(1.35f, 1.1f);
		(*jefe).Avance1 = 1;
		(*jefe).Avance2 = (*jefe).Avance3 = (*jefe).Avance4 = 0;
		(*jefe).texturaAnim = 3;
		(*jefe).actualframe = 0;
		(*jefe).lineaAnimacion = 0;
		(*jefe).ultimoTDisparoNormal = 0;
		(*jefe).Rotacion = 0;
		JefeS.radioCollitionRued = (int) (200 * (*jefe).Escala.X);
		JefeS.radioCollitionFlechaRued = (int) (94 * 0.5f);
		Jefe_defineTrozoAnim(jefe, (*jefe).actualframe * 409.6,
				(*jefe).lineaAnimacion * 512, 409.6, 512);
		Jefe_setOrigen(jefe);
		Jefe_inicializaFlechasRued(jefe);
		break;
	}
	Jefe_setAumentoVida(jefe, aumentoVida);
}

void Jefe_load()
{
	JefeS.adicionDanoNave = 0;
	Jefe_setTextura();
	Jefe_cargaListaDisparos();
	Jefe_defineCollitionBox();
	Lista_Inicia(&(JefeS.jefesTotales));
	Lista_AddInFin(&(JefeS.jefesTotales), Jefe_crearObject(JLaxo, 300, 2048));
	Lista_AddInFin(&(JefeS.jefesTotales), Jefe_crearObject(JEsp, 300, 2048));
	Lista_AddInFin(&(JefeS.jefesTotales), Jefe_crearObject(JQuad, 300, 2048));
	Lista_AddInFin(&(JefeS.jefesTotales), Jefe_crearObject(JRued, 300, 2048));
}

//metodo update de enemigo
void Jefe_update(Jefe* jefe)
{
	Jefe_defineDatosAnimacion(jefe);
	Jefe_velocidadAnimacion(jefe);
	Jefe_updateDisparos(jefe);
	Jefe_updateAnimaciones(jefe);
	Jefe_updateTiemposEspera(jefe);
}

//metodo draw de enemigo
void Jefe_draw(Jefe* jefe)
{
	/*spriteBatch.Draw(texJefes[(*jefe).texturaAnim], Position, TrozoAnim,
	 Color.White, MathHelper.ToRadians((*jefe).Rotacion), Origen,
	 (*jefe).Escala, SpriteEffects.None, 0.44f);*/

	Dibujar_drawSpriteTr(
			(TexturaObjeto*) Lista_RetornaElemento(&(JefeS.texJefes),
					(*jefe).texturaAnim), (*jefe).Position, (*jefe).TrozoAnim,
			GameFramework_Color(1, 1, 1, 1), (*jefe).Rotacion, (*jefe).Origen,
			(*jefe).Escala);

	Jefe_drawDisparosLaxo(jefe);
	Jefe_drawAnimaciones(jefe);
	Jefe_drawBrazosEsp(jefe);
	Jefe_drawMartillosQuad(jefe);
	Jefe_drawFlechasRued(jefe);
}

//*******************Definicion de metodos agregados*******************

void Jefe_setAumentoVida(Jefe* jefe, int v)
{
	(*jefe).vida = (*jefe).vida + v;
	(*jefe).maxVida = (*jefe).maxVida + v;
}

//(*jefe).velocidad de la animacion
void Jefe_velocidadAnimacion(Jefe* jefe)
{
	(*jefe).dato += GameFramework_getSwapTime();
	if ((*jefe).dato >= (GameFramework_getSwapTime() * (*jefe).velocidad))
	{
		(*jefe).actualframe = (*jefe).actualframe + 1;
		(*jefe).dato = 0;
	}

	if ((*jefe).actualframe >= 5)
	{
		(*jefe).actualframe = 0;
		(*jefe).lineaAnimacion++;
		if ((*jefe).tipoJefe != JLaxo)
		{
			if ((*jefe).lineaAnimacion >= 2)
			{
				(*jefe).lineaAnimacion = 0;
			}
		}
		else
		{
			if ((*jefe).modoIndefenso == 1)
			{
				if ((*jefe).lineaAnimacion >= 4)
				{
					(*jefe).lineaAnimacion = 2;
				}
			}
			else
			{
				if ((*jefe).lineaAnimacion >= 2)
				{
					(*jefe).lineaAnimacion = 0;
				}
			}
		}
	}
}

//Cargar lista (*jefe).disparos
void Jefe_cargaListaDisparos()
{
	Lista_Inicia(&(JefeS.disparosMaxTotal));
	for (JefeS.i = 0; JefeS.i < 50; JefeS.i++)
	{
		Lista_AddInFin(&(JefeS.disparosMaxTotal),
				DisparoJefe_crearObjecto(GameFramework_Vector2(0, 0),
						DJmaximo));
	}

	Lista_Inicia(&(JefeS.disparosNormalTotal));
	for (JefeS.i = 0; JefeS.i < 50; JefeS.i++)
	{
		Lista_AddInFin(&(JefeS.disparosNormalTotal),
				DisparoJefe_crearObjecto(GameFramework_Vector2(0, 0),
						DJnormal));
	}
}

//update (*jefe).disparos laxo
void Jefe_updateDisparos(Jefe* jefe)
{
	if ((*jefe).disparos.size > 0)
	{
		for (JefeS.i = 0; JefeS.i < (*jefe).disparos.size; JefeS.i++)
		{
			DisparoJefe_update(
					(DisparoJefe*) Lista_GetIn(&((*jefe).disparos), JefeS.i));
		}

		for (JefeS.i = 0; JefeS.i < (*jefe).disparos.size; JefeS.i++)
		{
			if ((*(DisparoJefe*) Lista_GetIn(&((*jefe).disparos), JefeS.i)).estaEnColision
					== 1)
			{
				(*(DisparoJefe*) Lista_GetIn(&((*jefe).disparos), JefeS.i)).estaEnColision =
						0;

				if ((*Animation_getSObject()).animacionesDispANaveTotal.size
						> 0)
				{
					Animation_setPosition(
							(Animation*) Lista_GetIn(
									&((*Animation_getSObject()).animacionesDispANaveTotal),
									(*Animation_getSObject()).animacionesDispANaveTotal.size
											- 1),
							(*(DisparoJefe*) Lista_GetIn(&((*jefe).disparos),
									JefeS.i)).Position);

					Lista_AddInFin(&((*jefe).animaciones),
							Lista_GetIn(
									&((*Animation_getSObject()).animacionesDispANaveTotal),
									(*Animation_getSObject()).animacionesDispANaveTotal.size
											- 1));
					Lista_RemoveAt(
							&((*Animation_getSObject()).animacionesDispANaveTotal),
							(*Animation_getSObject()).animacionesDispANaveTotal.size
									- 1);
				}

				if ((*(DisparoJefe*) Lista_GetIn(&((*jefe).disparos), JefeS.i)).tipoActual
						== DJmaximo)
				{
					(*(DisparoJefe*) Lista_GetIn(&((*jefe).disparos), JefeS.i)).postDisparo =
							0;
					Lista_AddInFin(&(JefeS.disparosMaxTotal),
							Lista_GetIn(&((*jefe).disparos), JefeS.i));

					Lista_RemoveAt(&((*jefe).disparos), JefeS.i);
				}
				else
				{
					(*(DisparoJefe*) Lista_GetIn(&((*jefe).disparos), JefeS.i)).postDisparo =
							0;
					Lista_AddInFin(&(JefeS.disparosNormalTotal),
							Lista_GetIn(&((*jefe).disparos), JefeS.i));
					Lista_RemoveAt(&((*jefe).disparos), JefeS.i);
				}
			}
		}

		for (JefeS.i = 0; JefeS.i < (*jefe).disparos.size; JefeS.i++)
		{
			if ((*(DisparoJefe*) Lista_GetIn(&((*jefe).disparos), JefeS.i)).Position.X
					> 600
					|| (*(DisparoJefe*) Lista_GetIn(&((*jefe).disparos),
							JefeS.i)).Position.X < 0
					|| (*(DisparoJefe*) Lista_GetIn(&((*jefe).disparos),
							JefeS.i)).Position.Y < 0
					|| (*(DisparoJefe*) Lista_GetIn(&((*jefe).disparos),
							JefeS.i)).Position.Y > 1024)
			{
				if ((*(DisparoJefe*) Lista_GetIn(&((*jefe).disparos), JefeS.i)).tipoActual
						== DJmaximo)
				{
					(*(DisparoJefe*) Lista_GetIn(&((*jefe).disparos), JefeS.i)).postDisparo =
							0;
					Lista_AddInFin(&(JefeS.disparosMaxTotal),
							Lista_GetIn(&((*jefe).disparos), JefeS.i));

					Lista_RemoveAt(&((*jefe).disparos), JefeS.i);
				}
				else
				{
					(*(DisparoJefe*) Lista_GetIn(&((*jefe).disparos), JefeS.i)).postDisparo =
							0;
					Lista_AddInFin(&(JefeS.disparosNormalTotal),
							Lista_GetIn(&((*jefe).disparos), JefeS.i));

					Lista_RemoveAt(&((*jefe).disparos), JefeS.i);
				}
			}
		} //End for
	} //End if
}

//Draw (*jefe).disparos Laxo
void Jefe_drawDisparosLaxo(Jefe* jefe)
{
	for (JefeS.i = 0; JefeS.i < (*jefe).disparos.size; JefeS.i++)
	{
		DisparoJefe_draw(
				(DisparoJefe*) Lista_GetIn(&((*jefe).disparos), JefeS.i));
	}
}

//define los datos de cada enemigo
void Jefe_defineDatosAnimacion(Jefe* jefe)
{
	switch ((*jefe).tipoJefe)
	{
	case JLaxo:
		Jefe_defineTrozoAnim(jefe, (*jefe).actualframe * 409.6,
				(*jefe).lineaAnimacion * 512, 409.6, 512);
		Jefe_setOrigen(jefe);
		Jefe_movimientoLaxo(jefe);
		break;

	case JEsp:
		Jefe_defineTrozoAnim(jefe, (*jefe).actualframe * 409.6,
				(*jefe).lineaAnimacion * 512, 409.6, 512);
		Jefe_setOrigen(jefe);
		Jefe_movimientoEsp(jefe);
		break;

	case JQuad:
		Jefe_defineTrozoAnim(jefe, (*jefe).actualframe * 409.6,
				(*jefe).lineaAnimacion * 256, 409.6, 256);
		Jefe_setOrigen(jefe);
		Jefe_movimientoQuad(jefe);
		break;

	case JRued:
		Jefe_defineTrozoAnim(jefe, (*jefe).actualframe * 409.6,
				(*jefe).lineaAnimacion * 512, 409.6, 512);
		Jefe_setOrigen(jefe);
		Jefe_movimientoRued(jefe);
		break;
	}
}

//Define movimiento de Lax
void Jefe_movimientoLaxo(Jefe* jefe)
{
	if ((*jefe).Position.Y >= 870)
	{
		(*jefe).Position.Y = (*jefe).Position.Y - 4;
	}

	if (GameFramework_getTotalTime()
			> (*jefe).ultimoTDisparoNormal + (*jefe).tiempoEsperaDisparo)
	{
		if (JefeS.disparosNormalTotal.size > 0)
		{
			DisparoJefe_setPosition(
					(DisparoJefe*) Lista_GetIn(&(JefeS.disparosNormalTotal),
							JefeS.disparosNormalTotal.size - 1),
					(*jefe).Position, 1);

			(*(DisparoJefe*) Lista_GetIn(&(JefeS.disparosNormalTotal),
					JefeS.disparosNormalTotal.size - 1)).Disparar = 1;

			Lista_AddInFin(&((*jefe).disparos),
					Lista_GetIn(&(JefeS.disparosNormalTotal),
							JefeS.disparosNormalTotal.size - 1));

			Lista_RemoveAt(&(JefeS.disparosNormalTotal),
					JefeS.disparosNormalTotal.size - 1);
		}

		(*jefe).ultimoTDisparoNormal = GameFramework_getTotalTime();
	}

	if (GameFramework_getTotalTime()
			> (*jefe).ultimoTDisparoNormalAux
					+ ((*jefe).tiempoEsperaDisparo - 1000000))
	{
		if (JefeS.disparosNormalTotal.size > 0)
		{
			DisparoJefe_setPosition(
					(DisparoJefe*) Lista_GetIn(&(JefeS.disparosNormalTotal),
							JefeS.disparosNormalTotal.size - 1),
					(*jefe).Position, 0);

			(*(DisparoJefe*) Lista_GetIn(&(JefeS.disparosNormalTotal),
					JefeS.disparosNormalTotal.size - 1)).Disparar = 1;

			Lista_AddInFin(&((*jefe).disparos),
					Lista_GetIn(&(JefeS.disparosNormalTotal),
							JefeS.disparosNormalTotal.size - 1));

			Lista_RemoveAt(&(JefeS.disparosNormalTotal),
					JefeS.disparosNormalTotal.size - 1);
		}
		(*jefe).ultimoTDisparoNormalAux = GameFramework_getTotalTime();
	}

	if (GameFramework_getTotalTime() > (*jefe).ultimoTDisparoMax + 6000000)
	{
		if (JefeS.disparosMaxTotal.size > 0)
		{
			DisparoJefe_setPosition2(
					Lista_GetIn(&(JefeS.disparosMaxTotal),
							JefeS.disparosMaxTotal.size - 1), (*jefe).Position);

			(*(DisparoJefe*) Lista_GetIn(&(JefeS.disparosMaxTotal),
					JefeS.disparosMaxTotal.size - 1)).Disparar = 1;

			Lista_AddInFin(&((*jefe).disparos),
					Lista_GetIn(&(JefeS.disparosMaxTotal),
							JefeS.disparosMaxTotal.size - 1));

			Lista_RemoveAt(&(JefeS.disparosMaxTotal),
					JefeS.disparosMaxTotal.size - 1);
		}

		if ((*jefe).modoIndefenso == 1)
		{
			(*jefe).modoIndefenso = 0;
		}
		else
		{
			(*jefe).lineaAnimacion = 2;
			(*jefe).modoIndefenso = 1;
		}
		(*jefe).ultimoTDisparoMax = GameFramework_getTotalTime();
	}

	Jefe_confirmaColisionesLaxo(jefe);
}

//*****Movimiento Esp *****
void Jefe_movimientoEsp(Jefe* jefe)
{
	if ((*jefe).Position.Y >= 820)
	{
		(*jefe).Position.Y = (*jefe).Position.Y - 10;
	}
	else
	{
		if ((*jefe).llegadaFinalizada == 1)
		{
			(*jefe).ultimoTParteAtaca = GameFramework_getTotalTime();
			(*jefe).llegadaFinalizada = 0;
		}
	}

	if (GameFramework_getTotalTime()
			> (*jefe).ultimoTDisparoNormal + (*jefe).tiempoEsperaDisparo)
	{
		if (JefeS.disparosNormalTotal.size > 0)
		{
			DisparoJefe_setPosition(
					(DisparoJefe*) Lista_GetIn(&(JefeS.disparosNormalTotal),
							JefeS.disparosNormalTotal.size - 1),
					(*jefe).Position, 1);

			(*(DisparoJefe*) Lista_GetIn(&(JefeS.disparosNormalTotal),
					JefeS.disparosNormalTotal.size - 1)).Disparar = 1;

			Lista_AddInFin(&((*jefe).disparos),
					Lista_GetIn(&(JefeS.disparosNormalTotal),
							JefeS.disparosNormalTotal.size - 1));

			Lista_RemoveAt(&(JefeS.disparosNormalTotal),
					JefeS.disparosNormalTotal.size - 1);
		}

		(*jefe).ultimoTDisparoNormal = GameFramework_getTotalTime();
	}

	if (GameFramework_getTotalTime()
			> (*jefe).ultimoTDisparoNormalAux
					+ ((*jefe).tiempoEsperaDisparo - 1000000))
	{
		if (JefeS.disparosNormalTotal.size > 0)
		{
			DisparoJefe_setPosition(
					(DisparoJefe*) Lista_GetIn(&(JefeS.disparosNormalTotal),
							JefeS.disparosNormalTotal.size - 1),
					(*jefe).Position, 0);

			(*(DisparoJefe*) Lista_GetIn(&(JefeS.disparosNormalTotal),
					JefeS.disparosNormalTotal.size - 1)).Disparar = 1;

			Lista_AddInFin(&((*jefe).disparos),
					Lista_GetIn(&(JefeS.disparosNormalTotal),
							JefeS.disparosNormalTotal.size - 1));

			Lista_RemoveAt(&(JefeS.disparosNormalTotal),
					JefeS.disparosNormalTotal.size - 1);
		}

		(*jefe).ultimoTDisparoNormalAux = GameFramework_getTotalTime();
	}

	Jefe_updateBrazosEsp(jefe);
	Jefe_confirmaColisionesEsp(jefe);
}

//Movimeinto Quad
void Jefe_movimientoQuad(Jefe* jefe)
{
	if ((*jefe).Position.Y >= 840)
	{
		(*jefe).Position.Y = (*jefe).Position.Y - 10;
	}
	else
	{
		if ((*jefe).llegadaFinalizada)
		{
			(*jefe).ultimoTParteAtaca = GameFramework_getTotalTime();
			(*jefe).llegadaFinalizada = 0;
		}
	}

	if (GameFramework_getTotalTime()
			> (*jefe).ultimoTDisparoNormalAux2
					+ ((*jefe).tiempoEsperaDisparo - 1000000))
	{
		if (JefeS.disparosNormalTotal.size > 0)
		{
			DisparoJefe_setPosition2(
					(DisparoJefe*) Lista_GetIn(&(JefeS.disparosNormalTotal),
							JefeS.disparosNormalTotal.size - 1),
					(*jefe).Position);

			(*(DisparoJefe*) Lista_GetIn(&(JefeS.disparosNormalTotal),
					JefeS.disparosNormalTotal.size - 1)).Disparar = 1;

			Lista_AddInFin(&((*jefe).disparos),
					Lista_GetIn(&(JefeS.disparosNormalTotal),
							JefeS.disparosNormalTotal.size - 1));

			Lista_RemoveAt(&(JefeS.disparosNormalTotal),
					JefeS.disparosNormalTotal.size - 1);
		}

		(*jefe).ultimoTDisparoNormalAux2 = GameFramework_getTotalTime();
	}

	Jefe_updateMartillosQuad(jefe);
	Jefe_confirmaColisionesQuad(jefe);
}

//Movimiento Rued
void Jefe_movimientoRued(Jefe* jefe)
{
	if ((*jefe).vida <= (((*jefe).maxVida * 3) / 4) && (*jefe).Avance1)
	{
		(*jefe).limiteLlegada = (*jefe).limiteLlegada - 150;
		(*jefe).Avance1 = 0;
		(*jefe).Avance2 = 1;
	}
	if ((*jefe).vida <= (((*jefe).maxVida * 2) / 4) && (*jefe).Avance2)
	{
		(*jefe).limiteLlegada = (*jefe).limiteLlegada - 150;
		(*jefe).Avance2 = 0;
		(*jefe).Avance3 = 1;
	}
	if ((*jefe).vida <= (((*jefe).maxVida * 1) / 4) && (*jefe).Avance3)
	{
		(*jefe).limiteLlegada = (*jefe).limiteLlegada - 100;
		(*jefe).Avance3 = 0;
		(*jefe).Avance4 = 1;
	}
	if ((*jefe).vida <= 100 && (*jefe).Avance4)
	{
		(*jefe).limiteLlegada = (*jefe).limiteLlegada - 80;
		(*jefe).Avance4 = 0;
	}

	if ((*jefe).Position.Y >= (*jefe).limiteLlegada)
	{
		if ((*jefe).vida > 100)
		{
			(*jefe).Position.Y = (*jefe).Position.Y - 10;
		}
		else
		{
			(*jefe).Position.Y = (*jefe).Position.Y - 1;
		}
	}
	else
	{
		if ((*jefe).llegadaFinalizada == 1)
		{
			(*jefe).ultimoTParteAtaca = GameFramework_getTotalTime();
			(*jefe).llegadaFinalizada = 0;
		}
	}

	if (GameFramework_getTotalTime()
			> (*jefe).ultimoTDisparoNormalAux2
					+ ((*jefe).tiempoEsperaDisparo - 1000000))
	{
		if (JefeS.disparosNormalTotal.size > 0)
		{
			DisparoJefe_setPosition2(
					(DisparoJefe*) Lista_GetIn(&(JefeS.disparosNormalTotal),
							JefeS.disparosNormalTotal.size - 1),
					(*jefe).Position);

			(*(DisparoJefe*) Lista_GetIn(&(JefeS.disparosNormalTotal),
					JefeS.disparosNormalTotal.size - 1)).Disparar = 1;

			Lista_AddInFin(&((*jefe).disparos),
					Lista_GetIn(&(JefeS.disparosNormalTotal),
							JefeS.disparosNormalTotal.size - 1));

			Lista_RemoveAt(&(JefeS.disparosNormalTotal),
					JefeS.disparosNormalTotal.size - 1);
		}

		(*jefe).ultimoTDisparoNormalAux2 = GameFramework_getTotalTime();
	}

	Jefe_updateFlechasRued(jefe);
}

//* ********* Update Animaciones ************

void Jefe_updateAnimaciones(Jefe* jefe)
{
	if ((*jefe).animaciones.size > 0)
	{
		for (JefeS.i = 0; JefeS.i < (*jefe).animaciones.size; JefeS.i++)
		{
			Animation_update(
					(Animation*) Lista_GetIn(&((*jefe).animaciones), JefeS.i));
		}
	}
	if ((*jefe).animaciones.size > 0)
	{
		for (JefeS.i = 0; JefeS.i < (*jefe).animaciones.size; JefeS.i++)
		{
			if ((*(Animation*) Lista_GetIn(&((*jefe).animaciones), JefeS.i)).animacionFinalizada
					== 1
					&& (*(Animation*) Lista_GetIn(&((*jefe).animaciones),
							JefeS.i)).tipoActual == ADisparoANave)
			{
				(*(Animation*) Lista_GetIn(&((*jefe).animaciones), JefeS.i)).animacionFinalizada =
						0;

				Lista_AddInFin(
						&((*Animation_getSObject()).animacionesDispANaveTotal),
						Lista_GetIn(&((*jefe).animaciones), JefeS.i));

				Lista_RemoveAt(&((*jefe).animaciones), JefeS.i);
			}
		}
	}
}

//***************** Draw (*jefe).animaciones ***********

void Jefe_drawAnimaciones(Jefe* jefe)
{
	if ((*jefe).animaciones.size > 0)
	{
		for (JefeS.i = 0; JefeS.i < (*jefe).animaciones.size; JefeS.i++)
		{
			Animation_draw(
					(Animation*) Lista_GetIn(&((*jefe).animaciones), JefeS.i));
		}
	}
}

//define el cuandro de la animacion actual
void Jefe_defineTrozoAnim(Jefe* jefe, int x, int y, int width, int height)
{
	(*jefe).TrozoAnim.X = x;
	(*jefe).TrozoAnim.Y = y;
	(*jefe).TrozoAnim.Width = width;
	(*jefe).TrozoAnim.Height = height;
}

void Jefe_updateTiemposEspera(Jefe* jefe)
{
	if ((*jefe).vida <= (((*jefe).maxVida * 6) / 8)
			&& (*jefe).vida > (((*jefe).maxVida * 5) / 8))
	{
		(*jefe).tiempoEsperaDisparo = 7000000;
		(*jefe).tiempoEsperaAtaque = 6000000;
	}
	if ((*jefe).vida <= (((*jefe).maxVida * 5) / 8)
			&& (*jefe).vida > (((*jefe).maxVida * 2) / 8))
	{
		(*jefe).tiempoEsperaDisparo = 5000000;
		(*jefe).tiempoEsperaAtaque = 5000000;
	}
	if ((*jefe).vida <= (((*jefe).maxVida * 2) / 8)
			&& (*jefe).vida > (((*jefe).maxVida * 1) / 8))
	{
		(*jefe).tiempoEsperaDisparo = 3000000;
		(*jefe).tiempoEsperaAtaque = 4000000;
	}
	if ((*jefe).vida <= (((*jefe).maxVida * 1) / 8))
	{
		(*jefe).tiempoEsperaDisparo = 2000000;
		(*jefe).tiempoEsperaAtaque = 3000000;
	}
}

//Inicializa Brazos Esp
void Jefe_inicializaBrazosEsp(Jefe* jefe)
{
	(*jefe).EscalaParte = GameFramework_Vector2(1.25f, 1.25f);
	(*jefe).ultimoTParteAtaca = 0;
	(*jefe).parteAtaca = 0;
	(*jefe).detenerUnTiempo = 0;
	(*jefe).tiempoDetencion = 0;
	(*jefe).devolverParte = 0;
	(*jefe).parteReposo = 1;
	(*jefe).colorParte1.R = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte1.G = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte1.B = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte1.A = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte2.R = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte2.G = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte2.B = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte2.A = GameFramework_getByteColorToFloatColor(255);
	(*jefe).velocidadCambioColor = 35;
	(*jefe).tonoAzul = 101;
	(*jefe).tonoVerde = 101;
	(*jefe).numeroAleatorio = (rand() % 3) + 1; // GameFramework_Rand(1, 4);
	(*jefe).llegadaFinalizada = 1;

	(*jefe).PositionParte1.X = -400;
	(*jefe).PositionParte1.Y = 720;
	(*jefe).TrozoAnimParte1.X = 0;
	(*jefe).TrozoAnimParte1.Y = 0;
	(*jefe).TrozoAnimParte1.Width = 512;
	(*jefe).TrozoAnimParte1.Height = 64;
	(*jefe).OrigenParte1.X = 10; //10
	(*jefe).OrigenParte1.Y = 31; //31
	(*jefe).RotacionParte1 = -50; //50

	(*jefe).PositionParte2.X = 1000;
	(*jefe).PositionParte2.Y = 720;
	(*jefe).TrozoAnimParte2.X = 0;
	(*jefe).TrozoAnimParte2.Y = 0;
	(*jefe).TrozoAnimParte2.Width = 512;
	(*jefe).TrozoAnimParte2.Height = 64;
	(*jefe).OrigenParte2.X = 10; //490;
	(*jefe).OrigenParte2.Y = 31; //31;
	(*jefe).RotacionParte2 = -130; //-50
}

// Inicializa Martillos Quad
void Jefe_inicializaMartillosQuad(Jefe* jefe)
{
	(*jefe).EscalaParte = GameFramework_Vector2(1.0f, 1.4f);
	(*jefe).ultimoTParteAtaca = 0;
	(*jefe).parteAtaca = 0;
	(*jefe).detenerUnTiempo = 0;
	(*jefe).tiempoDetencion = 0;
	(*jefe).devolverParte = 0;
	(*jefe).parteReposo = 1;
	(*jefe).colorParte1.R = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte1.G = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte1.B = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte1.A = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte2.R = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte2.G = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte2.B = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte2.A = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte3.R = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte3.G = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte3.B = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte3.A = GameFramework_getByteColorToFloatColor(255);
	(*jefe).velocidadCambioColor = 35;
	(*jefe).tonoAzul = 101;
	(*jefe).tonoVerde = 101;
	(*jefe).numeroAleatorio = 3;
	(*jefe).llegadaFinalizada = 1;

	(*jefe).PositionParte1.X = 800;
	(*jefe).PositionParte1.Y = 130;
	(*jefe).PositionParte2.X = 800;
	(*jefe).PositionParte2.Y = 330;
	(*jefe).PositionParte3.X = 800;
	(*jefe).PositionParte3.Y = 530;

	(*jefe).RotacionParte1 = 0;
	(*jefe).RotacionParte2 = 0;
	(*jefe).RotacionParte3 = 0;
	(*jefe).actualframeParte = 0;
	(*jefe).lineaAnimacionParte = 0;
	Jefe_defineTrozoAnimMartillo(jefe, (*jefe).actualframeParte * 409.6,
			(*jefe).lineaAnimacionParte * 128, 409.6, 128);
	Jefe_setOrigenMartillo(jefe);
}

// Inicializa Flechas Rued
void Jefe_inicializaFlechasRued(Jefe* jefe)
{
	(*jefe).EscalaParte = GameFramework_Vector2(0.8, 0.5);
	(*jefe).velocidadCambioColor = 35;
	(*jefe).tonoAzul = 101;
	(*jefe).tonoVerde = 101;
	(*jefe).numeroAleatorio = (rand() % 3) + 1; // GameFramework_Rand(1, 4);
	(*jefe).llegadaFinalizada = 1;
	(*jefe).PositionParte1.X = 120;
	(*jefe).PositionParte1.Y = (*jefe).Position.Y - 250;
	(*jefe).PositionParte2.X = 300;
	(*jefe).PositionParte2.Y = (*jefe).Position.Y - 250;
	(*jefe).PositionParte3.X = 480;
	(*jefe).PositionParte3.Y = (*jefe).Position.Y - 250;
	(*jefe).colorParte1.R = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte1.G = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte1.B = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte1.A = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte2.R = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte2.G = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte2.B = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte2.A = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte3.R = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte3.G = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte3.B = GameFramework_getByteColorToFloatColor(255);
	(*jefe).colorParte3.A = GameFramework_getByteColorToFloatColor(255);
	(*jefe).RotacionParte1 = 180;
	(*jefe).RotacionParte2 = 180;
	(*jefe).RotacionParte3 = 180;
	(*jefe).actualframeParte = 0;
	(*jefe).lineaAnimacionParte = 0;
	(*jefe).parteAtaca = 0;
	(*jefe).parteReposo = 1;
	(*jefe).devolverParte = 0;
	(*jefe).detenerUnTiempo = 0;
	(*jefe).ultimoTParteAtaca = 0;
	(*jefe).tiempoDetencion = 0;
	Jefe_defineTrozoAnimMartillo(jefe, (*jefe).actualframeParte * 204.8, 0,
			204.8, 256);
	Jefe_setOrigenFlecha(jefe);
}

//Actualiza brazos de Esp
void Jefe_updateBrazosEsp(Jefe* jefe)
{
	if ((*jefe).PositionParte1.X < 0)
	{
		(*jefe).PositionParte1.X = (*jefe).PositionParte1.X + 3;
	}
	if ((*jefe).PositionParte2.X > 600)
	{
		(*jefe).PositionParte2.X = (*jefe).PositionParte2.X - 3;
	}
	if ((*jefe).PositionParte1.X >= 0 && (*jefe).PositionParte2.X <= 600
			&& (*jefe).parteReposo == 1)
	{
		if ((((*jefe).ultimoTParteAtaca + (*jefe).tiempoEsperaAtaque)
				- GameFramework_getTotalTime()) > 2000000)
		{
			(*jefe).numeroAleatorio = GameFramework_Rand(1, 3);
		}

		if (((*jefe).ultimoTParteAtaca + (*jefe).tiempoEsperaAtaque)
				- GameFramework_getTotalTime() <= 2000000)
		{
			(*jefe).tonoAzul = (*jefe).tonoAzul + (*jefe).velocidadCambioColor;
			(*jefe).tonoVerde = (*jefe).tonoVerde
					+ (*jefe).velocidadCambioColor;

			if ((*jefe).tonoAzul <= 100 || (*jefe).tonoAzul >= 235)
			{
				(*jefe).velocidadCambioColor = (*jefe).velocidadCambioColor
						* (-1);
			}

			switch ((*jefe).numeroAleatorio)
			{
			case 1:
				(*jefe).colorParte1.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte1.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				break;
			case 2:
				(*jefe).colorParte2.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte2.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				break;
			case 3:
				(*jefe).colorParte1.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte1.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				(*jefe).colorParte2.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte2.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				break;
			}
		}

		if (GameFramework_getTotalTime()
				> ((*jefe).ultimoTParteAtaca + (*jefe).tiempoEsperaAtaque))
		{
			(*jefe).parteAtaca = 1;
			(*jefe).parteReposo = 0;
			(*jefe).colorParte1.B = GameFramework_getByteColorToFloatColor(254);
			(*jefe).colorParte1.G = GameFramework_getByteColorToFloatColor(254);
			(*jefe).colorParte2.B = GameFramework_getByteColorToFloatColor(254);
			(*jefe).colorParte2.G = GameFramework_getByteColorToFloatColor(254);
		}
	}

	if ((*jefe).parteAtaca == 1)
	{
		switch ((*jefe).numeroAleatorio)
		{
		case 1:
			if ((*jefe).RotacionParte1 > -90)
			{
				(*jefe).RotacionParte1 = (*jefe).RotacionParte1 - 5;
			}
			else
			{
				(*jefe).detenerUnTiempo = 1;
				(*jefe).parteAtaca = 0;
				(*jefe).tiempoDetencion = GameFramework_getTotalTime();
			}
			break;

		case 2:
			if ((*jefe).RotacionParte2 < -90)
			{
				(*jefe).RotacionParte2 = (*jefe).RotacionParte2 + 5;
			}
			else
			{
				(*jefe).detenerUnTiempo = 1;
				(*jefe).parteAtaca = 0;
				(*jefe).tiempoDetencion = GameFramework_getTotalTime();
			}
			break;

		case 3:
			if ((*jefe).RotacionParte1 > -90 && (*jefe).RotacionParte2 < -90)
			{
				(*jefe).RotacionParte1 = (*jefe).RotacionParte1 - 5;
				(*jefe).RotacionParte2 = (*jefe).RotacionParte2 + 5;
			}
			else
			{
				(*jefe).detenerUnTiempo = 1;
				(*jefe).parteAtaca = 0;
				(*jefe).tiempoDetencion = GameFramework_getTotalTime();
			}
			break;
		}
	}

	if ((*jefe).detenerUnTiempo == 1)
	{
		if (GameFramework_getTotalTime() > (*jefe).tiempoDetencion + 5000000)
		{
			(*jefe).devolverParte = 1;
			(*jefe).detenerUnTiempo = 0;
		}
	}

	if ((*jefe).devolverParte == 1)
	{
		if ((*jefe).RotacionParte1 < -50)
		{
			(*jefe).RotacionParte1 = (*jefe).RotacionParte1 + 0.3f;
		}
		if ((*jefe).RotacionParte2 > -130)
		{
			(*jefe).RotacionParte2 = (*jefe).RotacionParte2 - 0.3f;
		}
		if ((*jefe).RotacionParte1 >= -50 && (*jefe).RotacionParte2 <= -130)
		{
			(*jefe).devolverParte = 0;
			(*jefe).parteReposo = 1;
			(*jefe).ultimoTParteAtaca = GameFramework_getTotalTime();
		}
	}
}

//Actualiza martillos Quad
void Jefe_updateMartillosQuad(Jefe* jefe)
{
	if ((*jefe).PositionParte1.X > 530)
	{
		(*jefe).PositionParte1.X = (*jefe).PositionParte1.X - 3;
	}
	if ((*jefe).PositionParte2.X > 530)
	{
		(*jefe).PositionParte2.X = (*jefe).PositionParte2.X - 3;
	}
	if ((*jefe).PositionParte3.X > 530)
	{
		(*jefe).PositionParte3.X = (*jefe).PositionParte3.X - 3;
	}
	Jefe_defineTrozoAnimMartillo(jefe, (*jefe).actualframeParte * 409.6,
			(*jefe).lineaAnimacionParte * 128, 409.6, 128);
	Jefe_setOrigenMartillo(jefe);

	if ((*jefe).PositionParte1.X <= 530 && (*jefe).PositionParte2.X <= 530
			&& (*jefe).PositionParte3.X <= 530 && (*jefe).parteReposo == 1)
	{
		if ((((*jefe).ultimoTParteAtaca + (*jefe).tiempoEsperaAtaque)
				- GameFramework_getTotalTime()) > 2000000)
		{
			(*jefe).numeroAleatorio = GameFramework_Rand(1, 6);
		}

		if (((*jefe).ultimoTParteAtaca + (*jefe).tiempoEsperaAtaque)
				- GameFramework_getTotalTime() <= 2000000)
		{
			(*jefe).tonoAzul = (*jefe).tonoAzul + (*jefe).velocidadCambioColor;
			(*jefe).tonoVerde = (*jefe).tonoVerde
					+ (*jefe).velocidadCambioColor;

			if ((*jefe).tonoAzul <= 100 || (*jefe).tonoAzul >= 235)
			{
				(*jefe).velocidadCambioColor = (*jefe).velocidadCambioColor
						* (-1);
			}

			switch ((*jefe).numeroAleatorio)
			{
			case 1:
				(*jefe).colorParte1.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte1.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				break;
			case 2:
				(*jefe).colorParte2.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte2.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				break;
			case 3:
				(*jefe).colorParte3.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte3.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				break;
			case 4:
				(*jefe).colorParte1.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte1.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				(*jefe).colorParte2.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte2.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				break;
			case 5:
				(*jefe).colorParte1.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte1.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				(*jefe).colorParte3.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte3.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				break;
			case 6:
				(*jefe).colorParte2.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte2.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				(*jefe).colorParte3.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte3.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				break;
			}
		}

		if (GameFramework_getTotalTime()
				> ((*jefe).ultimoTParteAtaca + (*jefe).tiempoEsperaAtaque))
		{
			(*jefe).parteAtaca = 1;
			(*jefe).parteReposo = 0;
			(*jefe).colorParte1.B = GameFramework_getByteColorToFloatColor(254);
			(*jefe).colorParte1.G = GameFramework_getByteColorToFloatColor(254);
			(*jefe).colorParte2.B = GameFramework_getByteColorToFloatColor(254);
			(*jefe).colorParte2.G = GameFramework_getByteColorToFloatColor(254);
			(*jefe).colorParte3.B = GameFramework_getByteColorToFloatColor(254);
			(*jefe).colorParte3.G = GameFramework_getByteColorToFloatColor(254);
		}
	}

	if ((*jefe).parteAtaca == 1)
	{
		switch ((*jefe).numeroAleatorio)
		{
		case 1:
			if ((*jefe).PositionParte1.X > 0)
			{
				(*jefe).PositionParte1.X = (*jefe).PositionParte1.X - 20;
			}
			else
			{
				(*jefe).detenerUnTiempo = 1;
				(*jefe).parteAtaca = 0;
				(*jefe).tiempoDetencion = GameFramework_getTotalTime();
			}
			break;

		case 2:
			if ((*jefe).PositionParte2.X > 0)
			{
				(*jefe).PositionParte2.X = (*jefe).PositionParte2.X - 20;
			}
			else
			{
				(*jefe).detenerUnTiempo = 1;
				(*jefe).parteAtaca = 0;
				(*jefe).tiempoDetencion = GameFramework_getTotalTime();
			}
			break;

		case 3:
			if ((*jefe).PositionParte3.X > 0)
			{
				(*jefe).PositionParte3.X = (*jefe).PositionParte3.X - 20;
			}
			else
			{
				(*jefe).detenerUnTiempo = 1;
				(*jefe).parteAtaca = 0;
				(*jefe).tiempoDetencion = GameFramework_getTotalTime();
			}
			break;

		case 4:
			if ((*jefe).PositionParte1.X > 0 && (*jefe).PositionParte2.X > 0)
			{
				(*jefe).PositionParte1.X = (*jefe).PositionParte1.X - 20;
				(*jefe).PositionParte2.X = (*jefe).PositionParte2.X - 20;
			}
			else
			{
				(*jefe).detenerUnTiempo = 1;
				(*jefe).parteAtaca = 0;
				(*jefe).tiempoDetencion = GameFramework_getTotalTime();
			}
			break;

		case 5:
			if ((*jefe).PositionParte1.X > 0 && (*jefe).PositionParte3.X > 0)
			{
				(*jefe).PositionParte1.X = (*jefe).PositionParte1.X - 20;
				(*jefe).PositionParte3.X = (*jefe).PositionParte3.X - 20;
			}
			else
			{
				(*jefe).detenerUnTiempo = 1;
				(*jefe).parteAtaca = 0;
				(*jefe).tiempoDetencion = GameFramework_getTotalTime();
			}
			break;

		case 6:
			if ((*jefe).PositionParte3.X > 0 && (*jefe).PositionParte2.X > 0)
			{
				(*jefe).PositionParte3.X = (*jefe).PositionParte3.X - 20;
				(*jefe).PositionParte2.X = (*jefe).PositionParte2.X - 20;
			}
			else
			{
				(*jefe).detenerUnTiempo = 1;
				(*jefe).parteAtaca = 0;
				(*jefe).tiempoDetencion = GameFramework_getTotalTime();
			}
			break;
		}
	}

	if ((*jefe).detenerUnTiempo == 1)
	{
		if (GameFramework_getTotalTime() > (*jefe).tiempoDetencion + 4000000)
		{
			(*jefe).devolverParte = 1;
			(*jefe).detenerUnTiempo = 0;
		}
	}

	if ((*jefe).devolverParte == 1)
	{
		if ((*jefe).PositionParte1.X < 530)
		{
			(*jefe).PositionParte1.X = (*jefe).PositionParte1.X + 10;
		}
		if ((*jefe).PositionParte2.X < 530)
		{
			(*jefe).PositionParte2.X = (*jefe).PositionParte2.X + 10;
		}
		if ((*jefe).PositionParte3.X < 530)
		{
			(*jefe).PositionParte3.X = (*jefe).PositionParte3.X + 10;
		}
		if ((*jefe).PositionParte1.X >= 530 && (*jefe).PositionParte2.X >= 530
				&& (*jefe).PositionParte3.X >= 530)
		{
			(*jefe).devolverParte = 0;
			(*jefe).parteReposo = 1;
			(*jefe).ultimoTParteAtaca = GameFramework_getTotalTime();
		}
	}

	Jefe_velocidadAnimacionMartillo(jefe);
}

//Actualiza flechas Rued
void Jefe_updateFlechasRued(Jefe* jefe)
{
	if ((*jefe).PositionParte1.Y > (*jefe).Position.Y - 250)
	{
		if ((*jefe).vida > 100)
		{
			(*jefe).PositionParte1.Y = (*jefe).PositionParte1.Y - 10;
		}
		else
		{
			(*jefe).PositionParte1.Y = (*jefe).PositionParte1.Y - 1;
		}
	}
	if ((*jefe).PositionParte2.Y > (*jefe).Position.Y - 250)
	{
		if ((*jefe).vida > 100)
		{
			(*jefe).PositionParte2.Y = (*jefe).PositionParte2.Y - 10;
		}
		else
		{
			(*jefe).PositionParte2.Y = (*jefe).PositionParte2.Y - 1;
		}
	}
	if ((*jefe).PositionParte3.Y > (*jefe).Position.Y - 250)
	{
		if ((*jefe).vida > 100)
		{
			(*jefe).PositionParte3.Y = (*jefe).PositionParte3.Y - 10;
		}
		else
		{
			(*jefe).PositionParte3.Y = (*jefe).PositionParte3.Y - 1;
		}
	}

	Jefe_defineTrozoAnimMartillo(jefe, (*jefe).actualframeParte * 204.8, 0,
			204.8, 256);
	Jefe_setOrigenFlecha(jefe);

	if ((*jefe).PositionParte1.Y <= (*jefe).Position.Y - 250
			&& (*jefe).PositionParte2.Y <= (*jefe).Position.Y - 250
			&& (*jefe).PositionParte3.Y <= (*jefe).Position.Y - 250
			&& (*jefe).parteReposo)
	{
		if ((((*jefe).ultimoTParteAtaca + (*jefe).tiempoEsperaAtaque)
				- GameFramework_getTotalTime()) > 2000000)
		{
			(*jefe).numeroAleatorio = GameFramework_Rand(1, 6);
		}

		if (((*jefe).ultimoTParteAtaca + (*jefe).tiempoEsperaAtaque)
				- GameFramework_getTotalTime() <= 2000000)
		{
			(*jefe).tonoAzul = (*jefe).tonoAzul + (*jefe).velocidadCambioColor;
			(*jefe).tonoVerde = (*jefe).tonoVerde
					+ (*jefe).velocidadCambioColor;

			if ((*jefe).tonoAzul <= 100 || (*jefe).tonoAzul >= 235)
			{
				(*jefe).velocidadCambioColor = (*jefe).velocidadCambioColor
						* (-1);
			}

			switch ((*jefe).numeroAleatorio)
			{
			case 1:
				(*jefe).colorParte1.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte1.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				break;
			case 2:
				(*jefe).colorParte2.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte2.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				break;
			case 3:
				(*jefe).colorParte3.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte3.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				break;
			case 4:
				(*jefe).colorParte1.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte1.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				(*jefe).colorParte2.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte2.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				break;
			case 5:
				(*jefe).colorParte1.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte1.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				(*jefe).colorParte3.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte3.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				break;
			case 6:
				(*jefe).colorParte2.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte2.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				(*jefe).colorParte3.B = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoAzul);
				(*jefe).colorParte3.G = GameFramework_getByteColorToFloatColor(
						(*jefe).tonoVerde);
				break;
			}
		}

		if (GameFramework_getTotalTime()
				> ((*jefe).ultimoTParteAtaca + (*jefe).tiempoEsperaAtaque))
		{
			(*jefe).parteAtaca = 1;
			(*jefe).parteReposo = 0;
			(*jefe).colorParte1.B = GameFramework_getByteColorToFloatColor(254);
			(*jefe).colorParte1.G = GameFramework_getByteColorToFloatColor(254);
			(*jefe).colorParte2.B = GameFramework_getByteColorToFloatColor(254);
			(*jefe).colorParte2.G = GameFramework_getByteColorToFloatColor(254);
			(*jefe).colorParte3.B = GameFramework_getByteColorToFloatColor(254);
			(*jefe).colorParte3.G = GameFramework_getByteColorToFloatColor(254);
		}
	}

	if ((*jefe).parteAtaca==1)
	{
		switch ((*jefe).numeroAleatorio)
		{
		case 1:
			if ((*jefe).PositionParte1.Y > 0)
			{
				(*jefe).PositionParte1.Y = (*jefe).PositionParte1.Y - 20;
			}
			else
			{
				(*jefe).detenerUnTiempo = 1;
				(*jefe).parteAtaca = 0;
				(*jefe).tiempoDetencion = GameFramework_getTotalTime();
			}
			break;

		case 2:
			if ((*jefe).PositionParte2.Y > 0)
			{
				(*jefe).PositionParte2.Y = (*jefe).PositionParte2.Y - 20;
			}
			else
			{
				(*jefe).detenerUnTiempo = 1;
				(*jefe).parteAtaca = 0;
				(*jefe).tiempoDetencion = GameFramework_getTotalTime();
			}
			break;

		case 3:
			if ((*jefe).PositionParte3.Y > 0)
			{
				(*jefe).PositionParte3.Y = (*jefe).PositionParte3.Y - 20;
			}
			else
			{
				(*jefe).detenerUnTiempo = 1;
				(*jefe).parteAtaca = 0;
				(*jefe).tiempoDetencion = GameFramework_getTotalTime();
			}
			break;

		case 4:
			if ((*jefe).PositionParte1.Y > 0
					&& (*jefe).PositionParte2.Y > 0)
			{
				(*jefe).PositionParte1.Y = (*jefe).PositionParte1.Y - 20;
				(*jefe).PositionParte2.Y = (*jefe).PositionParte2.Y - 20;
			}
			else
			{
				(*jefe).detenerUnTiempo = 1;
				(*jefe).parteAtaca = 0;
				(*jefe).tiempoDetencion = GameFramework_getTotalTime();
			}
			break;

		case 5:
			if ((*jefe).PositionParte1.Y > 0
					&& (*jefe).PositionParte3.Y > 0)
			{
				(*jefe).PositionParte1.Y = (*jefe).PositionParte1.Y - 20;
				(*jefe).PositionParte3.Y = (*jefe).PositionParte3.Y - 20;
			}
			else
			{
				(*jefe).detenerUnTiempo = 1;
				(*jefe).parteAtaca = 0;
				(*jefe).tiempoDetencion = GameFramework_getTotalTime();
			}
			break;

		case 6:
			if ((*jefe).PositionParte3.Y > 0
					&& (*jefe).PositionParte2.Y > 0)
			{
				(*jefe).PositionParte3.Y = (*jefe).PositionParte3.Y - 20;
				(*jefe).PositionParte2.Y = (*jefe).PositionParte2.Y - 20;
			}
			else
			{
				(*jefe).detenerUnTiempo = 1;
				(*jefe).parteAtaca = 0;
				(*jefe).tiempoDetencion = GameFramework_getTotalTime();
			}
			break;
		}
	}

	if ((*jefe).detenerUnTiempo == 1)
	{
		if (GameFramework_getTotalTime() > (*jefe).tiempoDetencion + 2000000)
		{
			(*jefe).devolverParte = 1;
			(*jefe).detenerUnTiempo = 0;
		}
	}

	if ((*jefe).devolverParte == 1)
	{
		if ((*jefe).PositionParte1.Y <= (*jefe).Position.Y - 250)
		{
			(*jefe).PositionParte1.Y = (*jefe).PositionParte1.Y + 10;
		}
		if ((*jefe).PositionParte2.Y <= (*jefe).Position.Y - 250)
		{
			(*jefe).PositionParte2.Y = (*jefe).PositionParte2.Y + 10;
		}
		if ((*jefe).PositionParte3.Y <= (*jefe).Position.Y - 250)
		{
			(*jefe).PositionParte3.Y = (*jefe).PositionParte3.Y + 10;
		}
		if ((*jefe).PositionParte1.Y > (*jefe).Position.Y - 250
				&& (*jefe).PositionParte2.Y > (*jefe).Position.Y - 250
				&& (*jefe).PositionParte3.Y > (*jefe).Position.Y - 250)
		{
			(*jefe).devolverParte = 0;
			(*jefe).parteReposo = 1;
			(*jefe).ultimoTParteAtaca = GameFramework_getTotalTime();
		}
	}

	Jefe_velocidadAnimacionFlecha(jefe);
	Jefe_confirmaColisionesRued(jefe);
}

//Dibuja brazos Esp
void Jefe_drawBrazosEsp(Jefe* jefe)
{
	if ((*jefe).tipoJefe == JEsp)
	{
		Dibujar_drawSpriteTr(
				(TexturaObjeto*) Lista_RetornaElemento(&(JefeS.texPartesJefe),
						0), (*jefe).PositionParte1, (*jefe).TrozoAnimParte1,
				(*jefe).colorParte1, (*jefe).RotacionParte1,
				(*jefe).OrigenParte1, (*jefe).EscalaParte);

		Dibujar_drawSpriteTr(
				(TexturaObjeto*) Lista_RetornaElemento(&(JefeS.texPartesJefe),
						1), (*jefe).PositionParte2, (*jefe).TrozoAnimParte2,
				(*jefe).colorParte2, (*jefe).RotacionParte2,
				(*jefe).OrigenParte2, (*jefe).EscalaParte);
	}
}

//Dibuja Martillo Quad
void Jefe_drawMartillosQuad(Jefe* jefe)
{
	if ((*jefe).tipoJefe == JQuad)
	{
		Dibujar_drawSpriteTr(
				(TexturaObjeto*) Lista_RetornaElemento(&(JefeS.texPartesJefe),
						2), (*jefe).PositionParte1, (*jefe).TrozoAnimParte,
				(*jefe).colorParte1, (*jefe).RotacionParte1,
				(*jefe).OrigenParte, (*jefe).EscalaParte);

		Dibujar_drawSpriteTr(
				(TexturaObjeto*) Lista_RetornaElemento(&(JefeS.texPartesJefe),
						2), (*jefe).PositionParte2, (*jefe).TrozoAnimParte,
				(*jefe).colorParte2, (*jefe).RotacionParte2,
				(*jefe).OrigenParte, (*jefe).EscalaParte);

		Dibujar_drawSpriteTr(
				(TexturaObjeto*) Lista_RetornaElemento(&(JefeS.texPartesJefe),
						2), (*jefe).PositionParte3, (*jefe).TrozoAnimParte,
				(*jefe).colorParte3, (*jefe).RotacionParte3,
				(*jefe).OrigenParte, (*jefe).EscalaParte);
	}
}

//Dibuja Flechas Rued
void Jefe_drawFlechasRued(Jefe* jefe)
{
	if ((*jefe).tipoJefe == JRued)
	{

		Dibujar_drawSpriteTr(
				(TexturaObjeto*) Lista_RetornaElemento(&(JefeS.texPartesJefe),
						3), (*jefe).PositionParte1, (*jefe).TrozoAnimParte,
				(*jefe).colorParte1, (*jefe).RotacionParte1,
				(*jefe).OrigenParte, (*jefe).EscalaParte);

		Dibujar_drawSpriteTr(
				(TexturaObjeto*) Lista_RetornaElemento(&(JefeS.texPartesJefe),
						3), (*jefe).PositionParte2, (*jefe).TrozoAnimParte,
				(*jefe).colorParte2, (*jefe).RotacionParte2,
				(*jefe).OrigenParte, (*jefe).EscalaParte);

		Dibujar_drawSpriteTr(
				(TexturaObjeto*) Lista_RetornaElemento(&(JefeS.texPartesJefe),
						3), (*jefe).PositionParte3, (*jefe).TrozoAnimParte,
				(*jefe).colorParte3, (*jefe).RotacionParte3,
				(*jefe).OrigenParte, (*jefe).EscalaParte);

	}
}

//Velocidad animacion martillo
void Jefe_velocidadAnimacionMartillo(Jefe* jefe)
{
	(*jefe).dato2 += GameFramework_getSwapTime();
	if ((*jefe).dato2 >= ((GameFramework_getSwapTime()) * 3))
	{
		(*jefe).actualframeParte = (*jefe).actualframeParte + 1;
		(*jefe).dato2 = 0;
	}

	if ((*jefe).actualframeParte >= 5)
	{
		(*jefe).actualframeParte = 0;
		(*jefe).lineaAnimacionParte++;
		if ((*jefe).lineaAnimacionParte >= 2)
		{
			(*jefe).lineaAnimacionParte = 0;
		}
	}
}

//Velocidad animacion flecha
void Jefe_velocidadAnimacionFlecha(Jefe* jefe)
{
	(*jefe).dato2 += GameFramework_getSwapTime();
	if ((*jefe).dato2 >= ((GameFramework_getSwapTime()) * 3))
	{
		(*jefe).actualframeParte = (*jefe).actualframeParte + 1;
		(*jefe).dato2 = 0;
	}
	if ((*jefe).actualframeParte >= 10)
	{
		(*jefe).actualframeParte = 0;
	}
}

//trozo animacion martillo
void Jefe_defineTrozoAnimMartillo(Jefe* jefe, int x, int y, int width,
		int height)
{
	(*jefe).TrozoAnimParte.X = x;
	(*jefe).TrozoAnimParte.Y = y;
	(*jefe).TrozoAnimParte.Width = width;
	(*jefe).TrozoAnimParte.Height = height;
}

void Jefe_setOrigenMartillo(Jefe* jefe)
{
	(*jefe).OrigenParte.X = 0;
	(*jefe).OrigenParte.Y = (*jefe).TrozoAnimParte.Height / 2;
}

void Jefe_setOrigenFlecha(Jefe *jefe)
{
	(*jefe).OrigenParte.X = (*jefe).TrozoAnimParte.Width / 2;
	(*jefe).OrigenParte.Y = (*jefe).TrozoAnimParte.Height / 2;
}

//define el origen de coordenadas de la animacion
void Jefe_setOrigen(Jefe* jefe)
{
	(*jefe).Origen.X = (*jefe).TrozoAnim.Width / 2;
	(*jefe).Origen.Y = (*jefe).TrozoAnim.Height / 2;
}

//*Definicion de las texturas de los Jefes
void Jefe_setTextura()
{
	Lista_Inicia(&(JefeS.texJefes));
	Lista_AddInFin(&(JefeS.texJefes), Textura_getTextura("JLaxo")); //0
	Lista_AddInFin(&(JefeS.texJefes), Textura_getTextura("JEsp")); //1
	Lista_AddInFin(&(JefeS.texJefes), Textura_getTextura("JQuad")); //2
	Lista_AddInFin(&(JefeS.texJefes), Textura_getTextura("JRued")); //3

	Lista_Inicia(&(JefeS.texPartesJefe));
	Lista_AddInFin(&(JefeS.texPartesJefe), Textura_getTextura("JEspBrazos1")); //0
	Lista_AddInFin(&(JefeS.texPartesJefe), Textura_getTextura("JEspBrazos2")); //1
	Lista_AddInFin(&(JefeS.texPartesJefe), Textura_getTextura("JQuadMartillo")); //2
	Lista_AddInFin(&(JefeS.texPartesJefe), Textura_getTextura("JRuedFlecha")); //3
}

void Jefe_defineCollitionBox()
{
	//COLLITION LAXO
	JefeS.collitionLaxoX[0] = (int) (-34 * 1.1f);
	JefeS.collitionLaxoY[0] = (int) (-168 * 1.1f);
	JefeS.collitionLaxoX[1] = (int) (0 * 1.1f);
	JefeS.collitionLaxoY[1] = (int) (-168 * 1.1f);
	JefeS.collitionLaxoX[2] = (int) (40 * 1.1f);
	JefeS.collitionLaxoY[2] = (int) (-168 * 1.1f);

	//COLLITION ESP
	JefeS.collitionEspX[0] = (int) (-148 * 1.2f);
	JefeS.collitionEspY[0] = (int) (-7 * 1.2f);
	JefeS.collitionEspX[1] = (int) (-103 * 1.2f);
	JefeS.collitionEspY[1] = (int) (-52 * 1.2f);
	JefeS.collitionEspX[2] = (int) (-54 * 1.2f);
	JefeS.collitionEspY[2] = (int) (-101 * 1.2f);
	JefeS.collitionEspX[3] = (int) (1 * 1.2f);
	JefeS.collitionEspY[3] = (int) (-152 * 1.2f);
	JefeS.collitionEspX[4] = (int) (55 * 1.2f);
	JefeS.collitionEspY[4] = (int) (-101 * 1.2f);
	JefeS.collitionEspX[5] = (int) (108 * 1.2f);
	JefeS.collitionEspY[5] = (int) (-52 * 1.2f);
	JefeS.collitionEspX[6] = (int) (146 * 1.2f);
	JefeS.collitionEspY[6] = (int) (-7 * 1.2f);
	//Brazo1
	JefeS.collitionBrazoEspX[0] = (int) (30 * 1.1f);
	JefeS.collitionBrazoEspY[0] = (int) (6 * 1.1f);
	JefeS.collitionBrazoEspX[1] = (int) (80 * 1.1f);
	JefeS.collitionBrazoEspY[1] = (int) (6 * 1.1f);
	JefeS.collitionBrazoEspX[2] = (int) (130 * 1.1f);
	JefeS.collitionBrazoEspY[2] = (int) (6 * 1.1f);
	JefeS.collitionBrazoEspX[3] = (int) (180 * 1.1f);
	JefeS.collitionBrazoEspY[3] = (int) (6 * 1.1f);
	JefeS.collitionBrazoEspX[4] = (int) (230 * 1.1f);
	JefeS.collitionBrazoEspY[4] = (int) (6 * 1.1f);
	JefeS.collitionBrazoEspX[5] = (int) (280 * 1.1f);
	JefeS.collitionBrazoEspY[5] = (int) (6 * 1.1f);
	JefeS.collitionBrazoEspX[6] = (int) (330 * 1.1f);
	JefeS.collitionBrazoEspY[6] = (int) (6 * 1.1f);
	JefeS.collitionBrazoEspX[7] = (int) (380 * 1.1f);
	JefeS.collitionBrazoEspY[7] = (int) (6 * 1.1f);
	JefeS.collitionBrazoEspX[8] = (int) (430 * 1.1f);
	JefeS.collitionBrazoEspY[8] = (int) (6 * 1.1f);
	//Brazo2
	JefeS.collitionBrazoEspX[9] = (int) (430 * 1.1f);
	JefeS.collitionBrazoEspY[9] = (int) (6 * 1.1f);
	JefeS.collitionBrazoEspX[10] = (int) (380 * 1.1f);
	JefeS.collitionBrazoEspY[10] = (int) (6 * 1.1f);
	JefeS.collitionBrazoEspX[11] = (int) (330 * 1.1f);
	JefeS.collitionBrazoEspY[11] = (int) (6 * 1.1f);
	JefeS.collitionBrazoEspX[12] = (int) (280 * 1.1f);
	JefeS.collitionBrazoEspY[12] = (int) (6 * 1.1f);
	JefeS.collitionBrazoEspX[13] = (int) (230 * 1.1f);
	JefeS.collitionBrazoEspY[13] = (int) (6 * 1.1f);
	JefeS.collitionBrazoEspX[14] = (int) (180 * 1.1f);
	JefeS.collitionBrazoEspY[14] = (int) (6 * 1.1f);
	JefeS.collitionBrazoEspX[15] = (int) (130 * 1.1f);
	JefeS.collitionBrazoEspY[15] = (int) (6 * 1.1f);
	JefeS.collitionBrazoEspX[16] = (int) (80 * 1.1f);
	JefeS.collitionBrazoEspY[16] = (int) (6 * 1.1f);
	JefeS.collitionBrazoEspX[17] = (int) (30 * 1.1f);
	JefeS.collitionBrazoEspY[17] = (int) (6 * 1.1f);

	//COLLITION QUAD
	JefeS.collitionQuadX[0] = (int) (-150 * 1.1f);
	JefeS.collitionQuadY[0] = (int) (-120 * 1.1f);
	JefeS.collitionQuadX[1] = (int) (-49 * 1.1f);
	JefeS.collitionQuadY[1] = (int) (-120 * 1.1f);
	JefeS.collitionQuadX[2] = (int) (51 * 1.1f);
	JefeS.collitionQuadY[2] = (int) (-120 * 1.1f);
	JefeS.collitionQuadX[3] = (int) (151 * 1.1f);
	JefeS.collitionQuadY[3] = (int) (-120 * 1.1f);
	//Martillo
	JefeS.collitionMartilloQuadX[0] = (int) (32 * 1.2f);
	JefeS.collitionMartilloQuadY[0] = (int) (0 * 1.2f);
	JefeS.collitionMartilloQuadX[1] = (int) (109 * 1.2f);
	JefeS.collitionMartilloQuadY[1] = (int) (0 * 1.2f);
	JefeS.collitionMartilloQuadX[2] = (int) (182 * 1.2f);
	JefeS.collitionMartilloQuadY[2] = (int) (0 * 1.2f);
	JefeS.collitionMartilloQuadX[3] = (int) (250 * 1.2f);
	JefeS.collitionMartilloQuadY[3] = (int) (0 * 1.2f);
}

// Confirma colisiones Laxo
void Jefe_confirmaColisionesLaxo(Jefe* jefe)
{
	JefeS.rotacionActual = (*jefe).RotacionParte1;

	if (CollitionJefe_colisionDisparoALaxo(&((*jefe).collitionJefe), jefe,
			(*jefe).tipoJefe) == 1 && (*jefe).tipoJefe == JLaxo)
	{
		(*jefe).estaEnColisionDisp = 1;
		if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1
				&& (*jefe).lineaAnimacion >= 2)
		{
			if ((*CollitionJefe_getSObject()).ultimaColisionTipoDisparo
					!= Dsuper)
			{
				(*jefe).vida = (*jefe).vida - 5;
			}
			else
			{
				(*jefe).vida = (*jefe).vida - 10;
			}
		}
	}

	if (CollitionJefe_colisionNaveALaxo(&((*jefe).collitionJefe), jefe) == 1)
	{
		if ((*Nave_getSObject()).permiteColisionesJefe == 1)
		{
			(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
					- (30 + JefeS.adicionDanoNave);
			(*Nave_getSObject()).permiteColisionesJefe = 0;
		}
	}

	switch ((*Nave_getSObject()).TexturaNave)
	{
	case 0:
		for (JefeS.i = 0; JefeS.i < 4; JefeS.i++)
		{
			if (CollitionJefe_colisionPoderGalacticaALaxo(
					&((*jefe).collitionJefe), jefe, (*jefe).tipoJefe) == 1)
			{
				(*jefe).estaEnColisionPoder = 1;
				if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1
						&& (*jefe).lineaAnimacion >= 2)
				{
					(*jefe).vida = (*jefe).vida - 50;
				}
			}
		}
		break;
	case 1:
		for (JefeS.i = 0; JefeS.i < 5; JefeS.i++)
		{
			if (CollitionJefe_colisionPoderCarafeALaxo(&((*jefe).collitionJefe),
					jefe, (*jefe).tipoJefe) == 1)
			{
				(*jefe).estaEnColisionPoder = 1;
				if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1
						&& (*jefe).lineaAnimacion >= 2)
				{
					(*jefe).vida = (*jefe).vida - 30;
				}
			}
		}
		break;
	case 2:
		if (CollitionJefe_colisionPoderHelixALaxo(&((*jefe).collitionJefe),
				jefe, (*jefe).tipoJefe) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
			if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1
					&& (*jefe).lineaAnimacion >= 2)
			{
				(*jefe).vida = (*jefe).vida - 1500;
			}
		}
		break;
	case 3:
		if (CollitionJefe_colisionPoderArpALaxo(&((*jefe).collitionJefe), jefe,
				(*jefe).tipoJefe))
		{
			(*jefe).estaEnColisionPoder = 1;
			if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1
					&& (*jefe).lineaAnimacion >= 2)
			{
				(*jefe).vida = (*jefe).vida - 8000;
			}
		}
		break;
	case 4:
		if (CollitionJefe_colisionPoderPerseusALaxo(&((*jefe).collitionJefe),
				jefe, (*jefe).tipoJefe))
		{
			(*jefe).estaEnColisionPoder = 1;
			if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1
					&& (*jefe).lineaAnimacion >= 2)
			{
				(*jefe).vida = (*jefe).vida - GameFramework_Rand(1000, 5000);
			}
		}
		break;
	case 5:
		if (CollitionJefe_colisionPoderPiscesALaxo(&((*jefe).collitionJefe),
				jefe, (*jefe).tipoJefe))
		{
			(*jefe).estaEnColisionPoder = 1;
			if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1
					&& (*jefe).lineaAnimacion >= 2)
			{
				(*jefe).vida = (*jefe).vida - 80;
			}
		}
		break;
	}

}

//Confirma colisiones Esp
void Jefe_confirmaColisionesEsp(Jefe* jefe)
{
	if (CollitionJefe_colisionDisparoAEsp(&((*jefe).collitionJefe), jefe,
			(*jefe).tipoJefe) == 1)
	{
		(*jefe).estaEnColisionDisp = 1;
		if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1)
		{
			if ((*CollitionJefe_getSObject()).ultimaColisionTipoDisparo
					!= Dsuper)
			{
				(*jefe).vida = (*jefe).vida - 5;
			}
			else
			{
				(*jefe).vida = (*jefe).vida - 10;
			}
		}
	}

	if (CollitionJefe_colisionDisparoABrazo1Esp(&((*jefe).collitionJefe),
			(*jefe).PositionParte1, (*jefe).RotacionParte1) == 1)
	{
		(*jefe).estaEnColisionDisp = 1;
	}

	if (CollitionJefe_colisionDisparoABrazo2Esp(&((*jefe).collitionJefe),
			(*jefe).PositionParte2, (*jefe).RotacionParte2) == 1)
	{
		(*jefe).estaEnColisionDisp = 1;
	}

	if (CollitionJefe_colisionNaveAEsp(&((*jefe).collitionJefe), jefe) == 1)
	{
		if ((*Nave_getSObject()).permiteColisionesJefe)
		{
			(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
					- (30 + JefeS.adicionDanoNave);
			(*Nave_getSObject()).permiteColisionesJefe = 0;
		}
	}
	if (CollitionJefe_colisionNaveABrazo1Esp(&((*jefe).collitionJefe),
			(*jefe).PositionParte1, (*jefe).RotacionParte1) == 1)
	{
		if ((*Nave_getSObject()).permiteColisionesJefe)
		{
			(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
					- (25 + JefeS.adicionDanoNave);
			(*Nave_getSObject()).permiteColisionesJefe = 0;
		}
	}
	if (CollitionJefe_colisionNaveABrazo2Esp(&((*jefe).collitionJefe),
			(*jefe).PositionParte2, (*jefe).RotacionParte2) == 1)
	{
		if ((*Nave_getSObject()).permiteColisionesJefe)
		{
			(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
					- (25 + JefeS.adicionDanoNave);
			(*Nave_getSObject()).permiteColisionesJefe = 0;
		}
	}

	switch ((*Nave_getSObject()).TexturaNave)
	{
	case 0:
		for (JefeS.i = 0; JefeS.i < 4; JefeS.i++)
		{
			if (CollitionJefe_colisionPGalacticaAEsp(&((*jefe).collitionJefe),
					jefe, (*jefe).tipoJefe) == 1)
			{
				(*jefe).estaEnColisionPoder = 1;
				if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1)
				{
					(*jefe).vida = (*jefe).vida - 50;
				}
			}
			if (CollitionJefe_colisionPGalacticaABrazo1(
					&((*jefe).collitionJefe), (*jefe).PositionParte1,
					(*jefe).RotacionParte1) == 1)
			{
				(*jefe).estaEnColisionPoder = 1;
			}
			if (CollitionJefe_colisionPGalacticaABrazo2(
					&((*jefe).collitionJefe), (*jefe).PositionParte2,
					(*jefe).RotacionParte2) == 1)
			{
				(*jefe).estaEnColisionPoder = 1;
			}
		}
		break;
	case 1:
		for (JefeS.i = 0; JefeS.i < 5; JefeS.i++)
		{
			if (CollitionJefe_colisionPCarafeAEsp(&((*jefe).collitionJefe),
					jefe, (*jefe).tipoJefe) == 1)
			{
				(*jefe).estaEnColisionPoder = 1;
				if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1)
				{
					(*jefe).vida = (*jefe).vida - 30;
				}
			}
			if (CollitionJefe_colisionPCarafeABrazo1(&((*jefe).collitionJefe),
					(*jefe).PositionParte1, (*jefe).RotacionParte1) == 1)
			{
				(*jefe).estaEnColisionPoder = 1;
			}
			if (CollitionJefe_colisionPCarafeABrazo2(&((*jefe).collitionJefe),
					(*jefe).PositionParte2, (*jefe).RotacionParte2) == 1)
			{
				(*jefe).estaEnColisionPoder = 1;
			}
		}
		break;
	case 2:
		if (CollitionJefe_colisionPHelixAEsp(&((*jefe).collitionJefe), jefe,
				(*jefe).tipoJefe) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
			if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1)
			{
				(*jefe).vida = (*jefe).vida - 1500;
			}
		}
		if (CollitionJefe_colisionPHelixABrazo1(&((*jefe).collitionJefe),
				(*jefe).PositionParte1, (*jefe).RotacionParte1) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
		}
		if (CollitionJefe_colisionPHelixABrazo2(&((*jefe).collitionJefe),
				(*jefe).PositionParte2, (*jefe).RotacionParte2) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
		}
		break;
	case 3:
		if (CollitionJefe_colisionPArpAEsp(&((*jefe).collitionJefe), jefe,
				(*jefe).tipoJefe) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
			if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1)
			{
				(*jefe).vida = (*jefe).vida - 8000;
			}
		}
		break;
	case 4:
		if (CollitionJefe_colisionPPerseusAEsp(&((*jefe).collitionJefe), jefe,
				(*jefe).tipoJefe) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
			if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1)
			{
				(*jefe).vida = (*jefe).vida - GameFramework_Rand(1000, 5000);
			}
		}
		break;
	case 5:
		if (CollitionJefe_colisionPPiscesAEsp(&((*jefe).collitionJefe), jefe,
				(*jefe).tipoJefe) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
			if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1)
			{
				(*jefe).vida = (*jefe).vida - 80;
			}
		}
		if (CollitionJefe_colisionPPiscesABrazo1(&((*jefe).collitionJefe),
				(*jefe).PositionParte1, (*jefe).RotacionParte1) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
		}
		if (CollitionJefe_colisionPPiscesABrazo2(&((*jefe).collitionJefe),
				(*jefe).PositionParte2, (*jefe).RotacionParte2) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
		}
		break;
	}

}

// Confirma colisiones Quad
void Jefe_confirmaColisionesQuad(Jefe* jefe)
{
	if (CollitionJefe_colisionDisparoAQuad((&(*jefe).collitionJefe), jefe,
			(*jefe).tipoJefe) == 1)
	{
		(*jefe).estaEnColisionDisp = 1;
	}

	if (CollitionJefe_colisionNaveAQuad((&(*jefe).collitionJefe), jefe) == 1)
	{
		if ((*Nave_getSObject()).permiteColisionesJefe)
		{
			(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
					- (30 + JefeS.adicionDanoNave);
			(*Nave_getSObject()).permiteColisionesJefe = 0;
		}
	}

	if (CollitionJefe_colisionNaveAMartilloQuad((&(*jefe).collitionJefe),
			(*jefe).PositionParte1) == 1
			|| CollitionJefe_colisionNaveAMartilloQuad((&(*jefe).collitionJefe),
					(*jefe).PositionParte2) == 1
			|| CollitionJefe_colisionNaveAMartilloQuad((&(*jefe).collitionJefe),
					(*jefe).PositionParte3) == 1)
	{
		if ((*Nave_getSObject()).permiteColisionesJefe)
		{
			(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
					- (25 + JefeS.adicionDanoNave);
			(*Nave_getSObject()).permiteColisionesJefe = 0;
		}
	}

	if (CollitionJefe_colisionDisparoAMartilloQuad((&(*jefe).collitionJefe),
			(*jefe).PositionParte1) == 1
			|| CollitionJefe_colisionDisparoAMartilloQuad(
					(&(*jefe).collitionJefe), (*jefe).PositionParte2) == 1
			|| CollitionJefe_colisionDisparoAMartilloQuad(
					(&(*jefe).collitionJefe), (*jefe).PositionParte3) == 1)
	{
		(*jefe).estaEnColisionDisp = 1;
		if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1)
		{
			if ((*CollitionJefe_getSObject()).ultimaColisionTipoDisparo
					!= Dsuper)
			{
				(*jefe).vida = (*jefe).vida - 5;
			}
			else
			{
				(*jefe).vida = (*jefe).vida - 10;
			}
		}
	}

	if (CollitionJefe_colisionPGalacticaAQuad((&(*jefe).collitionJefe), jefe,
			(*jefe).tipoJefe) == 1)
	{
		(*jefe).estaEnColisionPoder = 1;
	}
	if (CollitionJefe_colisionPGalacticaAMartilloQuad((&(*jefe).collitionJefe),
			(*jefe).PositionParte1) == 1
			|| CollitionJefe_colisionPGalacticaAMartilloQuad(
					(&(*jefe).collitionJefe), (*jefe).PositionParte2) == 1
			|| CollitionJefe_colisionPGalacticaAMartilloQuad(
					(&(*jefe).collitionJefe), (*jefe).PositionParte3) == 1)
	{
		(*jefe).estaEnColisionPoder = 1;
		if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1)
		{
			(*jefe).vida = (*jefe).vida - 50;
		}
	}

	switch ((*Nave_getSObject()).TexturaNave)
	{
	case 0:
		for (JefeS.i = 0; JefeS.i < 4; JefeS.i++)
		{
			if (CollitionJefe_colisionPGalacticaAQuad((&(*jefe).collitionJefe),
					jefe, (*jefe).tipoJefe))
			{
				(*jefe).estaEnColisionPoder = 1;
			}
			if (CollitionJefe_colisionPGalacticaAMartilloQuad(
					(&(*jefe).collitionJefe), (*jefe).PositionParte1) == 1
					|| CollitionJefe_colisionPGalacticaAMartilloQuad(
							(&(*jefe).collitionJefe), (*jefe).PositionParte2)
							== 1
					|| CollitionJefe_colisionPGalacticaAMartilloQuad(
							(&(*jefe).collitionJefe), (*jefe).PositionParte3)
							== 1)
			{
				(*jefe).estaEnColisionPoder = 1;
				if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1)
				{
					(*jefe).vida = (*jefe).vida - 50;
				}
			}
		}
		break;
	case 1:
		for (JefeS.i = 0; JefeS.i < 5; JefeS.i++)
		{
			if (CollitionJefe_colisionPCarafeAQuad((&(*jefe).collitionJefe),
					jefe, (*jefe).tipoJefe) == 1)
			{
				(*jefe).estaEnColisionPoder = 1;
			}
			if (CollitionJefe_colisionPCarafeAMartilloQuad(
					(&(*jefe).collitionJefe), (*jefe).PositionParte1) == 1
					|| CollitionJefe_colisionPCarafeAMartilloQuad(
							(&(*jefe).collitionJefe), (*jefe).PositionParte2)
							== 1
					|| CollitionJefe_colisionPCarafeAMartilloQuad(
							(&(*jefe).collitionJefe), (*jefe).PositionParte3)
							== 1)
			{
				(*jefe).estaEnColisionPoder = 1;
				if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1)
				{
					(*jefe).vida = (*jefe).vida - 30;
				}
			}
		}
		break;
	case 2:
		if (CollitionJefe_colisionPHelixAQuad((&(*jefe).collitionJefe), jefe,
				(*jefe).tipoJefe) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
		}
		if (CollitionJefe_colisionPHelixAMartilloQuad((&(*jefe).collitionJefe),
				(*jefe).PositionParte1) == 1
				|| CollitionJefe_colisionPHelixAMartilloQuad(
						(&(*jefe).collitionJefe), (*jefe).PositionParte2) == 1
				|| CollitionJefe_colisionPHelixAMartilloQuad(
						(&(*jefe).collitionJefe), (*jefe).PositionParte3) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
			if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1)
			{
				(*jefe).vida = (*jefe).vida - 1500;
			}
		}
		break;
	case 3:
		if (CollitionJefe_colisionPArpAMartilloQuad((&(*jefe).collitionJefe),
				(*jefe).PositionParte1) == 1
				|| CollitionJefe_colisionPArpAMartilloQuad(
						(&(*jefe).collitionJefe), (*jefe).PositionParte2) == 1
				|| CollitionJefe_colisionPArpAMartilloQuad(
						(&(*jefe).collitionJefe), (*jefe).PositionParte3) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
			if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1)
			{
				(*jefe).vida = (*jefe).vida - 8000;
			}
		}
		break;
	case 4:
		if (CollitionJefe_colisionPPerseusAMartilloQuad(
				(&(*jefe).collitionJefe), (*jefe).PositionParte1) == 1
				|| CollitionJefe_colisionPPerseusAMartilloQuad(
						(&(*jefe).collitionJefe), (*jefe).PositionParte2) == 1
				|| CollitionJefe_colisionPPerseusAMartilloQuad(
						(&(*jefe).collitionJefe), (*jefe).PositionParte3) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
			if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1)
			{
				(*jefe).vida = (*jefe).vida - GameFramework_Rand(1000, 5000);
			}
		}
		break;
	case 5:
		if (CollitionJefe_colisionPPiscesAQuad((&(*jefe).collitionJefe), jefe,
				(*jefe).tipoJefe) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
		}
		if (CollitionJefe_colisionPPiscesAMartilloQuad((&(*jefe).collitionJefe),
				(*jefe).PositionParte1) == 1
				|| CollitionJefe_colisionPPiscesAMartilloQuad(
						(&(*jefe).collitionJefe), (*jefe).PositionParte2) == 1
				|| CollitionJefe_colisionPPiscesAMartilloQuad(
						(&(*jefe).collitionJefe), (*jefe).PositionParte3) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
			if ((*CollitionJefe_getSObject()).ultimoTipoColision == 1)
			{
				(*jefe).vida = (*jefe).vida - 80;
			}
		}
		break;
	}

}

// Confirma colisiones Rued
void Jefe_confirmaColisionesRued(Jefe* jefe)
{
	if (CollitionJefe_colisionDisparoARued(&((*jefe).collitionJefe), jefe,
			(*jefe).tipoJefe) == 1)
	{
		(*jefe).estaEnColisionDisp = 1;
	}

	if (CollitionJefe_colisionNaveAFlechaRued(&((*jefe).collitionJefe),
			(*jefe).PositionParte1) == 1
			|| CollitionJefe_colisionNaveAFlechaRued(&((*jefe).collitionJefe),
					(*jefe).PositionParte2) == 1
			|| CollitionJefe_colisionNaveAFlechaRued(&((*jefe).collitionJefe),
					(*jefe).PositionParte3) == 1)
	{
		if ((*Nave_getSObject()).permiteColisionesJefe)
		{
			(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
					- (25 + JefeS.adicionDanoNave);
			(*Nave_getSObject()).permiteColisionesJefe = 0;
		}
	}

	if (CollitionJefe_colisionNaveARued(&((*jefe).collitionJefe), jefe) == 1)
	{
		if ((*Nave_getSObject()).permiteColisionesJefe)
		{
			(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
					- (30 + JefeS.adicionDanoNave);
			(*Nave_getSObject()).permiteColisionesJefe = 0;
		}
	}

	if (CollitionJefe_colisionDisparoAFlechaRued(&((*jefe).collitionJefe),
			(*jefe).PositionParte1) == 1
			|| CollitionJefe_colisionDisparoAFlechaRued(
					&((*jefe).collitionJefe), (*jefe).PositionParte2) == 1
			|| CollitionJefe_colisionDisparoAFlechaRued(
					&((*jefe).collitionJefe), (*jefe).PositionParte3) == 1)
	{
		(*jefe).estaEnColisionDisp = 1;
		if ((*CollitionJefe_getSObject()).ultimaColisionTipoDisparo != Dsuper)
		{
			(*jefe).vida = (*jefe).vida - 5;
		}
		else
		{
			(*jefe).vida = (*jefe).vida - 10;
		}
	}

	if (CollitionJefe_colisionPGalacticaARued(&((*jefe).collitionJefe), jefe,
			(*jefe).tipoJefe) == 1)
	{
		(*jefe).estaEnColisionPoder = 1;
	}
	if (CollitionJefe_colisionPGalacticaAFlechaRued(&((*jefe).collitionJefe),
			(*jefe).PositionParte1) == 1
			|| CollitionJefe_colisionPGalacticaAFlechaRued(
					&((*jefe).collitionJefe), (*jefe).PositionParte2) == 1
			|| CollitionJefe_colisionPGalacticaAFlechaRued(
					&((*jefe).collitionJefe), (*jefe).PositionParte3) == 1)
	{
		(*jefe).estaEnColisionPoder = 1;
		(*jefe).vida = (*jefe).vida - 50;
	}

	switch ((*Nave_getSObject()).TexturaNave)
	{
	case 0:
		for (JefeS.i = 0; JefeS.i < 4; JefeS.i++)
		{
			if (CollitionJefe_colisionPGalacticaARued(&((*jefe).collitionJefe),
					jefe, (*jefe).tipoJefe))
			{
				(*jefe).estaEnColisionPoder = 1;
			}
			if (CollitionJefe_colisionPGalacticaAFlechaRued(
					&((*jefe).collitionJefe), (*jefe).PositionParte1) == 1
					|| CollitionJefe_colisionPGalacticaAFlechaRued(
							&((*jefe).collitionJefe), (*jefe).PositionParte2)
							== 1
					|| CollitionJefe_colisionPGalacticaAFlechaRued(
							&((*jefe).collitionJefe), (*jefe).PositionParte3)
							== 1)
			{
				(*jefe).estaEnColisionPoder = 1;
				(*jefe).vida = (*jefe).vida - 50;
			}
		}
		break;
	case 1:
		for (JefeS.i = 0; JefeS.i < 5; JefeS.i++)
		{
			if (CollitionJefe_colisionPCarafeARued(&((*jefe).collitionJefe),
					jefe, (*jefe).tipoJefe) == 1)
			{
				(*jefe).estaEnColisionPoder = 1;
			}
			if (CollitionJefe_colisionPCarafeAFlechaRued(
					&((*jefe).collitionJefe), (*jefe).PositionParte1) == 1
					|| CollitionJefe_colisionPCarafeAFlechaRued(
							&((*jefe).collitionJefe), (*jefe).PositionParte2)
							== 1
					|| CollitionJefe_colisionPCarafeAFlechaRued(
							&((*jefe).collitionJefe), (*jefe).PositionParte3)
							== 1)
			{
				(*jefe).estaEnColisionPoder = 1;
				(*jefe).vida = (*jefe).vida - 30;
			}
		}
		break;
	case 2:
		if (CollitionJefe_colisionPHelixARued(&((*jefe).collitionJefe), jefe,
				(*jefe).tipoJefe) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
		}
		if (CollitionJefe_colisionPHelixAFlechaRued(&((*jefe).collitionJefe),
				(*jefe).PositionParte1) == 1
				|| CollitionJefe_colisionPHelixAFlechaRued(
						&((*jefe).collitionJefe), (*jefe).PositionParte2) == 1
				|| CollitionJefe_colisionPHelixAFlechaRued(
						&((*jefe).collitionJefe), (*jefe).PositionParte3) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
			(*jefe).vida = (*jefe).vida - 1500;
		}
		break;
	case 3:
		if (CollitionJefe_colisionPArpAFlechaRued(&((*jefe).collitionJefe),
				(*jefe).PositionParte1) == 1
				|| CollitionJefe_colisionPArpAFlechaRued(
						&((*jefe).collitionJefe), (*jefe).PositionParte2) == 1
				|| CollitionJefe_colisionPArpAFlechaRued(
						&((*jefe).collitionJefe), (*jefe).PositionParte3) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
			(*jefe).vida = (*jefe).vida - 8000;
		}
		break;
	case 4:
		if (CollitionJefe_colisionPPerseusAFlechaRued(&((*jefe).collitionJefe),
				(*jefe).PositionParte1) == 1
				|| CollitionJefe_colisionPPerseusAFlechaRued(
						&((*jefe).collitionJefe), (*jefe).PositionParte2) == 1
				|| CollitionJefe_colisionPPerseusAFlechaRued(
						&((*jefe).collitionJefe), (*jefe).PositionParte3) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
			(*jefe).vida = (*jefe).vida - GameFramework_Rand(1000, 5000);
		}
		break;
	case 5:
		if (CollitionJefe_colisionPPiscesARued(&((*jefe).collitionJefe), jefe,
				(*jefe).tipoJefe) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
		}
		if (CollitionJefe_colisionPPiscesAFlechaRued(&((*jefe).collitionJefe),
				(*jefe).PositionParte1) == 1
				|| CollitionJefe_colisionPPiscesAFlechaRued(
						&((*jefe).collitionJefe), (*jefe).PositionParte2) == 1
				|| CollitionJefe_colisionPPiscesAFlechaRued(
						&((*jefe).collitionJefe), (*jefe).PositionParte3) == 1)
		{
			(*jefe).estaEnColisionPoder = 1;
			(*jefe).vida = (*jefe).vida - 80;
		}
		break;
	}

}

Jefe* Jefe_getSObject()
{
	return &JefeS;
}

