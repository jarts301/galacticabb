/*
 * Ayuda.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "Ayuda.h"
#include "Animation.h"
#include "Textura.h"
#include "Lista.h"
#include "GameComun.h"
#include "Dibujar.h"
#include "Boton.h"
#include <stdlib.h>

static Ayuda AyudaS;

void Ayuda_load()
{
	Ayuda_setTexturas();
	Ayuda_loadTitulo();
	Lista_Inicia(&(AyudaS.botones));
	Lista_InsertaEnFinal(&(AyudaS.botones), AyudaS.botones.fin,
			Boton_Crear(BBack, 10, 10));

}

void Ayuda_update()
{
	GameComun_updateEstrellas();
	for (AyudaS.i = 0; AyudaS.i < AyudaS.botones.size; AyudaS.i++)
	{
		Boton_update(
				(Boton*) Lista_RetornaElemento(&(AyudaS.botones), AyudaS.i));
	}
}

void Ayuda_draw()
{
	//CLEAR NEGRO
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_RetornaElemento(&(AyudaS.textura), 1),
			GameFramework_Vector2(0, 0), GameFramework_Color(0, 0, 0, 1.0),
			0.0f, GameFramework_Vector2(0, 0), GameFramework_Vector2(100, 100));

	GameComun_drawEstrellas();
	Ayuda_drawTitulo();
	for (AyudaS.i = 0; AyudaS.i < AyudaS.botones.size; AyudaS.i++)
	{
		Boton_draw((Boton*) Lista_RetornaElemento(&(AyudaS.botones), AyudaS.i));
	}
}
//******************************************

//******TITULO***********
void Ayuda_loadTitulo()
{
	AyudaS.positionTitulo.X = 0;
	AyudaS.positionTitulo.Y = 0;
	AyudaS.origenTitulo.X = 0;
	AyudaS.origenTitulo.Y = 0;
	AyudaS.escalaTitulo.X = 1.17;
	AyudaS.escalaTitulo.Y = 1;
}

void Ayuda_drawTitulo()
{
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_RetornaElemento(&(AyudaS.textura), 0),
			AyudaS.positionTitulo, GameFramework_Color(1, 1, 1, 1), 0.0f,
			AyudaS.origenTitulo, AyudaS.escalaTitulo);
	/*sb.Draw(textura[0], Vector2.Zero, null, Color.White, 0.0f, origenTitulo,
	 1.0f, SpriteEffects.None, 0.5f);*/
}

//*********Texturas**************
void Ayuda_setTexturas()
{
	Lista_Inicia(&(AyudaS.textura));
	Lista_InsertaEnFinal(&(AyudaS.textura), AyudaS.textura.fin,
			Textura_getTextura("Help")); //0
	Lista_InsertaEnFinal(&(AyudaS.textura), AyudaS.textura.fin,
			Textura_getTexturaFondo(0)); //1
}

