/*
 * Audio.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef AUDIO_H_
#define AUDIO_H_

#include "GalacticaTipos.h"

#define SUCCESS  0
#define FAILURE -1
#define MSG_SIZE 1024

// *******************

void Audio_Inicia();
int Audio_addAudio(char* dir, char* nombre, int repetir);
Musica* Audio_getAudio(char* nombre);
void Audio_playAudio(char* nombre);
Lista* Audio_getListaMusica();
Lista* Audio_getListaNombreMusica();
Audio* Audio_getSObject();
void Audio_stopAudio(char* nombre);

#endif /* AUDIO_H_ */
