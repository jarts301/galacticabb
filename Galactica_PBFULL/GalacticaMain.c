/*
 * GalacticaMain.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "GalacticaMain.h"
#include "GameFramework.h"
#include "Textura.h"
#include "String.h"
#include "bbutil.h"
#include "Dibujar.h"
#include "AcercaDe.h"
#include "Cadena.h"
#include "Poderes.h"
#include "Animation.h"
#include "BarraVida.h"
#include "Boton.h"
#include "Onda.h"
#include "Disparo.h"
#include "DisparoEnemigo.h"
#include "DisparoJefe.h"
#include "Enemigo.h"
#include "Plus.h"
#include "Estrella.h"
#include "GameComun.h"
#include "MainMenu.h"
#include "Nave.h"
#include "Jefe.h"
#include "QuickMode.h"
#include "Acelerometro.h"
#include "Almacenamiento.h"
#include "Audio.h"
#include "Ayuda.h"
#include "Buy.h"
#include "Control.h"
#include "GalacticaTipos.h"
#include "InGameBase.h"
#include "Lista.h"
#include "Mision1.h"
#include "Mision10.h"
#include "Mision11.h"
#include "Mision12.h"
#include "Mision13.h"
#include "Mision14.h"
#include "Mision15.h"
#include "Mision16.h"
#include "Mision17.h"
#include "Mision18.h"
#include "Mision19.h"
#include "Mision2.h"
#include "Mision20.h"
#include "Mision3.h"
#include "Mision4.h"
#include "Mision5.h"
#include "Mision6.h"
#include "Mision7.h"
#include "Mision8.h"
#include "Mision9.h"
#include "MisionMode.h"
#include "MisionModeGame.h"
#include "MisionModeGOver.h"
#include "MisionModeInfo.h"
#include "MisionModeMenu.h"
#include "MisionModePause.h"
#include "NMini.h"
#include "Opciones.h"
#include "QuickModeGOver.h"
#include "QuickModeGame.h"
#include "QuickModeMenu.h"
#include "QuickModePause.h"
#include "SavedGame.h"
#include "TouchControl.h"

#include <GLES/gl.h>

static GalacticaMain GalacticaMainS;

void GalacticaMain_Load()
{
	GalacticaMainS.BESTSCORE = 0;
	GalacticaMainS.checkTrialModeDelay = 1000000;
	GalacticaMainS.introTime = 4000000;
	GalacticaMainS.dibujaIntro = 1, GalacticaMainS.empezarCarga = 1;
	GalacticaMainS.enCarga = 0, GalacticaMainS.enJuego = 0;
	GalacticaMainS.isTrialMode = 1; //static
	glClearColor(0.0f, 0.0f, 0.2f, 1.0f);
	Textura_cargaIntro();
}

void GalacticaMain_cargarTodo()
{
	GalacticaMainS.estadoActual = GMMainMenu;
	GalacticaMain_cargarTexturas();
	Dibujar_drawErrorText("Texturas Cargadas", GameFramework_Vector2(10, 200));
	GalacticaMain_cargarAudio();
	Dibujar_drawErrorText("Audios Cargadas", GameFramework_Vector2(10, 200));

	//Lo que debe cargar
	Acelerometro_load();
	Dibujar_drawErrorText("Acelerometro Cargado",
			GameFramework_Vector2(10, 200));
	Poderes_load();
	Dibujar_drawErrorText("Poderes Cargado", GameFramework_Vector2(10, 200));
	Animation_load();
	Dibujar_drawErrorText("Animacion Cargado", GameFramework_Vector2(10, 200));
	BarraVida_load();
	Dibujar_drawErrorText("Barra vida Cargado", GameFramework_Vector2(10, 200));
	Boton_load();
	Dibujar_drawErrorText("Boton Cargado", GameFramework_Vector2(10, 200));
	Onda_load();
	Dibujar_drawErrorText("OndaCargado", GameFramework_Vector2(10, 200));
	Disparo_load();
	Dibujar_drawErrorText("Disparo Cargado", GameFramework_Vector2(10, 200));
	DisparoEnemigo_load();
	Dibujar_drawErrorText("DisparoEnemigo Cargado",
			GameFramework_Vector2(10, 200));
	Enemigo_load();
	Dibujar_drawErrorText("Enemigo Cargado", GameFramework_Vector2(10, 200));
	Plus_load();
	Dibujar_drawErrorText("Plus Cargado", GameFramework_Vector2(10, 200));
	Estrella_load();
	Dibujar_drawErrorText("Estrella Cargado", GameFramework_Vector2(10, 200));
	GameComun_loadEstrellas();
	Dibujar_drawErrorText("GameComun Cargado", GameFramework_Vector2(10, 200));
	MainMenu_load();
	Dibujar_drawErrorText("MainMenu Cargado", GameFramework_Vector2(10, 200));
	QuickMode_load();
	Dibujar_drawErrorText("QuickMode Cargado", GameFramework_Vector2(10, 200));
	Nave_load();
	Dibujar_drawErrorText("Nave Cargado", GameFramework_Vector2(10, 200));
	DisparoJefe_load();
	Dibujar_drawErrorText("DisparoJefe Cargado",
			GameFramework_Vector2(10, 200));
	Jefe_load();
	Dibujar_drawErrorText("Jefe Cargado", GameFramework_Vector2(10, 200));
	QuickModeGame_load();
	Dibujar_drawErrorText("QuickModeGame Cargado",
			GameFramework_Vector2(10, 200));
	QuickModePause_load();
	Dibujar_drawErrorText("QuickModePause Cargado",
			GameFramework_Vector2(10, 200));
	NMini_load();
	Dibujar_drawErrorText("NMini Cargado", GameFramework_Vector2(10, 200));
	QuickModeMenu_load();
	Dibujar_drawErrorText("QuickModeMenu Cargado",
			GameFramework_Vector2(10, 200));
	QuickModeGOver_load();
	Dibujar_drawErrorText("QuickModeGOver Cargado",
			GameFramework_Vector2(10, 200));
	MisionModeGame_load();
	Dibujar_drawErrorText("QuickModeGame Cargado",
			GameFramework_Vector2(10, 200));
	MisionModeMenu_load();
	Dibujar_drawErrorText("MisionModeMenu Cargado",
			GameFramework_Vector2(10, 200));
	MisionModeInfo_load();
	Dibujar_drawErrorText("MisionModeInfo Cargado",
			GameFramework_Vector2(10, 200));
	MisionModePause_load();
	Dibujar_drawErrorText("MisionModePause Cargado",
			GameFramework_Vector2(10, 200));
	MisionModeGOver_load();
	Dibujar_drawErrorText("MisionModeGOver Cargado",
			GameFramework_Vector2(10, 200));
	Control_load();
	Dibujar_drawErrorText("Control Cargado", GameFramework_Vector2(10, 200));
	Opciones_load();
	Dibujar_drawErrorText("Opciones Cargado", GameFramework_Vector2(10, 200));
	Ayuda_load();
	Dibujar_drawErrorText("Ayuda Cargado", GameFramework_Vector2(10, 200));
	AcercaDe_load();
	Dibujar_drawErrorText("Acercade Cargado", GameFramework_Vector2(10, 200));
	SavedGame_load();
	Dibujar_drawErrorText("Saved game Cargado", GameFramework_Vector2(10, 200));
	Buy_load();
	Dibujar_drawErrorText("Buy Cargado", GameFramework_Vector2(10, 200));
	MisionMode_load();
	Dibujar_drawErrorText("MisionMode Cargado", GameFramework_Vector2(10, 200));

	//Almacenamiento.borrar();
	Almacenamiento_crearDatos();
	Dibujar_drawErrorText("Crear datos Cargado",
			GameFramework_Vector2(10, 200));
	Almacenamiento_cargarOpciones();
	Dibujar_drawErrorText("Cargar opciones Cargado",
			GameFramework_Vector2(10, 200));
	Almacenamiento_cargarScore();
	Dibujar_drawErrorText("Cargar score Cargado",
			GameFramework_Vector2(10, 200));
}

// *********** UPDATE ************
void GalacticaMain_Update()
{
	if (GalacticaMainS.introTime >= 0)
	{
		GalacticaMainS.introTime = GalacticaMainS.introTime
				- GameFramework_getSwapTime();
		GalacticaMainS.dibujaIntro = 1;
		if (GalacticaMainS.introTime <= 0)
		{
			GalacticaMainS.dibujaIntro = 0;
		}
	}
	if (GalacticaMainS.enCarga == 1)
	{
		GalacticaMain_cargarTodo();
		GalacticaMainS.enJuego = 1;
		GalacticaMainS.enCarga = 0;
	}
	if (GalacticaMainS.enJuego == 1)
	{

		GalacticaMainS.contUpdates++;
		Acelerometro_update();

		switch (GalacticaMainS.estadoActual)
		{

		case GMMainMenu:
			MainMenu_update();
			break;
		case GMQuickGame:
			QuickMode_update();
			break;
		case GMMisionMode:
			MisionMode_update();
			break;
		case GMOptions:
			Opciones_update();
			break;
		case GMHelp:
			Ayuda_update();
			break;
		case GMAbout:
			AcercaDe_update();
			break;
		case GMSavedGame:
			SavedGame_update();
			break;
		case GMBuy:
			Buy_update();
			break;

		}

		//Trial mode comprobacion
		if (GalacticaMainS.isTrialMode
				&& GalacticaMainS.checkTrialModeDelay >= 0)
		{
			GalacticaMainS.checkTrialModeDelay -= GameFramework_getSwapTime();
			if (GalacticaMainS.checkTrialModeDelay <= 0)
			{
				GalacticaMainS.isTrialMode = 1;
			}
		}

		//Fotogramas por segundo o FPS
		if (GameFramework_getTotalTime()
				> GalacticaMainS.ultima_actua + 1000000)
		{
			GalacticaMainS.frames = GalacticaMainS.cont_draw;
			GalacticaMainS.updates = GalacticaMainS.contUpdates;
			GalacticaMainS.cont_draw = 0;
			GalacticaMainS.contUpdates = 0;

			GalacticaMainS.ultima_actua = GameFramework_getTotalTime();
		}
	}

}

// ************** DRAW ****************
void GalacticaMain_Draw()
{
	GalacticaMainS.cont_draw++;
	glClear(GL_COLOR_BUFFER_BIT);

	//glMatrixMode(GL_MODELVIEW);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);

	if (GalacticaMainS.dibujaIntro == 1)
	{
		Dibujar_drawSprite((*Textura_getSObject()).intro,
				GameFramework_Vector2(0, 0), GameFramework_Color(1, 1, 1, 1), 0,
				GameFramework_Vector2(0, 0), GameFramework_Vector2(1.17, 1));

		if (GalacticaMainS.empezarCarga == 1)
		{
			GalacticaMainS.enCarga = 1;
			GalacticaMainS.empezarCarga = 0;
		}
	}
	else
	{
		switch (GalacticaMainS.estadoActual)
		{

		case GMMainMenu:
			MainMenu_draw();
			break;
		case GMQuickGame:
			QuickMode_draw();
			break;
		case GMMisionMode:
			MisionMode_draw();
			break;
		case GMOptions:
			Opciones_draw();
			break;
		case GMHelp:
			Ayuda_draw();
			break;
		case GMAbout:
			AcercaDe_draw();
			break;
		case GMSavedGame:
			SavedGame_draw();
			break;
		case GMBuy:
			Buy_draw();
			break;

		}
	}

	/*Dibujar_drawText((*Textura_getSObject()).Fuente,
	 GameFramework_Concatenar(2, "FPS: ",
	 GameFramework_EnteroACadena(GalacticaMainS.frames)),
	 GameFramework_Vector2(10, 50), GameFramework_Color(1, 1, 1, 1),
	 0.8);

	 Dibujar_drawText((*Textura_getSObject()).Fuente,
	 GameFramework_Concatenar(2, "UPS: ",
	 GameFramework_EnteroACadena(GalacticaMainS.updates)),
	 GameFramework_Vector2(10, 70), GameFramework_Color(1, 1, 1, 1),
	 0.8);*/

	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

}

/*void OnActivated(object sender, EventArgs args)
 {
 isTrialMode = true;
 checkTrialModeDelay = 1000.0f;
 GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
 QuickMode.estadoActual = QuickMode.Estado.Menu;
 }

 void OnExiting(object sender, EventArgs args)
 {
 if (QuickMode.estadoActual == QuickMode.Estado.Game)
 {
 if (GalacticaMain.estadoActual == GalacticaMain.Estado.Buy
 || GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
 {
 Almacenamiento.salvarInGame();
 }
 }
 }

 void OnDeactivated(object sender, EventArgs args)
 {
 if (QuickMode.estadoActual == QuickMode.Estado.Game)
 {
 if (GalacticaMain.estadoActual == GalacticaMain.Estado.Buy
 || GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
 {
 Almacenamiento.salvarInGame();
 }
 }
 }*/

// *** Cargar Audio
void GalacticaMain_cargarAudio()
{
	Audio_Inicia();

	Audio_addAudio("/app/native/Audio/disparo.mp3",
			String_creaString("Disparo"), 0);
	Audio_addAudio("/app/native/Audio/disparo2.mp3",
			String_creaString("Disparo2"), 0);
	Audio_addAudio("/app/native/Audio/disparo3.mp3",
			String_creaString("Disparo3"), 0);
	Audio_addAudio("/app/native/Audio/EstallidoEnemigo.mp3",
			String_creaString("EstallidoEnemigo"), 0);
	Audio_addAudio("/app/native/Audio/EstallidoJefe.mp3",
			String_creaString("EstallidoJefe"), 0);
	Audio_addAudio("/app/native/Audio/inicioPoder.mp3",
			String_creaString("InicioPoder"), 0);
	Audio_addAudio("/app/native/Audio/presionarBoton.mp3",
			String_creaString("PresionarBoton"), 0);
	Audio_addAudio("/app/native/Audio/Estallido.mp3",
			String_creaString("Estallido"), 0);
	Audio_addAudio("/app/native/Audio/Error.mp3", String_creaString("Error"),
			0);
	Audio_addAudio("/app/native/Audio/Estrella.mp3",
			String_creaString("Estrella"), 0);
	Audio_addAudio("/app/native/Audio/silencio.mp3",
			String_creaString("Silencio"), 0);
	Audio_addAudio("/app/native/Audio/Galactic.mp3",
			String_creaString("Galactic"), 1);
	Audio_addAudio("/app/native/Audio/GameOver.mp3",
			String_creaString("GameOver"), 0);
	Audio_addAudio("/app/native/Audio/mainMenu.mp3",
			String_creaString("MainMenu"), 1);
}

//**********Carga Texturas**************************
void GalacticaMain_cargarTexturas()
{
	Textura_Inicia();
	Textura_addTextura("app/native/Sprites/NGalactica.png",
			String_creaString("NGalactica"));
	Textura_addTextura("app/native/Sprites/NCarafe.png",
			String_creaString("NCarafe"));
	Textura_addTextura("app/native/Sprites/NHelix.png",
			String_creaString("NHelix"));
	Textura_addTextura("app/native/Sprites/NArp.png",
			String_creaString("NArp"));
	Textura_addTextura("app/native/Sprites/NPerseus.png",
			String_creaString("NPerseus"));
	Textura_addTextura("app/native/Sprites/NPisces.png",
			String_creaString("NPisces"));
	Textura_addTextura("app/native/Sprites/PoderGalactica.png",
			String_creaString("PoderGalactica"));
	Textura_addTextura("app/native/Sprites/PoderCarafe.png",
			String_creaString("PoderCarafe"));
	Textura_addTextura("app/native/Sprites/PoderHelix.png",
			String_creaString("PoderHelix"));
	Textura_addTextura("app/native/Sprites/PoderArp.png",
			String_creaString("PoderArp"));
	Textura_addTextura("app/native/Sprites/PoderPerseus.png",
			String_creaString("PoderPerseus"));
	Textura_addTextura("app/native/Sprites/PoderPisces.png",
			String_creaString("PoderPisces"));
	Textura_addTextura("app/native/Sprites/ENor.png",
			String_creaString("ENor"));
	Textura_addTextura("app/native/Sprites/EKami.png",
			String_creaString("EKami"));
	Textura_addTextura("app/native/Sprites/ESid.png",
			String_creaString("ESid"));
	Textura_addTextura("app/native/Sprites/Disparo.png",
			String_creaString("Disparo"));
	Textura_addTextura("app/native/Sprites/SuperDisparo.png",
			String_creaString("SuperDisparo"));
	Textura_addTextura("app/native/Sprites/Titulo.png",
			String_creaString("Titulo"));
	Textura_addTextura("app/native/Sprites/DisparoSid.png",
			String_creaString("DisparoSid"));
	Textura_addTextura("app/native/Sprites/DisparoNor.png",
			String_creaString("DisparoNor"));
	Textura_addTextura("app/native/Sprites/Estrella.png",
			String_creaString("Estrella"));
	Textura_addTextura("app/native/Sprites/animationDispANave.png",
			String_creaString("animationDispANave"));
	Textura_addTextura("app/native/Sprites/animationDispAEnemigo.png",
			String_creaString("animationDispAEnemigo"));
	Textura_addTextura("app/native/Sprites/animationPoder.png",
			String_creaString("animationPoder"));
	Textura_addTextura("app/native/Sprites/EstallidoEnemigo.png",
			String_creaString("EstallidoEnemigo"));
	Textura_addTextura("app/native/Sprites/EstallidoJefe.png",
			String_creaString("EstallidoJefe"));
	Textura_addTextura("app/native/Sprites/EstallidoJefe.png",
			String_creaString("EstallidoJefe"));
	Textura_addTextura("app/native/Sprites/EstallidoNave.png",
			String_creaString("EstallidoNave"));
	Textura_addTextura("app/native/Sprites/PauseBoton.png",
			String_creaString("PauseBoton"));
	Textura_addTextura("app/native/Sprites/TituloPause.png",
			String_creaString("TituloPause"));
	Textura_addTextura("app/native/Sprites/TituloPauseMision.png",
			String_creaString("TituloPauseMision"));
	Textura_addTextura("app/native/Sprites/TituloQuickGame.png",
			String_creaString("TituloQuickGame"));
	Textura_addTextura("app/native/Sprites/TituloQuickGame.png",
			String_creaString("TituloQuickGame"));
	Textura_addTextura("app/native/Sprites/TituloGameOver.png",
			String_creaString("TituloGameOver"));
	Textura_addTextura("app/native/Sprites/TituloMisionMode.png",
			String_creaString("TituloMisionMode"));
	Textura_addTextura("app/native/Sprites/Plus.png",
			String_creaString("Plus"));
	Textura_addTextura("app/native/Sprites/BarraVida.png",
			String_creaString("BarraVida"));
	Textura_addTextura("app/native/Sprites/BarraVidaMarco.png",
			String_creaString("BarraVidaMarco"));
	Textura_addTextura("app/native/Sprites/BQuickMode.png",
			String_creaString("BQuickMode"));
	Textura_addTextura("app/native/Sprites/BOpciones.png",
			String_creaString("BOpciones"));
	Textura_addTextura("app/native/Sprites/BResumeGame.png",
			String_creaString("BResumeGame"));
	Textura_addTextura("app/native/Sprites/BExitGame.png",
			String_creaString("BExitGame"));
	Textura_addTextura("app/native/Sprites/BFlecha.png",
			String_creaString("BFlecha"));
	Textura_addTextura("app/native/Sprites/BPlay.png",
			String_creaString("BPlay"));
	Textura_addTextura("app/native/Sprites/BAudio.png",
			String_creaString("BAudio"));
	Textura_addTextura("app/native/Sprites/BMision.png",
			String_creaString("BMision"));
	Textura_addTextura("app/native/Sprites/BMisionMode.png",
			String_creaString("BMisionMode"));
	Textura_addTextura("app/native/Sprites/BPower.png",
			String_creaString("BPower"));
	Textura_addTextura("app/native/Sprites/BContinue.png",
			String_creaString("BContinue"));
	Textura_addTextura("app/native/Sprites/BBack.png",
			String_creaString("BBack"));
	Textura_addTextura("app/native/Sprites/BNextMision.png",
			String_creaString("BNextMision"));
	Textura_addTextura("app/native/Sprites/BRestart.png",
			String_creaString("BRestart"));
	Textura_addTextura("app/native/Sprites/BSalir.png",
			String_creaString("BSalir"));
	Textura_addTextura("app/native/Sprites/BHelp.png",
			String_creaString("BHelp"));
	Textura_addTextura("app/native/Sprites/BAbout.png",
			String_creaString("BAbout"));
	Textura_addTextura("app/native/Sprites/BTilting.png",
			String_creaString("BTilting"));
	Textura_addTextura("app/native/Sprites/BButtons.png",
			String_creaString("BButtons"));
	Textura_addTextura("app/native/Sprites/BMedium.png",
			String_creaString("BMedium"));
	Textura_addTextura("app/native/Sprites/BLarge.png",
			String_creaString("BLarge"));
	Textura_addTextura("app/native/Sprites/BContinueGame.png",
			String_creaString("BContinueGame"));
	Textura_addTextura("app/native/Sprites/BNewGame.png",
			String_creaString("BNewGame"));
	Textura_addTextura("app/native/Sprites/BFacebook.png",
			String_creaString("BFacebook"));
	Textura_addTextura("app/native/Sprites/BTwitter.png",
			String_creaString("BTwitter"));
	Textura_addTextura("app/native/Sprites/BComent.png",
			String_creaString("BComent"));
	Textura_addTextura("app/native/Sprites/BGoBuy.png",
			String_creaString("BGoBuy"));
	Textura_addTextura("app/native/Sprites/BNotNow.png",
			String_creaString("BNotNow"));
	Textura_addTextura("app/native/Sprites/TEscogerNave.png",
			String_creaString("TEscogerNave"));
	Textura_addTextura("app/native/Sprites/TResultados.png",
			String_creaString("TResultados"));
	Textura_addTextura("app/native/Sprites/TInfo.png",
			String_creaString("TInfo"));
	Textura_addTextura("app/native/Sprites/TRecompensa.png",
			String_creaString("TRecompensa"));
	Textura_addTextura("app/native/Sprites/TituloWin.png",
			String_creaString("TituloWin"));
	Textura_addTextura("app/native/Sprites/TituloLose.png",
			String_creaString("TituloLose"));
	Textura_addTextura("app/native/Sprites/TituloOpciones.png",
			String_creaString("TituloOpciones"));
	Textura_addTextura("app/native/Sprites/TituloSonidosMusica.png",
			String_creaString("TituloSonidosMusica"));
	Textura_addTextura("app/native/Sprites/TituloControlMode.png",
			String_creaString("TituloControlMode"));
	Textura_addTextura("app/native/Sprites/TituloButtonSize.png",
			String_creaString("TituloButtonSize"));
	Textura_addTextura("app/native/Sprites/TituloSavedGame.png",
			String_creaString("TituloSavedGame"));
	Textura_addTextura("app/native/Sprites/TituloBuyGame.png",
			String_creaString("TituloBuyGame"));
	Textura_addTextura("app/native/Sprites/Separador.png",
			String_creaString("Separador"));
	Textura_addTextura("app/native/Sprites/JLaxo.png",
			String_creaString("JLaxo"));
	Textura_addTextura("app/native/Sprites/JEsp.png",
			String_creaString("JEsp"));
	Textura_addTextura("app/native/Sprites/JEspBrazos1.png",
			String_creaString("JEspBrazos1"));
	Textura_addTextura("app/native/Sprites/JEspBrazos2.png",
			String_creaString("JEspBrazos2"));
	Textura_addTextura("app/native/Sprites/JQuad.png",
			String_creaString("JQuad"));
	Textura_addTextura("app/native/Sprites/JQuadMartillo.png",
			String_creaString("JQuadMartillo"));
	Textura_addTextura("app/native/Sprites/JRued.png",
			String_creaString("JRued"));
	Textura_addTextura("app/native/Sprites/JRuedFlecha.png",
			String_creaString("JRuedFlecha"));
	Textura_addTextura("app/native/Sprites/JefeDisparoMax.png",
			String_creaString("JefeDisparoMax"));
	Textura_addTextura("app/native/Sprites/JefeDisparoNormal.png",
			String_creaString("JefeDisparoNormal"));
	Textura_addTextura("app/native/Sprites/Capitana.png",
			String_creaString("Capitana"));
	Textura_addTextura("app/native/Sprites/NMini.png",
			String_creaString("NMini"));
	Textura_addTextura("app/native/Sprites/Onda.png",
			String_creaString("Onda"));
	Textura_addTextura("app/native/Sprites/Control.png",
			String_creaString("Control"));
	Textura_addTextura("app/native/Sprites/Help.png",
			String_creaString("Help"));
	Textura_addTextura("app/native/Sprites/About.png",
			String_creaString("About"));
	//Fondos
	Textura_addTexturaFondo("app/native/Sprites/FondoBlack.png",
			String_creaString("FondoBlack")); //0
	Textura_addTexturaFondo("app/native/Sprites/FondoEstadisticas.png",
			String_creaString("FondoEstadisticas")); //1
	Textura_addTexturaFondo("app/native/Sprites/FondoPause.png",
			String_creaString("FondoPause")); //2
}

GalacticaMain* GalacticaMain_getObject()
{
	return &GalacticaMainS;
}

