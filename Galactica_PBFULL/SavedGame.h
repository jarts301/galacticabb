/*
 * SavedGame.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef SAVEDGAME_H_
#define SAVEDGAME_H_

#include "GalacticaTipos.h"

void SavedGame_load();
void SavedGame_update();
void SavedGame_draw();
void SavedGame_loadTitulo();
void SavedGame_drawTitulo();
void SavedGame_setTexturas();

#endif /* SAVEDGAME_H_ */
