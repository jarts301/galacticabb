/*
 * MisionMode.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "MisionMode.h"
#include "Nave.h"
#include "MisionModeGOver.h"
#include "MisionModeGame.h"
#include "MisionModeInfo.h"
#include "MisionModeMenu.h"
#include "MisionModePause.h"
#include "Mision1.h"
#include "Mision10.h"
#include "Mision11.h"
#include "Mision12.h"
#include "Mision13.h"
#include "Mision14.h"
#include "Mision15.h"
#include "Mision16.h"
#include "Mision17.h"
#include "Mision18.h"
#include "Mision19.h"
#include "Mision2.h"
#include "Mision20.h"
#include "Mision3.h"
#include "Mision4.h"
#include "Mision5.h"
#include "Mision6.h"
#include "Mision7.h"
#include "Mision8.h"
#include "Mision9.h"

static MisionMode MisionModeS;

void MisionMode_load()
{
	MisionModeS.estadoActual = MMMenu;
	Mision1_Load();
	Mision2_load();
	Mision3_load();
	Mision4_load();
	Mision5_load();
	Mision6_load();
	Mision7_load();
	Mision8_load();
	Mision9_load();
	Mision10_load();
	Mision11_load();
	Mision12_load();
	Mision13_load();
	Mision14_load();
	Mision15_load();
	Mision16_load();
	Mision17_load();
	Mision18_load();
	Mision19_load();
	Mision20_load();
}

void MisionMode_update()
{
	(*Nave_getSObject()).TexturaNave = 0;
	switch (MisionModeS.estadoActual)
	{
	case MMInfo:
		MisionModeInfo_update();
		break;
	case MMGame:
		MisionModeGame_update();
		break;
	case MMMenu:
		MisionModeMenu_update();
		break;
	case MMPause:
		MisionModePause_update();
		break;
	case MMGameOver:
		MisionModeGOver_update();
		break;
	}
}

void MisionMode_draw()
{
	if (MisionModeS.estadoActual == MMGame
			|| MisionModeS.estadoActual == MMPause
			|| MisionModeS.estadoActual == MMGameOver)
	{
		MisionModeGame_draw();
	}
	if (MisionModeS.estadoActual == MMInfo)
	{
		MisionModeInfo_draw();
	}
	if (MisionModeS.estadoActual == MMMenu)
	{
		MisionModeMenu_draw();
	}
	if (MisionModeS.estadoActual == MMPause)
	{
		MisionModePause_draw();
	}
	if (MisionModeS.estadoActual == MMGameOver)
	{
		MisionModeGOver_draw();
	}
}

MisionMode* MisionMode_getSObject()
{
	return &MisionModeS;
}

