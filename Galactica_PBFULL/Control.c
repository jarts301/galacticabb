/*
 * Control.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "Control.h"
#include "GameFramework.h"
#include "Textura.h"
#include "TouchControl.h"
#include "Nave.h"
#include "Dibujar.h"
#include <math.h>
#include <stdlib.h>

static Control ControlS;
Touche* touchSobre;

Control* Control_creaObjecto(Vector2 pos)
{
	Control* nuevo;
	if ((nuevo = (Control*) malloc(sizeof(Control))) == NULL)
		return NULL;

	(*nuevo).Position = pos;
	(*nuevo).color = GameFramework_Color(1, 1, 1, 1);
	(*nuevo).Rotacion = 0.0f;
	(*nuevo).Escala = GameFramework_Vector2(0.75f, 0.75f);
	ControlS.controlActivo = 0;
	ControlS.tamanoGrande = 0;
	(*nuevo).actualframe = 0;
	(*nuevo).Origen = GameFramework_Vector2(128, 128);
	Control_defineTrozoAnim(nuevo, 0, 0, 256, 256);
	Control_setOrigen(nuevo);
	return nuevo;
}

void Control_load()
{
	ControlS.texControl = Textura_getTextura("Control");
	ControlS.centro1 = GameFramework_Vector2(130, 150);
	ControlS.centro2 = GameFramework_Vector2(150, 150);
}

void Control_update(Control* control)
{
	//touches = TouchControl.touches;
	if (ControlS.tamanoGrande == 1)
	{
		ControlS.elCentro = ControlS.centro2;
		(*control).regNeutra = 28;
		(*control).Escala = GameFramework_Vector2(0.9f, 0.9f);
		Control_setPosition(control, ControlS.centro2);
	}
	else
	{
		ControlS.elCentro = ControlS.centro1;
		(*control).regNeutra = 23;
		(*control).Escala = GameFramework_Vector2(0.75f, 0.75f);
		Control_setPosition(control, ControlS.centro1);
	}

	if (Control_estaSobre(control) == 1)
	{
		(*control).actualframe = 0;
		Control_defineTrozoAnim(control, 256 * (*control).actualframe, 0, 256,
				256);
		Control_setOrigen(control);

		Control_procesaMovimiento(control);
	}
	else
	{
		(*control).actualframe = 1;
		Control_defineTrozoAnim(control, 256 * (*control).actualframe, 0, 256,
				256);
		Control_setOrigen(control);
	}

}

void Control_draw(Control* control)
{
	Dibujar_drawSpriteTr(ControlS.texControl, (*control).Position,
			(*control).TrozoAnim, (*control).color,
			GameFramework_ToRadians((*control).Rotacion), (*control).Origen,
			(*control).Escala);
	/*sb.Draw(texControl, Position, TrozoAnim, color,
	 GameFramework_ToRadians((*control).Rotacion), Origen, Escala, SpriteEffects.None,
	 0.29f);*/
}

//*****************************************************************************
void Control_defineTrozoAnim(Control* control, int x, int y, int width,
		int height)
{
	(*control).TrozoAnim.X = x;
	(*control).TrozoAnim.Y = y;
	(*control).TrozoAnim.Width = width;
	(*control).TrozoAnim.Height = height;
}

void Control_setOrigen(Control* control)
{
	(*control).Origen.X = (*control).TrozoAnim.Width / 2;
	(*control).Origen.Y = (*control).TrozoAnim.Height / 2;
}

void Control_setPosition(Control* control, Vector2 pos)
{
	(*control).Position = pos;
}

int Control_estaSobre(Control* control)
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (ControlS.i = 0; ControlS.i < (*TouchControl_getTouches()).size;
				ControlS.i++)
		{
			(*control).distancia =
					(float) sqrt(
							pow(
									(*TouchControl_getATouch(ControlS.i)).position.X
											- ControlS.elCentro.X, 2)
									+ pow(
											(*TouchControl_getATouch(ControlS.i)).position.Y
													- ControlS.elCentro.Y, 2));

			if ((*control).distancia <= (126 * (*control).Escala.X))
			{
				touchSobre = TouchControl_getATouch(ControlS.i);
				return 1;
			}
		}
	}
	touchSobre = NULL;
	return 0;
}

void Control_procesaMovimiento(Control* control)
{
	(*Nave_getSObject()).limiteShot = 300;

	/*if ((*TouchControl_getTouches()).size > 0)
	 {
	 for (ControlS.i = 0; ControlS.i < (*TouchControl_getTouches()).size;
	 ControlS.i++)
	 {*/
	(*control).resX = (*touchSobre).position.X - ControlS.elCentro.X;
	(*control).resY = (*touchSobre).position.Y - ControlS.elCentro.Y;

	if ((*control).resX > 0 && (*control).resY > 0)
	{
		(*control).angulo = GameFramework_ToDegrees(
				(float) atan((*control).resY / (*control).resX));
	}
	if ((*control).resX < 0 && (*control).resY > 0)
	{
		(*control).angulo = 180
				+ GameFramework_ToDegrees(
						(float) atan((*control).resY / (*control).resX));
	}
	if ((*control).resX < 0 && (*control).resY < 0)
	{
		(*control).angulo = 180
				+ GameFramework_ToDegrees(
						(float) atan((*control).resY / (*control).resX));
	}
	if ((*control).resX > 0 && (*control).resY < 0)
	{
		(*control).angulo = 360
				+ GameFramework_ToDegrees(
						(float) atan((*control).resY / (*control).resX));
	}
	/*}
	 }*/

	if ((*control).distancia > (*control).regNeutra)
	{
		if ((*control).angulo < 22.5f || (*control).angulo > 337.5)
		{
			(*Nave_getSObject()).auxPosition.X =
					(*Nave_getSObject()).auxPosition.X
							+ (*Nave_getSObject()).velocidadNave;
		}
		if ((*control).angulo > 22.5f && (*control).angulo < 67.5f
				&& (*Nave_getSObject()).movEnY == 1)
		{
			(*Nave_getSObject()).auxPosition.X =
					(*Nave_getSObject()).auxPosition.X
							+ (*Nave_getSObject()).velocidadNave;
			(*Nave_getSObject()).auxPosition.Y =
					(*Nave_getSObject()).auxPosition.Y
							+ (*Nave_getSObject()).velocidadNave;
		}
		if ((*control).angulo > 67.5f && (*control).angulo < 112.5f
				&& (*Nave_getSObject()).movEnY == 1)
		{
			(*Nave_getSObject()).auxPosition.Y =
					(*Nave_getSObject()).auxPosition.Y
							+ (*Nave_getSObject()).velocidadNave;
		}
		if ((*control).angulo > 112.5f && (*control).angulo < 157.5f
				&& (*Nave_getSObject()).movEnY==1)
		{
			(*Nave_getSObject()).auxPosition.X =
					(*Nave_getSObject()).auxPosition.X
							- (*Nave_getSObject()).velocidadNave;
			(*Nave_getSObject()).auxPosition.Y =
					(*Nave_getSObject()).auxPosition.Y
							+ (*Nave_getSObject()).velocidadNave;
		}
		if ((*control).angulo > 157.5f && (*control).angulo < 202.5f)
		{
			(*Nave_getSObject()).auxPosition.X =
					(*Nave_getSObject()).auxPosition.X
							- (*Nave_getSObject()).velocidadNave;
		}
		if ((*control).angulo > 202.5f && (*control).angulo < 247.5f
				&& (*Nave_getSObject()).movEnY==1)
		{
			(*Nave_getSObject()).auxPosition.X =
					(*Nave_getSObject()).auxPosition.X
							- (*Nave_getSObject()).velocidadNave;
			(*Nave_getSObject()).auxPosition.Y =
					(*Nave_getSObject()).auxPosition.Y
							- (*Nave_getSObject()).velocidadNave;
		}
		if ((*control).angulo > 247.5f && (*control).angulo < 292.5f
				&& (*Nave_getSObject()).movEnY==1)
		{
			(*Nave_getSObject()).auxPosition.Y =
					(*Nave_getSObject()).auxPosition.Y
							- (*Nave_getSObject()).velocidadNave;
		}
		if ((*control).angulo > 292.5f && (*control).angulo < 337.5f
				&& (*Nave_getSObject()).movEnY==1)
		{
			(*Nave_getSObject()).auxPosition.X =
					(*Nave_getSObject()).auxPosition.X
							+ (*Nave_getSObject()).velocidadNave;
			(*Nave_getSObject()).auxPosition.Y =
					(*Nave_getSObject()).auxPosition.Y
							- (*Nave_getSObject()).velocidadNave;
		}

		Nave_setPosition((*Nave_getSObject()).auxPosition);
	}

}

Control* Control_getSObject()
{
	return &ControlS;
}

