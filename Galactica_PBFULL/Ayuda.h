/*
 * Ayuda.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef AYUDA_H_
#define AYUDA_H_

#include "GalacticaTipos.h"

void Ayuda_load();
void Ayuda_update();
void Ayuda_draw();
void Ayuda_loadTitulo();
void Ayuda_drawTitulo();
void Ayuda_setTexturas();

#endif /* AYUDA_H_ */
