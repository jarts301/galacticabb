/*
 * QuickMode.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "QuickMode.h"
#include "QuickModeGOver.h"
#include "QuickModeGame.h"
#include "QuickModeMenu.h"
#include "QuickModePause.h"

static QuickMode QuickModeS;

void QuickMode_load()
{
	QuickModeS.estadoActual = QMMenu;
}

void QuickMode_update()
{
	switch (QuickModeS.estadoActual)
	{
	case QMGame:
		QuickModeGame_update();
		break;
	case QMPause:
		QuickModePause_update();
		break;
	case QMMenu:
		QuickModeMenu_update();
		break;
	case QMGameOver:
		QuickModeGOver_update();
		break;
	}
}

void QuickMode_draw()
{
	if (QuickModeS.estadoActual == QMGame || QuickModeS.estadoActual == QMPause
			|| QuickModeS.estadoActual == QMGameOver)
	{
		QuickModeGame_draw();
	}
	if (QuickModeS.estadoActual == QMPause)
	{
		QuickModePause_draw();
	}
	if (QuickModeS.estadoActual == QMMenu)
	{
		QuickModeMenu_draw();
	}
	if (QuickModeS.estadoActual == QMGameOver)
	{
		QuickModeGOver_draw();
	}
}

QuickMode* QuickMode_getSObject()
{
	return &QuickModeS;
}

