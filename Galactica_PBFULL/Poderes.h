/*
 * Poderes.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef PODERES_H_
#define PODERES_H_

#include "GalacticaTipos.h"

Poderes* Poderes_Crear(Vector2 posN, Poderes_tipo t, Poderes_tipoPoderCarafe tc);
void Poderes_load();
void Poderes_update(Poderes* poderes);
void Poderes_draw(Poderes* poderes);
void Poderes_setTextura();
Vector2 Poderes_getPosition(Poderes* poderes);
void Poderes_setPosition(Poderes* poderes, Vector2 pos);
void Poderes_movimiento(Poderes* poderes);
void Poderes_setOrigen(Poderes* poderes, int tex);
void Poderes_defineCollitionBoxPArp();
Poderes* Poderes_getSObject();

#endif /* PODERES_H_ */
