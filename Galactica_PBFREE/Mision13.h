/*
 * Mision13.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION13_H_
#define MISION13_H_

#include "GalacticaTipos.h"

void Mision13_reiniciar();
void Mision13_load();
void Mision13_update();
void Mision13_draw();
void Mision13_drawEstadNave();
void Mision13_updateGame();
void Mision13_gameOver();
void Mision13_drawMensajeInicial();
void Mision13_insertaEnemigo();
Mision13* Mision13_getSObject();

#endif /* MISION13_H_ */
