/*
 * NMini.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "NMini.h"
#include "Dibujar.h"
#include "Textura.h"
#include "Lista.h"
#include <stdlib.h>

static NMini NMiniS;

NMini* NMini_Crear()
{
	NMini* nuevo;
	if ((nuevo = (NMini*) malloc(sizeof(NMini))) == NULL)
		return NULL;

	(*nuevo).Rotacion = 0;
	(*nuevo).Escala = GameFramework_Vector2(0.37, 0.37);
	(*nuevo).Position.X = 280;
	(*nuevo).Position.Y = 100;
	(*nuevo).cambiarMov = 1;
	(*nuevo).texturaActual = 0;
	(*nuevo).actualframe=0;
	(*nuevo).radioCollition = (int) (30 * (*nuevo).Escala.X);
	NMini_setTrozoAnimacion(nuevo);
	NMini_setOrigen2(nuevo);
	NMini_defineCollitionBox(nuevo);
	return nuevo;

}

void NMini_reiniciar(NMini* nMini)
{
	(*nMini).vida = (*nMini).maxVida = 50;
	(*nMini).Rotacion = 0;
	(*nMini).Escala = GameFramework_Vector2(0.37, 0.37);
	(*nMini).Position.X = 280;
	(*nMini).Position.Y = 100;
	(*nMini).cambiarMov = 1;
	(*nMini).radioCollition = (int) (30 * (*nMini).Escala.X);
	(*nMini).actualframe=0;
	NMini_setTrozoAnimacion(nMini);
	NMini_setOrigen2(nMini);
	(*nMini).texturaActual = 0;
	NMini_defineCollitionBox(nMini);
}

void NMini_load()
{
	NMini_setTextura();
}

void NMini_update(NMini* nMini)
{
	NMini_velocidadAnimacion(nMini, 4);
	NMini_setTrozoAnimacion(nMini);
	NMini_setOrigen2(nMini);
	NMini_movimiento(nMini);
}

void NMini_draw(NMini* nMini)
{
	Dibujar_drawSpriteTr(
			(TexturaObjeto*) Lista_GetIn(&NMiniS.texDisparo,
					(*nMini).texturaActual), (*nMini).Position,
			(*nMini).TrozoAnim, GameFramework_Color(1, 1, 1, 1),
			(*nMini).Rotacion, (*nMini).Origen, (*nMini).Escala);
}

//*********************************

void NMini_setTextura()
{
	Lista_Inicia(&NMiniS.texDisparo);
	Lista_AddInFin(&NMiniS.texDisparo, Textura_getTextura("NMini"));
}

void NMini_setPosition(NMini* nMini, Vector2 posNave)
{
	(*nMini).Position.X = posNave.X;
	(*nMini).Position.Y = posNave.Y;
}

Vector2 NMini_getPosition(NMini* nMini)
{
	return (*nMini).Position;
}

void NMini_setOrigen(NMini* nMini, int textura)
{
	(*nMini).Origen.X = (*(TexturaObjeto*) Lista_GetIn(&NMiniS.texDisparo,
			textura)).ancho / 2;
	(*nMini).Origen.Y = (*(TexturaObjeto*) Lista_GetIn(&NMiniS.texDisparo,
			textura)).alto / 2;
}

//define la velocidad de la animacion de la nave
void NMini_velocidadAnimacion(NMini* nMini, int velocidad)
{
	(*nMini).dato += GameFramework_getSwapTime();
	if ((*nMini).dato >= ((GameFramework_getSwapTime()) * velocidad))
	{
		(*nMini).actualframe = (*nMini).actualframe + 1;
		(*nMini).dato = 0;
	}
	if ((*nMini).actualframe >= 2)
	{
		(*nMini).actualframe = 0;
	}
}

void NMini_setOrigen2(NMini* nMini)
{
	(*nMini).Origen.X = (*nMini).TrozoAnim.Width / 2;
	(*nMini).Origen.Y = (*nMini).TrozoAnim.Height / 2;
}

//el cuadro que debe mostrar en el siguiente draw
void NMini_setTrozoAnimacion(NMini* nMini)
{
	(*nMini).TrozoAnim.X = (*nMini).actualframe * 256;
	(*nMini).TrozoAnim.Y = 0;
	(*nMini).TrozoAnim.Width = 256;
	(*nMini).TrozoAnim.Height = 256;
}

//DETECTOR DE COLISIONES
void NMini_defineCollitionBox(NMini* nMini)
{
	//Centros de los circulos de colision
	//suma necesaria a partir de la pos actual de la nave
	(*nMini).collitionX[0] = (int) (-1 * NMiniS.Escala.X);
	(*nMini).collitionY[0] = (int) (-36 * NMiniS.Escala.Y);
	(*nMini).collitionX[1] = (int) (-10 * NMiniS.Escala.X);
	(*nMini).collitionY[1] = (int) (-18 * NMiniS.Escala.Y);
	(*nMini).collitionX[2] = (int) (-20 * NMiniS.Escala.X);
	(*nMini).collitionY[2] = (int) (-2 * NMiniS.Escala.Y);
	(*nMini).collitionX[3] = (int) (-26 * NMiniS.Escala.X);
	(*nMini).collitionY[3] = (int) (16 * NMiniS.Escala.Y);
	(*nMini).collitionX[4] = (int) (-34 * NMiniS.Escala.X);
	(*nMini).collitionY[4] = (int) (33 * NMiniS.Escala.Y);
	(*nMini).collitionX[5] = (int) (-9 * NMiniS.Escala.X);
	(*nMini).collitionY[5] = (int) (17 * NMiniS.Escala.Y);
	(*nMini).collitionX[6] = (int) (9 * NMiniS.Escala.X);
	(*nMini).collitionY[6] = (int) (17 * NMiniS.Escala.Y);
	(*nMini).collitionX[7] = (int) (34 * NMiniS.Escala.X);
	(*nMini).collitionY[7] = (int) (33 * NMiniS.Escala.Y);
	(*nMini).collitionX[8] = (int) (26 * NMiniS.Escala.X);
	(*nMini).collitionY[8] = (int) (16 * NMiniS.Escala.Y);
	(*nMini).collitionX[9] = (int) (18 * NMiniS.Escala.X);
	(*nMini).collitionY[9] = (int) (-2 * NMiniS.Escala.Y);
	(*nMini).collitionX[10] = (int) (8 * NMiniS.Escala.X);
	(*nMini).collitionY[10] = (int) (-18 * NMiniS.Escala.Y);
}

//Movimiento nave mini
void NMini_movimiento(NMini* nMini)
{
	if ((*nMini).cambiarMov == 1)
	{
		(*nMini).velocidadX = GameFramework_Rand(3, 4);
		(*nMini).velocidadY = GameFramework_Rand(3, 4);
		(*nMini).num = GameFramework_Rand(1, 4);
		switch ((*nMini).num)
		{
		case 1:
			(*nMini).velocidadX = (*nMini).velocidadX * (1);
			(*nMini).velocidadY = (*nMini).velocidadY * (1);
			break;
		case 2:
			(*nMini).velocidadX = (*nMini).velocidadX * (-1);
			(*nMini).velocidadY = (*nMini).velocidadY * (1);
			break;
		case 3:
			(*nMini).velocidadX = (*nMini).velocidadX * (1);
			(*nMini).velocidadY = (*nMini).velocidadY * (-1);
			break;
		case 4:
			(*nMini).velocidadX = (*nMini).velocidadX * (-1);
			(*nMini).velocidadY = (*nMini).velocidadY * (-1);
			break;

		}
		(*nMini).cambiarMov = 0;
	}

	if ((*nMini).Position.Y >= 400 && (*nMini).velocidadY > 0)
	{
		(*nMini).velocidadY = (*nMini).velocidadY * (-1);
		//cambiarMov = 1;
	}
	if ((*nMini).Position.Y <= 30 && (*nMini).velocidadY < 0)
	{
		(*nMini).velocidadY = (*nMini).velocidadY * (-1);
		(*nMini).cambiarMov = 1;
	}
	if ((*nMini).Position.X >= 570 && (*nMini).velocidadX > 0)
	{
		(*nMini).velocidadX = (*nMini).velocidadX * (-1);
		//cambiarMov = 1;
	}
	if ((*nMini).Position.X <= 30 && (*nMini).velocidadX < 0)
	{
		(*nMini).velocidadX = (*nMini).velocidadX * (-1);
		(*nMini).cambiarMov = 1;
	}

	(*nMini).Position.X = (*nMini).Position.X + (*nMini).velocidadX;
	(*nMini).Position.Y = (*nMini).Position.Y + (*nMini).velocidadY;
}

NMini* NMini_getSObject()
{
	return &NMiniS;
}

