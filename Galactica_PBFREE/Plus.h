/*
 * Plus.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef PLUS_H_
#define PLUS_H_

#include "GalacticaTipos.h"

Plus* Plus_Crear(Vector2 pos);
void Plus_reiniciar(Plus* plus, Vector2 pos);
void Plus_load();
void Plus_update(Plus* plus);
void Plus_draw(Plus* plus);
void Plus_setTextura();
void Plus_setPosition(Plus* plus, Vector2 v);
Vector2 Plus_getPosition(Plus* plus);
void Plus_setOrigen(Plus* plus, int textura);
Plus* Plus_getSObject();

#endif /* PLUS_H_ */
