/*
 * Nave.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "Nave.h"
#include "Animation.h"
#include "Poderes.h"
#include "Lista.h"
#include "Dibujar.h"
#include "Disparo.h"
#include "Textura.h"
#include "TouchControl.h"
#include "Audio.h"
#include "Acelerometro.h"
#include <stdlib.h>

static Nave NaveS;

void Nave_load()
{
	NaveS.MaxVida = 100;
	NaveS.tEsperaPGalactica = 50000;
	NaveS.Destruidos = 0;
	Lista_Inicia(&NaveS.disparos);
	Lista_Inicia(&NaveS.disparosTotales);
	Nave_loadDisparos();
	Nave_SetTexturas();
	Nave_loadPoder();
	Nave_iniciaAnimacionesPoder();
	NaveS.poderActivo = 0;
	NaveS.movEnY = 1;
	NaveS.disparosDiagActivos = 0;
	NaveS.disparoSuperActivo = 0;
	NaveS.disparosActivos = 1;
	NaveS.sePuedeDispDiag = 0;
	NaveS.sePuedeDispSuper = 0;
	Nave_setTrozoAnimacion();
	Nave_setOrigen();
	NaveS.velocidadNave = 5;
	NaveS.tiempoAyudaPorColision = 0;
	NaveS.actualFrameAyuda = GameFramework_Vector2(0,0);
	NaveS.contadorPoderGalactica = 0;
	NaveS.contadorPoderCarafe = 0;
	NaveS.contadorPoderHelix = 0;
	NaveS.contadorPoderArp = 0;
	NaveS.contadorPoderPerseus = 0;
	NaveS.contadorPoderPisces = 0;
	NaveS.actualframe = 0;
	NaveS.contadorToques = 0;
	NaveS.Rotacion = 0;
	NaveS.EscalaN = NaveS.escalaInicial = GameFramework_Vector2(0.365, 0.53);
	NaveS.radioCollitionN = (int) (50 * NaveS.EscalaN.X);
	NaveS.Vida = NaveS.MaxVida;
	NaveS.Puntos = 0;
	NaveS.ultimoTPoder = 0;
	NaveS.permiteColisionesJefe = 1;
	NaveS.tiempoAyuda = 0;
	Nave_puntoInicio(GameFramework_Vector2(0, 0));
	Nave_defineCollitionBox();
	Nave_reiniciaPoderes();
	(*(*Animation_getSObject()).animacionesEstallidoNave).animacionFinalizada =
			0;
	Animation_defineTrozoAnim(NaveS.animacionPoderGalactica,
			(*NaveS.animacionPoderGalactica).actualframe * 512, 0, 512, 1024);
	Animation_setOrigenV(NaveS.animacionPoderGalactica,
			GameFramework_Vector2(0, 0));
}

void Nave_reiniciar()
{
	NaveS.poderActivo = 0;
	NaveS.disparosActivos = 1;
	NaveS.movEnY = 1;
	NaveS.disparosDiagActivos = 0;
	NaveS.disparoSuperActivo = 0;
	NaveS.tiempoAyudaPorColision = 0;
	NaveS.actualFrameAyuda = GameFramework_Vector2(0,0);
	NaveS.velocidadNave = 5;
	NaveS.contadorPoderGalactica = 0;
	NaveS.contadorPoderCarafe = 0;
	NaveS.contadorPoderHelix = 0;
	NaveS.contadorPoderArp = 0;
	NaveS.contadorPoderPerseus = 0;
	NaveS.contadorPoderPisces = 0;
	NaveS.EscalaN = NaveS.escalaInicial = GameFramework_Vector2(0.365, 0.53);
	NaveS.radioCollitionN = (int) (50 * NaveS.EscalaN.X);
	NaveS.Vida = NaveS.MaxVida;
	NaveS.Puntos = 0;
	NaveS.Destruidos = 0;
	NaveS.ultimoTPoder = 0;
	NaveS.permiteColisionesJefe = 1;
	NaveS.estallaActivo = 1;
	NaveS.tiempoAyuda = 0;
	NaveS.tEsperaPGalactica = 50000;
	Vector2 VectorZ;
	VectorZ.X = 0;
	VectorZ.Y = 0;
	Nave_setTrozoAnimacion();
	Nave_setOrigen();
	Nave_puntoInicio(VectorZ);
	Nave_reiniciaPoderes();
	Nave_reiniciarDisparos();
	(*(*Animation_getSObject()).animacionesEstallidoNave).animacionFinalizada =
			0;
	Animation_defineTrozoAnim(NaveS.animacionPoderGalactica,
			(*NaveS.animacionPoderGalactica).actualframe * 512, 0, 512, 1024);
	Animation_setOrigenV(NaveS.animacionPoderGalactica,
			GameFramework_Vector2(0, 0));
}

void Nave_update()
{
	Nave_velocidadAnimacion(5);
	Nave_analisisPos();
	Nave_setTrozoAnimacion();
	Nave_setOrigen();
	Nave_cicloDisparos();
	Nave_updatePoder();
	Animation_update(NaveS.animacionPoderGalactica);
	Animation_update(NaveS.animacionPoderCarafe);
	Animation_update(NaveS.animacionPoderHelix);
	Animation_update(NaveS.animacionPoderArp);
	Animation_update(NaveS.animacionPoderPerseus);
	Animation_update(NaveS.animacionPoderPisces);
	Nave_updateColisionesJefe();
	if (NaveS.Vida <= 0 && NaveS.estallaActivo == 1)
	{
		Audio_playAudio("Estallido");
		NaveS.estallaActivo = 0;
	}
}

void Nave_draw()
{
	Nave_dibujaDisparo();
	Nave_drawPoder();

	Dibujar_drawSpriteTr(
			(TexturaObjeto*) Lista_GetIn(&NaveS.texNaves, NaveS.TexturaNave),
			NaveS.PositionNav, NaveS.TrozoAnim, GameFramework_Color(1, 1, 1, 1),
			NaveS.Rotacion, NaveS.Origen, NaveS.EscalaN);

}

void Nave_drawAnimacionesPoder()
{
	Animation_draw(NaveS.animacionPoderGalactica);
	Animation_draw(NaveS.animacionPoderCarafe);
	Animation_draw(NaveS.animacionPoderHelix);
	Animation_draw(NaveS.animacionPoderArp);
	Animation_draw(NaveS.animacionPoderPerseus);
	Animation_draw(NaveS.animacionPoderPisces);
}

//*******************************************************
//DETECTOR DE COLISIONES
void Nave_defineCollitionBox()
{
	//Centros de los circulos de colision
	//suma necesaria a partir de la pos actual de la nave
	NaveS.collitionX[0] = (int) (-1 * NaveS.EscalaN.X);
	NaveS.collitionY[0] = (int) (-36 * NaveS.EscalaN.Y);
	NaveS.collitionX[1] = (int) (-10 * NaveS.EscalaN.X);
	NaveS.collitionY[1] = (int) (-18 * NaveS.EscalaN.Y);
	NaveS.collitionX[2] = (int) (-20 * NaveS.EscalaN.X);
	NaveS.collitionY[2] = (int) (-2 * NaveS.EscalaN.Y);
	NaveS.collitionX[3] = (int) (-26 * NaveS.EscalaN.X);
	NaveS.collitionY[3] = (int) (16 * NaveS.EscalaN.Y);
	NaveS.collitionX[4] = (int) (-34 * NaveS.EscalaN.X);
	NaveS.collitionY[4] = (int) (33 * NaveS.EscalaN.Y);
	NaveS.collitionX[5] = (int) (-9 * NaveS.EscalaN.X);
	NaveS.collitionY[5] = (int) (17 * NaveS.EscalaN.Y);
	NaveS.collitionX[6] = (int) (9 * NaveS.EscalaN.X);
	NaveS.collitionY[6] = (int) (17 * NaveS.EscalaN.Y);
	NaveS.collitionX[7] = (int) (34 * NaveS.EscalaN.X);
	NaveS.collitionY[7] = (int) (33 * NaveS.EscalaN.Y);
	NaveS.collitionX[8] = (int) (26 * NaveS.EscalaN.X);
	NaveS.collitionY[8] = (int) (16 * NaveS.EscalaN.Y);
	NaveS.collitionX[9] = (int) (18 * NaveS.EscalaN.X);
	NaveS.collitionY[9] = (int) (-2 * NaveS.EscalaN.Y);
	NaveS.collitionX[10] = (int) (8 * NaveS.EscalaN.X);
	NaveS.collitionY[10] = (int) (-18 * NaveS.EscalaN.Y);
}

// define la posicion
void Nave_setPosition(Vector2 position)
{
	NaveS.PositionNav.X = position.X;
	NaveS.PositionNav.Y = position.Y;
}

//obtiene la posicion
Vector2 Nave_getPosition()
{
	return NaveS.PositionNav;
}
//carga la textura de la nave
void Nave_SetTexturas()
{
	Lista_Inicia(&NaveS.texNaves);
	Lista_AddInFin(&NaveS.texNaves, Textura_getTextura("NGalactica")); //0
	Lista_AddInFin(&NaveS.texNaves, Textura_getTextura("NCarafe")); //1
	Lista_AddInFin(&NaveS.texNaves, Textura_getTextura("NHelix")); //2
	Lista_AddInFin(&NaveS.texNaves, Textura_getTextura("NArp")); //3
	Lista_AddInFin(&NaveS.texNaves, Textura_getTextura("NPerseus")); //4
	Lista_AddInFin(&NaveS.texNaves, Textura_getTextura("NPisces")); //5
}
//define el trozo de la animacion a mostrar
//el cuadro que debe mostrar en el siguiente draw
void Nave_setTrozoAnimacion()
{
	NaveS.TrozoAnim.X = NaveS.actualframe * 341.333;
	NaveS.TrozoAnim.Y = 0;
	NaveS.TrozoAnim.Width = 341.333;
	NaveS.TrozoAnim.Height = 256;
}
//define el origen de coordenadas
//del cuadro de animacion actual
void Nave_setOrigen()
{
	NaveS.Origen.X = NaveS.TrozoAnim.Width / 2;
	NaveS.Origen.Y = NaveS.TrozoAnim.Height / 2;
}

//define el punto de inicio de la nave
void Nave_puntoInicio(Vector2 pos)
{
	if (pos.X == 0 && pos.Y == 0)
	{
		NaveS.auxPosition.X = 300;
		NaveS.auxPosition.Y = 300;
	}
	else
	{
		NaveS.auxPosition.X = pos.X;
		NaveS.auxPosition.Y = pos.Y;
	}

	Nave_setPosition(NaveS.auxPosition);
}

//**********************Reinicia Disparos***************************
void Nave_reiniciarDisparos()
{
	for (NaveS.i = 0; NaveS.i < NaveS.disparos.size; NaveS.i++)
	{
		if ((*(Disparo*) Lista_GetIn(&NaveS.disparos, NaveS.i)).tipoActual
				== Dpequed
				|| (*(Disparo*) Lista_GetIn(&NaveS.disparos, NaveS.i)).tipoActual
						== Dpequei)
		{
			Lista_AddInFin(&NaveS.disparosTotales,
					Lista_GetIn(&NaveS.disparos, NaveS.i));
		}
		else if ((*(Disparo*) Lista_GetIn(&NaveS.disparos, NaveS.i)).tipoActual
				== DdiagD)
		{
			Lista_AddInFin(&NaveS.disparosDiagDerechaTotales,
					Lista_GetIn(&NaveS.disparos, NaveS.i));
		}
		else if ((*(Disparo*) Lista_GetIn(&NaveS.disparos, NaveS.i)).tipoActual
				== DdiagI)
		{
			Lista_AddInFin(&NaveS.disparosDiagIzquierdaTotales,
					Lista_GetIn(&NaveS.disparos, NaveS.i));
		}
		else if ((*(Disparo*) Lista_GetIn(&NaveS.disparos, NaveS.i)).tipoActual
				== Dsuper)
		{
			Lista_AddInFin(&NaveS.disparosSuperTotales,
					Lista_GetIn(&NaveS.disparos, NaveS.i));
		}
	}

	Lista_Destruir(&(NaveS.disparos));
}

//**********************Reinicia poderes****************************
void Nave_reiniciaPoderes()
{
	//Galactica
	for (NaveS.j = 0; NaveS.j < NaveS.poderGalactica.size; NaveS.j++)
	{
		Lista_InsertaEnFinal(&(NaveS.poderGalacticaTotal),
				NaveS.poderGalacticaTotal.fin,
				Lista_RetornaElemento(&(NaveS.poderGalactica), NaveS.j));
	}
	Lista_Destruir(&(NaveS.poderGalactica));
	NaveS.poderActivo = 0;
	Animation_TerminaAnimacion(NaveS.animacionPoderGalactica, 1);
	NaveS.contadorPoderGalactica = 0;

	//Carafe
	for (NaveS.j = 0; NaveS.j < NaveS.poderCarafe.size; NaveS.j++)
	{
		(*(Poderes*) Lista_RetornaElemento(&(NaveS.poderCarafe), NaveS.j)).valAuxCarafe =
				10;
		if ((*(Poderes*) Lista_RetornaElemento(&(NaveS.poderCarafe), NaveS.j)).tipoPoderCarafeActual
				== PCMedio)
		{
			Lista_InsertaEnFinal(&(NaveS.poderCarafeMedioTotal),
					NaveS.poderCarafeMedioTotal.fin,
					Lista_RetornaElemento(&(NaveS.poderCarafe), NaveS.j));
		}
		else if ((*(Poderes*) Lista_RetornaElemento(&(NaveS.poderCarafe),
				NaveS.j)).tipoPoderCarafeActual == PCIzquierda)
		{
			Lista_AddInFin(&NaveS.poderCarafeIzquierdaTotal,
					Lista_GetIn(&NaveS.poderCarafe, NaveS.j));
		}
		else if ((*(Poderes*) Lista_RetornaElemento(&(NaveS.poderCarafe),
				NaveS.j)).tipoPoderCarafeActual == PCDerecha)
		{
			Lista_AddInFin(&NaveS.poderCarafeDerechaTotal,
					Lista_GetIn(&NaveS.poderCarafe, NaveS.j));
		}
	}
	Lista_Destruir(&(NaveS.poderCarafe));
	Animation_TerminaAnimacion(NaveS.animacionPoderCarafe, 1);
	NaveS.contadorPoderCarafe = 0;

	//Helix
	for (NaveS.j = 0; NaveS.j < NaveS.poderHelix.size; NaveS.j++)
	{
		Lista_AddInFin(&NaveS.poderHelixTotal,
				Lista_GetIn(&NaveS.poderHelix, NaveS.j));
	}
	Lista_Destruir(&(NaveS.poderHelix));
	Animation_TerminaAnimacion(NaveS.animacionPoderHelix, 1);
	NaveS.contadorPoderHelix = 0;
	NaveS.ultimoTPoder = 0;

	//Arp
	for (NaveS.j = 0; NaveS.j < NaveS.poderArp.size; NaveS.j++)
	{
		Lista_AddInFin(&NaveS.poderArpTotal,
				Lista_GetIn(&NaveS.poderArp, NaveS.j));
	}
	Lista_Destruir(&(NaveS.poderArp));
	Animation_TerminaAnimacion(NaveS.animacionPoderArp, 1);
	NaveS.contadorPoderArp = 0;

	//Perseus
	for (NaveS.j = 0; NaveS.j < NaveS.poderPerseus.size; NaveS.j++)
	{
		(*(Poderes*) Lista_RetornaElemento(&(NaveS.poderPerseusTotal), NaveS.j)).Escala.X =
				0.1f;
		(*(Poderes*) Lista_RetornaElemento(&(NaveS.poderPerseusTotal), NaveS.j)).Escala.Y =
				0.1f;
		Lista_AddInFin(&NaveS.poderPerseusTotal,
				Lista_GetIn(&NaveS.poderPerseus, NaveS.j));
	}
	Lista_Destruir(&(NaveS.poderPerseus));
	Animation_TerminaAnimacion(NaveS.animacionPoderPerseus, 1);
	NaveS.contadorPoderPerseus = 0;

	//Pisces
	for (NaveS.j = 0; NaveS.j < NaveS.poderPisces.size; NaveS.j++)
	{
		Lista_AddInFin(&NaveS.poderPiscesTotal,
				Lista_GetIn(&NaveS.poderPisces, NaveS.j));
	}
	Lista_Destruir(&(NaveS.poderPisces));
	Animation_TerminaAnimacion(NaveS.animacionPoderPisces, 1);
	NaveS.contadorPoderPisces = 0;
}

//dibuja los disparos de la nave
void Nave_dibujaDisparo()
{
	if (NaveS.disparos.size > 0)
	{
		for (NaveS.i = 0; NaveS.i < NaveS.disparos.size; NaveS.i++)
		{
			Disparo_draw((Disparo*) Lista_GetIn(&NaveS.disparos, NaveS.i));
		}
	}
}
//define la velocidad de la animacion de la nave
void Nave_velocidadAnimacion(int velocidad)
{
	NaveS.dato += GameFramework_getSwapTime();
	if (NaveS.dato >= ((GameFramework_getSwapTime()) * velocidad))
	{
		NaveS.actualframe = NaveS.actualframe + 1;
		NaveS.dato = 0;
	}
	if (NaveS.actualframe >= 3)
	{
		NaveS.actualframe = 0;
	}
}
//hace todas las comprobaciones con disparos
void Nave_cicloDisparos()
{
	if ((*TouchControl_getTouches()).size > 0)
	{
		for (NaveS.i = 0; NaveS.i < (*TouchControl_getTouches()).size;
				NaveS.i++)
		{
			NaveS.touched = TouchControl_getATouch(NaveS.i);
			if ((*NaveS.touched).estado == Pressed
					&& (*NaveS.touched).position.X > NaveS.limiteShot
					&& NaveS.disparosActivos == 1)
			{
				Audio_playAudio("Disparo");

				NaveS.contadorToques++;
				if (NaveS.disparosTotales.size > 0)
				{
					Disparo_setPositionPequed(
							(Disparo*) Lista_GetIn(&NaveS.disparosTotales,
									NaveS.disparosTotales.size - 1),
							NaveS.PositionNav);

					Lista_AddInFin(&NaveS.disparos,
							Lista_GetIn(&NaveS.disparosTotales,
									NaveS.disparosTotales.size - 1));

					Lista_RemoveAt(&NaveS.disparosTotales,
							NaveS.disparosTotales.size - 1);

					Disparo_setPositionPequei(
							(Disparo*) Lista_GetIn(&NaveS.disparosTotales,
									NaveS.disparosTotales.size - 1),
							NaveS.PositionNav);

					Lista_AddInFin(&NaveS.disparos,
							Lista_GetIn(&NaveS.disparosTotales,
									NaveS.disparosTotales.size - 1));

					Lista_RemoveAt(&NaveS.disparosTotales,
							NaveS.disparosTotales.size - 1);
				}

				if (NaveS.disparosDiagActivos == 1)
				{
					if (NaveS.disparosDiagDerechaTotales.size > 0)
					{
						Disparo_setPositionPequed(
								(Disparo*) Lista_GetIn(
										&NaveS.disparosDiagDerechaTotales,
										NaveS.disparosDiagDerechaTotales.size
												- 1), NaveS.PositionNav);

						Lista_AddInFin(&NaveS.disparos,
								Lista_GetIn(&NaveS.disparosDiagDerechaTotales,
										NaveS.disparosDiagDerechaTotales.size
												- 1));

						Lista_RemoveAt(&NaveS.disparosDiagDerechaTotales,
								NaveS.disparosDiagDerechaTotales.size - 1);
					}

					if (NaveS.disparosDiagIzquierdaTotales.size > 0)
					{
						Disparo_setPositionPequei(
								(Disparo*) Lista_GetIn(
										&NaveS.disparosDiagIzquierdaTotales,
										NaveS.disparosDiagIzquierdaTotales.size
												- 1), NaveS.PositionNav);

						Lista_AddInFin(&NaveS.disparos,
								Lista_GetIn(&NaveS.disparosDiagIzquierdaTotales,
										NaveS.disparosDiagIzquierdaTotales.size
												- 1));

						Lista_RemoveAt(&NaveS.disparosDiagIzquierdaTotales,
								NaveS.disparosDiagIzquierdaTotales.size - 1);
					}
				}

				if (NaveS.contadorToques >= 3 && NaveS.disparoSuperActivo == 1)
				{
					if (NaveS.disparosSuperTotales.size > 0)
					{
						Disparo_setPositionSuper(
								(Disparo*) Lista_GetIn(
										&NaveS.disparosSuperTotales,
										NaveS.disparosSuperTotales.size - 1),
								NaveS.PositionNav);

						Lista_AddInFin(&NaveS.disparos,
								Lista_GetIn(&NaveS.disparosSuperTotales,
										NaveS.disparosSuperTotales.size - 1));

						Lista_RemoveAt(&NaveS.disparosSuperTotales,
								NaveS.disparosSuperTotales.size - 1);
					}
					NaveS.contadorToques = 0;
				}
			}
		}
	}

	if (NaveS.disparos.size > 0)
	{
		for (NaveS.i = 0; NaveS.i < NaveS.disparos.size; NaveS.i++)
		{
			Disparo_update(Lista_GetIn(&NaveS.disparos, NaveS.i));
		}
	}

	if (NaveS.disparos.size > 0)
	{
		for (NaveS.i = 0; NaveS.i < NaveS.disparos.size; NaveS.i++)
		{
			if ((*(Disparo*) Lista_GetIn(&NaveS.disparos, NaveS.i)).Position.Y
					> 1024
					|| (*(Disparo*) Lista_GetIn(&NaveS.disparos, NaveS.i)).Position.X
							< 0
					|| (*(Disparo*) Lista_GetIn(&NaveS.disparos, NaveS.i)).Position.X
							> 600)
			{
				if ((*(Disparo*) Lista_GetIn(&NaveS.disparos, NaveS.i)).tipoActual
						== Dpequed
						|| (*(Disparo*) Lista_GetIn(&NaveS.disparos, NaveS.i)).tipoActual
								== Dpequei)
				{
					Lista_AddInFin(&NaveS.disparosTotales,
							Lista_GetIn(&NaveS.disparos, NaveS.i));

					Lista_RemoveAt(&NaveS.disparos, NaveS.i);
				}
				else if ((*(Disparo*) Lista_GetIn(&NaveS.disparos, NaveS.i)).tipoActual
						== DdiagD)
				{
					Lista_AddInFin(&NaveS.disparosDiagDerechaTotales,
							Lista_GetIn(&NaveS.disparos, NaveS.i));

					Lista_RemoveAt(&NaveS.disparos, NaveS.i);
				}
				else if ((*(Disparo*) Lista_GetIn(&NaveS.disparos, NaveS.i)).tipoActual
						== DdiagI)
				{
					Lista_AddInFin(&NaveS.disparosDiagIzquierdaTotales,
							Lista_GetIn(&NaveS.disparos, NaveS.i));

					Lista_RemoveAt(&NaveS.disparos, NaveS.i);
				}
				else if ((*(Disparo*) Lista_GetIn(&NaveS.disparos, NaveS.i)).tipoActual
						== Dsuper)
				{
					Lista_AddInFin(&NaveS.disparosSuperTotales,
							Lista_GetIn(&NaveS.disparos, NaveS.i));

					Lista_RemoveAt(&NaveS.disparos, NaveS.i);
				}

			}
		}
	}
}
//analiza la pos de la nave y sus limites
//tambien el movimiento de acuerdo al acelerometro
void Nave_analisisPos()
{
	if (NaveS.auxPosition.X >= 580)
	{
		NaveS.auxPosition.X = 580;
	}
	if (NaveS.auxPosition.X <= 20)
	{
		NaveS.auxPosition.X = 20;
	}
	if (NaveS.auxPosition.Y >= 1000)
	{
		NaveS.auxPosition.Y = 1000;
	}
	if (NaveS.auxPosition.Y <= 50)
	{
		NaveS.auxPosition.Y = 50;
	}

	Nave_setPosition(NaveS.auxPosition);
}

//************************Poder ******************
//* Carga Poder
void Nave_loadPoder()
{
	Lista_Inicia(&NaveS.poderGalacticaTotal);
	Lista_Inicia(&NaveS.poderGalactica);
	Lista_Inicia(&NaveS.poderCarafeMedioTotal);
	Lista_Inicia(&NaveS.poderCarafeIzquierdaTotal);
	Lista_Inicia(&NaveS.poderCarafeDerechaTotal);
	Lista_Inicia(&NaveS.poderCarafe);
	Lista_Inicia(&NaveS.poderHelixTotal);
	Lista_Inicia(&NaveS.poderHelix);
	Lista_Inicia(&NaveS.poderArpTotal);
	Lista_Inicia(&NaveS.poderArp);
	Lista_Inicia(&NaveS.poderPerseusTotal);
	Lista_Inicia(&NaveS.poderPerseus);
	Lista_Inicia(&NaveS.poderPiscesTotal);
	Lista_Inicia(&NaveS.poderPisces);

	for (NaveS.i = 0; NaveS.i < 300; NaveS.i++)
	{
		Lista_AddInFin(&NaveS.poderGalacticaTotal,
				Poderes_Crear(GameFramework_Vector2(0, 0), PGalactica, PCNull));

		Lista_AddInFin(&NaveS.poderCarafeMedioTotal,
				Poderes_Crear(GameFramework_Vector2(0, 0), PCarafe, PCMedio));

		Lista_AddInFin(&NaveS.poderCarafeIzquierdaTotal,
				Poderes_Crear(GameFramework_Vector2(0, 0), PCarafe,
						PCIzquierda));

		Lista_AddInFin(&NaveS.poderCarafeDerechaTotal,
				Poderes_Crear(GameFramework_Vector2(0, 0), PCarafe, PCDerecha));

		Lista_AddInFin(&NaveS.poderHelixTotal,
				Poderes_Crear(GameFramework_Vector2(0, 0), PHelix, PCNull));

		Lista_AddInFin(&NaveS.poderArpTotal,
				Poderes_Crear(GameFramework_Vector2(0, 0), PArp, PCNull));

		Lista_AddInFin(&NaveS.poderPerseusTotal,
				Poderes_Crear(GameFramework_Vector2(0, 0), PPerseus, PCNull));

		Lista_AddInFin(&NaveS.poderPiscesTotal,
				Poderes_Crear(GameFramework_Vector2(0, 0), PPisces, PCNull));
	}
}

//* Actualizar poder
void Nave_updatePoder()
{
	if (NaveS.poderActivo == 1)
	{
		switch (NaveS.TexturaNave)
		{
		case 0:
			Animation_TerminaAnimacion(NaveS.animacionPoderGalactica, 0);
			if (NaveS.contadorPoderGalactica < 200)
			{
				if (NaveS.poderGalacticaTotal.size > 0)
				{

					Poderes_setPosition(
							(Poderes*) Lista_GetIn(&NaveS.poderGalacticaTotal,
									NaveS.poderGalacticaTotal.size - 1),
							NaveS.PositionNav);

					Lista_AddInFin(&NaveS.poderGalactica,
							Lista_GetIn(&NaveS.poderGalacticaTotal,
									NaveS.poderGalacticaTotal.size - 1));

					Lista_RemoveAt(&NaveS.poderGalacticaTotal,
							NaveS.poderGalacticaTotal.size - 1);
				}
				NaveS.contadorPoderGalactica++;
			}

			for (NaveS.j = 0; NaveS.j < NaveS.poderGalactica.size; NaveS.j++)
			{
				Poderes_update(
						(Poderes*) Lista_GetIn(&NaveS.poderGalactica, NaveS.j));
			}

			for (NaveS.j = 0; NaveS.j < NaveS.poderGalactica.size; NaveS.j++)
			{
				if ((*(Poderes*) Lista_GetIn(&NaveS.poderGalactica, NaveS.j)).Position.Y
						> 1040)
				{
					Lista_AddInFin(&NaveS.poderGalacticaTotal,
							Lista_GetIn(&NaveS.poderGalactica, NaveS.j));

					Lista_RemoveAt(&NaveS.poderGalactica, NaveS.j);
				}
			}

			if (NaveS.poderGalactica.size <= 0)
			{
				NaveS.poderActivo = 0;
				Animation_TerminaAnimacion(NaveS.animacionPoderGalactica, 1);
				NaveS.contadorPoderGalactica = 0;
			}

			break;

		case 1:
			Animation_TerminaAnimacion(NaveS.animacionPoderCarafe, 0);
			if (NaveS.contadorPoderCarafe < 200)
			{
				if (NaveS.poderCarafeMedioTotal.size > 0)
				{
					Poderes_setPosition(
							(Poderes*) Lista_GetIn(&NaveS.poderCarafeMedioTotal,
									NaveS.poderCarafeMedioTotal.size - 1),
							NaveS.PositionNav);

					Lista_AddInFin(&NaveS.poderCarafe,
							Lista_GetIn(&NaveS.poderCarafeMedioTotal,
									NaveS.poderCarafeMedioTotal.size - 1));

					Lista_RemoveAt(&NaveS.poderCarafeMedioTotal,
							NaveS.poderCarafeMedioTotal.size - 1);
				}

				if (NaveS.poderCarafeIzquierdaTotal.size > 0)
				{
					Poderes_setPosition(
							(Poderes*) Lista_GetIn(
									&NaveS.poderCarafeIzquierdaTotal,
									NaveS.poderCarafeIzquierdaTotal.size - 1),
							NaveS.PositionNav);

					Lista_AddInFin(&NaveS.poderCarafe,
							Lista_GetIn(&NaveS.poderCarafeIzquierdaTotal,
									NaveS.poderCarafeIzquierdaTotal.size - 1));

					Lista_RemoveAt(&NaveS.poderCarafeIzquierdaTotal,
							NaveS.poderCarafeIzquierdaTotal.size - 1);
				}

				if (NaveS.poderCarafeDerechaTotal.size > 0)
				{
					Poderes_setPosition(
							(Poderes*) Lista_GetIn(
									&NaveS.poderCarafeDerechaTotal,
									NaveS.poderCarafeDerechaTotal.size - 1),
							NaveS.PositionNav);

					Lista_AddInFin(&NaveS.poderCarafe,
							Lista_GetIn(&NaveS.poderCarafeDerechaTotal,
									NaveS.poderCarafeDerechaTotal.size - 1));

					Lista_RemoveAt(&NaveS.poderCarafeDerechaTotal,
							NaveS.poderCarafeDerechaTotal.size - 1);
				}

				NaveS.contadorPoderCarafe++;
			}

			for (NaveS.j = 0; NaveS.j < NaveS.poderCarafe.size; NaveS.j++)
			{
				Poderes_update(
						(Poderes*) Lista_GetIn(&NaveS.poderCarafe, NaveS.j));
			}

			for (NaveS.j = 0; NaveS.j < NaveS.poderCarafe.size; NaveS.j++)
			{
				if ((*(Poderes*) Lista_GetIn(&NaveS.poderCarafe, NaveS.j)).Position.Y
						> 1024
						|| (*(Poderes*) Lista_GetIn(&NaveS.poderCarafe, NaveS.j)).Position.X
								< 0
						|| (*(Poderes*) Lista_GetIn(&NaveS.poderCarafe, NaveS.j)).Position.X
								> 600)
				{
					(*(Poderes*) Lista_GetIn(&NaveS.poderCarafe, NaveS.j)).valAuxCarafe =
							10;

					if ((*(Poderes*) Lista_GetIn(&NaveS.poderCarafe, NaveS.j)).tipoPoderCarafeActual
							== PCMedio)
					{
						Lista_AddInFin(&NaveS.poderCarafeMedioTotal,
								Lista_GetIn(&NaveS.poderCarafe, NaveS.j));
					}
					else if ((*(Poderes*) Lista_GetIn(&NaveS.poderCarafe,
							NaveS.j)).tipoPoderCarafeActual == PCIzquierda)
					{
						Lista_AddInFin(&NaveS.poderCarafeIzquierdaTotal,
								Lista_GetIn(&NaveS.poderCarafe, NaveS.j));
					}
					else if ((*(Poderes*) Lista_GetIn(&NaveS.poderCarafe,
							NaveS.j)).tipoPoderCarafeActual == PCDerecha)
					{
						Lista_AddInFin(&NaveS.poderCarafeDerechaTotal,
								Lista_GetIn(&NaveS.poderCarafe, NaveS.j));
					}
					Lista_RemoveAt(&NaveS.poderCarafe, NaveS.j);
				}
			}

			if (NaveS.poderCarafe.size == 0)
			{
				NaveS.poderActivo = 0;
				Animation_TerminaAnimacion(NaveS.animacionPoderCarafe, 1);
				NaveS.contadorPoderCarafe = 0;
			}
			break;

		case 2:
			Animation_TerminaAnimacion(NaveS.animacionPoderHelix, 0);
			if (GameFramework_getTotalTime() > NaveS.ultimoTPoder + 1000000)
			{
				if (NaveS.contadorPoderHelix < 10)
				{
					if (NaveS.poderHelixTotal.size > 0)
					{
						Poderes_setPosition(
								(Poderes*) Lista_GetIn(&NaveS.poderHelixTotal,
										NaveS.poderHelixTotal.size - 1),
								NaveS.PositionNav);
						Lista_AddInFin(&NaveS.poderHelix,
								Lista_GetIn(&NaveS.poderHelixTotal,
										NaveS.poderHelixTotal.size - 1));
						Lista_RemoveAt(&NaveS.poderHelixTotal,
								NaveS.poderHelixTotal.size - 1);
					}
					NaveS.contadorPoderHelix++;
				}
				NaveS.ultimoTPoder = GameFramework_getTotalTime();

				if (NaveS.poderHelix.size <= 0)
				{
					NaveS.poderActivo = 0;
					Animation_TerminaAnimacion(NaveS.animacionPoderHelix, 1);
					NaveS.contadorPoderHelix = 0;
					NaveS.ultimoTPoder = 0;
				}

			}

			for (NaveS.j = 0; NaveS.j < NaveS.poderHelix.size; NaveS.j++)
			{
				Poderes_update(
						(Poderes*) Lista_GetIn(&NaveS.poderHelix, NaveS.j));
			}

			for (NaveS.j = 0; NaveS.j < NaveS.poderHelix.size; NaveS.j++)
			{
				if ((*(Poderes*) Lista_GetIn(&NaveS.poderHelix, NaveS.j)).Position.Y
						> 1024)
				{
					Lista_AddInFin(&NaveS.poderHelixTotal,
							Lista_GetIn(&NaveS.poderHelix, NaveS.j));
					Lista_RemoveAt(&NaveS.poderHelix, NaveS.j);
				}
			}
			break;

		case 3:
			Animation_TerminaAnimacion(NaveS.animacionPoderArp, 0);
			if (GameFramework_getTotalTime() > NaveS.ultimoTPoder + 4000000)
			{
				if (NaveS.contadorPoderArp < 2)
				{
					if (NaveS.poderArpTotal.size > 0)
					{
						Poderes_setPosition(
								(Poderes*) Lista_GetIn(&NaveS.poderArpTotal,
										NaveS.poderArpTotal.size - 1),
								GameFramework_Vector2(300, -30));
						Lista_AddInFin(&NaveS.poderArp,
								Lista_GetIn(&NaveS.poderArpTotal,
										NaveS.poderArpTotal.size - 1));
						Lista_RemoveAt(&NaveS.poderArpTotal,
								NaveS.poderArpTotal.size - 1);
					}
					NaveS.contadorPoderArp++;
				}
				NaveS.ultimoTPoder = GameFramework_getTotalTime();

				if (NaveS.poderArp.size <= 0)
				{
					NaveS.poderActivo = 0;
					Animation_TerminaAnimacion(NaveS.animacionPoderArp, 1);
					NaveS.contadorPoderArp = 0;
					NaveS.ultimoTPoder = 0;
				}

			}

			for (NaveS.j = 0; NaveS.j < NaveS.poderArp.size; NaveS.j++)
			{
				Poderes_update(
						(Poderes*) Lista_GetIn(&NaveS.poderArp, NaveS.j));
			}

			for (NaveS.j = 0; NaveS.j < NaveS.poderArp.size; NaveS.j++)
			{
				if ((*(Poderes*) Lista_GetIn(&NaveS.poderArp, NaveS.j)).Position.Y
						> 1024)
				{
					Lista_AddInFin(&NaveS.poderArpTotal,
							Lista_GetIn(&NaveS.poderArp, NaveS.j));
					Lista_RemoveAt(&NaveS.poderArp, NaveS.j);
				}
			}
			break;

		case 4:
			Animation_TerminaAnimacion(NaveS.animacionPoderPerseus, 0);
			if (GameFramework_getTotalTime() > NaveS.ultimoTPoder + 1000000)
			{
				if (NaveS.contadorPoderPerseus < 5)
				{
					if (NaveS.poderPerseusTotal.size > 0)
					{
						Poderes_setPosition(
								(Poderes*) Lista_GetIn(&NaveS.poderPerseusTotal,
										NaveS.poderPerseusTotal.size - 1),
								NaveS.PositionNav);

						Lista_AddInFin(&NaveS.poderPerseus,
								Lista_GetIn(&NaveS.poderPerseusTotal,
										NaveS.poderPerseusTotal.size - 1));

						Lista_RemoveAt(&NaveS.poderPerseusTotal,
								NaveS.poderPerseusTotal.size - 1);
					}
					NaveS.contadorPoderPerseus++;
				}
				NaveS.ultimoTPoder = GameFramework_getTotalTime();

				if (NaveS.poderPerseus.size <= 0)
				{
					NaveS.poderActivo = 0;
					Animation_TerminaAnimacion(NaveS.animacionPoderPerseus, 1);
					NaveS.contadorPoderPerseus = 0;
					NaveS.ultimoTPoder = 0;
				}
			}

			for (NaveS.j = 0; NaveS.j < NaveS.poderPerseus.size; NaveS.j++)
			{
				Poderes_update(
						(Poderes*) Lista_GetIn(&NaveS.poderPerseus, NaveS.j));
			}

			for (NaveS.j = 0; NaveS.j < NaveS.poderPerseus.size; NaveS.j++)
			{
				if ((*(Poderes*) Lista_GetIn(&NaveS.poderPerseus, NaveS.j)).Escala.X
						> 3)
				{
					(*(Poderes*) Lista_GetIn(&NaveS.poderPerseus, NaveS.j)).Escala =
							GameFramework_Vector2(0.1, 0.1);
					Lista_AddInFin(&NaveS.poderPerseusTotal,
							Lista_GetIn(&NaveS.poderPerseus, NaveS.j));
					Lista_RemoveAt(&NaveS.poderPerseus, NaveS.j);
				}
			}
			break;

		case 5:
			Animation_TerminaAnimacion(NaveS.animacionPoderPisces, 0);
			if (NaveS.contadorPoderPisces < 200)
			{
				if (NaveS.poderPiscesTotal.size > 0)
				{
					Poderes_setPosition(
							(Poderes*) Lista_GetIn(&NaveS.poderPiscesTotal,
									NaveS.poderPiscesTotal.size - 1),
							NaveS.PositionNav);
					Lista_AddInFin(&NaveS.poderPisces,
							Lista_GetIn(&NaveS.poderPiscesTotal,
									NaveS.poderPiscesTotal.size - 1));
					Lista_RemoveAt(&NaveS.poderPiscesTotal,
							NaveS.poderPiscesTotal.size - 1);
				}
				NaveS.contadorPoderPisces++;
			}

			for (NaveS.j = 0; NaveS.j < NaveS.poderPisces.size; NaveS.j++)
			{
				Poderes_update(
						(Poderes*) Lista_GetIn(&NaveS.poderPisces, NaveS.j));
			}

			for (NaveS.j = 0; NaveS.j < NaveS.poderPisces.size; NaveS.j++)
			{
				if ((*(Poderes*) Lista_GetIn(&NaveS.poderPisces, NaveS.j)).Position.Y
						> 1040
						|| (*(Poderes*) Lista_GetIn(&NaveS.poderPisces, NaveS.j)).Position.X
								< -20
						|| (*(Poderes*) Lista_GetIn(&NaveS.poderPisces, NaveS.j)).Position.X
								> 650)
				{
					Lista_AddInFin(&NaveS.poderPiscesTotal,
							Lista_GetIn(&NaveS.poderPisces, NaveS.j));
					Lista_RemoveAt(&NaveS.poderPisces, NaveS.j);
				}
			}

			if (NaveS.poderPisces.size <= 0)
			{
				NaveS.poderActivo = 0;
				Animation_TerminaAnimacion(NaveS.animacionPoderPisces, 1);
				NaveS.contadorPoderPisces = 0;
			}
			break;
		}
	}
}

//* Dibuja Poder
void Nave_drawPoder()
{
	if (NaveS.poderActivo)
	{
		switch (NaveS.TexturaNave)
		{
		case 0:
			for (NaveS.j = 0; NaveS.j < NaveS.poderGalactica.size; NaveS.j++)
			{
				Poderes_draw(
						(Poderes*) Lista_GetIn(&NaveS.poderGalactica, NaveS.j));
			}
			break;

		case 1:
			for (NaveS.j = 0; NaveS.j < NaveS.poderCarafe.size; NaveS.j++)
			{
				Poderes_draw(
						(Poderes*) Lista_GetIn(&NaveS.poderCarafe, NaveS.j));
			}
			break;

		case 2:
			for (NaveS.j = 0; NaveS.j < NaveS.poderHelix.size; NaveS.j++)
			{
				Poderes_draw(
						(Poderes*) Lista_GetIn(&NaveS.poderHelix, NaveS.j));
			}
			break;

		case 3:
			for (NaveS.j = 0; NaveS.j < NaveS.poderArp.size; NaveS.j++)
			{
				Poderes_draw((Poderes*) Lista_GetIn(&NaveS.poderArp, NaveS.j));
			}
			break;

		case 4:
			for (NaveS.j = 0; NaveS.j < NaveS.poderPerseus.size; NaveS.j++)
			{
				Poderes_draw(
						(Poderes*) Lista_GetIn(&NaveS.poderPerseus, NaveS.j));
			}
			break;
		case 5:
			for (NaveS.j = 0; NaveS.j < NaveS.poderPisces.size; NaveS.j++)
			{
				Poderes_draw(
						(Poderes*) Lista_GetIn(&NaveS.poderPisces, NaveS.j));
			}
			break;
		}
	}
}

void Nave_iniciaAnimacionesPoder()
{
	NaveS.animacionPoderGalactica = Animation_Crear(APoderGalactica);
	Animation_TerminaAnimacion(NaveS.animacionPoderGalactica, 1);
	Animation_setPosition(NaveS.animacionPoderGalactica,
			GameFramework_Vector2(0, 0));

	NaveS.animacionPoderCarafe = Animation_Crear(APoderCarafe);
	Animation_TerminaAnimacion(NaveS.animacionPoderCarafe, 1);
	Animation_setPosition(NaveS.animacionPoderCarafe,
			GameFramework_Vector2(0, 0));

	NaveS.animacionPoderHelix = Animation_Crear(APoderHelix);
	Animation_TerminaAnimacion(NaveS.animacionPoderHelix, 1);
	Animation_setPosition(NaveS.animacionPoderHelix,
			GameFramework_Vector2(0, 0));

	NaveS.animacionPoderArp = Animation_Crear(APoderArp);
	Animation_TerminaAnimacion(NaveS.animacionPoderArp, 1);
	Animation_setPosition(NaveS.animacionPoderArp, GameFramework_Vector2(0, 0));

	NaveS.animacionPoderPerseus = Animation_Crear(APoderPerseus);
	Animation_TerminaAnimacion(NaveS.animacionPoderPerseus, 1);
	Animation_setPosition(NaveS.animacionPoderPerseus,
			GameFramework_Vector2(0, 0));

	NaveS.animacionPoderPisces = Animation_Crear(APoderPisces);
	Animation_TerminaAnimacion(NaveS.animacionPoderPisces, 1);
	Animation_setPosition(NaveS.animacionPoderPisces,
			GameFramework_Vector2(0, 0));
}

void Nave_updateColisionesJefe()
{
	if (NaveS.permiteColisionesJefe)
	{
		NaveS.tiempoAyudaPorColision = GameFramework_getTotalTime();
	}

	if (NaveS.permiteColisionesJefe == 0)
	{

		NaveS.EscalaN = GameFramework_Vector2(NaveS.actualFrameAyuda.X,
				NaveS.actualFrameAyuda.Y);

		NaveS.tiempoAyuda += GameFramework_getSwapTime();
		if (NaveS.tiempoAyuda >= (GameFramework_getSwapTime()) * 2)
		{
			NaveS.actualFrameAyuda.X = NaveS.actualFrameAyuda.X
					+ NaveS.escalaInicial.X;
			NaveS.actualFrameAyuda.Y = NaveS.actualFrameAyuda.Y
					+ NaveS.escalaInicial.Y;
			NaveS.tiempoAyuda = 0;
		}

		if (NaveS.actualFrameAyuda.X > NaveS.escalaInicial.X)
		{
			NaveS.actualFrameAyuda = GameFramework_Vector2(0,0);
		}

		if (GameFramework_getTotalTime()
				> NaveS.tiempoAyudaPorColision + 2000000)
		{
			NaveS.EscalaN = NaveS.escalaInicial;
			NaveS.permiteColisionesJefe = 1;
		}

	}
}

void Nave_loadDisparos()
{
	Lista_Inicia(&NaveS.disparosTotales);
	for (NaveS.i = 0; NaveS.i < 150; NaveS.i++)
	{
		Lista_AddInFin(&NaveS.disparosTotales,
				Disparo_CrearObject(GameFramework_Vector2(0, 0), Dpequed));
		Lista_AddInFin(&NaveS.disparosTotales,
				Disparo_CrearObject(GameFramework_Vector2(0, 0), Dpequei));
	}

	Lista_Inicia(&NaveS.disparosDiagDerechaTotales);
	for (NaveS.i = 0; NaveS.i < 100; NaveS.i++)
	{
		Lista_AddInFin(&NaveS.disparosDiagDerechaTotales,
				Disparo_CrearObject(GameFramework_Vector2(0, 0), DdiagD));
	}

	Lista_Inicia(&NaveS.disparosDiagIzquierdaTotales);
	for (NaveS.i = 0; NaveS.i < 100; NaveS.i++)
	{
		Lista_AddInFin(&NaveS.disparosDiagIzquierdaTotales,
				Disparo_CrearObject(GameFramework_Vector2(0, 0), DdiagI));
	}

	Lista_Inicia(&NaveS.disparosSuperTotales);
	for (NaveS.i = 0; NaveS.i < 100; NaveS.i++)
	{
		Lista_AddInFin(&NaveS.disparosSuperTotales,
				Disparo_CrearObject(GameFramework_Vector2(0, 0), Dsuper));
	}
}

Nave* Nave_getSObject()
{
	return &NaveS;
}
