/*
 * MainMenu.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "MainMenu.h"
#include "Boton.h"
#include "Textura.h"
#include "Dibujar.h"
#include "GameComun.h"
#include "Audio.h"
#include "GalacticaMain.h"
#include <stdlib.h>

static MainMenu MainMenuS;

void MainMenu_load()
{
	MainMenu_setTexturas();
	MainMenu_loadTitulo();
	Lista_Inicia(&(MainMenuS.botones));
	Lista_AddInFin(&(MainMenuS.botones), Boton_Crear(BQuickGame, 30, 650));
	Lista_AddInFin(&(MainMenuS.botones), Boton_Crear(BMisionMode, 265, 540));
	Lista_AddInFin(&(MainMenuS.botones), Boton_Crear(BOpciones, 30, 430));
	Lista_AddInFin(&(MainMenuS.botones), Boton_Crear(BHelp, 265, 320));
	Lista_AddInFin(&(MainMenuS.botones), Boton_Crear(BAbout, 30, 210));
	Lista_AddInFin(&(MainMenuS.botones), Boton_Crear(BSalir, 265, 100));
	Lista_AddInFin(&(MainMenuS.botones), Boton_Crear(BFacebook, 10, 100));
	Lista_AddInFin(&(MainMenuS.botones), Boton_Crear(BTwitter, 100, 100));
	Lista_AddInFin(&(MainMenuS.botones), Boton_Crear(BGoBuy, 370, 920));
	MainMenuS.playMusic = 1;
}

void MainMenu_update()
{
	GameComun_updateEstrellas();
	for (MainMenuS.i = 0; MainMenuS.i < MainMenuS.botones.size; MainMenuS.i++)
	{
		Boton_update((Boton*) Lista_GetIn(&(MainMenuS.botones), MainMenuS.i));
	}

	if ((*Audio_getSObject()).musicaActiva == 1 && MainMenuS.playMusic == 1)
	{
		Audio_playAudio("MainMenu");
		MainMenuS.playMusic = 0;
	}
}

void MainMenu_draw()
{
	//CLEAR NEGRO
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_RetornaElemento(&(MainMenuS.textura), 1),
			GameFramework_Vector2(0, 0), GameFramework_Color(0, 0, 0, 1), 0,
			GameFramework_Vector2(0, 0), GameFramework_Vector2(100, 100));

	GameComun_drawEstrellas();

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "Best Score: ",
					GameFramework_EnteroACadena(
							(*GalacticaMain_getObject()).BESTSCORE)),
			GameFramework_Vector2(10, 10),
			GameFramework_Color(GameFramework_getByteColorToFloatColor(244),
					GameFramework_getByteColorToFloatColor(232),
					GameFramework_getByteColorToFloatColor(16), 1), 1);

	MainMenu_drawTitulo();
	for (MainMenuS.i = 0; MainMenuS.i < MainMenuS.botones.size;
			MainMenuS.i++)
	{
		if ((*(Boton*) Lista_GetIn(&(MainMenuS.botones), MainMenuS.i)).tipoActual
				== BGoBuy)
		{
			(*(Boton*) Lista_GetIn(&(MainMenuS.botones), MainMenuS.i)).Escala =
					GameFramework_Vector2(0.8, 0.6);
		}
		Boton_draw((Boton*) Lista_GetIn(&(MainMenuS.botones), MainMenuS.i));
	}
}

//******************************************

//******TITULO***********
void MainMenu_loadTitulo()
{
	MainMenuS.positionTitulo = GameFramework_Vector2(300, 850);
	MainMenuS.origenTitulo = GameFramework_Vector2(
			(*(TexturaObjeto*) Lista_GetIn(&(MainMenuS.textura), 0)).ancho / 2,
			(*(TexturaObjeto*) Lista_GetIn(&(MainMenuS.textura), 0)).alto / 2);
}

void MainMenu_drawTitulo()
{
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_RetornaElemento(&(MainMenuS.textura), 0),
			MainMenuS.positionTitulo, GameFramework_Color(1, 1, 1, 1), 0,
			MainMenuS.origenTitulo, GameFramework_Vector2(1.2, 1.2));
}

//*********Texturas***************
void MainMenu_setTexturas()
{
	Lista_Inicia(&(MainMenuS.textura));
	Lista_AddInFin(&(MainMenuS.textura), Textura_getTextura("Titulo")); //0
	Lista_AddInFin(&(MainMenuS.textura), Textura_getTexturaFondo(0)); //1
}

MainMenu* MainMenu_getSObject()
{
	return &MainMenuS;
}

