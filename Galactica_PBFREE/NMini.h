/*
 * NMini.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef NMINI_H_
#define NMINI_H_

#include "GalacticaTipos.h"

NMini* NMini_Crear();
void NMini_reiniciar(NMini* nMini);
void NMini_load();
void NMini_update(NMini* nMini);
void NMini_draw(NMini* nMini);
void NMini_setTextura();
void NMini_setPosition(NMini* nMini, Vector2 posNave);
Vector2 NMini_getPosition(NMini* nMini);
void NMini_setOrigen(NMini* nMini, int textura);
void NMini_velocidadAnimacion(NMini* nMini, int velocidad);
void NMini_setOrigen2(NMini* nMini);
void NMini_setTrozoAnimacion(NMini* nMini);
void NMini_defineCollitionBox();
void NMini_movimiento(NMini* nMini);
NMini* NMini_getSObject();

#endif /* NMINI_H_ */
