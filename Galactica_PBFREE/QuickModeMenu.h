/*
 * QuickModeMenu.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef QUICKMODEMENU_H_
#define QUICKMODEMENU_H_

#include "GalacticaTipos.h"

void QuickModeMenu_load();
void QuickModeMenu_reiniciar();
void QuickModeMenu_update();
void QuickModeMenu_draw();
void QuickModeMenu_loadTitulo();
void QuickModeMenu_drawTitulo();
void QuickModeMenu_updateNave();
void QuickModeMenu_setTexturas();
QuickModeMenu* QuickModeMenu_getSObject();

#endif /* QUICKMODEMENU_H_ */
