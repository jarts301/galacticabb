/*
 * Mision17.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION17_H_
#define MISION17_H_

#include "GalacticaTipos.h"

void Mision17_reiniciar();
void Mision17_load();
void Mision17_update();
void Mision17_draw();
void Mision17_drawEstadoNave();
void Mision17_updateGame();
void Mision17_gameOver();
void Mision17_drawMensajeInicial();
Mision17* Mision17_getSObject();

#endif /* MISION17_H_ */

