/*
 * GameComun.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "GameComun.h"
#include "Lista.h"
#include "Estrella.h"
#include "GameFramework.h"

static GameComun GameComunS;

//***********************ESTRELLAS*********************/

/****Carga estrellas**/

void GameComun_loadEstrellas()
{
	Lista_Inicia(&(GameComunS.estrellas));
	for (GameComunS.e = 0; GameComunS.e < 60; GameComunS.e++)
	{
		Lista_AddInFin(&(GameComunS.estrellas),
				Estrella_crear(
						GameFramework_Vector2(GameFramework_Rand(0, 600),
								GameFramework_Rand(0, 1024))));

		(*(Estrella*) Lista_GetIn(&(GameComunS.estrellas), GameComunS.e)).color =
				GameFramework_Color(
						GameFramework_getByteColorToFloatColor(
								GameFramework_Rand(200, 255)),
						GameFramework_getByteColorToFloatColor(
								GameFramework_Rand(200, 255)),
						GameFramework_getByteColorToFloatColor(
								GameFramework_Rand(200, 255)), 1);

		double r=GameFramework_Rand0A1()+0.4;
		(*(Estrella*) Lista_GetIn(&(GameComunS.estrellas), GameComunS.e)).Escala =
				GameFramework_Vector2(r,r);
		(*(Estrella*) Lista_GetIn(&(GameComunS.estrellas), GameComunS.e)).vel =
				GameFramework_Rand(3, 17);
	}
}

//****Actualiza estrellas***
void GameComun_updateEstrellas()
{
	for (GameComunS.e = 0; GameComunS.e < 50; GameComunS.e++)
	{
		Estrella_update(
				((Estrella*) Lista_RetornaElemento(&(GameComunS.estrellas),
						GameComunS.e)));
	}
}

//****Dibuja estrellas*
void GameComun_drawEstrellas()
{
	for (GameComunS.e = 0; GameComunS.e < 50; GameComunS.e++)
	{
		Estrella_draw(
				((Estrella*) Lista_RetornaElemento(&(GameComunS.estrellas),
						GameComunS.e)));
	}
}

