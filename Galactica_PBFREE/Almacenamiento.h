/*
 * Almacenamiento.h
 *
 *  Created on: 14/03/2013
 *      Author: Jarts
 */

#ifndef ALMACENAMIENTO_H_
#define ALMACENAMIENTO_H_

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "GalacticaTipos.h"

void Almacenamiento_Inicia();
void Almacenamiento_borrar();
void Almacenamiento_crearDatos();
void Almacenamiento_cargarScore();
void Almacenamiento_salvarScore();
void Almacenamiento_cargarMisiones();
void Almacenamiento_salvarMisiones();
void Almacenamiento_cargarBonus();
void Almacenamiento_salvarBonus(int datoASalvar, BonusTipo tipoB);
void Almacenamiento_salvarOpciones();
void Almacenamiento_cargarOpciones();
void Almacenamiento_salvarInGame();
void Almacenamiento_borrarInGame();
void Almacenamiento_cargarInGame();
int Almacenamiento_getMisionesActivas();
int Almacenamiento_getPremiosDados();
int Almacenamiento_getBestScore();
void Almacenamiento_datosASalvarInGame(FILE* InGameFile);
int Almacenamiento_hayJuegoSalvado();

#endif /* ALMACENAMIENTO_H_ */
