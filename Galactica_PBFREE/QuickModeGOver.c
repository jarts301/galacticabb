/*
 * QuickModeGOver.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "QuickModeGOver.h"
#include "Lista.h"
#include "Boton.h"
#include "Textura.h"
#include "GameComun.h"
#include "QuickModeGame.h"
#include "Dibujar.h"
#include "Nave.h"

static QuickModeGOver QuickModeGOverS;

void QuickModeGOver_load()
{
	QuickModeGOverS.tEsperaActivaBoton = 1200000;
	QuickModeGOver_setTexturas();
	QuickModeGOver_loadTitulo();
	Lista_Inicia(&QuickModeGOverS.botones);
	QuickModeGOverS.positionPuntos = GameFramework_Vector2(100, 585);
	QuickModeGOverS.positionDestruidos = GameFramework_Vector2(100, 545);
	QuickModeGOverS.positionResultados = GameFramework_Vector2(0, 532);
	Lista_AddInFin(&QuickModeGOverS.botones, Boton_Crear(BContinue, 336, 10));
	Lista_AddInFin(&QuickModeGOverS.botones, Boton_Crear(BRestart, 10, 10));
	Lista_AddInFin(&QuickModeGOverS.botones, Boton_Crear(BComent, 160, 220));
}

void QuickModeGOver_update()
{
	GameComun_updateEstrellas();
	if (QuickModeGOverS.tEsperaActivaBoton > 0)
	{
		QuickModeGOverS.tEsperaActivaBoton -= GameFramework_getSwapTime();
	}
	else
	{
		for (QuickModeGOverS.i = 0;
				QuickModeGOverS.i < QuickModeGOverS.botones.size;
				QuickModeGOverS.i++)
		{
			Boton_update(
					(Boton*) Lista_GetIn(&QuickModeGOverS.botones,
							QuickModeGOverS.i));
		}
	}
}

void QuickModeGOver_draw()
{
	Dibujar_drawSprite(Textura_getTexturaFondo(2), GameFramework_Vector2(0, 0),
			GameFramework_Color(GameFramework_getByteColorToFloatColor(249),
					GameFramework_getByteColorToFloatColor(36),
					GameFramework_getByteColorToFloatColor(132), 1), 0,
			GameFramework_Vector2(0, 0), GameFramework_Vector2(2.5, 2));

	GameComun_drawEstrellas();
	QuickModeGOver_drawTitulo();
	for (QuickModeGOverS.i = 0;
			QuickModeGOverS.i < QuickModeGOverS.botones.size;
			QuickModeGOverS.i++)
	{
		Boton_draw(
				(Boton*) Lista_GetIn(&QuickModeGOverS.botones,
						QuickModeGOverS.i));
	}

	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_GetIn(&QuickModeGOverS.textura, 1),
			QuickModeGOverS.positionResultados, GameFramework_Color(1, 1, 1, 1),
			0, GameFramework_Vector2(0, 0), GameFramework_Vector2(1.2, 1.3));

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "Score: ",
					GameFramework_EnteroACadena((*Nave_getSObject()).Puntos)),
			QuickModeGOverS.positionPuntos,
			GameFramework_Color(GameFramework_getByteColorToFloatColor(244),
					GameFramework_getByteColorToFloatColor(232),
					GameFramework_getByteColorToFloatColor(16), 1), 1.3);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "Destroyed: ",
					GameFramework_EnteroACadena(
							(*Nave_getSObject()).Destruidos)),
			QuickModeGOverS.positionDestruidos,
			GameFramework_Color(GameFramework_getByteColorToFloatColor(244),
					GameFramework_getByteColorToFloatColor(232),
					GameFramework_getByteColorToFloatColor(16), 1), 1.3);

}

//******************************************

//******TITULO***********
void QuickModeGOver_loadTitulo()
{
	QuickModeGOverS.positionTitulo = GameFramework_Vector2(300, 810);
	QuickModeGOverS.origenTitulo = GameFramework_Vector2(
			(*(TexturaObjeto*) Lista_GetIn(&QuickModeGOverS.textura, 0)).ancho
					/ 2,
			(*(TexturaObjeto*) Lista_GetIn(&QuickModeGOverS.textura, 0)).alto
					/ 2);
}

void QuickModeGOver_drawTitulo()
{
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_GetIn(&QuickModeGOverS.textura, 0),
			QuickModeGOverS.positionTitulo, GameFramework_Color(1, 1, 1, 1), 0,
			QuickModeGOverS.origenTitulo, GameFramework_Vector2(1.1, 1.1));
}

//*********Texturas***************
void QuickModeGOver_setTexturas()
{
	Lista_Inicia(&QuickModeGOverS.textura);
	Lista_AddInFin(&QuickModeGOverS.textura,
			Textura_getTextura("TituloGameOver")); //0
	Lista_AddInFin(&QuickModeGOverS.textura, Textura_getTextura("TResultados")); //1
}

QuickModeGOver* QuickModeGOver_getSObject()
{
	return &QuickModeGOverS;
}
