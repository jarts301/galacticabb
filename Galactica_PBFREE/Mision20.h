/*
 * Mision20.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION20_H_
#define MISION20_H_

#include "GalacticaTipos.h"

void Mision20_reiniciar();
void Mision20_load();
void Mision20_update();
void Mision20_draw();
void Mision20_drawEstadNave();
void Mision20_gameOver();
void Mision20_updateJefes();
void Mision20_drawJefes();
void Mision20_drawMensajeInicial();
void Mision20_reiniciaJefe();
Mision20* Mision20_getSObject();

#endif /* MISION20_H_ */
