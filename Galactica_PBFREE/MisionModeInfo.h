/*
 * MisionModeInfo.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISIONMODEINFO_H_
#define MISIONMODEINFO_H_

#include "GalacticaTipos.h"

void MisionModeInfo_load();
void MisionModeInfo_update();
void MisionModeInfo_draw();
void MisionModeInfo_loadTitulo();
void MisionModeInfo_drawTitulo();
void MisionModeInfo_setTexturas();
MisionModeInfo* MisionModeInfo_getSObject();

#endif /* MISIONMODEINFO_H_ */
