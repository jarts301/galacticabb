/*
 * Mision3.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "Mision3.h"
#include "QuickModeGame.h"
#include "Lista.h"
#include "Boton.h"
#include "Nave.h"
#include "Enemigo.h"
#include "BarraVida.h"
#include "Audio.h"
#include "MisionModeGOver.h"
#include "MisionMode.h"
#include "Almacenamiento.h"
#include "Animation.h"
#include "Dibujar.h"
#include "Textura.h"
#include "Control.h"
#include "Collition.h"
#include "NMini.h"
#include "Mision1.h"
#include "Cadena.h"

static Mision3 Mision3S;

void Mision3_reiniciar()
{
	QuickModeGame_eliminarTodos();
	QuickModeGame_eliminaAnimaciones();
	Mision3S.positionMiniS.X = 6;
	Mision3S.positionMiniS.Y = 900;
	Mision3S.positionTime.X = 420;
	Mision3S.positionTime.Y = 900;
	Lista_Destruir(&((*QuickModeGame_getSObject()).igb.botones));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BPause, 5, 950));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BAudio, 530, 950));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BPower, 480, 20));

	(*QuickModeGame_getSObject()).igb.velocidadEnem = 2;
	(*QuickModeGame_getSObject()).igb.velocidadEnemY = 2;
	(*QuickModeGame_getSObject()).igb.velocidadHiEnem = 3;
	(*QuickModeGame_getSObject()).igb.velocidadHiEnemY = 3;
	(*QuickModeGame_getSObject()).igb.maxEnemigos = 3;
	Mision3S.cuentaTiempo = 60;
	(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo = 0;
	Mision3S.ultimoTMinuto = 0;
	(*QuickModeGame_getSObject()).igb.ultimoAumento = 0;
	NMini_reiniciar((Mision3S.mini));
	(*QuickModeGame_getSObject()).igb.EscalaMensaje =
			(*QuickModeGame_getSObject()).igb.escalaInicialMensaje = 1.5f;
	(*QuickModeGame_getSObject()).igb.actualFrameMensaje = 0;
	(*QuickModeGame_getSObject()).igb.tiempoTitileoMensaje = 0;
	(*QuickModeGame_getSObject()).igb.mensajeInicialActivo = 1;
}

void Mision3_load()
{
	//All static
	Mision3S.tituloMision = "Mission 3";

	Lista_Inicia(&Mision3S.mensajeMision);
	Lista_AddInFin(&Mision3S.mensajeMision,
			String_creaString("Protects the small"));
	Lista_AddInFin(&Mision3S.mensajeMision,
			String_creaString("spaceship of enemies,"));
	Lista_AddInFin(&Mision3S.mensajeMision,
			String_creaString("and avoid that them "));
	Lista_AddInFin(&Mision3S.mensajeMision,
			String_creaString("destroy it, until the "));
	Lista_AddInFin(&Mision3S.mensajeMision, String_creaString("time finish."));
	Lista_AddInFin(&Mision3S.mensajeMision,
			String_creaString("Tip:Do not let the"));
	Lista_AddInFin(&Mision3S.mensajeMision,
			String_creaString("enemy get too close."));

	Lista_Inicia(&Mision3S.recompensa);
	Lista_AddInFin(&Mision3S.recompensa,
			String_creaString("Decreases the waiting"));
	Lista_AddInFin(&Mision3S.recompensa,
			String_creaString("time to activate the"));
	Lista_AddInFin(&Mision3S.recompensa,
			String_creaString("power,in Quick Game."));

	Lista_Inicia(&Mision3S.noRecompensa);
	Lista_AddInFin(&Mision3S.noRecompensa, String_creaString("No Reward.."));

	Mision3S.cuentaTiempo = 60;
	Mision3S.mini = NMini_Crear();
	Mision3S.barraVidaMini = BarraVida_Crear(GameFramework_Vector2(6, 890),
			BVmini);
}

void Mision3_update()
{

	Nave_update();
	QuickModeGame_updateEstrellas();
	Mision3_updateGame();
	QuickModeGame_updateEnemigos();
	QuickModeGame_updateAnimaciones();
	NMini_update(Mision3S.mini);
	for (Mision3S.i = 0;
			Mision3S.i < (*QuickModeGame_getSObject()).igb.botones.size;
			Mision3S.i++)
	{
		Boton_update(
				(Boton*) Lista_GetIn(&(*QuickModeGame_getSObject()).igb.botones,
						Mision3S.i));
	}

	for (Mision3S.i = 0;
			Mision3S.i < (*QuickModeGame_getSObject()).igb.enemigos.size;
			Mision3S.i++)
	{
		for (Mision3S.j = 0;
				Mision3S.j
						< (*(Enemigo*) Lista_GetIn(
								&(*QuickModeGame_getSObject()).igb.enemigos,
								Mision3S.i)).disparosSid.size; Mision3S.j++)
		{
			if (Collition_colisionDispAMini(
					&(*QuickModeGame_getSObject()).igb.colision, Mision3S.mini,
					(DisparoEnemigo*) Lista_GetIn(
							&(*(Enemigo*) Lista_GetIn(
									&(*QuickModeGame_getSObject()).igb.enemigos,
									Mision3S.i)).disparosSid, Mision3S.j)) == 1
					&& (*(Enemigo*) Lista_GetIn(
							&(*QuickModeGame_getSObject()).igb.enemigos,
							Mision3S.i)).tipoEnemigo == ESid)
			{
				(*(DisparoEnemigo*) Lista_GetIn(
						&(*(Enemigo*) Lista_GetIn(
								&(*QuickModeGame_getSObject()).igb.enemigos,
								Mision3S.i)).disparosSid, Mision3S.j)).estaEnColision =
						0;

				(*Mision3S.mini).vida = (*Mision3S.mini).vida - 3;

				Lista_AddInFin(&(*Enemigo_getSObject()).disparosSidTotal,
						Lista_GetIn(
								&(*(Enemigo*) Lista_GetIn(
										&(*QuickModeGame_getSObject()).igb.enemigos,
										Mision3S.i)).disparosSid, Mision3S.j));

				Lista_RemoveAt(
						&(*(Enemigo*) Lista_GetIn(
								&(*QuickModeGame_getSObject()).igb.enemigos,
								Mision3S.i)).disparosSid, Mision3S.j);
			}
		}

	}

	for (Mision3S.i = 0;
			Mision3S.i < (*QuickModeGame_getSObject()).igb.enemigos.size;
			Mision3S.i++)
	{
		if (Collition_colisionMiniAEnemigo(
				&(*QuickModeGame_getSObject()).igb.colision, Mision3S.mini,
				(Enemigo*) Lista_GetIn(
						&(*QuickModeGame_getSObject()).igb.enemigos,
						Mision3S.i)) == 1)
		{
			(*Mision3S.mini).vida = (*Mision3S.mini).vida - 20;
			(*(Enemigo*) Lista_GetIn(
					&(*QuickModeGame_getSObject()).igb.enemigos, Mision3S.i)).estaEnColisionNave =
					1;
			(*(Enemigo*) Lista_GetIn(
					&(*QuickModeGame_getSObject()).igb.enemigos, Mision3S.i)).vida =
					0;
		}
	}

	//Cuenta de tiempo
	if (GameFramework_getTotalTime() > Mision3S.ultimoTMinuto + 1000000)
	{
		Mision3S.cuentaTiempo = Mision3S.cuentaTiempo - 1;
		Mision3S.ultimoTMinuto = GameFramework_getTotalTime();
	}

	QuickModeGame_updateMensajeInicial();
	BarraVida_update((*QuickModeGame_getSObject()).igb.barraVidaNave,
			(*Nave_getSObject()).Vida, (*Nave_getSObject()).MaxVida);
	BarraVida_update(Mision3S.barraVidaMini, (*Mision3S.mini).vida,
			(*Mision3S.mini).maxVida);
	if ((*Nave_getSObject()).Vida <= 0 || (*Mision3S.mini).vida <= 0)
	{
		Animation_update((*Animation_getSObject()).animacionesEstallidoNave);
	}
	Mision3_gameOver();

	if ((*Control_getSObject()).controlActivo == 1)
	{
		Control_update((*QuickModeGame_getSObject()).igb.control);
	}

}

void Mision3_draw()
{
	//CLEAR NEGRO
	Dibujar_drawSprite(Textura_getTexturaFondo(0), GameFramework_Vector2(0, 0),
			GameFramework_Color(0, 0, 0, 1), 0, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(40, 40));

	Nave_draw();
	QuickModeGame_drawEstrellas();
	QuickModeGame_drawEnemigos();
	Mision3_drawEstadNave();
	QuickModeGame_drawAnimaciones();
	Mision3_drawMensajeInicial();
	NMini_draw(Mision3S.mini);
	if ((*Nave_getSObject()).Vida <= 0 || (*Mision3S.mini).vida <= 0)
	{
		Animation_draw((*Animation_getSObject()).animacionesEstallidoNave);
	}

	for (Mision3S.i = 0;
			Mision3S.i < (*QuickModeGame_getSObject()).igb.botones.size;
			Mision3S.i++)
	{
		Boton_draw(
				(Boton*) Lista_GetIn(&(*QuickModeGame_getSObject()).igb.botones,
						Mision3S.i));
	}
	if ((*Control_getSObject()).controlActivo == 1)
	{
		Control_draw((*QuickModeGame_getSObject()).igb.control);
	}
	//drawEstadisticas(sb);
}

//******************Estadisticas de la nave*************
void Mision3_drawEstadNave()
{
	//** Vida de la nave
	Dibujar_drawSprite(Textura_getTexturaFondo(1),
			GameFramework_Vector2(0, 945), GameFramework_Color(0, 0, 0, 1), 0,
			GameFramework_Vector2(0, 0), GameFramework_Vector2(50, 2.6));

	BarraVida_draw((*QuickModeGame_getSObject()).igb.barraVidaNave);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_EnteroACadena((*Nave_getSObject()).Vida),
			GameFramework_Vector2(100, 975), GameFramework_Color(1, 1, 1, 1),
			0.8);

	Dibujar_drawText((*Textura_getSObject()).Fuente, "/",
			GameFramework_Vector2(285, 965), GameFramework_Color(1, 1, 1, 1),
			1.4);

	Nave_drawAnimacionesPoder();

	//Tiempo restante
	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "Time:",
					GameFramework_EnteroACadena(Mision3S.cuentaTiempo)),
			GameFramework_Vector2(420, 900),
			GameFramework_Color(GameFramework_getByteColorToFloatColor(244),
					GameFramework_getByteColorToFloatColor(232),
					GameFramework_getByteColorToFloatColor(16), 1), 0.9);

	BarraVida_draw(Mision3S.barraVidaMini);

	Dibujar_drawText((*Textura_getSObject()).Fuente, "Mini S",
			Mision3S.positionMiniS, GameFramework_Color(1, 1, 1, 1), 0.75);

}

void Mision3_updateGame()
{
	if (Mision3S.cuentaTiempo == 20
			&& (*QuickModeGame_getSObject()).igb.maxEnemigos <= 3)
	{
		(*QuickModeGame_getSObject()).igb.maxEnemigos =
				(*QuickModeGame_getSObject()).igb.maxEnemigos + 2;
	}

	if ((*QuickModeGame_getSObject()).igb.enemigos.size
			< (*QuickModeGame_getSObject()).igb.maxEnemigos)
	{
		Mision3_insertaEnemigo();
	}
}

void Mision3_gameOver()
{

	if (Mision3S.cuentaTiempo <= 0)
	{
		if ((*Audio_getSObject()).musicaActiva == 1)
		{
			Audio_stopAudio("Galactic");
			Audio_playAudio("GameOver");
		}
		(*MisionModeGOver_getSObject()).gana = 1;

		Almacenamiento_cargarMisiones();
		if (Almacenamiento_getPremiosDados() == 2)
		{
			Almacenamiento_salvarBonus(6000000, ALtEsperaPoder);
			(*MisionModeGOver_getSObject()).cosasGanadas = &Mision3S.recompensa;
			Almacenamiento_salvarMisiones();
		}
		else
		{
			(*MisionModeGOver_getSObject()).cosasGanadas =
					&(*Mision1_getSObject()).yaRecompensa;
		}

		(*MisionMode_getSObject()).estadoActual = MMGameOver;
	}

	if ((*Nave_getSObject()).Vida <= 0)
	{
		(*Nave_getSObject()).Vida = 0;
		(*Nave_getSObject()).EscalaN = GameFramework_Vector2(0, 0);
		Animation_setPosition(
				(*Animation_getSObject()).animacionesEstallidoNave,
				(*Nave_getSObject()).PositionNav);
		(*MisionModeGOver_getSObject()).gana = 0;
		(*MisionModeGOver_getSObject()).cosasGanadas = &Mision3S.noRecompensa;
		if ((*(*Animation_getSObject()).animacionesEstallidoNave).animacionFinalizada
				== 1)
		{
			if ((*Audio_getSObject()).musicaActiva == 1)
			{
				Audio_stopAudio("Galactic");
				Audio_playAudio("GameOver");
			}
			(*MisionMode_getSObject()).estadoActual = MMGameOver;
		}
	}
	else if ((*Mision3S.mini).vida <= 0)
	{
		(*Mision3S.mini).vida = 0;
		(*Mision3S.mini).Escala = GameFramework_Vector2(0, 0);
		Animation_setPosition(
				(*Animation_getSObject()).animacionesEstallidoNave,
				(*Mision3S.mini).Position);
		(*MisionModeGOver_getSObject()).gana = 0;
		(*MisionModeGOver_getSObject()).cosasGanadas = &Mision3S.noRecompensa;
		if ((*(*Animation_getSObject()).animacionesEstallidoNave).animacionFinalizada
				== 1)
		{
			if ((*Audio_getSObject()).musicaActiva == 1)
			{
				Audio_stopAudio("Galactic");
				Audio_playAudio("GameOver");
			}
			(*MisionMode_getSObject()).estadoActual = MMGameOver;
		}
	}

}

//********Dibujo Mensaje Inicial
void Mision3_drawMensajeInicial()
{
	if ((*QuickModeGame_getSObject()).igb.mensajeInicialActivo == 1)
	{
		Dibujar_drawText((*Textura_getSObject()).Fuente,
				GameFramework_Concatenar(2, Mision3S.tituloMision, " Start!!"),
				GameFramework_Vector2(50, 620),
				GameFramework_Color(GameFramework_getByteColorToFloatColor(7),
						GameFramework_getByteColorToFloatColor(237),
						GameFramework_getByteColorToFloatColor(134), 1),
				(*QuickModeGame_getSObject()).igb.EscalaMensaje);
	}
}

void Mision3_insertaEnemigo()
{
	(*QuickModeGame_getSObject()).igb.num = 0;
	(*QuickModeGame_getSObject()).igb.num2 = 0;
	(*QuickModeGame_getSObject()).igb.num = GameFramework_Rand(1, 2);

	if ((*QuickModeGame_getSObject()).igb.num == 1)
	{
		(*QuickModeGame_getSObject()).igb.num2 = GameFramework_Rand(1, 3);
		if ((*QuickModeGame_getSObject()).igb.num2 == 1)
		{
			if ((*Enemigo_getSObject()).KamiTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).KamiTotal),
						(*Enemigo_getSObject()).KamiTotal.size - 1);
			}
		}
		if ((*QuickModeGame_getSObject()).igb.num2 == 2)
		{
			if ((*Enemigo_getSObject()).KamiTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).KamiTotal),
						(*Enemigo_getSObject()).KamiTotal.size - 1);
			}
		}
	}
	if ((*QuickModeGame_getSObject()).igb.num == 2)
	{
		(*QuickModeGame_getSObject()).igb.num2 = GameFramework_Rand(1, 3);
		if ((*QuickModeGame_getSObject()).igb.num2 == 1)
		{
			if ((*Enemigo_getSObject()).SidTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem,
						(*QuickModeGame_getSObject()).igb.velocidadEnemY,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).SidTotal),
						(*Enemigo_getSObject()).SidTotal.size - 1);
			}
		}
		if ((*QuickModeGame_getSObject()).igb.num2 == 2)
		{
			if ((*Enemigo_getSObject()).SidTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem,
						(*QuickModeGame_getSObject()).igb.velocidadEnemY,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).SidTotal),
						(*Enemigo_getSObject()).SidTotal.size - 1);
			}
		}
	}

}

Mision3* Mision3_getSObject()
{
	return &Mision3S;
}
