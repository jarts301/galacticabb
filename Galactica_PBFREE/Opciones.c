/*
 * Opciones.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "Opciones.h"
#include "Lista.h"
#include "Textura.h"
#include "Boton.h"
#include "Dibujar.h"
#include "Control.h"
#include "Nave.h"
#include "GameComun.h"

static Opciones OpcionesS;

void Opciones_load()
{
	Opciones_setTexturas();
	Opciones_loadTitulo();
	Lista_Inicia(&OpcionesS.botones);
	Lista_AddInFin(&OpcionesS.botones, Boton_Crear(BBack, 10, 10));
	Lista_AddInFin(&OpcionesS.botones, Boton_Crear(BAudio, 260, 730));
	Lista_AddInFin(&OpcionesS.botones, Boton_Crear(BTilting, 40, 510));
	Lista_AddInFin(&OpcionesS.botones, Boton_Crear(BButtons, 320, 510));
	Lista_AddInFin(&OpcionesS.botones, Boton_Crear(BMedium, 40, 280));
	Lista_AddInFin(&OpcionesS.botones, Boton_Crear(BLarge, 320, 280));
}

void Opciones_update()
{
	GameComun_updateEstrellas();
	for (OpcionesS.i = 0; OpcionesS.i < OpcionesS.botones.size - 2;
			OpcionesS.i++)
	{
		Boton_update((Boton*) Lista_GetIn(&OpcionesS.botones, OpcionesS.i));
	}
	if ((*Control_getSObject()).controlActivo == 1)
	{
		Boton_update((Boton*) Lista_GetIn(&OpcionesS.botones, 4));
		Boton_update((Boton*) Lista_GetIn(&OpcionesS.botones, 5));
	}
	(*(Boton*) Lista_GetIn(&OpcionesS.botones, 1)).escala = 2.0f;
	(*Boton_getSObject()).ultimaActualizacion = GameFramework_getTotalTime();
	(*Nave_getSObject()).ultimaActualizacion = GameFramework_getTotalTime();
}

void Opciones_draw()
{
	//CLEAR NEGRO
	Dibujar_drawSprite(Textura_getTexturaFondo(0), GameFramework_Vector2(0, 0),
			GameFramework_Color(0, 0, 0, 1), 0, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(100, 100));

	GameComun_drawEstrellas();
	Opciones_drawTitulo();
	for (OpcionesS.i = 0; OpcionesS.i < OpcionesS.botones.size - 2;
			OpcionesS.i++)
	{
		Boton_draw((Boton*) Lista_GetIn(&OpcionesS.botones, OpcionesS.i));
	}
	if ((*Control_getSObject()).controlActivo == 1)
	{
		Boton_draw((Boton*) Lista_GetIn(&OpcionesS.botones, 4));
		Boton_draw((Boton*) Lista_GetIn(&OpcionesS.botones, 5));
	}
}

//*****************************************

//******TITULO**********
void Opciones_loadTitulo()
{
	OpcionesS.positionTitulo = GameFramework_Vector2(300, 930);
	OpcionesS.origenTitulo = GameFramework_Vector2(
			(*(TexturaObjeto*) Lista_GetIn(&OpcionesS.textura, 0)).ancho / 2,
			(*(TexturaObjeto*) Lista_GetIn(&OpcionesS.textura, 0)).alto / 2);
}

void Opciones_drawTitulo()
{
	Dibujar_drawSprite((TexturaObjeto*) Lista_GetIn(&OpcionesS.textura, 0),
			GameFramework_Vector2(300, 930),
			GameFramework_Color(1, 1, 1, 1), 0, OpcionesS.origenTitulo,
			GameFramework_Vector2(1.3, 1.2));

	Dibujar_drawSprite((TexturaObjeto*) Lista_GetIn(&OpcionesS.textura, 1),
			GameFramework_Vector2(80, 830), GameFramework_Color(1, 1, 1, 1), 0,
			GameFramework_Vector2(0, 0), GameFramework_Vector2(0.9, 0.9));

	Dibujar_drawSprite((TexturaObjeto*) Lista_GetIn(&OpcionesS.textura, 4),
			GameFramework_Vector2(45, 680), GameFramework_Color(1, 1, 1, 1), 0,
			GameFramework_Vector2(0, 0), GameFramework_Vector2(1, 1));

	Dibujar_drawSprite((TexturaObjeto*) Lista_GetIn(&OpcionesS.textura, 2),
			GameFramework_Vector2(40, 640), GameFramework_Color(1, 1, 1, 1), 0,
			GameFramework_Vector2(0, 0), GameFramework_Vector2(1, 1));

	Dibujar_drawSprite((TexturaObjeto*) Lista_GetIn(&OpcionesS.textura, 4),
			GameFramework_Vector2(45, 460), GameFramework_Color(1, 1, 1, 1), 0,
			GameFramework_Vector2(0, 0), GameFramework_Vector2(1, 1));

	if ((*Control_getSObject()).controlActivo == 1)
	{
		Dibujar_drawSprite((TexturaObjeto*) Lista_GetIn(&OpcionesS.textura, 3),
				GameFramework_Vector2(60, 410), GameFramework_Color(1, 1, 1, 1),
				0, GameFramework_Vector2(0, 0), GameFramework_Vector2(0.9, 1));
	}
}

//*********Texturas***************
void Opciones_setTexturas()
{
	Lista_Inicia(&OpcionesS.textura);
	Lista_AddInFin(&OpcionesS.textura, Textura_getTextura("TituloOpciones")); //0
	Lista_AddInFin(&OpcionesS.textura,
			Textura_getTextura("TituloSonidosMusica")); //1
	Lista_AddInFin(&OpcionesS.textura, Textura_getTextura("TituloControlMode")); //2
	Lista_AddInFin(&OpcionesS.textura, Textura_getTextura("TituloButtonSize")); //3
	Lista_AddInFin(&OpcionesS.textura, Textura_getTextura("Separador")); //4
}

Opciones* Opciones_getSObject()
{
	return &OpcionesS;
}


