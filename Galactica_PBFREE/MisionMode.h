/*
 * MisionMode.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISIONMODE_H_
#define MISIONMODE_H_

#include "GalacticaTipos.h"

void MisionMode_load();
void MisionMode_update();
void MisionMode_draw();
MisionMode* MisionMode_getSObject();

#endif /* MISIONMODE_H_ */
