/*
 * GameFramework.c
 *
 *  Created on: 31/01/2013
 *      Author: Jarts
 */

#include "GameFramework.h"
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

static GameTime gameTime;
static double auxSwapTime = 0;
struct timespec tp1;
char bufer[100];
char concatenacion[500];
int cont = 0;

void GameFramework_setStartTime()
{
	clock_gettime(CLOCK_REALTIME, &tp1);
	gameTime.startTime = tp1.tv_sec;
	srand(time(0));
}

void GameFramework_updateTotalTime()
{
	clock_gettime(CLOCK_REALTIME, &tp1);
	gameTime.totalTime = ((tp1.tv_sec - gameTime.startTime) * 1000000)
			+ (tp1.tv_nsec / 1000); //microsegundos
	//1 milisegundo = 1000 microsegundos
}

void GameFramework_setSwapTime()
{
	gameTime.swapTime = gameTime.totalTime - auxSwapTime;
	auxSwapTime = gameTime.totalTime;
}

double GameFramework_getTotalTime()
{
	return gameTime.totalTime;
}

double GameFramework_getSwapTime()
{
	return gameTime.swapTime;
}

Color GameFramework_Color(float r, float g, float b, float a)
{
	Color nuevo;

	nuevo.R = r;
	nuevo.G = g;
	nuevo.B = b;
	nuevo.A = a;

	return nuevo;
}

void GameFramework_ColorTo(Color* color, float r, float g, float b, float a)
{
	(*color).R = r;
	(*color).G = g;
	(*color).B = b;
	(*color).A = a;
}

int* GameFramework_creaInteger(int numero)
{
	int* nuevo;
	if ((nuevo = (int*) malloc(sizeof(int))) == NULL)
		return NULL;

	(*nuevo) = numero;

	return nuevo;
}

float GameFramework_getByteColorToFloatColor(int bytes)
{
	float r;
	r = ((float) bytes) / 255.0f;

	return r;
}

int GameFramework_Rand(int min, int max)
{
	int r = min + rand() % ((max + 1) - min);
	return r;
}

double GameFramework_Rand0A1()
{
	double r = ((float) (rand() % 101)) / 100.0f;
	return r;
}

double GameFramework_ToRadians(double dat)
{
	double r = (dat * PI) / 180;
	return r;
}

double GameFramework_ToDegrees(double dat)
{
	double r = (dat * 180) / PI;
	return r;
}

Vector2 GameFramework_Vector2(float x, float y)
{
	Vector2 nuevo;
	nuevo.X = x;
	nuevo.Y = y;

	return nuevo;
}

Vector2* GameFramework_Vector2Crear(float x, float y)
{
	Vector2* nuevo;
	if ((nuevo = (Vector2*) malloc(sizeof(Vector2))) == NULL)
		return NULL;

	(*nuevo).X = x;
	(*nuevo).Y = y;

	return nuevo;
}

char* GameFramework_EnteroACadena(int i)
{
	memset(bufer, '\0', 100);
	sprintf(bufer, "%d", i);
	return bufer;
}

char* GameFramework_Concatenar(int numCads, ...)
{
	memset(concatenacion, '\0', 500);
	va_list ap;
	va_start(ap, numCads);
	for (cont = 0; cont < numCads; cont++)
	{
		strcat(concatenacion, va_arg(ap,char*));
	}
	va_end(ap);
	return concatenacion;
}

int GameFramework_Vector2IgualA(Vector2 vectorA, Vector2 vectorB)
{
	if (vectorA.X == vectorB.X && vectorA.Y == vectorB.Y)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

