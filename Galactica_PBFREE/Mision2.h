/*
 * Mision2.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION2_H_
#define MISION2_H_

#include "GalacticaTipos.h"

void Mision2_reiniciar();
void Mision2_load();
void Mision2_update();
void Mision2_draw();
void Mision2_drawEstadNave();
void Mision2_updateGame();
void Mision2_gameOver();
void Mision2_drawMensajeInicial();
void Mision2_insertaEnemigo();
Mision2* Mision2_getSObject();

#endif /* MISION2_H_ */
