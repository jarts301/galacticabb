/*
 * Textura.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "Textura.h"
#include "Lista.h"
#include "Cadena.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "bbutil.h"

static Textura TexturaS;

void Textura_Inicia()
{
	Lista_Inicia(&(TexturaS.Texturas));
	Lista_Inicia(&(TexturaS.TexturasNom));
	Lista_Inicia(&(TexturaS.TexturasFondo));
}

int Textura_cargaIntro()
{
	if ((TexturaS.intro = (TexturaObjeto*) malloc(sizeof(TexturaObjeto))) == NULL)
		return 0;

	(*TexturaS.intro).setOrigen=1;

	if ((bbutil_load_texture("app/native/Sprites/Intro.png", &((*TexturaS.intro).ancho), &((*TexturaS.intro).alto),
			&((*TexturaS.intro).texx), &((*TexturaS.intro).texy), &((*TexturaS.intro).textura)))
			== 0)
	{
		return 0;
	}

	(*TexturaS.intro).setOrigen=1;

	return 1;
}

int Textura_addTextura(char* direccion, char *nombre)
{

	TexturaObjeto *nuevo;
	if ((nuevo = (TexturaObjeto*) malloc(sizeof(TexturaObjeto))) == NULL)
		return EXIT_FAILURE;

	if ((bbutil_load_texture(direccion, &((*nuevo).ancho), &((*nuevo).alto),
			&((*nuevo).texx), &((*nuevo).texy), &((*nuevo).textura)))
			== EXIT_FAILURE)
	{
		return EXIT_FAILURE;
	}

	(*nuevo).setOrigen = 1;

	/*(*nuevo).texCoord[0] = 0.0f;
	 (*nuevo).texCoord[1] = 1.0f;
	 (*nuevo).texCoord[2] = 1.0f;
	 (*nuevo).texCoord[3] = 1.0f;
	 (*nuevo).texCoord[4] = 0.0f;
	 (*nuevo).texCoord[5] = 0.0f;
	 (*nuevo).texCoord[6] = 1.0f;
	 (*nuevo).texCoord[7] = 0.0f;*/

	if (strcmp(nombre, "BAudio") == 0)
	{
		(*nuevo).cuadCoord[0] = 0.0f;
		(*nuevo).cuadCoord[1] = 64;
		(*nuevo).cuadCoord[2] = 64;
		(*nuevo).cuadCoord[3] = 64;
		(*nuevo).cuadCoord[4] = 0.0f;
		(*nuevo).cuadCoord[5] = 0.0f;
		(*nuevo).cuadCoord[6] = 64;
		(*nuevo).cuadCoord[7] = 0.0f;

		(*nuevo).setOrigen = 0;
	}

	if (strcmp(nombre, "animationPoder") == 0)
	{
		(*nuevo).cuadCoord[0] = 0.0f;
		(*nuevo).cuadCoord[1] = 1024;
		(*nuevo).cuadCoord[2] = 512;
		(*nuevo).cuadCoord[3] = 1024;
		(*nuevo).cuadCoord[4] = 0.0f;
		(*nuevo).cuadCoord[5] = 0.0f;
		(*nuevo).cuadCoord[6] = 512;
		(*nuevo).cuadCoord[7] = 0.0f;

		(*nuevo).setOrigen = 0;
	}

	if (strcmp(nombre, "EstallidoEnemigo") == 0)
	{
		(*nuevo).cuadCoord[0] = -64;
		(*nuevo).cuadCoord[1] = 64;
		(*nuevo).cuadCoord[2] = 64;
		(*nuevo).cuadCoord[3] = 64;
		(*nuevo).cuadCoord[4] = -64;
		(*nuevo).cuadCoord[5] = -64;
		(*nuevo).cuadCoord[6] = 64;
		(*nuevo).cuadCoord[7] = -64;

		(*nuevo).setOrigen = 0;
	}

	if (strcmp(nombre, "BMision") == 0)
	{
		(*nuevo).cuadCoord[0] = 0;
		(*nuevo).cuadCoord[1] = 128;
		(*nuevo).cuadCoord[2] = 128;
		(*nuevo).cuadCoord[3] = 128;
		(*nuevo).cuadCoord[4] = 0;
		(*nuevo).cuadCoord[5] = 0;
		(*nuevo).cuadCoord[6] = 128;
		(*nuevo).cuadCoord[7] = 0;

		(*nuevo).setOrigen = 0;
	}

	if (strcmp(nombre, "BMedium") == 0)
	{
		(*nuevo).cuadCoord[0] = 0;
		(*nuevo).cuadCoord[1] = 128;
		(*nuevo).cuadCoord[2] = 256;
		(*nuevo).cuadCoord[3] = 128;
		(*nuevo).cuadCoord[4] = 0;
		(*nuevo).cuadCoord[5] = 0;
		(*nuevo).cuadCoord[6] = 256;
		(*nuevo).cuadCoord[7] = 0;

		(*nuevo).setOrigen = 0;
	}

	if (strcmp(nombre, "TituloWin") == 0)
	{
		(*nuevo).cuadCoord[0] = -256;
		(*nuevo).cuadCoord[1] = 32;
		(*nuevo).cuadCoord[2] = 256;
		(*nuevo).cuadCoord[3] = 32;
		(*nuevo).cuadCoord[4] = -256;
		(*nuevo).cuadCoord[5] = -32;
		(*nuevo).cuadCoord[6] = 256;
		(*nuevo).cuadCoord[7] = -32;

		(*nuevo).setOrigen = 0;
	}

	if (strcmp(nombre, "NGalactica") == 0)
	{
		(*nuevo).cuadCoord[0] = -170.666;
		(*nuevo).cuadCoord[1] = 128;
		(*nuevo).cuadCoord[2] = 170.666;
		(*nuevo).cuadCoord[3] = 128;
		(*nuevo).cuadCoord[4] = -170.666;
		(*nuevo).cuadCoord[5] = -128;
		(*nuevo).cuadCoord[6] = 170.666;
		(*nuevo).cuadCoord[7] = -128;

		(*nuevo).setOrigen = 0;
	}
	if (strcmp(nombre, "NCarafe") == 0)
	{
		(*nuevo).cuadCoord[0] = -170.666;
		(*nuevo).cuadCoord[1] = 128;
		(*nuevo).cuadCoord[2] = 170.666;
		(*nuevo).cuadCoord[3] = 128;
		(*nuevo).cuadCoord[4] = -170.666;
		(*nuevo).cuadCoord[5] = -128;
		(*nuevo).cuadCoord[6] = 170.666;
		(*nuevo).cuadCoord[7] = -128;

		(*nuevo).setOrigen = 0;
	}

	if (strcmp(nombre, "NHelix") == 0)
	{
		(*nuevo).cuadCoord[0] = -170.666;
		(*nuevo).cuadCoord[1] = 128;
		(*nuevo).cuadCoord[2] = 170.666;
		(*nuevo).cuadCoord[3] = 128;
		(*nuevo).cuadCoord[4] = -170.666;
		(*nuevo).cuadCoord[5] = -128;
		(*nuevo).cuadCoord[6] = 170.666;
		(*nuevo).cuadCoord[7] = -128;

		(*nuevo).setOrigen = 0;
	}

	if (strcmp(nombre, "NArp") == 0)
	{
		(*nuevo).cuadCoord[0] = -170.666;
		(*nuevo).cuadCoord[1] = 128;
		(*nuevo).cuadCoord[2] = 170.666;
		(*nuevo).cuadCoord[3] = 128;
		(*nuevo).cuadCoord[4] = -170.666;
		(*nuevo).cuadCoord[5] = -128;
		(*nuevo).cuadCoord[6] = 170.666;
		(*nuevo).cuadCoord[7] = -128;

		(*nuevo).setOrigen = 0;
	}

	if (strcmp(nombre, "NPerseus") == 0)
	{
		(*nuevo).cuadCoord[0] = -170.666;
		(*nuevo).cuadCoord[1] = 128;
		(*nuevo).cuadCoord[2] = 170.666;
		(*nuevo).cuadCoord[3] = 128;
		(*nuevo).cuadCoord[4] = -170.666;
		(*nuevo).cuadCoord[5] = -128;
		(*nuevo).cuadCoord[6] = 170.666;
		(*nuevo).cuadCoord[7] = -128;

		(*nuevo).setOrigen = 0;
	}

	if (strcmp(nombre, "NPisces") == 0)
	{
		(*nuevo).cuadCoord[0] = -170.666;
		(*nuevo).cuadCoord[1] = 128;
		(*nuevo).cuadCoord[2] = 170.666;
		(*nuevo).cuadCoord[3] = 128;
		(*nuevo).cuadCoord[4] = -170.666;
		(*nuevo).cuadCoord[5] = -128;
		(*nuevo).cuadCoord[6] = 170.666;
		(*nuevo).cuadCoord[7] = -128;

		(*nuevo).setOrigen = 0;
	}

	Lista_InsertaEnFinal(&(TexturaS.Texturas), (TexturaS.Texturas).fin, nuevo);
	Lista_InsertaEnFinal(&(TexturaS.TexturasNom), (TexturaS.TexturasNom).fin,
			nombre);

	return EXIT_SUCCESS;
}

int Textura_addTexturaFondo(char* direccion, char *nombre)
{
	TexturaObjeto *nuevo;
	if ((nuevo = (TexturaObjeto*) malloc(sizeof(TexturaObjeto))) == NULL)
		return EXIT_FAILURE;

	if ((bbutil_load_texture(direccion, &((*nuevo).ancho), &((*nuevo).alto),
			&((*nuevo).texx), &((*nuevo).texy), &((*nuevo).textura)))
			== EXIT_FAILURE)
	{
		return EXIT_FAILURE;
	}

	(*nuevo).setOrigen = 1;

	(*nuevo).texCoord[0] = 0.0f;
	(*nuevo).texCoord[1] = 1.0f;
	(*nuevo).texCoord[2] = 1.0f;
	(*nuevo).texCoord[3] = 1.0f;
	(*nuevo).texCoord[4] = 0.0f;
	(*nuevo).texCoord[5] = 0.0f;
	(*nuevo).texCoord[6] = 1.0f;
	(*nuevo).texCoord[7] = 0.0f;

	(*nuevo).cuadCoord[0] = 0.0f;
	(*nuevo).cuadCoord[1] = (*nuevo).alto;
	(*nuevo).cuadCoord[2] = (*nuevo).ancho;
	(*nuevo).cuadCoord[3] = (*nuevo).alto;
	(*nuevo).cuadCoord[4] = 0.0f;
	(*nuevo).cuadCoord[5] = 0.0f;
	(*nuevo).cuadCoord[6] = ((*nuevo).ancho);
	(*nuevo).cuadCoord[7] = 0.0f;

	Lista_InsertaEnFinal(&(TexturaS.TexturasFondo),
			(TexturaS.TexturasFondo).fin, nuevo);

	return EXIT_SUCCESS;
}

TexturaObjeto* Textura_getTextura(char* nombre)
{
	for (TexturaS.i = 0; TexturaS.i < (TexturaS.Texturas).size; TexturaS.i++)
	{
		if (strcmp(
				(char*) Lista_RetornaElemento(&(TexturaS.TexturasNom),
						TexturaS.i), nombre) == 0)
		{
			return (TexturaObjeto*) (Lista_RetornaElemento(&(TexturaS.Texturas),
					TexturaS.i));
		}
	}

	return NULL;
}

TexturaObjeto* Textura_getTexturaIn(int indice)
{
	for (TexturaS.i = 0; TexturaS.i < (TexturaS.Texturas).size; TexturaS.i++)
	{
		if (TexturaS.i == indice)
		{
			return (TexturaObjeto*) (Lista_RetornaElemento(&(TexturaS.Texturas),
					TexturaS.i));
		}
	}
	return NULL;
}

TexturaObjeto* Textura_getTexturaFondo(int indice)
{
	for (TexturaS.i = 0; TexturaS.i < TexturaS.TexturasFondo.size; TexturaS.i++)
	{
		if (TexturaS.i == indice)
		{
			return (TexturaObjeto*) (Lista_RetornaElemento(
					&(TexturaS.TexturasFondo), TexturaS.i));
		}
	}
	return NULL;
}

Textura* Textura_getSObject()
{
	return &TexturaS;
}

