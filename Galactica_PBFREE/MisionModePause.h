/*
 * MisionModePause.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISIONMODEPAUSE_H_
#define MISIONMODEPAUSE_H_

#include "GalacticaTipos.h"

void MisionModePause_load();
void MisionModePause_update();
void MisionModePause_draw();
void MisionModePause_loadTitulo();
void MisionModePause_drawTitulo();
void MisionModePause_setTexturas();
MisionModePause* MisionModePause_getSObject();

#endif /* MISIONMODEPAUSE_H_ */
