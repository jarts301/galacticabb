/*
 * DisparoJefe.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "DisparoJefe.h"
#include "Textura.h"
#include "Collition.h"
#include "Dibujar.h"
#include "Nave.h"

static DisparoJefe DisparoJefeS;

/*public DisparoJefe()
 {
 }*/

DisparoJefe* DisparoJefe_crearObjecto(Vector2 posEnem, DisparoJefe_tipo t)
{
	DisparoJefe* nuevo;
	if ((nuevo = (DisparoJefe*) malloc(sizeof(DisparoJefe))) == NULL)
		return NULL;

	if (t == DJmaximo)
	{
		(*nuevo).Position.X = posEnem.X;
		(*nuevo).Position.Y = posEnem.Y;
		(*nuevo).tipoActual = t;
		(*nuevo).textura = 0;
		(*nuevo).numeroCuadros = 4;
		(*nuevo).Escala = GameFramework_Vector2(0.7f, 0.7f);
		DisparoJefe_defineTrozoAnim(nuevo, 0, 0, 256, 256);
		DisparoJefe_setOrigen(nuevo);
		(*nuevo).radioCollition = (int) (80 * (*nuevo).Escala.X);
		(*nuevo).color = GameFramework_Color(1, 1, 1, 1);
	}
	else if (t == DJnormal)
	{
		(*nuevo).Position.X = posEnem.X;
		(*nuevo).Position.Y = posEnem.Y;
		(*nuevo).tipoActual = t;
		(*nuevo).Disparar = 0;
		(*nuevo).postDisparo = 0;
		DisparoJefe_defineTrozoAnim(nuevo, 0, 0, 256, 256);
		(*nuevo).Tiempo = 0;
		(*nuevo).textura = 1;
		(*nuevo).numeroCuadros = 1;
		(*nuevo).Escala = GameFramework_Vector2(0.2f, 0.2f);
		DisparoJefe_setOrigen(nuevo);
		(*nuevo).radioCollition = (int) (75 * (*nuevo).Escala.X);
		(*nuevo).color = GameFramework_Color(
				GameFramework_getByteColorToFloatColor(112),
				GameFramework_getByteColorToFloatColor(245),
				GameFramework_getByteColorToFloatColor(161), 1);
	}
	(*nuevo).estaEnColision = 0;

	return nuevo;
}

void DisparoJefe_load()
{
	DisparoJefeS.adicionDanoDisNave = 0;
	DisparoJefe_setTextura();
}

void DisparoJefe_update(DisparoJefe* disparoJefe)
{
	if ((*disparoJefe).tipoActual == DJmaximo)
	{
		DisparoJefe_movimDisparoMaximo(disparoJefe);
	}
	else if ((*disparoJefe).tipoActual == DJnormal)
	{
		DisparoJefe_movimDisparoNormal(disparoJefe);
	}
}

void DisparoJefe_draw(DisparoJefe* disparoJefe)
{
	Dibujar_drawSpriteTr(
			(TexturaObjeto*) Lista_RetornaElemento(&(DisparoJefeS.Texturas),
					(*disparoJefe).textura), (*disparoJefe).Position,
			(*disparoJefe).TrozoAnim, (*disparoJefe).color, 0.0f,
			(*disparoJefe).Origen, (*disparoJefe).Escala);

	/*spriteBatch.Draw(Texturas[textura], Position, TrozoAnim, color, 0.0f,
			Origen, Escala, SpriteEffects.None, 0.439f);*/

}

//************************************

void DisparoJefe_movimDisparoMaximo(DisparoJefe* disparoJefe)
{

	DisparoJefe_defineTrozoAnim(disparoJefe, (*disparoJefe).actualframe * 256,
			0, 256, 256);
	//DisparoJefe_setOrigen();

	(*disparoJefe).Position.Y = (*disparoJefe).Position.Y - 10;

	DisparoJefe_velocidadAnimacion(disparoJefe);

	if (Collition_colisionDispJefeANave((*disparoJefe).collition, disparoJefe)
			== 1)
	{
		(*disparoJefe).estaEnColision = 1;
		(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
				- (20 + DisparoJefeS.adicionDanoDisNave);
	}
}

void DisparoJefe_movimDisparoNormal(DisparoJefe* disparoJefe)
{

	if ((*disparoJefe).Disparar == 1)
	{
		(*disparoJefe).Tiempo = 0;
		(*disparoJefe).posNave = (*Nave_getSObject()).PositionNav;
		(*disparoJefe).x = (*disparoJefe).Position.X;
		(*disparoJefe).y = (*disparoJefe).Position.Y;
		(*disparoJefe).velocidadX = (int) (((*disparoJefe).posNave.X
				- (*disparoJefe).x)) * 1 / 45;
		(*disparoJefe).velocidadY = (int) (((*disparoJefe).posNave.Y
				- (*disparoJefe).y)) * 1 / 45;
		(*disparoJefe).postDisparo = 1;
		(*disparoJefe).Disparar = 0;
	}
	if ((*disparoJefe).postDisparo == 1)
	{
		(*disparoJefe).Tiempo++;
		(*disparoJefe).Position.X = (*disparoJefe).x
				+ (*disparoJefe).velocidadX * (*disparoJefe).Tiempo;
		(*disparoJefe).Position.Y = (*disparoJefe).y
				+ (*disparoJefe).velocidadY * (*disparoJefe).Tiempo;
	}

	if (Collition_colisionDispJefeANave((*disparoJefe).collition, disparoJefe))
	{
		(*disparoJefe).estaEnColision = 1;
		(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
				- (4 + DisparoJefeS.adicionDanoDisNave);
	}
}

void DisparoJefe_setPosition(DisparoJefe* disparoJefe, Vector2 p, int Derecha)
{
	if ((*disparoJefe).tipoActual == DJnormal)
	{
		if (Derecha == 1)
		{
			(*disparoJefe).Position.X = p.X - 130;
			(*disparoJefe).Position.Y = p.Y - 160;
		}

		if (Derecha == 0)
		{
			(*disparoJefe).Position.X = p.X + 130;
			(*disparoJefe).Position.Y = p.Y - 160;
		}
	}

}

void DisparoJefe_setPosition2(DisparoJefe* disparoJefe, Vector2 p)
{
	(*disparoJefe).Position.X = p.X;
	(*disparoJefe).Position.Y = p.Y - 200;
}

Vector2 DisparoJefe_getPosition(DisparoJefe* disparoJefe)
{
	return (*disparoJefe).Position;
}

void DisparoJefe_setTextura()
{
	Lista_Inicia(&(DisparoJefeS.Texturas));
	Lista_InsertaEnFinal(&(DisparoJefeS.Texturas), DisparoJefeS.Texturas.fin,
			Textura_getTextura("JefeDisparoMax")); //0
	Lista_InsertaEnFinal(&(DisparoJefeS.Texturas), DisparoJefeS.Texturas.fin,
			Textura_getTextura("JefeDisparoNormal")); //1
}

void DisparoJefe_setOrigen(DisparoJefe* disparoJefe)
{
	(*disparoJefe).Origen.X = (*disparoJefe).TrozoAnim.Width / 2;
	(*disparoJefe).Origen.Y = (*disparoJefe).TrozoAnim.Height / 2;
}

//define el cuandro de la animacion actual
void DisparoJefe_defineTrozoAnim(DisparoJefe* disparoJefe, int x, int y,
		int width, int height)
{
	(*disparoJefe).TrozoAnim.X = x;
	(*disparoJefe).TrozoAnim.Y = y;
	(*disparoJefe).TrozoAnim.Width = width;
	(*disparoJefe).TrozoAnim.Height = height;
}

void DisparoJefe_velocidadAnimacion(DisparoJefe* disparoJefe)
{

	(*disparoJefe).dato += GameFramework_getSwapTime();
	if ((*disparoJefe).dato >= (GameFramework_getSwapTime() * 3))
	{
		(*disparoJefe).actualframe = (*disparoJefe).actualframe + 1;
		(*disparoJefe).dato = 0;
	}

	if ((*disparoJefe).actualframe >= (*disparoJefe).numeroCuadros)
	{
		(*disparoJefe).actualframe = 0;
	}
}

DisparoJefe* DisparoJefe_getSObject()
{
	return &DisparoJefeS;
}

