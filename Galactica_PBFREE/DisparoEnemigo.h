/*
 * DisparoEnemigo.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef DISPAROENEMIGO_H_
#define DISPAROENEMIGO_H_

#include "GalacticaTipos.h"

DisparoEnemigo* DisparoEnemigo_CrearObject(Vector2 posEnem,
		DisparoEnemigo_tipo t, int derecha);
void DisparoEnemigo_load();
void DisparoEnemigo_update(DisparoEnemigo* disparoEnemigo);
void DisparoEnemigo_draw(DisparoEnemigo* disparoEnemigo);
DisparoEnemigo* DisparoEnemigo_getSObject();
void DisparoEnemigo_movimDisparoSid(DisparoEnemigo* disparoEnemigo);
void DisparoEnemigo_movimDisparoNor(DisparoEnemigo* disparoEnemigo);
void DisparoEnemigo_setPosition(DisparoEnemigo* disparoEnemigo,Vector2 p);
Vector2 DisparoEnemigo_getPosition(DisparoEnemigo* disparoEnemigo);
void DisparoEnemigo_setLadoMovimiento(DisparoEnemigo* disparoEnemigo,int derecha);
void DisparoEnemigo_setTextura();
void DisparoEnemigo_setOrigen(DisparoEnemigo* disparoEnemigo, int numTex);
DisparoEnemigo* DisparoEnemigo_getSObject();

#endif /* DISPAROENEMIGO_H_ */
