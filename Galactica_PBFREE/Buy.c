/*
 * Buy.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "Buy.h"
#include "Lista.h"
#include "Textura.h"
#include "Boton.h"
#include "Nave.h"
#include "bbutil.h"
#include "GameComun.h"
#include "GameFramework.h"
#include "Dibujar.h"
#include "Cadena.h"
#include <stdlib.h>

static Buy BuyS;

void Buy_load()
{
	Buy_setTexturas();

	Buy_loadTitulo();

	Lista_Inicia(&BuyS.mensaje);
	Lista_AddInFin(&BuyS.mensaje, String_creaString("Enjoy all the missions,"));
	Lista_AddInFin(&BuyS.mensaje, String_creaString("Rescue all spaceships"));
	Lista_AddInFin(&BuyS.mensaje,
			String_creaString("with its incredible powers,"));
	Lista_AddInFin(&BuyS.mensaje,
			String_creaString("and Do not lose the benefit"));
	Lista_AddInFin(&BuyS.mensaje,
			String_creaString("of overcoming each mission,"));
	Lista_AddInFin(&BuyS.mensaje,
			String_creaString("so you can go a lot further"));
	Lista_AddInFin(&BuyS.mensaje,
			String_creaString("in Quick Game,and get many"));
	Lista_AddInFin(&BuyS.mensaje, String_creaString("more points."));

	Lista_Inicia(&(BuyS.botones));
	Lista_InsertaEnFinal(&(BuyS.botones), BuyS.botones.fin,
			Boton_Crear(BGoBuy, 120, 300));
	Lista_InsertaEnFinal(&(BuyS.botones), BuyS.botones.fin,
			Boton_Crear(BNotNow, 120, 160));
}

void Buy_update()
{
	GameComun_updateEstrellas();
	for (BuyS.i = 0; BuyS.i < BuyS.botones.size; BuyS.i++)
	{
		Boton_update((Boton*) Lista_RetornaElemento(&(BuyS.botones), BuyS.i));
	}
	(*Boton_getSObject()).ultimaActualizacion = GameFramework_getTotalTime();
	(*Nave_getSObject()).ultimaActualizacion = GameFramework_getTotalTime();
}

void Buy_draw()
{
	//CLEAR NEGRO
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_RetornaElemento(&(BuyS.textura), 1),
			GameFramework_Vector2(0, 0), GameFramework_Color(0, 0, 0, 1.0),
			0.0f, GameFramework_Vector2(0, 0), GameFramework_Vector2(100, 100));
	GameComun_drawEstrellas();
	Buy_drawTitulo();
	for (BuyS.i = 0; BuyS.i < BuyS.botones.size; BuyS.i++)
	{
		Boton_draw((Boton*) Lista_RetornaElemento(&(BuyS.botones), BuyS.i));
	}

	for (BuyS.z = 0; BuyS.z < BuyS.mensaje.size; BuyS.z++)
	{
		BuyS.positionInfo = GameFramework_Vector2(8, 730);
		BuyS.positionInfo.Y = BuyS.positionInfo.Y - (30 * (BuyS.z + 1));

		Dibujar_drawText((*Textura_getSObject()).Fuente,
				(char*) Lista_GetIn(&BuyS.mensaje, BuyS.z), BuyS.positionInfo,
				GameFramework_Color(1, 1, 1, 1), 0.9);
	}

	/*for (BuyS.i = 0; BuyS.i < BuyS.mensaje.size; BuyS.i++)
	 {
	 Dibujar_drawText((*Textura_getSObject()).Fuente, Lista_GetIn(*BuyS.mensaje,BuyS.i),
	 GameFramework_Vector2(20, 140), GameFramework_Color(1, 1, 1, 1),
	 1.0f);
	 }*/

//(Letra, mensaje, new Vector2(20,140),
//		Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.01f);
}

//*****************************************

//******TITULO**********
void Buy_loadTitulo()
{
	BuyS.positionTitulo.X = 300;
	BuyS.positionTitulo.Y = 900;
	BuyS.origenTitulo.X = (*(TexturaObjeto*) Lista_RetornaElemento(
			&(BuyS.textura), 0)).ancho / 2;
	BuyS.origenTitulo.Y = (*(TexturaObjeto*) Lista_RetornaElemento(
			&(BuyS.textura), 0)).alto / 2;
}

void Buy_drawTitulo()
{
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_RetornaElemento(&(BuyS.textura), 0),
			BuyS.positionTitulo, GameFramework_Color(1, 1, 1, 1.0), 0.0f,
			BuyS.origenTitulo, GameFramework_Vector2(1.1, 1.1));
}

//*********Texturas***************
void Buy_setTexturas()
{
	Lista_Inicia(&(BuyS.textura));
	Lista_InsertaEnFinal(&(BuyS.textura), BuyS.textura.fin,
			Textura_getTextura("TituloBuyGame")); //0
	Lista_InsertaEnFinal(&(BuyS.textura), BuyS.textura.fin,
			Textura_getTexturaFondo(0)); //1
}

