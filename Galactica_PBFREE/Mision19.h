/*
 * Mision19.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION19_H_
#define MISION19_H_

#include "GalacticaTipos.h"

void Mision19_reiniciar();
void Mision19_load();
void Mision19_update();
void Mision19_draw();
void Mision19_drawEstadoNave();
void Mision19_gameOver();
void Mision19_drawMensajeInicial();
Mision19* Mision19_getSObject();

#endif /* MISION19_H_ */
