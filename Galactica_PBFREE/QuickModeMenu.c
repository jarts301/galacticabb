/*
 * QuickModeMenu.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "QuickModeMenu.h"
#include "Almacenamiento.h"
#include "Lista.h"
#include "Nave.h"
#include "Boton.h"
#include "Plus.h"
#include "QuickModeGame.h"
#include "Jefe.h"
#include "DisparoJefe.h"
#include "Enemigo.h"
#include "DisparoEnemigo.h"
#include "Textura.h"
#include "GameComun.h"
#include "Dibujar.h"
#include "Cadena.h"

static QuickModeMenu QuickModeMenuS;

void QuickModeMenu_load()
{
	QuickModeMenu_setTexturas();
	QuickModeMenu_loadTitulo();
	QuickModeMenuS.texturaNaveActual = 0;
	QuickModeMenuS.rescatadas = 1;
	QuickModeMenuS.positionNave = GameFramework_Vector2(300, 400);
	QuickModeMenuS.origenNave = GameFramework_Vector2(0, 0);
	Lista_Inicia(&QuickModeMenuS.botones);
	QuickModeMenuS.positionNavesRescatadas = GameFramework_Vector2(70, 685);
	QuickModeMenuS.positionNombreNave = GameFramework_Vector2(70, 655);
	Lista_AddInFin(&QuickModeMenuS.botones,
			Boton_CrearBotonF(BFlecha, 10, 380, 0));
	Lista_AddInFin(&QuickModeMenuS.botones,
			Boton_CrearBotonF(BFlecha, 590, 490, 1));
	Lista_AddInFin(&QuickModeMenuS.botones, Boton_Crear(BPlay, 290, 10));
	Lista_AddInFin(&QuickModeMenuS.botones, Boton_Crear(BBack, 10, 10));
	QuickModeMenuS.actualframe = 0;
	QuickModeMenuS.dato = 0;
	QuickModeMenuS.TrozoAnim.X = QuickModeMenuS.actualframe * 341.33;
	QuickModeMenuS.TrozoAnim.Y = 0;
	QuickModeMenuS.TrozoAnim.Width = 341.33;
	QuickModeMenuS.TrozoAnim.Height = 256;
	QuickModeMenuS.origenNave.X = QuickModeMenuS.TrozoAnim.Width / 2;
	QuickModeMenuS.origenNave.Y = QuickModeMenuS.TrozoAnim.Height / 2;
}

void QuickModeMenu_reiniciar()
{
	QuickModeMenuS.texturaNaveActual = 0;
	(*Nave_getSObject()).TexturaNave = 0;
}

void QuickModeMenu_update()
{
	GameComun_updateEstrellas();
	QuickModeMenu_updateNave();
	for (QuickModeMenuS.i = 0; QuickModeMenuS.i < QuickModeMenuS.botones.size;
			QuickModeMenuS.i++)
	{
		Boton_update(
				(Boton*) Lista_GetIn(&QuickModeMenuS.botones,
						QuickModeMenuS.i));
	}
}

void QuickModeMenu_draw()
{
	Dibujar_drawSprite(Textura_getTexturaFondo(0), GameFramework_Vector2(0, 0),
			GameFramework_Color(0, 0, 0, 1), 0, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(40, 40));

	GameComun_drawEstrellas();
	QuickModeMenu_drawTitulo();
	for (QuickModeMenuS.i = 0; QuickModeMenuS.i < QuickModeMenuS.botones.size;
			QuickModeMenuS.i++)
	{
		Boton_draw(
				(Boton*) Lista_GetIn(&QuickModeMenuS.botones,
						QuickModeMenuS.i));
	}
	//*Naves
	Dibujar_drawSpriteTr(
			(TexturaObjeto*) Lista_GetIn(&QuickModeMenuS.texturaNaves,
					QuickModeMenuS.texturaNaveActual),
			QuickModeMenuS.positionNave, QuickModeMenuS.TrozoAnim,
			GameFramework_Color(1, 1, 1, 1), 0, QuickModeMenuS.origenNave,
			GameFramework_Vector2(1.0, 1.4));

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(3, "SPACESHIPS RESCUED: ",
					GameFramework_EnteroACadena(QuickModeMenuS.rescatadas),
					"/6"), QuickModeMenuS.positionNavesRescatadas,
			GameFramework_Color(1, 1, 1, 1), 0.92);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "NAME: ",
					(char*) Lista_GetIn(&QuickModeMenuS.nombresNave,
							QuickModeMenuS.texturaNaveActual)),
			QuickModeMenuS.positionNombreNave, GameFramework_Color(1, 1, 1, 1),
			0.92);
}

//*****************************************

//******TITULO*********
void QuickModeMenu_loadTitulo()
{
	QuickModeMenuS.positionTitulo = GameFramework_Vector2(300, 910);
	QuickModeMenuS.origenTitulo = GameFramework_Vector2(
			(*(TexturaObjeto*) Lista_GetIn(&QuickModeMenuS.textura, 0)).ancho
					/ 2,
			(*(TexturaObjeto*) Lista_GetIn(&QuickModeMenuS.textura, 0)).alto
					/ 2);
	QuickModeMenuS.positionSubT = GameFramework_Vector2(300, 720);
	QuickModeMenuS.origenSubT = GameFramework_Vector2(
			(*(TexturaObjeto*) Lista_GetIn(&QuickModeMenuS.textura, 1)).ancho
					/ 2,
			(*(TexturaObjeto*) Lista_GetIn(&QuickModeMenuS.textura, 1)).alto
					/ 2);
}

void QuickModeMenu_drawTitulo()
{
	Dibujar_drawSprite((TexturaObjeto*) Lista_GetIn(&QuickModeMenuS.textura, 0),
			QuickModeMenuS.positionTitulo, GameFramework_Color(1, 1, 1, 1), 0,
			QuickModeMenuS.origenTitulo, GameFramework_Vector2(1.0, 1.0));

	Dibujar_drawSprite((TexturaObjeto*) Lista_GetIn(&QuickModeMenuS.textura, 1),
			QuickModeMenuS.positionSubT, GameFramework_Color(1, 1, 1, 1), 0,
			QuickModeMenuS.origenSubT, GameFramework_Vector2(1.2, 1.2));
}

//**Actualiza Nave
void QuickModeMenu_updateNave()
{
	QuickModeMenuS.dato += GameFramework_getSwapTime();
	if (QuickModeMenuS.dato >= ((GameFramework_getSwapTime()) * 6))
	{
		QuickModeMenuS.actualframe = QuickModeMenuS.actualframe + 1;
		QuickModeMenuS.dato = 0;
	}

	if (QuickModeMenuS.actualframe >= 3)
	{
		QuickModeMenuS.actualframe = 0;
	}

	QuickModeMenuS.TrozoAnim.X = QuickModeMenuS.actualframe * 341.33333;
	QuickModeMenuS.TrozoAnim.Y = 0;
	QuickModeMenuS.TrozoAnim.Width = 341.33333;
	QuickModeMenuS.TrozoAnim.Height = 256;

	QuickModeMenuS.origenNave.X = QuickModeMenuS.TrozoAnim.Width / 2;
	QuickModeMenuS.origenNave.Y = QuickModeMenuS.TrozoAnim.Height / 2;
}

//*********Texturas***************
void QuickModeMenu_setTexturas()
{
	Lista_Inicia(&QuickModeMenuS.textura);
	Lista_Inicia(&QuickModeMenuS.texturaNaves);
	Lista_Inicia(&QuickModeMenuS.nombresNave);

	Lista_AddInFin(&QuickModeMenuS.textura,
			Textura_getTextura("TituloQuickGame")); //0
	Lista_AddInFin(&QuickModeMenuS.textura, Textura_getTextura("TEscogerNave")); //1

	Lista_AddInFin(&QuickModeMenuS.texturaNaves,
			Textura_getTextura("NGalactica")); //0
	Lista_AddInFin(&QuickModeMenuS.texturaNaves, Textura_getTextura("NCarafe")); //1
	Lista_AddInFin(&QuickModeMenuS.texturaNaves, Textura_getTextura("NHelix")); //2
	Lista_AddInFin(&QuickModeMenuS.texturaNaves, Textura_getTextura("NArp")); //3
	Lista_AddInFin(&QuickModeMenuS.texturaNaves,
			Textura_getTextura("NPerseus")); //4
	Lista_AddInFin(&QuickModeMenuS.texturaNaves, Textura_getTextura("NPisces")); //5

	Lista_AddInFin(&QuickModeMenuS.nombresNave, String_creaString("GALACTICA")); //0
	Lista_AddInFin(&QuickModeMenuS.nombresNave, String_creaString("CARAFE")); //1
	Lista_AddInFin(&QuickModeMenuS.nombresNave, String_creaString("HELIX")); //2
	Lista_AddInFin(&QuickModeMenuS.nombresNave, String_creaString("ARP")); //3
	Lista_AddInFin(&QuickModeMenuS.nombresNave, String_creaString("PERSEUS")); //4
	Lista_AddInFin(&QuickModeMenuS.nombresNave, String_creaString("PISCES")); //5
}

QuickModeMenu* QuickModeMenu_getSObject()
{
	return &QuickModeMenuS;
}

