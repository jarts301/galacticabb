/*
 * Plus.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "Plus.h"
#include "Dibujar.h"
#include "Textura.h"
#include "Lista.h"
#include "Collition.h"
#include "Nave.h"
#include <stdlib.h>

static Plus PlusS;

Plus* Plus_Crear(Vector2 pos)
{
	Plus* nuevo;
	if ((nuevo = (Plus*) malloc(sizeof(Plus))) == NULL)
		return NULL;

	(*nuevo).Rotacion = 0;
	(*nuevo).plusActivo = 0;
	(*nuevo).Escala = GameFramework_Vector2(0.3, 0.3);
	Plus_setPosition(nuevo,pos);
	Plus_setOrigen(nuevo,0);
	(*nuevo).radioCollition = (int) (128 * (*nuevo).Escala.X);
	(*nuevo).texturaActual = 0;

	return nuevo;
}

void Plus_reiniciar(Plus* plus, Vector2 pos)
{
	(*plus).Rotacion = 0;
	Plus_setPosition(plus,pos);
	(*plus).i = GameFramework_Rand(1, 2);
	if ((*plus).i == 1)
	{
		(*plus).tipoPlus = PLvida;
	}
	else if ((*plus).i == 2)
	{
		(*plus).tipoPlus = PLdisparo;
	}
}

void Plus_load()
{
	Plus_setTextura();
}

void Plus_update(Plus* plus)
{
	if ((*plus).muestra == 0)
	{
		(*plus).Position.Y = (*plus).Position.Y - 6;

		(*plus).Rotacion = (*plus).Rotacion + 3;
		if ((*plus).Rotacion >= 360)
		{
			(*plus).Rotacion = 0;
		}

		if (Collition_colisionPlusANave(&(*plus).colision, plus) == 1)
		{
			if ((*plus).tipoPlus == PLvida)
			{
				if ((*Nave_getSObject()).MaxVida == (*Nave_getSObject()).Vida)
				{
					if ((*Nave_getSObject()).disparosDiagActivos == 0
							&& (*Nave_getSObject()).sePuedeDispDiag == 1)
					{
						(*Nave_getSObject()).disparosDiagActivos = 1;
					}
					else
					{
						if ((*Nave_getSObject()).disparoSuperActivo == 0
								&& (*Nave_getSObject()).sePuedeDispSuper == 1)
						{
							(*Nave_getSObject()).disparoSuperActivo = 1;
						}
					}
				}
				else
				{
					(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
							+ PlusS.recuperacion;
					if ((*Nave_getSObject()).Vida
							> (*Nave_getSObject()).MaxVida)
					{
						(*Nave_getSObject()).Vida =
								(*Nave_getSObject()).MaxVida;
					}
				}
			}
			if ((*plus).tipoPlus == PLdisparo)
			{
				if ((*Nave_getSObject()).disparosDiagActivos == 0
						&& (*Nave_getSObject()).sePuedeDispDiag == 1
						&& (*Nave_getSObject()).Vida
								> (((*Nave_getSObject()).MaxVida * 1) / 8))
				{
					(*Nave_getSObject()).disparosDiagActivos = 1;
				}
				else
				{
					if ((*Nave_getSObject()).disparoSuperActivo == 0
							&& (*Nave_getSObject()).sePuedeDispSuper == 1
							&& (*Nave_getSObject()).Vida
									> (((*Nave_getSObject()).MaxVida * 2) / 8))
					{
						(*Nave_getSObject()).disparoSuperActivo = 1;
					}
					else
					{
						(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
								+ PlusS.recuperacion;
						if ((*Nave_getSObject()).Vida
								> (*Nave_getSObject()).MaxVida)
						{
							(*Nave_getSObject()).Vida =
									(*Nave_getSObject()).MaxVida;
						}
					}
				}
			}
			(*plus).plusActivo = 0;
		}
	}
}

void Plus_draw(Plus* plus)
{
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_GetIn(&PlusS.texPlus, (*plus).texturaActual),
			(*plus).Position, GameFramework_Color(1, 1, 1, 1), (*plus).Rotacion,
			(*plus).Origen, (*plus).Escala);
}

//**********************************

void Plus_setTextura()
{
	Lista_Inicia(&PlusS.texPlus);
	Lista_AddInFin(&PlusS.texPlus, Textura_getTextura("Plus"));
}

void Plus_setPosition(Plus* plus, Vector2 v)
{
	(*plus).Position.X = v.X;
	(*plus).Position.Y = v.Y;
}

Vector2 Plus_getPosition(Plus* plus)
{
	return (*plus).Position;
}

void Plus_setOrigen(Plus* plus, int textura)
{
	(*plus).Origen.X =
			(*(TexturaObjeto*) Lista_GetIn(&PlusS.texPlus, textura)).ancho
					/ 2;
	(*plus).Origen.Y =
			(*(TexturaObjeto*) Lista_GetIn(&PlusS.texPlus, textura)).alto / 2;
}

Plus* Plus_getSObject()
{
	return &PlusS;
}

