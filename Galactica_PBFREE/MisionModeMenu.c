/*
 * MisionModeMenu.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "MisionModeMenu.h"
#include "Almacenamiento.h"
#include "Lista.h"
#include "Nave.h"
#include "Boton.h"
#include "Plus.h"
#include "QuickModeGame.h"
#include "Jefe.h"
#include "DisparoJefe.h"
#include "Enemigo.h"
#include "DisparoEnemigo.h"
#include "Textura.h"
#include "GameComun.h"
#include "Dibujar.h"

static MisionModeMenu MisionModeMenuS;

void MisionModeMenu_load()
{
	MisionModeMenu_setTexturas();
	MisionModeMenu_loadTitulo();
	MisionModeMenuS.posx = 10;
	MisionModeMenuS.posy = 140;
	MisionModeMenuS.cont = 0;
	Lista_Inicia(&MisionModeMenuS.botones);
	Lista_Inicia(&MisionModeMenuS.idMision);
	Lista_Inicia(&MisionModeMenuS.numeroMision);
	Lista_Inicia(&MisionModeMenuS.estaActivo);
	for (MisionModeMenuS.i = 0; MisionModeMenuS.i < 5; MisionModeMenuS.i++)
	{
		for (MisionModeMenuS.j = 0; MisionModeMenuS.j < 4; MisionModeMenuS.j++)
		{
			MisionModeMenuS.cont++;
			Lista_AddInFin(&MisionModeMenuS.botones,
					Boton_Crear(BMision, MisionModeMenuS.posx,
							MisionModeMenuS.posy));

			Lista_AddInFin(&MisionModeMenuS.idMision,
					GameFramework_Vector2Crear(MisionModeMenuS.posx+10,
							MisionModeMenuS.posy+10));

			Lista_AddInFin(&MisionModeMenuS.numeroMision,
					GameFramework_creaInteger(MisionModeMenuS.cont));

			Lista_AddInFin(&MisionModeMenuS.estaActivo,
					GameFramework_creaInteger(0));

			MisionModeMenuS.posx = MisionModeMenuS.posx + 150;
		}
		MisionModeMenuS.posx = 10;
		MisionModeMenuS.posy = MisionModeMenuS.posy + 150;
	}
	//(*(int*) Lista_GetIn(&MisionModeMenuS.estaActivo, 0)) = 1;
	Lista_AddInFin(&MisionModeMenuS.botones, Boton_Crear(BBack, 10, 10));

}

//Carga las misiones para saber cuales activar
void MisionModeMenu_cargarMisiones()
{
	Almacenamiento_cargarMisiones();
	for (MisionModeMenuS.i = 0;
			MisionModeMenuS.i < Almacenamiento_getMisionesActivas();
			MisionModeMenuS.i++)
	{
		(*(Integer*) Lista_RetornaElemento(&(MisionModeMenuS.estaActivo),
				MisionModeMenuS.i)).num = 1;
	}

	(*Nave_getSObject()).MaxVida = 100;
	(*Nave_getSObject()).sePuedeDispDiag = 1;
	(*Nave_getSObject()).sePuedeDispSuper = 1;
	(*Boton_getSObject()).tiempoEsperaPoder = 6000000;
	(*Nave_getSObject()).masPoderDisp = 0;
	(*Plus_getSObject()).recuperacion = 50;
	(*Nave_getSObject()).Puntos = 0;
	(*Nave_getSObject()).Vida = 100;
	(*Nave_getSObject()).TexturaNave = 0;
	(*QuickModeGame_getSObject()).igb.maxEnemigos = 0;
	(*Nave_getSObject()).Destruidos = 0;
	(*QuickModeGame_getSObject()).igb.tiempoEsperaPlus = 30000000;
	(*QuickModeGame_getSObject()).igb.aumentoEnVidaJefe = 0;
	(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo = 0;
	(*Jefe_getSObject()).adicionDanoNave = 0;
	(*DisparoJefe_getSObject()).adicionDanoDisNave = 0;
	(*Enemigo_getSObject()).adicionDanoNave = 0;
	(*DisparoEnemigo_getSObject()).adicionDanoDisNave = 0;
	(*QuickModeGame_getSObject()).igb.ultimaAdicionJefe = 0;
}

void MisionModeMenu_update()
{
	GameComun_updateEstrellas();
	for (MisionModeMenuS.i = 0;
			MisionModeMenuS.i < MisionModeMenuS.botones.size;
			MisionModeMenuS.i++)
	{
		Boton_update(
				(Boton*) Lista_GetIn(&MisionModeMenuS.botones,
						MisionModeMenuS.i));
	}
	MisionModeMenu_verificaBotonMision();
}

void MisionModeMenu_draw()
{
	Dibujar_drawSprite(Textura_getTexturaFondo(0), GameFramework_Vector2(0, 0),
			GameFramework_Color(0, 0, 0, 1), 0, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(40, 40));

	GameComun_drawEstrellas();
	MisionModeMenu_drawTitulo();
	for (MisionModeMenuS.i = 0;
			MisionModeMenuS.i < MisionModeMenuS.botones.size;
			MisionModeMenuS.i++)
	{
		Boton_draw(
				(Boton*) Lista_GetIn(&MisionModeMenuS.botones,
						MisionModeMenuS.i));
	}

	for (MisionModeMenuS.i = 0;
			MisionModeMenuS.i < MisionModeMenuS.numeroMision.size;
			MisionModeMenuS.i++)
	{
		Dibujar_drawText((*Textura_getSObject()).Fuente,
				GameFramework_EnteroACadena(
						*(int*) Lista_GetIn(&MisionModeMenuS.numeroMision,
								MisionModeMenuS.i)),
				*(Vector2*) Lista_GetIn(&MisionModeMenuS.idMision,
						MisionModeMenuS.i), GameFramework_Color(1, 1, 1, 1),
				1.7);
	}
}

//******************************************

//******TITULO***********
void MisionModeMenu_loadTitulo()
{
	MisionModeMenuS.positionTitulo = GameFramework_Vector2(300, 920);
	MisionModeMenuS.origenTitulo = GameFramework_Vector2(
			(*(TexturaObjeto*) Lista_GetIn(&MisionModeMenuS.textura, 0)).ancho
					/ 2,
			(*(TexturaObjeto*) Lista_GetIn(&MisionModeMenuS.textura, 0)).alto
					/ 2);
}

void MisionModeMenu_drawTitulo()
{
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_GetIn(&MisionModeMenuS.textura, 0),
			MisionModeMenuS.positionTitulo, GameFramework_Color(1, 1, 1, 1), 0,
			MisionModeMenuS.origenTitulo, GameFramework_Vector2(1.0, 1.0));
}

//**Verifica si boton activo
void MisionModeMenu_verificaBotonMision()
{
	for (MisionModeMenuS.i = 0;
			MisionModeMenuS.i < MisionModeMenuS.estaActivo.size;
			MisionModeMenuS.i++)
	{
		if (*(int*) Lista_GetIn(&MisionModeMenuS.estaActivo, MisionModeMenuS.i)
				== 1)
		{
			(*(Boton*) Lista_GetIn(&MisionModeMenuS.botones, MisionModeMenuS.i)).actualframeB =
					0;
		}
	}
}

//*********Texturas***************
void MisionModeMenu_setTexturas()
{
	Lista_Inicia(&MisionModeMenuS.textura);
	Lista_AddInFin(&MisionModeMenuS.textura,
			Textura_getTextura("TituloMisionMode")); //0
}

MisionModeMenu* MisionModeMenu_getSObject()
{
	return &MisionModeMenuS;
}
