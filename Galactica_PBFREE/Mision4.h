/*
 * Mision4.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION4_H_
#define MISION4_H_

#include "GalacticaTipos.h"

void Mision4_reiniciar();
void Mision4_load();
void Mision4_update();
void Mision4_draw();
void Mision4_drawEstadNave();
void Mision4_updateGame();
void Mision4_gameOver();
void Mision4_drawMensajeInicial();
void Mision4_insertaEnemigo();
Mision4* Mision4_getSObject();

#endif /* MISION4_H_ */
