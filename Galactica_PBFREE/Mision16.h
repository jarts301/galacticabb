/*
 * Mision16.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION16_H_
#define MISION16_H_

#include "GalacticaTipos.h"

void Mision16_reiniciar();
void Mision16_load();
void Mision16_update();
void Mision16_draw();
void Mision16_drawEstadNave();
void Mision16_updateGame();
void Mision16_gameOver();
void Mision16_drawMensajeInicial();
void Mision16_insertaEnemigo();
Mision16* Mision16_getSObject();

#endif /* MISION16_H_ */
