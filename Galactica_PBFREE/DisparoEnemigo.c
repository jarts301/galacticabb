/*
 * DisparoEnemigo.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "DisparoEnemigo.h"
#include "Collition.h"
#include "Dibujar.h"
#include "Textura.h"
#include "Nave.h"
#include "Lista.h"
#include <stdlib.h>

static DisparoEnemigo DisparoEnemigoS;

/*
 public DisparoEnemigo()
 {
 }
 */

DisparoEnemigo* DisparoEnemigo_CrearObject(Vector2 posEnem,
		DisparoEnemigo_tipo t, int derecha)
{
	DisparoEnemigo* nuevo;
	if ((nuevo = (DisparoEnemigo*) malloc(sizeof(DisparoEnemigo))) == NULL)
		return NULL;

	if (t == DEdispSid)
	{
		(*nuevo).Position.X = posEnem.X;
		(*nuevo).Position.Y = posEnem.Y;
		(*nuevo).disDerecha = derecha;
		(*nuevo).tipoActual = t;
		(*nuevo).textura = 0;
		(*nuevo).Escala = GameFramework_Vector2(1.0f, 1.0f);
		DisparoEnemigo_setOrigen(nuevo, 0);
		(*nuevo).radioCollition = (int) (6 * (*nuevo).Escala.X);
	}
	else if (t == DEdispNor)
	{
		(*nuevo).Position.X = posEnem.X;
		(*nuevo).Position.Y = posEnem.Y;
		(*nuevo).Disparar = 0;
		(*nuevo).postDisparo = 0;
		(*nuevo).Tiempo = 0;
		(*nuevo).tipoActual = t;
		(*nuevo).textura = 1;
		(*nuevo).Escala = GameFramework_Vector2(1.0f, 1.0f);
		DisparoEnemigo_setOrigen(nuevo, 1);
		(*nuevo).radioCollition = (int) (6 * (*nuevo).Escala.X);
	}
	(*nuevo).estaEnColision = 0;

	return nuevo;
}

void DisparoEnemigo_load()
{
	DisparoEnemigoS.adicionDanoDisNave = 0;
	DisparoEnemigo_setTextura();
}

void DisparoEnemigo_update(DisparoEnemigo* disparoEnemigo)
{
	if ((*disparoEnemigo).tipoActual == DEdispSid)
	{
		DisparoEnemigo_movimDisparoSid(disparoEnemigo);
	}
	else if ((*disparoEnemigo).tipoActual == DEdispNor)
	{
		DisparoEnemigo_movimDisparoNor(disparoEnemigo);
	}
}

void DisparoEnemigo_draw(DisparoEnemigo* disparoEnemigo)
{
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_RetornaElemento(&(DisparoEnemigoS.Texturas),
					(*disparoEnemigo).textura), (*disparoEnemigo).Position,
			GameFramework_Color(1, 1, 1, 1.0), 0.0f, (*disparoEnemigo).Origen,
			(*disparoEnemigo).Escala);
	/*spriteBatch.Draw(Texturas[textura], Position, null, Color.White, 0.0f,
	 Origen, Escala, SpriteEffects.None, 0.45f);*/
}

///*************************************

void DisparoEnemigo_movimDisparoSid(DisparoEnemigo* disparoEnemigo)
{
	if ((*disparoEnemigo).disDerecha == 1)
	{
		(*disparoEnemigo).Position.X = (*disparoEnemigo).Position.X + 20;
	}
	if ((*disparoEnemigo).disDerecha == 0)
	{
		(*disparoEnemigo).Position.X = (*disparoEnemigo).Position.X - 20;
	}

	if (Collition_colisionDispEnemigoANave((*disparoEnemigo).collition,
			disparoEnemigo) == 1)
	{
		(*disparoEnemigo).estaEnColision = 1;
		(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
				- (3 + DisparoEnemigoS.adicionDanoDisNave);
	}
}

void DisparoEnemigo_movimDisparoNor(DisparoEnemigo* disparoEnemigo)
{
	if ((*disparoEnemigo).Disparar == 1)
	{
		(*disparoEnemigo).Tiempo = 0;
		(*disparoEnemigo).posNave = (*Nave_getSObject()).PositionNav;
		(*disparoEnemigo).x = (*disparoEnemigo).Position.X;
		(*disparoEnemigo).y = (*disparoEnemigo).Position.Y;
		(*disparoEnemigo).velocidadX = (int) (((*disparoEnemigo).posNave.X
				- (*disparoEnemigo).x)) * 1 / 40;
		(*disparoEnemigo).velocidadY = (int) (((*disparoEnemigo).posNave.Y
				- (*disparoEnemigo).y)) * 1 / 40;
		(*disparoEnemigo).postDisparo = 1;
		(*disparoEnemigo).Disparar = 0;
	}
	if ((*disparoEnemigo).postDisparo)
	{
		(*disparoEnemigo).Tiempo++;
		(*disparoEnemigo).Position.X = (*disparoEnemigo).x
				+ (*disparoEnemigo).velocidadX * (*disparoEnemigo).Tiempo;
		(*disparoEnemigo).Position.Y = (*disparoEnemigo).y
				+ (*disparoEnemigo).velocidadY * (*disparoEnemigo).Tiempo;
	}

	if (Collition_colisionDispEnemigoANave((*disparoEnemigo).collition,
			disparoEnemigo) == 1)
	{
		(*disparoEnemigo).estaEnColision = 1;
		(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
				- (3 + DisparoEnemigoS.adicionDanoDisNave);
	}
}

void DisparoEnemigo_setPosition(DisparoEnemigo* disparoEnemigo, Vector2 p)
{
	(*disparoEnemigo).Position.X = p.X;
	(*disparoEnemigo).Position.Y = p.Y;
}
Vector2 DisparoEnemigo_getPosition(DisparoEnemigo* disparoEnemigo)
{
	return (*disparoEnemigo).Position;
}

void DisparoEnemigo_setLadoMovimiento(DisparoEnemigo* disparoEnemigo, int derecha)
{
	(*disparoEnemigo).disDerecha = derecha;
}

void DisparoEnemigo_setTextura()
{
	Lista_Inicia(&(DisparoEnemigoS.Texturas));
	Lista_InsertaEnFinal(&(DisparoEnemigoS.Texturas),
			DisparoEnemigoS.Texturas.fin, Textura_getTextura("DisparoSid")); //0
	Lista_InsertaEnFinal(&(DisparoEnemigoS.Texturas),
			DisparoEnemigoS.Texturas.fin, Textura_getTextura("DisparoNor")); //1
}

void DisparoEnemigo_setOrigen(DisparoEnemigo* disparoEnemigo, int numTex)
{
	(*disparoEnemigo).Origen.X = (*(TexturaObjeto*) Lista_RetornaElemento(
			&(DisparoEnemigoS.Texturas), numTex)).ancho / 2;
	(*disparoEnemigo).Origen.Y = (*(TexturaObjeto*) Lista_RetornaElemento(
			&(DisparoEnemigoS.Texturas), numTex)).alto / 2;
}

DisparoEnemigo* DisparoEnemigo_getSObject()
{
	return &DisparoEnemigoS;
}
