/*
 * Poderes.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "Poderes.h"
#include "Textura.h"
#include "Dibujar.h"
#include "Lista.h"
#include <stdlib.h>

static Poderes PoderesS;

Poderes* Poderes_Crear(Vector2 posN, Poderes_tipo t, Poderes_tipoPoderCarafe tc) {
	Poderes* nuevo;
	if ((nuevo = (Poderes*) malloc(sizeof(Poderes))) == NULL)
		return NULL;

	(*nuevo).tipoActual = t;

	switch (t) {
	case PGalactica:
		(*nuevo).texturaActual = 0;
		Poderes_setPosition(nuevo, posN);
		Poderes_setOrigen(nuevo, 0);
		(*nuevo).Escala = GameFramework_Vector2(0.4, 0.75);
		PoderesS.radioCollitPGalactica = (int) (128 * 0.4);
		(*nuevo).Rotacion = 0;
		break;

	case PCarafe:
		(*nuevo).texturaActual = 1;
		(*nuevo).tipoPoderCarafeActual = tc;
		Poderes_setPosition(nuevo, posN);
		Poderes_setOrigen(nuevo, 1);
		(*nuevo).Escala = GameFramework_Vector2(0.25, 0.25);
		PoderesS.radioCollitPCarafe = (int) (40 * (*nuevo).Escala.X);
		(*nuevo).Rotacion = 0;
		break;

	case PHelix:
		(*nuevo).texturaActual = 2;
		Poderes_setPosition(nuevo, posN);
		Poderes_setOrigen(nuevo, 2);
		(*nuevo).Escala = GameFramework_Vector2(0.6, 0.6);
		PoderesS.radioCollitPHelix = (int) (100 * (*nuevo).Escala.X);
		(*nuevo).Rotacion = 0;
		break;

	case PArp:
		(*nuevo).texturaActual = 3;
		Poderes_setPosition(nuevo, posN);
		Poderes_setOrigen(nuevo, 3);
		(*nuevo).Escala = GameFramework_Vector2(1.3, 0.9);
		PoderesS.radioCollitPArp = (int) (40 * (*nuevo).Escala.X);
		(*nuevo).Rotacion = 0;
		break;

	case PPerseus:
		(*nuevo).texturaActual = 4;
		Poderes_setPosition(nuevo, posN);
		Poderes_setOrigen(nuevo, 4);
		(*nuevo).Escala = GameFramework_Vector2(0.1, 0.1);
		PoderesS.radioCollitPPerseus = (int) (256 * (*nuevo).Escala.X);
		(*nuevo).Rotacion = 0;
		break;

	case PPisces:
		(*nuevo).texturaActual = 5;
		Poderes_setPosition(nuevo, posN);
		Poderes_setOrigen(nuevo, 5);
		(*nuevo).Escala = GameFramework_Vector2(0.5, 0.5);
		(*nuevo).velocidadRandX = (*nuevo).velocidadRandY =
				(*nuevo).direccionPoder = 0;
		PoderesS.radioCollitPPisces = (int) (50 * (*nuevo).Escala.X);
		(*nuevo).Rotacion = 0;
		break;
	}
	(*nuevo).valAuxCarafe = 10;

	return nuevo;

}

void Poderes_load() {
	Poderes_setTextura();
	Poderes_defineCollitionBoxPArp();
}

void Poderes_update(Poderes* poderes) {
	Poderes_movimiento(poderes);
}

void Poderes_draw(Poderes* poderes) {
	Dibujar_drawSprite(
			Lista_GetIn(&PoderesS.texPoderes, (*poderes).texturaActual),
			(*poderes).Position, GameFramework_Color(1, 1, 1, 1),
			(*poderes).Rotacion, (*poderes).Origen, (*poderes).Escala);
}

//**********************************

void Poderes_setTextura() {
	Lista_Inicia(&PoderesS.texPoderes);
	Lista_AddInFin(&PoderesS.texPoderes, Textura_getTextura("PoderGalactica")); //0
	Lista_AddInFin(&PoderesS.texPoderes, Textura_getTextura("PoderCarafe")); //1
	Lista_AddInFin(&PoderesS.texPoderes, Textura_getTextura("PoderHelix")); //2
	Lista_AddInFin(&PoderesS.texPoderes, Textura_getTextura("PoderArp")); //3
	Lista_AddInFin(&PoderesS.texPoderes, Textura_getTextura("PoderPerseus")); //4
	Lista_AddInFin(&PoderesS.texPoderes, Textura_getTextura("PoderPisces")); //5
}

Vector2 Poderes_getPosition(Poderes* poderes) {
	return (*poderes).Position;
}

void Poderes_setPosition(Poderes* poderes, Vector2 pos) {
	switch ((*poderes).tipoActual) {
	case PGalactica:
		(*poderes).Position.X = pos.X;
		(*poderes).Position.Y = pos.Y - 35;
		break;

	case PCarafe:
		(*poderes).Position.X = pos.X;
		(*poderes).Position.Y = pos.Y;
		break;

	case PHelix:
		(*poderes).Position.X = pos.X;
		(*poderes).Position.Y = pos.Y - 25;
		break;

	case PArp:
		(*poderes).Position.X = pos.X;
		(*poderes).Position.Y = pos.Y;
		break;

	case PPerseus:
		(*poderes).Position.X = pos.X;
		(*poderes).Position.Y = pos.Y;
		break;

	case PPisces:
		(*poderes).Position.X = pos.X;
		(*poderes).Position.Y = pos.Y;

		(*poderes).direccionPoder = GameFramework_Rand(1, 2);
		if ((*poderes).direccionPoder == 1) {
			(*poderes).direccionPoder = 1;
		} else if ((*poderes).direccionPoder == 2) {
			(*poderes).direccionPoder = -1;
		}

		(*poderes).velocidadRandX = (GameFramework_Rand(0, 18))
				* ((*poderes).direccionPoder);
		(*poderes).velocidadRandY = (GameFramework_Rand(10, 18));
		break;
	}
}

void Poderes_movimiento(Poderes* poderes) {
	switch ((*poderes).tipoActual) {
	case PGalactica:
		(*poderes).Position.Y = (*poderes).Position.Y + 10;
		break;

	case PCarafe:
		switch ((*poderes).tipoPoderCarafeActual) {
		case PCMedio:
			(*poderes).Position.Y = (*poderes).Position.Y + 15;
			break;

		case PCDerecha:
			(*poderes).Position.X = (*poderes).Position.X
					+ (*poderes).valAuxCarafe;
			(*poderes).Position.Y = (*poderes).Position.Y + 15;
			break;

		case PCIzquierda:
			(*poderes).Position.X = (*poderes).Position.X
					- (*poderes).valAuxCarafe;
			(*poderes).Position.Y = (*poderes).Position.Y + 15;
			break;
		case PCNull:
			break;
		}
		break;

	case PHelix:
		(*poderes).Position.Y = (*poderes).Position.Y + 25;
		break;

	case PArp:
		(*poderes).Position.Y = (*poderes).Position.Y + 5;
		break;

	case PPerseus:
		(*poderes).Escala.X = (*poderes).Escala.X + 0.04f;
		(*poderes).Escala.Y = (*poderes).Escala.Y + 0.04f;
		PoderesS.radioCollitPPerseus = (int) (256 * (*poderes).Escala.X);
		break;

	case PPisces:
		(*poderes).Position.X = (*poderes).Position.X
				+ (*poderes).velocidadRandX;
		(*poderes).Position.Y = (*poderes).Position.Y
				+ (*poderes).velocidadRandY;
		(*poderes).Rotacion = (*poderes).Rotacion + 15;
		if ((*poderes).Rotacion >= 360) {
			(*poderes).Rotacion = 0;
		}
		break;
	}
}

void Poderes_setOrigen(Poderes* poderes, int tex) {
	(*poderes).Origen.X = (*(TexturaObjeto*) Lista_GetIn(&PoderesS.texPoderes,
			tex)).ancho / 2;
	(*poderes).Origen.Y = (*(TexturaObjeto*) Lista_GetIn(&PoderesS.texPoderes,
			tex)).alto / 2;
}

//DETECTOR DE COLISIONES DE PoderArp
void Poderes_defineCollitionBoxPArp() {
	//suma necesaria a partir de la pos actual del poder
	PoderesS.collitionPArpX[0] = (int) (-200 * 1.0f);
	PoderesS.collitionPArpY[0] = 0;
	PoderesS.collitionPArpX[1] = (int) (-120 * 1.0f);
	PoderesS.collitionPArpY[1] = 0;
	PoderesS.collitionPArpX[2] = (int) (-40 * 1.0f);
	PoderesS.collitionPArpY[2] = 0;
	PoderesS.collitionPArpX[3] = (int) (40 * 1.0f);
	PoderesS.collitionPArpY[3] = 0;
	PoderesS.collitionPArpX[4] = (int) (120 * 1.0f);
	PoderesS.collitionPArpY[4] = 0;
	PoderesS.collitionPArpX[5] = (int) (200 * 1.0f);
	PoderesS.collitionPArpY[5] = 0;
}

Poderes* Poderes_getSObject() {
	return &PoderesS;
}

