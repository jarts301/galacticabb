/*
 * QuickModePause.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef QUICKMODEPAUSE_H_
#define QUICKMODEPAUSE_H_

#include "GalacticaTipos.h"

void QuickModePause_load();
void QuickModePause_update();
void QuickModePause_draw();
void QuickModePause_loadTitulo();
void QuickModePause_drawTitulo();
void QuickModePause_setTexturas();
QuickModePause* QuickModePause_getSObject();

#endif /* QUICKMODEPAUSE_H_ */
