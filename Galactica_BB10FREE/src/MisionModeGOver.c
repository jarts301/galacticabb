/*
 * MisionModeGOver.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "MisionModeGOver.h"
#include "Lista.h"
#include "Boton.h"
#include "Textura.h"
#include "GameComun.h"
#include "MisionModeGame.h"
#include "Dibujar.h"

static MisionModeGOver MisionModeGOverS;

void MisionModeGOver_load()
{
	MisionModeGOver_setTexturas();
	MisionModeGOver_loadTitulo();
	MisionModeGOverS.gana = 0;
	Lista_Inicia(&MisionModeGOverS.botones);
	MisionModeGOverS.positionPuntos = GameFramework_Vector2(40, 610);
	MisionModeGOverS.positionDestruidos = GameFramework_Vector2(90, 385);
	MisionModeGOverS.positionResultados = GameFramework_Vector2(4, 500);
	Lista_AddInFin(&MisionModeGOverS.botones, Boton_Crear(BContinue, 336, 10));
	Lista_AddInFin(&MisionModeGOverS.botones, Boton_Crear(BRestart, 10, 10));
	Lista_AddInFin(&MisionModeGOverS.botones,
			Boton_Crear(BNextMision, 120, 180));

}

void MisionModeGOver_update()
{
	GameComun_updateEstrellas();
	MisionModeGOver_updateTitulo();
	if (MisionModeGOverS.tEsperaActivaBoton > 0)
	{
		MisionModeGOverS.tEsperaActivaBoton -= GameFramework_getSwapTime();
	}
	else
	{
		for (MisionModeGOverS.i = 0; MisionModeGOverS.i < 2;
				MisionModeGOverS.i++)
		{
			Boton_update(
					(Boton*) Lista_GetIn(&MisionModeGOverS.botones,
							MisionModeGOverS.i));
		}
		if (MisionModeGOverS.gana
				&& (*MisionModeGame_getSObjec()).numMision != 20)
		{
			Boton_update((Boton*) Lista_GetIn(&MisionModeGOverS.botones, 2));
		}
	}
}

void MisionModeGOver_draw()
{
	Dibujar_drawSprite(Textura_getTexturaFondo(2), GameFramework_Vector2(0, 0),
			GameFramework_Color(GameFramework_getByteColorToFloatColor(7),
					GameFramework_getByteColorToFloatColor(237),
					GameFramework_getByteColorToFloatColor(134), 1), 0,
			GameFramework_Vector2(0, 0), GameFramework_Vector2(2.5, 2));

	GameComun_drawEstrellas();
	MisionModeGOver_drawTitulo();
	for (MisionModeGOverS.i = 0; MisionModeGOverS.i < 2; MisionModeGOverS.i++)
	{
		Boton_draw(
				(Boton*) Lista_GetIn(&MisionModeGOverS.botones,
						MisionModeGOverS.i));
	}

	if (MisionModeGOverS.gana == 1
			&& (*MisionModeGame_getSObjec()).numMision != 20)
	{
		Boton_draw((Boton*) Lista_GetIn(&MisionModeGOverS.botones, 2));
	}

	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_RetornaElemento(&(MisionModeGOverS.textura),
					2), MisionModeGOverS.positionResultados,
			GameFramework_Color(1, 1, 1, 1), 0.0f, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(1.17, 0.9));

	for (MisionModeGOverS.z = 0;
			MisionModeGOverS.z < (*MisionModeGOverS.cosasGanadas).size;
			MisionModeGOverS.z++)
	{
		MisionModeGOverS.positionPuntos = GameFramework_Vector2(40, 610);
		MisionModeGOverS.positionPuntos.Y = MisionModeGOverS.positionPuntos.Y
				- (30 * (MisionModeGOverS.z + 1));

		Dibujar_drawText((*Textura_getSObject()).Fuente,
				(char*) Lista_GetIn(MisionModeGOverS.cosasGanadas,
						MisionModeGOverS.z), MisionModeGOverS.positionPuntos,
				GameFramework_Color(1, 1, 1, 1), 0.9);
	}
}
//******************************************

//******TITULO***********
void MisionModeGOver_loadTitulo()
{
	MisionModeGOverS.positionTitulo = GameFramework_Vector2(300, 820);
}

void MisionModeGOver_updateTitulo()
{
	if (MisionModeGOverS.gana == 1)
	{
		MisionModeGOverS.texturaTitulo = 0;
	}
	else
	{
		MisionModeGOverS.texturaTitulo = 1;
	}
	MisionModeGOver_setOrigen(MisionModeGOverS.texturaTitulo);
}

void MisionModeGOver_drawTitulo()
{
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_RetornaElemento(&(MisionModeGOverS.textura),
					MisionModeGOverS.texturaTitulo),
			MisionModeGOverS.positionTitulo, GameFramework_Color(1, 1, 1, 1),
			0.0f, MisionModeGOverS.origenTitulo,
			GameFramework_Vector2(1.1, 1.1));
}

void MisionModeGOver_setOrigen(int Tex)
{
	MisionModeGOverS.origenTitulo =
			GameFramework_Vector2(
					(*(TexturaObjeto*) Lista_GetIn(&MisionModeGOverS.textura,
							Tex)).ancho / 2,
					(*(TexturaObjeto*) Lista_GetIn(&MisionModeGOverS.textura,
							Tex)).alto / 2);
}

//*********Texturas***************
void MisionModeGOver_setTexturas()
{
	Lista_Inicia(&MisionModeGOverS.textura);
	Lista_AddInFin(&MisionModeGOverS.textura, Textura_getTextura("TituloWin")); //0
	Lista_AddInFin(&MisionModeGOverS.textura, Textura_getTextura("TituloLose")); //1
	Lista_AddInFin(&MisionModeGOverS.textura,
			Textura_getTextura("TRecompensa")); //2
}

MisionModeGOver* MisionModeGOver_getSObject()
{
	return &MisionModeGOverS;
}
