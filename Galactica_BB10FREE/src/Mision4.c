/*
 * Mision4.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "Mision4.h"
#include "QuickModeGame.h"
#include "Lista.h"
#include "Boton.h"
#include "Nave.h"
#include "Enemigo.h"
#include "BarraVida.h"
#include "Audio.h"
#include "MisionModeGOver.h"
#include "MisionMode.h"
#include "Almacenamiento.h"
#include "Animation.h"
#include "Dibujar.h"
#include "Textura.h"
#include "Control.h"
#include "Collition.h"
#include "Mision1.h"
#include "Onda.h"
#include "Cadena.h"

static Mision4 Mision4S;

void Mision4_reiniciar()
{
	QuickModeGame_eliminarTodos();
	QuickModeGame_eliminaAnimaciones();
	Mision4S.positionTime.X = 420;
	Mision4S.positionTime.Y = 900;
	Lista_Destruir(&((*QuickModeGame_getSObject()).igb.botones));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BPause, 5, 950));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BAudio, 530, 950));
	(*QuickModeGame_getSObject()).igb.velocidadEnem = 2;
	(*QuickModeGame_getSObject()).igb.velocidadEnemY = 2;
	(*QuickModeGame_getSObject()).igb.velocidadHiEnem = 3;
	(*QuickModeGame_getSObject()).igb.velocidadHiEnemY = 3;
	(*QuickModeGame_getSObject()).igb.maxEnemigos = 2;
	Mision4S.cuentaTiempo = 90;
	Mision4S.cuentaGolpes = 3;
	(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo = 0;
	Mision4S.ultimoTMinuto = 0;
	(*QuickModeGame_getSObject()).igb.ultimoAumento = 0;
	Onda_reiniciar(Mision4S.onda);
	Nave_puntoInicio(GameFramework_Vector2(300, 300));
	(*QuickModeGame_getSObject()).igb.EscalaMensaje =
			(*QuickModeGame_getSObject()).igb.escalaInicialMensaje = 1.5f;
	(*QuickModeGame_getSObject()).igb.actualFrameMensaje = 0;
	(*QuickModeGame_getSObject()).igb.tiempoTitileoMensaje = 0;
	(*QuickModeGame_getSObject()).igb.mensajeInicialActivo = 1;
}

void Mision4_load()
{
	Mision4S.tituloMision = "Mission 4";

	Lista_Inicia(&Mision4S.mensajeMision);
	Lista_AddInFin(&Mision4S.mensajeMision,
			String_creaString("Escape most fast as"));
	Lista_AddInFin(&Mision4S.mensajeMision,
			String_creaString("possible of the expansive"));
	Lista_AddInFin(&Mision4S.mensajeMision,
			String_creaString("wave. avoid collide or "));
	Lista_AddInFin(&Mision4S.mensajeMision,
			String_creaString("you will lose."));
	Lista_AddInFin(&Mision4S.mensajeMision,
			String_creaString("Tip:Keep moving."));

	Lista_Inicia(&Mision4S.recompensa);
	Lista_AddInFin(&Mision4S.recompensa,
			String_creaString("Maximum energy increased"));
	Lista_AddInFin(&Mision4S.recompensa,
			String_creaString("by 100, in QuickGame."));

	Lista_Inicia(&Mision4S.noRecompensa);
	Lista_AddInFin(&Mision4S.noRecompensa, String_creaString("No Reward.."));

	Mision4S.cuentaTiempo = 90, Mision4S.cuentaGolpes = 3;
	Mision4S.onda = Onda_Crear();
}

void Mision4_update()
{
	Nave_update();
	QuickModeGame_updateEstrellas();
	QuickModeGame_updateEnemigos();
	Mision4_updateGame();
	QuickModeGame_updateAnimaciones();
	Onda_update(Mision4S.onda);
	(*Nave_getSObject()).disparosActivos = 0;
	(*Nave_getSObject()).velocidadNave = 11;
	(*Nave_getSObject()).movEnY = 0;
	for (Mision4S.i = 0;
			Mision4S.i < (*QuickModeGame_getSObject()).igb.botones.size;
			Mision4S.i++)
	{
		Boton_update(
				(Boton*) Lista_GetIn(&(*QuickModeGame_getSObject()).igb.botones,
						Mision4S.i));
	}

	//Fotogramas por segundo o FPS
	if (GameFramework_getTotalTime() > Mision4S.ultimoTMinuto + 1000000)
	{
		Mision4S.cuentaTiempo = Mision4S.cuentaTiempo - 1;
		Mision4S.ultimoTMinuto = GameFramework_getTotalTime();
	}

	QuickModeGame_updateMensajeInicial();
	BarraVida_update((*QuickModeGame_getSObject()).igb.barraVidaNave,
			(*Nave_getSObject()).Vida, (*Nave_getSObject()).MaxVida);
	if ((*Nave_getSObject()).Vida <= 0)
	{
		Animation_update((*Animation_getSObject()).animacionesEstallidoNave);
	}
	Mision4_gameOver();
	if ((*Control_getSObject()).controlActivo == 1)
	{
		Control_update((*QuickModeGame_getSObject()).igb.control);
	}

}

void Mision4_draw()
{
	//CLEAR NEGRO
	Dibujar_drawSprite(Textura_getTexturaFondo(0), GameFramework_Vector2(0, 0),
			GameFramework_Color(0, 0, 0, 1), 0, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(40, 40));

	Nave_draw();
	QuickModeGame_drawEstrellas();
	QuickModeGame_drawEnemigos();
	Mision4_drawEstadNave();
	QuickModeGame_drawAnimaciones();
	Mision4_drawMensajeInicial();
	Onda_draw(Mision4S.onda);
	if ((*Nave_getSObject()).Vida <= 0)
	{
		Animation_draw((*Animation_getSObject()).animacionesEstallidoNave);
	}

	for (Mision4S.i = 0;
			Mision4S.i < (*QuickModeGame_getSObject()).igb.botones.size;
			Mision4S.i++)
	{
		Boton_draw(
				(Boton*) Lista_GetIn(&(*QuickModeGame_getSObject()).igb.botones,
						Mision4S.i));
	}
	if ((*Control_getSObject()).controlActivo == 1)
	{
		Control_draw((*QuickModeGame_getSObject()).igb.control);
	}
	// drawEstadisticas(sb);
}

//******************Estadisticas de la nave*************
void Mision4_drawEstadNave()
{
	//** Vida de la nave
	Dibujar_drawSprite(Textura_getTexturaFondo(1),
			GameFramework_Vector2(0, 945), GameFramework_Color(0, 0, 0, 1), 0,
			GameFramework_Vector2(0, 0), GameFramework_Vector2(50, 2.6));

	BarraVida_draw((*QuickModeGame_getSObject()).igb.barraVidaNave);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_EnteroACadena((*Nave_getSObject()).Vida),
			GameFramework_Vector2(100, 975), GameFramework_Color(1, 1, 1, 1),
			0.8);

	Dibujar_drawText((*Textura_getSObject()).Fuente, "/",
			GameFramework_Vector2(285, 965), GameFramework_Color(1, 1, 1, 1),
			1.4);

	Nave_drawAnimacionesPoder();

	//Tiempo restante
	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "Time:",
					GameFramework_EnteroACadena(Mision4S.cuentaTiempo)),
			GameFramework_Vector2(420, 900),
			GameFramework_Color(GameFramework_getByteColorToFloatColor(244),
					GameFramework_getByteColorToFloatColor(232),
					GameFramework_getByteColorToFloatColor(16), 1), 0.9);
}

void Mision4_updateGame()
{
	if (Mision4S.cuentaTiempo == 70
			&& (*QuickModeGame_getSObject()).igb.maxEnemigos < 3)
	{
		(*QuickModeGame_getSObject()).igb.maxEnemigos =
				(*QuickModeGame_getSObject()).igb.maxEnemigos + 1;
	}
	if (Mision4S.cuentaTiempo == 45
			&& (*QuickModeGame_getSObject()).igb.maxEnemigos < 4)
	{
		(*QuickModeGame_getSObject()).igb.maxEnemigos =
				(*QuickModeGame_getSObject()).igb.maxEnemigos + 1;
	}
	if (Mision4S.cuentaTiempo == 15
			&& (*QuickModeGame_getSObject()).igb.maxEnemigos < 5)
	{
		(*QuickModeGame_getSObject()).igb.maxEnemigos =
				(*QuickModeGame_getSObject()).igb.maxEnemigos + 1;
	}
	if ((*Nave_getSObject()).Vida <= 70 && Mision4S.cuentaGolpes == 3)
	{
		Mision4S.cuentaGolpes--;
		(*Mision4S.onda).Position.Y = 50;
	}
	if ((*Nave_getSObject()).Vida <= 40 && Mision4S.cuentaGolpes == 2)
	{
		Mision4S.cuentaGolpes--;
		(*Mision4S.onda).Position.Y = 100;
	}
	if ((*Nave_getSObject()).Vida <= 10 && Mision4S.cuentaGolpes == 1)
	{
		Mision4S.cuentaGolpes--;
		(*Mision4S.onda).Position.Y = 150;
		(*Nave_getSObject()).Vida = 0;
	}

	if ((*QuickModeGame_getSObject()).igb.enemigos.size
			< (*QuickModeGame_getSObject()).igb.maxEnemigos)
	{
		Mision4_insertaEnemigo();
	}

}

void Mision4_gameOver()
{

	if (Mision4S.cuentaTiempo <= 0)
	{
		if ((*Audio_getSObject()).musicaActiva == 1)
		{
			Audio_stopAudio("Galactic");
			Audio_playAudio("GameOver");
		}
		(*MisionModeGOver_getSObject()).gana = 1;

		Almacenamiento_cargarMisiones();
		if (Almacenamiento_getPremiosDados() == 3)
		{
			Almacenamiento_salvarBonus(250, ALsumarAsalud);
			(*MisionModeGOver_getSObject()).cosasGanadas = &Mision4S.recompensa;
			Almacenamiento_salvarMisiones();
		}
		else
		{
			(*MisionModeGOver_getSObject()).cosasGanadas =
					&(*Mision1_getSObject()).yaRecompensa;
		}

		(*MisionMode_getSObject()).estadoActual = MMGameOver;
	}

	if ((*Nave_getSObject()).Vida <= 0)
	{
		(*Nave_getSObject()).Vida = 0;
		(*Nave_getSObject()).EscalaN = GameFramework_Vector2(0, 0);
		Animation_setPosition(
				(*Animation_getSObject()).animacionesEstallidoNave,
				(*Nave_getSObject()).PositionNav);
		(*MisionModeGOver_getSObject()).gana = 0;
		(*MisionModeGOver_getSObject()).cosasGanadas = &Mision4S.noRecompensa;
		if ((*(*Animation_getSObject()).animacionesEstallidoNave).animacionFinalizada
				== 1)
		{
			if ((*Audio_getSObject()).musicaActiva == 1)
			{
				Audio_stopAudio("Galactic");
				Audio_playAudio("GameOver");
			}
			(*MisionMode_getSObject()).estadoActual = MMGameOver;
		}
	}
}

//********Dibujo Mensaje Inicial
void Mision4_drawMensajeInicial()
{
	if ((*QuickModeGame_getSObject()).igb.mensajeInicialActivo == 1)
	{
		Dibujar_drawText((*Textura_getSObject()).Fuente,
				GameFramework_Concatenar(2, Mision4S.tituloMision, " Start!!"),
				GameFramework_Vector2(50, 620),
				GameFramework_Color(GameFramework_getByteColorToFloatColor(7),
						GameFramework_getByteColorToFloatColor(237),
						GameFramework_getByteColorToFloatColor(134), 1),
				(*QuickModeGame_getSObject()).igb.EscalaMensaje);
	}
}

void Mision4_insertaEnemigo()
{
	(*QuickModeGame_getSObject()).igb.num2 = 0;
	(*QuickModeGame_getSObject()).igb.num2 = GameFramework_Rand(1, 2);

	if ((*QuickModeGame_getSObject()).igb.num2 == 1)
	{
		if ((*Enemigo_getSObject()).KamiTotal.size > 0)
		{
			Enemigo_reinicia(
					(Enemigo*) Lista_GetIn(&((*Enemigo_getSObject()).KamiTotal),
							(*Enemigo_getSObject()).KamiTotal.size - 1),
					(-1) * GameFramework_Rand(100, 500),
					GameFramework_Rand(600, 1000),
					(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
					(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

			Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
					Lista_GetIn(&((*Enemigo_getSObject()).KamiTotal),
							(*Enemigo_getSObject()).KamiTotal.size - 1));

			Lista_RemoveAt(&((*Enemigo_getSObject()).KamiTotal),
					(*Enemigo_getSObject()).KamiTotal.size - 1);
		}
	}
	if ((*QuickModeGame_getSObject()).igb.num2 == 2)
	{
		if ((*Enemigo_getSObject()).KamiTotal.size > 0)
		{
			Enemigo_reinicia(
					(Enemigo*) Lista_GetIn(&((*Enemigo_getSObject()).KamiTotal),
							(*Enemigo_getSObject()).KamiTotal.size - 1),
					GameFramework_Rand(1000, 1500),
					GameFramework_Rand(600, 1000),
					(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
					(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

			Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
					Lista_GetIn(&((*Enemigo_getSObject()).KamiTotal),
							(*Enemigo_getSObject()).KamiTotal.size - 1));

			Lista_RemoveAt(&((*Enemigo_getSObject()).KamiTotal),
					(*Enemigo_getSObject()).KamiTotal.size - 1);
		}
	}

}

Mision4* Mision4_getSObject()
{
	return &Mision4S;
}

