/*
 * Opciones.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef OPCIONES_H_
#define OPCIONES_H_

#include "GalacticaTipos.h"

void Opciones_load();
void Opciones_update();
void Opciones_draw();
void Opciones_loadTitulo();
void Opciones_drawTitulo();
void Opciones_setTexturas();
Opciones* Opciones_getSObject();

#endif /* OPCIONES_H_ */
