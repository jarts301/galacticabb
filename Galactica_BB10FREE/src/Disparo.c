/*
 * Disparo.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "Disparo.h"
#include "Dibujar.h"
#include "Textura.h"
#include <stdlib.h>

static Disparo DisparoS;

Disparo* Disparo_CrearObject(Vector2 posN, Disparo_Tipo t)
{
	Disparo* nuevo;
	if ((nuevo = (Disparo*) malloc(sizeof(Disparo))) == NULL)
		return NULL;

	(*nuevo).tipoActual = t;
	(*nuevo).Rotacion = 0;
	(*nuevo).Escala = GameFramework_Vector2(0.85f, 0.85f);
	(*nuevo).radioCollition = (int) (7 * (*nuevo).Escala.X);

	if (t == Dpequed || t == DdiagD)
	{
		Disparo_setPositionPequed(nuevo, posN);
		Disparo_setOrigen(nuevo, 0);
		(*nuevo).texturaActual = 0;
		if (t == DdiagD)
		{
			(*nuevo).Rotacion = 35;
		}
	}
	if (t == Dpequei || t == DdiagI)
	{
		Disparo_setPositionPequei(nuevo, posN);
		Disparo_setOrigen(nuevo, 0);
		(*nuevo).texturaActual = 0;
		if (t == DdiagI)
		{
			(*nuevo).Rotacion = -35;
		}
	}
	if (t == Dsuper)
	{
		Disparo_setPositionSuper(nuevo, posN);
		Disparo_setOrigen(nuevo, 1);
		(*nuevo).texturaActual = 1;
		(*nuevo).Escala = GameFramework_Vector2(0.65f, 0.65f);
		(*nuevo).radioCollition = (int) (37 * (*nuevo).Escala.X);
	}

	return nuevo;

}

void Disparo_load()
{
	Disparo_setTextura();
}

void Disparo_update(Disparo* disparo)
{
	if ((*disparo).tipoActual == Dpequed || (*disparo).tipoActual == Dpequei)
	{
		Disparo_movimientoDisparo(disparo);
	}
	if ((*disparo).tipoActual == DdiagD)
	{
		Disparo_movimientoDisparoDiagDerecha(disparo);
	}
	if ((*disparo).tipoActual == DdiagI)
	{
		Disapro_movimientoDisparoDiagIzquierda(disparo);
	}
	if ((*disparo).tipoActual == Dsuper)
	{
		Disparo_movimientoDisparoSuper(disparo);
	}
}

void Disparo_draw(Disparo* disparo)
{
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_RetornaElemento(&(DisparoS.texDisparo),
					(*disparo).texturaActual), (*disparo).Position,
			GameFramework_Color(1, 1, 1, 1.0), (*disparo).Rotacion,
			(*disparo).Origen, (*disparo).Escala);

	/*spriteBatch.Draw(texDisparo[texturaActual], Position, null, Color.White,
	 MathHelper.ToRadians(Rotacion), Origen, Escala, SpriteEffects.None,
	 0.6f);*/
}

//**********************************

void Disparo_setTextura()
{
	Lista_Inicia(&(DisparoS.texDisparo));
	Lista_InsertaEnFinal(&(DisparoS.texDisparo), DisparoS.texDisparo.fin,
			Textura_getTextura("Disparo"));
	Lista_InsertaEnFinal(&(DisparoS.texDisparo), DisparoS.texDisparo.fin,
			Textura_getTextura("SuperDisparo"));
}

void Disparo_setPositionPequed(Disparo* disparo, Vector2 posNave)
{
	(*disparo).Position.X = posNave.X - 25;
	(*disparo).Position.Y = posNave.Y + 3;
}

void Disparo_setPositionPequei(Disparo* disparo, Vector2 posNave)
{
	(*disparo).Position.X = posNave.X + 25;
	(*disparo).Position.Y = posNave.Y + 3;
}

void Disparo_setPositionSuper(Disparo* disparo, Vector2 posNave)
{
	(*disparo).Position.X = posNave.X;
	(*disparo).Position.Y = posNave.Y - 25;
}

Vector2 Disparo_getPosition(Disparo* disparo)
{
	return (*disparo).Position;
}

void Disparo_movimientoDisparo(Disparo* disparo)
{
	(*disparo).Position.Y = (*disparo).Position.Y + 20;
}

void Disparo_movimientoDisparoDiagDerecha(Disparo* disparo)
{
	(*disparo).Position.X = (*disparo).Position.X - 15;
	(*disparo).Position.Y = (*disparo).Position.Y + 20;
}

void Disapro_movimientoDisparoDiagIzquierda(Disparo* disparo)
{
	(*disparo).Position.X = (*disparo).Position.X + 15;
	(*disparo).Position.Y = (*disparo).Position.Y + 20;
}

void Disparo_movimientoDisparoSuper(Disparo* disparo)
{
	(*disparo).Position.Y = (*disparo).Position.Y + 20;
}

void Disparo_setOrigen(Disparo* disparo, int textura)
{
	(*disparo).Origen.X = (*(TexturaObjeto*) Lista_RetornaElemento(
			&(DisparoS.texDisparo), textura)).ancho / 2;
	(*disparo).Origen.Y = (*(TexturaObjeto*) Lista_RetornaElemento(
			&(DisparoS.texDisparo), textura)).alto / 2;
}

