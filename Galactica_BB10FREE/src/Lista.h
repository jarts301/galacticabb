/*
 * Lista.h
 *
 *  Created on: 21/02/2013
 *      Author: Jarts
 */

#ifndef LISTA_H_
#define LISTA_H_


typedef struct ElementoLista
{
  void *dato;
  struct ElementoLista *siguiente;
} Elemento;

typedef struct
{
  Elemento *inicio;
  Elemento *fin;
  int size;
} Lista;

/* inicializaci�n de la lista */
void Lista_Inicia (Lista* lista);


/* INSERCION */

/* inserci�n en une lista vac�a */
int Lista_InsertaEnVacia(Lista * lista, void *dato);

/* inserci�n al inicio de la lista */
int Lista_InsertaEnInicio(Lista * lista, void *dato);

/* inserci�n al final de la lista */
int Lista_InsertaEnFinal(Lista * lista, Elemento * actual, void *dato);
int Lista_AddInFin(Lista * lista, void *dato);

/* inserci�n en otra parte */
int Lista_InsertaEnIndice(Lista * lista, void *dato, int pos);


/* SUPRESION */

int Lista_SuprimeInicio(Lista * lista);
int Lista_SuprimeElemento(Lista * lista, int pos);
int Lista_RemoveAt(Lista * lista, int pos);

void* Lista_RetornaElemento(Lista * lista, int pos);
void* Lista_GetIn(Lista * lista, int pos);


//int menu (Lista *lista,int *k);
void Lista_ToString(Lista * lista);
void Lista_Destruir(Lista * lista);
/* -------- FIN lista.h --------- */


#endif /* LISTA_H_ */
