/*
 * QuickModeGOver.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef QUICKMODEGOVER_H_
#define QUICKMODEGOVER_H_

#include "GalacticaTipos.h"

void QuickModeGOver_load();
void QuickModeGOver_update();
void QuickModeGOver_draw();
void QuickModeGOver_loadTitulo();
void QuickModeGOver_drawTitulo();
void QuickModeGOver_setTexturas();
QuickModeGOver* QuickModeGOver_getSObject();

#endif /* QUICKMODEGOVER_H_ */
