/*
 * BarraVida.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef BARRAVIDA_H_
#define BARRAVIDA_H_

#include "GalacticaTipos.h"

BarraVida* BarraVida_Crear(Vector2 Pos, BarraVida_tipo tipoBarra);
void BarraVida_load();
void BarraVida_update(BarraVida* barraVida, float vida, float maxVida);
void BarraVida_draw(BarraVida* barraVida);
void BarraVida_reiniciaColor(BarraVida* barraVida);
void BarraVida_setOrigen(BarraVida* barraVida);
void BarraVida_setPosition(BarraVida* barraVida,Vector2 pos);

#endif /* BARRAVIDA_H_ */
