/*
 * Mision14.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION14_H_
#define MISION14_H_

#include "GalacticaTipos.h"

void Mision14_reiniciar();
void Mision14_load();
void Mision14_update();
void Mision14_draw();
void Mision14_drawEstadNave();
void Mision14_updateGame();
void Mision14_gameOver();
void Mision14_drawMensajeInicial();
void Mision14_insertaEnemigo();
Mision14* Mision14_getSObject();

#endif /* MISION14_H_ */
