/*
 * CollitionJefe.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef COLLITIONJEFE_H_
#define COLLITIONJEFE_H_

#include "GalacticaTipos.h"

int CollitionJefe_colisionDisparoALaxo(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPoderGalacticaALaxo(CollitionJefe* collitionJefe, Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPoderCarafeALaxo(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPoderHelixALaxo(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPoderArpALaxo(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPoderPerseusALaxo(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPoderPiscesALaxo(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionDisparoABrazo1Esp(CollitionJefe* collitionJefe,Vector2 PosBrazo, float Rotacion);
int CollitionJefe_colisionDisparoABrazo2Esp(CollitionJefe* collitionJefe,Vector2 PosBrazo, float Rotacion);
int CollitionJefe_colisionPGalacticaABrazo1(CollitionJefe* collitionJefe,Vector2 PosBrazo, float Rotacion);
int CollitionJefe_colisionPGalacticaABrazo2(CollitionJefe* collitionJefe,Vector2 PosBrazo, float Rotacion);
int CollitionJefe_colisionPCarafeABrazo1(CollitionJefe* collitionJefe,Vector2 PosBrazo, float Rotacion);
int CollitionJefe_colisionPCarafeABrazo2(CollitionJefe* collitionJefe,Vector2 PosBrazo, float Rotacion);
int CollitionJefe_colisionPHelixABrazo1(CollitionJefe* collitionJefe,Vector2 PosBrazo, float Rotacion);
int CollitionJefe_colisionPHelixABrazo2(CollitionJefe* collitionJefe,Vector2 PosBrazo, float Rotacion);
int CollitionJefe_colisionPPiscesABrazo1(CollitionJefe* collitionJefe,Vector2 PosBrazo, float Rotacion);
int CollitionJefe_colisionPPiscesABrazo2(CollitionJefe* collitionJefe,Vector2 PosBrazo, float Rotacion);
int CollitionJefe_colisionDisparoAEsp(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPGalacticaAEsp(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPCarafeAEsp(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPHelixAEsp(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPArpAEsp(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPPerseusAEsp(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPPiscesAEsp(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionDisparoAQuad(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPGalacticaAQuad(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPCarafeAQuad(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPHelixAQuad(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPPiscesAQuad(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionDisparoAMartilloQuad(CollitionJefe* collitionJefe,Vector2 PosMartillo);
int CollitionJefe_colisionPGalacticaAMartilloQuad(CollitionJefe* collitionJefe,Vector2 PosMartillo);
int CollitionJefe_colisionPCarafeAMartilloQuad(CollitionJefe* collitionJefe,Vector2 PosMartillo);
int CollitionJefe_colisionPHelixAMartilloQuad(CollitionJefe* collitionJefe,Vector2 PosMartillo);
int CollitionJefe_colisionPArpAMartilloQuad(CollitionJefe* collitionJefe,Vector2 PosMartillo);
int CollitionJefe_colisionPPerseusAMartilloQuad(CollitionJefe* collitionJefe,Vector2 PosMartillo);
int CollitionJefe_colisionPPiscesAMartilloQuad(CollitionJefe* collitionJefe,Vector2 PosMartillo);
int CollitionJefe_colisionDisparoARued(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionDisparoAFlechaRued(CollitionJefe* collitionJefe,Vector2 PosFlecha);
int CollitionJefe_colisionPGalacticaARued(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPCarafeARued(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPHelixARued(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPPiscesARued(CollitionJefe* collitionJefe,Jefe* jefe, Jefe_tipo tipoJefe);
int CollitionJefe_colisionPGalacticaAFlechaRued(CollitionJefe* collitionJefe,Vector2 PosFlecha);
int CollitionJefe_colisionPCarafeAFlechaRued(CollitionJefe* collitionJefe,Vector2 PosFlecha);
int CollitionJefe_colisionPHelixAFlechaRued(CollitionJefe* collitionJefe,Vector2 PosFlecha);
int CollitionJefe_colisionPArpAFlechaRued(CollitionJefe* collitionJefe,Vector2 PosFlecha);
int CollitionJefe_colisionPPerseusAFlechaRued(CollitionJefe* collitionJefe,Vector2 PosFlecha);
int CollitionJefe_colisionPPiscesAFlechaRued(CollitionJefe* collitionJefe,Vector2 PosFlecha);
int CollitionJefe_colisionNaveALaxo(CollitionJefe* collitionJefe,Jefe* jefe);
int CollitionJefe_colisionNaveAEsp(CollitionJefe* collitionJefe,Jefe* jefe);
int CollitionJefe_colisionNaveABrazo1Esp(CollitionJefe* collitionJefe,Vector2 PosBrazo, float Rotacion);
int CollitionJefe_colisionNaveABrazo2Esp(CollitionJefe* collitionJefe,Vector2 PosBrazo, float Rotacion);
int CollitionJefe_colisionNaveAQuad(CollitionJefe* collitionJefe,Jefe* jefe);
int CollitionJefe_colisionNaveAMartilloQuad(CollitionJefe* collitionJefe,Vector2 PosMartillo);
int CollitionJefe_colisionNaveARued(CollitionJefe* collitionJefe,Jefe* jefe);
int CollitionJefe_colisionNaveAFlechaRued(CollitionJefe* collitionJefe,Vector2 PosFlecha);
void CollitionJefe_confirmaEliminaPoderCarafe(int indice);
void CollitionJefe_confirmaEliminaDisparoNave(int indice);
CollitionJefe* CollitionJefe_getSObject();

#endif /* COLLITIONJEFE_H_ */
