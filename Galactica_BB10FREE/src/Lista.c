/*
 * Lista.c
 *
 *  Created on: 21/02/2013
 *      Author: Jarts
 */
#include "Lista.h"
#include <stdlib.h>
#include <stdio.h>

void Lista_Inicia(Lista *lista)
{
	(*lista).inicio = 0;
	(*lista).fin = 0;
	(*lista).size = 0;
}

// insercion en une lista vacia
int Lista_InsertaEnVacia(Lista* lista, void* dato)
{
	Elemento *nuevo_elemento;

	//asignar memoria al elemento
	if ((nuevo_elemento = (Elemento*) malloc(sizeof(Elemento))) == 0)
		return -1;

	//guardar direccion de dato
	(*nuevo_elemento).dato = dato;

	//strcpy ((*nuevo_elemento).dato, dato);

	//Inicio y fin al primer elemento
	(*nuevo_elemento).siguiente = 0; //NULL==0
	(*lista).inicio = nuevo_elemento;
	(*lista).fin = nuevo_elemento;
	(*lista).size++;
	return 0;
}

/* inserci�n al inicio de la lista */
int Lista_InsertaEnInicio(Lista* lista, void* dato)
{

	if ((*lista).size < 1)
	{
		Lista_InsertaEnVacia(lista, dato);
		return 0;
	}

	Elemento *nuevo_elemento;
	if ((nuevo_elemento = (Elemento *) malloc(sizeof(Elemento))) == NULL)
		return -1;

	//guardar direccion de dato
	(*nuevo_elemento).dato = dato;

	/*if ((nuevo_elemento->dato = (char *) malloc (50 * sizeof (char)))
	 == NULL)
	 return -1;
	 strcpy (nuevo_elemento->dato, dato);*/

	(*nuevo_elemento).siguiente = (*lista).inicio;
	(*lista).inicio = nuevo_elemento;
	(*lista).size++;
	return 0;
}

/*insercion al final de la lista */
int Lista_InsertaEnFinal(Lista * lista, Elemento* actual, void *dato)
{

	if ((*lista).size < 1)
	{
		Lista_InsertaEnVacia(lista, dato);
		return 0;
	}

	Elemento *nuevo_elemento;
	if ((nuevo_elemento = (Elemento *) malloc(sizeof(Elemento))) == NULL)
		return -1;

	//guardar direccion de dato
	(*nuevo_elemento).dato = dato;

	(*actual).siguiente = nuevo_elemento;
	(*nuevo_elemento).siguiente = NULL;

	(*lista).fin = nuevo_elemento;

	(*lista).size++;
	return 0;
}

/*insercion al final de la lista */
int Lista_AddInFin(Lista * lista, void *dato)
{

	if ((*lista).size < 1)
	{
		Lista_InsertaEnVacia(lista, dato);
		return 0;
	}

	Elemento *nuevo_elemento;
	if ((nuevo_elemento = (Elemento *) malloc(sizeof(Elemento))) == NULL)
		return -1;

	//guardar direccion de dato
	(*nuevo_elemento).dato = dato;
	(*(*lista).fin).siguiente = nuevo_elemento;
	(*nuevo_elemento).siguiente = NULL;
	(*lista).fin=nuevo_elemento;

	(*lista).size++;
	return 0;
}

/* insercion en la posicion solicitada */
int Lista_InsertaEnIndice(Lista* lista, void *dato, int pos)
{

	if ((*lista).size < 2)
		return -1;
	if (pos < 1 || pos >= (*lista).size)
		return -1;

	Elemento *actual;
	Elemento *nuevo_elemento;

	int i;

	if ((nuevo_elemento = (Elemento *) malloc(sizeof(Elemento))) == NULL)
		return -1;

	//guardar direccion de dato
	(*nuevo_elemento).dato = dato;

	actual = (*lista).inicio;
	for (i = 1; i < pos; ++i)
		actual = (*actual).siguiente;
	if ((*actual).siguiente == 0)
		return -1;

	(*nuevo_elemento).siguiente = (*actual).siguiente;
	(*actual).siguiente = nuevo_elemento;
	(*lista).size++;
	return 0;
}

/* supresi�n al inicio de la lista */
int Lista_SuprimeInicio(Lista * lista)
{
	if ((*lista).size == 0)
		return -1;
	Elemento *sup_elemento;
	sup_elemento = (*lista).inicio;
	(*lista).inicio = (*(*lista).inicio).siguiente;
	if ((*lista).size == 1)
		(*lista).fin = 0;
	//free ((*sup_elemento).dato);
	//free(sup_elemento);
	(*lista).size--;
	return 0;
}

/* suprimir un elemento despu�s de la posici�n solicitada */
int Lista_SuprimeElemento(Lista * lista, int pos)
{
	/*if ((*lista).size <= 1 || pos < 1 || pos >= (*lista).size)
	 return -1;*/
	if (pos < 0 || pos >= (*lista).size)
	{
		return -1;
	}
	if ((*lista).size == 1)
	{
		Lista_Destruir(lista);
		return 0;
	}
	int i;
	Elemento *actual, *anterior;
	Elemento *sup_elemento;
	actual = (*lista).inicio;
	anterior = (*lista).inicio;

	for (i = 0; i < pos; i++)
	{
		actual = (*actual).siguiente;
	}

	if ((*actual).siguiente == NULL)
	{
		for (i = 0; i < pos - 1; i++)
		{
			anterior = (*anterior).siguiente;
		}
		sup_elemento = actual;
		(*lista).fin = anterior;
	}
	else if (pos != 0)
	{
		for (i = 0; i < pos - 1; i++)
		{
			anterior = (*anterior).siguiente;
		}
		(*anterior).siguiente = (*actual).siguiente;
		sup_elemento = actual;
	}
	else
	{
		sup_elemento = actual;
		(*lista).inicio = (*actual).siguiente;
	}

	//free ((*sup_elemento).dato);
	//free(sup_elemento);
	(*(*lista).fin).siguiente=NULL;
	(*lista).size--;
	return 0;
}

/* suprimir un elemento en la posici�n solicitada */
int Lista_RemoveAt(Lista * lista, int pos)
{
	if (pos < 0 || pos >= (*lista).size)
	{
		return -1;
	}
	if ((*lista).size == 1)
	{
		Lista_Destruir(lista);
		Lista_Inicia(lista);
		return 0;
	}
	int i;
	Elemento *actual, *anterior;
	Elemento *sup_elemento;
	actual = (*lista).inicio;
	anterior = (*lista).inicio;

	for (i = 0; i < pos; i++)
	{
		actual = (*actual).siguiente;
	}

	if ((*actual).siguiente == NULL)
	{
		for (i = 0; i < pos - 1; i++)
		{
			anterior = (*anterior).siguiente;
		}
		sup_elemento = actual;
		(*lista).fin = anterior;
	}
	else
	{
		if (pos != 0)
		{
			for (i = 0; i < pos - 1; i++)
			{
				anterior = (*anterior).siguiente;
			}
			(*anterior).siguiente = (*actual).siguiente;
			sup_elemento = actual;
		}
		else
		{
			sup_elemento = actual;
			(*lista).inicio = (*actual).siguiente;
		}
	}

	//free ((*sup_elemento).dato);
	//free(sup_elemento);
	(*(*lista).fin).siguiente=NULL;
	(*lista).size--;
	return 0;
}

/*  Retornar un elemento*/
void* Lista_RetornaElemento(Lista * lista, int pos)
{
	if (pos >= (*lista).size)
	{
		return NULL;
	}

	int i;
	Elemento *actual;
	actual = (*lista).inicio;

	for (i = 0; i < pos; i++)
	{
		actual = (*actual).siguiente;
	}

	return (*actual).dato;
}

/*  Retornar un elemento*/
void* Lista_GetIn(Lista * lista, int pos)
{
	if (pos >= (*lista).size)
	{
		return NULL;
	}

	int i;
	Elemento *actual;
	actual = (*lista).inicio;

	for (i = 0; i < pos; i++)
	{
		actual = (*actual).siguiente;
	}

	return (*actual).dato;
}

/* visualizaci�n de la Lista */
void Lista_ToString(Lista * lista)
{
	Elemento *actual;
	actual = (*lista).inicio;
	while (actual != 0)
	{
		printf("%p - %s\n", actual, (char*) ((*actual).dato));
		actual = (*actual).siguiente;
	}
}

/* destruir la Lista */
void Lista_Destruir(Lista * lista)
{
	while ((*lista).size > 0)
		Lista_SuprimeInicio(lista);

	Lista_Inicia(lista);
}

/*int menu (Lista *lista,int *k){
 int elecci�n;
 printf("********** MENU **********\n");
 if (lista ->tama�o == 0){
 printf ("1. Adici�n del1er elemento\n");
 printf ("2. Quitar\n");
 }else if(lista ->tama�o == 1 || *k == 1){
 printf ("1. Adici�n al inicio de la lista\n");
 printf ("2. Adici�n al final de la lista\n");
 printf ("4. Supresi�n al inicio de la lista\n");
 printf ("6. Destruir la lista\n");
 printf ("7. Quitar\n");
 }else {
 printf ("1. Adici�n al inicio de la lista\n");
 printf ("2. Adici�n al final de la lista\n");
 printf ("3. Adici�n despu�s de la posici�n indicada\n");
 printf ("4. Supresi�n al inicio de la lista\n");
 printf ("5. Supresi�n despu�s de la posici�n indicada\n");
 printf ("6. Destruir la lista\n");
 printf ("7. Quitar\n");
 }
 printf ("\n\nElegir: ");
 scanf ("%d", &elecci�n);
 getchar();
 if(lista->tama�o == 0 && elecci�n == 2)
 elecci�n = 7;
 return elecci�n;
 }*/

