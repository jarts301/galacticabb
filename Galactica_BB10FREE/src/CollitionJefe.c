/*
 * CollitionJefe.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "CollitionJefe.h"
#include "Jefe.h"
#include "Nave.h"
#include "Poderes.h"
#include <math.h>

static CollitionJefe CollitionJefeS;

int CollitionJefe_colisionDisparoALaxo(CollitionJefe* collitionJefe, Jefe* jefe,
		Jefe_tipo tipoJefe)
{
	//Colision zona no da�o Laxo
	if (tipoJefe == JLaxo && (*Nave_getSObject()).disparos.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).disparos.size;
				CollitionJefeS.i++)
		{
			(*collitionJefe).distancia = (float) sqrt(
					pow(
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionJefeS.i)).Position.X
									- (*jefe).Position.X, 2)
							+ pow(
									(*(Disparo*) Lista_RetornaElemento(
											&((*Nave_getSObject()).disparos),
											CollitionJefeS.i)).Position.Y
											- (*jefe).Position.Y, 2));

			if ((*collitionJefe).distancia
					<= (*(Disparo*) Lista_RetornaElemento(
							&((*Nave_getSObject()).disparos), CollitionJefeS.i)).radioCollition
							+ (*Jefe_getSObject()).radioCollitionLaxo)
			{
				CollitionJefeS.positionUltimoDisparo.X =
						(*(Disparo*) Lista_RetornaElemento(
								&((*Nave_getSObject()).disparos),
								CollitionJefeS.i)).Position.X;
				CollitionJefeS.positionUltimoDisparo.Y =
						(*(Disparo*) Lista_RetornaElemento(
								&((*Nave_getSObject()).disparos),
								CollitionJefeS.i)).Position.Y;
				CollitionJefeS.ultimoTipoColision = 0;
				CollitionJefe_confirmaEliminaDisparoNave(CollitionJefeS.i);
				return 1;
			}
		}
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).disparos.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 3; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Disparo*) Lista_RetornaElemento(
												&((*Nave_getSObject()).disparos),
												CollitionJefeS.i)).Position.X
												- ((*jefe).Position.X
														+ (*Jefe_getSObject()).collitionLaxoX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Disparo*) Lista_RetornaElemento(
														&((*Nave_getSObject()).disparos),
														CollitionJefeS.i)).Position.Y
														- ((*jefe).Position.Y
																+ (*Jefe_getSObject()).collitionLaxoY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*(Disparo*) Lista_RetornaElemento(
								&((*Nave_getSObject()).disparos),
								CollitionJefeS.i)).radioCollition
								+ (*Jefe_getSObject()).radioCollitionDanoLaxo)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionJefeS.i)).Position.Y;
					CollitionJefeS.ultimoTipoColision = 1;
					CollitionJefe_confirmaEliminaDisparoNave(CollitionJefeS.i);
					return 1;
				}
			}
		}
	}

	return 0;
}

//*Colision PoderGalactica a Laxo
int CollitionJefe_colisionPoderGalacticaALaxo(CollitionJefe* collitionJefe,
		Jefe* jefe, Jefe_tipo tipoJefe)
{
	if (tipoJefe == JLaxo && (*Nave_getSObject()).poderGalactica.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderGalactica.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 3; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderGalactica),
												CollitionJefeS.i)).Position.X
												- ((*jefe).Position.X
														+ (*Jefe_getSObject()).collitionLaxoX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderGalactica),
														CollitionJefeS.i)).Position.Y
														- ((*jefe).Position.Y
																+ (*Jefe_getSObject()).collitionLaxoY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPGalactica
								+ (*Jefe_getSObject()).radioCollitionDanoLaxo)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionJefeS.i)).Position.Y;
					CollitionJefeS.ultimoTipoColision = 1;

					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderGalacticaTotal),
							(*Nave_getSObject()).poderGalacticaTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderGalactica),
							CollitionJefeS.i);
					return 1;
				}
			}
		}
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderGalactica.size;
				CollitionJefeS.i++)
		{
			(*collitionJefe).distancia =
					(float) sqrt(
							pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderGalactica),
											CollitionJefeS.i)).Position.X
											- (*jefe).Position.X, 2)
									+ pow(
											(*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderGalactica),
													CollitionJefeS.i)).Position.Y
													- (*jefe).Position.Y, 2));

			if ((*collitionJefe).distancia
					<= (*Poderes_getSObject()).radioCollitPGalactica
							+ (*Jefe_getSObject()).radioCollitionLaxo)
			{
				CollitionJefeS.positionUltimoDisparo.X =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderGalactica),
								CollitionJefeS.i)).Position.X;
				CollitionJefeS.positionUltimoDisparo.Y =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderGalactica),
								CollitionJefeS.i)).Position.Y;
				CollitionJefeS.ultimoTipoColision = 0;

				Lista_InsertaEnFinal(
						&((*Nave_getSObject()).poderGalacticaTotal),
						(*Nave_getSObject()).poderGalacticaTotal.fin,
						Lista_RetornaElemento(
								&((*Nave_getSObject()).poderGalactica),
								CollitionJefeS.i));

				Lista_RemoveAt(&((*Nave_getSObject()).poderGalactica),
						CollitionJefeS.i);
				return 1;
			}
		}
	}

	return 0;
}

//*Colision PoderCarafe a Laxo
int CollitionJefe_colisionPoderCarafeALaxo(CollitionJefe* collitionJefe,
		Jefe* jefe, Jefe_tipo tipoJefe)
{
	if (tipoJefe == JLaxo && (*Nave_getSObject()).poderCarafe.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderCarafe.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 3; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderCarafe),
												CollitionJefeS.i)).Position.X
												- ((*jefe).Position.X
														+ (*Jefe_getSObject()).collitionLaxoX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderCarafe),
														CollitionJefeS.i)).Position.Y
														- ((*jefe).Position.Y
																+ (*Jefe_getSObject()).collitionLaxoY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPCarafe
								+ (*Jefe_getSObject()).radioCollitionDanoLaxo)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionJefeS.i)).Position.Y;
					CollitionJefeS.ultimoTipoColision = 1;
					CollitionJefe_confirmaEliminaPoderCarafe(CollitionJefeS.i);
					return 1;
				}
			}
		}
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderCarafe.size;
				CollitionJefeS.i++)
		{
			(*collitionJefe).distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionJefeS.i)).Position.X
									- (*jefe).Position.X, 2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderCarafe),
											CollitionJefeS.i)).Position.Y
											- (*jefe).Position.Y, 2));

			if ((*collitionJefe).distancia
					<= (*Poderes_getSObject()).radioCollitPCarafe
							+ (*Jefe_getSObject()).radioCollitionLaxo)
			{
				CollitionJefeS.positionUltimoDisparo.X =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderCarafe),
								CollitionJefeS.i)).Position.X;
				CollitionJefeS.positionUltimoDisparo.Y =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderCarafe),
								CollitionJefeS.i)).Position.Y;
				CollitionJefeS.ultimoTipoColision = 0;
				CollitionJefe_confirmaEliminaPoderCarafe(CollitionJefeS.i);
				return 1;
			}
		}
	}

	return 0;
}

//*Colision PoderHelix a Laxo
int CollitionJefe_colisionPoderHelixALaxo(CollitionJefe* collitionJefe,
		Jefe* jefe, Jefe_tipo tipoJefe)
{
	for (CollitionJefeS.i = 0;
			CollitionJefeS.i < (*Nave_getSObject()).poderHelix.size;
			CollitionJefeS.i++)
	{
		for (CollitionJefeS.j = 0; CollitionJefeS.j < 3; CollitionJefeS.j++)
		{
			(*collitionJefe).distancia =
					(float) sqrt(
							pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderHelix),
											CollitionJefeS.i)).Position.X
											- ((*jefe).Position.X
													+ (*Jefe_getSObject()).collitionLaxoX[CollitionJefeS.j]),
									2)
									+ pow(
											(*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderHelix),
													CollitionJefeS.i)).Position.Y
													- ((*jefe).Position.Y
															+ (*Jefe_getSObject()).collitionLaxoY[CollitionJefeS.j]),
											2));

			if ((*collitionJefe).distancia
					<= (*Poderes_getSObject()).radioCollitPHelix
							+ (*Jefe_getSObject()).radioCollitionDanoLaxo)
			{
				CollitionJefeS.positionUltimoDisparo.X =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderHelix),
								CollitionJefeS.i)).Position.X;
				CollitionJefeS.positionUltimoDisparo.Y =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderHelix),
								CollitionJefeS.i)).Position.Y;
				CollitionJefeS.ultimoTipoColision = 1;

				Lista_InsertaEnFinal(&((*Nave_getSObject()).poderHelixTotal),
						(*Nave_getSObject()).poderHelixTotal.fin,
						Lista_RetornaElemento(
								&((*Nave_getSObject()).poderHelix),
								CollitionJefeS.i));

				Lista_RemoveAt(&((*Nave_getSObject()).poderHelix),
						CollitionJefeS.i);
				return 1;
			}
		}
	}
	for (CollitionJefeS.i = 0;
			CollitionJefeS.i < (*Nave_getSObject()).poderHelix.size;
			CollitionJefeS.i++)
	{
		(*collitionJefe).distancia = (float) sqrt(
				pow(
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderHelix),
								CollitionJefeS.i)).Position.X
								- (*jefe).Position.X, 2)
						+ pow(
								(*(Poderes*) Lista_RetornaElemento(
										&((*Nave_getSObject()).poderHelix),
										CollitionJefeS.i)).Position.Y
										- (*jefe).Position.Y, 2));

		if ((*collitionJefe).distancia
				<= (*Poderes_getSObject()).radioCollitPHelix
						+ (*Jefe_getSObject()).radioCollitionLaxo)
		{
			CollitionJefeS.positionUltimoDisparo.X =
					(*(Poderes*) Lista_RetornaElemento(
							&((*Nave_getSObject()).poderHelix),
							CollitionJefeS.i)).Position.X;
			CollitionJefeS.positionUltimoDisparo.Y =
					(*(Poderes*) Lista_RetornaElemento(
							&((*Nave_getSObject()).poderHelix),
							CollitionJefeS.i)).Position.Y;
			CollitionJefeS.ultimoTipoColision = 0;

			Lista_InsertaEnFinal(&((*Nave_getSObject()).poderHelixTotal),
					(*Nave_getSObject()).poderHelixTotal.fin,
					Lista_RetornaElemento(&((*Nave_getSObject()).poderHelix),
							CollitionJefeS.i));

			Lista_RemoveAt(&((*Nave_getSObject()).poderHelix),
					CollitionJefeS.i);
			return 1;
		}
	}

	return 0;
}

//*Colision Poder Arp a Laxo
int CollitionJefe_colisionPoderArpALaxo(CollitionJefe* collitionJefe,
		Jefe* jefe, Jefe_tipo tipoJefe)
{
	for (CollitionJefeS.i = 0;
			CollitionJefeS.i < (*Nave_getSObject()).poderArp.size;
			CollitionJefeS.i++)
	{
		for (CollitionJefeS.w = 0; CollitionJefeS.w < 6; CollitionJefeS.w++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 3; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										((*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderArp),
												CollitionJefeS.i)).Position.X
												+ (*Poderes_getSObject()).collitionPArpX[CollitionJefeS.w])
												- ((*jefe).Position.X
														+ (*Jefe_getSObject()).collitionLaxoX[CollitionJefeS.j]),
										2)
										+ pow(
												((*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderArp),
														CollitionJefeS.i)).Position.Y
														+ (*Poderes_getSObject()).collitionPArpY[CollitionJefeS.w])
														- ((*jefe).Position.Y
																+ (*Jefe_getSObject()).collitionLaxoY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPArp
								+ (*Jefe_getSObject()).radioCollitionDanoLaxo)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderArp),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderArp),
									CollitionJefeS.i)).Position.Y;
					CollitionJefeS.ultimoTipoColision = 1;

					Lista_InsertaEnFinal(&((*Nave_getSObject()).poderArpTotal),
							(*Nave_getSObject()).poderArpTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderArp),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderArp),
							CollitionJefeS.i);
					return 1;
				}
			}
		}
	}
	for (CollitionJefeS.i = 0;
			CollitionJefeS.i < (*Nave_getSObject()).poderArp.size;
			CollitionJefeS.i++)
	{
		for (CollitionJefeS.w = 0; CollitionJefeS.w < 6; CollitionJefeS.w++)
		{
			(*collitionJefe).distancia =
					(float) sqrt(
							pow(
									((*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderArp),
											CollitionJefeS.i)).Position.X
											+ (*Poderes_getSObject()).collitionPArpX[CollitionJefeS.w])
											- (*jefe).Position.X, 2)
									+ pow(
											((*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderArp),
													CollitionJefeS.i)).Position.Y
													+ (*Poderes_getSObject()).collitionPArpY[CollitionJefeS.w])
													- (*jefe).Position.Y, 2));

			if ((*collitionJefe).distancia
					<= (*Poderes_getSObject()).radioCollitPArp
							+ (*Jefe_getSObject()).radioCollitionLaxo)
			{
				CollitionJefeS.positionUltimoDisparo.X =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderArp),
								CollitionJefeS.i)).Position.X;
				CollitionJefeS.positionUltimoDisparo.Y =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderArp),
								CollitionJefeS.i)).Position.Y;
				CollitionJefeS.ultimoTipoColision = 0;
				return 1;
			}
		}
	}
	return 0;
}

//*Colision Poder Perseus a Laxo
int CollitionJefe_colisionPoderPerseusALaxo(CollitionJefe* collitionJefe,
		Jefe* jefe, Jefe_tipo tipoJefe)
{
	for (CollitionJefeS.i = 0;
			CollitionJefeS.i < (*Nave_getSObject()).poderPerseus.size;
			CollitionJefeS.i++)
	{
		for (CollitionJefeS.j = 0; CollitionJefeS.j < 3; CollitionJefeS.j++)
		{
			(*collitionJefe).distancia =
					(float) sqrt(
							pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderPerseus),
											CollitionJefeS.i)).Position.X
											- ((*jefe).Position.X
													+ (*Jefe_getSObject()).collitionLaxoX[CollitionJefeS.j]),
									2)
									+ pow(
											(*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderPerseus),
													CollitionJefeS.i)).Position.Y
													- ((*jefe).Position.Y
															+ (*Jefe_getSObject()).collitionLaxoY[CollitionJefeS.j]),
											2));

			if ((*collitionJefe).distancia
					<= (*Poderes_getSObject()).radioCollitPPerseus
							+ (*Jefe_getSObject()).radioCollitionDanoLaxo)
			{
				CollitionJefeS.positionUltimoDisparo.X = (*jefe).Position.X;
				CollitionJefeS.positionUltimoDisparo.Y = (*jefe).Position.Y;
				CollitionJefeS.ultimoTipoColision = 1;
				(*(Poderes*) Lista_RetornaElemento(
						&((*Nave_getSObject()).poderPerseus), CollitionJefeS.i)).Escala =
						GameFramework_Vector2(0.1f, 0.1f);

				Lista_InsertaEnFinal(&((*Nave_getSObject()).poderPerseusTotal),
						(*Nave_getSObject()).poderPerseusTotal.fin,
						Lista_RetornaElemento(
								&((*Nave_getSObject()).poderPerseus),
								CollitionJefeS.i));

				Lista_RemoveAt(&((*Nave_getSObject()).poderPerseus),
						CollitionJefeS.i);
				return 1;
			}
		}
	}
	for (CollitionJefeS.i = 0;
			CollitionJefeS.i < (*Nave_getSObject()).poderPerseus.size;
			CollitionJefeS.i++)
	{
		(*collitionJefe).distancia = (float) sqrt(
				pow(
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderPerseus),
								CollitionJefeS.i)).Position.X
								- (*jefe).Position.X, 2)
						+ pow(
								(*(Poderes*) Lista_RetornaElemento(
										&((*Nave_getSObject()).poderPerseus),
										CollitionJefeS.i)).Position.Y
										- (*jefe).Position.Y, 2));

		if ((*collitionJefe).distancia
				<= (*Poderes_getSObject()).radioCollitPPerseus
						+ (*Jefe_getSObject()).radioCollitionLaxo)
		{
			CollitionJefeS.positionUltimoDisparo.X = (*jefe).Position.X;
			CollitionJefeS.positionUltimoDisparo.Y = (*jefe).Position.Y;
			CollitionJefeS.ultimoTipoColision = 0;
			return 1;
		}
	}
	return 0;
}

//*Colision Poder Pisces a Laxo
int CollitionJefe_colisionPoderPiscesALaxo(CollitionJefe* collitionJefe,
		Jefe* jefe, Jefe_tipo tipoJefe)
{
	for (CollitionJefeS.i = 0;
			CollitionJefeS.i < (*Nave_getSObject()).poderPisces.size;
			CollitionJefeS.i++)
	{
		for (CollitionJefeS.j = 0; CollitionJefeS.j < 3; CollitionJefeS.j++)
		{
			(*collitionJefe).distancia =
					(float) sqrt(
							pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderPisces),
											CollitionJefeS.i)).Position.X
											- ((*jefe).Position.X
													+ (*Jefe_getSObject()).collitionLaxoX[CollitionJefeS.j]),
									2)
									+ pow(
											(*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderPisces),
													CollitionJefeS.i)).Position.Y
													- ((*jefe).Position.Y
															+ (*Jefe_getSObject()).collitionLaxoY[CollitionJefeS.j]),
											2));

			if ((*collitionJefe).distancia
					<= (*Poderes_getSObject()).radioCollitPPisces
							+ (*Jefe_getSObject()).radioCollitionDanoLaxo)
			{
				CollitionJefeS.positionUltimoDisparo.X =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderPisces),
								CollitionJefeS.i)).Position.X;
				CollitionJefeS.positionUltimoDisparo.Y =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderPisces),
								CollitionJefeS.i)).Position.Y;
				CollitionJefeS.ultimoTipoColision = 1;

				Lista_InsertaEnFinal(&((*Nave_getSObject()).poderPiscesTotal),
						(*Nave_getSObject()).poderPiscesTotal.fin,
						Lista_RetornaElemento(
								&((*Nave_getSObject()).poderPisces),
								CollitionJefeS.i));

				Lista_RemoveAt(&((*Nave_getSObject()).poderPisces),
						CollitionJefeS.i);
				return 1;
			}
		}
	}
	for (CollitionJefeS.i = 0;
			CollitionJefeS.i < (*Nave_getSObject()).poderPisces.size;
			CollitionJefeS.i++)
	{
		(*collitionJefe).distancia = (float) sqrt(
				pow(
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderPisces),
								CollitionJefeS.i)).Position.X
								- (*jefe).Position.X, 2)
						+ pow(
								(*(Poderes*) Lista_RetornaElemento(
										&((*Nave_getSObject()).poderPisces),
										CollitionJefeS.i)).Position.Y
										- (*jefe).Position.Y, 2));

		if ((*collitionJefe).distancia
				<= (*Poderes_getSObject()).radioCollitPPisces
						+ (*Jefe_getSObject()).radioCollitionLaxo)
		{
			CollitionJefeS.positionUltimoDisparo.X =
					(*(Poderes*) Lista_RetornaElemento(
							&((*Nave_getSObject()).poderPisces),
							CollitionJefeS.i)).Position.X;
			CollitionJefeS.positionUltimoDisparo.Y =
					(*(Poderes*) Lista_RetornaElemento(
							&((*Nave_getSObject()).poderPisces),
							CollitionJefeS.i)).Position.Y;
			CollitionJefeS.ultimoTipoColision = 0;

			Lista_InsertaEnFinal(&((*Nave_getSObject()).poderPiscesTotal),
					(*Nave_getSObject()).poderPiscesTotal.fin,
					Lista_RetornaElemento(&((*Nave_getSObject()).poderPisces),
							CollitionJefeS.i));

			Lista_RemoveAt(&((*Nave_getSObject()).poderPisces),
					CollitionJefeS.i);
			;
			return 1;
		}
	}
	return 0;
}

//Colision disparo a brazo 1 esp
int CollitionJefe_colisionDisparoABrazo1Esp(CollitionJefe* collitionJefe,
		Vector2 PosBrazo, float Rotacion)
{
	if ((*Nave_getSObject()).disparos.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).disparos.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 9; CollitionJefeS.j++)
			{
				(*collitionJefe).X1p =
						((*Jefe_getSObject()).collitionBrazoEspX[CollitionJefeS.j]);
				(*collitionJefe).Y1p =
						((*Jefe_getSObject()).collitionBrazoEspY[CollitionJefeS.j]);

				(*collitionJefe).X1 = (PosBrazo.X)
						+ (((*collitionJefe).X1p * cos(
								GameFramework_ToRadians(Rotacion)))
								- ((*collitionJefe).Y1p * sin(
										GameFramework_ToRadians(
												Rotacion))));
				(*collitionJefe).Y1 = (PosBrazo.Y)
						+ (((*collitionJefe).X1p * sin(
								GameFramework_ToRadians(Rotacion)))
								+ ((*collitionJefe).Y1p * cos(
										GameFramework_ToRadians(
												Rotacion))));

				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Disparo*) Lista_RetornaElemento(
												&((*Nave_getSObject()).disparos),
												CollitionJefeS.i)).Position.X
												- (*collitionJefe).X1, 2)
										+ pow(
												(*(Disparo*) Lista_RetornaElemento(
														&((*Nave_getSObject()).disparos),
														CollitionJefeS.i)).Position.Y
														- (*collitionJefe).Y1,
												2));

				if ((*collitionJefe).distancia
						<= (*(Disparo*) Lista_RetornaElemento(
								&((*Nave_getSObject()).disparos),
								CollitionJefeS.i)).radioCollition
								+ (*Jefe_getSObject()).radioCollitionBrazoEsp)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionJefeS.i)).Position.Y;
					CollitionJefe_confirmaEliminaDisparoNave(CollitionJefeS.i);
					return 1;
				}
			}

		}
	}
	return 0;
}

//**********Colision Disparo a brazo 2 esp
int CollitionJefe_colisionDisparoABrazo2Esp(CollitionJefe* collitionJefe,
		Vector2 PosBrazo, float Rotacion)
{
	if ((*Nave_getSObject()).disparos.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).disparos.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 9; CollitionJefeS.j < 18;
					CollitionJefeS.j++)
			{
				(*collitionJefe).X1p =
						((*Jefe_getSObject()).collitionBrazoEspX[CollitionJefeS.j]);
				(*collitionJefe).Y1p =
						((*Jefe_getSObject()).collitionBrazoEspY[CollitionJefeS.j]);

				(*collitionJefe).X1 = (PosBrazo.X)
						+ (((*collitionJefe).X1p * cos(
								GameFramework_ToRadians(Rotacion)))
								- ((*collitionJefe).Y1p * sin(
										GameFramework_ToRadians(
												Rotacion))));
				(*collitionJefe).Y1 = (PosBrazo.Y)
						+ (((*collitionJefe).X1p * sin(
								GameFramework_ToRadians(Rotacion)))
								+ ((*collitionJefe).Y1p * cos(
										GameFramework_ToRadians(
												Rotacion))));

				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Disparo*) Lista_RetornaElemento(
												&((*Nave_getSObject()).disparos),
												CollitionJefeS.i)).Position.X
												- (*collitionJefe).X1, 2)
										+ pow(
												(*(Disparo*) Lista_RetornaElemento(
														&((*Nave_getSObject()).disparos),
														CollitionJefeS.i)).Position.Y
														- (*collitionJefe).Y1,
												2));

				if ((*collitionJefe).distancia
						<= (*(Disparo*) Lista_RetornaElemento(
								&((*Nave_getSObject()).disparos),
								CollitionJefeS.i)).radioCollition
								+ (*Jefe_getSObject()).radioCollitionBrazoEsp)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionJefeS.i)).Position.Y;
					CollitionJefe_confirmaEliminaDisparoNave(CollitionJefeS.i);
					return 1;
				}
			}

		}
	}
	return 0;
}

//*******Colision Poder Galactica Brazo 1
int CollitionJefe_colisionPGalacticaABrazo1(CollitionJefe* collitionJefe,
		Vector2 PosBrazo, float Rotacion)
{
	if ((*Nave_getSObject()).poderGalactica.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderGalactica.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 9; CollitionJefeS.j++)
			{
				(*collitionJefe).X1p =
						((*Jefe_getSObject()).collitionBrazoEspX[CollitionJefeS.j]);
				(*collitionJefe).Y1p =
						((*Jefe_getSObject()).collitionBrazoEspY[CollitionJefeS.j]);

				(*collitionJefe).X1 = (PosBrazo.X)
						+ (((*collitionJefe).X1p * cos(
								GameFramework_ToRadians(Rotacion)))
								- ((*collitionJefe).Y1p * sin(
										GameFramework_ToRadians(
												Rotacion))));
				(*collitionJefe).Y1 = (PosBrazo.Y)
						+ (((*collitionJefe).X1p * sin(
								GameFramework_ToRadians(Rotacion)))
								+ ((*collitionJefe).Y1p * cos(
										GameFramework_ToRadians(
												Rotacion))));

				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderGalactica),
												CollitionJefeS.i)).Position.X
												- (*collitionJefe).X1, 2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderGalactica),
														CollitionJefeS.i)).Position.Y
														- (*collitionJefe).Y1,
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPGalactica
								+ (*Jefe_getSObject()).radioCollitionBrazoEsp)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionJefeS.i)).Position.Y;

					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderGalacticaTotal),
							(*Nave_getSObject()).poderGalacticaTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderGalactica),
							CollitionJefeS.i);
					return 1;
				}
			}

		}
	}
	return 0;
}

//*******Colision Poder Galactica Brazo 2
int CollitionJefe_colisionPGalacticaABrazo2(CollitionJefe* collitionJefe,
		Vector2 PosBrazo, float Rotacion)
{
	if ((*Nave_getSObject()).poderGalactica.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderGalactica.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 9; CollitionJefeS.j < 18;
					CollitionJefeS.j++)
			{
				(*collitionJefe).X1p =
						((*Jefe_getSObject()).collitionBrazoEspX[CollitionJefeS.j]);
				(*collitionJefe).Y1p =
						((*Jefe_getSObject()).collitionBrazoEspY[CollitionJefeS.j]);

				(*collitionJefe).X1 = (PosBrazo.X)
						+ (((*collitionJefe).X1p * cos(
								GameFramework_ToRadians(Rotacion)))
								- ((*collitionJefe).Y1p * sin(
										GameFramework_ToRadians(
												Rotacion))));
				(*collitionJefe).Y1 = (PosBrazo.Y)
						+ (((*collitionJefe).X1p * sin(
								GameFramework_ToRadians(Rotacion)))
								+ ((*collitionJefe).Y1p * cos(
										GameFramework_ToRadians(
												Rotacion))));

				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderGalactica),
												CollitionJefeS.i)).Position.X
												- (*collitionJefe).X1, 2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderGalactica),
														CollitionJefeS.i)).Position.Y
														- (*collitionJefe).Y1,
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPGalactica
								+ (*Jefe_getSObject()).radioCollitionBrazoEsp)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionJefeS.i)).Position.Y;

					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderGalacticaTotal),
							(*Nave_getSObject()).poderGalacticaTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderGalactica),
							CollitionJefeS.i);
					return 1;
				}
			}

		}
	}
	return 0;
}

//*******Colision Poder Carafe Brazo 1
int CollitionJefe_colisionPCarafeABrazo1(CollitionJefe* collitionJefe,
		Vector2 PosBrazo, float Rotacion)
{
	if ((*Nave_getSObject()).poderCarafe.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderCarafe.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 9; CollitionJefeS.j++)
			{
				(*collitionJefe).X1p =
						((*Jefe_getSObject()).collitionBrazoEspX[CollitionJefeS.j]);
				(*collitionJefe).Y1p =
						((*Jefe_getSObject()).collitionBrazoEspY[CollitionJefeS.j]);

				(*collitionJefe).X1 = (PosBrazo.X)
						+ (((*collitionJefe).X1p * cos(
								GameFramework_ToRadians(Rotacion)))
								- ((*collitionJefe).Y1p * sin(
										GameFramework_ToRadians(
												Rotacion))));
				(*collitionJefe).Y1 = (PosBrazo.Y)
						+ (((*collitionJefe).X1p * sin(
								GameFramework_ToRadians(Rotacion)))
								+ ((*collitionJefe).Y1p * cos(
										GameFramework_ToRadians(
												Rotacion))));

				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderCarafe),
												CollitionJefeS.i)).Position.X
												- (*collitionJefe).X1, 2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderCarafe),
														CollitionJefeS.i)).Position.Y
														- (*collitionJefe).Y1,
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPCarafe
								+ (*Jefe_getSObject()).radioCollitionBrazoEsp)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionJefeS.i)).Position.Y;
					CollitionJefe_confirmaEliminaPoderCarafe(CollitionJefeS.i);
					return 1;
				}
			}

		}
	}
	return 0;
}

//*******Colision Poder Carafe Brazo 2
int CollitionJefe_colisionPCarafeABrazo2(CollitionJefe* collitionJefe,
		Vector2 PosBrazo, float Rotacion)
{
	if ((*Nave_getSObject()).poderCarafe.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderCarafe.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 9; CollitionJefeS.j < 18;
					CollitionJefeS.j++)
			{
				(*collitionJefe).X1p =
						((*Jefe_getSObject()).collitionBrazoEspX[CollitionJefeS.j]);
				(*collitionJefe).Y1p =
						((*Jefe_getSObject()).collitionBrazoEspY[CollitionJefeS.j]);

				(*collitionJefe).X1 = (PosBrazo.X)
						+ (((*collitionJefe).X1p * cos(
								GameFramework_ToRadians(Rotacion)))
								- ((*collitionJefe).Y1p * sin(
										GameFramework_ToRadians(
												Rotacion))));
				(*collitionJefe).Y1 = (PosBrazo.Y)
						+ (((*collitionJefe).X1p * sin(
								GameFramework_ToRadians(Rotacion)))
								+ ((*collitionJefe).Y1p * cos(
										GameFramework_ToRadians(
												Rotacion))));

				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderCarafe),
												CollitionJefeS.i)).Position.X
												- (*collitionJefe).X1, 2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderCarafe),
														CollitionJefeS.i)).Position.Y
														- (*collitionJefe).Y1,
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPCarafe
								+ (*Jefe_getSObject()).radioCollitionBrazoEsp)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionJefeS.i)).Position.Y;
					CollitionJefe_confirmaEliminaPoderCarafe(CollitionJefeS.i);
					return 1;
				}
			}

		}
	}
	return 0;
}

//*******Colision Poder Helix Brazo 1
int CollitionJefe_colisionPHelixABrazo1(CollitionJefe* collitionJefe,
		Vector2 PosBrazo, float Rotacion)
{
	if ((*Nave_getSObject()).poderHelix.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderHelix.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 9; CollitionJefeS.j++)
			{
				(*collitionJefe).X1p =
						((*Jefe_getSObject()).collitionBrazoEspX[CollitionJefeS.j]);
				(*collitionJefe).Y1p =
						((*Jefe_getSObject()).collitionBrazoEspY[CollitionJefeS.j]);

				(*collitionJefe).X1 = (PosBrazo.X)
						+ (((*collitionJefe).X1p * cos(
								GameFramework_ToRadians(Rotacion)))
								- ((*collitionJefe).Y1p * sin(
										GameFramework_ToRadians(
												Rotacion))));
				(*collitionJefe).Y1 = (PosBrazo.Y)
						+ (((*collitionJefe).X1p * sin(
								GameFramework_ToRadians(Rotacion)))
								+ ((*collitionJefe).Y1p * cos(
										GameFramework_ToRadians(
												Rotacion))));

				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderHelix),
												CollitionJefeS.i)).Position.X
												- (*collitionJefe).X1, 2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderHelix),
														CollitionJefeS.i)).Position.Y
														- (*collitionJefe).Y1,
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPHelix
								+ (*Jefe_getSObject()).radioCollitionBrazoEsp)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionJefeS.i)).Position.Y;

					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderHelixTotal),
							(*Nave_getSObject()).poderHelixTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderHelix),
							CollitionJefeS.i);
					return 1;
				}
			}

		}
	}
	return 0;
}

//*******Colision Poder Helix Brazo 2
int CollitionJefe_colisionPHelixABrazo2(CollitionJefe* collitionJefe,
		Vector2 PosBrazo, float Rotacion)
{
	if ((*Nave_getSObject()).poderHelix.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderHelix.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 9; CollitionJefeS.j < 18;
					CollitionJefeS.j++)
			{
				(*collitionJefe).X1p =
						((*Jefe_getSObject()).collitionBrazoEspX[CollitionJefeS.j]);
				(*collitionJefe).Y1p =
						((*Jefe_getSObject()).collitionBrazoEspY[CollitionJefeS.j]);

				(*collitionJefe).X1 = (PosBrazo.X)
						+ (((*collitionJefe).X1p * cos(
								GameFramework_ToRadians(Rotacion)))
								- ((*collitionJefe).Y1p * sin(
										GameFramework_ToRadians(
												Rotacion))));
				(*collitionJefe).Y1 = (PosBrazo.Y)
						+ (((*collitionJefe).X1p * sin(
								GameFramework_ToRadians(Rotacion)))
								+ ((*collitionJefe).Y1p * cos(
										GameFramework_ToRadians(
												Rotacion))));

				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderHelix),
												CollitionJefeS.i)).Position.X
												- (*collitionJefe).X1, 2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderHelix),
														CollitionJefeS.i)).Position.Y
														- (*collitionJefe).Y1,
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPHelix
								+ (*Jefe_getSObject()).radioCollitionBrazoEsp)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionJefeS.i)).Position.Y;

					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderHelixTotal),
							(*Nave_getSObject()).poderHelixTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderHelix),
							CollitionJefeS.i);
					return 1;
				}
			}

		}
	}
	return 0;
}

//*******Colision Poder Pisces Brazo 1
int CollitionJefe_colisionPPiscesABrazo1(CollitionJefe* collitionJefe,
		Vector2 PosBrazo, float Rotacion)
{
	if ((*Nave_getSObject()).poderPisces.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderPisces.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 9; CollitionJefeS.j++)
			{
				(*collitionJefe).X1p =
						((*Jefe_getSObject()).collitionBrazoEspX[CollitionJefeS.j]);
				(*collitionJefe).Y1p =
						((*Jefe_getSObject()).collitionBrazoEspY[CollitionJefeS.j]);

				(*collitionJefe).X1 = (PosBrazo.X)
						+ (((*collitionJefe).X1p * cos(
								GameFramework_ToRadians(Rotacion)))
								- ((*collitionJefe).Y1p * sin(
										GameFramework_ToRadians(
												Rotacion))));
				(*collitionJefe).Y1 = (PosBrazo.Y)
						+ (((*collitionJefe).X1p * sin(
								GameFramework_ToRadians(Rotacion)))
								+ ((*collitionJefe).Y1p * cos(
										GameFramework_ToRadians(
												Rotacion))));

				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderPisces),
												CollitionJefeS.i)).Position.X
												- (*collitionJefe).X1, 2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderPisces),
														CollitionJefeS.i)).Position.Y
														- (*collitionJefe).Y1,
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPPisces
								+ (*Jefe_getSObject()).radioCollitionBrazoEsp)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionJefeS.i)).Position.Y;

					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderPiscesTotal),
							(*Nave_getSObject()).poderPiscesTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderPisces),
							CollitionJefeS.i);
					;
					return 1;
				}
			}

		}
	}
	return 0;
}

//*******Colision Poder Pisces Brazo 2
int CollitionJefe_colisionPPiscesABrazo2(CollitionJefe* collitionJefe,
		Vector2 PosBrazo, float Rotacion)
{
	if ((*Nave_getSObject()).poderPisces.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderPisces.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 9; CollitionJefeS.j < 18;
					CollitionJefeS.j++)
			{
				(*collitionJefe).X1p =
						((*Jefe_getSObject()).collitionBrazoEspX[CollitionJefeS.j]);
				(*collitionJefe).Y1p =
						((*Jefe_getSObject()).collitionBrazoEspY[CollitionJefeS.j]);

				(*collitionJefe).X1 = (PosBrazo.X)
						+ (((*collitionJefe).X1p * cos(
								GameFramework_ToRadians(Rotacion)))
								- ((*collitionJefe).Y1p * sin(
										GameFramework_ToRadians(
												Rotacion))));
				(*collitionJefe).Y1 = (PosBrazo.Y)
						+ (((*collitionJefe).X1p * sin(
								GameFramework_ToRadians(Rotacion)))
								+ ((*collitionJefe).Y1p * cos(
										GameFramework_ToRadians(
												Rotacion))));

				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderPisces),
												CollitionJefeS.i)).Position.X
												- (*collitionJefe).X1, 2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderPisces),
														CollitionJefeS.i)).Position.Y
														- (*collitionJefe).Y1,
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPPisces
								+ (*Jefe_getSObject()).radioCollitionBrazoEsp)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionJefeS.i)).Position.Y;

					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderPiscesTotal),
							(*Nave_getSObject()).poderPiscesTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderPisces),
							CollitionJefeS.i);
					;
					return 1;
				}
			}

		}
	}
	return 0;
}

//*********Colision disparo a Esp
int CollitionJefe_colisionDisparoAEsp(CollitionJefe* collitionJefe, Jefe* jefe,
		Jefe_tipo tipoJefe)
{
	if (tipoJefe == JEsp && (*Nave_getSObject()).disparos.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).disparos.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 7; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Disparo*) Lista_RetornaElemento(
												&((*Nave_getSObject()).disparos),
												CollitionJefeS.i)).Position.X
												- ((*jefe).Position.X
														+ (*Jefe_getSObject()).collitionEspX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Disparo*) Lista_RetornaElemento(
														&((*Nave_getSObject()).disparos),
														CollitionJefeS.i)).Position.Y
														- ((*jefe).Position.Y
																+ (*Jefe_getSObject()).collitionEspY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*(Disparo*) Lista_RetornaElemento(
								&((*Nave_getSObject()).disparos),
								CollitionJefeS.i)).radioCollition
								+ (*Jefe_getSObject()).radioCollitionEsp)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionJefeS.i)).Position.Y;
					CollitionJefeS.ultimoTipoColision = 1;
					CollitionJefe_confirmaEliminaDisparoNave(CollitionJefeS.i);
					return 1;
				}
			}
		}
	}
	return 0;
}

//*********Colision Poder Galactica a Esp
int CollitionJefe_colisionPGalacticaAEsp(CollitionJefe* collitionJefe,
		Jefe* jefe, Jefe_tipo tipoJefe)
{
	if (tipoJefe == JEsp && (*Nave_getSObject()).poderGalactica.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderGalactica.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 7; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderGalactica),
												CollitionJefeS.i)).Position.X
												- ((*jefe).Position.X
														+ (*Jefe_getSObject()).collitionEspX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderGalactica),
														CollitionJefeS.i)).Position.Y
														- ((*jefe).Position.Y
																+ (*Jefe_getSObject()).collitionEspY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPGalactica
								+ (*Jefe_getSObject()).radioCollitionEsp)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionJefeS.i)).Position.Y;
					CollitionJefeS.ultimoTipoColision = 1;

					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderGalacticaTotal),
							(*Nave_getSObject()).poderGalacticaTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderGalactica),
							CollitionJefeS.i);
					return 1;
				}
			}
		}
	}
	return 0;
}

//*********Colision Poder Carafe a Esp
int CollitionJefe_colisionPCarafeAEsp(CollitionJefe* collitionJefe, Jefe* jefe,
		Jefe_tipo tipoJefe)
{
	if (tipoJefe == JEsp && (*Nave_getSObject()).poderCarafe.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderCarafe.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 7; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderCarafe),
												CollitionJefeS.i)).Position.X
												- ((*jefe).Position.X
														+ (*Jefe_getSObject()).collitionEspX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderCarafe),
														CollitionJefeS.i)).Position.Y
														- ((*jefe).Position.Y
																+ (*Jefe_getSObject()).collitionEspY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPCarafe
								+ (*Jefe_getSObject()).radioCollitionEsp)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionJefeS.i)).Position.Y;
					CollitionJefeS.ultimoTipoColision = 1;
					CollitionJefe_confirmaEliminaPoderCarafe(CollitionJefeS.i);
					return 1;
				}
			}
		}
	}
	return 0;
}

//*********Colision Poder Helix a Esp
int CollitionJefe_colisionPHelixAEsp(CollitionJefe* collitionJefe, Jefe* jefe,
		Jefe_tipo tipoJefe)
{
	if (tipoJefe == JEsp && (*Nave_getSObject()).poderHelix.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderHelix.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 7; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderHelix),
												CollitionJefeS.i)).Position.X
												- ((*jefe).Position.X
														+ (*Jefe_getSObject()).collitionEspX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderHelix),
														CollitionJefeS.i)).Position.Y
														- ((*jefe).Position.Y
																+ (*Jefe_getSObject()).collitionEspY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPHelix
								+ (*Jefe_getSObject()).radioCollitionEsp)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionJefeS.i)).Position.Y;
					CollitionJefeS.ultimoTipoColision = 1;

					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderHelixTotal),
							(*Nave_getSObject()).poderHelixTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderHelix),
							CollitionJefeS.i);
					return 1;
				}
			}
		}
	}
	return 0;
}

//*********Colision Poder Arp a Esp
int CollitionJefe_colisionPArpAEsp(CollitionJefe* collitionJefe, Jefe* jefe,
		Jefe_tipo tipoJefe)
{
	if (tipoJefe == JEsp && (*Nave_getSObject()).poderArp.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderArp.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 7; CollitionJefeS.j++)
			{
				for (CollitionJefeS.w = 0; CollitionJefeS.w < 6;
						CollitionJefeS.w++)
				{
					(*collitionJefe).distancia =
							(float) sqrt(
									pow(
											((*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderArp),
													CollitionJefeS.i)).Position.X
													+ (*Poderes_getSObject()).collitionPArpX[CollitionJefeS.w])
													- ((*jefe).Position.X
															+ (*Jefe_getSObject()).collitionEspX[CollitionJefeS.j]),
											2)
											+ pow(
													((*(Poderes*) Lista_RetornaElemento(
															&((*Nave_getSObject()).poderArp),
															CollitionJefeS.i)).Position.Y
															+ (*Poderes_getSObject()).collitionPArpY[CollitionJefeS.w])
															- ((*jefe).Position.Y
																	+ (*Jefe_getSObject()).collitionEspY[CollitionJefeS.j]),
													2));

					if ((*collitionJefe).distancia
							<= (*Poderes_getSObject()).radioCollitPArp
									+ (*Jefe_getSObject()).radioCollitionEsp)
					{
						CollitionJefeS.positionUltimoDisparo.X =
								(*(Poderes*) Lista_RetornaElemento(
										&((*Nave_getSObject()).poderArp),
										CollitionJefeS.i)).Position.X;
						CollitionJefeS.positionUltimoDisparo.Y =
								(*(Poderes*) Lista_RetornaElemento(
										&((*Nave_getSObject()).poderArp),
										CollitionJefeS.i)).Position.Y;
						CollitionJefeS.ultimoTipoColision = 1;

						Lista_InsertaEnFinal(
								&((*Nave_getSObject()).poderArpTotal),
								(*Nave_getSObject()).poderArpTotal.fin,
								Lista_RetornaElemento(
										&((*Nave_getSObject()).poderArp),
										CollitionJefeS.i));

						Lista_RemoveAt(&((*Nave_getSObject()).poderArp),
								CollitionJefeS.i);
						return 1;
					}
				}
			}
		}
	}
	return 0;
}

//*********Colision Poder Perseus a Esp
int CollitionJefe_colisionPPerseusAEsp(CollitionJefe* collitionJefe, Jefe* jefe,
		Jefe_tipo tipoJefe)
{
	if (tipoJefe == JEsp && (*Nave_getSObject()).poderPerseus.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderPerseus.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 7; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderPerseus),
												CollitionJefeS.i)).Position.X
												- ((*jefe).Position.X
														+ (*Jefe_getSObject()).collitionEspX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderPerseus),
														CollitionJefeS.i)).Position.Y
														- ((*jefe).Position.Y
																+ (*Jefe_getSObject()).collitionEspY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPPerseus
								+ (*Jefe_getSObject()).radioCollitionEsp)
				{
					CollitionJefeS.positionUltimoDisparo.X = (*jefe).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y = (*jefe).Position.Y;
					CollitionJefeS.ultimoTipoColision = 1;
					(*(Poderes*) Lista_RetornaElemento(
							&((*Nave_getSObject()).poderPerseus),
							CollitionJefeS.i)).Escala = GameFramework_Vector2(
							0.1f, 0.1f);

					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderPerseusTotal),
							(*Nave_getSObject()).poderPerseusTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPerseus),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderPerseus),
							CollitionJefeS.i);

					return 1;
				}
			}
		}
	}
	return 0;
}

//*********Colision Poder Pisces a Esp
int CollitionJefe_colisionPPiscesAEsp(CollitionJefe* collitionJefe, Jefe* jefe,
		Jefe_tipo tipoJefe)
{
	if (tipoJefe == JEsp && (*Nave_getSObject()).poderPisces.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderPisces.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 7; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderPisces),
												CollitionJefeS.i)).Position.X
												- ((*jefe).Position.X
														+ (*Jefe_getSObject()).collitionEspX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderPisces),
														CollitionJefeS.i)).Position.Y
														- ((*jefe).Position.Y
																+ (*Jefe_getSObject()).collitionEspY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPPisces
								+ (*Jefe_getSObject()).radioCollitionEsp)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionJefeS.i)).Position.Y;
					CollitionJefeS.ultimoTipoColision = 1;

					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderPiscesTotal),
							(*Nave_getSObject()).poderPiscesTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderPisces),
							CollitionJefeS.i);
					;
					return 1;
				}
			}
		}
	}
	return 0;
}

//*********Colision disparo a Quad
int CollitionJefe_colisionDisparoAQuad(CollitionJefe* collitionJefe, Jefe* jefe,
		Jefe_tipo tipoJefe)
{
	if ((*Nave_getSObject()).disparos.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).disparos.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 4; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Disparo*) Lista_RetornaElemento(
												&((*Nave_getSObject()).disparos),
												CollitionJefeS.i)).Position.X
												- ((*jefe).Position.X
														+ (*Jefe_getSObject()).collitionQuadX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Disparo*) Lista_RetornaElemento(
														&((*Nave_getSObject()).disparos),
														CollitionJefeS.i)).Position.Y
														- ((*jefe).Position.Y
																+ (*Jefe_getSObject()).collitionQuadY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*(Disparo*) Lista_RetornaElemento(
								&((*Nave_getSObject()).disparos),
								CollitionJefeS.i)).radioCollition
								+ (*Jefe_getSObject()).radioCollitionQuad)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionJefeS.i)).Position.Y;
					CollitionJefe_confirmaEliminaDisparoNave(CollitionJefeS.i);
					return 1;
				}
			}
		}
	}
	return 0;
}

//*********Colision Poder Galactica a Quad
int CollitionJefe_colisionPGalacticaAQuad(CollitionJefe* collitionJefe,
		Jefe* jefe, Jefe_tipo tipoJefe)
{
	if ((*Nave_getSObject()).poderGalactica.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderGalactica.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 4; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderGalactica),
												CollitionJefeS.i)).Position.X
												- ((*jefe).Position.X
														+ (*Jefe_getSObject()).collitionQuadX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderGalactica),
														CollitionJefeS.i)).Position.Y
														- ((*jefe).Position.Y
																+ (*Jefe_getSObject()).collitionQuadY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPGalactica
								+ (*Jefe_getSObject()).radioCollitionQuad)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionJefeS.i)).Position.Y;

					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderGalacticaTotal),
							(*Nave_getSObject()).poderGalacticaTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderGalactica),
							CollitionJefeS.i);
					return 1;
				}
			}
		}
	}
	return 0;
}

//*********Colision Poder Carafe a Quad
int CollitionJefe_colisionPCarafeAQuad(CollitionJefe* collitionJefe, Jefe* jefe,
		Jefe_tipo tipoJefe)
{
	if ((*Nave_getSObject()).poderCarafe.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderCarafe.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 4; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderCarafe),
												CollitionJefeS.i)).Position.X
												- ((*jefe).Position.X
														+ (*Jefe_getSObject()).collitionQuadX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderCarafe),
														CollitionJefeS.i)).Position.Y
														- ((*jefe).Position.Y
																+ (*Jefe_getSObject()).collitionQuadY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPCarafe
								+ (*Jefe_getSObject()).radioCollitionQuad)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionJefeS.i)).Position.Y;
					CollitionJefe_confirmaEliminaPoderCarafe(CollitionJefeS.i);
					return 1;
				}
			}
		}
	}
	return 0;
}

//*********Colision Poder Helix a Quad
int CollitionJefe_colisionPHelixAQuad(CollitionJefe* collitionJefe, Jefe* jefe,
		Jefe_tipo tipoJefe)
{
	if ((*Nave_getSObject()).poderHelix.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderHelix.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 4; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderHelix),
												CollitionJefeS.i)).Position.X
												- ((*jefe).Position.X
														+ (*Jefe_getSObject()).collitionQuadX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderHelix),
														CollitionJefeS.i)).Position.Y
														- ((*jefe).Position.Y
																+ (*Jefe_getSObject()).collitionQuadY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPHelix
								+ (*Jefe_getSObject()).radioCollitionQuad)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionJefeS.i)).Position.Y;

					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderHelixTotal),
							(*Nave_getSObject()).poderHelixTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderHelix),
							CollitionJefeS.i);
					return 1;
				}
			}
		}
	}
	return 0;
}

//*********Colision Poder Pisces a Quad
int CollitionJefe_colisionPPiscesAQuad(CollitionJefe* collitionJefe, Jefe* jefe,
		Jefe_tipo tipoJefe)
{
	if ((*Nave_getSObject()).poderPisces.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderPisces.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 4; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderPisces),
												CollitionJefeS.i)).Position.X
												- ((*jefe).Position.X
														+ (*Jefe_getSObject()).collitionQuadX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderPisces),
														CollitionJefeS.i)).Position.Y
														- ((*jefe).Position.Y
																+ (*Jefe_getSObject()).collitionQuadY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPPisces
								+ (*Jefe_getSObject()).radioCollitionQuad)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionJefeS.i)).Position.Y;

					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderPiscesTotal),
							(*Nave_getSObject()).poderPiscesTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderPisces),
							CollitionJefeS.i);
					;
					return 1;
				}
			}
		}
	}
	return 0;
}

//*********Colision disparo a Martillo Quad
int CollitionJefe_colisionDisparoAMartilloQuad(CollitionJefe* collitionJefe,
		Vector2 PosMartillo)
{
	if ((*Nave_getSObject()).disparos.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).disparos.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 4; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Disparo*) Lista_RetornaElemento(
												&((*Nave_getSObject()).disparos),
												CollitionJefeS.i)).Position.X
												- (PosMartillo.X
														+ (*Jefe_getSObject()).collitionMartilloQuadX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Disparo*) Lista_RetornaElemento(
														&((*Nave_getSObject()).disparos),
														CollitionJefeS.i)).Position.Y
														- (PosMartillo.Y
																+ (*Jefe_getSObject()).collitionMartilloQuadY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*(Disparo*) Lista_RetornaElemento(
								&((*Nave_getSObject()).disparos),
								CollitionJefeS.i)).radioCollition
								+ (*Jefe_getSObject()).radioCollitionMartilloQuad)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionJefeS.i)).Position.Y;
					if (CollitionJefeS.j >= 1)
					{
						CollitionJefeS.ultimoTipoColision = 1;
					}
					else
					{
						CollitionJefeS.ultimoTipoColision = 0;
					}
					CollitionJefe_confirmaEliminaDisparoNave(CollitionJefeS.i);
					return 1;
				}
			}
		}
	}
	return 0;
}

//*********Colision Poder Galactica a Martillo Quad
int CollitionJefe_colisionPGalacticaAMartilloQuad(CollitionJefe* collitionJefe,
		Vector2 PosMartillo)
{
	if ((*Nave_getSObject()).poderGalactica.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderGalactica.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 4; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderGalactica),
												CollitionJefeS.i)).Position.X
												- (PosMartillo.X
														+ (*Jefe_getSObject()).collitionMartilloQuadX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderGalactica),
														CollitionJefeS.i)).Position.Y
														- (PosMartillo.Y
																+ (*Jefe_getSObject()).collitionMartilloQuadY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPGalactica
								+ (*Jefe_getSObject()).radioCollitionMartilloQuad)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionJefeS.i)).Position.Y;
					if (CollitionJefeS.j >= 1)
					{
						CollitionJefeS.ultimoTipoColision = 1;
					}
					else
					{
						CollitionJefeS.ultimoTipoColision = 0;
					}

					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderGalacticaTotal),
							(*Nave_getSObject()).poderGalacticaTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderGalactica),
							CollitionJefeS.i);
					return 1;
				}
			}
		}
	}
	return 0;
}

//*********Colision Poder Carafe a Martillo Quad
int CollitionJefe_colisionPCarafeAMartilloQuad(CollitionJefe* collitionJefe,
		Vector2 PosMartillo)
{
	if ((*Nave_getSObject()).poderCarafe.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderCarafe.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 4; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderCarafe),
												CollitionJefeS.i)).Position.X
												- (PosMartillo.X
														+ (*Jefe_getSObject()).collitionMartilloQuadX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderCarafe),
														CollitionJefeS.i)).Position.Y
														- (PosMartillo.Y
																+ (*Jefe_getSObject()).collitionMartilloQuadY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPCarafe
								+ (*Jefe_getSObject()).radioCollitionMartilloQuad)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionJefeS.i)).Position.Y;
					if (CollitionJefeS.j >= 1)
					{
						CollitionJefeS.ultimoTipoColision = 1;
					}
					else
					{
						CollitionJefeS.ultimoTipoColision = 0;
					}
					CollitionJefe_confirmaEliminaPoderCarafe(CollitionJefeS.i);
					return 1;
				}
			}
		}
	}
	return 0;
}

//*********Colision Poder Helix a Martillo Quad
int CollitionJefe_colisionPHelixAMartilloQuad(CollitionJefe* collitionJefe,
		Vector2 PosMartillo)
{
	if ((*Nave_getSObject()).poderHelix.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderHelix.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 4; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderHelix),
												CollitionJefeS.i)).Position.X
												- (PosMartillo.X
														+ (*Jefe_getSObject()).collitionMartilloQuadX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderHelix),
														CollitionJefeS.i)).Position.Y
														- (PosMartillo.Y
																+ (*Jefe_getSObject()).collitionMartilloQuadY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPHelix
								+ (*Jefe_getSObject()).radioCollitionMartilloQuad)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionJefeS.i)).Position.Y;
					if (CollitionJefeS.j >= 1)
					{
						CollitionJefeS.ultimoTipoColision = 1;
					}
					else
					{
						CollitionJefeS.ultimoTipoColision = 0;
					}

					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderHelixTotal),
							(*Nave_getSObject()).poderHelixTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderHelix),
							CollitionJefeS.i);
					return 1;
				}
			}
		}
	}
	return 0;
}

//*********Colision Poder Arp a Martillo Quad
int CollitionJefe_colisionPArpAMartilloQuad(CollitionJefe* collitionJefe,
		Vector2 PosMartillo)
{
	if ((*Nave_getSObject()).poderArp.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderArp.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 1; CollitionJefeS.j < 4; CollitionJefeS.j++)
			{
				for (CollitionJefeS.w = 0; CollitionJefeS.w < 6;
						CollitionJefeS.w++)
				{
					(*collitionJefe).distancia =
							(float) sqrt(
									pow(
											((*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderArp),
													CollitionJefeS.i)).Position.X
													+ (*Poderes_getSObject()).collitionPArpX[CollitionJefeS.w])
													- (PosMartillo.X
															+ (*Jefe_getSObject()).collitionMartilloQuadX[CollitionJefeS.j]),
											2)
											+ pow(
													((*(Poderes*) Lista_RetornaElemento(
															&((*Nave_getSObject()).poderArp),
															CollitionJefeS.i)).Position.Y
															+ (*Poderes_getSObject()).collitionPArpY[CollitionJefeS.w])
															- (PosMartillo.Y
																	+ (*Jefe_getSObject()).collitionMartilloQuadY[CollitionJefeS.j]),
													2));

					if ((*collitionJefe).distancia
							<= (*Poderes_getSObject()).radioCollitPArp
									+ (*Jefe_getSObject()).radioCollitionMartilloQuad)
					{
						CollitionJefeS.positionUltimoDisparo.X =
								(*(Poderes*) Lista_RetornaElemento(
										&((*Nave_getSObject()).poderArp),
										CollitionJefeS.i)).Position.X;
						CollitionJefeS.positionUltimoDisparo.Y =
								(*(Poderes*) Lista_RetornaElemento(
										&((*Nave_getSObject()).poderArp),
										CollitionJefeS.i)).Position.Y;
						if (CollitionJefeS.j >= 1)
						{
							CollitionJefeS.ultimoTipoColision = 1;

							Lista_InsertaEnFinal(
									&((*Nave_getSObject()).poderArpTotal),
									(*Nave_getSObject()).poderArpTotal.fin,
									Lista_RetornaElemento(
											&((*Nave_getSObject()).poderArp),
											CollitionJefeS.i));

							Lista_RemoveAt(&((*Nave_getSObject()).poderArp),
									CollitionJefeS.i);
						}

						return 1;
					}
				}
			}
		}
	}
	return 0;
}

//*********Colision Poder Perseus a Martillo Quad
int CollitionJefe_colisionPPerseusAMartilloQuad(CollitionJefe* collitionJefe,
		Vector2 PosMartillo)
{
	if ((*Nave_getSObject()).poderPerseus.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderPerseus.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 1; CollitionJefeS.j < 4; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderPerseus),
												CollitionJefeS.i)).Position.X
												- (PosMartillo.X
														+ (*Jefe_getSObject()).collitionMartilloQuadX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderPerseus),
														CollitionJefeS.i)).Position.Y
														- (PosMartillo.Y
																+ (*Jefe_getSObject()).collitionMartilloQuadY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPPerseus
								+ (*Jefe_getSObject()).radioCollitionMartilloQuad)
				{
					CollitionJefeS.positionUltimoDisparo.X = PosMartillo.X;
					CollitionJefeS.positionUltimoDisparo.Y = PosMartillo.Y;
					if (CollitionJefeS.j >= 1)
					{
						CollitionJefeS.ultimoTipoColision = 1;
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderPerseus),
								CollitionJefeS.i)).Escala =
								GameFramework_Vector2(0.1f, 0.1f);

						Lista_InsertaEnFinal(
								&((*Nave_getSObject()).poderPerseusTotal),
								(*Nave_getSObject()).poderPerseusTotal.fin,
								Lista_RetornaElemento(
										&((*Nave_getSObject()).poderPerseus),
										CollitionJefeS.i));

						Lista_RemoveAt(&((*Nave_getSObject()).poderPerseus),
								CollitionJefeS.i);
						;
					}
					else
					{
						CollitionJefeS.ultimoTipoColision = 0;
					}
					return 1;
				}
			}
		}
	}
	return 0;
}

//*********Colision Poder Pisces a Martillo Quad
int CollitionJefe_colisionPPiscesAMartilloQuad(CollitionJefe* collitionJefe,
		Vector2 PosMartillo)
{
	if ((*Nave_getSObject()).poderPisces.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderPisces.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.j = 0; CollitionJefeS.j < 4; CollitionJefeS.j++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderPisces),
												CollitionJefeS.i)).Position.X
												- (PosMartillo.X
														+ (*Jefe_getSObject()).collitionMartilloQuadX[CollitionJefeS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderPisces),
														CollitionJefeS.i)).Position.Y
														- (PosMartillo.Y
																+ (*Jefe_getSObject()).collitionMartilloQuadY[CollitionJefeS.j]),
												2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPPisces
								+ (*Jefe_getSObject()).radioCollitionMartilloQuad)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionJefeS.i)).Position.Y;
					if (CollitionJefeS.j >= 1)
					{
						CollitionJefeS.ultimoTipoColision = 1;
					}
					else
					{
						CollitionJefeS.ultimoTipoColision = 0;
					}

					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderPiscesTotal),
							(*Nave_getSObject()).poderPiscesTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderPisces),
							CollitionJefeS.i);
					return 1;
				}
			}
		}
	}
	return 0;
}

//*********Colision disparo a Rued
int CollitionJefe_colisionDisparoARued(CollitionJefe* collitionJefe, Jefe* jefe,
		Jefe_tipo tipoJefe)
{
	if ((*Nave_getSObject()).disparos.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).disparos.size;
				CollitionJefeS.i++)
		{
			(*collitionJefe).distancia = (float) sqrt(
					pow(
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionJefeS.i)).Position.X
									- (*jefe).Position.X, 2)
							+ pow(
									(*(Disparo*) Lista_RetornaElemento(
											&((*Nave_getSObject()).disparos),
											CollitionJefeS.i)).Position.Y
											- (*jefe).Position.Y, 2));

			if ((*collitionJefe).distancia
					<= (*(Disparo*) Lista_RetornaElemento(
							&((*Nave_getSObject()).disparos), CollitionJefeS.i)).radioCollition
							+ (*Jefe_getSObject()).radioCollitionRued)
			{
				CollitionJefeS.positionUltimoDisparo.X =
						(*(Disparo*) Lista_RetornaElemento(
								&((*Nave_getSObject()).disparos),
								CollitionJefeS.i)).Position.X;
				CollitionJefeS.positionUltimoDisparo.Y =
						(*(Disparo*) Lista_RetornaElemento(
								&((*Nave_getSObject()).disparos),
								CollitionJefeS.i)).Position.Y;
				CollitionJefe_confirmaEliminaDisparoNave(CollitionJefeS.i);
				return 1;
			}
		}
	}

	return 0;
}

//*********Colision disparo a Flecha Rued
int CollitionJefe_colisionDisparoAFlechaRued(CollitionJefe* collitionJefe,
		Vector2 PosFlecha)
{
	if ((*Nave_getSObject()).disparos.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).disparos.size;
				CollitionJefeS.i++)
		{
			(*collitionJefe).distancia = (float) sqrt(
					pow(
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionJefeS.i)).Position.X - PosFlecha.X,
							2)
							+ pow(
									(*(Disparo*) Lista_RetornaElemento(
											&((*Nave_getSObject()).disparos),
											CollitionJefeS.i)).Position.Y
											- PosFlecha.Y, 2));

			if ((*collitionJefe).distancia
					<= (*(Disparo*) Lista_RetornaElemento(
							&((*Nave_getSObject()).disparos), CollitionJefeS.i)).radioCollition
							+ (*Jefe_getSObject()).radioCollitionFlechaRued)
			{
				CollitionJefeS.positionUltimoDisparo.X =
						(*(Disparo*) Lista_RetornaElemento(
								&((*Nave_getSObject()).disparos),
								CollitionJefeS.i)).Position.X;
				CollitionJefeS.positionUltimoDisparo.Y =
						(*(Disparo*) Lista_RetornaElemento(
								&((*Nave_getSObject()).disparos),
								CollitionJefeS.i)).Position.Y;
				CollitionJefe_confirmaEliminaDisparoNave(CollitionJefeS.i);
				return 1;
			}
		}
	}
	return 0;
}

//*********Colision Poder Glactica a Rued
int CollitionJefe_colisionPGalacticaARued(CollitionJefe* collitionJefe,
		Jefe* jefe, Jefe_tipo tipoJefe)
{
	if ((*Nave_getSObject()).poderGalactica.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderGalactica.size;
				CollitionJefeS.i++)
		{
			(*collitionJefe).distancia =
					(float) sqrt(
							pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderGalactica),
											CollitionJefeS.i)).Position.X
											- (*jefe).Position.X, 2)
									+ pow(
											(*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderGalactica),
													CollitionJefeS.i)).Position.Y
													- (*jefe).Position.Y, 2));

			if ((*collitionJefe).distancia
					<= (*Poderes_getSObject()).radioCollitPGalactica
							+ (*Jefe_getSObject()).radioCollitionRued)
			{
				CollitionJefeS.positionUltimoDisparo.X =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderGalactica),
								CollitionJefeS.i)).Position.X;
				CollitionJefeS.positionUltimoDisparo.Y =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderGalactica),
								CollitionJefeS.i)).Position.Y;

				Lista_InsertaEnFinal(
						&((*Nave_getSObject()).poderGalacticaTotal),
						(*Nave_getSObject()).poderGalacticaTotal.fin,
						Lista_RetornaElemento(
								&((*Nave_getSObject()).poderGalactica),
								CollitionJefeS.i));

				Lista_RemoveAt(&((*Nave_getSObject()).poderGalactica),
						CollitionJefeS.i);
				return 1;
			}
		}
	}

	return 0;
}

//*********Colision Poder Carafe a Rued
int CollitionJefe_colisionPCarafeARued(CollitionJefe* collitionJefe, Jefe* jefe,
		Jefe_tipo tipoJefe)
{
	if ((*Nave_getSObject()).poderCarafe.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderCarafe.size;
				CollitionJefeS.i++)
		{
			(*collitionJefe).distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionJefeS.i)).Position.X
									- (*jefe).Position.X, 2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderCarafe),
											CollitionJefeS.i)).Position.Y
											- (*jefe).Position.Y, 2));

			if ((*collitionJefe).distancia
					<= (*Poderes_getSObject()).radioCollitPCarafe
							+ (*Jefe_getSObject()).radioCollitionRued)
			{
				CollitionJefeS.positionUltimoDisparo.X =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderCarafe),
								CollitionJefeS.i)).Position.X;
				CollitionJefeS.positionUltimoDisparo.Y =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderCarafe),
								CollitionJefeS.i)).Position.Y;
				CollitionJefe_confirmaEliminaPoderCarafe(CollitionJefeS.i);
				return 1;
			}
		}
	}

	return 0;
}

//*********Colision Poder Helix a Rued
int CollitionJefe_colisionPHelixARued(CollitionJefe* collitionJefe, Jefe* jefe,
		Jefe_tipo tipoJefe)
{
	if ((*Nave_getSObject()).poderHelix.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderHelix.size;
				CollitionJefeS.i++)
		{
			(*collitionJefe).distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionJefeS.i)).Position.X
									- (*jefe).Position.X, 2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderHelix),
											CollitionJefeS.i)).Position.Y
											- (*jefe).Position.Y, 2));

			if ((*collitionJefe).distancia
					<= (*Poderes_getSObject()).radioCollitPHelix
							+ (*Jefe_getSObject()).radioCollitionRued)
			{
				CollitionJefeS.positionUltimoDisparo.X =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderHelix),
								CollitionJefeS.i)).Position.X;
				CollitionJefeS.positionUltimoDisparo.Y =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderHelix),
								CollitionJefeS.i)).Position.Y;

				Lista_InsertaEnFinal(&((*Nave_getSObject()).poderHelixTotal),
						(*Nave_getSObject()).poderHelixTotal.fin,
						Lista_RetornaElemento(
								&((*Nave_getSObject()).poderHelix),
								CollitionJefeS.i));

				Lista_RemoveAt(&((*Nave_getSObject()).poderHelix),
						CollitionJefeS.i);
				return 1;
			}
		}
	}

	return 0;
}

//*********Colision Poder Pisces a Rued
int CollitionJefe_colisionPPiscesARued(CollitionJefe* collitionJefe, Jefe* jefe,
		Jefe_tipo tipoJefe)
{
	if ((*Nave_getSObject()).poderPisces.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderPisces.size;
				CollitionJefeS.i++)
		{
			(*collitionJefe).distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionJefeS.i)).Position.X
									- (*jefe).Position.X, 2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderPisces),
											CollitionJefeS.i)).Position.Y
											- (*jefe).Position.Y, 2));

			if ((*collitionJefe).distancia
					<= (*Poderes_getSObject()).radioCollitPPisces
							+ (*Jefe_getSObject()).radioCollitionRued)
			{
				CollitionJefeS.positionUltimoDisparo.X =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderPisces),
								CollitionJefeS.i)).Position.X;
				CollitionJefeS.positionUltimoDisparo.Y =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderPisces),
								CollitionJefeS.i)).Position.Y;

				Lista_InsertaEnFinal(&((*Nave_getSObject()).poderPiscesTotal),
						(*Nave_getSObject()).poderPiscesTotal.fin,
						Lista_RetornaElemento(
								&((*Nave_getSObject()).poderPisces),
								CollitionJefeS.i));

				Lista_RemoveAt(&((*Nave_getSObject()).poderPisces),
						CollitionJefeS.i);
				return 1;
			}
		}
	}

	return 0;
}

//*********Colision Poder Glactica a Flecha Rued
int CollitionJefe_colisionPGalacticaAFlechaRued(CollitionJefe* collitionJefe,
		Vector2 PosFlecha)
{
	if ((*Nave_getSObject()).poderGalactica.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderGalactica.size;
				CollitionJefeS.i++)
		{
			(*collitionJefe).distancia =
					(float) sqrt(
							pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderGalactica),
											CollitionJefeS.i)).Position.X
											- PosFlecha.X, 2)
									+ pow(
											(*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderGalactica),
													CollitionJefeS.i)).Position.Y
													- PosFlecha.Y, 2));

			if ((*collitionJefe).distancia
					<= (*Poderes_getSObject()).radioCollitPGalactica
							+ (*Jefe_getSObject()).radioCollitionFlechaRued)
			{
				CollitionJefeS.positionUltimoDisparo.X =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderGalactica),
								CollitionJefeS.i)).Position.X;
				CollitionJefeS.positionUltimoDisparo.Y =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderGalactica),
								CollitionJefeS.i)).Position.Y;

				Lista_InsertaEnFinal(
						&((*Nave_getSObject()).poderGalacticaTotal),
						(*Nave_getSObject()).poderGalacticaTotal.fin,
						Lista_RetornaElemento(
								&((*Nave_getSObject()).poderGalactica),
								CollitionJefeS.i));

				Lista_RemoveAt(&((*Nave_getSObject()).poderGalactica),
						CollitionJefeS.i);
				return 1;
			}
		}
	}

	return 0;
}

//*********Colision Poder Carafe a Flecha Rued
int CollitionJefe_colisionPCarafeAFlechaRued(CollitionJefe* collitionJefe,
		Vector2 PosFlecha)
{
	if ((*Nave_getSObject()).poderCarafe.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderCarafe.size;
				CollitionJefeS.i++)
		{
			(*collitionJefe).distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionJefeS.i)).Position.X - PosFlecha.X,
							2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderCarafe),
											CollitionJefeS.i)).Position.Y
											- PosFlecha.Y, 2));

			if ((*collitionJefe).distancia
					<= (*Poderes_getSObject()).radioCollitPCarafe
							+ (*Jefe_getSObject()).radioCollitionFlechaRued)
			{
				CollitionJefeS.positionUltimoDisparo.X =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderCarafe),
								CollitionJefeS.i)).Position.X;
				CollitionJefeS.positionUltimoDisparo.Y =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderCarafe),
								CollitionJefeS.i)).Position.Y;
				CollitionJefe_confirmaEliminaPoderCarafe(CollitionJefeS.i);
				return 1;
			}
		}
	}

	return 0;
}

//*********Colision Poder Helix a Flecha Rued
int CollitionJefe_colisionPHelixAFlechaRued(CollitionJefe* collitionJefe,
		Vector2 PosFlecha)
{
	if ((*Nave_getSObject()).poderHelix.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderHelix.size;
				CollitionJefeS.i++)
		{
			(*collitionJefe).distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionJefeS.i)).Position.X - PosFlecha.X,
							2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderHelix),
											CollitionJefeS.i)).Position.Y
											- PosFlecha.Y, 2));

			if ((*collitionJefe).distancia
					<= (*Poderes_getSObject()).radioCollitPHelix
							+ (*Jefe_getSObject()).radioCollitionFlechaRued)
			{
				CollitionJefeS.positionUltimoDisparo.X =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderHelix),
								CollitionJefeS.i)).Position.X;
				CollitionJefeS.positionUltimoDisparo.Y =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderHelix),
								CollitionJefeS.i)).Position.Y;

				Lista_InsertaEnFinal(&((*Nave_getSObject()).poderHelixTotal),
						(*Nave_getSObject()).poderHelixTotal.fin,
						Lista_RetornaElemento(
								&((*Nave_getSObject()).poderHelix),
								CollitionJefeS.i));

				Lista_RemoveAt(&((*Nave_getSObject()).poderHelix),
						CollitionJefeS.i);
				return 1;
			}
		}
	}

	return 0;
}

//*********Colision Poder Arp a Flecha Rued
int CollitionJefe_colisionPArpAFlechaRued(CollitionJefe* collitionJefe,
		Vector2 PosFlecha)
{
	if ((*Nave_getSObject()).poderArp.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderArp.size;
				CollitionJefeS.i++)
		{
			for (CollitionJefeS.w = 0; CollitionJefeS.w < 6; CollitionJefeS.w++)
			{
				(*collitionJefe).distancia =
						(float) sqrt(
								pow(
										((*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderArp),
												CollitionJefeS.i)).Position.X
												+ (*Poderes_getSObject()).collitionPArpX[CollitionJefeS.w])
												- PosFlecha.X, 2)
										+ pow(
												((*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderArp),
														CollitionJefeS.i)).Position.Y
														+ (*Poderes_getSObject()).collitionPArpY[CollitionJefeS.w])
														- PosFlecha.Y, 2));

				if ((*collitionJefe).distancia
						<= (*Poderes_getSObject()).radioCollitPArp
								+ (*Jefe_getSObject()).radioCollitionFlechaRued)
				{
					CollitionJefeS.positionUltimoDisparo.X =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderArp),
									CollitionJefeS.i)).Position.X;
					CollitionJefeS.positionUltimoDisparo.Y =
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderArp),
									CollitionJefeS.i)).Position.Y;

					Lista_InsertaEnFinal(&((*Nave_getSObject()).poderArpTotal),
							(*Nave_getSObject()).poderArpTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderArp),
									CollitionJefeS.i));

					Lista_RemoveAt(&((*Nave_getSObject()).poderArp),
							CollitionJefeS.i);
					return 1;
				}
			}
		}
	}

	return 0;
}

//*********Colision Poder Perseus a Flecha Rued
int CollitionJefe_colisionPPerseusAFlechaRued(CollitionJefe* collitionJefe,
		Vector2 PosFlecha)
{
	if ((*Nave_getSObject()).poderPerseus.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderPerseus.size;
				CollitionJefeS.i++)
		{
			(*collitionJefe).distancia =
					(float) sqrt(
							pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderPerseus),
											CollitionJefeS.i)).Position.X
											- PosFlecha.X, 2)
									+ pow(
											(*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderPerseus),
													CollitionJefeS.i)).Position.Y
													- PosFlecha.Y, 2));

			if ((*collitionJefe).distancia
					<= (*Poderes_getSObject()).radioCollitPPerseus
							+ (*Jefe_getSObject()).radioCollitionFlechaRued)
			{
				CollitionJefeS.positionUltimoDisparo.X = PosFlecha.X;
				CollitionJefeS.positionUltimoDisparo.Y = PosFlecha.Y;
				(*(Poderes*) Lista_RetornaElemento(
						&((*Nave_getSObject()).poderPerseus), CollitionJefeS.i)).Escala =
						GameFramework_Vector2(0.1f, 0.1f);

				Lista_InsertaEnFinal(&((*Nave_getSObject()).poderPerseusTotal),
						(*Nave_getSObject()).poderPerseusTotal.fin,
						Lista_RetornaElemento(
								&((*Nave_getSObject()).poderPerseus),
								CollitionJefeS.i));

				Lista_RemoveAt(&((*Nave_getSObject()).poderPerseus),
						CollitionJefeS.i);
				;
				return 1;
			}

		}
	}

	return 0;
}

//*********Colision Poder Pisces a Flecha Rued
int CollitionJefe_colisionPPiscesAFlechaRued(CollitionJefe* collitionJefe,
		Vector2 PosFlecha)
{
	if ((*Nave_getSObject()).poderPisces.size > 0)
	{
		for (CollitionJefeS.i = 0;
				CollitionJefeS.i < (*Nave_getSObject()).poderPisces.size;
				CollitionJefeS.i++)
		{
			(*collitionJefe).distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionJefeS.i)).Position.X - PosFlecha.X,
							2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderPisces),
											CollitionJefeS.i)).Position.Y
											- PosFlecha.Y, 2));

			if ((*collitionJefe).distancia
					<= (*Poderes_getSObject()).radioCollitPPisces
							+ (*Jefe_getSObject()).radioCollitionFlechaRued)
			{
				CollitionJefeS.positionUltimoDisparo.X =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderPisces),
								CollitionJefeS.i)).Position.X;
				CollitionJefeS.positionUltimoDisparo.Y =
						(*(Poderes*) Lista_RetornaElemento(
								&((*Nave_getSObject()).poderPisces),
								CollitionJefeS.i)).Position.Y;

				Lista_InsertaEnFinal(&((*Nave_getSObject()).poderPiscesTotal),
						(*Nave_getSObject()).poderPiscesTotal.fin,
						Lista_RetornaElemento(
								&((*Nave_getSObject()).poderPisces),
								CollitionJefeS.i));

				Lista_RemoveAt(&((*Nave_getSObject()).poderPisces),
						CollitionJefeS.i);
				;
				return 1;
			}

		}
	}

	return 0;
}

//*****Nave a Jefe
int CollitionJefe_colisionNaveALaxo(CollitionJefe* collitionJefe, Jefe* jefe)
{
	for (CollitionJefeS.i = 0; CollitionJefeS.i < 11; CollitionJefeS.i++)
	{
		(*collitionJefe).distancia =
				(float) sqrt(
						pow(
								(*jefe).Position.X
										- ((*Nave_getSObject()).PositionNav.X
												+ (*Nave_getSObject()).collitionX[CollitionJefeS.i]),
								2)
								+ pow(
										(*jefe).Position.Y
												- ((*Nave_getSObject()).PositionNav.Y
														+ (*Nave_getSObject()).collitionY[CollitionJefeS.i]),
										2));

		if ((*collitionJefe).distancia
				<= (*Jefe_getSObject()).radioCollitionLaxo
						+ (*Nave_getSObject()).radioCollitionN)
		{
			return 1;
		}
	}
	for (CollitionJefeS.i = 0; CollitionJefeS.i < 11; CollitionJefeS.i++)
	{
		for (CollitionJefeS.j = 0; CollitionJefeS.j < 3; CollitionJefeS.j++)
		{
			(*collitionJefe).distancia =
					(float) sqrt(
							pow(
									((*jefe).Position.X
											+ (*Jefe_getSObject()).collitionLaxoX[CollitionJefeS.j])
											- ((*Nave_getSObject()).PositionNav.X
													+ (*Nave_getSObject()).collitionX[CollitionJefeS.i]),
									2)
									+ pow(
											((*jefe).Position.Y
													+ (*Jefe_getSObject()).collitionLaxoY[CollitionJefeS.j])
													- ((*Nave_getSObject()).PositionNav.Y
															+ (*Nave_getSObject()).collitionY[CollitionJefeS.i]),
											2));

			if ((*collitionJefe).distancia
					<= (*Jefe_getSObject()).radioCollitionDanoLaxo
							+ (*Nave_getSObject()).radioCollitionN)
			{
				return 1;
			}
		}
	}
	return 0;
}

//*****Nave a Esp
int CollitionJefe_colisionNaveAEsp(CollitionJefe* collitionJefe, Jefe* jefe)
{
	for (CollitionJefeS.i = 0; CollitionJefeS.i < 11; CollitionJefeS.i++)
	{
		for (CollitionJefeS.j = 0; CollitionJefeS.j < 7; CollitionJefeS.j++)
		{
			(*collitionJefe).distancia =
					(float) sqrt(
							pow(
									((*jefe).Position.X
											+ (*Jefe_getSObject()).collitionEspX[CollitionJefeS.j])
											- ((*Nave_getSObject()).PositionNav.X
													+ (*Nave_getSObject()).collitionX[CollitionJefeS.i]),
									2)
									+ pow(
											((*jefe).Position.Y
													+ (*Jefe_getSObject()).collitionEspY[CollitionJefeS.j])
													- ((*Nave_getSObject()).PositionNav.Y
															+ (*Nave_getSObject()).collitionY[CollitionJefeS.i]),
											2));

			if ((*collitionJefe).distancia
					<= (*Jefe_getSObject()).radioCollitionEsp
							+ (*Nave_getSObject()).radioCollitionN)
			{
				return 1;
			}
		}
	}
	return 0;
}

//*****Nave a Brazo 1 Esp
int CollitionJefe_colisionNaveABrazo1Esp(CollitionJefe* collitionJefe,
		Vector2 PosBrazo, float Rotacion)
{
	for (CollitionJefeS.i = 0; CollitionJefeS.i < 11; CollitionJefeS.i++)
	{
		for (CollitionJefeS.j = 0; CollitionJefeS.j < 9; CollitionJefeS.j++)
		{
			(*collitionJefe).X1p =
					((*Jefe_getSObject()).collitionBrazoEspX[CollitionJefeS.j]);
			(*collitionJefe).Y1p =
					((*Jefe_getSObject()).collitionBrazoEspY[CollitionJefeS.j]);

			(*collitionJefe).X1 =
					(PosBrazo.X)
							+ (((*collitionJefe).X1p
									* cos(GameFramework_ToRadians(Rotacion)))
									- ((*collitionJefe).Y1p
											* sin(GameFramework_ToRadians(Rotacion))));
			(*collitionJefe).Y1 =
					(PosBrazo.Y)
							+ (((*collitionJefe).X1p
									* sin(GameFramework_ToRadians(Rotacion)))
									+ ((*collitionJefe).Y1p
											* cos(GameFramework_ToRadians(Rotacion))));

			(*collitionJefe).distancia =
					(float) sqrt(
							pow(
									((*Nave_getSObject()).PositionNav.X
											+ (*Nave_getSObject()).collitionX[CollitionJefeS.i])
											- (*collitionJefe).X1, 2)
									+ pow(
											((*Nave_getSObject()).PositionNav.Y
													+ (*Nave_getSObject()).collitionY[CollitionJefeS.i])
													- (*collitionJefe).Y1, 2));

			if ((*collitionJefe).distancia
					<= (*Nave_getSObject()).radioCollitionN
							+ (*Jefe_getSObject()).radioCollitionBrazoEsp)
			{
				return 1;
			}
		}

	}
	return 0;
}

//*****Nave a Brazo 1 Esp
int CollitionJefe_colisionNaveABrazo2Esp(CollitionJefe* collitionJefe,
		Vector2 PosBrazo, float Rotacion)
{
	for (CollitionJefeS.i = 0; CollitionJefeS.i < 11; CollitionJefeS.i++)
	{
		for (CollitionJefeS.j = 9; CollitionJefeS.j < 18; CollitionJefeS.j++)
		{
			(*collitionJefe).X1p =
					((*Jefe_getSObject()).collitionBrazoEspX[CollitionJefeS.j]);
			(*collitionJefe).Y1p =
					((*Jefe_getSObject()).collitionBrazoEspY[CollitionJefeS.j]);

			(*collitionJefe).X1 =
					(PosBrazo.X)
							+ (((*collitionJefe).X1p
									* cos(GameFramework_ToRadians(Rotacion)))
									- ((*collitionJefe).Y1p
											* sin(GameFramework_ToRadians(Rotacion))));
			(*collitionJefe).Y1 =
					(PosBrazo.Y)
							+ (((*collitionJefe).X1p
									* sin(GameFramework_ToRadians(Rotacion)))
									+ ((*collitionJefe).Y1p
											* cos(GameFramework_ToRadians(Rotacion))));

			(*collitionJefe).distancia =
					(float) sqrt(
							pow(
									((*Nave_getSObject()).PositionNav.X
											+ (*Nave_getSObject()).collitionX[CollitionJefeS.i])
											- (*collitionJefe).X1, 2)
									+ pow(
											((*Nave_getSObject()).PositionNav.Y
													+ (*Nave_getSObject()).collitionY[CollitionJefeS.i])
													- (*collitionJefe).Y1, 2));

			if ((*collitionJefe).distancia
					<= (*Nave_getSObject()).radioCollitionN
							+ (*Jefe_getSObject()).radioCollitionBrazoEsp)
			{
				return 1;
			}
		}

	}
	return 0;
}

//*****Nave a Quad
int CollitionJefe_colisionNaveAQuad(CollitionJefe* collitionJefe, Jefe* jefe)
{
	for (CollitionJefeS.i = 0; CollitionJefeS.i < 11; CollitionJefeS.i++)
	{
		for (CollitionJefeS.j = 0; CollitionJefeS.j < 4; CollitionJefeS.j++)
		{
			(*collitionJefe).distancia =
					(float) sqrt(
							pow(
									((*jefe).Position.X
											+ (*Jefe_getSObject()).collitionQuadX[CollitionJefeS.j])
											- ((*Nave_getSObject()).PositionNav.X
													+ (*Nave_getSObject()).collitionX[CollitionJefeS.i]),
									2)
									+ pow(
											((*jefe).Position.Y
													+ (*Jefe_getSObject()).collitionQuadY[CollitionJefeS.j])
													- ((*Nave_getSObject()).PositionNav.Y
															+ (*Nave_getSObject()).collitionY[CollitionJefeS.i]),
											2));

			if ((*collitionJefe).distancia
					<= (*Jefe_getSObject()).radioCollitionQuad
							+ (*Nave_getSObject()).radioCollitionN)
			{
				return 1;
			}
		}
	}
	return 0;
}

//*****Nave a Martillo Quad
int CollitionJefe_colisionNaveAMartilloQuad(CollitionJefe* collitionJefe,
		Vector2 PosMartillo)
{

	for (CollitionJefeS.i = 0; CollitionJefeS.i < 11; CollitionJefeS.i++)
	{
		for (CollitionJefeS.j = 0; CollitionJefeS.j < 4; CollitionJefeS.j++)
		{
			(*collitionJefe).distancia =
					(float) sqrt(
							pow(
									(PosMartillo.X
											+ (*Jefe_getSObject()).collitionMartilloQuadX[CollitionJefeS.j])
											- ((*Nave_getSObject()).PositionNav.X
													+ (*Nave_getSObject()).collitionX[CollitionJefeS.i]),
									2)
									+ pow(
											(PosMartillo.Y
													+ (*Jefe_getSObject()).collitionMartilloQuadY[CollitionJefeS.j])
													- ((*Nave_getSObject()).PositionNav.Y
															+ (*Nave_getSObject()).collitionY[CollitionJefeS.i]),
											2));

			if ((*collitionJefe).distancia
					<= (*Jefe_getSObject()).radioCollitionMartilloQuad
							+ (*Nave_getSObject()).radioCollitionN)
			{
				return 1;
			}
		}
	}
	return 0;
}

//*****Nave a Rued
int CollitionJefe_colisionNaveARued(CollitionJefe* collitionJefe, Jefe* jefe)
{
	for (CollitionJefeS.i = 0; CollitionJefeS.i < 11; CollitionJefeS.i++)
	{
		(*collitionJefe).distancia =
				(float) sqrt(
						pow(
								(*jefe).Position.X
										- ((*Nave_getSObject()).PositionNav.X
												+ (*Nave_getSObject()).collitionX[CollitionJefeS.i]),
								2)
								+ pow(
										(*jefe).Position.Y
												- ((*Nave_getSObject()).PositionNav.Y
														+ (*Nave_getSObject()).collitionY[CollitionJefeS.i]),
										2));

		if ((*collitionJefe).distancia
				<= (*Jefe_getSObject()).radioCollitionRued
						+ (*Nave_getSObject()).radioCollitionN)
		{
			return 1;
		}
	}
	return 0;
}

//*****Nave a Flecha Rued
int CollitionJefe_colisionNaveAFlechaRued(CollitionJefe* collitionJefe,
		Vector2 PosFlecha)
{
	for (CollitionJefeS.i = 0; CollitionJefeS.i < 11; CollitionJefeS.i++)
	{
		(*collitionJefe).distancia =
				(float) sqrt(
						pow(
								PosFlecha.X
										- ((*Nave_getSObject()).PositionNav.X
												+ (*Nave_getSObject()).collitionX[CollitionJefeS.i]),
								2)
								+ pow(
										PosFlecha.Y
												- ((*Nave_getSObject()).PositionNav.Y
														+ (*Nave_getSObject()).collitionY[CollitionJefeS.i]),
										2));

		if ((*collitionJefe).distancia
				<= (*Jefe_getSObject()).radioCollitionFlechaRued
						+ (*Nave_getSObject()).radioCollitionN)
		{
			return 1;
		}
	}
	return 0;
}

//Comprobacion de poder carafe al colisionar con enemigos
void CollitionJefe_confirmaEliminaPoderCarafe(int indice)
{
	if ((*(Poderes*) Lista_RetornaElemento(&((*Nave_getSObject()).poderCarafe),
			indice)).tipoPoderCarafeActual == PCMedio)
	{

		Lista_InsertaEnFinal(&((*Nave_getSObject()).poderCarafeMedioTotal),
				(*Nave_getSObject()).poderCarafeMedioTotal.fin,
				Lista_RetornaElemento(&((*Nave_getSObject()).poderCarafe),
						indice));

	}
	else if ((*(Poderes*) Lista_RetornaElemento(
			&((*Nave_getSObject()).poderCarafe), indice)).tipoPoderCarafeActual
			== PCIzquierda)
	{

		Lista_InsertaEnFinal(&((*Nave_getSObject()).poderCarafeIzquierdaTotal),
				(*Nave_getSObject()).poderCarafeIzquierdaTotal.fin,
				Lista_RetornaElemento(&((*Nave_getSObject()).poderCarafe),
						indice));

	}
	else if ((*(Poderes*) Lista_RetornaElemento(
			&((*Nave_getSObject()).poderCarafe), indice)).tipoPoderCarafeActual
			== PCDerecha)
	{
		Lista_InsertaEnFinal(&((*Nave_getSObject()).poderCarafeDerechaTotal),
				(*Nave_getSObject()).poderCarafeDerechaTotal.fin,
				Lista_RetornaElemento(&((*Nave_getSObject()).poderCarafe),
						indice));

	}
	Lista_RemoveAt(&((*Nave_getSObject()).poderCarafe), indice);
}

// Comprobacion de disparos de la nave al colisionar con enemigos
void CollitionJefe_confirmaEliminaDisparoNave(int indice)
{
	if ((*(Disparo*) Lista_RetornaElemento(&((*Nave_getSObject()).disparos),
			indice)).tipoActual == Dpequed
			|| (*(Disparo*) Lista_RetornaElemento(
					&((*Nave_getSObject()).disparos), indice)).tipoActual
					== Dpequei)
	{
		CollitionJefeS.ultimaColisionTipoDisparo =
				(*(Disparo*) Lista_RetornaElemento(
						&((*Nave_getSObject()).disparos), indice)).tipoActual;

		Lista_InsertaEnFinal(&((*Nave_getSObject()).disparosTotales),
				(*Nave_getSObject()).disparosTotales.fin,
				Lista_RetornaElemento(&((*Nave_getSObject()).disparos),
						indice));

		Lista_RemoveAt(&((*Nave_getSObject()).disparos), indice);
	}
	else if ((*(Disparo*) Lista_RetornaElemento(
			&((*Nave_getSObject()).disparos), indice)).tipoActual == DdiagD)
	{
		CollitionJefeS.ultimaColisionTipoDisparo =
				(*(Disparo*) Lista_RetornaElemento(
						&((*Nave_getSObject()).disparos), indice)).tipoActual;

		Lista_InsertaEnFinal(&((*Nave_getSObject()).disparosDiagDerechaTotales),
				(*Nave_getSObject()).disparosDiagDerechaTotales.fin,
				Lista_RetornaElemento(&((*Nave_getSObject()).disparos),
						indice));

		Lista_RemoveAt(&((*Nave_getSObject()).disparos), indice);
	}
	else if ((*(Disparo*) Lista_RetornaElemento(
			&((*Nave_getSObject()).disparos), indice)).tipoActual == DdiagI)
	{
		CollitionJefeS.ultimaColisionTipoDisparo =
				(*(Disparo*) Lista_RetornaElemento(
						&((*Nave_getSObject()).disparos), indice)).tipoActual;

		Lista_InsertaEnFinal(
				&((*Nave_getSObject()).disparosDiagIzquierdaTotales),
				(*Nave_getSObject()).disparosDiagIzquierdaTotales.fin,
				Lista_RetornaElemento(&((*Nave_getSObject()).disparos),
						indice));

		Lista_RemoveAt(&((*Nave_getSObject()).disparos), indice);
	}
	else if ((*(Disparo*) Lista_RetornaElemento(
			&((*Nave_getSObject()).disparos), indice)).tipoActual == Dsuper)
	{
		CollitionJefeS.ultimaColisionTipoDisparo =
				(*(Disparo*) Lista_RetornaElemento(
						&((*Nave_getSObject()).disparos), indice)).tipoActual;

		Lista_InsertaEnFinal(&((*Nave_getSObject()).disparosSuperTotales),
				(*Nave_getSObject()).disparosSuperTotales.fin,
				Lista_RetornaElemento(&((*Nave_getSObject()).disparos),
						indice));

		Lista_RemoveAt(&((*Nave_getSObject()).disparos), indice);
	}

}

CollitionJefe* CollitionJefe_getSObject()
{
	return &CollitionJefeS;
}

