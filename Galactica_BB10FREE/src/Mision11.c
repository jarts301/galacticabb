/*
 * Mision11.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "Mision11.h"
#include "QuickModeGame.h"
#include "Enemigo.h"
#include "Animation.h"
#include "Jefe.h"
#include "BarraVida.h"
#include "Boton.h"
#include "Nave.h"
#include "Lista.h"
#include "Control.h"
#include "Textura.h"
#include "Dibujar.h"
#include "Audio.h"
#include "Mision1.h"
#include "MisionMode.h"
#include "CollitionJefe.h"
#include "Almacenamiento.h"
#include "MisionModeGOver.h"
#include "Cadena.h"

static Mision11 Mision11S;

void Mision11_reiniciar()
{
	QuickModeGame_eliminarTodos();
	QuickModeGame_eliminaAnimaciones();
	Mision11S.positionToBeat.X = 6;
	Mision11S.positionToBeat.Y = 900;
	Mision11S.positionTime.X = 420;
	Mision11S.positionTime.Y = 900;
	(*QuickModeGame_getSObject()).igb.velocidadEnem = 2;
	(*QuickModeGame_getSObject()).igb.velocidadEnemY = 2;
	(*QuickModeGame_getSObject()).igb.velocidadHiEnem = 3;
	(*QuickModeGame_getSObject()).igb.velocidadHiEnemY = 3;
	(*QuickModeGame_getSObject()).igb.maxEnemigos = 5;
	Mision11S.destruidos = 0;
	(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo = 0;
	(*QuickModeGame_getSObject()).igb.ultimoAumento = 0;
	Mision11S.ultimoTEliminar = 0;
	(*QuickModeGame_getSObject()).igb.EscalaMensaje =
			(*QuickModeGame_getSObject()).igb.escalaInicialMensaje = 1.5f;
	(*QuickModeGame_getSObject()).igb.actualFrameMensaje = 0;
	(*QuickModeGame_getSObject()).igb.tiempoTitileoMensaje = 0;
	(*QuickModeGame_getSObject()).igb.mensajeInicialActivo = 1;
	Lista_Destruir(&((*QuickModeGame_getSObject()).igb.botones));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BPause, 5, 950));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BAudio, 530, 950));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BPower, 480, 20));
}

void Mision11_load()
{
	Mision11S.tituloMision = "Mission 11";

	Lista_Inicia(&Mision11S.mensajeMision);
	Lista_AddInFin(&Mision11S.mensajeMision,
			String_creaString("Eliminate only red color"));
	Lista_AddInFin(&Mision11S.mensajeMision,
			String_creaString("enemies, if you fail"));
	Lista_AddInFin(&Mision11S.mensajeMision,
			String_creaString("you will lose."));
	Lista_AddInFin(&Mision11S.mensajeMision,
			String_creaString("Tip:If no red enemies,"));
	Lista_AddInFin(&Mision11S.mensajeMision,
			String_creaString("wait some time,"));
	Lista_AddInFin(&Mision11S.mensajeMision,
			String_creaString("they will appear."));

	Lista_Inicia(&Mision11S.recompensa);
	Lista_AddInFin(&Mision11S.recompensa,
			String_creaString("Recharge, the energy in"));
	Lista_AddInFin(&Mision11S.recompensa,
			String_creaString("100, to catch the star"));
	Lista_AddInFin(&Mision11S.recompensa, String_creaString("in Quick Game."));

	Lista_Inicia(&Mision11S.noRecompensa);
	Lista_AddInFin(&Mision11S.noRecompensa, String_creaString("No Reward.."));

	Mision11S.destruidos = 0;
	Mision11S.ultimoTEliminar = 0;
}

void Mision11_update()
{
	Nave_update();
	QuickModeGame_updateEstrellas();
	Mision11_updateEnemigos();
	Mision11_updateGame();
	QuickModeGame_updateAnimaciones();
	for (Mision11S.i = 0;
			Mision11S.i < (*QuickModeGame_getSObject()).igb.botones.size;
			Mision11S.i++)
	{
		Boton_update(
				(Boton*) Lista_GetIn(&(*QuickModeGame_getSObject()).igb.botones,
						Mision11S.i));
	}

	QuickModeGame_updateMensajeInicial();
	BarraVida_update((*QuickModeGame_getSObject()).igb.barraVidaNave,
			(*Nave_getSObject()).Vida, (*Nave_getSObject()).MaxVida);

	if ((*Nave_getSObject()).Vida <= 0)
	{
		Animation_update((*Animation_getSObject()).animacionesEstallidoNave);
	}
	Mision11_gameOver();
	if ((*Control_getSObject()).controlActivo == 1)
	{
		Control_update((*QuickModeGame_getSObject()).igb.control);
	}

}

void Mision11_draw()
{
	//CLEAR NEGRO
	Dibujar_drawSprite(Textura_getTexturaFondo(0), GameFramework_Vector2(0, 0),
			GameFramework_Color(0, 0, 0, 1), 0, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(40, 40));

	Nave_draw();
	QuickModeGame_drawEstrellas();
	QuickModeGame_drawEnemigos();
	Mision11_drawEstadNave();
	QuickModeGame_drawAnimaciones();
	Mision11_drawMensajeInicial();
	if ((*Nave_getSObject()).Vida <= 0)
	{
		Animation_draw((*Animation_getSObject()).animacionesEstallidoNave);
	}

	for (Mision11S.i = 0;
			Mision11S.i < (*QuickModeGame_getSObject()).igb.botones.size;
			Mision11S.i++)
	{
		Boton_draw(
				(Boton*) Lista_GetIn(&(*QuickModeGame_getSObject()).igb.botones,
						Mision11S.i));
	}
	if ((*Control_getSObject()).controlActivo == 1)
	{
		Control_draw((*QuickModeGame_getSObject()).igb.control);
	}
	// drawEstadisticas(sb);
}

//******************Estadisticas de la nave*************
void Mision11_drawEstadNave()
{
	//** Vida de la nave
	Dibujar_drawSprite(Textura_getTexturaFondo(1),
			GameFramework_Vector2(0, 945), GameFramework_Color(0, 0, 0, 1), 0,
			GameFramework_Vector2(0, 0), GameFramework_Vector2(50, 2.6));

	BarraVida_draw((*QuickModeGame_getSObject()).igb.barraVidaNave);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_EnteroACadena((*Nave_getSObject()).Vida),
			GameFramework_Vector2(100, 975), GameFramework_Color(1, 1, 1, 1),
			0.8);

	Dibujar_drawText((*Textura_getSObject()).Fuente, "/",
			GameFramework_Vector2(285, 965), GameFramework_Color(1, 1, 1, 1),
			1.4);

	Nave_drawAnimacionesPoder();

	//****Puntos a vencer
	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(3, "Destroyed red:",
					GameFramework_EnteroACadena(Mision11S.destruidos), "/15"),
			Mision11S.positionToBeat,
			GameFramework_Color(GameFramework_getByteColorToFloatColor(244),
					GameFramework_getByteColorToFloatColor(232),
					GameFramework_getByteColorToFloatColor(16), 1), 1.0);

}

void Mision11_updateGame()
{
	if (GameFramework_getTotalTime() > Mision11S.ultimoTEliminar + 15000000)
	{
		QuickModeGame_eliminarTodosLosEnemigos();
		Mision11S.ultimoTEliminar = GameFramework_getTotalTime();
	}

	if ((*QuickModeGame_getSObject()).igb.enemigos.size
			< (*QuickModeGame_getSObject()).igb.maxEnemigos)
	{
		Mision11_insertaEnemigo();
	}
}

void Mision11_gameOver()
{
	if (Mision11S.destruidos >= 15)
	{
		if ((*Audio_getSObject()).musicaActiva == 1)
		{
			Audio_stopAudio("Galactic");
			Audio_playAudio("GameOver");
		}
		(*MisionModeGOver_getSObject()).gana = 1;

		Almacenamiento_cargarMisiones();
		if (Almacenamiento_getPremiosDados() == 10)
		{
			Almacenamiento_salvarBonus(100, ALrecuperacion);
			(*MisionModeGOver_getSObject()).cosasGanadas = &Mision11S.recompensa;
			Almacenamiento_salvarMisiones();
		}
		else
		{
			(*MisionModeGOver_getSObject()).cosasGanadas =
					&(*Mision1_getSObject()).yaRecompensa;
		}

		(*MisionMode_getSObject()).estadoActual = MMGameOver;
	}

	if ((*Nave_getSObject()).Vida <= 0)
	{
		(*Nave_getSObject()).Vida = 0;
		(*Nave_getSObject()).EscalaN = GameFramework_Vector2(0, 0);

		Animation_setPosition(
				(*Animation_getSObject()).animacionesEstallidoNave,
				(*Nave_getSObject()).PositionNav);

		(*MisionModeGOver_getSObject()).gana = 0;
		(*MisionModeGOver_getSObject()).cosasGanadas = &Mision11S.noRecompensa;
		if ((*(*Animation_getSObject()).animacionesEstallidoNave).animacionFinalizada
				== 1)
		{
			if ((*Audio_getSObject()).musicaActiva)
			{
				Audio_stopAudio("Galactic");
				Audio_playAudio("GameOver");
			}
			(*MisionMode_getSObject()).estadoActual = MMGameOver;
		}
	}
}

//********Dibujo Mensaje Inicial
void Mision11_drawMensajeInicial()
{
	if ((*QuickModeGame_getSObject()).igb.mensajeInicialActivo == 1)
	{
		Dibujar_drawText((*Textura_getSObject()).Fuente,
				GameFramework_Concatenar(2, Mision11S.tituloMision, " Start!!"),
				GameFramework_Vector2(50, 620),
				GameFramework_Color(GameFramework_getByteColorToFloatColor(7),
						GameFramework_getByteColorToFloatColor(237),
						GameFramework_getByteColorToFloatColor(134), 1),
				(*QuickModeGame_getSObject()).igb.EscalaMensaje);

	}
}

//*Actualiza enemigos
void Mision11_updateEnemigos()
{
	if ((*QuickModeGame_getSObject()).igb.enemigos.size > 0)
	{
		for (Mision11S.i = 0;
				Mision11S.i < (*QuickModeGame_getSObject()).igb.enemigos.size;
				Mision11S.i++)
		{
			Enemigo_update(
					(Enemigo*) Lista_GetIn(
							&(*QuickModeGame_getSObject()).igb.enemigos,
							Mision11S.i));

			if ((*(Enemigo*) Lista_GetIn(
					&(*QuickModeGame_getSObject()).igb.enemigos, Mision11S.i)).estaEnColisionDisp
					== 1
					|| (*(Enemigo*) Lista_GetIn(
							&(*QuickModeGame_getSObject()).igb.enemigos,
							Mision11S.i)).estaEnColisionNave == 1
					|| (*(Enemigo*) Lista_GetIn(
							&(*QuickModeGame_getSObject()).igb.enemigos,
							Mision11S.i)).estaEnColisionPoder == 1)
			{

				(*(Enemigo*) Lista_GetIn(
						&(*QuickModeGame_getSObject()).igb.enemigos,
						Mision11S.i)).estaEnColisionDisp = 0;
				(*(Enemigo*) Lista_GetIn(
						&(*QuickModeGame_getSObject()).igb.enemigos,
						Mision11S.i)).estaEnColisionNave = 0;
				(*(Enemigo*) Lista_GetIn(
						&(*QuickModeGame_getSObject()).igb.enemigos,
						Mision11S.i)).estaEnColisionPoder = 0;

				if ((*Animation_getSObject()).animacionesDispAEnemigoTotal.size
						> 0)
				{
					Animation_setPosition(
							(Animation*) Lista_GetIn(
									&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
									(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
											- 1),
							(*(Enemigo*) Lista_GetIn(
									&(*QuickModeGame_getSObject()).igb.enemigos,
									Mision11S.i)).Position);

					Lista_AddInFin(
							&(*QuickModeGame_getSObject()).igb.animacionesQ,
							Lista_GetIn(
									&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
									(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
											- 1));

					Lista_RemoveAt(
							&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
							(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
									- 1);
				}

				if ((*(Enemigo*) Lista_GetIn(
						&(*QuickModeGame_getSObject()).igb.enemigos,
						Mision11S.i)).vida <= 0
						|| (*(Enemigo*) Lista_GetIn(
								&(*QuickModeGame_getSObject()).igb.enemigos,
								Mision11S.i)).estaEnColisionNave == 1)
				{
					Audio_playAudio("EstallidoEnemigo");
					(*Nave_getSObject()).Destruidos =
							(*Nave_getSObject()).Destruidos + 1;
					(*(Enemigo*) Lista_GetIn(
							&(*QuickModeGame_getSObject()).igb.enemigos,
							Mision11S.i)).estaEnColisionNave = 0;

					if ((*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
							> 0)
					{
						Animation_setPosition(
								(Animation*) Lista_GetIn(
										&(*Animation_getSObject()).animacionesEstallidoEnemigoTotal,
										(*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
												- 1),
								(*(Enemigo*) Lista_GetIn(
										&(*QuickModeGame_getSObject()).igb.enemigos,
										Mision11S.i)).Position);

						Lista_AddInFin(
								&(*QuickModeGame_getSObject()).igb.animacionesQ,
								Lista_GetIn(
										&(*Animation_getSObject()).animacionesEstallidoEnemigoTotal,
										(*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
												- 1));

						Lista_RemoveAt(
								&(*Animation_getSObject()).animacionesEstallidoEnemigoTotal,
								(*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
										- 1);
					}

					switch ((*(Enemigo*) Lista_GetIn(
							&(*QuickModeGame_getSObject()).igb.enemigos,
							Mision11S.i)).tipoEnemigo)
					{
					case ENor:
						QuickModeGame_confirmaEliminarNor(Mision11S.i);
						Mision11S.destruidos++;
						break;
					case EKami:
						QuickModeGame_EliminarYRetornoEnemigoTotal(Mision11S.i);
						(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
								- 15;
						break;
					case ESid:
						QuickModeGame_confirmaEliminarSid(Mision11S.i);
						(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
								- 15;
						break;
					case EHiNor:
						QuickModeGame_confirmaEliminarNor(Mision11S.i);
						Mision11S.destruidos++;
						break;
					case EHiKami:
						QuickModeGame_EliminarYRetornoEnemigoTotal(Mision11S.i);
						(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
								- 15;
						break;
					case EHiSid:
						QuickModeGame_confirmaEliminarSid(Mision11S.i);
						(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
								- 15;
						break;
					}

				} //si murio el enemigo

			} //si esta wne colision

		} //for mirando cada enemigo

	} //si (*QuickModeGame_getSObject()).igb.enemigos count >0

}

void Mision11_insertaEnemigo()
{
	(*QuickModeGame_getSObject()).igb.num = 0;
	(*QuickModeGame_getSObject()).igb.num2 = 0;
	(*QuickModeGame_getSObject()).igb.num = GameFramework_Rand(1, 15);

	if ((*QuickModeGame_getSObject()).igb.num == 1
			|| (*QuickModeGame_getSObject()).igb.num == 2
			|| (*QuickModeGame_getSObject()).igb.num == 3
			|| (*QuickModeGame_getSObject()).igb.num == 4
			|| (*QuickModeGame_getSObject()).igb.num == 5
			|| (*QuickModeGame_getSObject()).igb.num == 6)
	{
		(*QuickModeGame_getSObject()).igb.num2 = GameFramework_Rand(1, 2);
		if ((*QuickModeGame_getSObject()).igb.num2 == 1)
		{
			if ((*Enemigo_getSObject()).NorTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).NorTotal),
								(*Enemigo_getSObject()).NorTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).NorTotal),
								(*Enemigo_getSObject()).NorTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).NorTotal),
						(*Enemigo_getSObject()).NorTotal.size - 1);
			}
		}
		if ((*QuickModeGame_getSObject()).igb.num2 == 2)
		{
			if ((*Enemigo_getSObject()).NorTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).NorTotal),
								(*Enemigo_getSObject()).NorTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).NorTotal),
								(*Enemigo_getSObject()).NorTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).NorTotal),
						(*Enemigo_getSObject()).NorTotal.size - 1);
			}
		}
	}
	if ((*QuickModeGame_getSObject()).igb.num == 7
			|| (*QuickModeGame_getSObject()).igb.num == 8
			|| (*QuickModeGame_getSObject()).igb.num == 9
			|| (*QuickModeGame_getSObject()).igb.num == 10
			|| (*QuickModeGame_getSObject()).igb.num == 11)
	{
		(*QuickModeGame_getSObject()).igb.num2 = GameFramework_Rand(1, 3);
		if ((*QuickModeGame_getSObject()).igb.num2 == 1)
		{
			if ((*Enemigo_getSObject()).KamiTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).KamiTotal),
						(*Enemigo_getSObject()).KamiTotal.size - 1);
			}
		}
		if ((*QuickModeGame_getSObject()).igb.num2 == 2)
		{
			if ((*Enemigo_getSObject()).KamiTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).KamiTotal),
						(*Enemigo_getSObject()).KamiTotal.size - 1);
			}
		}
	}
	if ((*QuickModeGame_getSObject()).igb.num == 12
			|| (*QuickModeGame_getSObject()).igb.num == 13
			|| (*QuickModeGame_getSObject()).igb.num == 14
			|| (*QuickModeGame_getSObject()).igb.num == 15)
	{
		(*QuickModeGame_getSObject()).igb.num2 = GameFramework_Rand(1, 3);
		if ((*QuickModeGame_getSObject()).igb.num2 == 1)
		{
			if ((*Enemigo_getSObject()).SidTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem,
						(*QuickModeGame_getSObject()).igb.velocidadEnemY,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).SidTotal),
						(*Enemigo_getSObject()).SidTotal.size - 1);
			}
		}
		if ((*QuickModeGame_getSObject()).igb.num2 == 2)
		{
			if ((*Enemigo_getSObject()).SidTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem,
						(*QuickModeGame_getSObject()).igb.velocidadEnemY,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).SidTotal),
						(*Enemigo_getSObject()).SidTotal.size - 1);
			}
		}
	}

}

Mision11* Mision11_getSObject()
{
	return &Mision11S;
}

