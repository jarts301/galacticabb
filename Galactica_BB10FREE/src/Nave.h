/*
 * Nave.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef NAVE_H_
#define NAVE_H_

#include "GalacticaTipos.h"

void Nave_load();
void Nave_reiniciar();
void Nave_update();
void Nave_draw();
void Nave_defineCollitionBox();
void Nave_setPosition(Vector2 position);
Vector2 Nave_getPosition();
void Nave_SetTexturas();
void Nave_setTrozoAnimacion();
void Nave_setOrigen();
void Nave_puntoInicio(Vector2 pos);
void Nave_reiniciarDisparos();
void Nave_reiniciaPoderes();
void Nave_dibujaDisparo();
void Nave_velocidadAnimacion(int velocidad);
void Nave_cicloDisparos();
void Nave_analisisPos();
void Nave_loadPoder();
void Nave_updatePoder();
void Nave_drawPoder();
void Nave_iniciaAnimacionesPoder();
void Nave_updateColisionesJefe();
void Nave_loadDisparos();
void Nave_drawAnimacionesPoder();
Nave* Nave_getSObject();


#endif /* NAVE_H_ */
