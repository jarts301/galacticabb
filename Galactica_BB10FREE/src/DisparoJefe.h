/*
 * DisparoJefe.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef DISPAROJEFE_H_
#define DISPAROJEFE_H_

#include "GalacticaTipos.h"

DisparoJefe* DisparoJefe_crearObjecto(Vector2 posEnem, DisparoJefe_tipo t);
void DisparoJefe_load();
void DisparoJefe_update(DisparoJefe* disparoJefe);
void DisparoJefe_draw(DisparoJefe* disparoJefe);
void DisparoJefe_movimDisparoMaximo(DisparoJefe* disparoJefe);
void DisparoJefe_movimDisparoNormal(DisparoJefe* disparoJefe);
void DisparoJefe_setPosition(DisparoJefe* disparoJefe, Vector2 p, int Derecha);
void DisparoJefe_setPosition2(DisparoJefe* disparoJefe, Vector2 p);
Vector2 DisparoJefe_getPosition(DisparoJefe* disparoJefe);
void DisparoJefe_setTextura();
void DisparoJefe_setOrigen(DisparoJefe* disparoJefe);
void DisparoJefe_defineTrozoAnim(DisparoJefe* disparoJefe, int x, int y,
		int width, int height);
void DisparoJefe_velocidadAnimacion(DisparoJefe* disparoJefe);

DisparoJefe* DisparoJefe_getSObject();

#endif /* DISPAROJEFE_H_ */
