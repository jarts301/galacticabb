/*
 * Mision9.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "Mision9.h"
#include "QuickModeGame.h"
#include "Animation.h"
#include "Jefe.h"
#include "BarraVida.h"
#include "Boton.h"
#include "Nave.h"
#include "Lista.h"
#include "Control.h"
#include "Textura.h"
#include "Dibujar.h"
#include "Audio.h"
#include "Mision1.h"
#include "Enemigo.h"
#include "MisionMode.h"
#include "CollitionJefe.h"
#include "Almacenamiento.h"
#include "MisionModeGOver.h"
#include "Cadena.h"

static Mision9 Mision9S;

void Mision9_reiniciar()
{
	QuickModeGame_eliminarTodos();
	QuickModeGame_eliminaAnimaciones();
	Mision9S.positionToBeat.X = 6;
	Mision9S.positionToBeat.Y = 900;
	Mision9S.positionTime.X = 340;
	Mision9S.positionTime.Y = 50;
	Lista_Destruir(&((*QuickModeGame_getSObject()).igb.botones));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BPause, 5, 950));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BAudio, 530, 950));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BPower, 480, 20));
	(*QuickModeGame_getSObject()).igb.velocidadEnem = 2;
	(*QuickModeGame_getSObject()).igb.velocidadEnemY = 2;
	(*QuickModeGame_getSObject()).igb.velocidadHiEnem = 3;
	(*QuickModeGame_getSObject()).igb.velocidadHiEnemY = 3;
	(*QuickModeGame_getSObject()).igb.maxEnemigos = 7;
	Mision9S.puntosAVencer = 3100;
	Mision9S.cuentaTiempo = 120;
	(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo = 0;
	Mision9S.ultimoTMinuto = 0;
	(*QuickModeGame_getSObject()).igb.ultimoAumento = 0;
	(*QuickModeGame_getSObject()).igb.EscalaMensaje =
			(*QuickModeGame_getSObject()).igb.escalaInicialMensaje = 1.5f;
	(*QuickModeGame_getSObject()).igb.actualFrameMensaje = 0;
	(*QuickModeGame_getSObject()).igb.tiempoTitileoMensaje = 0;
	(*QuickModeGame_getSObject()).igb.mensajeInicialActivo = 1;
	(*Nave_getSObject()).disparosDiagActivos = 1;
}

void Mision9_load()
{
	Mision9S.tituloMision = "Mission 9";

	Lista_Inicia(&Mision9S.mensajeMision);
	Lista_AddInFin(&Mision9S.mensajeMision,
			String_creaString("Destroy the greater"));
	Lista_AddInFin(&Mision9S.mensajeMision,
			String_creaString("amount of enemies and"));
	Lista_AddInFin(&Mision9S.mensajeMision,
			String_creaString("surpass the required"));
	Lista_AddInFin(&Mision9S.mensajeMision,
			String_creaString("score before the "));
	Lista_AddInFin(&Mision9S.mensajeMision, String_creaString("time finish."));
	Lista_AddInFin(&Mision9S.mensajeMision,
			String_creaString("Tip:Destroy the enemies"));
	Lista_AddInFin(&Mision9S.mensajeMision,
			String_creaString("as fast as possible."));

	Lista_Inicia(&Mision9S.recompensa);
	Lista_AddInFin(&Mision9S.recompensa,
			String_creaString("Maximum energy increased"));
	Lista_AddInFin(&Mision9S.recompensa,
			String_creaString("by 100, in Quick Game."));

	Lista_Inicia(&Mision9S.noRecompensa);
	Lista_AddInFin(&Mision9S.noRecompensa, String_creaString("No Reward.."));

	Mision9S.puntosAVencer = 3100, Mision9S.cuentaTiempo = 120;
}

void Mision9_update()
{
	Nave_update();
	QuickModeGame_updateEstrellas();
	QuickModeGame_updateEnemigos();
	Mision9_updateGame();
	QuickModeGame_updateAnimaciones();
	for (Mision9S.i = 0;
			Mision9S.i < (*QuickModeGame_getSObject()).igb.botones.size;
			Mision9S.i++)
	{
		Boton_update(
				(Boton*) Lista_GetIn(
						&((*QuickModeGame_getSObject()).igb.botones),
						Mision9S.i));
	}

	if (GameFramework_getTotalTime() > Mision9S.ultimoTMinuto + 1000000)
	{
		Mision9S.cuentaTiempo = Mision9S.cuentaTiempo - 1;
		Mision9S.ultimoTMinuto = GameFramework_getTotalTime();
	}

	QuickModeGame_updateMensajeInicial();
	BarraVida_update(((*QuickModeGame_getSObject()).igb.barraVidaNave),
			(*Nave_getSObject()).Vida, (*Nave_getSObject()).MaxVida);

	if ((*Nave_getSObject()).Vida <= 0)
	{
		Animation_update((*Animation_getSObject()).animacionesEstallidoNave);
	}
	Mision9_gameOver();

	if ((*Control_getSObject()).controlActivo == 1)
	{
		Control_update(((*QuickModeGame_getSObject()).igb.control));
	}
}

void Mision9_draw()
{
	//CLEAR NEGRO
	Dibujar_drawSprite(Textura_getTexturaFondo(0), GameFramework_Vector2(0, 0),
			GameFramework_Color(0, 0, 0, 1), 0, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(40, 40));

	Nave_draw();
	QuickModeGame_drawEstrellas();
	QuickModeGame_drawEnemigos();
	Mision9_drawEstadoNave();
	QuickModeGame_drawAnimaciones();
	Mision9_drawMensajeInicial();
	if ((*Nave_getSObject()).Vida <= 0)
	{
		Animation_draw((*Animation_getSObject()).animacionesEstallidoNave);
	}

	for (Mision9S.i = 0;
			Mision9S.i < (*QuickModeGame_getSObject()).igb.botones.size;
			Mision9S.i++)
	{
		Boton_draw(
				(Boton*) Lista_GetIn(
						&((*QuickModeGame_getSObject()).igb.botones),
						Mision9S.i));
	}

	if ((*Control_getSObject()).controlActivo == 1)
	{
		Control_draw(((*QuickModeGame_getSObject()).igb.control));
	}
	// drawEstadisticas(sb);

}

//******************Estadisticas de la nave*************
void Mision9_drawEstadoNave()
{

	//** Vida de la nave
	Dibujar_drawSprite(Textura_getTexturaFondo(1),
			GameFramework_Vector2(0, 945), GameFramework_Color(0, 0, 0, 1), 0,
			GameFramework_Vector2(0, 0), GameFramework_Vector2(50, 2.6));

	BarraVida_draw((*QuickModeGame_getSObject()).igb.barraVidaNave);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_EnteroACadena((*Nave_getSObject()).Vida),
			GameFramework_Vector2(100, 975), GameFramework_Color(1, 1, 1, 1),
			0.8);
	//**Puntos
	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_EnteroACadena((*Nave_getSObject()).Puntos),
			GameFramework_Vector2(310, 965),
			GameFramework_Color(GameFramework_getByteColorToFloatColor(244),
					GameFramework_getByteColorToFloatColor(232),
					GameFramework_getByteColorToFloatColor(16), 1), 1.4);

	Dibujar_drawText((*Textura_getSObject()).Fuente, "/",
			GameFramework_Vector2(285, 965), GameFramework_Color(1, 1, 1, 1),
			1.4);

	Nave_drawAnimacionesPoder();

	//****Puntos a vencer
	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "To Beat:",
					GameFramework_EnteroACadena(Mision9S.puntosAVencer)),
			Mision9S.positionToBeat, GameFramework_Color(1, 1, 1, 1), 0.9);

	//Tiempo restante
	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "Time:",
					GameFramework_EnteroACadena(Mision9S.cuentaTiempo)),
			GameFramework_Vector2(420, 900),
			GameFramework_Color(GameFramework_getByteColorToFloatColor(244),
					GameFramework_getByteColorToFloatColor(232),
					GameFramework_getByteColorToFloatColor(16), 1), 0.9);

}

void Mision9_updateGame()
{
	if ((*QuickModeGame_getSObject()).igb.enemigos.size
			< (*QuickModeGame_getSObject()).igb.maxEnemigos)
	{
		Mision9_insertaEnemigo();
	}
}

void Mision9_gameOver()
{
	if (Mision9S.cuentaTiempo <= 0)
	{
		if ((*Audio_getSObject()).musicaActiva == 1)
		{
			Audio_stopAudio("Galactic");
			Audio_playAudio("GameOver");
		}
		(*MisionModeGOver_getSObject()).gana = 0;
		(*MisionModeGOver_getSObject()).cosasGanadas = &Mision9S.noRecompensa;
		(*MisionMode_getSObject()).estadoActual = MMGameOver;
	}

	if (Mision9S.puntosAVencer < (*Nave_getSObject()).Puntos)
	{
		if ((*Audio_getSObject()).musicaActiva == 1)
		{
			Audio_stopAudio("Galactic");
			Audio_playAudio("GameOver");
		}
		(*MisionModeGOver_getSObject()).gana = 1;

		Almacenamiento_cargarMisiones();
		if (Almacenamiento_getPremiosDados() == 8)
		{
			Almacenamiento_salvarBonus(400, ALsumarAsalud);
			(*MisionModeGOver_getSObject()).cosasGanadas = &Mision9S.recompensa;
			Almacenamiento_salvarMisiones();
		}
		else
		{
			(*MisionModeGOver_getSObject()).cosasGanadas =
					&(*Mision1_getSObject()).yaRecompensa;
		}

		(*MisionMode_getSObject()).estadoActual = MMGameOver;
	}

	if ((*Nave_getSObject()).Vida <= 0)
	{
		(*Nave_getSObject()).Vida = 0;
		(*Nave_getSObject()).EscalaN = GameFramework_Vector2(0, 0);
		Animation_setPosition(
				(*Animation_getSObject()).animacionesEstallidoNave,
				(*Nave_getSObject()).PositionNav);
		(*MisionModeGOver_getSObject()).gana = 0;
		(*MisionModeGOver_getSObject()).cosasGanadas = &Mision9S.noRecompensa;
		if ((*(*Animation_getSObject()).animacionesEstallidoNave).animacionFinalizada
				== 1)
		{
			if ((*Audio_getSObject()).musicaActiva == 1)
			{
				Audio_stopAudio("Galactic");
				Audio_playAudio("GameOver");
			}
			(*MisionMode_getSObject()).estadoActual = MMGameOver;
		}
	}
}

//********Dibujo Mensaje Inicial
void Mision9_drawMensajeInicial()
{
	if ((*QuickModeGame_getSObject()).igb.mensajeInicialActivo == 1)
	{
		Dibujar_drawText((*Textura_getSObject()).Fuente,
				GameFramework_Concatenar(2, Mision9S.tituloMision, " Start!!"),
				GameFramework_Vector2(50, 620),
				GameFramework_Color(GameFramework_getByteColorToFloatColor(7),
						GameFramework_getByteColorToFloatColor(237),
						GameFramework_getByteColorToFloatColor(134), 1),
				(*QuickModeGame_getSObject()).igb.EscalaMensaje);
	}
}

void Mision9_insertaEnemigo()
{
	(*QuickModeGame_getSObject()).igb.num = 0;
	(*QuickModeGame_getSObject()).igb.num2 = 0;
	(*QuickModeGame_getSObject()).igb.num = GameFramework_Rand(1, 19);

	if ((*QuickModeGame_getSObject()).igb.num == 1
			|| (*QuickModeGame_getSObject()).igb.num == 2
			|| (*QuickModeGame_getSObject()).igb.num == 3
			|| (*QuickModeGame_getSObject()).igb.num == 4
			|| (*QuickModeGame_getSObject()).igb.num == 5
			|| (*QuickModeGame_getSObject()).igb.num == 6)
	{
		(*QuickModeGame_getSObject()).igb.num2 = GameFramework_Rand(1, 2);
		if ((*QuickModeGame_getSObject()).igb.num2 == 1)
		{
			if ((*Enemigo_getSObject()).NorTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).NorTotal),
								(*Enemigo_getSObject()).NorTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).NorTotal),
								(*Enemigo_getSObject()).NorTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).NorTotal),
						(*Enemigo_getSObject()).NorTotal.size - 1);
			}
		}
		if ((*QuickModeGame_getSObject()).igb.num2 == 2)
		{
			if ((*Enemigo_getSObject()).NorTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).NorTotal),
								(*Enemigo_getSObject()).NorTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).NorTotal),
								(*Enemigo_getSObject()).NorTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).NorTotal),
						(*Enemigo_getSObject()).NorTotal.size - 1);
			}
		}
	}
	if ((*QuickModeGame_getSObject()).igb.num == 7
			|| (*QuickModeGame_getSObject()).igb.num == 8
			|| (*QuickModeGame_getSObject()).igb.num == 9
			|| (*QuickModeGame_getSObject()).igb.num == 10
			|| (*QuickModeGame_getSObject()).igb.num == 11)
	{
		(*QuickModeGame_getSObject()).igb.num2 = GameFramework_Rand(1, 3);
		if ((*QuickModeGame_getSObject()).igb.num2 == 1)
		{
			if ((*Enemigo_getSObject()).KamiTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).KamiTotal),
						(*Enemigo_getSObject()).KamiTotal.size - 1);
			}
		}
		if ((*QuickModeGame_getSObject()).igb.num2 == 2)
		{
			if ((*Enemigo_getSObject()).KamiTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).KamiTotal),
								(*Enemigo_getSObject()).KamiTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).KamiTotal),
						(*Enemigo_getSObject()).KamiTotal.size - 1);
			}
		}
	}
	if ((*QuickModeGame_getSObject()).igb.num == 12
			|| (*QuickModeGame_getSObject()).igb.num == 13
			|| (*QuickModeGame_getSObject()).igb.num == 14
			|| (*QuickModeGame_getSObject()).igb.num == 15)
	{
		(*QuickModeGame_getSObject()).igb.num2 = GameFramework_Rand(1, 3);
		if ((*QuickModeGame_getSObject()).igb.num2 == 1)
		{
			if ((*Enemigo_getSObject()).SidTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem,
						(*QuickModeGame_getSObject()).igb.velocidadEnemY,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).SidTotal),
						(*Enemigo_getSObject()).SidTotal.size - 1);
			}
		}
		if ((*QuickModeGame_getSObject()).igb.num2 == 2)
		{
			if ((*Enemigo_getSObject()).SidTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem,
						(*QuickModeGame_getSObject()).igb.velocidadEnemY,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).SidTotal),
								(*Enemigo_getSObject()).SidTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).SidTotal),
						(*Enemigo_getSObject()).SidTotal.size - 1);
			}
		}
	}
	if ((*QuickModeGame_getSObject()).igb.num == 16
			|| (*QuickModeGame_getSObject()).igb.num == 17)
	{
		(*QuickModeGame_getSObject()).igb.num2 = GameFramework_Rand(1, 2);
		if ((*QuickModeGame_getSObject()).igb.num2 == 1)
		{
			if ((*Enemigo_getSObject()).HiNorTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).HiNorTotal),
								(*Enemigo_getSObject()).HiNorTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).HiNorTotal),
								(*Enemigo_getSObject()).HiNorTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).HiNorTotal),
						(*Enemigo_getSObject()).HiNorTotal.size - 1);
			}
		}
		if ((*QuickModeGame_getSObject()).igb.num2 == 2)
		{
			if ((*Enemigo_getSObject()).HiNorTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).HiNorTotal),
								(*Enemigo_getSObject()).HiNorTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).HiNorTotal),
								(*Enemigo_getSObject()).HiNorTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).HiNorTotal),
						(*Enemigo_getSObject()).HiNorTotal.size - 1);
			}
		}
	}
	if ((*QuickModeGame_getSObject()).igb.num == 18
			|| (*QuickModeGame_getSObject()).igb.num == 19)
	{
		(*QuickModeGame_getSObject()).igb.num2 = GameFramework_Rand(1, 3);
		if ((*QuickModeGame_getSObject()).igb.num2 == 1)
		{
			if ((*Enemigo_getSObject()).HiKamiTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).HiKamiTotal),
								(*Enemigo_getSObject()).HiKamiTotal.size - 1),
						(-1) * GameFramework_Rand(100, 500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).HiKamiTotal),
								(*Enemigo_getSObject()).HiKamiTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).HiKamiTotal),
						(*Enemigo_getSObject()).HiKamiTotal.size - 1);
			}
		}
		if ((*QuickModeGame_getSObject()).igb.num2 == 2)
		{
			if ((*Enemigo_getSObject()).HiKamiTotal.size > 0)
			{
				Enemigo_reinicia(
						(Enemigo*) Lista_GetIn(
								&((*Enemigo_getSObject()).HiKamiTotal),
								(*Enemigo_getSObject()).HiKamiTotal.size - 1),
						GameFramework_Rand(1000, 1500),
						GameFramework_Rand(600, 1000),
						(*QuickModeGame_getSObject()).igb.velocidadEnem, 0,
						(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo);

				Lista_AddInFin(&((*QuickModeGame_getSObject()).igb.enemigos),
						Lista_GetIn(&((*Enemigo_getSObject()).HiKamiTotal),
								(*Enemigo_getSObject()).HiKamiTotal.size - 1));

				Lista_RemoveAt(&((*Enemigo_getSObject()).HiKamiTotal),
						(*Enemigo_getSObject()).HiKamiTotal.size - 1);
			}
		}
	}

}

Mision9* Mision9_getSObject()
{
	return &Mision9S;
}
