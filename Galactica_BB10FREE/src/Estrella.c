/*
 * Estrella.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "Estrella.h"
#include "GameFramework.h"
#include "AcercaDe.h"
#include "Textura.h"
#include "Boton.h"
#include "Lista.h"
#include "Dibujar.h"
#include <stdio.h>
#include <stdlib.h>

static Estrella EstrellaS;

/*
 public Estrella()
 {
 }
 */
Estrella* Estrella_crear(Vector2 posIni)
{
	Estrella* nuevo;
	if ((nuevo = (Estrella*) malloc(sizeof(Estrella))) == NULL)
		return NULL;

	Estrella_setOrigen(nuevo);
	Estrella_setPosition(nuevo, posIni);

	return nuevo;
}

void Estrella_load()
{
	Estrella_setTextura();
}

void Estrella_update(Estrella* estrella)
{
	Estrella_movimientoEstrella(estrella);
}

void Estrella_draw(Estrella* estrella)
{
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_RetornaElemento(&(EstrellaS.textura), 0),
			(*estrella).Position, (*estrella).color, 0.0f, (*estrella).Origen,
			(*estrella).Escala);
}

//**********************************

void Estrella_setTextura()
{
	Lista_Inicia(&(EstrellaS.textura));
	Lista_InsertaEnFinal(&(EstrellaS.textura), EstrellaS.textura.fin,
			Textura_getTextura("Estrella")); //0
}

void Estrella_setPosition(Estrella* estrella,Vector2 pos)
{
	(*estrella).Position = pos;
}

void Estrella_setOrigen(Estrella* estrella)
{
	(*estrella).Origen.X = (*(TexturaObjeto*) Lista_GetIn(&(EstrellaS.textura),
			0)).ancho / 2;
	(*estrella).Origen.Y = (*(TexturaObjeto*) Lista_GetIn(&(EstrellaS.textura),
			0)).alto / 2;
}

void Estrella_movimientoEstrella(Estrella* estrella)
{
	if ((*estrella).Position.Y > -10)
	{
		(*estrella).Position.Y = (*estrella).Position.Y - (*estrella).vel;
	}
	else
	{
		(*estrella).Position.Y = 1034;
		(*estrella).Position.X = GameFramework_Rand(0, 600);
	}
}

