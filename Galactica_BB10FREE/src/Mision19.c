/*
 * Mision19.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "Mision19.h"
#include "QuickModeGame.h"
#include "Enemigo.h"
#include "Animation.h"
#include "Jefe.h"
#include "BarraVida.h"
#include "Boton.h"
#include "Nave.h"
#include "Lista.h"
#include "Control.h"
#include "Textura.h"
#include "Dibujar.h"
#include "Audio.h"
#include "Mision1.h"
#include "MisionMode.h"
#include "CollitionJefe.h"
#include "Almacenamiento.h"
#include "MisionModeGOver.h"
#include "Cadena.h"

static Mision19 Mision19S;

void Mision19_reiniciar()
{
	QuickModeGame_eliminarTodos();
	QuickModeGame_eliminaAnimaciones();
	Mision19S.positionToBeat.X = 6;
	Mision19S.positionToBeat.Y = 50;
	Mision19S.positionTime.X = 340;
	Mision19S.positionTime.Y = 50;
	Mision19S.positionEnemigoS.X = 6;
	Mision19S.positionEnemigoS.Y = 900;
	Lista_Destruir(&((*QuickModeGame_getSObject()).igb.botones));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BPause, 5, 950));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BAudio, 530, 950));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BPower, 480, 20));
	(*QuickModeGame_getSObject()).igb.velocidadEnem = 2;
	(*QuickModeGame_getSObject()).igb.velocidadEnemY = 2;
	(*QuickModeGame_getSObject()).igb.velocidadHiEnem = 3;
	(*QuickModeGame_getSObject()).igb.velocidadHiEnemY = 3;
	(*QuickModeGame_getSObject()).igb.maxEnemigos = 5;
	Mision19S.cuentaTiempo = 90;
	(*Mision19S.elEnemigo).movimientoLoco = 1;
	Enemigo_setPosition(Mision19S.elEnemigo, GameFramework_Vector2(300, 750));
	(*Mision19S.elEnemigo).maxVida = 300;
	(*Mision19S.elEnemigo).vida = 30;
	(*Mision19S.elEnemigo).velMinLoco = 4;
	(*Mision19S.elEnemigo).velMaxLoco = 8;
	(*Mision19S.elEnemigo).locoAbajo = 450;
	(*Mision19S.elEnemigo).locoArriba = 900;
	(*Mision19S.elEnemigo).locoDerecha = 600;
	(*Mision19S.elEnemigo).locoIzquierda = 0;
	(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo = 0;
	Mision19S.ultimoTMinuto = 0;
	(*QuickModeGame_getSObject()).igb.ultimoAumento = 0;
	(*QuickModeGame_getSObject()).igb.EscalaMensaje =
			(*QuickModeGame_getSObject()).igb.escalaInicialMensaje = 1.5f;
	(*QuickModeGame_getSObject()).igb.actualFrameMensaje = 0;
	(*QuickModeGame_getSObject()).igb.tiempoTitileoMensaje = 0;
	(*QuickModeGame_getSObject()).igb.mensajeInicialActivo = 1;
	(*Nave_getSObject()).disparoSuperActivo = 1;
	Nave_puntoInicio(GameFramework_Vector2(300, 250));
}

void Mision19_load()
{
	Mision19S.tituloMision = "Mission 19";

	Lista_Inicia(&Mision19S.mensajeMision);
	Lista_AddInFin(&Mision19S.mensajeMision,
			String_creaString("Shoot to the enemy all"));
	Lista_AddInFin(&Mision19S.mensajeMision,
			String_creaString("the time, dont let to"));
	Lista_AddInFin(&Mision19S.mensajeMision,
			String_creaString("do it or he will reload"));
	Lista_AddInFin(&Mision19S.mensajeMision,
			String_creaString("all his energy and"));
	Lista_AddInFin(&Mision19S.mensajeMision,
			String_creaString("you will lose."));
	Lista_AddInFin(&Mision19S.mensajeMision,
			String_creaString("Tip:Shoot as fast as"));
	Lista_AddInFin(&Mision19S.mensajeMision, String_creaString("you can."));

	Lista_Inicia(&Mision19S.recompensa);
	Lista_AddInFin(&Mision19S.recompensa,
			String_creaString("Decreases power timeout."));
	Lista_AddInFin(&Mision19S.recompensa, String_creaString("in Quick Game."));

	Lista_Inicia(&Mision19S.noRecompensa);
	Lista_AddInFin(&Mision19S.noRecompensa, String_creaString("No Reward.."));

	Mision19S.cuentaTiempo = 90;
	Mision19S.elEnemigo = Enemigo_Crear(ENor, 300, 750, 0, 0);
	Mision19S.barraVidaEnemigo = BarraVida_Crear(GameFramework_Vector2(6, 890),
			BVmini);
}

void Mision19_update()
{
	Nave_update();
	QuickModeGame_updateEstrellas();
	(*Nave_getSObject()).movEnY = 0;
	Enemigo_update(Mision19S.elEnemigo);
	if ((*Mision19S.elEnemigo).estaEnColisionDisp)
	{
		(*Mision19S.elEnemigo).estaEnColisionDisp = 0;
		if ((*Animation_getSObject()).animacionesDispAEnemigoTotal.size > 0)
		{
			Animation_setPosition(
					(Animation*) Lista_GetIn(
							&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
							(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
									- 1), (*Mision19S.elEnemigo).Position);

			Lista_AddInFin(&(*QuickModeGame_getSObject()).igb.animacionesQ,
					Lista_GetIn(
							&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
							(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
									- 1));

			Lista_RemoveAt(
					&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
					(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
							- 1);
		}
	}

	QuickModeGame_updateAnimaciones();
	for (Mision19S.i = 0;
			Mision19S.i < (*QuickModeGame_getSObject()).igb.botones.size;
			Mision19S.i++)
	{
		Boton_update(
				(Boton*) Lista_GetIn(&(*QuickModeGame_getSObject()).igb.botones,
						Mision19S.i));
	}

	if (GameFramework_getTotalTime() > Mision19S.ultimoTMinuto + 1000000)
	{
		Mision19S.cuentaTiempo = Mision19S.cuentaTiempo - 1;
		Mision19S.ultimoTMinuto = GameFramework_getTotalTime();
		(*Mision19S.elEnemigo).vida = (*Mision19S.elEnemigo).vida + 17;
	}
	if ((*Mision19S.elEnemigo).vida <= 0)
	{
		(*Mision19S.elEnemigo).vida = 20;
	}

	QuickModeGame_updateMensajeInicial();
	BarraVida_update((*QuickModeGame_getSObject()).igb.barraVidaNave,
			(*Nave_getSObject()).Vida, (*Nave_getSObject()).MaxVida);

	BarraVida_update(Mision19S.barraVidaEnemigo, (*Mision19S.elEnemigo).vida,
			(*Mision19S.elEnemigo).maxVida);

	if ((*Nave_getSObject()).Vida <= 0)
	{
		Animation_update((*Animation_getSObject()).animacionesEstallidoNave);
	}
	Mision19_gameOver();
	if ((*Control_getSObject()).controlActivo == 1)
	{
		Control_update((*QuickModeGame_getSObject()).igb.control);
	}
}

void Mision19_draw()
{
	//CLEAR NEGRO
	Dibujar_drawSprite(Textura_getTexturaFondo(0), GameFramework_Vector2(0, 0),
			GameFramework_Color(0, 0, 0, 1), 0, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(40, 40));

	Nave_draw();
	QuickModeGame_drawEstrellas();
	Mision19_drawEstadoNave();
	Enemigo_draw(Mision19S.elEnemigo);
	QuickModeGame_drawAnimaciones();
	Mision19_drawMensajeInicial();
	if ((*Nave_getSObject()).Vida <= 0)
	{
		Animation_draw((*Animation_getSObject()).animacionesEstallidoNave);
	}

	for (Mision19S.i = 0;
			Mision19S.i < (*QuickModeGame_getSObject()).igb.botones.size;
			Mision19S.i++)
	{
		Boton_draw(
				(Boton*) Lista_GetIn(&(*QuickModeGame_getSObject()).igb.botones,
						Mision19S.i));
	}
	if ((*Control_getSObject()).controlActivo == 1)
	{
		Control_draw((*QuickModeGame_getSObject()).igb.control);
	}
	// drawEstadisticas(sb);
}

//******************Estadisticas de la nave*************
void Mision19_drawEstadoNave()
{
	//** Vida de la nave
	Dibujar_drawSprite(Textura_getTexturaFondo(1),
			GameFramework_Vector2(0, 945), GameFramework_Color(0, 0, 0, 1), 0,
			GameFramework_Vector2(0, 0), GameFramework_Vector2(50, 2.6));

	BarraVida_draw((*QuickModeGame_getSObject()).igb.barraVidaNave);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_EnteroACadena((*Nave_getSObject()).Vida),
			GameFramework_Vector2(100, 975), GameFramework_Color(1, 1, 1, 1),
			0.8);

	Dibujar_drawText((*Textura_getSObject()).Fuente, "/",
			GameFramework_Vector2(285, 965), GameFramework_Color(1, 1, 1, 1),
			1.4);

	Nave_drawAnimacionesPoder();

	//Tiempo restante
	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_Concatenar(2, "Time:",
					GameFramework_EnteroACadena(Mision19S.cuentaTiempo)),
			GameFramework_Vector2(420, 900),
			GameFramework_Color(GameFramework_getByteColorToFloatColor(244),
					GameFramework_getByteColorToFloatColor(232),
					GameFramework_getByteColorToFloatColor(16), 1), 0.9);

	BarraVida_draw(Mision19S.barraVidaEnemigo);

	Dibujar_drawText((*Textura_getSObject()).Fuente, "Enemy",
			Mision19S.positionEnemigoS, GameFramework_Color(1, 1, 1, 1), 0.75);
}

void Mision19_gameOver()
{
	if (Mision19S.cuentaTiempo <= 0)
	{
		if ((*Audio_getSObject()).musicaActiva == 1)
		{
			Audio_stopAudio("Galactic");
			Audio_playAudio("GameOver");
		}
		(*MisionModeGOver_getSObject()).gana = 1;

		Almacenamiento_cargarMisiones();
		if (Almacenamiento_getPremiosDados() == 18)
		{
			Almacenamiento_salvarBonus(2000000, ALtEsperaPoder);
			(*MisionModeGOver_getSObject()).cosasGanadas =
					&Mision19S.recompensa;
			Almacenamiento_salvarMisiones();
		}
		else
		{
			(*MisionModeGOver_getSObject()).cosasGanadas =
					&(*Mision1_getSObject()).yaRecompensa;
		}

		(*MisionMode_getSObject()).estadoActual = MMGameOver;
	}

	if ((*Nave_getSObject()).Vida <= 0
			|| (*Mision19S.elEnemigo).vida >= (*Mision19S.elEnemigo).maxVida)
	{
		(*Nave_getSObject()).Vida = 0;
		(*Mision19S.elEnemigo).vida = (*Mision19S.elEnemigo).maxVida;
		(*Nave_getSObject()).EscalaN = GameFramework_Vector2(0, 0);
		Animation_setPosition(
				(*Animation_getSObject()).animacionesEstallidoNave,
				(*Nave_getSObject()).PositionNav);
		(*MisionModeGOver_getSObject()).gana = 0;
		(*MisionModeGOver_getSObject()).cosasGanadas = &Mision19S.noRecompensa;
		if ((*(*Animation_getSObject()).animacionesEstallidoNave).animacionFinalizada
				== 1)
		{
			if ((*Audio_getSObject()).musicaActiva == 1)
			{
				Audio_stopAudio("Galactic");
				Audio_playAudio("GameOver");
			}
			(*MisionMode_getSObject()).estadoActual = MMGameOver;
		}
	}
}

//********Dibujo Mensaje Inicial
void Mision19_drawMensajeInicial()
{
	if ((*QuickModeGame_getSObject()).igb.mensajeInicialActivo == 1)
	{
		Dibujar_drawText((*Textura_getSObject()).Fuente,
				GameFramework_Concatenar(2, Mision19S.tituloMision, " Start!!"),
				GameFramework_Vector2(50, 620),
				GameFramework_Color(GameFramework_getByteColorToFloatColor(7),
						GameFramework_getByteColorToFloatColor(237),
						GameFramework_getByteColorToFloatColor(134), 1),
				(*QuickModeGame_getSObject()).igb.EscalaMensaje);
	}
}

Mision19* Mision19_getSObject()
{
	return &Mision19S;
}
