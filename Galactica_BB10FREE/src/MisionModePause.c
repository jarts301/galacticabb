/*
 * MisionModePause.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "MisionModePause.h"
#include "Lista.h"
#include "Boton.h"
#include "Nave.h"
#include "Dibujar.h"
#include "Textura.h"
#include "GameComun.h"

static MisionModePause MisionModePauseS;

void MisionModePause_load()
{
	MisionModePause_setTexturas();
	MisionModePause_loadTitulo();
	Lista_Inicia(&MisionModePauseS.botones);
	Lista_AddInFin(&MisionModePauseS.botones,
			Boton_Crear(BResumeGame, 120, 620));
	Lista_AddInFin(&MisionModePauseS.botones, Boton_Crear(BExitGame, 120, 440));
}

void MisionModePause_update()
{
	GameComun_updateEstrellas(time);
	for (MisionModePauseS.i = 0;
			MisionModePauseS.i < MisionModePauseS.botones.size;
			MisionModePauseS.i++)
	{
		Boton_update(
				(Boton*) Lista_GetIn(&MisionModePauseS.botones,
						MisionModePauseS.i));
	}
	(*Boton_getSObject()).ultimaActualizacion = GameFramework_getTotalTime();
	(*Nave_getSObject()).ultimaActualizacion = GameFramework_getTotalTime();
}

void MisionModePause_draw()
{
	//CLEAR NEGRO
	Dibujar_drawSprite(Textura_getTexturaFondo(2), GameFramework_Vector2(0, 0),
			GameFramework_Color(1, 1, 1, 1), 0, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(2.5, 2));

	GameComun_drawEstrellas();
	MisionModePause_drawTitulo();
	for (MisionModePauseS.i = 0;
			MisionModePauseS.i < MisionModePauseS.botones.size;
			MisionModePauseS.i++)
	{
		Boton_draw(
				(Boton*) Lista_GetIn(&MisionModePauseS.botones,
						MisionModePauseS.i));
	}
}

//******************************************

//******TITULO***********
void MisionModePause_loadTitulo()
{
	MisionModePauseS.positionTitulo = GameFramework_Vector2(300, 880);
	MisionModePauseS.origenTitulo = GameFramework_Vector2(
			(*(TexturaObjeto*) Lista_GetIn(&MisionModePauseS.textura, 0)).ancho
					/ 2,
			(*(TexturaObjeto*) Lista_GetIn(&MisionModePauseS.textura, 0)).alto
					/ 2);
}

void MisionModePause_drawTitulo()
{
	Dibujar_drawSprite(
			(TexturaObjeto*) Lista_GetIn(&MisionModePauseS.textura, 0),
			MisionModePauseS.positionTitulo, GameFramework_Color(1, 1, 1, 1), 0,
			MisionModePauseS.origenTitulo, GameFramework_Vector2(1.0, 1.0));
}

//*********Texturas***************
void MisionModePause_setTexturas()
{
	Lista_Inicia(&MisionModePauseS.textura);
	Lista_AddInFin(&MisionModePauseS.textura,
			Textura_getTextura("TituloPauseMision")); //0
}

MisionModePause* MisionModePause_getSObject()
{
	return &MisionModePauseS;
}

