/*
 * GalacticaTipos.h
 *
 *  Created on: 17/05/2013
 *      Author: Jarts
 */

#ifndef GALACTICATIPOS_H_
#define GALACTICATIPOS_H_

#include "GameFramework.h"
#include "bbutil.h"
#include "Lista.h"
#include <GLES/gl.h>
#include <sys/strm.h>
#include <stdio.h>
#include <unistd.h>
#include <mm/renderer.h>
#include <limits.h>
#include <errno.h>
#include <fcntl.h>
#include <gulliver.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/slogcodes.h>
#include <ctype.h>
#include <screen/screen.h>
#include <sys/asoundlib.h>
#include <bps/bps.h>
#include <bps/audiomixer.h>
#include <bps/audiodevice.h>
#include <bps/dialog.h>
#include <bps/navigator.h>

#define SUCCESS  0
#define FAILURE -1
#define MSG_SIZE 1024

struct TexturaObjetoType;
struct ToucheType;
struct NaveType;
struct PlusType;
struct NMiniType;
struct OndaType;

//-***************************Disparo.h ****************************
typedef enum
{
	Dpequei, Dpequed, DdiagI, DdiagD, Dsuper
} Disparo_Tipo;

//-***************************Acelerometro.h*********************************

typedef struct
{
	Vector3 AcelerometroAcelerDat;
	//Static
	int acelActivo;
} Acelerometro;

//-******************************AcercaDe.h******************************
typedef struct
{
	Lista textura; //Static-Texture2D
	Vector2 positionTitulo, origenTitulo, escalaTitulo; //Static
	Color color;
	Lista botones; //Boton
	int i;
} AcercaDe;

//-***************************Almacenamiento.h ****************************
typedef enum
{
	ALsumarAsalud,
	ALdisparoSuper,
	ALdisparoDiag,
	ALnaves,
	ALtEsperaPoder,
	ALpoderDisp,
	ALrecuperacion
} BonusTipo;

typedef struct
{
	char* directoryName;
	char* misionFile;
	char* bonusFile;
	char* gameFile;
	char* scoreFile;
	char* optionsFile;
} Almacenamiento;

typedef struct
{
	int score;
} DatScore;

typedef struct
{
	int activas;
	int premio;
} DatMisiones;

typedef struct
{
	int sumarASaludNave;
	int disparoSuperActivo;
	int disparoDiagActivo;
	int naves;
	int tiempoEsperaPoder;
	int sumaPoderDisparos;
	int recuperacion;
} DatBonus;

typedef struct
{
	int puntos;
	int vidaNave;
	int texturaNaveActual;
	int maxEnemigos;
	int enemigosDestruidos;
	int disparoDiagActivo;
	int disparoSuperActivo;
	int tiempoEsperaPlus;
	int aumentoVidaJefe;
	int aumentoVidaEnemigo;
	int adicionDanoJefe;
	int adicionDanoDispJefe;
	int adicionDanoEnemigo;
	int adicionDanoDispEnemigo;
	int ultimaAdJefe;
	int ultimoAumento;
	int hayJefe;
	int tipoJefe;
	int vidaJefe;
} DatInGame;

typedef struct
{
	int audioActivo;
	int controlMode;
	int buttonSize;
} DatOpciones;

//-***************************Animation.h ****************************
typedef enum
{
	ADisparoANave,
	ADisparoAEnemigo,
	AEstallidoEnemigo,
	AEstallidoJefe,
	AEstallidoNave,
	APoderGalactica,
	APoderCarafe,
	APoderHelix,
	APoderArp,
	APoderPerseus,
	APoderPisces
} Animation_tipo;

typedef struct AnimaExtra
{
	int actualframe, velocidad, dato;
	Vector2 Escala, Position, Origen;
	Rectangle TrozoAnim;
	Lista texturas; //static
	Animation_tipo tipoActual;
	int textura, maxFrames;
	int animacionFinalizada; //bolean
	//Static
	Color Tinte; //=new Color();
	Lista animacionesDispANaveTotal; //static
	Lista animacionesDispAEnemigoTotal; //static
	Lista animacionesEstallidoEnemigoTotal; //static
	Lista animacionesEstallidoJefeTotal; //static
	int w;
	struct AnimaExtra *animacionesEstallidoNave, *animacionEstallidoJefe;
} Animation;

//-***************************Audio.h ****************************
typedef enum
{
	stopped, playing, idle, destroyed
} AudioEstado;

typedef struct
{
	Lista Musica;
	Lista nombreMusica;
	//SoundEffect silencio;
	int i;
	int efectosActivos, musicaActiva; //bool
} Audio;

typedef struct
{
	const char *mmrname;
	const char *ctxtname;
	const char *audioout;
	const char *inputtype;
	char cwd[1024];
	char inputurl[1024];
	int audio_oid;
	strm_dict_t* dic;
	strm_dict_t *aoparams;
	mmr_connection_t *connection;
	mmr_context_t *ctxt;
	mode_t mode;
	void* nombreCod;
	AudioEstado estado;
} Musica;

//-***************************Ayuda.h ****************************
typedef struct
{
	Lista textura; //static-Texture2D
	Vector2 positionTitulo, origenTitulo, escalaTitulo; //static
	Lista botones; //Boton
	int i;
} Ayuda;

//-***************************BarraVida.h ****************************
typedef enum
{
	BVjefe, BVnave, BVmini
} BarraVida_tipo;

typedef struct
{
	struct TexturaObjetoType* texturaBarra; //Static
	struct TexturaObjetoType* texturaMarco; //Static
	float Rotacion;
	Color Tinte;
	Vector2 escala, escalaMarco, Position, Origen;
	float factorEscala;
	float escalaInicialX, escalaInicialY;
} BarraVida;

//-***************************Boton.h ****************************
typedef enum
{
	BQuickGame,
	BPause,
	BResumeGame,
	BExitGame,
	BFlecha,
	BPlay,
	BPower,
	BContinue,
	BMision,
	BMisionMode,
	BBack,
	BNextMision,
	BRestart,
	BAudio,
	BOpciones,
	BSalir,
	BTilting,
	BButtons,
	BMedium,
	BLarge,
	BHelp,
	BAbout,
	BContinueGame,
	BNewGame,
	BFacebook,
	BTwitter,
	BComent,
	BGoBuy,
	BNotNow
} Boton_tipo;

typedef struct
{
	Lista textura; //Statica-Texture2D
	int navesActivas, tiempoEsperaPoder; //Static
	double ultimaActualizacion; //Static
	int indiceMision; //Static =-1
	double dato; //static
	struct ToucheType* touched;
	Boton_tipo tipoActual;
	Color color;
	int texActual;
	float escala;
	int flechaDerecha; //boolean
	float Rotacion;
	int bPoderActivo; //boolean
	int actualframeB;
	int lineaAnim;
	Rectangle TrozoAnim;
	Vector2 Position, Origen, Escala;
	int i;
	int j;
} Boton;

//-***************************Buy.h ****************************
typedef struct
{
	Lista textura; //static texture2d
	Vector2 positionTitulo, origenTitulo; //static
	Lista botones; //Boton
	font_t* Letra; //static
	int i,z;
	Vector2 positionInfo;
	Lista mensaje; //static//=new StringBuilder("Enjoy all the missions,\nand the full Quick Game.\nRescue all spaceships,\nwith its incredible powers,\nand Do not lose the benefit\nof overcoming each mission,\nso you can go a lot further\nin Quick Game,and get many\nmore points.");
} Buy;

//-***************************Collition.h ****************************
typedef struct CollitionSType
{
	float distancia;
	int i, j, w; //inician en 0 static
	Disparo_Tipo ultimaColisionTipoDisparo; //Static
} Collition;

//-***************************CollitionJefe.h ****************************
typedef struct
{
	float distancia;
	int i, j, w;
	Disparo_Tipo ultimaColisionTipoDisparo; //Static
	Vector2 positionUltimoDisparo; //=new Vector2();//Static
	int ultimoTipoColision; //1 da�o 0 no da�o//Static
	double X1, Y1, X1p, Y1p;
} CollitionJefe;

//-***************************Control.h ****************************
typedef struct
{
	int actualframe;
	Rectangle TrozoAnim;
	Vector2 Position, Escala, Origen;
	struct TexturaObjetoType* texControl; //Static
	Vector2 centro1, centro2, elCentro; //=new Vector2(90,700),centro2=new Vector2(110,680),elCentro;Static
	float Rotacion;
	Color color;
	int controlActivo, tamanoGrande; //Static bool
	float distancia;
	struct ToucheType* touche;
	int i;
	int regNeutra;
	float resX, resY;
	float angulo;
} Control;

//-***************************Disparo.h ****************************

typedef struct
{
	int texturaActual, radioCollition;
	Vector2 Position, Origen, Escala;
	Disparo_Tipo tipoActual;
	float Rotacion;
	Lista texDisparo; //Static
} Disparo;

//-***************************DisparoEnemigo.h ****************************
typedef enum
{
	DEdispSid, DEdispNor
} DisparoEnemigo_tipo;

typedef struct DisparoEnemigoSType
{
	Vector2 Position, Escala, Origen;
	struct CollitionSType* collition;
	int radioCollition;
	Rectangle TrozoAnim;
	Lista Texturas; //static
	DisparoEnemigo_tipo tipoActual;
	int textura;
	int disDerecha;
	Vector2 posNave;
	float x, y, Tiempo;
	int velocidadX, velocidadY;
	int adicionDanoDisNave; //static
	int Disparar, postDisparo; //bool
	int estaEnColision; //bool
} DisparoEnemigo;

//-***************************DisparoJefe.h ****************************
typedef enum
{
	DJmaximo, DJnormal
} DisparoJefe_tipo;

typedef struct
{
	Vector2 Position, Escala, Origen;
	struct CollitionSType* collition;
	int radioCollition;
	Rectangle TrozoAnim;
	Lista Texturas; //Static
	DisparoJefe_tipo tipoActual;
	int textura;
	Vector2 posNave;
	int velocidadX, velocidadY;
	int Disparar, postDisparo; //bool
	int estaEnColision; //bool
	float x, y, Tiempo;
	int adicionDanoDisNave; //Static
	int numeroCuadros;
	int actualframe;
	double dato;
	Color color;
} DisparoJefe;

//-***************************Enemigo.h ****************************
typedef enum
{
	ENor, EKami, ESid, EHiNor, EHiKami, EHiSid
} TipoEnemigo;

typedef struct
{
	Vector2 Position, Escala, Origen;
	int radioCollition, actualframe, velocidad, numeroCuadros;
	Rectangle TrozoAnim;
	/***************Valores importantes**********/
	Lista disparosNor;
	Lista disparosSid;
	Lista animaciones;
	Lista SidTotal; //Static
	Lista NorTotal; //Static
	Lista KamiTotal; //Static
	Lista HiSidTotal; //Static
	Lista HiNorTotal; //Static
	Lista HiKamiTotal; //Static

	Lista Texturas; //static
	Lista disparosSidTotal; //Sid//Static
	Lista disparosNorTotal; //Nor//Static

	//public static List<Animation> animacionesDispANaveTotal;

	/****Para colisiones de enemigos****/
	int radioCollitionNor; //Static
	int radioCollitionKami; //Static
	int radioCollitionSid; //Static
	int radioCollitionHiNor; //Static
	int radioCollitionHiKami; //Static
	int radioCollitionHiSid; //Static

	int collitionXSid[5]; //=new List<int>();//Static
	int collitionYSid[5]; //=new List<int>();//Static
	int collitionXHiSid[5]; //= new List<int>();//Static
	int collitionYHiSid[5]; //= new List<int>();//Static

	int estaEnColisionDisp; //bool
	int estaEnColisionNave; //bool
	int estaEnColisionPoder; //bool

	/*************Miembros de Enemigo**********/
	Collition colision;
	double dato;
	int vida, maxVida;
	int texturaAnim;
	int KamiPasivo; //Kami//bool
	int SidMovHorizontal, dispararSid, SidMovVertical; //Sid//bool
	Vector2 posNave;
	TipoEnemigo tipoEnemigo;
	int velocidadX, velocidadY, velocidadXini, velocidadYini;
	int auxKamiPos; //kami
	int contadorDisparosSid; //Sid
	double ultima_actua; //Nor
	int goDerecha/*Todos*/, goArriba/*Kami*/; //bool
	float x1, y1, Tiempo;
	int i, k;
	float Rotacion;
	Color colorEnemigo;
	int velocidadCambioColor, tonoAzul, tonoRojo;
	double ultimoTDisparo;
	int adicionDanoNave; //static
	int num; //=0;//static
	int movimientoLoco, cambiarMov, movAposition; //bool
	int velMinLoco, velMaxLoco, numero; //Todos inician en 0
	int locoAbajo, locoArriba, locoDerecha, locoIzquierda; //Todos inician en 0
	Vector2 positionAMoverse; //=new Vector2();

} Enemigo;

//-***************************Estrella.h ****************************
typedef struct
{
	Lista textura; //Static
	Color color;
	float escala;
	int vel;
	Vector2 Position, Origen, Escala;
} Estrella;

//-***************************GalacticaMain.h ****************************
typedef enum
{
	GMMainMenu,
	GMQuickGame,
	GMMisionMode,
	GMOptions,
	GMHelp,
	GMAbout,
	GMSavedGame,
	GMBuy
} GalacticaMain_Estado;

typedef struct
{
	GalacticaMain_Estado estadoActual; //Static
	int shutdown; //bool
	double checkTrialModeDelay; // = 1000.0;
	double introTime;
	float Wwidth, Wheight;
	int dibujaIntro,empezarCarga;
	int enCarga, enJuego;
	int isTrialMode; // = true;//static
	double cont_draw, frames, ultima_actua;
	double updates, contUpdates;
	int BESTSCORE; //Static

} GalacticaMain;

//-***************************GameComun******************************
typedef struct
{
	Lista estrellas; //Estrella static
	int e; //static
} GameComun;

//-***************************InGameBase.h ****************************
typedef struct
{
	//ALL STATIC
	double tiempoAyuda;
	Collition colision; //= new Collition();
	Lista animaciones; //= new List<Animation>();
	Lista disparosSid; // = new List<DisparoEnemigo>();
	Lista disparosNor; // = new List<DisparoEnemigo>();
	int i, j, k,z;
	int count;
	Lista texturas; //texture 2d
	Lista estrellas; //=new List<Estrella>();
	Lista enemigos; //=new List<Enemigo>();
	Lista jefes; //= new List<Jefe>();
	int maxEnemigos, aumentoEnVidaEnemigo, aumentoEnVidaJefe;
	Color colorLife;
	int velocidadEnem, velocidadEnemY, velocidadHiEnem, velocidadHiEnemY;
	int valIntercambio;
	int num, num2;
	int ultimoAumento, ultimaAdicionJefe;
	Lista animacionesQ; //= new List<Animation>();

	//Para pintar vida
	Vector2 PositionSalud; //= new Vector2(60, 14);
	Vector2 PositionVistaSalud; //= new Vector2(63, 11);
	Vector2 PositionBarraSalud; //= new Vector2(55, 3);
	BarraVida* barraVidaJefe; //= new BarraVida(new Vector2(5, 50), BarraVida.tipo.jefe);
	BarraVida* barraVidaNave; //= new BarraVida(new Vector2(55, 3), BarraVida.tipo.nave);
	int llegaA1500; //bool
	struct PlusType* plus; //= new Plus(Vector2.Zero);
	double ultimoTPlus, tiempoEsperaPlus; // ultimoTSalvado;

	//**Mensaje Inicial
	float EscalaMensaje, actualFrameMensaje, escalaInicialMensaje;
	double tiempoTitileoMensaje;
	int mensajeInicialActivo; //bool

	/*Fondo*/
	Vector2 position1; // = new Vector2(0, 0);
	Vector2 position2; // = new Vector2(0, -810);

	//Para pintar puntaje
	Vector2 PositionPuntos; //= new Vector2(220, 0);
	Vector2 PositionLabelPuntos; //= new Vector2(200, 0);
	Lista botones; //= new List<Boton>();

	//Control
	Control* control; //= new Control(Vector2.Zero);
} InGameBase;

//-***************************Jefe.h ****************************
typedef enum
{
	JLaxo, JEsp, JQuad, JRued
} Jefe_tipo;

typedef struct
{
	Vector2 Position, Escala, Origen;
	int radioCollition, actualframe, velocidad;
	Rectangle TrozoAnim;
	/***************Valores importantes**********/
	Lista animaciones;
	Lista texJefes; //Static
	Lista texPartesJefe; //Static
	Lista jefesTotales; //Static
	Lista disparos; //=new List<DisparoJefe>();
	Lista disparosMaxTotal; //static
	Lista disparosNormalTotal; //static
	int vida, maxVida, limiteLlegada;
	int tiempoEsperaDisparo, tiempoEsperaAtaque;
	int adicionDanoNave; //Static
	int i; //static

	/****** Brazos Esp *******/
	Vector2 PositionParte1; //=new Vector2();
	Vector2 OrigenParte1; //= new Vector2();
	Rectangle TrozoAnimParte1; //=new Rectangle();
	Vector2 PositionParte2; //=new Vector2();
	Vector2 OrigenParte2; //=new Vector2();
	Rectangle TrozoAnimParte2; // = new Rectangle();
	float RotacionParte1, RotacionParte2;
	Vector2 EscalaParte;

	//************Colisiones
	int radioCollitionDanoLaxo, radioCollitionLaxo; //static
	int radioCollitionEsp, radioCollitionBrazoEsp; //static
	int radioCollitionQuad, radioCollitionMartilloQuad; //static
	int radioCollitionRued, radioCollitionFlechaRued; //static
	int collitionLaxoX[3], collitionLaxoY[3]; //static
	int collitionEspX[7], collitionEspY[7], collitionBrazoEspX[18],
			collitionBrazoEspY[18]; //static
	int collitionQuadX[4], collitionQuadY[4], collitionMartilloQuadX[4],
			collitionMartilloQuadY[4]; //static
	double rotacionActual; //static
	int estaEnColisionDisp, estaEnColisionPoder; //bool

	//*************Miembros de Jefe*********
	CollitionJefe collitionJefe;
	double dato,dato2;
	int texturaAnim, lineaAnimacion;
	Jefe_tipo tipoJefe;
	float Rotacion;
	double ultimoTDisparoNormal, ultimoTDisparoMax, ultimoTDisparoNormalAux,
			ultimoTDisparoNormalAux2;
	double ultimoTParteAtaca, tiempoDetencion;
	int modoIndefenso; //bool
	int parteAtaca, detenerUnTiempo, devolverParte, parteReposo; //bool
	int numeroAleatorio;
	Color colorParte1, colorParte2;
	int velocidadCambioColor, tonoAzul, tonoVerde;
	int llegadaFinalizada; //bool
	Vector2 PositionParte3; //=new Vector2();
	Rectangle TrozoAnimParte; //=new Rectangle();
	Color colorParte3;
	int RotacionParte3;
	Vector2 OrigenParte; //=new Vector2();
	int actualframeParte, lineaAnimacionParte;
	int Avance1, Avance2, Avance3, Avance4; //bool
} Jefe;

//-***************************MainMenu.h ****************************
typedef struct
{
	Lista textura; //static
	Vector2 positionTitulo, origenTitulo;
	Lista botones;
	int playMusic; //static bool
	int i;
} MainMenu;

//-***************************Mision1.h******************************
typedef struct
{
	//All static
	char* tituloMision; //=new StringBuilder("Mission 1");
	Lista mensajeMision; // = new StringBuilder("Destroy the greater\namount of enemies and\nsurpass the required\nscore before the \ntime finish.\nTip:Destroy the enemies\nas fast as possible.");
	Lista recompensa; // = new StringBuilder("Maximum energy increased\nby 50, in Quick Game.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	Lista  yaRecompensa; // = new StringBuilder("You already have it.");
	int puntosAVencer, cuentaTiempo; //puntosAVencer = 1400,cuentaTiempo = 120
	Vector2 positionToBeat, positionTime;
	double ultimoTMinuto;
	int i, j;

} Mision1;

//-***************************Mision10.h ****************************
typedef struct
{
	//All static
	char* tituloMision; //= new StringBuilder("Mission 10");
	Lista mensajeMision; //= new StringBuilder("Destroy the super\nmetallic enemy, shooting\nin color changing zones.\nTip: Use the power,\nwill be very effective.");
	Lista recompensa; //= new StringBuilder("Rescued the spaceship,\nHelix Use it in \nQick Game.");
	Lista  noRecompensa; //= new StringBuilder("No Reward...");
	Jefe* elJefe; //= new Jefe(Jefe.tipo.Esp, 240, -800);
	int i, j;
} Mision10;

//-***************************Mision11.h ****************************
typedef struct
{
	char* tituloMision; // = new StringBuilder("Mission 11");
	Lista mensajeMision; // = new StringBuilder("Eliminate only red color\nenemies, if you fail\nyou will lose.\nTip:If no red enemies,\nwait some time,\nthey will appear.");
	Lista recompensa; // = new StringBuilder("Recharge, the energy in\n100, to catch the star\n in Quick Game.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	int destruidos; //=0;
	Vector2 positionToBeat, positionTime;
	double ultimoTEliminar; // = 0;
	int i, j;
} Mision11;

//-***************************Mision12.h ****************************
typedef struct
{
	//All static
	char* tituloMision; // = new StringBuilder("Mission 12");
	Lista mensajeMision; // = new StringBuilder("Protects the small\nspaceship of enemies,\nand avoid that them \ndestroy it, until the \ntime finish.\nTip:Do not let the\nenemy get too close.");
	Lista recompensa; // = new StringBuilder("Maximum energy increased\nby 200, in Quick Game.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	int cuentaTiempo; //=90;
	Vector2 positionMiniS, positionTime;
	double ultimoTMinuto;
	struct NMiniType* mini; // = new NMini();
	BarraVida *barraVidaMini; // = new BarraVida(new Vector2(6, 50), BarraVida.tipo.mini);
	int i, j;
} Mision12;

//-***************************Mision13.h ****************************
typedef struct
{
	char* tituloMision; // = new StringBuilder("Mission 13");
	Lista mensajeMision; // = new StringBuilder("Escape most fast as\npossible of the expansive\nwave. avoid collide or \nyou will lose.\nTip:Keep moving.");
	Lista recompensa; // = new StringBuilder("Decreases power timeout.\nin Quick Game.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	int cuentaTiempo, cuentaGolpes; //cuentaTiempo=100,cuentaGolpes=3;
	Vector2 positionTime;
	double ultimoTMinuto;
	struct OndaType* onda; //=new Onda();
	int i;
} Mision13;

//-***************************Mision14.h ****************************
typedef struct
{
//All static
	char* tituloMision; // = new StringBuilder("Mission 14");
	Lista mensajeMision; // = new StringBuilder("You will not be able\nto shoot. Dodge the \nmost amount of shots, \nto avoid them destroy \nyou, until the time\nfinish.\nTip:Keep moving.");
	Lista recompensa; // = new StringBuilder("Recharge, the energy in\n300, to catch the star\n in Quick Game.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	int cuentaTiempo; //=120;
	Vector2 positionTime;
	double ultimoTMinuto;
	int i;
} Mision14;

//-***************************Mision15.h ****************************
typedef struct
{
//All static
	char* tituloMision; // = new StringBuilder("Mission 15");
	Lista mensajeMision; // = new StringBuilder("Destroy the super\nmetallic enemy, shooting\nin color changing zones.\nTip: Use the power,\nwill be very effective.");
	Lista recompensa; // = new StringBuilder("Rescued the spaceship,\nArp Use it in Qick Game.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	Jefe* elJefe; // = new Jefe(Jefe.tipo.Quad, 240, -800);
	int i, j;
} Mision15;

//-***************************Mision16.h ****************************
typedef struct
{
//All static
	char* tituloMision; // = new StringBuilder("Mission 16");
	Lista mensajeMision; // = new StringBuilder("Destroy the greater\namount of enemies and\nsurpass the required\nscore before the \ntime finish.\nTip:Destroy the enemies\nas fast as possible.");
	Lista recompensa; // = new StringBuilder("Maximum energy increased\nby 200, in Quick Game.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	int puntosAVencer, cuentaTiempo; //puntosAVencer = 5400, cuentaTiempo = 120;
	Vector2 positionToBeat, positionTime;
	double ultimoTMinuto;
	int i;
} Mision16;

//-***************************Mision17.h ****************************
typedef struct
{
	//all static
	char* tituloMision; // = new StringBuilder("Mission 17");
	Lista mensajeMision; // = new StringBuilder("Look attentively which\nof the enemies hide\nthe star (plus) and\ndestroy him, dont fail\nmore than three times\nor you will lose.\nTip:Keep your eye on\nthe target.");
	Lista recompensa; // = new StringBuilder("Recharge, the energy in\n500, to catch the star\n in Quick Game.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	Vector2 positionEnemigoS, positionTime, positionToBeat, positionAyuda1,
			positionAyuda2;
	Lista elEnemigo; // = new List<Enemigo>();
	struct PlusType* estrella; //=new Plus(Vector2.Zero);
	int escogido, enemSeleccionado, aciertos; //escogido = 0, enemSeleccionado = 20, aciertos = 0;
	int periodoMuestra, periodoEscoger; //bool
	double tiempoEsperaMuestra, tiempoMovLoco; //tiempoEsperaMuestra = 0, tiempoMovLoco = 0;
	int i, j;
} Mision17;

//-***************************Mision18.h ****************************
typedef struct
{
	//all static
	char* tituloMision; // = new StringBuilder("Mission 18");
	Lista mensajeMision; // = new StringBuilder("Eliminate only red color\nenemies, if you fail\nyou will lose.\nTip:If no red enemies,\nwait some time,\nthey will appear.");
	Lista recompensa; // = new StringBuilder("Increases the shooting\npower,in Quick Game.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	int destruidos; // = 0;
	Vector2 positionToBeat, positionTime;
	double ultimoTEliminar; // = 0;
	int i, j;
} Mision18;

//-***************************Mision19.h ****************************
typedef struct
{
//all static
	char* tituloMision; // = new StringBuilder("Mission 19");
	Lista mensajeMision; // = new StringBuilder("Shoot to the enemy all\nthe time, dont let to\ndo it or he will reload\nall his energy and\nyou will lose.\nTip:Shoot as fast as\nyou can.");
	Lista recompensa; // = new StringBuilder("Decreases power timeout.\nin Quick Game.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	int cuentaTiempo; // = 90;
	Vector2 positionToBeat;
	double ultimoTMinuto;
	Vector2 positionEnemigoS, positionTime;
	Enemigo* elEnemigo; //= new Enemigo(Enemigo.tipo.Nor,200,200,0,0);
	BarraVida* barraVidaEnemigo; // = new BarraVida(new Vector2(6, 50), BarraVida.tipo.mini);
	int i, j;
} Mision19;

//-***************************Mision2.h ****************************
typedef struct
{
	//All static
	char* tituloMision; // = new StringBuilder("Mission 2");
	Lista mensajeMision; // = new StringBuilder("You will not be able\nto shoot. Dodge the \nmost amount of shots, \nto avoid them destroy \nyou, until the time\nfinish.\nTip:Keep moving.");
	Lista recompensa; // = new StringBuilder("Double diagonal shot, \nto take the plus(star)\nin Quick Game.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	int cuentaTiempo; //=90;
	Vector2 positionTime;
	double ultimoTMinuto;
	int i, j;
} Mision2;

//-***************************Mision20.h ****************************
typedef struct
{
//all static
	char* tituloMision; // = new StringBuilder("Mission 20");
	Lista mensajeMision; // = new StringBuilder("Destroy the super\nmetallic enemy, shooting\nin color changing zones.\nTip: Use the power,\nwill be very effective.");
	Lista recompensa; // = new StringBuilder("Rescued the spaceships,\nPerseus and Pisces,\nUse it in Qick Game.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	Jefe* elJefe; // = new Jefe(Jefe.tipo.Rued, 240, -800);
	int i, j;
} Mision20;

//-***************************Mision3.h ****************************
typedef struct
{
	//All static
	char* tituloMision; //= new StringBuilder("Mission 3");
	Lista mensajeMision; // = new StringBuilder("Protects the small\nspaceship of enemies,\nand avoid that them \ndestroy it, until the \ntime finish.\nTip:Do not let the\nenemy get too close.");
	Lista recompensa; // = new StringBuilder("Decreases the waiting\ntime to activate the\npower,in Quick Game.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	int cuentaTiempo; //=60;
	Vector2 positionMiniS, positionTime;
	double ultimoTMinuto;
	struct NMiniType* mini; //= new NMini();
	BarraVida* barraVidaMini; //=new BarraVida(new Vector2(6,50),BarraVida.tipo.mini);
	int i, j;
} Mision3;

//-***************************Mision4.h ****************************
typedef struct
{
	//All sttatic
	char* tituloMision; //= new StringBuilder("Mission 4");
	Lista mensajeMision; // = new StringBuilder("Escape most fast as\npossible of the expansive\nwave. avoid collide or \nyou will lose.\nTip:Keep moving.");
	Lista recompensa; // = new StringBuilder("Maximum energy increased\nby 100, in QuickGame.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	int cuentaTiempo, cuentaGolpes; //cuentaTiempo=90,cuentaGolpes=3;
	Vector2 positionTime;
	double ultimoTMinuto;
	struct OndaType* onda; //=new Onda();
	int i, j;
} Mision4;

//-***************************Mision5.h ****************************
typedef struct
{
	//All static
	char* tituloMision; //= new StringBuilder("Mission 5");
	Lista mensajeMision; // = new StringBuilder("Destroy the super\nmetallic enemy, shooting\nin color changing zones.\nTip: Use the power,\nwill be very effective.");
	Lista recompensa; // = new StringBuilder("Rescued the spaceship,\nCarafe Use it in \nQick Game.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	Jefe* elJefe; // = new Jefe(Jefe.tipo.Laxo, 240, -800);
	int i, j;
} Mision5;

//-***************************Mision6.h ****************************
typedef struct
{
//all static
	char* tituloMision; // = new StringBuilder("Mission 6");
	Lista mensajeMision; // = new StringBuilder("You will not be able\nto shoot. Dodge the \nmost amount of shots, \nto avoid them destroy \nyou, until the time\nfinish.\nTip:Keep moving.");
	Lista recompensa; // = new StringBuilder("Super shot to take the \nplus(star) in Quick Game.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	int cuentaTiempo; //=90;
	Vector2 positionTime;
	double ultimoTMinuto;
	int i, j;
} Mision6;

//-***************************Mision7.h ****************************
typedef struct
{
//All static
	char* tituloMision; //= new StringBuilder("Mission 7");
	Lista mensajeMision; // = new StringBuilder("Shoot to the enemy all\nthe time, dont let to\ndo it or he will reload\nall his energy and\nyou will lose.\nTip:Shoot as fast as\nyou can.");
	Lista recompensa; // = new StringBuilder("Maximum energy increased\nby 50, in Quick Game.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	int cuentaTiempo; //=60;
	Vector2 positionToBeat;
	double ultimoTMinuto;
	Vector2 positionEnemigoS, positionTime;
	Enemigo* elEnemigo; //= new Enemigo(Enemigo.tipo.Nor,200,200,0,0);
	BarraVida* barraVidaEnemigo; // = new BarraVida(new Vector2(6, 50), BarraVida.tipo.mini);
	int i, j;
} Mision7;

//-***************************Mision8.h ****************************
typedef struct
{
	//all static
	char* tituloMision; // = new StringBuilder("Mission 8");
	Lista mensajeMision; // = new StringBuilder("Look attentively which\nof the enemies hide\nthe star (plus) and\ndestroy him, dont fail\nmore than three times\nor you will lose.\nTip:Keep your eye on\nthe target.");
	Lista recompensa; // = new StringBuilder("Increases the shooting\npower,in Quick Game.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	Vector2 positionEnemigoS, positionTime, positionToBeat, positionAyuda1,
			positionAyuda2;
	Lista elEnemigo; //= new List<Enemigo>();
	//static new Random random=new Random();
	struct PlusType* estrella; //=new Plus(Vector2.Zero);
	int escogido, enemSeleccionado, aciertos; //escogido=0,enemSeleccionado=20,aciertos=0;
	int periodoMuestra, periodoEscoger;
	double tiempoEsperaMuestra, tiempoMovLoco; //tiempoEsperaMuestra=0, tiempoMovLoco=0;
	int i, j;
} Mision8;

//-***************************Mision9.h ****************************
typedef struct
{
	//All static
	char* tituloMision; //= new StringBuilder("Mission 9");
	Lista mensajeMision; // = new StringBuilder("Destroy the greater\namount of enemies and\nsurpass the required\nscore before the \ntime finish.\nTip:Destroy the enemies\nas fast as possible.");
	Lista recompensa; // = new StringBuilder("Maximum energy increased\nby 100, in Quick Game.");
	Lista  noRecompensa; // = new StringBuilder("No Reward...");
	int puntosAVencer, cuentaTiempo; //puntosAVencer = 3100,cuentaTiempo=120;
	Vector2 positionToBeat, positionTime;
	double ultimoTMinuto;
	int i, j;
} Mision9;

//-***************************MisionMode.h ****************************
typedef enum
{
	MMGame, MMPause, MMGameOver, MMMenu, MMInfo
} MisionMode_Estado;

typedef struct
{
	MisionMode_Estado estadoActual;

} MisionMode;

//-***************************MisionModeGame.h ****************************
typedef struct
{
	int numMision; //static
} MisionModeGame;

//-***************************MisionModeGOver.h ****************************
typedef struct
{
	Lista textura; //static Texture2D
	font_t* fuente; //static
	Vector2 positionTitulo, origenTitulo, positionPuntos, positionDestruidos,
			positionResultados;
	Lista botones; //Boton
	int texturaTitulo;
	int gana; //static bool
	int i,z;
	double tEsperaActivaBoton; // = 1200.0;//static
	Lista* cosasGanadas; // = new StringBuilder();//static
} MisionModeGOver;

//-***************************MisionModeInfo.h ****************************
typedef struct
{
	Lista textura; //static Texture2D
	font_t* Letra; //static
	Vector2 positionTitulo, origenTitulo, positionImagen;
	Vector2 positionSubT, origenSubT, positionInfo, positionTMision;
	Lista botones; //boton
	char* TituloMision; // = new StringBuilder();//static
	Lista* InfoMision; //=new StringBuilder();//static
	int i,z;
} MisionModeInfo;

//-***************************MisionModeMenu.h ****************************
typedef struct
{
	Lista textura; //static
	font_t* Letra; //static
	Lista numeroMision; //static int
	Lista estaActivo; //static bool
	Lista idMision; //static vector2
	Vector2 positionTitulo, origenTitulo;
	Lista botones; //static boton
	int i, j, posx, posy, cont;
} MisionModeMenu;

//-***************************MisionModePause.h ****************************
typedef struct
{
	Lista textura; //static texture2d
	Vector2 positionTitulo, origenTitulo;
	Lista botones; //boton
	int i;
} MisionModePause;

//-***************************Nave.h ****************************
typedef struct
{
	Lista texNaves; //static
	float Rotacion;
	int actualframe;
	Rectangle TrozoAnim;
	Vector2 Origen;
	Vector2 auxPosition; //=new Vector2();//static
	Vector2 PositionNav; //=new Vector2();//static
	//All static
	Lista disparosTotales;
	Lista disparosDiagIzquierdaTotales;
	Lista disparosDiagDerechaTotales;
	Lista disparosSuperTotales;
	Lista disparos;
	int collitionX[11]; //=new List<int>();
	int collitionY[11]; //=new List<int>();
	int radioCollitionN;
	double dato;
	struct ToucheType* touched;
	int TexturaNave; //Static
	int MaxVida, Destruidos;
	float Vida;
	int Puntos;
	int disparosActivos; //bool
	int poderActivo;
	Lista poderGalacticaTotal, poderGalactica, poderCarafeMedioTotal;
	Lista poderCarafeIzquierdaTotal, poderCarafeDerechaTotal, poderCarafe;
	Lista poderHelixTotal, poderHelix, poderArpTotal, poderArp;
	Lista poderPerseusTotal, poderPerseus, poderPiscesTotal, poderPisces;
	double ultimaActualizacion, ultimoTPoder;
	int contadorPoderGalactica, contadorPoderHelix, contadorPoderArp;
	int contadorPoderCarafe, contadorPoderPerseus, contadorPoderPisces;
	struct AnimaExtra *animacionPoderGalactica, *animacionPoderCarafe,
			*animacionPoderHelix;
	struct AnimaExtra *animacionPoderArp, *animacionPoderPerseus,
			*animacionPoderPisces;
	int contadorToques; //No static
	int permiteColisionesJefe; //bool
	double tiempoAyuda, tEsperaPGalactica;
	double tiempoAyudaPorColision;
	Vector2 EscalaN;
	Vector2 escalaInicial;
	Vector2 actualFrameAyuda;
	int i, j;
	int disparosDiagActivos, disparoSuperActivo; //bool
	int velocidadNave;
	int movEnY, estallaActivo; //bool
	int limiteShot, masPoderDisp;
	int sePuedeDispDiag, sePuedeDispSuper; //bool
} Nave;

//-***************************NMini.h ****************************
typedef struct NMiniType
{
	Vector2 Position, Escala, Origen;
	int radioCollition;
	Rectangle TrozoAnim;
	Lista texDisparo; //static
	int texturaActual;
	int num;
	double dato;
	int actualframe;
	float Rotacion;
	int vida, maxVida; //igualadas a 80
	int velocidadX, velocidadY;
	int cambiarMov; //bool
	int collitionX[11]; // = new List<int>();//static
	int collitionY[11]; // = new List<int>();//static
} NMini;

//-***************************Onda.h ****************************
typedef struct OndaType
{
	Vector2 Position, Escala, Origen;
	Lista texDisparo; //static
	Rectangle TrozoAnim;
	double dato;
	int actualframe;
	int texturaActual;
	float Rotacion;
} Onda;

//-***************************Opciones.h ****************************
typedef struct
{
	Lista textura; //Static - Texture2D
	Vector2 positionTitulo, origenTitulo; //Static
	Lista botones; //Boton
	int i;
} Opciones;

//-***************************Plus.h ****************************
typedef enum
{
	PLvida, PLdisparo
} PlusTipo;

typedef struct PlusType
{
	Vector2 Position, Escala, Origen;
	int radioCollition;
	Rectangle TrozoAnim;
	Lista texPlus; //Estatico
	int texturaActual;
	PlusTipo tipoPlus;
	float Rotacion;
	Collition colision;
	int plusActivo; //bool
	int muestra; //bool
	int i;
	int recuperacion; //Estatico
} Plus;

//-***************************Poderes.h ****************************
typedef enum
{
	PGalactica, PCarafe, PHelix, PArp, PPerseus, PPisces
} Poderes_tipo;
typedef enum
{
	PCMedio, PCDerecha, PCIzquierda, PCNull
} Poderes_tipoPoderCarafe;

typedef struct
{
	Vector2 Position, Escala, Origen;
	int radioCollition, actualframe, velocidad, numeroCuadros;
	Rectangle TrozoAnim;
	Lista texPoderes; //static
	Poderes_tipoPoderCarafe tipoPoderCarafeActual;
	int texturaActual;
	Poderes_tipo tipoActual;
	int valAuxCarafe; //=10;
	int collitionPArpX[6], collitionPArpY[6]; //static
	int radioCollitPGalactica, radioCollitPCarafe, radioCollitPHelix; //static
	int radioCollitPArp, radioCollitPPerseus, radioCollitPPisces; //static
	int velocidadRandX, velocidadRandY, direccionPoder;
	float Rotacion;
} Poderes;

//-***************************QuickMode.h ****************************
typedef enum
{
	QMGame, QMPause, QMGameOver, QMMenu
} QuickMode_Estado;

typedef struct
{
	QuickMode_Estado estadoActual;
} QuickMode;

//-***************************QuickModeGame.h ****************************
typedef struct
{
	//ALL STATIC
	//Inicializar lo que hay en in game base
	InGameBase igb;
} QuickModeGame;

//-***************************QuickModeGOver.h ****************************
typedef struct
{
	Lista textura; //Static
	font_t* fuente; //Static
	Vector2 positionTitulo, origenTitulo, positionPuntos, positionDestruidos,
			positionResultados;
	Lista botones;
	double tEsperaActivaBoton; //=1200.0;//Static
	int i;
} QuickModeGOver;

//-***************************QuickModeMenu.h ****************************
typedef struct
{
	Lista textura; //Static
	Lista texturaNaves; //Static
	Lista nombresNave; //Static
	font_t* Letra; //Static
	Vector2 positionTitulo, origenTitulo;
	Vector2 positionSubT, origenSubT;
	Vector2 positionNave, origenNave;
	Vector2 positionNavesRescatadas, positionNombreNave;
	Lista botones;
	Rectangle TrozoAnim;
	double dato;
	int actualframe;
	int texturaNaveActual, rescatadas; //Static
	int i;
} QuickModeMenu;

//-***************************QuickModePause.h ****************************
typedef struct
{
	Lista textura; //textura2d
	Vector2 positionTitulo, origenTitulo;
	Lista botones; //boton
	int i;
} QuickModePause;

//-***************************SavedGame.h ****************************
typedef struct
{
	Lista textura; //Static - Texture2D
	Vector2 positionTitulo, origenTitulo;
	Lista botones; //Boton
	int i;
} SavedGame;

//-***************************Textura.h ****************************
typedef struct
{
	Lista Texturas; //Static - Texture2D
	Lista TexturasNom; //Static -String
	Lista TexturasFondo; //Static -Texture2D
	font_t* Fuente; //Static
	struct TexturaObjetoType *intro;
	int i;
} Textura;

typedef struct TexturaObjetoType
{
	int ancho, alto;
	float texx, texy;
	float texCoord[8], cuadCoord[8];
	int setOrigen;
	GLuint textura;
} TexturaObjeto;

//-***************************TouchControl.h ****************************
typedef enum
{
	Pressed, Moved, Released, Nada, libre
} touchEvento;

typedef struct
{
	Lista touches;
	Lista touchesLibres;
	int i;
} TouchControl;

typedef struct ToucheType
{
	Vector2 position;
	touchEvento estado;
	int identifi;
} Touche;

#endif /* GALACTICATIPOS_H_ */
