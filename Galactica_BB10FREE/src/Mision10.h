/*
 * Mision10.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION10_H_
#define MISION10_H_

#include "GalacticaTipos.h"

void Mision10_reiniciar();
void Mision10_load();
void Mision10_update();
void Mision10_draw();
void Mision10_drawEstadNave();
void Mision10_gameOver();
void Mision10_updateJefes();
void Mision10_drawJefes();
void Mision10_drawMensajeInicial();
void Mision10_reiniciaJefe();
Mision10* Mision10_getSObject();

#endif /* MISION10_H_ */
