/*
 * Mision8.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MISION8_H_
#define MISION8_H_

#include "GalacticaTipos.h"

void Mision8_reiniciar();
void Mision8_load();
void Mision8_update();
void Mision8_draw();
void Mision8_drawEstadNave();
void Mision8_updateGame();
void Mision8_gameOver();
void Mision8_drawMensajeInicial();
Mision8* Mision8_getSObject();

#endif /* MISION8_H_ */
