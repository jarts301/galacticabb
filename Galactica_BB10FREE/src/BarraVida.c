/*
 * BarraVida.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */

#include "BarraVida.h"
#include "GameFramework.h"
#include "Textura.h"
#include "Lista.h"
#include "Dibujar.h"
#include <stdlib.h>

static BarraVida BarraVidaS;

BarraVida* BarraVida_Crear(Vector2 Pos, BarraVida_tipo tipoBarra)
{
	BarraVida* nuevo;
	if ((nuevo = (BarraVida*) malloc(sizeof(BarraVida))) == NULL)
		return NULL;

	(*nuevo).Position.X = Pos.X;
	(*nuevo).Position.Y = Pos.Y;
	(*nuevo).Tinte.R = GameFramework_getByteColorToFloatColor(207);
	(*nuevo).Tinte.G = GameFramework_getByteColorToFloatColor(232);
	(*nuevo).Tinte.B = GameFramework_getByteColorToFloatColor(157);
	(*nuevo).Tinte.A = GameFramework_getByteColorToFloatColor(255);
	(*nuevo).Rotacion = 0;
	(*nuevo).factorEscala = 0;
	BarraVida_setOrigen(nuevo);
	if (tipoBarra == BVjefe)
	{
		(*nuevo).escalaInicialX = 4.6f;
		(*nuevo).escalaInicialY = 0.5f;
	}
	if (tipoBarra == BVnave)
	{
		(*nuevo).escalaInicialX = 1.42f;
		(*nuevo).escalaInicialY = 1.3f;
	}
	if (tipoBarra == BVmini)
	{
		(*nuevo).escalaInicialX = 1.2f;
		(*nuevo).escalaInicialY = 1.1f;
	}

	(*nuevo).escala.X = (*nuevo).escalaInicialX;
	(*nuevo).escala.Y = (*nuevo).escalaInicialY;
	(*nuevo).escalaMarco.X = (*nuevo).escalaInicialX;
	(*nuevo).escalaMarco.Y = (*nuevo).escalaInicialY;

	return nuevo;
}

void BarraVida_load()
{
	BarraVidaS.texturaBarra = Textura_getTextura("BarraVida");
	BarraVidaS.texturaMarco = Textura_getTextura("BarraVidaMarco");
}

void BarraVida_update(BarraVida* barraVida, float vida, float maxVida)
{
	if (vida > ((maxVida * 3) / 8))
	{
		(*barraVida).Tinte.R = GameFramework_getByteColorToFloatColor(90);
		(*barraVida).Tinte.G = GameFramework_getByteColorToFloatColor(217);
		(*barraVida).Tinte.B = GameFramework_getByteColorToFloatColor(58);
		(*barraVida).Tinte.A = GameFramework_getByteColorToFloatColor(255);
	}
	if (vida <= ((maxVida * 3) / 8))
	{
		(*barraVida).Tinte.R = GameFramework_getByteColorToFloatColor(255);
		(*barraVida).Tinte.G = GameFramework_getByteColorToFloatColor(210);
		(*barraVida).Tinte.B = GameFramework_getByteColorToFloatColor(31);
		(*barraVida).Tinte.A = GameFramework_getByteColorToFloatColor(255);
	}
	if (vida <= ((maxVida * 1) / 8))
	{
		(*barraVida).Tinte.R = GameFramework_getByteColorToFloatColor(255);
		(*barraVida).Tinte.G = GameFramework_getByteColorToFloatColor(10);
		(*barraVida).Tinte.B = GameFramework_getByteColorToFloatColor(10);
		(*barraVida).Tinte.A = GameFramework_getByteColorToFloatColor(255);
	}
	if (maxVida != 0)
	{
		(*barraVida).factorEscala = vida / maxVida;
		(*barraVida).escala.X = (*barraVida).factorEscala
				* (*barraVida).escalaInicialX;
	}
}

void BarraVida_draw(BarraVida* barraVida)
{
	Dibujar_drawSprite(BarraVidaS.texturaBarra, (*barraVida).Position,
			(*barraVida).Tinte, (*barraVida).Rotacion, (*barraVida).Origen,
			(*barraVida).escala);
	Dibujar_drawSprite(BarraVidaS.texturaMarco, (*barraVida).Position,
			GameFramework_Color(1, 1, 1, 1), (*barraVida).Rotacion,
			(*barraVida).Origen, (*barraVida).escalaMarco);
}
//***********************************************************
void BarraVida_reiniciaColor(BarraVida* barraVida)
{
	//Verde Claro
	(*barraVida).Tinte.R = GameFramework_getByteColorToFloatColor(90);
	(*barraVida).Tinte.G = GameFramework_getByteColorToFloatColor(217);
	(*barraVida).Tinte.B = GameFramework_getByteColorToFloatColor(58);
	(*barraVida).Tinte.A = GameFramework_getByteColorToFloatColor(255);
}

void BarraVida_setOrigen(BarraVida* barraVida)
{
	(*barraVida).Origen.X = 0;
	(*barraVida).Origen.Y = 0;
}

void BarraVida_setPosition(BarraVida* barraVida,Vector2 pos)
{
	(*barraVida).Position.X = pos.X;
	(*barraVida).Position.Y = pos.Y;
}

