/*
 * Mision15.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "Mision15.h"
#include "QuickModeGame.h"
#include "Animation.h"
#include "Jefe.h"
#include "BarraVida.h"
#include "Boton.h"
#include "Nave.h"
#include "Lista.h"
#include "Control.h"
#include "Textura.h"
#include "Dibujar.h"
#include "Audio.h"
#include "Mision1.h"
#include "MisionMode.h"
#include "CollitionJefe.h"
#include "Almacenamiento.h"
#include "MisionModeGOver.h"
#include "Cadena.h"

static Mision15 Mision15S;

void Mision15_reiniciar()
{
	QuickModeGame_eliminarTodos();
	QuickModeGame_eliminaAnimaciones();
	Lista_Destruir(&((*QuickModeGame_getSObject()).igb.botones));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BPause, 5, 950));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BAudio, 530, 950));
	Lista_InsertaEnFinal(&((*QuickModeGame_getSObject()).igb.botones),
			(*QuickModeGame_getSObject()).igb.botones.fin,
			Boton_Crear(BPower, 480, 20));
	(*QuickModeGame_getSObject()).igb.velocidadEnem = 2;
	(*QuickModeGame_getSObject()).igb.velocidadEnemY = 2;
	(*QuickModeGame_getSObject()).igb.velocidadHiEnem = 3;
	(*QuickModeGame_getSObject()).igb.velocidadHiEnemY = 3;
	(*QuickModeGame_getSObject()).igb.maxEnemigos = 5;
	(*QuickModeGame_getSObject()).igb.aumentoEnVidaEnemigo = 0;
	(*QuickModeGame_getSObject()).igb.ultimoAumento = 0;
	Jefe_reinicia((Mision15S.elJefe), 300, 2048, 0);
	(*QuickModeGame_getSObject()).igb.EscalaMensaje =
			(*QuickModeGame_getSObject()).igb.escalaInicialMensaje = 1.5f;
	(*QuickModeGame_getSObject()).igb.actualFrameMensaje = 0;
	(*QuickModeGame_getSObject()).igb.tiempoTitileoMensaje = 0;
	(*QuickModeGame_getSObject()).igb.mensajeInicialActivo = 1;
	Mision15_reiniciaJefe();
	(*Nave_getSObject()).disparoSuperActivo = 1;
	(*(*Animation_getSObject()).animacionEstallidoJefe).animacionFinalizada = 0;
}

void Mision15_load()
{
	Mision15S.tituloMision = "Mission 15";

	Lista_Inicia(&Mision15S.mensajeMision);
	Lista_AddInFin(&Mision15S.mensajeMision,
			String_creaString("Destroy the super"));
	Lista_AddInFin(&Mision15S.mensajeMision,
			String_creaString("metallic enemy, shooting"));
	Lista_AddInFin(&Mision15S.mensajeMision,
			String_creaString("in color changing zones."));
	Lista_AddInFin(&Mision15S.mensajeMision,
			String_creaString("Tip: Use the power,"));
	Lista_AddInFin(&Mision15S.mensajeMision,
			String_creaString("will be very effective."));

	Lista_Inicia(&Mision15S.recompensa);
	Lista_AddInFin(&Mision15S.recompensa,
			String_creaString("Rescued the spaceship,"));
	Lista_AddInFin(&Mision15S.recompensa,
			String_creaString("Arp Use it in Qick Game."));

	Lista_Inicia(&Mision15S.noRecompensa);
	Lista_AddInFin(&Mision15S.noRecompensa, String_creaString("No Reward.."));

	Mision15S.elJefe = Jefe_crearObject(JQuad, 300, 2048);
}

void Mision15_update()
{
	Nave_update();
	QuickModeGame_updateEstrellas();
	Mision15_updateJefes();
	QuickModeGame_updateAnimaciones();
	for (Mision15S.i = 0;
			Mision15S.i < (*QuickModeGame_getSObject()).igb.botones.size;
			Mision15S.i++)
	{
		Boton_update(
				(Boton*) Lista_GetIn(
						&((*QuickModeGame_getSObject()).igb.botones),
						Mision15S.i));
	}
	QuickModeGame_updateMensajeInicial();
	BarraVida_update(((*QuickModeGame_getSObject()).igb.barraVidaNave),
			(*Nave_getSObject()).Vida, (*Nave_getSObject()).MaxVida);
	if ((*Nave_getSObject()).Vida <= 0)
	{
		Animation_update((*Animation_getSObject()).animacionesEstallidoNave);
	}
	if ((*Mision15S.elJefe).vida <= 0)
	{
		Animation_update((*Animation_getSObject()).animacionEstallidoJefe);
	}
	Mision15_gameOver();
	if ((*Control_getSObject()).controlActivo == 1)
	{
		Control_update((*QuickModeGame_getSObject()).igb.control);
	}
}

void Mision15_draw()
{
	//CLEAR NEGRO
	Dibujar_drawSprite(Textura_getTexturaFondo(0), GameFramework_Vector2(0, 0),
			GameFramework_Color(0, 0, 0, 1), 0, GameFramework_Vector2(0, 0),
			GameFramework_Vector2(40, 40));

	Nave_draw();
	QuickModeGame_drawEstrellas();
	Mision15_drawJefes();
	Mision15_drawEstadNave();
	QuickModeGame_drawAnimaciones();
	Mision15_drawMensajeInicial();
	if ((*Nave_getSObject()).Vida <= 0)
	{
		Animation_draw((*Animation_getSObject()).animacionesEstallidoNave);
	}
	if ((*Mision15S.elJefe).vida <= 0)
	{
		Animation_draw((*Animation_getSObject()).animacionEstallidoJefe);
	}

	for (Mision15S.i = 0;
			Mision15S.i < (*QuickModeGame_getSObject()).igb.botones.size;
			Mision15S.i++)
	{
		Boton_draw(
				Lista_GetIn(&(*QuickModeGame_getSObject()).igb.botones,
						Mision15S.i));
	}
	if ((*Control_getSObject()).controlActivo == 1)
	{
		Control_draw((*QuickModeGame_getSObject()).igb.control);
	}
	//drawEstadisticas(sb);
}

//******************Estadisticas de la nave*************
void Mision15_drawEstadNave()
{
	//** Vida de la nave
	Dibujar_drawSprite(Textura_getTexturaFondo(1),
			GameFramework_Vector2(0, 945), GameFramework_Color(0, 0, 0, 1), 0,
			GameFramework_Vector2(0, 0), GameFramework_Vector2(50, 2.6));

	BarraVida_draw((*QuickModeGame_getSObject()).igb.barraVidaNave);

	Dibujar_drawText((*Textura_getSObject()).Fuente,
			GameFramework_EnteroACadena((*Nave_getSObject()).Vida),
			GameFramework_Vector2(100, 975), GameFramework_Color(1, 1, 1, 1),
			0.8);

	Dibujar_drawText((*Textura_getSObject()).Fuente, "/",
			GameFramework_Vector2(285, 965), GameFramework_Color(1, 1, 1, 1),
			1.4);

	Nave_drawAnimacionesPoder();
}

void Mision15_gameOver()
{
	if ((*Mision15S.elJefe).vida <= 0)
	{
		Mision15_reiniciaJefe();
		(*Mision15S.elJefe).vida = 0;
		(*Mision15S.elJefe).Escala = GameFramework_Vector2(0, 0);
		Animation_setPosition((*Animation_getSObject()).animacionEstallidoJefe,
				(*Mision15S.elJefe).Position);
		(*MisionModeGOver_getSObject()).gana = 1;

		if ((*(*Animation_getSObject()).animacionEstallidoJefe).animacionFinalizada
				== 1)
		{
			Audio_playAudio("EstallidoJefe");
			if ((*Audio_getSObject()).musicaActiva == 1)
			{
				Audio_stopAudio("Galactic");
				Audio_playAudio("GameOver");
			}

			Almacenamiento_cargarMisiones();
			if (Almacenamiento_getPremiosDados() == 14)
			{
				Almacenamiento_salvarBonus(4, ALnaves);
				(*MisionModeGOver_getSObject()).cosasGanadas =
						&Mision15S.recompensa;
				Almacenamiento_salvarMisiones();
			}
			else
			{
				(*MisionModeGOver_getSObject()).cosasGanadas =
						&(*Mision1_getSObject()).yaRecompensa;
			}

			(*MisionMode_getSObject()).estadoActual = MMGameOver;
		}
	}

	if ((*Nave_getSObject()).Vida <= 0)
	{
		(*Nave_getSObject()).Vida = 0;
		(*Nave_getSObject()).EscalaN = GameFramework_Vector2(0, 0);
		Animation_setPosition(
				(*Animation_getSObject()).animacionesEstallidoNave,
				(*Nave_getSObject()).PositionNav);
		(*MisionModeGOver_getSObject()).gana = 0;
		(*MisionModeGOver_getSObject()).cosasGanadas = &Mision15S.noRecompensa;
		if ((*(*Animation_getSObject()).animacionesEstallidoNave).animacionFinalizada
				== 1)
		{
			if ((*Audio_getSObject()).musicaActiva == 1)
			{
				Audio_stopAudio("Galactic");
				Audio_playAudio("GameOver");
			}
			(*MisionMode_getSObject()).estadoActual = MMGameOver;
		}
	}
}

//***Update Jefes
void Mision15_updateJefes()
{
	Jefe_update(Mision15S.elJefe);
	if ((*Mision15S.elJefe).estaEnColisionDisp == 1)
	{
		(*Mision15S.elJefe).estaEnColisionDisp = 0;
		if ((*Animation_getSObject()).animacionesDispAEnemigoTotal.size > 0)
		{
			Animation_setPosition(
					(Animation*) Lista_GetIn(
							&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
							(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
									- 1),
					(*CollitionJefe_getSObject()).positionUltimoDisparo);

			Lista_AddInFin(&(*QuickModeGame_getSObject()).igb.animacionesQ,
					Lista_GetIn(
							&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
							(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
									- 1));

			Lista_RemoveAt(
					&(*Animation_getSObject()).animacionesDispAEnemigoTotal,
					(*Animation_getSObject()).animacionesDispAEnemigoTotal.size
							- 1);
		}
	}
	if ((*Mision15S.elJefe).estaEnColisionPoder)
	{
		(*Mision15S.elJefe).estaEnColisionPoder = false;
		if ((*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size > 0)
		{
			Animation_setPosition(
					(Animation*) Lista_GetIn(
							&(*Animation_getSObject()).animacionesEstallidoEnemigoTotal,
							(*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
									- 1),
					(*CollitionJefe_getSObject()).positionUltimoDisparo);

			Lista_AddInFin(&(*QuickModeGame_getSObject()).igb.animacionesQ,
					Lista_GetIn(
							&(*Animation_getSObject()).animacionesEstallidoEnemigoTotal,
							(*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
									- 1));

			Lista_RemoveAt(
					&(*Animation_getSObject()).animacionesEstallidoEnemigoTotal,
					(*Animation_getSObject()).animacionesEstallidoEnemigoTotal.size
							- 1);
		}
	}
	BarraVida_update((*QuickModeGame_getSObject()).igb.barraVidaJefe,
			(*Mision15S.elJefe).vida, (*Mision15S.elJefe).maxVida);
}

//******Dibujar jefes
void Mision15_drawJefes()
{
	Jefe_draw(Mision15S.elJefe);
	BarraVida_draw((*QuickModeGame_getSObject()).igb.barraVidaJefe);
}

//********Dibujo Mensaje Inicial
void Mision15_drawMensajeInicial()
{
	if ((*QuickModeGame_getSObject()).igb.mensajeInicialActivo == 1)
	{
		Dibujar_drawText((*Textura_getSObject()).Fuente,
				GameFramework_Concatenar(2, Mision15S.tituloMision, " Start!!"),
				GameFramework_Vector2(50, 620),
				GameFramework_Color(GameFramework_getByteColorToFloatColor(7),
						GameFramework_getByteColorToFloatColor(237),
						GameFramework_getByteColorToFloatColor(134), 1),
				(*QuickModeGame_getSObject()).igb.EscalaMensaje);
	}
}

//Reiniciar Jefe
void Mision15_reiniciaJefe()
{
	if ((*Mision15S.elJefe).animaciones.size > 0)
	{
		for (Mision15S.j = 0;
				Mision15S.j < (*Mision15S.elJefe).animaciones.size;
				Mision15S.j++)
		{
			if ((*(Animation*) Lista_RetornaElemento(
					&((*Mision15S.elJefe).animaciones), Mision15S.j)).tipoActual
					== ADisparoANave)
			{
				(*(Animation*) Lista_RetornaElemento(
						&((*Mision15S.elJefe).animaciones), Mision15S.j)).animacionFinalizada =
						0;

				Lista_AddInFin(
						&(*Animation_getSObject()).animacionesDispANaveTotal,
						Lista_GetIn(&(*Mision15S.elJefe).animaciones,
								Mision15S.j));

				Lista_RemoveAt(&((*Mision15S.elJefe).animaciones), Mision15S.j);
			}
		}
	}

	if ((*Mision15S.elJefe).disparos.size > 0)
	{
		for (Mision15S.j = 0; Mision15S.j < (*Mision15S.elJefe).disparos.size;
				Mision15S.j++)
		{
			if ((*(DisparoJefe*) Lista_GetIn(&(*Mision15S.elJefe).disparos,
					Mision15S.j)).tipoActual == DJnormal)
			{
				Lista_AddInFin(&(*Jefe_getSObject()).disparosNormalTotal,
						Lista_GetIn(&(*Mision15S.elJefe).disparos,
								Mision15S.j));
			}
		}
		for (Mision15S.j = 0; Mision15S.j < (*Mision15S.elJefe).disparos.size;
				Mision15S.j++)
		{
			if ((*(DisparoJefe*) Lista_GetIn(&(*Mision15S.elJefe).disparos,
					Mision15S.j)).tipoActual == DJmaximo)
			{
				Lista_AddInFin(&(*Jefe_getSObject()).disparosMaxTotal,
						Lista_GetIn(&(*Mision15S.elJefe).disparos,
								Mision15S.j));
			}
		}
	}
	Lista_Destruir(&(*Mision15S.elJefe).disparos);
	BarraVida_reiniciaColor((*QuickModeGame_getSObject()).igb.barraVidaJefe);
}

Mision15* Mision15_getSObject()
{
	return &Mision15S;
}

