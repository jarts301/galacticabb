/*
 * Audio.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "Audio.h"
#include "Lista.h"
#include "Cadena.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <bps/bps.h>
#include <bps/audiomixer.h>
#include <bps/dialog.h>
#include <bps/navigator.h>
#include <mm/renderer.h>
#include <screen/screen.h>
#include "GameFramework.h"
#include <sys/strm.h>
#include <limits.h>
#include <fcntl.h>
#include <gulliver.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/slogcodes.h>
#include <ctype.h>
#include <screen/screen.h>
#include <sys/asoundlib.h>
#include <bps/bps.h>
#include <bps/audiodevice.h>
#include <bps/dialog.h>
#include <bps/mmrenderer.h>

static Audio audioS;

float volume_;
int rc_;

#if ! defined(DEFAULT_AUDIO_OUT)
#define DEFAULT_AUDIO_OUT "audio:default"
#endif

#define COLOR_PURPLE 0xffff00ff

void Audio_Inicia()
{
	audioS.efectosActivos = 1;
	audioS.musicaActiva = 1;
	Lista_Inicia(&(audioS.Musica));
	Lista_Inicia(&(audioS.nombreMusica));
}

int Audio_addAudio(char* dir, char* nombre, int repetir)
{
	Musica* nuevo;
	if ((nuevo = (Musica*) malloc(sizeof(Musica))) == NULL)
		return 0;

	(*nuevo).mmrname = NULL;
	(*nuevo).ctxtname = nombre;
	(*nuevo).audioout = NULL;
	(*nuevo).inputtype = "autolist"; //"track";
	(*nuevo).mode = S_IRUSR | S_IXUSR; // S_IRUSR indicates read permission
									   // and S_IXUSR indicates
									   // execute/search permission

	getcwd((*nuevo).cwd, PATH_MAX);
	rc_ = snprintf((*nuevo).inputurl, PATH_MAX, "file://%s%s", (*nuevo).cwd,
			dir);
	if (rc_ > PATH_MAX - 1)
	{
		//show_dialog_message("File name and path too long");
	}

	//mode_t mode = S_IRUSR | S_IXUSR;
	//int audio_oid; // output ID
	(*nuevo).aoparams = NULL; // output parameters

	(*nuevo).audioout = DEFAULT_AUDIO_OUT;

	if (((*nuevo).connection = mmr_connect((*nuevo).mmrname)) == NULL)
	{
		//snprintf(msg_, MSG_SIZE, "mmr_connect: %s", strerror(errno));
		printf("Error al conectar mmr");
		//show_dialog_message(msg);
	}
	else if (((*nuevo).ctxt = mmr_context_create((*nuevo).connection,
			(*nuevo).ctxtname, 0, (*nuevo).mode)) == NULL)
	{
		//snprintf(msg, MSG_SIZE, "%s: %s", ctxtname, strerror(errno));
		printf("Error al crear mmr");
		//show_dialog_message(msg);
	}
	else if ((*nuevo).audioout
			&& ((*nuevo).audio_oid = mmr_output_attach((*nuevo).ctxt,
					(*nuevo).audioout, "audio")) < 0)
	{
		//mmrerror( ctxt_, audioout_ );
		printf("Error attach output mmr");
	}
	else if ((*nuevo).aoparams
			&& mmr_output_parameters((*nuevo).ctxt, (*nuevo).audio_oid,
					(*nuevo).aoparams))
	{
		//mmrerror( ctxt_, "output parameters (audio)" );
		printf("Error output parametters mmr");
	}
	else if (mmr_input_attach((*nuevo).ctxt, (*nuevo).inputurl,
			(*nuevo).inputtype) < 0)
	{
		//mmrerror( ctxt_, inputurl_ );
		printf("Error attach input mmr");
	}
	(*nuevo).dic = strm_dict_new();
	if (repetir == 1)
	{
		(*nuevo).dic = strm_dict_set((*nuevo).dic, "repeat", "all");
	}
	else
	{
		(*nuevo).dic = strm_dict_set((*nuevo).dic, "repeat", "none");
	}

	//mmr_input_attach((*nuevo).ctxt, (*nuevo).inputurl, (*nuevo).inputtype);
	if (-1 == mmr_input_parameters((*nuevo).ctxt, (*nuevo).dic))
	{
		printf("Error input parametters mmr");
	}

	/*(*nuevo).nombreCod = nombre;
	 if (BPS_SUCCESS
	 != mmrenderer_request_events((*nuevo).ctxtname, 0,
	 (intptr_t) ((*nuevo).nombreCod)))
	 {
	 printf("Error request events mmr");
	 }*/

	Lista_InsertaEnFinal(&(audioS.Musica), audioS.Musica.fin, nuevo);
	Lista_InsertaEnFinal(&(audioS.nombreMusica), audioS.nombreMusica.fin,
			nombre);

	return 1;
}

Musica* Audio_getAudio(char* nombre)
{
	for (audioS.i = 0; audioS.i < (audioS.Musica).size; audioS.i++)
	{
		if (strcmp(
				(char*) Lista_RetornaElemento(&(audioS.nombreMusica), audioS.i),
				nombre) == 0)
		{
			return (Musica*) (Lista_RetornaElemento(&(audioS.Musica), audioS.i));
		}
	}
	return NULL;
}

void Audio_playAudio(char* nombre)
{
	mmr_context_t* contexto = (*Audio_getAudio(nombre)).ctxt;
	AudioEstado estado = (*Audio_getAudio(nombre)).estado;

	if (audioS.musicaActiva == 1 && audioS.efectosActivos == 1)
	{
		if (strcmp(nombre, "Disparo") == 0)
		{
			if (estado == playing || estado == destroyed)
			{
				contexto = (*Audio_getAudio("Disparo2")).ctxt;
				estado = (*Audio_getAudio("Disparo2")).estado;
				if (estado == playing || estado == destroyed)
				{
					contexto = (*Audio_getAudio("Disparo3")).ctxt;
					estado = (*Audio_getAudio("Disparo3")).estado;
					if (estado == playing || estado == destroyed)
					{
						return; //fin audio disparo
					}
					else
					{
						mmr_play(contexto);
						return;
					}
				}
				else
				{
					mmr_play(contexto);
					return;
				}
			}
			else
			{
				mmr_play(contexto);
				return;
			}
		}
		else
		{
			mmr_play(contexto);
			return;
		}
	}
	else
	{
		contexto = (*Audio_getAudio("Silencio")).ctxt;
		mmr_play(contexto);
		return;
	}
}

void Audio_stopAudio(char* nombre)
{
	mmr_stop((*Audio_getAudio(nombre)).ctxt);
}

Lista* Audio_getListaMusica()
{
	return &(audioS.Musica);
}

Lista* Audio_getListaNombreMusica()
{
	return &(audioS.nombreMusica);
}

Audio* Audio_getSObject()
{
	return &audioS;
}
