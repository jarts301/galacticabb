/*
 * Enemigo.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "Enemigo.h"
#include "Textura.h"
#include "DisparoEnemigo.h"
#include "Animation.h"
#include "Nave.h"
#include "Collition.h"
#include "Dibujar.h"
#include <stdlib.h>

static Enemigo EnemigoS;

/*********Constructores de Enemigo*************/

/*
 public Enemigo()
 {
 }
 */
Enemigo* Enemigo_Crear(TipoEnemigo t, int x, int y, int vx, int vy)
{
	Enemigo* nuevo;
	if ((nuevo = (Enemigo*) malloc(sizeof(Enemigo))) == NULL)
		return NULL;

	(*nuevo).movimientoLoco = 0;
	(*nuevo).cambiarMov = 1;
	(*nuevo).movAposition = 0;
	(*nuevo).num = 0;
	(*nuevo).tipoEnemigo = t;
	Enemigo_defineDatosIniciales(nuevo);
	(*nuevo).Position.X = x;
	(*nuevo).Position.Y = y;
	(*nuevo).velocidadX = vx;
	(*nuevo).velocidadCambioColor = 5;
	(*nuevo).tonoAzul = 151;
	(*nuevo).tonoRojo = 151;
	(*nuevo).velocidadY = vy;
	(*nuevo).velocidadXini = vx;
	if (t == ESid && t == EHiSid)
	{
		(*nuevo).velocidadYini = vy;
	}
	(*nuevo).ultima_actua = 0;
	(*nuevo).auxKamiPos = 0;
	(*nuevo).KamiPasivo = 1;
	(*nuevo).SidMovHorizontal = 1;
	(*nuevo).SidMovVertical = 0;
	(*nuevo).goArriba = 1;
	(*nuevo).dispararSid = 0;
	(*nuevo).estaEnColisionDisp = 0;
	(*nuevo).estaEnColisionNave = 0;
	(*nuevo).estaEnColisionPoder = 0;
	(*nuevo).actualframe = 0;
	(*nuevo).dato = 0;
	(*nuevo).contadorDisparosSid = 0;
	(*nuevo).Rotacion = 0;
	(*nuevo).ultimoTDisparo = 0;
	if ((*nuevo).Position.X >= 570)
	{
		(*nuevo).goDerecha = 0;
	}
	if ((*nuevo).Position.X <= 30)
	{
		(*nuevo).goDerecha = 1;
	}
	if ((*nuevo).Position.X >= 570 && (t == ESid || t == EHiSid))
	{
		(*nuevo).Rotacion = 180;
	}
	Lista_Inicia(&(*nuevo).disparosSid);
	Lista_Inicia(&(*nuevo).disparosNor);
	Lista_Inicia(&(*nuevo).animaciones);

	Enemigo_defineDatosAnimacion(nuevo);

	return nuevo;

}

//**************Metodos principales de Enemigo******

void Enemigo_reinicia(Enemigo* enemigo, int x, int y, int vx, int vy,
		int aumentoVida)
{
	(*enemigo).movimientoLoco = 0;
	(*enemigo).cambiarMov = 1;
	EnemigoS.num = 0;
	Enemigo_defineDatosIniciales(enemigo);
	Enemigo_setAumentoVida(enemigo, aumentoVida);
	(*enemigo).Position.X = x;
	(*enemigo).Position.Y = y;
	(*enemigo).velocidadX = vx;
	(*enemigo).velocidadCambioColor = 5;
	(*enemigo).tonoAzul = 151;
	(*enemigo).tonoRojo = 151;
	(*enemigo).velocidadY = vy;
	(*enemigo).velocidadXini = vx;
	if ((*enemigo).tipoEnemigo == ESid && (*enemigo).tipoEnemigo == EHiSid)
	{
		(*enemigo).velocidadYini = vy;
	}
	(*enemigo).ultima_actua = 0;
	(*enemigo).auxKamiPos = 0;
	(*enemigo).KamiPasivo = 1;
	(*enemigo).SidMovHorizontal = 1;
	(*enemigo).SidMovVertical = 0;
	(*enemigo).goArriba = 1;
	(*enemigo).dispararSid = 0;
	(*enemigo).estaEnColisionDisp = 0;
	(*enemigo).estaEnColisionNave = 0;
	(*enemigo).estaEnColisionPoder = 0;
	(*enemigo).actualframe = 0;
	(*enemigo).contadorDisparosSid = 0;
	(*enemigo).Rotacion = 0;
	(*enemigo).ultimoTDisparo = 0;
	(*enemigo).dato = 0;
	if ((*enemigo).Position.X >= 570)
	{
		(*enemigo).goDerecha = 0;
	}
	if ((*enemigo).Position.X <= 30)
	{
		(*enemigo).goDerecha = 1;
	}
	if ((*enemigo).Position.X >= 570
			&& ((*enemigo).tipoEnemigo == ESid
					|| (*enemigo).tipoEnemigo == EHiSid))
	{
		(*enemigo).Rotacion = 180;
	}
	Enemigo_defineDatosAnimacion(enemigo);
}

//Metodo load de enemigo
void Enemigo_load()
{
	EnemigoS.adicionDanoNave = 0;
	Enemigo_setTextura();
	Enemigo_defineListasDeDisparos();
	EnemigoS.radioCollitionNor = (int) (128 * 0.34f);
	EnemigoS.radioCollitionHiNor = (int) (128 * 0.68f);
	EnemigoS.radioCollitionKami = (int) (100 * 0.44f);
	EnemigoS.radioCollitionHiKami = (int) (100 * 0.88f);
	Enemigo_defineCollitionBoxSid();
	Enemigo_defineCollitionBoxHiSid();
	Enemigo_loadListasEnemigos();
}

//metodo update de enemigo
void Enemigo_update(Enemigo* enemigo)
{
	Enemigo_defineDatosAnimacion(enemigo);
	Enemigo_velocidadAnimacion(enemigo);
	Enemigo_updateDisparosSid(enemigo);
	Enemigo_updateDisparosNor(enemigo);
	Enemigo_updateAnimaciones(enemigo);
}

//metodo draw de enemigo
void Enemigo_draw(Enemigo* enemigo)
{
	Dibujar_drawSpriteTr(
			(TexturaObjeto*) Lista_GetIn(&(EnemigoS.Texturas),
					(*enemigo).texturaAnim), (*enemigo).Position,
			(*enemigo).TrozoAnim, (*enemigo).colorEnemigo, (*enemigo).Rotacion,
			(*enemigo).Origen, (*enemigo).Escala);

	/*spriteBatch.Draw(texEnemigos[texturaAnim], Position, TrozoAnim,
	 colorEnemigo, MathHelper.ToRadians(Rotacion), Origen, Escala,
	 SpriteEffects.None, 0.43f);*/
	Enemigo_drawDisparosSid(enemigo);
	Enemigo_drawDisparosNor(enemigo);

	Dibujar_drawSpriteTr(
			(TexturaObjeto*) Lista_GetIn(&(EnemigoS.Texturas),
					(*enemigo).texturaAnim), (*enemigo).Position,
			(*enemigo).TrozoAnim, (*enemigo).colorEnemigo, (*enemigo).Rotacion,
			(*enemigo).Origen, (*enemigo).Escala);

	Enemigo_drawAnimaciones(enemigo);
}
//**************FIN Metodos principales de Enemigo******

//*******************Definicion de metodos agregados********************

void Enemigo_setAumentoVida(Enemigo* enemigo, int v)
{
	(*enemigo).vida = (*enemigo).vida + v;
	(*enemigo).maxVida = (*enemigo).maxVida + v;
}

void Enemigo_setPosition(Enemigo* enemigo, Vector2 pos)
{
	(*enemigo).Position.X = pos.X;
	(*enemigo).Position.Y = pos.Y;
}

void Enemigo_loadListasEnemigos()
{
	Lista_Inicia(&(EnemigoS.SidTotal));
	Lista_Inicia(&(EnemigoS.NorTotal));
	Lista_Inicia(&(EnemigoS.KamiTotal));
	Lista_Inicia(&(EnemigoS.HiSidTotal));
	Lista_Inicia(&(EnemigoS.HiNorTotal));
	Lista_Inicia(&(EnemigoS.HiKamiTotal));

	for (EnemigoS.i = 0; EnemigoS.i < 100; EnemigoS.i++)
	{
		Lista_InsertaEnFinal(&(EnemigoS.NorTotal), EnemigoS.NorTotal.fin,
				Enemigo_Crear(ENor, 0, 0, 0, 0));
		Lista_InsertaEnFinal(&(EnemigoS.KamiTotal), EnemigoS.KamiTotal.fin,
				Enemigo_Crear(EKami, 0, 0, 0, 0));
		Lista_InsertaEnFinal(&(EnemigoS.SidTotal), EnemigoS.SidTotal.fin,
				Enemigo_Crear(ESid, 0, 0, 0, 0));
		Lista_InsertaEnFinal(&(EnemigoS.HiNorTotal), EnemigoS.HiNorTotal.fin,
				Enemigo_Crear(EHiNor, 0, 0, 0, 0));
		Lista_InsertaEnFinal(&(EnemigoS.HiKamiTotal), EnemigoS.HiKamiTotal.fin,
				Enemigo_Crear(EHiKami, 0, 0, 0, 0));
		Lista_InsertaEnFinal(&(EnemigoS.HiSidTotal), EnemigoS.HiSidTotal.fin,
				Enemigo_Crear(EHiSid, 0, 0, 0, 0));
	}
}

//velocidad de la animacion
void Enemigo_velocidadAnimacion(Enemigo* enemigo)
{
	(*enemigo).dato += GameFramework_getSwapTime();
	if ((*enemigo).dato
			>= ((GameFramework_getSwapTime()) * (*enemigo).velocidad))
	{
		(*enemigo).actualframe = (*enemigo).actualframe + 1;
		(*enemigo).dato = 0;
	}

	if ((*enemigo).actualframe >= (*enemigo).numeroCuadros)
	{
		(*enemigo).actualframe = 0;
	}
}

//define los datos de cada enemigo
void Enemigo_defineDatosAnimacion(Enemigo* enemigo)
{
	if ((*enemigo).tipoEnemigo == ENor || (*enemigo).tipoEnemigo == EHiNor)
	{
		Enemigo_defineTrozoAnim(enemigo, 256 * (*enemigo).actualframe, 0, 256,
				256);
		Enemigo_defineOrigen(enemigo);
		Enemigo_movimientoNor(enemigo);
	}
	else if ((*enemigo).tipoEnemigo == EKami
			|| (*enemigo).tipoEnemigo == EHiKami)
	{
		Enemigo_defineTrozoAnim(enemigo, 256 * (*enemigo).actualframe, 0, 256,
				256);
		Enemigo_defineOrigen(enemigo);
		Enemigo_movimientoKami(enemigo);
	}
	else if ((*enemigo).tipoEnemigo == ESid || (*enemigo).tipoEnemigo == EHiSid)
	{
		Enemigo_defineTrozoAnim(enemigo, 292 * (*enemigo).actualframe, 0, 292,
				256);
		Enemigo_defineOrigen(enemigo);
		Enemigo_movimientoSid(enemigo);
	}

	if ((*enemigo).tipoEnemigo == EHiNor || (*enemigo).tipoEnemigo == EHiKami
			|| (*enemigo).tipoEnemigo == EHiSid)
	{
		(*enemigo).tonoAzul = (*enemigo).tonoAzul
				+ (*enemigo).velocidadCambioColor;
		(*enemigo).tonoRojo = (*enemigo).tonoRojo
				+ (*enemigo).velocidadCambioColor;

		if ((*enemigo).tonoAzul <= 150 || (*enemigo).tonoAzul >= 250)
		{
			(*enemigo).velocidadCambioColor = (*enemigo).velocidadCambioColor
					* (-1);
		}

		(*enemigo).colorEnemigo.B = GameFramework_getByteColorToFloatColor(
				(*enemigo).tonoAzul);
		(*enemigo).colorEnemigo.R = GameFramework_getByteColorToFloatColor(
				(*enemigo).tonoRojo);
	}
}

//Datos iniciales de cada enemigo
void Enemigo_defineDatosIniciales(Enemigo* enemigo)
{
	switch ((*enemigo).tipoEnemigo)
	{
	case ENor:
		(*enemigo).velocidad = 3;
		(*enemigo).numeroCuadros = 8;
		(*enemigo).texturaAnim = 0;
		(*enemigo).Escala.X = 0.34f;
		(*enemigo).Escala.Y = 0.34f;
		(*enemigo).colorEnemigo.R = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).colorEnemigo.G = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).colorEnemigo.B = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).colorEnemigo.A = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).vida = (*enemigo).maxVida = 15;
		break;

	case EKami:
		(*enemigo).velocidad = 3;
		(*enemigo).numeroCuadros = 8;
		(*enemigo).texturaAnim = 1;
		(*enemigo).Escala.X = 0.44f;
		(*enemigo).Escala.Y = 0.44f;
		(*enemigo).colorEnemigo.R = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).colorEnemigo.G = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).colorEnemigo.B = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).colorEnemigo.A = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).vida = (*enemigo).maxVida = 15;
		break;

	case ESid:
		(*enemigo).velocidad = 3;
		(*enemigo).numeroCuadros = 7;
		(*enemigo).texturaAnim = 2;
		(*enemigo).Escala.X = 0.35f;
		(*enemigo).Escala.Y = 0.355f;
		(*enemigo).colorEnemigo.R = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).colorEnemigo.G = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).colorEnemigo.B = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).colorEnemigo.A = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).vida = (*enemigo).maxVida = 15;
		break;

	case EHiNor:
		(*enemigo).velocidad = 3;
		(*enemigo).numeroCuadros = 8;
		(*enemigo).texturaAnim = 0;
		(*enemigo).Escala.X = 0.52f;
		(*enemigo).Escala.Y = 0.52f;
		(*enemigo).colorEnemigo.R = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).colorEnemigo.G = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).colorEnemigo.B = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).colorEnemigo.A = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).vida = (*enemigo).maxVida = 30;
		break;

	case EHiKami:
		(*enemigo).velocidad = 3;
		(*enemigo).numeroCuadros = 8;
		(*enemigo).texturaAnim = 1;
		(*enemigo).Escala.X = 0.62f;
		(*enemigo).Escala.Y = 0.62f;
		(*enemigo).colorEnemigo.R = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).colorEnemigo.G = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).colorEnemigo.B = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).colorEnemigo.A = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).vida = (*enemigo).maxVida = 30;
		break;

	case EHiSid:
		(*enemigo).velocidad = 3;
		(*enemigo).numeroCuadros = 7;
		(*enemigo).texturaAnim = 2;
		(*enemigo).Escala.X = 0.49f;
		(*enemigo).Escala.Y = 0.5f;
		(*enemigo).colorEnemigo.R = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).colorEnemigo.G = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).colorEnemigo.B = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).colorEnemigo.A = GameFramework_getByteColorToFloatColor(255);
		(*enemigo).vida = (*enemigo).maxVida = 30;
		break;
	}
}

//define el cuandro de la animacion actual
void Enemigo_defineTrozoAnim(Enemigo* enemigo, int x, int y, int width,
		int height)
{
	(*enemigo).TrozoAnim.X = x;
	(*enemigo).TrozoAnim.Y = y;
	(*enemigo).TrozoAnim.Width = width;
	(*enemigo).TrozoAnim.Height = height;
}

//define el origen de coordenadas de la animacion
void Enemigo_defineOrigen(Enemigo* enemigo)
{
	(*enemigo).Origen.X = (*enemigo).TrozoAnim.Width / 2;
	(*enemigo).Origen.Y = (*enemigo).TrozoAnim.Height / 2;
}

//**************Inteligencia de Nor*****************
void Enemigo_movimientoNor(Enemigo* enemigo)
{
	if ((*enemigo).movimientoLoco == 0)
	{
		if ((*enemigo).goDerecha)
		{
			(*enemigo).Position.X = (*enemigo).Position.X
					+ ((*enemigo).velocidadX - (*enemigo).velocidadY);
			if ((*enemigo).Position.X >= 520
					&& ((*enemigo).velocidadX - (*enemigo).velocidadY) != 0)
			{
				(*enemigo).velocidadY++;
			}
			if (((*enemigo).velocidadX - (*enemigo).velocidadY) == 0
					|| (*enemigo).Position.X >= 560)
			{
				(*enemigo).goDerecha = 0;
				(*enemigo).velocidadY = 0;
			}
		}
		else if ((*enemigo).goDerecha == 0)
		{
			(*enemigo).Position.X = (*enemigo).Position.X
					- ((*enemigo).velocidadX - (*enemigo).velocidadY);
			if ((*enemigo).Position.X <= 80
					&& ((*enemigo).velocidadX - (*enemigo).velocidadY) != 0)
			{
				(*enemigo).velocidadY++;
			}
			if (((*enemigo).velocidadX - (*enemigo).velocidadY) == 0
					|| (*enemigo).Position.X <= 30)
			{
				(*enemigo).goDerecha = 1;
				(*enemigo).velocidadY = 0;
			}
		}

		if ((GameFramework_getTotalTime() > (*enemigo).ultima_actua + 5000000)
				&& ((*enemigo).Position.X > 0 && (*enemigo).Position.X < 600))
		{
			if (EnemigoS.disparosNorTotal.size > 0)
			{
				DisparoEnemigo_setPosition(
						(DisparoEnemigo*) Lista_GetIn(
								&(EnemigoS.disparosNorTotal),
								EnemigoS.disparosNorTotal.size - 1),
						(*enemigo).Position);

				(*(DisparoEnemigo*) Lista_GetIn(&(EnemigoS.disparosNorTotal),
						EnemigoS.disparosNorTotal.size - 1)).Disparar = 1;

				Lista_AddInFin(&((*enemigo).disparosNor),
						Lista_GetIn(&(EnemigoS.disparosNorTotal),
								EnemigoS.disparosNorTotal.size - 1));

				Lista_RemoveAt(&(EnemigoS.disparosNorTotal),
						EnemigoS.disparosNorTotal.size - 1);
			}

			(*enemigo).ultima_actua = GameFramework_getTotalTime();
		}
	}
	else
	{
		if ((*enemigo).movAposition == 0)
		{
			if ((*enemigo).cambiarMov==1)
			{
				(*enemigo).velocidadX = GameFramework_Rand(
						(*enemigo).velMinLoco, (*enemigo).velMaxLoco);
				(*enemigo).velocidadY = GameFramework_Rand(
						(*enemigo).velMinLoco, (*enemigo).velMaxLoco);
				(*enemigo).numero = GameFramework_Rand(1, 4);
				switch ((*enemigo).numero)
				{
				case 1:
					(*enemigo).velocidadX = (*enemigo).velocidadX * (1);
					(*enemigo).velocidadY = (*enemigo).velocidadY * (1);
					break;
				case 2:
					(*enemigo).velocidadX = (*enemigo).velocidadX * (-1);
					(*enemigo).velocidadY = (*enemigo).velocidadY * (1);
					break;
				case 3:
					(*enemigo).velocidadX = (*enemigo).velocidadX * (1);
					(*enemigo).velocidadY = (*enemigo).velocidadY * (-1);
					break;
				case 4:
					(*enemigo).velocidadX = (*enemigo).velocidadX * (-1);
					(*enemigo).velocidadY = (*enemigo).velocidadY * (-1);
					break;

				}
				(*enemigo).cambiarMov = 0;
			}

			if ((*enemigo).Position.Y <= (*enemigo).locoAbajo
					&& (*enemigo).velocidadY < 0)
			{
				(*enemigo).velocidadY = (*enemigo).velocidadY * (-1);
				//cambiarMov = true;
			}
			if ((*enemigo).Position.Y >= (*enemigo).locoArriba
					&& (*enemigo).velocidadY > 0)
			{
				(*enemigo).velocidadY = (*enemigo).velocidadY * (-1);
				(*enemigo).cambiarMov = 1;
			}
			if ((*enemigo).Position.X >= (*enemigo).locoDerecha
					&& (*enemigo).velocidadX > 0)
			{
				(*enemigo).velocidadX = (*enemigo).velocidadX * (-1);
				//cambiarMov = true;
			}
			if ((*enemigo).Position.X <= (*enemigo).locoIzquierda
					&& (*enemigo).velocidadX < 0)
			{
				(*enemigo).velocidadX = (*enemigo).velocidadX * (-1);
				(*enemigo).cambiarMov = 1;
			}

			(*enemigo).Position.X = (*enemigo).Position.X
					+ (*enemigo).velocidadX;
			(*enemigo).Position.Y = (*enemigo).Position.Y
					+ (*enemigo).velocidadY;
		}
		else
		{
			if ((*enemigo).positionAMoverse.X != (*enemigo).Position.X
					&& (*enemigo).Position.X > (*enemigo).positionAMoverse.X)
			{
				(*enemigo).Position.X = (*enemigo).Position.X - 1;
			}
			if ((*enemigo).positionAMoverse.X != (*enemigo).Position.X
					&& (*enemigo).Position.X < (*enemigo).positionAMoverse.X)
			{
				(*enemigo).Position.X = (*enemigo).Position.X + 1;
			}
			if ((*enemigo).positionAMoverse.Y != (*enemigo).Position.Y
					&& (*enemigo).Position.Y > (*enemigo).positionAMoverse.Y)
			{
				(*enemigo).Position.Y = (*enemigo).Position.Y - 1;
			}
			if ((*enemigo).positionAMoverse.Y != (*enemigo).Position.Y
					&& (*enemigo).Position.Y < (*enemigo).positionAMoverse.Y)
			{
				(*enemigo).Position.Y = (*enemigo).Position.Y + 1;
			}
		}
	}

	Enemigo_confirmaColisiones(&((*enemigo).colision), enemigo, 20);

}

//update de disparos de Nor
void Enemigo_updateDisparosNor(Enemigo* enemigo)
{
	if ((*enemigo).disparosNor.size > 0)
	{
		for (EnemigoS.i = 0; EnemigoS.i < (*enemigo).disparosNor.size;
				EnemigoS.i++)
		{
			DisparoEnemigo_update(
					(DisparoEnemigo*) Lista_GetIn(&((*enemigo).disparosNor),
							EnemigoS.i));

		}

		for (EnemigoS.i = 0; EnemigoS.i < (*enemigo).disparosNor.size;
				EnemigoS.i++)
		{
			if ((*(DisparoEnemigo*) Lista_GetIn(&((*enemigo).disparosNor),
					EnemigoS.i)).estaEnColision == 1)
			{
				(*(DisparoEnemigo*) Lista_GetIn(&((*enemigo).disparosNor),
						EnemigoS.i)).estaEnColision = 0;

				if ((*Animation_getSObject()).animacionesDispANaveTotal.size
						> 0)
				{
					Animation_setPosition(
							(Animation*) Lista_GetIn(
									&((*Animation_getSObject()).animacionesDispANaveTotal),
									(*Animation_getSObject()).animacionesDispANaveTotal.size
											- 1),
							(*(Enemigo*) Lista_GetIn(&((*enemigo).disparosNor),
									EnemigoS.i)).Position);
					Lista_AddInFin(&((*enemigo).animaciones),
							Lista_GetIn(
									&((*Animation_getSObject()).animacionesDispANaveTotal),
									(*Animation_getSObject()).animacionesDispANaveTotal.size
											- 1));
					Lista_RemoveAt(
							&((*Animation_getSObject()).animacionesDispANaveTotal),
							(*Animation_getSObject()).animacionesDispANaveTotal.size
									- 1);
				}

				(*(DisparoEnemigo*) Lista_GetIn(&((*enemigo).disparosNor),
						EnemigoS.i)).postDisparo = 0;
				Lista_AddInFin(&(EnemigoS.disparosNorTotal),
						Lista_GetIn(&((*enemigo).disparosNor), EnemigoS.i));
				Lista_RemoveAt(&((*enemigo).disparosNor), EnemigoS.i);
			}
		}

		for (EnemigoS.i = 0; EnemigoS.i < (*enemigo).disparosNor.size;
				EnemigoS.i++)
		{
			if ((*(DisparoEnemigo*) Lista_GetIn(&((*enemigo).disparosNor),
					EnemigoS.i)).Position.X > 600
					|| (*(DisparoEnemigo*) Lista_GetIn(
							&((*enemigo).disparosNor), EnemigoS.i)).Position.X
							< 0
					|| (*(DisparoEnemigo*) Lista_GetIn(
							&((*enemigo).disparosNor), EnemigoS.i)).Position.Y
							< 0
					|| (*(DisparoEnemigo*) Lista_GetIn(
							&((*enemigo).disparosNor), EnemigoS.i)).Position.Y
							> 1024)
			{
				(*(DisparoEnemigo*) Lista_GetIn(&((*enemigo).disparosNor),
						EnemigoS.i)).postDisparo = 0;
				Lista_AddInFin(&(EnemigoS.disparosNorTotal),
						Lista_GetIn(&((*enemigo).disparosNor), EnemigoS.i));
				Lista_RemoveAt(&((*enemigo).disparosNor), EnemigoS.i);
			}
		}
	}
}

//draw disparos de Nor
void Enemigo_drawDisparosNor(Enemigo* enemigo)
{
	for (EnemigoS.i = 0; EnemigoS.i < (*enemigo).disparosNor.size; EnemigoS.i++)
	{
		DisparoEnemigo_draw(
				(DisparoEnemigo*) Lista_GetIn(&((*enemigo).disparosNor),
						EnemigoS.i));
	}
}
//**************Fin Inteligencia de Nor*****************

//**************Inteligencia de Kami*****************
void Enemigo_movimientoKami(Enemigo* enemigo)
{
	if ((*enemigo).KamiPasivo)
	{
		if ((*enemigo).goDerecha == 1)
		{
			(*enemigo).Position.X = (*enemigo).Position.X
					+ (*enemigo).velocidadX;
			(*enemigo).Position.Y = (*enemigo).Position.Y
					+ (*enemigo).velocidadY;
			if ((*enemigo).goArriba == 1)
			{
				(*enemigo).velocidadY = (*enemigo).velocidadY - 2;
			}
			if ((*enemigo).goArriba == 0)
			{
				(*enemigo).velocidadY = (*enemigo).velocidadY + 2;
			}
			if ((*enemigo).velocidadY > 10 || (*enemigo).velocidadY < -10)
			{
				if ((*enemigo).goArriba == 0)
				{
					(*enemigo).goArriba = 1;
				}
				else
				{
					(*enemigo).goArriba = 0;
				}
			}
			if ((*enemigo).Position.X >= 240)
			{
				(*enemigo).Tiempo = 0;
				(*enemigo).posNave = (*Nave_getSObject()).PositionNav;
				(*enemigo).x1 = (*enemigo).Position.X;
				(*enemigo).y1 = (*enemigo).Position.Y;
				(*enemigo).velocidadX = (int) (((*enemigo).posNave.X
						- (*enemigo).x1)) * 1 / 40;
				(*enemigo).velocidadY = (int) (((*enemigo).posNave.Y
						- (*enemigo).y1)) * 1 / 40;
				(*enemigo).KamiPasivo = 0;
			}
		}

		if ((*enemigo).goDerecha == 0)
		{
			(*enemigo).Position.X = (*enemigo).Position.X
					- (*enemigo).velocidadX;
			(*enemigo).Position.Y = (*enemigo).Position.Y
					+ (*enemigo).velocidadY;
			if ((*enemigo).goArriba == 1)
			{
				(*enemigo).velocidadY = (*enemigo).velocidadY - 2;
			}
			if ((*enemigo).goArriba == 0)
			{
				(*enemigo).velocidadY = (*enemigo).velocidadY + 2;
			}
			if ((*enemigo).velocidadY > 10 || (*enemigo).velocidadY < -10)
			{
				if ((*enemigo).goArriba == 0)
				{
					(*enemigo).goArriba = 1;
				}
				else
				{
					(*enemigo).goArriba = 0;
				}
			}
			if ((*enemigo).Position.X <= 240)
			{
				(*enemigo).Tiempo = 0;
				(*enemigo).posNave = (*Nave_getSObject()).PositionNav;
				(*enemigo).x1 = (*enemigo).Position.X;
				(*enemigo).y1 = (*enemigo).Position.Y;
				(*enemigo).velocidadX = (int) (((*enemigo).posNave.X
						- (*enemigo).x1)) * 1 / 50;
				(*enemigo).velocidadY = (int) (((*enemigo).posNave.Y
						- (*enemigo).y1)) * 1 / 50;
				(*enemigo).KamiPasivo = 0;
			}
		}
	}

	if ((*enemigo).KamiPasivo == 0)
	{
		(*enemigo).Tiempo++;
		(*enemigo).Position.X = (*enemigo).x1
				+ (*enemigo).velocidadX * (*enemigo).Tiempo;
		(*enemigo).Position.Y = (*enemigo).y1
				+ (*enemigo).velocidadY * (*enemigo).Tiempo;

		if (((*enemigo).Position.X > 600 || (*enemigo).Position.X < -200)
				|| ((*enemigo).Position.Y > 1200 || (*enemigo).Position.Y < -200))
		{
			(*enemigo).auxKamiPos = GameFramework_Rand(0, 1);
			if ((*enemigo).auxKamiPos == 0)
			{
				(*enemigo).Position.X = GameFramework_Rand(600, 1500);
				(*enemigo).goDerecha = 0;
			}
			else if ((*enemigo).auxKamiPos == 1)
			{
				(*enemigo).Position.X = GameFramework_Rand(-1000, -200);
				(*enemigo).goDerecha = 1;
			}
			(*enemigo).Position.Y = GameFramework_Rand(600, 1000);
			(*enemigo).velocidadX = (*enemigo).velocidadXini;
			(*enemigo).velocidadY = 0;
			(*enemigo).KamiPasivo = 1;
		}
	}

	Enemigo_confirmaColisiones(&((*enemigo).colision), enemigo, 30);

}
//**************Fin Inteligencia de Kami*****************
//**************Inteligencia de Sid*****************
void Enemigo_movimientoSid(Enemigo* enemigo)
{
	if ((*enemigo).SidMovHorizontal == 1)
	{
		if ((*enemigo).goDerecha == 1)
		{
			if ((*enemigo).Position.X < 20)
			{
				(*enemigo).Position.X = (*enemigo).Position.X
						+ (*enemigo).velocidadX;
			}
			else
			{
				(*enemigo).posNave = (*Nave_getSObject()).PositionNav;
				(*enemigo).SidMovHorizontal = 0;
				(*enemigo).SidMovVertical = 1;
			}
		}
		if ((*enemigo).goDerecha == 0)
		{
			if ((*enemigo).Position.X > 580)
			{
				(*enemigo).Position.X = (*enemigo).Position.X
						- (*enemigo).velocidadX;
			}
			else
			{
				(*enemigo).posNave = (*Nave_getSObject()).PositionNav;
				(*enemigo).SidMovHorizontal = 0;
				(*enemigo).SidMovVertical = 1;
			}
		}
	}
	if ((*enemigo).SidMovVertical == 1)
	{
		if ((*enemigo).Position.Y < (*enemigo).posNave.Y - 15)
		{
			(*enemigo).Position.Y = (*enemigo).Position.Y
					+ (*enemigo).velocidadY;
			(*enemigo).ultimoTDisparo = GameFramework_getTotalTime();
		}
		else
		{
			if ((*enemigo).Position.Y > (*enemigo).posNave.Y + 15)
			{
				(*enemigo).Position.Y = (*enemigo).Position.Y
						- (*enemigo).velocidadY;
				(*enemigo).ultimoTDisparo = GameFramework_getTotalTime();
			}
			else
			{
				if (GameFramework_getTotalTime()
						> (*enemigo).ultimoTDisparo + 500000)
				{
					(*enemigo).dispararSid = 1;
					(*enemigo).SidMovVertical = 1;
				}
			}
		}
	}

	if ((*enemigo).dispararSid)
	{
		if (EnemigoS.disparosSidTotal.size > 0)
		{
			DisparoEnemigo_setLadoMovimiento(
					(DisparoEnemigo*) Lista_GetIn(&(EnemigoS.disparosSidTotal),
							EnemigoS.disparosSidTotal.size - 1),
					(*enemigo).goDerecha);

			DisparoEnemigo_setPosition(
					(DisparoEnemigo*) Lista_GetIn(&(EnemigoS.disparosSidTotal),
							EnemigoS.disparosSidTotal.size - 1),
					(*enemigo).Position);

			Lista_AddInFin(&((*enemigo).disparosSid),
					Lista_GetIn(&(EnemigoS.disparosSidTotal),
							EnemigoS.disparosSidTotal.size - 1));

			Lista_RemoveAt(&(EnemigoS.disparosSidTotal),
					EnemigoS.disparosSidTotal.size - 1);

			(*enemigo).contadorDisparosSid++;
		}
	}

	if ((*enemigo).contadorDisparosSid > 5)
	{
		(*enemigo).contadorDisparosSid = 0;
		(*enemigo).dispararSid = 0;
		(*enemigo).posNave = (*Nave_getSObject()).PositionNav;
		(*enemigo).SidMovVertical = 1;
	}

	Enemigo_confirmaColisiones(&((*enemigo).colision), enemigo, 20);

}
//update de disparos de Sid
void Enemigo_updateDisparosSid(Enemigo* enemigo)
{
	if ((*enemigo).disparosSid.size != 0)
	{
		for (EnemigoS.i = 0; EnemigoS.i < (*enemigo).disparosSid.size;
				EnemigoS.i++)
		{
			DisparoEnemigo_update(
					(DisparoEnemigo*) Lista_RetornaElemento(
							&((*enemigo).disparosSid), EnemigoS.i));
		}
	}

	//*********************************
	for (EnemigoS.i = 0; EnemigoS.i < (*enemigo).disparosSid.size; EnemigoS.i++)
	{
		if ((*(DisparoEnemigo*) Lista_RetornaElemento(&((*enemigo).disparosSid),
				EnemigoS.i)).estaEnColision == 1)
		{
			(*(DisparoEnemigo*) Lista_RetornaElemento(&((*enemigo).disparosSid),
					EnemigoS.i)).estaEnColision = 0;
			if ((*Animation_getSObject()).animacionesDispANaveTotal.size > 0)
			{
				Animation_setPosition(
						(Animation*) Lista_RetornaElemento(
								&((*Animation_getSObject()).animacionesDispANaveTotal),
								(*Animation_getSObject()).animacionesDispANaveTotal.size
										- 1),
						(*(DisparoEnemigo*) Lista_RetornaElemento(
								&((*enemigo).disparosSid), EnemigoS.i)).Position);

				Lista_InsertaEnFinal(&((*enemigo).animaciones),
						(*enemigo).animaciones.fin,
						Lista_RetornaElemento(
								&((*Animation_getSObject()).animacionesDispANaveTotal),
								(*Animation_getSObject()).animacionesDispANaveTotal.size
										- 1));

				Lista_RemoveAt(
						&((*Animation_getSObject()).animacionesDispANaveTotal),
						(*Animation_getSObject()).animacionesDispANaveTotal.size
								- 1);

			}

			Lista_AddInFin((&(EnemigoS.disparosSidTotal)),
					Lista_GetIn(&((*enemigo).disparosSid), EnemigoS.i));

			Lista_RemoveAt(&((*enemigo).disparosSid), EnemigoS.i);
		}
	}
	//************************************

	for (EnemigoS.i = 0; EnemigoS.i < (*enemigo).disparosSid.size; EnemigoS.i++)
	{
		if ((*(DisparoEnemigo*) Lista_GetIn(&((*enemigo).disparosSid),
				EnemigoS.i)).Position.X > 600
				|| (*(DisparoEnemigo*) Lista_GetIn(&((*enemigo).disparosSid),
						EnemigoS.i)).Position.X < 0)
		{
			Lista_AddInFin(&(EnemigoS.disparosSidTotal),
					Lista_GetIn(&((*enemigo).disparosSid), EnemigoS.i));

			Lista_RemoveAt(&((*enemigo).disparosSid), EnemigoS.i);

		}
	}

}

//draw disparos de Sid
void Enemigo_drawDisparosSid(Enemigo* enemigo)
{
	for (EnemigoS.i = 0; EnemigoS.i < (*enemigo).disparosSid.size; EnemigoS.i++)
	{
		DisparoEnemigo_draw(
				(DisparoEnemigo*) Lista_GetIn(&((*enemigo).disparosSid),
						EnemigoS.i));
	}
}

void Enemigo_defineCollitionBoxSid()
{
	EnemigoS.radioCollitionSid = (int) (40 * 0.35f);

	EnemigoS.collitionXSid[0] = (int) (-25 * 0.85f);
	EnemigoS.collitionYSid[0] = (int) (-11 * 0.85f);
	EnemigoS.collitionXSid[1] = (int) (4 * 0.85f);
	EnemigoS.collitionYSid[1] = (int) (-11 * 0.85f);
	EnemigoS.collitionXSid[2] = (int) (-25 * 0.85f);
	EnemigoS.collitionYSid[2] = (int) (15 * 0.85f);
	EnemigoS.collitionXSid[3] = (int) (4 * 0.85f);
	EnemigoS.collitionYSid[3] = (int) (14 * 0.85f);
	EnemigoS.collitionXSid[4] = (int) (31 * 0.85f);
	EnemigoS.collitionYSid[4] = (int) (1 * 0.85f);
}

void Enemigo_defineCollitionBoxHiSid()
{
	EnemigoS.radioCollitionHiSid = (int) (40 * 0.70f);

	EnemigoS.collitionXHiSid[0] = (int) (-25 * 1.2f);
	EnemigoS.collitionYHiSid[0] = (int) (-11 * 1.2f);
	EnemigoS.collitionXHiSid[1] = (int) (4 * 1.2f);
	EnemigoS.collitionYHiSid[1] = (int) (-11 * 1.2f);
	EnemigoS.collitionXHiSid[2] = (int) (-25 * 1.2f);
	EnemigoS.collitionYHiSid[2] = (int) (15 * 1.2f);
	EnemigoS.collitionXHiSid[3] = (int) (4 * 1.2f);
	EnemigoS.collitionYHiSid[3] = (int) (14 * 1.2f);
	EnemigoS.collitionXHiSid[4] = (int) (31 * 1.2f);
	EnemigoS.collitionYHiSid[4] = (int) (1 * 1.2f);
}

//**************Fin Inteligencia de sid*****************

//********** Update Animaciones *************

void Enemigo_updateAnimaciones(Enemigo* enemigo)
{
	if ((*enemigo).animaciones.size > 0)
	{
		for (EnemigoS.i = 0; EnemigoS.i < (*enemigo).animaciones.size;
				EnemigoS.i++)
		{
			Animation_update(
					(Animation*) Lista_GetIn(&((*enemigo).animaciones),
							EnemigoS.i));
		}
	}
	if ((*enemigo).animaciones.size > 0)
	{
		for (EnemigoS.i = 0; EnemigoS.i < (*enemigo).animaciones.size;
				EnemigoS.i++)
		{
			if ((*(Animation*) Lista_GetIn(&((*enemigo).animaciones),
					EnemigoS.i)).animacionFinalizada == 1
					&& (*(Animation*) Lista_GetIn(&((*enemigo).animaciones),
							EnemigoS.i)).tipoActual == ADisparoANave)
			{
				(*(Animation*) Lista_GetIn(&((*enemigo).animaciones),
						EnemigoS.i)).animacionFinalizada = 0;

				Lista_AddInFin(
						&((*Animation_getSObject()).animacionesDispANaveTotal),
						Lista_GetIn(&((*enemigo).animaciones), EnemigoS.i));

				Lista_RemoveAt(&((*enemigo).animaciones), EnemigoS.i);
			}
		}
	}
}

//***************** Draw animaciones ***********

void Enemigo_drawAnimaciones(Enemigo* enemigo)
{
	if ((*enemigo).animaciones.size > 0)
	{
		for (EnemigoS.i = 0; EnemigoS.i < (*enemigo).animaciones.size;
				EnemigoS.i++)
		{
			Animation_draw(
					(Animation*) Lista_GetIn(&((*enemigo).animaciones),
							EnemigoS.i));
		}
	}
}

//*Definicion de las texturas de los enemigos
void Enemigo_setTextura()
{
	Lista_Inicia(&(EnemigoS.Texturas));
	Lista_InsertaEnFinal(&(EnemigoS.Texturas), EnemigoS.Texturas.fin,
			Textura_getTextura("ENor")); //0
	Lista_InsertaEnFinal(&(EnemigoS.Texturas), EnemigoS.Texturas.fin,
			Textura_getTextura("EKami")); //1
	Lista_InsertaEnFinal(&(EnemigoS.Texturas), EnemigoS.Texturas.fin,
			Textura_getTextura("ESid")); //2
}

//*Definicion listas de disparos disparos
void Enemigo_defineListasDeDisparos()
{
	//**disparos para todos los enemigos tipoSid
	Lista_Inicia(&(EnemigoS.disparosSidTotal));
	//disparos para todos los enemigos Tipo Nor
	Lista_Inicia(&(EnemigoS.disparosNorTotal));
	for (EnemigoS.i = 0; EnemigoS.i < 300; EnemigoS.i++)
	{
		Lista_InsertaEnFinal(&(EnemigoS.disparosSidTotal),
				EnemigoS.disparosSidTotal.fin,
				DisparoEnemigo_CrearObject(GameFramework_Vector2(0, 0),
						DEdispSid, 1));
		Lista_InsertaEnFinal(&(EnemigoS.disparosNorTotal),
				EnemigoS.disparosNorTotal.fin,
				DisparoEnemigo_CrearObject(GameFramework_Vector2(0, 0),
						DEdispNor, 1));
	}
}

// Confirma colisiones
void Enemigo_confirmaColisiones(Collition* colision, Enemigo* enemigo,
		int danoNave)
{
	if (Collition_colisionDispAEnemigo(colision, enemigo,
			(*enemigo).tipoEnemigo) == 1)
	{
		(*enemigo).estaEnColisionDisp = 1;
		if ((*Collition_getSObject()).ultimaColisionTipoDisparo != Dsuper)
		{
			(*enemigo).vida = (*enemigo).vida
					- (5 + (*Nave_getSObject()).masPoderDisp);
		}
		else
		{
			(*enemigo).vida = (*enemigo).vida
					- (10 + (*Nave_getSObject()).masPoderDisp);
		}
	}

	if (Collition_colisionNaveAEnemigo(colision, enemigo) == 1)
	{
		(*enemigo).estaEnColisionNave = 1;
		(*Nave_getSObject()).Vida = (*Nave_getSObject()).Vida
				- (danoNave + (*enemigo).adicionDanoNave);
		(*enemigo).vida = 0;
	}

	switch ((*Nave_getSObject()).TexturaNave)
	{
	case 0:
		if (Collition_colisionPoderGalacticaAEnemigo(colision, enemigo,
				(*enemigo).tipoEnemigo) == 1)
		{
			(*enemigo).estaEnColisionPoder = 1;
			(*enemigo).vida = (*enemigo).vida - 50;
		}
		break;
	case 1:
		if (Collition_colisionPoderCarafeAEnemigo(colision, enemigo,
				(*enemigo).tipoEnemigo) == 1)
		{
			(*enemigo).estaEnColisionPoder = 1;
			(*enemigo).vida = (*enemigo).vida - 30;
		}
		break;
	case 2:
		if (Collition_colisionPoderHelixAEnemigo(colision, enemigo,
				(*enemigo).tipoEnemigo) == 1)
		{
			(*enemigo).estaEnColisionPoder = 1;
			(*enemigo).vida = (*enemigo).vida - 1500;
		}
		break;
	case 3:
		if (Collition_colisionPoderArpAEnemigo(colision, enemigo,
				(*enemigo).tipoEnemigo) == 1)
		{
			(*enemigo).estaEnColisionPoder = 1;
			(*enemigo).vida = (*enemigo).vida - 8000;
		}
		break;
	case 4:
		if (Collition_colisionPoderPerseusAEnemigo(colision, enemigo,
				(*enemigo).tipoEnemigo) == 1)
		{
			(*enemigo).estaEnColisionPoder = 1;
			(*enemigo).vida = (*enemigo).vida - GameFramework_Rand(1000, 5000);
		}
		break;
	case 5:
		if (Collition_colisionPoderPiscesAEnemigo(colision, enemigo,
				(*enemigo).tipoEnemigo) == 1)
		{
			(*enemigo).estaEnColisionPoder = 1;
			(*enemigo).vida = (*enemigo).vida - 80;
		}
		break;
	}

}

Enemigo* Enemigo_getSObject()
{
	return &EnemigoS;
}

