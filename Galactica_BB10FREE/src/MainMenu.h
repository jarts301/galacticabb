/*
 * MainMenu.h
 *
 *  Created on: 20/02/2013
 *      Author: Jarts
 */

#ifndef MAINMENU_H_
#define MAINMENU_H_

#include "GalacticaTipos.h"

void MainMenu_load();
void MainMenu_update();
void MainMenu_draw();
void MainMenu_loadTitulo();
void MainMenu_drawTitulo();
void MainMenu_setTexturas();
MainMenu* MainMenu_getSObject();

#endif /* MAINMENU_H_ */
