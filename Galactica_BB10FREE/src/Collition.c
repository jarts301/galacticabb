/*
 * Collition.c
 *
 *  Created on: 18/02/2013
 *      Author: Jarts
 */
#include "Collition.h"
#include "Lista.h"
#include "Plus.h"
#include "Nave.h"
#include "Poderes.h"
#include "Audio.h"
#include "Enemigo.h"
#include "Disparo.h"
#include "DisparoEnemigo.h"
#include "DisparoJefe.h"
#include "NMini.h"
#include <math.h>

static Collition CollitionS;

//**Colision Plus Nave
//retorna boolean
int Collition_colisionPlusANave(Collition* collition, Plus* plus)
{
	for (CollitionS.i = 0; CollitionS.i < 11; CollitionS.i++)
	{
		CollitionS.distancia =
				(float) sqrt(
						pow(
								(*plus).Position.X
										- ((*Nave_getSObject()).PositionNav.X
												+ (*Nave_getSObject()).collitionX[CollitionS.i]),
								2)
								+ pow(
										(*plus).Position.Y
												- ((*Nave_getSObject()).PositionNav.Y
														+ (*Nave_getSObject()).collitionY[CollitionS.i]),
										2));

		if (CollitionS.distancia
				<= (*plus).radioCollition
						+ (*Nave_getSObject()).radioCollitionN)
		{
			Audio_playAudio("Estrella");
			return 1;
		}
	}
	return 0;
}

//*Colision disparo a enemigo
int Collition_colisionDispAEnemigo(Collition* collition, Enemigo* enemigo,
		TipoEnemigo tipoEnemigo)
{
	if (tipoEnemigo == ENor && (*Nave_getSObject()).disparos.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).disparos.size;
				CollitionS.i++)
		{
			CollitionS.distancia = (float) sqrt(
					pow(
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionS.i)).Position.X
									- (*enemigo).Position.X, 2)
							+ pow(
									(*(Disparo*) Lista_RetornaElemento(
											&((*Nave_getSObject()).disparos),
											CollitionS.i)).Position.Y
											- (*enemigo).Position.Y, 2));

			if (CollitionS.distancia
					<= (*(Disparo*) Lista_RetornaElemento(
							&((*Nave_getSObject()).disparos), CollitionS.i)).radioCollition
							+ (*Enemigo_getSObject()).radioCollitionNor)
			{
				Collition_confirmaEliminaDisparoNave(CollitionS.i);
				return 1;
			}

		}
	}
	else if (tipoEnemigo == EKami && (*Nave_getSObject()).disparos.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).disparos.size;
				CollitionS.i++)
		{
			CollitionS.distancia = (float) sqrt(
					pow(
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionS.i)).Position.X
									- (*enemigo).Position.X, 2)
							+ pow(
									(*(Disparo*) Lista_RetornaElemento(
											&((*Nave_getSObject()).disparos),
											CollitionS.i)).Position.Y
											- (*enemigo).Position.Y, 2));

			if (CollitionS.distancia
					<= (*(Disparo*) Lista_RetornaElemento(
							&((*Nave_getSObject()).disparos), CollitionS.i)).radioCollition
							+ (*Enemigo_getSObject()).radioCollitionKami)
			{
				Collition_confirmaEliminaDisparoNave(CollitionS.i);
				return 1;
			}
		}
	}
	else if (tipoEnemigo == ESid && (*Nave_getSObject()).disparos.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).disparos.size;
				CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 5; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										(*(Disparo*) Lista_RetornaElemento(
												&((*Nave_getSObject()).disparos),
												CollitionS.i)).Position.X
												- ((*enemigo).Position.X
														+ (*Enemigo_getSObject()).collitionXSid[CollitionS.j]),
										2)
										+ pow(
												(*(Disparo*) Lista_RetornaElemento(
														&((*Nave_getSObject()).disparos),
														CollitionS.i)).Position.Y
														- ((*enemigo).Position.Y
																+ (*Enemigo_getSObject()).collitionYSid[CollitionS.j]),
												2));

				if (CollitionS.distancia
						<= (*(Disparo*) Lista_RetornaElemento(
								&((*Nave_getSObject()).disparos), CollitionS.i)).radioCollition
								+ (*Enemigo_getSObject()).radioCollitionSid)
				{
					Collition_confirmaEliminaDisparoNave(CollitionS.i);
					return 1;
				}
			}
		}
	}
	else if (tipoEnemigo == EHiNor && (*Nave_getSObject()).disparos.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).disparos.size;
				CollitionS.i++)
		{
			CollitionS.distancia = (float) sqrt(
					pow(
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionS.i)).Position.X
									- (*enemigo).Position.X, 2)
							+ pow(
									(*(Disparo*) Lista_RetornaElemento(
											&((*Nave_getSObject()).disparos),
											CollitionS.i)).Position.Y
											- (*enemigo).Position.Y, 2));

			if (CollitionS.distancia
					<= (*(Disparo*) Lista_RetornaElemento(
							&((*Nave_getSObject()).disparos), CollitionS.i)).radioCollition
							+ (*Enemigo_getSObject()).radioCollitionHiNor)
			{
				Collition_confirmaEliminaDisparoNave(CollitionS.i);
				return 1;
			}
		}
	}
	else if (tipoEnemigo == EHiKami && (*Nave_getSObject()).disparos.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).disparos.size;
				CollitionS.i++)
		{
			CollitionS.distancia = (float) sqrt(
					pow(
							(*(Disparo*) Lista_RetornaElemento(
									&((*Nave_getSObject()).disparos),
									CollitionS.i)).Position.X
									- (*enemigo).Position.X, 2)
							+ pow(
									(*(Disparo*) Lista_RetornaElemento(
											&((*Nave_getSObject()).disparos),
											CollitionS.i)).Position.Y
											- (*enemigo).Position.Y, 2));

			if (CollitionS.distancia
					<= (*(Disparo*) Lista_RetornaElemento(
							&((*Nave_getSObject()).disparos), CollitionS.i)).radioCollition
							+ (*Enemigo_getSObject()).radioCollitionHiKami)
			{
				Collition_confirmaEliminaDisparoNave(CollitionS.i);
				return 1;
			}
		}
	}
	else if (tipoEnemigo == EHiSid && (*Nave_getSObject()).disparos.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).disparos.size;
				CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 5; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										(*(Disparo*) Lista_RetornaElemento(
												&((*Nave_getSObject()).disparos),
												CollitionS.i)).Position.X
												- ((*enemigo).Position.X
														+ (*Enemigo_getSObject()).collitionXHiSid[CollitionS.j]),
										2)
										+ pow(
												(*(Disparo*) Lista_RetornaElemento(
														&((*Nave_getSObject()).disparos),
														CollitionS.i)).Position.Y
														- ((*enemigo).Position.Y
																+ (*Enemigo_getSObject()).collitionYHiSid[CollitionS.j]),
												2));

				if (CollitionS.distancia
						<= (*(Disparo*) Lista_RetornaElemento(
								&((*Nave_getSObject()).disparos), CollitionS.i)).radioCollition
								+ (*Enemigo_getSObject()).radioCollitionHiSid)
				{
					Collition_confirmaEliminaDisparoNave(CollitionS.i);
					return 1;
				}
			}
		}
	}

	return 0;
}

//*Colision PoderGalactica a enemigo
int Collition_colisionPoderGalacticaAEnemigo(Collition* collition,
		Enemigo* enemigo, TipoEnemigo tipoEnemigo)
{

	if (tipoEnemigo == ENor && (*Nave_getSObject()).poderGalactica.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderGalactica.size;
				CollitionS.i++)
		{
			CollitionS.distancia =
					(float) sqrt(
							pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderGalactica),
											CollitionS.i)).Position.X
											- (*enemigo).Position.X, 2)
									+ pow(
											(*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderGalactica),
													CollitionS.i)).Position.Y
													- (*enemigo).Position.Y,
											2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPGalactica
							+ (*Enemigo_getSObject()).radioCollitionNor)
			{
				Lista_InsertaEnFinal(
						&((*Nave_getSObject()).poderGalacticaTotal),
						(*Nave_getSObject()).poderGalacticaTotal.fin,
						Lista_RetornaElemento(
								&((*Nave_getSObject()).poderGalactica),
								CollitionS.i));
				Lista_RemoveAt(&((*Nave_getSObject()).poderGalactica),
						CollitionS.i);
				return 1;
			}
		}
	}
	else if (tipoEnemigo == EKami
			&& (*Nave_getSObject()).poderGalactica.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderGalactica.size;
				CollitionS.i++)
		{
			CollitionS.distancia =
					(float) sqrt(
							pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderGalactica),
											CollitionS.i)).Position.X
											- (*enemigo).Position.X, 2)
									+ pow(
											(*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderGalactica),
													CollitionS.i)).Position.Y
													- (*enemigo).Position.Y,
											2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPGalactica
							+ (*Enemigo_getSObject()).radioCollitionKami)
			{
				Lista_InsertaEnFinal(
						&((*Nave_getSObject()).poderGalacticaTotal),
						(*Nave_getSObject()).poderGalacticaTotal.fin,
						Lista_RetornaElemento(
								&((*Nave_getSObject()).poderGalactica),
								CollitionS.i));
				Lista_RemoveAt(&((*Nave_getSObject()).poderGalactica),
						CollitionS.i);
				return 1;
			}
		}
	}
	else if (tipoEnemigo == ESid
			&& (*Nave_getSObject()).poderGalactica.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderGalactica.size;
				CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 5; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderGalactica),
												CollitionS.i)).Position.X
												- ((*enemigo).Position.X
														+ (*Enemigo_getSObject()).collitionXSid[CollitionS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderGalactica),
														CollitionS.i)).Position.Y
														- ((*enemigo).Position.Y
																+ (*Enemigo_getSObject()).collitionYSid[CollitionS.j]),
												2));

				if (CollitionS.distancia
						<= (*Poderes_getSObject()).radioCollitPGalactica
								+ (*Enemigo_getSObject()).radioCollitionSid)
				{
					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderGalacticaTotal),
							(*Nave_getSObject()).poderGalacticaTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionS.i));
					Lista_RemoveAt(&((*Nave_getSObject()).poderGalactica),
							CollitionS.i);
					return 1;
				}
			}
		}
	}
	else if (tipoEnemigo == EHiNor
			&& (*Nave_getSObject()).poderGalactica.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderGalactica.size;
				CollitionS.i++)
		{
			CollitionS.distancia =
					(float) sqrt(
							pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderGalactica),
											CollitionS.i)).Position.X
											- (*enemigo).Position.X, 2)
									+ pow(
											(*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderGalactica),
													CollitionS.i)).Position.Y
													- (*enemigo).Position.Y,
											2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPGalactica
							+ (*Enemigo_getSObject()).radioCollitionHiNor)
			{
				Lista_InsertaEnFinal(
						&((*Nave_getSObject()).poderGalacticaTotal),
						(*Nave_getSObject()).poderGalacticaTotal.fin,
						Lista_RetornaElemento(
								&((*Nave_getSObject()).poderGalactica),
								CollitionS.i));
				Lista_RemoveAt(&((*Nave_getSObject()).poderGalactica),
						CollitionS.i);
				return 1;
			}

		}
	}
	else if (tipoEnemigo == EHiKami
			&& (*Nave_getSObject()).poderGalactica.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderGalactica.size;
				CollitionS.i++)
		{
			CollitionS.distancia =
					(float) sqrt(
							pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderGalactica),
											CollitionS.i)).Position.X
											- (*enemigo).Position.X, 2)
									+ pow(
											(*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderGalactica),
													CollitionS.i)).Position.Y
													- (*enemigo).Position.Y,
											2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPGalactica
							+ (*Enemigo_getSObject()).radioCollitionHiKami)
			{
				Lista_InsertaEnFinal(
						&((*Nave_getSObject()).poderGalacticaTotal),
						(*Nave_getSObject()).poderGalacticaTotal.fin,
						Lista_RetornaElemento(
								&((*Nave_getSObject()).poderGalactica),
								CollitionS.i));
				Lista_RemoveAt(&((*Nave_getSObject()).poderGalactica),
						CollitionS.i);
				return 1;
			}
		}
	}
	else if (tipoEnemigo == EHiSid
			&& (*Nave_getSObject()).poderGalactica.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderGalactica.size;
				CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 5; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderGalactica),
												CollitionS.i)).Position.X
												- ((*enemigo).Position.X
														+ (*Enemigo_getSObject()).collitionXHiSid[CollitionS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderGalactica),
														CollitionS.i)).Position.Y
														- ((*enemigo).Position.Y
																+ (*Enemigo_getSObject()).collitionYHiSid[CollitionS.j]),
												2));

				if (CollitionS.distancia
						<= (*Poderes_getSObject()).radioCollitPGalactica
								+ (*Enemigo_getSObject()).radioCollitionHiSid)
				{
					Lista_InsertaEnFinal(
							&((*Nave_getSObject()).poderGalacticaTotal),
							(*Nave_getSObject()).poderGalacticaTotal.fin,
							Lista_RetornaElemento(
									&((*Nave_getSObject()).poderGalactica),
									CollitionS.i));
					Lista_RemoveAt(&((*Nave_getSObject()).poderGalactica),
							CollitionS.i);
					return 1;
				}
			}
		}
	}

	return 0;
}

//*Colision PoderCarafe a enemigo
int Collition_colisionPoderCarafeAEnemigo(Collition* collition,
		Enemigo* enemigo, TipoEnemigo tipoEnemigo)
{
	if (tipoEnemigo == ENor && (*Nave_getSObject()).poderCarafe.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderCarafe.size;
				CollitionS.i++)
		{
			CollitionS.distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionS.i)).Position.X
									- (*enemigo).Position.X, 2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderCarafe),
											CollitionS.i)).Position.Y
											- (*enemigo).Position.Y, 2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPCarafe
							+ (*Enemigo_getSObject()).radioCollitionNor)
			{
				Collition_confirmaEliminaPoderCarafe(CollitionS.i);
				return 1;
			}
		}
	}
	else if (tipoEnemigo == EKami && (*Nave_getSObject()).poderCarafe.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderCarafe.size;
				CollitionS.i++)
		{
			CollitionS.distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionS.i)).Position.X
									- (*enemigo).Position.X, 2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderCarafe),
											CollitionS.i)).Position.Y
											- (*enemigo).Position.Y, 2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPCarafe
							+ (*Enemigo_getSObject()).radioCollitionKami)
			{
				Collition_confirmaEliminaPoderCarafe(CollitionS.i);
				return 1;
			}
		}
	}
	else if (tipoEnemigo == ESid && (*Nave_getSObject()).poderCarafe.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderCarafe.size;
				CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 5; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderCarafe),
												CollitionS.i)).Position.X
												- ((*enemigo).Position.X
														+ (*Enemigo_getSObject()).collitionXSid[CollitionS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderCarafe),
														CollitionS.i)).Position.Y
														- ((*enemigo).Position.Y
																+ (*Enemigo_getSObject()).collitionYSid[CollitionS.j]),
												2));

				if (CollitionS.distancia
						<= (*Poderes_getSObject()).radioCollitPCarafe
								+ (*Enemigo_getSObject()).radioCollitionSid)
				{
					Collition_confirmaEliminaPoderCarafe(CollitionS.i);
					return 1;
				}
			}
		}
	}
	else if (tipoEnemigo == EHiNor && (*Nave_getSObject()).poderCarafe.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderCarafe.size;
				CollitionS.i++)
		{
			CollitionS.distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionS.i)).Position.X
									- (*enemigo).Position.X, 2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderCarafe),
											CollitionS.i)).Position.Y
											- (*enemigo).Position.Y, 2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPCarafe
							+ (*Enemigo_getSObject()).radioCollitionHiNor)
			{
				Collition_confirmaEliminaPoderCarafe(CollitionS.i);
				return 1;
			}
		}
	}
	else if (tipoEnemigo == EHiKami
			&& (*Nave_getSObject()).poderCarafe.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderCarafe.size;
				CollitionS.i++)
		{
			CollitionS.distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderCarafe),
									CollitionS.i)).Position.X
									- (*enemigo).Position.X, 2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderCarafe),
											CollitionS.i)).Position.Y
											- (*enemigo).Position.Y, 2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPCarafe
							+ (*Enemigo_getSObject()).radioCollitionHiKami)
			{
				Collition_confirmaEliminaPoderCarafe(CollitionS.i);
				return 1;
			}
		}
	}
	else if (tipoEnemigo == EHiSid && (*Nave_getSObject()).poderCarafe.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderCarafe.size;
				CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 5; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderCarafe),
												CollitionS.i)).Position.X
												- ((*enemigo).Position.X
														+ (*Enemigo_getSObject()).collitionXHiSid[CollitionS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderCarafe),
														CollitionS.i)).Position.Y
														- ((*enemigo).Position.Y
																+ (*Enemigo_getSObject()).collitionYHiSid[CollitionS.j]),
												2));

				if (CollitionS.distancia
						<= (*Poderes_getSObject()).radioCollitPCarafe
								+ (*Enemigo_getSObject()).radioCollitionHiSid)
				{
					Collition_confirmaEliminaPoderCarafe(CollitionS.i);
					return 1;
				}
			}
		}
	}
	return 0;
}

//Colision Poder Helix aEnemigo
int Collition_colisionPoderHelixAEnemigo(Collition* collition, Enemigo* enemigo,
		TipoEnemigo tipoEnemigo)
{
	if (tipoEnemigo == ENor && (*Nave_getSObject()).poderHelix.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderHelix.size;
				CollitionS.i++)
		{
			CollitionS.distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionS.i)).Position.X
									- (*enemigo).Position.X, 2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderHelix),
											CollitionS.i)).Position.Y
											- (*enemigo).Position.Y, 2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPHelix
							+ (*Enemigo_getSObject()).radioCollitionNor)
			{
				return 1;
			}
		}
	}
	else if (tipoEnemigo == EKami && (*Nave_getSObject()).poderHelix.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderHelix.size;
				CollitionS.i++)
		{
			CollitionS.distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionS.i)).Position.X
									- (*enemigo).Position.X, 2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderHelix),
											CollitionS.i)).Position.Y
											- (*enemigo).Position.Y, 2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPHelix
							+ (*Enemigo_getSObject()).radioCollitionKami)
			{
				return 1;
			}
		}
	}
	else if (tipoEnemigo == ESid && (*Nave_getSObject()).poderHelix.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderHelix.size;
				CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 5; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderHelix),
												CollitionS.i)).Position.X
												- ((*enemigo).Position.X
														+ (*Enemigo_getSObject()).collitionXSid[CollitionS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderHelix),
														CollitionS.i)).Position.Y
														- ((*enemigo).Position.Y
																+ (*Enemigo_getSObject()).collitionYSid[CollitionS.j]),
												2));

				if (CollitionS.distancia
						<= (*Poderes_getSObject()).radioCollitPHelix
								+ (*Enemigo_getSObject()).radioCollitionSid)
				{
					return 1;
				}
			}
		}
	}
	else if (tipoEnemigo == EHiNor && (*Nave_getSObject()).poderHelix.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderHelix.size;
				CollitionS.i++)
		{
			CollitionS.distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionS.i)).Position.X
									- (*enemigo).Position.X, 2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderHelix),
											CollitionS.i)).Position.Y
											- (*enemigo).Position.Y, 2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPHelix
							+ (*Enemigo_getSObject()).radioCollitionHiNor)
			{
				return 1;
			}
		}
	}
	else if (tipoEnemigo == EHiKami && (*Nave_getSObject()).poderHelix.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderHelix.size;
				CollitionS.i++)
		{
			CollitionS.distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderHelix),
									CollitionS.i)).Position.X
									- (*enemigo).Position.X, 2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderHelix),
											CollitionS.i)).Position.Y
											- (*enemigo).Position.Y, 2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPHelix
							+ (*Enemigo_getSObject()).radioCollitionHiKami)
			{
				return 1;
			}
		}
	}
	else if (tipoEnemigo == EHiSid && (*Nave_getSObject()).poderHelix.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderHelix.size;
				CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 5; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderHelix),
												CollitionS.i)).Position.X
												- ((*enemigo).Position.X
														+ (*Enemigo_getSObject()).collitionXHiSid[CollitionS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderHelix),
														CollitionS.i)).Position.Y
														- ((*enemigo).Position.Y
																+ (*Enemigo_getSObject()).collitionYHiSid[CollitionS.j]),
												2));

				if (CollitionS.distancia
						<= (*Poderes_getSObject()).radioCollitPHelix
								+ (*Enemigo_getSObject()).radioCollitionHiSid)
				{
					return 1;
				}
			}
		}
	}
	return 0;
}

//******Colision poder Arp a enemigo
int Collition_colisionPoderArpAEnemigo(Collition* collition, Enemigo* enemigo,
		TipoEnemigo tipoEnemigo)
{
	if (tipoEnemigo == ENor && (*Nave_getSObject()).poderArp.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderArp.size;
				CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 6; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										((*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderArp),
												CollitionS.i)).Position.X
												+ (*Poderes_getSObject()).collitionPArpX[CollitionS.j])
												- (*enemigo).Position.X, 2)
										+ pow(
												((*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderArp),
														CollitionS.i)).Position.Y
														+ (*Poderes_getSObject()).collitionPArpY[CollitionS.j])
														- (*enemigo).Position.Y,
												2));

				if (CollitionS.distancia
						<= (*Poderes_getSObject()).radioCollitPArp
								+ (*Enemigo_getSObject()).radioCollitionNor)
				{
					return 1;
				}
			}
		}
	}
	else if (tipoEnemigo == EKami && (*Nave_getSObject()).poderArp.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderArp.size;
				CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 6; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										((*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderArp),
												CollitionS.i)).Position.X
												+ (*Poderes_getSObject()).collitionPArpX[CollitionS.j])
												- (*enemigo).Position.X, 2)
										+ pow(
												((*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderArp),
														CollitionS.i)).Position.Y
														+ (*Poderes_getSObject()).collitionPArpY[CollitionS.j])
														- (*enemigo).Position.Y,
												2));

				if (CollitionS.distancia
						<= (*Poderes_getSObject()).radioCollitPArp
								+ (*Enemigo_getSObject()).radioCollitionKami)
				{
					return 1;
				}
			}
		}
	}
	else if (tipoEnemigo == ESid && (*Nave_getSObject()).poderArp.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderArp.size;
				CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 6; CollitionS.j++)
			{
				for (CollitionS.w = 0; CollitionS.w < 5; CollitionS.w++)
				{
					CollitionS.distancia =
							(float) sqrt(
									pow(
											((*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderArp),
													CollitionS.i)).Position.X
													+ (*Poderes_getSObject()).collitionPArpX[CollitionS.j])
													- ((*enemigo).Position.X
															+ (*Enemigo_getSObject()).collitionXSid[CollitionS.w]),
											2)
											+ pow(
													((*(Poderes*) Lista_RetornaElemento(
															&((*Nave_getSObject()).poderArp),
															CollitionS.i)).Position.Y
															+ (*Poderes_getSObject()).collitionPArpY[CollitionS.j])
															- ((*enemigo).Position.Y
																	+ (*Enemigo_getSObject()).collitionYSid[CollitionS.w]),
													2));

					if (CollitionS.distancia
							<= (*Poderes_getSObject()).radioCollitPArp
									+ (*Enemigo_getSObject()).radioCollitionSid)
					{
						return 1;
					}
				}
			}
		}
	}
	else if (tipoEnemigo == EHiNor && (*Nave_getSObject()).poderArp.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderArp.size;
				CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 6; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										((*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderArp),
												CollitionS.i)).Position.X
												+ (*Poderes_getSObject()).collitionPArpX[CollitionS.j])
												- (*enemigo).Position.X, 2)
										+ pow(
												((*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderArp),
														CollitionS.i)).Position.Y
														+ (*Poderes_getSObject()).collitionPArpY[CollitionS.j])
														- (*enemigo).Position.Y,
												2));

				if (CollitionS.distancia
						<= (*Poderes_getSObject()).radioCollitPArp
								+ (*Enemigo_getSObject()).radioCollitionHiNor)
				{
					return 1;
				}
			}
		}
	}
	else if (tipoEnemigo == EHiKami && (*Nave_getSObject()).poderArp.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderArp.size;
				CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 6; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										((*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderArp),
												CollitionS.i)).Position.X
												+ (*Poderes_getSObject()).collitionPArpX[CollitionS.j])
												- (*enemigo).Position.X, 2)
										+ pow(
												((*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderArp),
														CollitionS.i)).Position.Y
														+ (*Poderes_getSObject()).collitionPArpY[CollitionS.j])
														- (*enemigo).Position.Y,
												2));

				if (CollitionS.distancia
						<= (*Poderes_getSObject()).radioCollitPArp
								+ (*Enemigo_getSObject()).radioCollitionHiKami)
				{
					return 1;
				}
			}
		}
	}
	else if (tipoEnemigo == EHiSid && (*Nave_getSObject()).poderArp.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderArp.size;
				CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 6; CollitionS.j++)
			{
				for (CollitionS.w = 0; CollitionS.w < 5; CollitionS.w++)
				{
					CollitionS.distancia =
							(float) sqrt(
									pow(
											((*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderArp),
													CollitionS.i)).Position.X
													+ (*Poderes_getSObject()).collitionPArpX[CollitionS.j])
													- ((*enemigo).Position.X
															+ (*Enemigo_getSObject()).collitionXHiSid[CollitionS.w]),
											2)
											+ pow(
													((*(Poderes*) Lista_RetornaElemento(
															&((*Nave_getSObject()).poderArp),
															CollitionS.i)).Position.Y
															+ (*Poderes_getSObject()).collitionPArpY[CollitionS.j])
															- ((*enemigo).Position.Y
																	+ (*Enemigo_getSObject()).collitionYHiSid[CollitionS.w]),
													2));

					if (CollitionS.distancia
							<= (*Poderes_getSObject()).radioCollitPArp
									+ (*Enemigo_getSObject()).radioCollitionHiSid)
					{
						return 1;
					}
				}
			}
		}
	}

	return 0;
}

//*****Colision Poder Perseus a enemigo
int Collition_colisionPoderPerseusAEnemigo(Collition* collition, Enemigo* enemigo,
		TipoEnemigo tipoEnemigo)
{
	if (tipoEnemigo == ENor && (*Nave_getSObject()).poderPerseus.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderPerseus.size;
				CollitionS.i++)
		{
			CollitionS.distancia =
					(float) sqrt(
							pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderPerseus),
											CollitionS.i)).Position.X
											- (*enemigo).Position.X, 2)
									+ pow(
											(*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderPerseus),
													CollitionS.i)).Position.Y
													- (*enemigo).Position.Y,
											2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPPerseus
							+ (*Enemigo_getSObject()).radioCollitionNor)
			{
				return 1;
			}
		}
	}
	else if (tipoEnemigo == EKami && (*Nave_getSObject()).poderPerseus.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderPerseus.size;
				CollitionS.i++)
		{
			CollitionS.distancia =
					(float) sqrt(
							pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderPerseus),
											CollitionS.i)).Position.X
											- (*enemigo).Position.X, 2)
									+ pow(
											(*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderPerseus),
													CollitionS.i)).Position.Y
													- (*enemigo).Position.Y,
											2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPPerseus
							+ (*Enemigo_getSObject()).radioCollitionKami)
			{
				return 1;
			}
		}
	}
	else if (tipoEnemigo == ESid && (*Nave_getSObject()).poderPerseus.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderPerseus.size;
				CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 5; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderPerseus),
												CollitionS.i)).Position.X
												- ((*enemigo).Position.X
														+ (*Enemigo_getSObject()).collitionXSid[CollitionS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderPerseus),
														CollitionS.i)).Position.Y
														- ((*enemigo).Position.Y
																+ (*Enemigo_getSObject()).collitionYSid[CollitionS.j]),
												2));

				if (CollitionS.distancia
						<= (*Poderes_getSObject()).radioCollitPPerseus
								+ (*Enemigo_getSObject()).radioCollitionSid)
				{
					return 1;
				}
			}
		}
	}
	else if (tipoEnemigo == EHiNor
			&& (*Nave_getSObject()).poderPerseus.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderPerseus.size;
				CollitionS.i++)
		{
			CollitionS.distancia =
					(float) sqrt(
							pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderPerseus),
											CollitionS.i)).Position.X
											- (*enemigo).Position.X, 2)
									+ pow(
											(*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderPerseus),
													CollitionS.i)).Position.Y
													- (*enemigo).Position.Y,
											2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPPerseus
							+ (*Enemigo_getSObject()).radioCollitionHiNor)
			{
				return 1;
			}
		}
	}
	else if (tipoEnemigo == EHiKami
			&& (*Nave_getSObject()).poderPerseus.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderPerseus.size;
				CollitionS.i++)
		{
			CollitionS.distancia =
					(float) sqrt(
							pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderPerseus),
											CollitionS.i)).Position.X
											- (*enemigo).Position.X, 2)
									+ pow(
											(*(Poderes*) Lista_RetornaElemento(
													&((*Nave_getSObject()).poderPerseus),
													CollitionS.i)).Position.Y
													- (*enemigo).Position.Y,
											2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPPerseus
							+ (*Enemigo_getSObject()).radioCollitionHiKami)
			{
				return 1;
			}
		}
	}
	else if (tipoEnemigo == EHiSid
			&& (*Nave_getSObject()).poderPerseus.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderPerseus.size;
				CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 5; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderPerseus),
												CollitionS.i)).Position.X
												- ((*enemigo).Position.X
														+ (*Enemigo_getSObject()).collitionXHiSid[CollitionS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderPerseus),
														CollitionS.i)).Position.Y
														- ((*enemigo).Position.Y
																+ (*Enemigo_getSObject()).collitionYHiSid[CollitionS.j]),
												2));

				if (CollitionS.distancia
						<= (*Poderes_getSObject()).radioCollitPPerseus
								+ (*Enemigo_getSObject()).radioCollitionHiSid)
				{
					return 1;
				}
			}
		}
	}
	return 0;
}

//ColisionPoder Pisces a Enemigo
int Collition_colisionPoderPiscesAEnemigo(Collition* collition,
		Enemigo* enemigo, TipoEnemigo tipoEnemigo)
{
	if (tipoEnemigo == ENor && (*Nave_getSObject()).poderPisces.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderPisces.size;
				CollitionS.i++)
		{
			CollitionS.distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionS.i)).Position.X
									- (*enemigo).Position.X, 2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderPisces),
											CollitionS.i)).Position.Y
											- (*enemigo).Position.Y, 2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPPisces
							+ (*Enemigo_getSObject()).radioCollitionNor)
			{
				return 1;
			}
		}
	}
	else if (tipoEnemigo == EKami && (*Nave_getSObject()).poderPisces.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderPisces.size;
				CollitionS.i++)
		{
			CollitionS.distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionS.i)).Position.X
									- (*enemigo).Position.X, 2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderPisces),
											CollitionS.i)).Position.Y
											- (*enemigo).Position.Y, 2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPPisces
							+ (*Enemigo_getSObject()).radioCollitionKami)
			{
				return 1;
			}
		}
	}
	else if (tipoEnemigo == ESid && (*Nave_getSObject()).poderPisces.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderPisces.size;
				CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 5; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderPisces),
												CollitionS.i)).Position.X
												- ((*enemigo).Position.X
														+ (*Enemigo_getSObject()).collitionXSid[CollitionS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderPisces),
														CollitionS.i)).Position.Y
														- ((*enemigo).Position.Y
																+ (*Enemigo_getSObject()).collitionYSid[CollitionS.j]),
												2));

				if (CollitionS.distancia
						<= (*Poderes_getSObject()).radioCollitPPisces
								+ (*Enemigo_getSObject()).radioCollitionSid)
				{
					return 1;
				}
			}
		}
	}
	else if (tipoEnemigo == EHiNor && (*Nave_getSObject()).poderPisces.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderPisces.size;
				CollitionS.i++)
		{
			CollitionS.distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionS.i)).Position.X
									- (*enemigo).Position.X, 2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderPisces),
											CollitionS.i)).Position.Y
											- (*enemigo).Position.Y, 2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPPisces
							+ (*Enemigo_getSObject()).radioCollitionHiNor)
			{
				return 1;
			}
		}
	}
	else if (tipoEnemigo == EHiKami
			&& (*Nave_getSObject()).poderPisces.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderPisces.size;
				CollitionS.i++)
		{
			CollitionS.distancia = (float) sqrt(
					pow(
							(*(Poderes*) Lista_RetornaElemento(
									&((*Nave_getSObject()).poderPisces),
									CollitionS.i)).Position.X
									- (*enemigo).Position.X, 2)
							+ pow(
									(*(Poderes*) Lista_RetornaElemento(
											&((*Nave_getSObject()).poderPisces),
											CollitionS.i)).Position.Y
											- (*enemigo).Position.Y, 2));

			if (CollitionS.distancia
					<= (*Poderes_getSObject()).radioCollitPPisces
							+ (*Enemigo_getSObject()).radioCollitionHiKami)
			{
				return 1;
			}
		}
	}
	if (tipoEnemigo == EHiSid && (*Nave_getSObject()).poderPisces.size > 0)
	{
		for (CollitionS.i = 0;
				CollitionS.i < (*Nave_getSObject()).poderPisces.size;
				CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 5; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										(*(Poderes*) Lista_RetornaElemento(
												&((*Nave_getSObject()).poderPisces),
												CollitionS.i)).Position.X
												- ((*enemigo).Position.X
														+ (*Enemigo_getSObject()).collitionXHiSid[CollitionS.j]),
										2)
										+ pow(
												(*(Poderes*) Lista_RetornaElemento(
														&((*Nave_getSObject()).poderPisces),
														CollitionS.i)).Position.Y
														- ((*enemigo).Position.Y
																+ (*Enemigo_getSObject()).collitionYHiSid[CollitionS.j]),
												2));

				if (CollitionS.distancia
						<= (*Poderes_getSObject()).radioCollitPPisces
								+ (*Enemigo_getSObject()).radioCollitionHiSid)
				{
					return 1;
				}
			}
		}
	}
	return 0;
}

//*****Disparos a nave*******
int Collition_colisionDispEnemigoANave(Collition* collition,
		DisparoEnemigo* disparoEnemigo)
{
	for (CollitionS.i = 0; CollitionS.i < 11; CollitionS.i++)
	{
		CollitionS.distancia =
				(float) sqrt(
						pow(
								(*disparoEnemigo).Position.X
										- ((*Nave_getSObject()).PositionNav.X
												+ (*Nave_getSObject()).collitionX[CollitionS.i]),
								2)
								+ pow(
										(*disparoEnemigo).Position.Y
												- ((*Nave_getSObject()).PositionNav.Y
														+ (*Nave_getSObject()).collitionY[CollitionS.i]),
										2));

		if (CollitionS.distancia
				<= (*disparoEnemigo).radioCollition
						+ (*Nave_getSObject()).radioCollitionN)
		{
			return 1;
		}
	}
	return 0;
}

//colision disp Jefe a nave
int Collition_colisionDispJefeANave(Collition* collition,
		DisparoJefe* disparoJefe)
{
	for (CollitionS.i = 0; CollitionS.i < 11; CollitionS.i++)
	{
		CollitionS.distancia =
				(float) sqrt(
						pow(
								(*disparoJefe).Position.X
										- ((*Nave_getSObject()).PositionNav.X
												+ (*Nave_getSObject()).collitionX[CollitionS.i]),
								2)
								+ pow(
										(*disparoJefe).Position.Y
												- ((*Nave_getSObject()).PositionNav.Y
														+ (*Nave_getSObject()).collitionY[CollitionS.i]),
										2));

		if (CollitionS.distancia
				<= (*disparoJefe).radioCollition
						+ (*Nave_getSObject()).radioCollitionN)
		{
			return 1;
		}
	}

	return 0;
}

//*****Nave y Enemigos*******
int Collition_colisionNaveAEnemigo(Collition* collition, Enemigo* enemigo)
{
	if ((*enemigo).tipoEnemigo == ENor)
		for (CollitionS.i = 0; CollitionS.i < 11; CollitionS.i++)
		{
			CollitionS.distancia =
					(float) sqrt(
							pow(
									(*enemigo).Position.X
											- ((*Nave_getSObject()).PositionNav.X
													+ (*Nave_getSObject()).collitionX[CollitionS.i]),
									2)
									+ pow(
											(*enemigo).Position.Y
													- ((*Nave_getSObject()).PositionNav.Y
															+ (*Nave_getSObject()).collitionY[CollitionS.i]),
											2));

			if (CollitionS.distancia
					<= (*Enemigo_getSObject()).radioCollitionNor
							+ (*Nave_getSObject()).radioCollitionN)
			{
				return 1;
			}
		}

	if ((*enemigo).tipoEnemigo == EKami)
		for (CollitionS.i = 0; CollitionS.i < 11; CollitionS.i++)
		{
			CollitionS.distancia =
					(float) sqrt(
							pow(
									(*enemigo).Position.X
											- ((*Nave_getSObject()).PositionNav.X
													+ (*Nave_getSObject()).collitionX[CollitionS.i]),
									2)
									+ pow(
											(*enemigo).Position.Y
													- ((*Nave_getSObject()).PositionNav.Y
															+ (*Nave_getSObject()).collitionY[CollitionS.i]),
											2));

			if (CollitionS.distancia
					<= (*Enemigo_getSObject()).radioCollitionKami
							+ (*Nave_getSObject()).radioCollitionN)
			{
				return 1;
			}
		}

	if ((*enemigo).tipoEnemigo == ESid)
		for (CollitionS.i = 0; CollitionS.i < 11; CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 5; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										((*enemigo).Position.X
												+ (*Enemigo_getSObject()).collitionXSid[CollitionS.j])
												- ((*Nave_getSObject()).PositionNav.X
														+ (*Nave_getSObject()).collitionX[CollitionS.i]),
										2)
										+ pow(
												((*enemigo).Position.Y
														+ (*Enemigo_getSObject()).collitionYSid[CollitionS.j])
														- ((*Nave_getSObject()).PositionNav.Y
																+ (*Nave_getSObject()).collitionY[CollitionS.i]),
												2));

				if (CollitionS.distancia
						<= (*Enemigo_getSObject()).radioCollitionSid
								+ (*Nave_getSObject()).radioCollitionN)
				{
					return 1;
				}
			}
		}

	if ((*enemigo).tipoEnemigo == EHiNor)
		for (CollitionS.i = 0; CollitionS.i < 11; CollitionS.i++)
		{
			CollitionS.distancia =
					(float) sqrt(
							pow(
									(*enemigo).Position.X
											- ((*Nave_getSObject()).PositionNav.X
													+ (*Nave_getSObject()).collitionX[CollitionS.i]),
									2)
									+ pow(
											(*enemigo).Position.Y
													- ((*Nave_getSObject()).PositionNav.Y
															+ (*Nave_getSObject()).collitionY[CollitionS.i]),
											2));

			if (CollitionS.distancia
					<= (*Enemigo_getSObject()).radioCollitionHiNor
							+ (*Nave_getSObject()).radioCollitionN)
			{
				return 1;
			}
		}

	if ((*enemigo).tipoEnemigo == EHiKami)
		for (CollitionS.i = 0; CollitionS.i < 11; CollitionS.i++)
		{
			CollitionS.distancia =
					(float) sqrt(
							pow(
									(*enemigo).Position.X
											- ((*Nave_getSObject()).PositionNav.X
													+ (*Nave_getSObject()).collitionX[CollitionS.i]),
									2)
									+ pow(
											(*enemigo).Position.Y
													- ((*Nave_getSObject()).PositionNav.Y
															+ (*Nave_getSObject()).collitionY[CollitionS.i]),
											2));

			if (CollitionS.distancia
					<= (*Enemigo_getSObject()).radioCollitionHiKami
							+ (*Nave_getSObject()).radioCollitionN)
			{
				return 1;
			}
		}

	if ((*enemigo).tipoEnemigo == EHiSid)
		for (CollitionS.i = 0; CollitionS.i < 11; CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 5; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										((*enemigo).Position.X
												+ (*Enemigo_getSObject()).collitionXHiSid[CollitionS.j])
												- ((*Nave_getSObject()).PositionNav.X
														+ (*Nave_getSObject()).collitionX[CollitionS.i]),
										2)
										+ pow(
												((*enemigo).Position.Y
														+ (*Enemigo_getSObject()).collitionYHiSid[CollitionS.j])
														- ((*Nave_getSObject()).PositionNav.Y
																+ (*Nave_getSObject()).collitionY[CollitionS.i]),
												2));

				if (CollitionS.distancia
						<= (*Enemigo_getSObject()).radioCollitionHiSid
								+ (*Nave_getSObject()).radioCollitionN)
				{
					return 1;
				}
			}
		}

	return 0;
}

//*****Disparos a Mini*******
int Collition_colisionDispAMini(Collition* collition, NMini* mini,
		DisparoEnemigo* disparo)
{
	for (CollitionS.i = 0; CollitionS.i < 11; CollitionS.i++)
	{
		CollitionS.distancia =
				(float) sqrt(
						pow(
								(*disparo).Position.X
										- ((*mini).Position.X
												+ (*mini).collitionX[CollitionS.i]),
								2)
								+ pow(
										(*disparo).Position.Y
												- ((*mini).Position.Y
														+ (*mini).collitionY[CollitionS.i]),
										2));

		if (CollitionS.distancia
				<= (*disparo).radioCollition + (*mini).radioCollition)
		{
			return 1;
		}
	}
	return 0;
}

//*****Mini a Enemigos*******
int Collition_colisionMiniAEnemigo(Collition* collition, NMini* mini,
		Enemigo* enemigo)
{
	if ((*enemigo).tipoEnemigo == ENor)
		for (CollitionS.i = 0; CollitionS.i < 11; CollitionS.i++)
		{
			CollitionS.distancia =
					(float) sqrt(
							pow(
									(*enemigo).Position.X
											- ((*mini).Position.X
													+ (*mini).collitionX[CollitionS.i]),
									2)
									+ pow(
											(*enemigo).Position.Y
													- ((*mini).Position.Y
															+ (*mini).collitionY[CollitionS.i]),
											2));

			if (CollitionS.distancia
					<= (*Enemigo_getSObject()).radioCollitionNor
							+ (*mini).radioCollition)
			{
				return 1;
			}
		}

	if ((*enemigo).tipoEnemigo == EKami)
		for (CollitionS.i = 0; CollitionS.i < 11; CollitionS.i++)
		{
			CollitionS.distancia =
					(float) sqrt(
							pow(
									(*enemigo).Position.X
											- ((*mini).Position.X
													+ (*mini).collitionX[CollitionS.i]),
									2)
									+ pow(
											(*enemigo).Position.Y
													- ((*mini).Position.Y
															+ (*mini).collitionY[CollitionS.i]),
											2));

			if (CollitionS.distancia
					<= (*Enemigo_getSObject()).radioCollitionKami
							+ (*mini).radioCollition)
			{
				return 1;
			}
		}

	if ((*enemigo).tipoEnemigo == ESid)
		for (CollitionS.i = 0; CollitionS.i < 11; CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 5; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										((*enemigo).Position.X
												+ (*Enemigo_getSObject()).collitionXSid[CollitionS.j])
												- ((*mini).Position.X
														+ (*mini).collitionX[CollitionS.i]),
										2)
										+ pow(
												((*enemigo).Position.Y
														+ (*Enemigo_getSObject()).collitionYSid[CollitionS.j])
														- ((*mini).Position.Y
																+ (*mini).collitionY[CollitionS.i]),
												2));

				if (CollitionS.distancia
						<= (*Enemigo_getSObject()).radioCollitionSid
								+ (*mini).radioCollition)
				{
					return 1;
				}
			}
		}

	if ((*enemigo).tipoEnemigo == EHiNor)
		for (CollitionS.i = 0; CollitionS.i < 11; CollitionS.i++)
		{
			CollitionS.distancia =
					(float) sqrt(
							pow(
									(*enemigo).Position.X
											- ((*mini).Position.X
													+ (*mini).collitionX[CollitionS.i]),
									2)
									+ pow(
											(*enemigo).Position.Y
													- ((*mini).Position.Y
															+ (*mini).collitionY[CollitionS.i]),
											2));

			if (CollitionS.distancia
					<= (*Enemigo_getSObject()).radioCollitionHiNor
							+ (*mini).radioCollition)
			{
				return 1;
			}
		}

	if ((*enemigo).tipoEnemigo == EHiKami)
		for (CollitionS.i = 0; CollitionS.i < 11; CollitionS.i++)
		{
			CollitionS.distancia =
					(float) sqrt(
							pow(
									(*enemigo).Position.X
											- ((*mini).Position.X
													+ (*mini).collitionX[CollitionS.i]),
									2)
									+ pow(
											(*enemigo).Position.Y
													- ((*mini).Position.Y
															+ (*mini).collitionY[CollitionS.i]),
											2));

			if (CollitionS.distancia
					<= (*Enemigo_getSObject()).radioCollitionHiKami
							+ (*mini).radioCollition)
			{
				return 1;
			}
		}

	if ((*enemigo).tipoEnemigo == EHiSid)
		for (CollitionS.i = 0; CollitionS.i < 11; CollitionS.i++)
		{
			for (CollitionS.j = 0; CollitionS.j < 5; CollitionS.j++)
			{
				CollitionS.distancia =
						(float) sqrt(
								pow(
										((*enemigo).Position.X
												+ (*Enemigo_getSObject()).collitionXHiSid[CollitionS.j])
												- ((*mini).Position.X
														+ (*mini).collitionX[CollitionS.i]),
										2)
										+ pow(
												((*enemigo).Position.Y
														+ (*Enemigo_getSObject()).collitionYHiSid[CollitionS.j])
														- ((*mini).Position.Y
																+ (*mini).collitionY[CollitionS.i]),
												2));

				if (CollitionS.distancia
						<= (*Enemigo_getSObject()).radioCollitionHiSid
								+ (*mini).radioCollition)
				{
					return 1;
				}
			}
		}

	return 0;
}

//Comprobacion de poder carafe al colisionar con enemigos
void Collition_confirmaEliminaPoderCarafe(int indice)
{
	if ((*(Poderes*) Lista_RetornaElemento(&((*Nave_getSObject()).poderCarafe),
			indice)).tipoPoderCarafeActual == PCMedio)
	{
		Lista_InsertaEnFinal(&((*Nave_getSObject()).poderCarafeMedioTotal),
				(*Nave_getSObject()).poderCarafeMedioTotal.fin,
				Lista_RetornaElemento(&((*Nave_getSObject()).poderCarafe),
						indice));
	}
	else if ((*(Poderes*) Lista_RetornaElemento(
			&((*Nave_getSObject()).poderCarafe), indice)).tipoPoderCarafeActual
			== PCIzquierda)
	{
		Lista_InsertaEnFinal(&((*Nave_getSObject()).poderCarafeIzquierdaTotal),
				(*Nave_getSObject()).poderCarafeIzquierdaTotal.fin,
				Lista_RetornaElemento(&((*Nave_getSObject()).poderCarafe),
						indice));
	}
	else if ((*(Poderes*) Lista_RetornaElemento(
			&((*Nave_getSObject()).poderCarafe), indice)).tipoPoderCarafeActual
			== PCDerecha)
	{
		Lista_InsertaEnFinal(&((*Nave_getSObject()).poderCarafeDerechaTotal),
				(*Nave_getSObject()).poderCarafeDerechaTotal.fin,
				Lista_RetornaElemento(&((*Nave_getSObject()).poderCarafe),
						indice));
	}
	Lista_RemoveAt(&((*Nave_getSObject()).poderCarafe), indice);
}

// Comprobacion de disparos de la nave al colisionar con enemigos
void Collition_confirmaEliminaDisparoNave(int indice)
{
	if ((*(Disparo*) Lista_RetornaElemento(&((*Nave_getSObject()).disparos),
			indice)).tipoActual == Dpequed
			|| (*(Disparo*) Lista_RetornaElemento(
					&((*Nave_getSObject()).disparos), indice)).tipoActual
					== Dpequei)
	{
		CollitionS.ultimaColisionTipoDisparo =
				(*(Disparo*) Lista_RetornaElemento(
						&((*Nave_getSObject()).disparos), indice)).tipoActual;

		Lista_InsertaEnFinal(&((*Nave_getSObject()).disparosTotales),
				(*Nave_getSObject()).disparosTotales.fin,
				Lista_RetornaElemento(&((*Nave_getSObject()).disparos),
						indice));
		Lista_RemoveAt(&((*Nave_getSObject()).disparos), indice);
	}
	else if ((*(Disparo*) Lista_RetornaElemento(
			&((*Nave_getSObject()).disparos), indice)).tipoActual == DdiagD)
	{
		CollitionS.ultimaColisionTipoDisparo =
				(*(Disparo*) Lista_RetornaElemento(
						&((*Nave_getSObject()).disparos), indice)).tipoActual;

		Lista_InsertaEnFinal(&((*Nave_getSObject()).disparosDiagDerechaTotales),
				(*Nave_getSObject()).disparosDiagDerechaTotales.fin,
				Lista_RetornaElemento(&((*Nave_getSObject()).disparos),
						indice));

		Lista_RemoveAt(&((*Nave_getSObject()).disparos), indice);
	}
	else if ((*(Disparo*) Lista_RetornaElemento(
			&((*Nave_getSObject()).disparos), indice)).tipoActual == DdiagI)
	{
		CollitionS.ultimaColisionTipoDisparo =
				(*(Disparo*) Lista_RetornaElemento(
						&((*Nave_getSObject()).disparos), indice)).tipoActual;

		Lista_InsertaEnFinal(
				&((*Nave_getSObject()).disparosDiagIzquierdaTotales),
				(*Nave_getSObject()).disparosDiagIzquierdaTotales.fin,
				Lista_RetornaElemento(&((*Nave_getSObject()).disparos),
						indice));

		Lista_RemoveAt(&((*Nave_getSObject()).disparos), indice);
	}
	else if ((*(Disparo*) Lista_RetornaElemento(
			&((*Nave_getSObject()).disparos), indice)).tipoActual == Dsuper)
	{
		CollitionS.ultimaColisionTipoDisparo =
				(*(Disparo*) Lista_RetornaElemento(
						&((*Nave_getSObject()).disparos), indice)).tipoActual;

		Lista_InsertaEnFinal(
						&((*Nave_getSObject()).disparosSuperTotales),
						(*Nave_getSObject()).disparosSuperTotales.fin,
						Lista_RetornaElemento(&((*Nave_getSObject()).disparos),
								indice));

		Lista_RemoveAt(&((*Nave_getSObject()).disparos), indice);
	}
}

Collition* Collition_getSObject()
{
	return &CollitionS;
}

